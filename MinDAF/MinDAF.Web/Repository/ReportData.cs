﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Silverlight.Reporting.TestClient.Web.Model;
using System.Data.SqlClient;
using System.Data;

namespace Silverlight.Reporting.TestClient.Web.ModelRepository
{
    public static class ReportData
    {
        // Yes, this is super-lame, but I didn't want to introduce a database dependency

        private const int MaxResults = 30;

        private static Random rnd = new Random();
       
        public static IList<OfficeData> GetOfficeData() 
        {
            var office = new  List<OfficeData>();
            List<OfficeData> _ReturnData = new List<OfficeData>();

            SqlConnection conn = new SqlConnection();
            string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance;;User Id=sa;Password=minda1234";
            // string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance_dirty;User Id=sa;Password=minda1234;";
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionString;
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM mnda_users", con);
            DataSet ds = new DataSet();

           
            da.Fill(ds);
           
            foreach (DataRow item in ds.Tables[0].Rows)
            {
                OfficeData _office = new OfficeData();

                _office.Id = item[0].ToString();
                _office.OfficeName = item[1].ToString();
                office.Add(_office);
            }

            return office;
        }

        public static IList<ReportBalances> GetReportBalances(string _fundsource)
        {
            ReportItem c_item = new ReportItem();

            var _details = new List<ReportBalances>();

            DataSet _dsFundSource = c_item.FetchFundSourceDetails("2017", _fundsource );

           // DataSet _dsActualOBR = c_item.FetchOBRData(_year, _fund_source);

            foreach (DataRow item in _dsFundSource.Tables[0].Rows)
            {
                ReportBalances _item = new ReportBalances();
                _item.ExpenseItem = item["name"].ToString();
                _item.UACS = item["uacs_code"].ToString();
                _item.Allocation = item["Amount"].ToString();
                _item.ActualOBR = "0.00";
                _item.Balances = "0.00";

                _details.Add(_item);

            }

            return _details;
        }
       

        public static IList<ReportItem> GetEmployeeReportData()
        {
            string[] lastNames = new string[] {"Foor", "Badly", "Hackensack", "Anderson", "Lex", "Angel", "Brown", "Block", "Heuer", "Papa", "Imes", "Andreson", "Dente", "Stock", "Fudley", "Tolley", "Sune", "Blather", "Crimson", "Colling"};
            string[] firstNames = new string[] {"Fred", "Bill", "Joe", "Jim", "Jamie", "Jennifer", "Alexis", "Abby", "Ben", "Pete", "Tim", "Tom", "Scott", "Kevin", "Mark", "Marcus", "Brian", "Shawn", "Sean", "Alice", "Mary", "Alex", "Zac", "Jesse", "Terri", "Rich", "Amy", "June" };

            string[] streetNames = new string[] {"Elm", "Main", "Woodland", "Fodder", "Remnet", "Silver", "Poplar", "Mayapple", "Foddergard", "Thompson", "Water", "River", "Sandy View", "Mountain", "Hill", "Hilltop", "Studio", "June" };
            string[] streetSuffixes = new string[] { "St", "St", "St", "Blvd", "Way", "Circle", "Ave", "Ave", "Terrace" };

            string[] cities1 = new string[] {"Able", "Falls", "Aring", "Crof", "Gambri", "Oden", "Eden", "River", "Hill", "Hodge", "Plat"};
            string[] cities2 = new string[] {"town", "ton", " Church", "way", "ton", " Crossing"};
            string[] states = new string[] {"MA", "CT", "RI", "NH", "VT", "PA", "DC", "MD", "DE", "VA" };


            string[] commentIntro = new string[] {"Please", "You had better", "Your coworkers request that you", "Management all agrees that you", "Last request. Please"};
            string[] commentVerbs = new string[] { "stop", "continue", "begin", "eliminate", "terminate" };
            string[] commentVerbs2 = new string[] { "making", "giving", "naming", "coding", "eliminating", "randomizing", "making", "churning", "using" };
            string[] commentObjects = new string[] { "code", "feedback", "reviews", "pens", "animal sounds", "noises", "butter" };
            string[] commentPrep = new string[] {"in your", "on your", "for your", "in the", "for the", "as our" };
            string[] commentPrepObject = new string[] {"cube.", "cubicle.", "code.", "office.", "kitchen.", "bathroom.", "codebase.", "source control."};
            string[] additionalComments = new string[] { "Meh.", "Does good work. ", "Lorem ipsum dolar set.", "Do or do not. There is no try.", "Would like to have on all my teams. ", "Not performing to expectations.", "Probationary.", "Excellent worker, poor dresser.", "You are weak old man.", "Please shower daily.", "Too big to fail.", "Should have stuck to C++.", "Your weaknesses are just overuse of your strengths. You need to work on 'modulation.'", "You're no Tim Heuer!", "I've forgetten why I keep you employed.", "Great presentation skills", "Relaxed attitude", "Loyal.", "Stop naming your functions after simpson's characters."};


            var employees = new List<ReportItem>();

            for (int i = 0; i < MaxResults; i++)
            {
                int performanceRating = rnd.Next(4, 9);

                var item = new ReportItem()
                {
                    FirstName = firstNames.Random(),
                    LastName = lastNames.Random(),
                    Street = rnd.Next(5, 1300) + " " + streetNames.Random() + " " + streetSuffixes.Random(),
                    City = cities1.Random() + cities2.Random(),
                    State = states.Random(),
                    Zip = rnd.Next(15000, 89999).ToString(),
                    ReviewComments = commentIntro.Random() + " " + 
                                    commentVerbs.Random() + " " + commentVerbs2.Random() + " " + 
                                    commentObjects.Random() + " " +
                                    commentPrep.Random() + " " + commentPrepObject.Random() + " " +
                                    additionalComments.Random() + " " + additionalComments.Random(),

                    PerformanceRating = performanceRating,

                    Salary = rnd.Next(45000, 75000),

                    Bonus = rnd.Next (performanceRating * 500, performanceRating * 1000)

                };

                employees.Add(item);
            }


            return employees;

        }


        private static string Random(this string[] source)
        {
            return source[rnd.Next(source.Length-1)];
        }


    }
}