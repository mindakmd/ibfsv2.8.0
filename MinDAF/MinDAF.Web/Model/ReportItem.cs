﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Silverlight.Reporting.TestClient.Web.Model
{
    public class ReportItem
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }

        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

        public int PerformanceRating { get; set; }
        public decimal Salary { get; set; }
        public decimal Bonus { get; set; }

        public string ReviewComments { get; set; }


        public DataSet FetchFundSourceDetails(String _year, String _fundsource)
        {
            var sb = new System.Text.StringBuilder(428);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"msub.uacs_code,");
            sb.AppendLine(@"msub.name,");
            sb.AppendLine(@"(CASE WHEN mfsl.plusminus ='+'");
            sb.AppendLine(@"           		THEN (mfsl.Amount + mfsl.adjustment)");
            sb.AppendLine(@"                 ELSE (mfsl.Amount - mfsl.adjustment) End ) as Amount");
            sb.AppendLine(@"FROM mnda_fund_source_line_item mfsl");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures msub on msub.uacs_code = mfsl.mooe_id");
            sb.AppendLine(@"WHERE mfsl.Fund_Source_Id = '" + _fundsource + "'");
            sb.AppendLine(@"AND mfsl.Year = '2017'");

            return ExecuteDataSQL(sb.ToString());

        }


        private DataSet ExecuteDataSQL(String _sql)
        {


            SqlConnection conn = new SqlConnection();
            string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance;;User Id=sa;Password=minda1234";
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionString;
            SqlDataAdapter da = new SqlDataAdapter(_sql, con);
            DataSet ds = new DataSet();


            da.Fill(ds);


            return ds;
        }
        private DataSet ExecuteImportDataSQL(String _sql)
        {


            SqlConnection conn = new SqlConnection();
            string ConnectionString = FetchConnectionString();
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionString;
            SqlDataAdapter da = new SqlDataAdapter(_sql, con);
            DataSet ds = new DataSet();


            da.Fill(ds);



            return ds;
        }

        private String FetchConnectionString()
        {


            SqlConnection conn = new SqlConnection();
            string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance;;User Id=sa;Password=minda1234";
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionString;
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM dbo.mnda_settings", con);
            DataSet ds = new DataSet();


            da.Fill(ds);

            if (ds.Tables[0].Rows.Count != 0)
            {
                var sb = new System.Text.StringBuilder(94);
                sb.AppendLine(@"Server=" + ds.Tables[0].Rows[0]["host"].ToString() + ";Database=" + ds.Tables[0].Rows[0]["database_name"].ToString() + ";Uid=" + ds.Tables[0].Rows[0]["username"].ToString() + ";");
                sb.AppendLine(@"Pwd=" + ds.Tables[0].Rows[0]["password"].ToString() + ";");


                return sb.ToString();
            }
            return "";
        }
    }
    public class OfficeData
    {
        public String Id { get; set; }
        public String OfficeName { get; set; }
    }
    public class ReportBalances
    {
        public String UACS { get; set; }
        public String ExpenseItem { get; set; }
        public String Allocation { get; set; }
        public String ActualOBR { get; set; }
        public String Balances { get; set; }
    }
}