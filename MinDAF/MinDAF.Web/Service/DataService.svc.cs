﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Collections.Generic;
using Silverlight.Reporting.TestClient.Web.ModelRepository;
using Silverlight.Reporting.TestClient.Web.Model;
using System.Data.SqlClient;
using System.Data;

namespace Silverlight.Reporting.TestClient.Web.Services
{
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class DataService
    {
       

        [OperationContract]
        public IList<OfficeData> GetOfficeData()
        {
            return ReportData.GetOfficeData();
        }

        [OperationContract]
        public IList<ReportBalances> GetOBRDetails(String _fundsource)
        {
            return ReportData.GetReportBalances(_fundsource);
        }


    }

   
}
