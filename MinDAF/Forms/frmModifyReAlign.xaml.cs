﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmModifyReAlign : ChildWindow
    {
        public String WorkingYear { get; set; }
        public String DivisionId { get; set; }

        public String FundCode { get; set; }

        public event EventHandler RefreshData;
        public frmModifyReAlign()
        {
            InitializeComponent();
            this.Loaded += frmModifyReAlign_Loaded;
        }

        void frmModifyReAlign_Loaded(object sender, RoutedEventArgs e)
        {
            LoadActiveRAF();
        }

        List<rel_Listings> dtItems = new List<rel_Listings>();
        private void LoadActiveRAF()
        {
            MinDAFSVCClient svc = new MinDAFSVCClient();
             dtItems = new List<rel_Listings>();
            object _return = null;
            var sb = new System.Text.StringBuilder(438);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mar.id,");
            sb.AppendLine(@"raf.number,");
            sb.AppendLine(@"raf.alignment_id,");
            sb.AppendLine(@"msub.name as FROM_data,");
            sb.AppendLine(@"tsub.name as TO_data,");
            sb.AppendLine(@"mar.total_alignment");
            sb.AppendLine(@"FROM mnda_raf_no raf");
            sb.AppendLine(@"INNER JOIN mnda_alignment_record mar on mar.title = raf.alignment_id");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures msub on msub.uacs_code = mar.from_uacs");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures tsub on tsub.uacs_code = mar.to_uacs");
            sb.AppendLine(@"WHERE raf.year ="+this.WorkingYear +" and raf.status ='For Submission' and raf.div_id = '"+ this.DivisionId +"'");


            svc.ExecuteSQLCompleted += delegate(object sender,ExecuteSQLCompletedEventArgs e)
            {
                var _results = e.Result.ToString();

                svc.CloseAsync();


                XDocument oDocKeyResultsFetchRAF = XDocument.Parse(_results);
                var _dataListsFetchRAF = from info in oDocKeyResultsFetchRAF.Descendants("Table")
                                         select new rel_Listings
                                         {
                                             id = Convert.ToString(info.Element("id").Value),
                                             raf_no = Convert.ToString(info.Element("number").Value),
                                             alignment_id = Convert.ToString(info.Element("alignment_id").Value),
                                             name_from = Convert.ToString(info.Element("FROM_data").Value),
                                             name_to = Convert.ToString(info.Element("TO_data").Value),
                                             total_alignment = Convert.ToString(info.Element("total_alignment").Value)
                                         };



                foreach (var item in _dataListsFetchRAF)
                {
                    rel_Listings _var = new rel_Listings();
                    _var.alignment_id = item.alignment_id;
                    _var.id = item.id;
                    _var.name_from = item.name_from;
                    _var.name_to = item.name_to;
                    _var.raf_no = item.raf_no;
                    _var.total_alignment = item.total_alignment;

                    dtItems.Add(_var);
                }



                grdData.ItemsSource = null;
                grdData.ItemsSource = dtItems;

                grdData.Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
                grdData.Columns["raf_no"].Visibility = System.Windows.Visibility.Collapsed;
                grdData.Columns["alignment_id"].Visibility = System.Windows.Visibility.Collapsed;
                return;
            };
            svc.ExecuteSQLAsync(sb.ToString());
        }
        List<rel_UACSLIST> dtUACSFrom = new List<rel_UACSLIST>();
        List<rel_UACSLIST> dtUACSTo = new List<rel_UACSLIST>();

        private void LoadActiveUACS(String _selecteds)
        {
            MinDAFSVCClient svc = new MinDAFSVCClient();
          
            object _return = null;
            var sb = new System.Text.StringBuilder(328);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"msub.name as expense,");
            sb.AppendLine(@"mfsl.mooe_id as uacs,");
            sb.AppendLine(@"mfsl.amount");
            sb.AppendLine(@"FROM mnda_fund_source_line_item mfsl");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures msub on msub.uacs_code = mfsl.mooe_id");
            sb.AppendLine(@"WHERE mfsl.Fund_Source_Id = '"+ this.FundCode +"' ");
            sb.AppendLine(@"and mfsl.Year =" + this.WorkingYear + " and mfsl.division_id ='" + this.DivisionId + "' ORDER BY msub.name ASC");


            svc.ExecuteSQLCompleted += delegate(object sender, ExecuteSQLCompletedEventArgs e)
            {
                var _results = e.Result.ToString();

                svc.CloseAsync();


                XDocument oDocKeyResultsFetchRAF = XDocument.Parse(_results);
                var _dataListsFetchRAF = from info in oDocKeyResultsFetchRAF.Descendants("Table")
                                         select new rel_UACSLIST
                                         {
                                             expense = Convert.ToString(info.Element("expense").Value),
                                             uacs = Convert.ToString(info.Element("uacs").Value),
                                             amount = Convert.ToString(info.Element("amount").Value),
                                         };

                dtUACSFrom.Clear();
                cmbFrom.Items.Clear();
                String Allocation = "";

                double _value = 0.00;
                foreach (rel_Listings item in dtItems)
                {
                    if (item.name_from == _selecteds)
                    {
                        _value += Convert.ToDouble(item.total_alignment);
                    }
                }

                foreach (var item in _dataListsFetchRAF)
                {
                    rel_UACSLIST _var = new rel_UACSLIST();
                  
                   
                    _var.expense = item.expense;
                    _var.uacs = item.uacs;
                    if (_selecteds == "")
                    {
                        
                        _var.amount = (Convert.ToDouble(item.amount)).ToString();
                        _var.allocation = item.amount;
                        Allocation = _var.amount;
                   
                    }
                    else
                    {
                        if (item.expense == _selecteds)
                        {



                            _var.amount = (Convert.ToDouble(item.amount) - _value).ToString();
                            _var.allocation = item.amount;
                            Allocation = _var.amount;
                        }
                   
                    }
                    
                    dtUACSFrom.Add(_var);
                    cmbFrom.Items.Add(item.expense);
                }


                if (_selecteds !="")
                {
                    for (int i = 0; i < cmbFrom.Items.Count; i++)
                    {
                        if (cmbFrom.Items[i].ToString() == _selecteds)
                        {
                            cmbFrom.SelectedIndex = i;
                            txtAllocation.Value = Allocation;
                            break;
                        }
                    }
                    LoadAllExpenseItems(_selected.name_to);
                }
                else
                {
                    LoadAllExpenseItems("");
                }
               
                
                return;
            };
            svc.ExecuteSQLAsync(sb.ToString());
        }

        private void LoadAllExpenseItems(String _selecteds)
        {
            MinDAFSVCClient svc = new MinDAFSVCClient();

            object _return = null;
            var sb = new System.Text.StringBuilder(184);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"msub.uacs_code as uacs,");
            sb.AppendLine(@"msub.name as expense");
            sb.AppendLine(@"FROM mnda_mooe_sub_expenditures msub");
            sb.AppendLine(@"WHERE msub.is_active = 1 AND msub.expense_type = 'MOOE' ORDER BY msub.name ASC ");
           // sb.AppendLine(@"AND msub.name != '"+ _selecteds +"'");


            svc.ExecuteSQLCompleted += delegate(object sender, ExecuteSQLCompletedEventArgs e)
            {
                var _results = e.Result.ToString();

                svc.CloseAsync();


                XDocument oDocKeyResultsFetchRAF = XDocument.Parse(_results);
                var _dataListsFetchRAF = from info in oDocKeyResultsFetchRAF.Descendants("Table")
                                         select new rel_UACSLIST
                                         {
                                             expense = Convert.ToString(info.Element("expense").Value),
                                             uacs = Convert.ToString(info.Element("uacs").Value)
                                         };
                dtUACSTo.Clear();
                cmbTo.Items.Clear();
                foreach (var item in _dataListsFetchRAF)
                {
                    rel_UACSLIST _var = new rel_UACSLIST();
       
                    _var.expense = item.expense;
                    _var.uacs = item.uacs;

                    dtUACSTo.Add(_var);
                    cmbTo.Items.Add(item.expense);
                }
                if (_selecteds!="")
                {
                    for (int i = 0; i < cmbTo.Items.Count; i++)
                    {
                        if (cmbTo.Items[i].ToString() == _selecteds)
                        {
                            cmbTo.SelectedIndex = i;
                            break;
                        }
                    }

                    txtAmount.Value = _selected.total_alignment;
                }
                else
                {

                    txtAmount.Value = 0;
                }
                return;
            };

            svc.ExecuteSQLAsync(sb.ToString());
        }
        String CurrentRaF = "";
        //changed FOR APPROVAL to FOR SUBMISSION
        private void GetCurrrentRaf() 
        {
             String _sql = "";

            var sb = new System.Text.StringBuilder(125);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  raf.alignment_id");
            sb.AppendLine(@"FROM  dbo.mnda_raf_no raf ");
            sb.AppendLine(@"WHERE raf.year = "+ this.WorkingYear +" and raf.div_id ='"+ this.DivisionId +"' and raf.status='For Submission'");
            sb.AppendLine(@";");


            _sql = sb.ToString();

            MinDAFSVCClient svc = new MinDAFSVCClient();

            object _return = null;
       
            svc.ExecuteSQLCompleted += delegate(object sender, ExecuteSQLCompletedEventArgs e)
            {
                var _results = e.Result.ToString();

                svc.CloseAsync();


                XDocument oDocKeyResultsFetchRAF = XDocument.Parse(_results);
                var _dataListsFetchRAF = from info in oDocKeyResultsFetchRAF.Descendants("Table")
                                         select new DivisionRafNo
                                         {
                                             Raf = Convert.ToString(info.Element("alignment_id").Value)
                                         };
              
                foreach (var item in _dataListsFetchRAF)
                {
                    CurrentRaF = item.Raf;
                }
                SaveData();
                return;
            };

            svc.ExecuteSQLAsync(sb.ToString());

        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        rel_Listings _selected = null;
        private void grdData_CellClicked(object sender, Infragistics.Controls.Grids.CellClickedEventArgs e)
        {
            if (grdData.ActiveItem !=null)
            {
                 _selected = (rel_Listings)grdData.ActiveItem;

                LoadActiveUACS(_selected.name_from);
                
              
            }
        }


        private void UpdateData() 
        {
            _selected = (rel_Listings)grdData.ActiveItem;

            String MOOE_ID = "";
            String Amount = "";
            String Allocation = "";

            List<rel_UACSLIST> _selectFromUpdate = dtUACSFrom.Where(x => x.expense == cmbFrom.SelectedItem.ToString()).ToList();
            if (_selectFromUpdate.Count!=0)
            {
                Allocation = _selectFromUpdate[0].allocation;
            }
            
            List<rel_UACSLIST> _selectUpdate = dtUACSTo.Where(x => x.expense == cmbTo.SelectedItem.ToString()).ToList();

            if (_selectUpdate.Count!=0)
            {
                MOOE_ID = _selectUpdate[0].uacs;
                Amount = txtAmount.Value.ToString();

                if (Convert.ToDouble(Amount)> Convert.ToDouble(Allocation) )
                {
                    MessageBox.Show("Amount entered is greater than the allocation");
                    return;
                }

                var sb = new System.Text.StringBuilder(135);
                sb.AppendLine(@"UPDATE ");
                sb.AppendLine(@"  dbo.mnda_alignment_record  ");
                sb.AppendLine(@"SET ");
                sb.AppendLine(@"  to_uacs = '" + MOOE_ID + "',");
                sb.AppendLine(@"  total_alignment = '" + Amount + "'");
                sb.AppendLine(@"WHERE id =" + _selected.id + "  AND  title = '" + _selected.alignment_id + "' AND fundsource = '" + this.FundCode + "';");


                String _sqlString = sb.ToString();

                MinDAFSVCClient svc = new MinDAFSVCClient();
                object _return = null;

                svc.ExecuteQueryCompleted += delegate(object sender, ExecuteQueryCompletedEventArgs e)
                {
                    _return = e.Result;
                    svc.CloseAsync();
                    Boolean _res = (Boolean)_return;

                    if (_res)
                    {
                        MessageBox.Show("Revision has been updated successfully", "Confirmation", MessageBoxButton.OK);
                    }
                    if (RefreshData!=null)
                    {
                        RefreshData(this, new EventArgs());
                    }
                    LoadActiveRAF();
                    return;
                };
                svc.ExecuteQueryAsync(_sqlString);
            }
           
        }

        private void SaveData() 
        {
            if (cmbTo.SelectedItem == null)
            {
                return;
            }

            String FromUACS = "";
            String ToUACS = "";
            String Allocated = "";
            String Amount = "";
          
            List<rel_UACSLIST> _selectUpdate = dtUACSFrom.Where(x => x.expense == cmbFrom.SelectedItem.ToString()).ToList();
            if (_selectUpdate.Count != 0)
            {
                FromUACS = _selectUpdate[0].uacs;
            }
           _selectUpdate = dtUACSTo.Where(x => x.expense == cmbTo.SelectedItem.ToString()).ToList();

           if (_selectUpdate.Count != 0)
           {
               ToUACS = _selectUpdate[0].uacs;
           }

           Allocated = txtAllocation.Value.ToString();
        //   Amount = _selectUpdate[0].allocation;
           Amount = txtAmount.Value.ToString();


           if (Convert.ToDouble(Amount) > Convert.ToDouble(Allocated))
           {
               MessageBox.Show("Amount entered is greater than the allocation");
               return;
           }

           var sb = new System.Text.StringBuilder(409);
           sb.AppendLine(@"INSERT INTO ");
           sb.AppendLine(@"  dbo.mnda_alignment_record");
           sb.AppendLine(@"(");
           sb.AppendLine(@"  from_uacs,");
           sb.AppendLine(@"  from_total,");
           sb.AppendLine(@"  to_uacs,");
           sb.AppendLine(@"  total_alignment,");
           sb.AppendLine(@"  division_pap,");
           sb.AppendLine(@"  division_year,");
           sb.AppendLine(@"  is_approved,");
           sb.AppendLine(@"  months,");
           sb.AppendLine(@"  fundsource,");
           sb.AppendLine(@"  title,");
           sb.AppendLine(@"  is_active,is_submitted");
           sb.AppendLine(@") ");
           sb.AppendLine(@"VALUES (");
           sb.AppendLine(@"  '"+ FromUACS +"',");
           sb.AppendLine(@"  '0',");
           sb.AppendLine(@"  '"+ ToUACS +"',");
           sb.AppendLine(@"  '" + Amount + "',");
           sb.AppendLine(@"  '"+ this.DivisionId +"',");
           sb.AppendLine(@"  '"+ this.WorkingYear +"',");
           sb.AppendLine(@"  0,");
           sb.AppendLine(@"  '',");
           sb.AppendLine(@"  '"+ this.FundCode +"',");
           sb.AppendLine(@"  '"+ CurrentRaF +"',");
           sb.AppendLine(@"  1,");
           sb.AppendLine(@"  0");
           sb.AppendLine(@");");

           MinDAFSVCClient svc = new MinDAFSVCClient();
           object _return = null;

           svc.ExecuteQueryCompleted += delegate(object sender, ExecuteQueryCompletedEventArgs e)
           {
               _return = e.Result;
               svc.CloseAsync();
               Boolean _res = (Boolean)_return;

               if (_res)
               {
                   MessageBox.Show("Revision has been modified successfully", "Confirmation", MessageBoxButton.OK);
                   LoadActiveRAF();
               }
               if (RefreshData!=null)
               {
                   RefreshData(this, new EventArgs());
               }
               return;
           };
           svc.ExecuteQueryAsync(sb.ToString());
        }

        private void DeleteData()
        {
            MessageBoxResult result;

            result = MessageBox.Show("Confirm Delete Entry", "Confirm", MessageBoxButton.OKCancel);
            if (result== MessageBoxResult.OK)
            {
                if (_selected!=null)
                {
                    
                
                        String _sql = "";
                        var sb = new System.Text.StringBuilder(58);
                        sb.AppendLine(@"DELETE FROM ");
                        sb.AppendLine(@"  dbo.mnda_alignment_record ");
                        sb.AppendLine(@"WHERE  id = " + _selected.id + " ;");

                        _sql = sb.ToString();

                        MinDAFSVCClient svc = new MinDAFSVCClient();
                        object _return = null;

                        svc.ExecuteQueryCompleted += delegate(object sender, ExecuteQueryCompletedEventArgs e)
                        {
                            _return = e.Result;
                            svc.CloseAsync();
                            Boolean _res = (Boolean)_return;

                            if (_res)
                            {
                                MessageBox.Show("Entry has been deleted", "Confirmation", MessageBoxButton.OK);
                                LoadActiveRAF();
                                if (RefreshData != null)
                                {
                                    RefreshData(this, new EventArgs());
                                }
                               
                            }
                          
                            return;
                        };
                        svc.ExecuteQueryAsync(_sql);
                }

            }
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            switch (btnSave.Content.ToString())
            {
                case "Save":
                    UpdateData();
                    break;
                case "Add":
                    GetCurrrentRaf();

                
                    break;
            }
           
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            grdLock.Visibility = System.Windows.Visibility.Visible;
            btnNew.IsEnabled = false;
            LoadActiveUACS("");
            btnSave.Content = "Add";
            btnDelete.Content = "Cancel";
            
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            switch (btnDelete.Content.ToString())
            {
                case "Cancel":
                    cmbFrom.Items.Clear();
                    cmbTo.Items.Clear();
                    txtAllocation.Value = 0.00;
                    txtAmount.Value = 0.00;
                    btnDelete.Content = "Delete";
                    btnSave.Content = "Save";
                    btnNew.IsEnabled = true;
                    grdLock.Visibility = System.Windows.Visibility.Collapsed;
                    break;
                case "Delete":
                    DeleteData();
                    break;
                default:
                    break;
            }
        }

        private void cmbFrom_DropDownClosed(object sender, EventArgs e)
        {
            if (cmbFrom.SelectedItem==null)
            {
                return;
            }
            List<rel_UACSLIST> _selectUpdate = dtUACSFrom.Where(x => x.expense == cmbFrom.SelectedItem.ToString()).ToList();
            if (_selectUpdate.Count!=0)
            {
                double _value = 0.00;
                foreach (rel_Listings item in dtItems)
                {
                    if (item.name_from == cmbFrom.SelectedItem.ToString())
                    {
                        _value += Convert.ToDouble(item.total_alignment);
                    }
                }
                txtAllocation.Value = Convert.ToDouble(_selectUpdate[0].amount) - _value;
            }
        }
    }
}

