﻿using Infragistics.Controls.Grids;
using MinDAF.Class;
using MinDAF.MinDAFS;
using MinDAF.Usercontrol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using Infragistics;

namespace MinDAF.Forms
{
    public partial class approveNonPSDBMSupplies : ChildWindow
    {
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsBudgetSupplies c_budgetsupplies = new clsBudgetSupplies();
        private List<_NonPSDBMSupplies> ListNonPSDBMSupplies = new List<_NonPSDBMSupplies>();
        private int status;
        public approveNonPSDBMSupplies()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }



        private void FetchNonPSDBMSupplies()
        {
            c_budgetsupplies.Process = "FetchNonPSDBMItems";
            svc_mindaf.ExecuteSQLAsync(c_budgetsupplies.FetchProcurementSuppliesNonPSDBM());
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_budgetsupplies.Process)
            {
                case "FetchNonPSDBMItems":
                    XDocument oDocKeyResultsNonPSDBM = XDocument.Parse(_results);
                    var _dataListss = from info in oDocKeyResultsNonPSDBM.Descendants("Table")
                                      select new NonPSDBMSupplies
                                      {
                                          general_category = Convert.ToString(info.Element("general_category").Value),
                                          id = Convert.ToString(info.Element("id").Value),
                                          item_specs = Convert.ToString(info.Element("item_specifications").Value),
                                          price = Convert.ToString(info.Element("price").Value),
                                          sub_category = Convert.ToString(info.Element("sub_category").Value),
                                          unit_of_measurement = Convert.ToString(info.Element("unit_of_measure").Value)

                                      };

                    List<NonPSDBMSupplies> _dataCombos = new List<NonPSDBMSupplies>();
                    
                    foreach (var item in _dataListss)
                    {
                        NonPSDBMSupplies _varCombo = new NonPSDBMSupplies();
                        _NonPSDBMSupplies _itemCombo = new _NonPSDBMSupplies();

                        _varCombo.id = item.id;
                        _varCombo.item_specs = item.item_specs;
                        _varCombo.unit_of_measurement = item.unit_of_measurement;
                        _varCombo.general_category = item.general_category;
                        _varCombo.sub_category = item.sub_category;
                        _varCombo.price = item.price;


                        _itemCombo.id = item.id;
                        _itemCombo.item_specs = item.item_specs;
                        _itemCombo.unit_of_measurement = item.unit_of_measurement;
                        _itemCombo.general_category = item.general_category;
                        _itemCombo.sub_category = item.sub_category;
                        _itemCombo.price = item.price;
                        ListNonPSDBMSupplies.Add(_itemCombo);

                        _dataCombos.Add(_varCombo);
                    }
                    cmbSpecifications.ItemsSource = _dataCombos;
                    grdData.ItemsSource = null;
                    grdData.ItemsSource = _dataCombos;
                    grdData.Columns["id"].Visibility = System.Windows.Visibility.Collapsed;;
                    this.Cursor = Cursors.Arrow;

                    Column col_unit_of_measurement = grdData.Columns.DataColumns["unit_of_measurement"];
                    col_unit_of_measurement.Width = new ColumnWidth(50, false);

                    Column col_price = grdData.Columns.DataColumns["price"];
                    col_price.Width = new ColumnWidth(90, false);
                    //FetchProcurementDetails();
                    //LoadBudgetBalance();
                    break;
            }
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void approveNonPSDBMSupplies_Loaded(object sender, RoutedEventArgs e)
        {
            FetchNonPSDBMSupplies();
        }

        private void Approve_Click(object sender, RoutedEventArgs e)
        {
            status = 1;
            NonPSDBMSupplies selectedItem = (NonPSDBMSupplies)grdData.ActiveItem;
            List<_NonPSDBMSupplies> x = ListNonPSDBMSupplies.Where(item => item.item_specs == selectedItem.item_specs).ToList();
            if (MessageBox.Show("Approve Item: '" + x[0].item_specs + "'?", "Confirmation", MessageBoxButton.OKCancel) == System.Windows.MessageBoxResult.OK)
            {
                c_budgetsupplies.ApproveNonPSDBMSupplies(x[0].id.ToString(), status);
                MessageBox.Show("Item Approved");
            }
        }

        private void Reject_Click(object sender, RoutedEventArgs e)
        {
            status = 0;
            NonPSDBMSupplies selectedItem = (NonPSDBMSupplies)grdData.ActiveItem;
            List<_NonPSDBMSupplies> x = ListNonPSDBMSupplies.Where(item => item.item_specs == selectedItem.item_specs).ToList();
            if (MessageBox.Show("Reject Item: '" + x[0].item_specs + "'?", "Confirmation", MessageBoxButton.OKCancel) == System.Windows.MessageBoxResult.OK)
            {
                c_budgetsupplies.ApproveNonPSDBMSupplies(x[0].id.ToString(), status);
                MessageBox.Show("Item Rejected");
            }
        }

        private void grdData_CellDoubleClicked(object sender, CellClickedEventArgs e)
        {
            //approveNonPSDBM_Child anpsdbm_child = new approveNonPSDBM_Child();

            //NonPSDBMSupplies selectedItem = (NonPSDBMSupplies)grdData.ActiveItem;
            //List<_NonPSDBMSupplies> x = ListNonPSDBMSupplies.Where(item => item.item_specs == selectedItem.item_specs).ToList();
            //anpsdbm_child.ID = x[0].id;
            //anpsdbm_child.item_specifications = x[0].item_specs;
            //anpsdbm_child.Show();
            ////MessageBox.Show(x[0].item_specs);
        }

        private void cmbSpecifications_DropDownClosed(object sender, EventArgs e)
        {
            approveNonPSDBM_Child anpsdbm_child = new approveNonPSDBM_Child();

            var selectedItem = cmbSpecifications.SelectedItem as _NonPSDBMSupplies;

            if (selectedItem != null)
            {
                List<_NonPSDBMSupplies> x = ListNonPSDBMSupplies.Where(item => item.item_specs == selectedItem.item_specs).ToList();
                if (x.Count != 0)
                {
                    //anpsdbm_child.ID = x[0].id;
                    //anpsdbm_child.item_specifications = x[0].item_specs;
                    //anpsdbm_child.Show();
                    MessageBox.Show(x[0].item_specs);
                }

            }
        }

        private void approveNonPSDBMSupplies_MouseEnter(object sender, MouseEventArgs e)
        {
            FetchNonPSDBMSupplies();
        }
    }

    public class NonPSDBMSupplies {
        public string id { get; set; }
        public String item_specs { get; set; }
        public String unit_of_measurement { get; set; }
        public String general_category { get; set; }
        public String sub_category { get; set; }
        public String price { get; set; }
    }

    public class _NonPSDBMSupplies
    {
        public String id { get; set; }
        public String item_specs { get; set; }
        public String unit_of_measurement { get; set; }
        public String general_category { get; set; }
        public String sub_category { get; set; }
        public String price { get; set; }
    }
}

