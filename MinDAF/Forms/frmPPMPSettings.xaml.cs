﻿using MinDAF.Class;
using MinDAF.Forms;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmPPMPSettings : ChildWindow
    {
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsMenu c_menu = new clsMenu();
        private clsPPMP c_ppmp = new clsPPMP();
        private ObservableCollection<ListOfRecords> _objList;
        private ObservableCollection<ListOfSubExUpdate> _subExList;
        public ObservableCollection<ListOfRecords> objList
        {
            get { return _objList; }
            set
            {
                _objList = value;

            }
        }

        int ID = 0;
        String Name = "";
        Boolean IsActive = false;

        public frmPPMPSettings()
        {
            InitializeComponent();
            //_objList = new ObservableCollection<ListOfRecords>();
            //ListOfRecords supNode = new ListOfRecords() { ID = 0, Content = "Select All", 
            //    IsSelected = false};
            //_objList.Add(supNode);
            //for (int i = 1; i <= 10; i++)
            //{
            //    ListOfRecords l = new ListOfRecords() { ID = i, Content = "Content : " + 
            //       i.ToString(), IsSelected = false};
            //    _objList.Add(l);
            //}
            //objList = _objList;
            //lstTemp.ItemsSource = objList;

            //Grid grd = new Grid();

            //for (int i = 0; i < 15; i++)
            //{
            //    RowDefinition rd = new RowDefinition();
            //    rd.SetValue(NameProperty, i.ToString());
            //    rd.Height = new GridLength(25);
            //    grd.RowDefinitions.Add(rd);
            //}
            //grdSubExpenditures.Content = grd;
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            FetchSubExpenditures();
            _subExList = null;
        }


        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_menu.Process)
            {
                case "FetchSubExpenditures":
                    XDocument oDocKeyResultsFetchSubExpenditures = XDocument.Parse(_results);
                    var _dataListsFetchSubExpenditures = from info in oDocKeyResultsFetchSubExpenditures.Descendants("Table")
                                                                select new SubExpendituresList
                                                                {
                                                                    id = Convert.ToInt32(info.Element("id").Value),
                                                                    name = Convert.ToString(info.Element("name").Value),
                                                                    is_for_ppmp = Convert.ToBoolean(info.Element("is_for_ppmp").Value)
                                                                };

                    
                    int _id = 0;
                    int i = 0;
                    String _name = "";
                    Boolean _is_for_ppmp = false;

                    _objList = new ObservableCollection<ListOfRecords>();
                        ListOfRecords supNode = new ListOfRecords()
                        {
                            ID = 0,
                            Content = "Select All",
                            IsSelected = false
                        };
                        _objList.Add(supNode);

                    foreach (var item in _dataListsFetchSubExpenditures)
                    {
                        _id = item.id;
                        _name = item.name;
                        _is_for_ppmp = item.is_for_ppmp;
                        
                        if (_is_for_ppmp == true)
                        {
                            ListOfRecords l = new ListOfRecords()
                            {
                                ID = _id,
                                Content = _name,
                                IsSelected = true
                            };
                            _objList.Add(l);
                        }
                        else
                        {
                            ListOfRecords l = new ListOfRecords()
                            {
                                ID = _id,
                                Content = _name,
                                IsSelected = false
                            };
                            _objList.Add(l);
                        }
                        objList = _objList;
                        lstTemp.ItemsSource = objList;

                        Grid grd = new Grid();

                        foreach (var items in _dataListsFetchSubExpenditures)
                        {
                            i++;
                            RowDefinition rd = new RowDefinition();
                            rd.SetValue(NameProperty, i.ToString());
                            rd.Height = new GridLength(25);
                            grd.RowDefinitions.Add(rd);
                        }
                    }
                    break;
                case "FetchSubExpendituresForUpdating":
                    //XDocument oDocKeyResultsFetchSubExpendituresForUpdating = XDocument.Parse(_results);
                    //var _dataListsFetchSubExpendituresForUpdating = from info in oDocKeyResultsFetchSubExpendituresForUpdating.Descendants("Table")
                    //                                     select new SubExpendituresList
                    //                                     {
                    //                                         id = Convert.ToInt32(info.Element("id").Value),
                    //                                         name = Convert.ToString(info.Element("name").Value),
                    //                                         is_active = Convert.ToBoolean(info.Element("is_active").Value)
                    //                                     };


                    

                    //foreach (var item in _dataListsFetchSubExpendituresForUpdating)
                    //{
                    //    ID = item.id;
                    //    Name = item.name;
                    //    IsActive = item.is_active;
                    //}
                    break;
            }
        }

        private void FetchSubExpenditures()
        {
            c_menu.Process = "FetchSubExpenditures";
            svc_mindaf.ExecuteSQLAsync(c_ppmp.GetSubExpenditures());
            
        }

        private void chk_SubExpenditure_Checked(object sender, RoutedEventArgs e)
        {
            var cb = sender as CheckBox;
            ID = Convert.ToInt32(cb.Tag);
            IsActive = true;
        }

        private void chk_SubExpenditure_Unchecked(object sender, RoutedEventArgs e)
        {
            var cb = sender as CheckBox;
            ID = Convert.ToInt32(cb.Tag);
            IsActive = false;
        }

        private void cancelBtn_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void updateBtn_Click(object sender, RoutedEventArgs e)
        {
            //foreach (var item in _subExList)
            //{
            //    item._Id.ToString();
            //    item._Is_Active.ToString();
            //    MessageBox.Show("ID: " + item._Id.ToString() + "; Is Active: " + item._Is_Active.ToString());
            //}
            if(MessageBox.Show("Proceed with the changes?", "Confirmation", MessageBoxButton.OKCancel) == MessageBoxResult.OK) {
                foreach (var items in _objList)
                {
                //MessageBox.Show(items.Content + " : " + items.IsSelected);
                    c_ppmp.UpdatePPMPExpenseItems(items.ID, items.IsSelected);
                }
            }
            MessageBox.Show("Changes saved.");
            FetchSubExpenditures();
        }

        private void frmPPMPSettings_MouseEnter(object sender, MouseEventArgs e)
        {
            //FetchSubExpenditures();
        }
    }

    public class ListOfRecords
    {
        public int ID { get; set; }
        public String Content { get; set; }
        public Boolean IsSelected { get; set; }
    }

    public class ListOfSubExUpdate
    {
        public int _Id { get; set; }

        public String Content { get; set; }
        public Boolean _Is_For_PPMP { get; set; }
    }

}

