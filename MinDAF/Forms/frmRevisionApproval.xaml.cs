﻿using Infragistics.Controls.Grids;
using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmRevisionApproval : ChildWindow
    {
        public event EventHandler Reload;
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private MinDAFSVCClient svc_service = new MinDAFSVCClient();
        private clsFinanceApproval c_finance = new clsFinanceApproval();

        private List<App_FundSources> ListFundSourceType = new List<App_FundSources>();
        private List<SubPAP_Division> ListSubPap = new List<SubPAP_Division>();
        private List<OfficesPAP> ListOffice = new List<OfficesPAP>();
        private List<Divisions> ListDivision = new List<Divisions>();
        private List<Divisions> ListDivisionUnit = new List<Divisions>();

        private List<ChiefData_FundSourceMain> cd_FundSourceMain = new List<ChiefData_FundSourceMain>();
        private List<ChiefData_FundSourceMainDetails> cd_FundSourceMainDetails = new List<ChiefData_FundSourceMainDetails>();
        private List<ChiefData_FundSource> cd_FundSource = new List<ChiefData_FundSource>();
        private List<ChiefData_ListExpenditure> cd_ExpenditureLists = new List<ChiefData_ListExpenditure>();
        private List<Approved_Data> cd_Approved = new List<Approved_Data>();
        private List<ChiefData_DivisionFundSource> cd_DivisionFundSource = new List<ChiefData_DivisionFundSource>();
        private List<OfficeDetails> BalanceDetails = new List<OfficeDetails>();
        private List<ChiefData_ItemLists> ListItemDetails = new List<ChiefData_ItemLists>();
        private List<OBR_Details> ListGridDataDetails = new List<OBR_Details>();

        private List<RAFApprovedRevisions> ListApprovedRevisions = new List<RAFApprovedRevisions>();
        //    private String FundSource;

        private Boolean isUnit = false;


        private List<BudgetRevisionMain> _Main = new List<BudgetRevisionMain>();
        private List<BudgetRevision> _BudgetItems = new List<BudgetRevision>();
        private List<BudgetActivityItems> _BudgetActivityItems = new List<BudgetActivityItems>();
        private List<BudgetActivityItemsMonths> _BudgetActivityItemsMonths = new List<BudgetActivityItemsMonths>();
        private List<BudgetRevisedData> _BudgetListAdjustment = new List<BudgetRevisedData>();
        private List<BudgetRevisionItems> _BudgetRevision = new List<BudgetRevisionItems>();
        private List<BudgetRevisionTitles> _BudgetRevisionTitles = new List<BudgetRevisionTitles>();
        private List<DivisionRafNo> ListRafNo = new List<DivisionRafNo>();
        private List<DivisionAdjustment> ListAdjustments = new List<DivisionAdjustment>();
        private List<RafForApproval> _ForApprovalRAF = new List<RafForApproval>();
        private Boolean isRevision = false;

        public String WorkingYear { get; set; }

        public frmRevisionApproval(String _work)
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            svc_service.ExecuteSQLCompleted += svc_service_ExecuteSQLCompleted;
            svc_service.ExecuteImportDataSQLCompleted += svc_service_ExecuteImportDataSQLCompleted;
        }

        void svc_service_ExecuteImportDataSQLCompleted(object sender, ExecuteImportDataSQLCompletedEventArgs e)
        {
            String _results = e.Result.ToString();

            switch (c_obr.Process)
            {
                case "FetchOBRData":
                    XDocument oDocKeyFetchGridData = XDocument.Parse(_results);
                    var _dataListsFetchGridData = from info in oDocKeyFetchGridData.Descendants("Table")
                                                  select new OBR_Details
                                                  {
                                                      _Month = Convert.ToString(info.Element("_Month").Value),
                                                      AccountCode = Convert.ToString(info.Element("AccountCode").Value),
                                                      DateCreated = Convert.ToString(info.Element("DateCreated").Value),
                                                      Division = Convert.ToString(info.Element("Division").Value),
                                                      Total = Convert.ToString(info.Element("Total").Value)

                                                  };

                    ListGridDataDetails.Clear();

                    foreach (var item in _dataListsFetchGridData)
                    {
                        OBR_Details _varDetails = new OBR_Details();

                        switch (item._Month)
                        {
                            case "1": _varDetails._Month = "Jan"; break;
                            case "2": _varDetails._Month = "Feb"; break;
                            case "3": _varDetails._Month = "Mar"; break;
                            case "4": _varDetails._Month = "Apr"; break;
                            case "5": _varDetails._Month = "May"; break;
                            case "6": _varDetails._Month = "Jun"; break;
                            case "7": _varDetails._Month = "Jul"; break;
                            case "8": _varDetails._Month = "Aug"; break;
                            case "9": _varDetails._Month = "Sep"; break;
                            case "10": _varDetails._Month = "Oct"; break;
                            case "11": _varDetails._Month = "Nov"; break;
                            case "12": _varDetails._Month = "Dec"; break;
                        }

                        _varDetails.AccountCode = item.AccountCode;
                        _varDetails.DateCreated = item.DateCreated;
                        _varDetails.Division = item.Division;
                        _varDetails.Total = item.Total;

                        ListGridDataDetails.Add(_varDetails);

                    }
                    List<String> _months = new List<string>();

                    for (int i = 1; i < DateTime.Now.Month; i++)
                    {
                        switch (i.ToString())
                        {
                            case "1":
                                _months.Add("Jan");
                                break;
                            case "2":
                                _months.Add("Feb");
                                break;
                            case "3":
                                _months.Add("Mar");
                                break;
                            case "4":
                                _months.Add("Apr");
                                break;
                            case "5":
                                _months.Add("May");
                                break;
                            case "6":
                                _months.Add("Jun");
                                break;
                            case "7":
                                _months.Add("Jul");
                                break;
                            case "8":
                                _months.Add("Aug");
                                break;
                            case "9":
                                _months.Add("Sep");
                                break;
                            case "10":
                                _months.Add("Oct");
                                break;
                            case "11":
                                _months.Add("Nov");
                                break;
                            case "12":
                                _months.Add("Dec");
                                break;
                        }
                    }

                    foreach (var item in _Main)
                    {
                        if (item.TotalAmount.Contains("-----------------"))
                        {
                            break;
                        }
                        double _OBR = 0.00;

                        foreach (string item_months in _months)
                        {
                            double _Total = 0.00;
                            List<OBR_Details> _data = ListGridDataDetails.Where(x => x.AccountCode == item.UACS && x._Month == item_months).ToList();

                            foreach (OBR_Details item_OBR in _data)
                            {
                                _Total += Convert.ToDouble(item_OBR.Total);

                                _OBR += Convert.ToDouble(item_OBR.Total);
                            }
                            switch (item_months)
                            {
                                case "Jan":
                                    if (_Total == 0)
                                    {
                                        item.Jan = "(" + Convert.ToDouble(item.Jan).ToString("#,##0.00") + ")";
                                    }
                                    else
                                    {
                                        item.Jan = _Total.ToString("#,##0.00");
                                    }
                                    break;
                                case "Feb":
                                    if (_Total == 0)
                                    {
                                        item.Feb = "(" + Convert.ToDouble(item.Feb).ToString("#,##0.00") + ")";
                                    }
                                    else
                                    {
                                        item.Feb = _Total.ToString("#,##0.00");
                                    }
                                    break;
                                case "Mar":
                                    if (_Total == 0)
                                    {
                                        item.Mar = "(" + Convert.ToDouble(item.Mar).ToString("#,##0.00") + ")";
                                    }
                                    else
                                    {
                                        item.Mar = _Total.ToString("#,##0.00");
                                    }
                                    break;
                                case "Apr":
                                    if (_Total == 0)
                                    {
                                        item.Apr = "(" + Convert.ToDouble(item.Apr).ToString("#,##0.00") + ")";
                                    }
                                    else
                                    {
                                        item.Apr = _Total.ToString("#,##0.00");
                                    }
                                    break;
                                case "May":
                                    if (_Total == 0)
                                    {
                                        item.May = "(" + Convert.ToDouble(item.May).ToString("#,##0.00") + ")";
                                    }
                                    else
                                    {
                                        item.May = _Total.ToString("#,##0.00");
                                    }
                                    break;
                                case "Jun":
                                    if (_Total == 0)
                                    {
                                        item.Jun = "(" + Convert.ToDouble(item.Jun).ToString("#,##0.00") + ")";
                                    }
                                    else
                                    {
                                        item.Jun = _Total.ToString("#,##0.00");
                                    }
                                    break;
                                case "Jul":
                                    if (_Total == 0)
                                    {
                                        item.Jul = "(" + Convert.ToDouble(item.Jul).ToString("#,##0.00") + ")";
                                    }
                                    else
                                    {
                                        item.Jul = _Total.ToString("#,##0.00");
                                    }
                                    break;

                                case "Aug":
                                    if (_Total == 0)
                                    {
                                        item.Aug = "(" + Convert.ToDouble(item.Aug).ToString("#,##0.00") + ")";
                                    }
                                    else
                                    {
                                        item.Aug = _Total.ToString("#,##0.00");
                                    }
                                    break;
                                case "Sep":
                                    if (_Total == 0)
                                    {
                                        item.Sep = "(" + Convert.ToDouble(item.Sep).ToString("#,##0.00") + ")";
                                    }
                                    else
                                    {
                                        item.Sep = _Total.ToString("#,##0.00");
                                    }
                                    break;
                                case "Oct":
                                    if (_Total == 0)
                                    {
                                        item.Oct = "( " + Convert.ToDouble(item.Oct).ToString("#,##0.00") + ")";
                                    }
                                    else
                                    {
                                        item.Oct = _Total.ToString("#,##0.00");
                                    }
                                    break;
                                case "Nov":
                                    if (_Total == 0)
                                    {
                                        item.Nov = "(" + Convert.ToDouble(item.Nov).ToString("#,##0.00") + ")";
                                    }
                                    else
                                    {
                                        item.Nov = _Total.ToString("#,##0.00");
                                    }
                                    break;
                                case "Dec":
                                    if (_Total == 0)
                                    {
                                        item.Dec = "(" + Convert.ToDouble(item.Dec).ToString("#,##0.00") + ")";
                                    }
                                    else
                                    {
                                        item.Dec = _Total.ToString("#,##0.00");
                                    }
                                    break;
                            }


                        }
                        item.TotalOBR = _OBR.ToString("#,##0.00");
                        item.RemainingBalance = (Convert.ToDouble(item.Allocation) - _OBR).ToString("#,##0.00");
                    }

                    FetchActivityData();

                    //ComputeItem();
                    //BudgetRevisionMain _totals_line = new BudgetRevisionMain();
                    //_totals_line.ExpenseItem = "---------------------------------------------------";
                    //_totals_line.Allocation = "-----------------";
                    //_totals_line.TotalExpense = "-----------------";
                    //_totals_line.Adjustment = "-----------------";
                    //_totals_line.TotalOBR = "-----------------";
                    //_totals_line.RemainingBalance = "-----------------";


                    //BudgetRevisionMain _totals = new BudgetRevisionMain();
                    //double _TotalAllocation = 0.00;
                    //double _TotalExpense = 0.00;
                    //double _TotalOBR = 0.00;
                    //double _TotalBalance = 0.00;
                    //double _TotalAdjustmentsplus = 0.00;
                    //double _TotalAdjustmentsminus = 0.00;
                    //foreach (var item in _Main)
                    //{
                    //    _TotalAllocation += Convert.ToDouble(item.Allocation);
                    //    _TotalExpense += Convert.ToDouble(item.TotalExpense);
                    //    _TotalOBR += Convert.ToDouble(item.TotalOBR);
                    //    _TotalBalance += Convert.ToDouble(item.RemainingBalance);
                    //    if (item.Adjustment.Contains("("))
                    //    {
                    //        String _val = item.Adjustment.Replace("(","");
                    //        _val = _val.Replace(")","").Trim();

                    //        _TotalAdjustmentsminus += Convert.ToDouble(_val);
                    //    }
                    //    else
                    //    {
                    //        _TotalAdjustmentsplus += Convert.ToDouble(item.Adjustment);
                    //    }

                    //}
                    //_totals.ExpenseItem = "Total";
                    //_totals.Allocation = _TotalAllocation.ToString("#,##0.00");
                    //_totals.TotalExpense = _TotalExpense.ToString("#,##0.00");
                    //_totals.TotalOBR = _TotalOBR.ToString("#,##0.00");
                    //_totals.RemainingBalance = _TotalBalance.ToString("#,##0.00");
                    //_totals.Adjustment = (_TotalAdjustmentsplus - _TotalAdjustmentsminus).ToString("#,##0.00");
                    //_Main.Add(_totals_line);
                    //_Main.Add(_totals);
                    //grdData.ItemsSource = null;
                    //grdData.ItemsSource = _Main;
                    //FixColumns();
                    break;


            }
        }
        private void FixColumns()
        {


            Column col_ExpenseItem = grdData.Columns.DataColumns["ExpenseItem"];
            Column col_Allocation = grdData.Columns.DataColumns["Allocation"];
            Column col_TotalExpense = grdData.Columns.DataColumns["TotalExpense"];
            //   Column col_BalanceContigency = grdData.Columns.DataColumns["BalanceContigency"];
            Column col_Adjustment = grdData.Columns.DataColumns["Adjustment"];
            Column col_TotalOBR = grdData.Columns.DataColumns["TotalOBR"];
            Column col_RemainingBalance = grdData.Columns.DataColumns["RemainingBalance"];
            Column col_TotalAMount = grdData.Columns.DataColumns["TotalAmount"];

            Column col_Jan = grdData.Columns.DataColumns["Jan"];
            Column col_Feb = grdData.Columns.DataColumns["Feb"];
            Column col_Mar = grdData.Columns.DataColumns["Mar"];
            Column col_Apr = grdData.Columns.DataColumns["Apr"];
            Column col_May = grdData.Columns.DataColumns["May"];
            Column col_Jun = grdData.Columns.DataColumns["Jun"];
            Column col_Jul = grdData.Columns.DataColumns["Jul"];
            Column col_Aug = grdData.Columns.DataColumns["Aug"];
            Column col_Sep = grdData.Columns.DataColumns["Sep"];
            Column col_Oct = grdData.Columns.DataColumns["Oct"];
            Column col_Nov = grdData.Columns.DataColumns["Nov"];
            Column col_Dec = grdData.Columns.DataColumns["Dec"];

            col_ExpenseItem.Width = new ColumnWidth(350, false);
            col_Allocation.Width = new ColumnWidth(100, false);

            col_TotalOBR.Width = new ColumnWidth(100, false);
            col_TotalExpense.Width = new ColumnWidth(100, false);
            //     col_BalanceContigency.Width = new ColumnWidth(100, false);
            col_Adjustment.Width = new ColumnWidth(100, false);
            col_RemainingBalance.Width = new ColumnWidth(100, true);

            col_Jan.Width = new ColumnWidth(100, false);
            col_Feb.Width = new ColumnWidth(100, false);
            col_Mar.Width = new ColumnWidth(100, false);
            col_Apr.Width = new ColumnWidth(100, false);
            col_May.Width = new ColumnWidth(100, false);
            col_Jun.Width = new ColumnWidth(100, false);
            col_Jul.Width = new ColumnWidth(100, false);
            col_Aug.Width = new ColumnWidth(100, false);
            col_Sep.Width = new ColumnWidth(100, false);
            col_Oct.Width = new ColumnWidth(100, false);
            col_Nov.Width = new ColumnWidth(100, false);
            col_Dec.Width = new ColumnWidth(100, false);

            col_TotalExpense.HeaderText = "Adjusted";
            col_RemainingBalance.HeaderText = "Balance";
            col_TotalOBR.HeaderText = "Total OBR";
            grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_ExpenseItem);
            grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_Allocation);
            grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_Adjustment);
            grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_TotalExpense);

            grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_TotalOBR);
            grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_RemainingBalance);



            grdData.Columns["UACS"].Visibility = System.Windows.Visibility.Collapsed;
            grdData.Columns["Status"].Visibility = System.Windows.Visibility.Collapsed;

            col_Jan.Visibility = System.Windows.Visibility.Collapsed;
            col_Feb.Visibility = System.Windows.Visibility.Collapsed;
            col_Mar.Visibility = System.Windows.Visibility.Collapsed;
            col_Apr.Visibility = System.Windows.Visibility.Collapsed;
            col_May.Visibility = System.Windows.Visibility.Collapsed;
            col_Jun.Visibility = System.Windows.Visibility.Collapsed;
            col_Jul.Visibility = System.Windows.Visibility.Collapsed;
            col_Aug.Visibility = System.Windows.Visibility.Collapsed;
            col_Sep.Visibility = System.Windows.Visibility.Collapsed;
            col_Oct.Visibility = System.Windows.Visibility.Collapsed;
            col_Nov.Visibility = System.Windows.Visibility.Collapsed;
            col_Dec.Visibility = System.Windows.Visibility.Collapsed;
            col_TotalAMount.Visibility = System.Windows.Visibility.Collapsed;
        }
        private clsOBRData c_obr = new clsOBRData();
        private void FetchOBRData()
        {
            // lblTitle.Content = "Obligation Expense and Balances for Fund Source : " + _FundSource;
            c_obr.Process = "FetchOBRData";
            svc_service.ExecuteImportDataSQLAsync(c_obr.FetchOBRData(this.SelectedYear, this.FundName));
        }

        private void FetchBudgetRevisionTitles()
        {
            c_ppmp.Process = "FetchBudgetRevisionTitles";
            svc_service.ExecuteSQLAsync(c_ppmp.FetchRevisionTitles(this.FundSource, this.DivId, this.SelectedYear));
        }
        private void FetchBudgetRevision()
        {
            c_ppmp.Process = "FetchBudgetRevision";
            svc_service.ExecuteSQLAsync(c_ppmp.FetchRevisions(this.FundSource, this.DivId, this.SelectedYear));
        }
        private void FetchFundSource()
        {
            string f_type = "";
            List<App_FundSources> _data = ListFundSourceType.Where(x => x.Name == cmbFundSource.SelectedItem.ToString()).ToList();

            if (_data.Count != 0)
            {
                f_type = _data[0].Code;
            }
            c_ppmp.Process = "FetchFundSource";
            svc_service.ExecuteSQLAsync(c_ppmp.FetchFundSourceApproval(this.DivId, f_type,this.WorkingYear));
        }

        private void FetchActivityData()
        {
            c_ppmp.Process = "FetchActivityData";
            svc_service.ExecuteSQLAsync(c_ppmp.FetchActivityData(this.FundSource));
        }
        private void FetchRevisions()
        {
            c_ppmp.Process = "FetchRevisions";
            svc_service.ExecuteSQLAsync(c_ppmp.FetchActivityData(this.FundSource));
        }

        private void FetchActivityDataMonths()
        {
            c_ppmp.Process = "FetchActivityDataMonths";
            svc_service.ExecuteSQLAsync(c_ppmp.FetchActivityDataWithMonths(this.FundSource));
        }
        private void usr_b_revision_Loaded(object sender, RoutedEventArgs e)
        {
            FetchBudgetAllocation();
        }
        private Boolean HasAdjustments = false;
        void svc_service_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_ppmp.Process)
            {
                case "FetchRequestRevisions":
                    XDocument oDocFetchApprovedRevisions = XDocument.Parse(_results);
                    var _dataFetchApprovedRevisions = from info in oDocFetchApprovedRevisions.Descendants("Table")
                                                       select new RAFApprovedRevisions
                                                       {
                                                           from_total = Convert.ToString(info.Element("from_total").Value),
                                                           from_uacs = Convert.ToString(info.Element("from_uacs").Value),
                                                           to_uacs = Convert.ToString(info.Element("to_uacs").Value),
                                                           total_alignment = Convert.ToString(info.Element("total_alignment").Value),

                                                       };



                    ListApprovedRevisions.Clear();

                    foreach (var item in _dataFetchApprovedRevisions)
                    {
                        RAFApprovedRevisions _varData = new RAFApprovedRevisions();

                        _varData.from_total = item.from_total;
                        _varData.from_uacs = item.from_uacs;
                        _varData.to_uacs = item.to_uacs;
                        _varData.total_alignment = item.total_alignment;

                        ListApprovedRevisions.Add(_varData);

                    }

                    if (isApproved)
                    {
                        if (hasUnApproved)
                        {
                            btnApprove.Content = "Approve";
                            btnApprove.IsEnabled = true;
                        }
                        else
                        {
                            btnApprove.Content = "Unlock";
                            if (isRevision)
                            {
                                btnApprove.IsEnabled = true;
                            }
                            else
                            {
                                btnApprove.IsEnabled = false;
                            }
                        }


                    }
                    else
                    {
                        btnApprove.Content = "Approve";
                        btnApprove.IsEnabled = true;
                    }
                


                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchExpenditureDetails":
                    XDocument oDocFetchExpenditureDetails = XDocument.Parse(_results);
                    var _dataFetchExpenditureDetails = from info in oDocFetchExpenditureDetails.Descendants("Table")
                                                       select new ChiefData_ListExpenditure
                                                       {
                                                           expenditure = Convert.ToString(info.Element("expenditure").Value),
                                                           fund_source_id = Convert.ToString(info.Element("fund_source_id").Value),
                                                           total = Convert.ToString(info.Element("total").Value)

                                                       };



                    cd_ExpenditureLists.Clear();

                    foreach (var item in _dataFetchExpenditureDetails)
                    {
                        ChiefData_ListExpenditure _varData = new ChiefData_ListExpenditure();

                        _varData.expenditure = item.expenditure;
                        _varData.fund_source_id = item.fund_source_id;
                        _varData.total = item.total;

                        cd_ExpenditureLists.Add(_varData);

                    }
                    if (isUnit)
                    {
                        FetchBudgetAllocationDataProject();
                    }
                    else
                    {
                        FetchBudgetAllocationData();
                    }
                    

                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchBudgetRevisionTitles":
                    XDocument oDocKeyResultsFetchBudgetRevisionTitles = XDocument.Parse(_results);
                    var _dataListsFetchBudgetRevisionTitles = from info in oDocKeyResultsFetchBudgetRevisionTitles.Descendants("Table")
                                                              select new BudgetRevisionTitles
                                                              {
                                                                  Title = Convert.ToString(info.Element("title").Value)

                                                              };

                    _BudgetRevisionTitles.Clear();

                    foreach (var item in _dataListsFetchBudgetRevisionTitles)
                    {

                        BudgetRevisionTitles _varProf = new BudgetRevisionTitles();

                        _varProf.Title = item.Title;

                        _BudgetRevisionTitles.Add(_varProf);

                    }

                    FetchBudgetRevision();
                    this.Cursor = Cursors.Arrow;

                    break;
                case "FetchRequestRevision":
                    XDocument oDocKeyResultsFetchRequestRevision = XDocument.Parse(_results);
                    var _dataListsFetchRequestRevision = from info in oDocKeyResultsFetchRequestRevision.Descendants("Table")
                                                         select new BudgetRevisionCount
                                                         {
                                                             _Count = Convert.ToString(info.Element("_count").Value)

                                                         };



                    foreach (var item in _dataListsFetchRequestRevision)
                    {

                        if (item._Count == "0")
                        {
                            isRevision = false;
                        }
                        else
                        {
                            isRevision = true;
                        }

                        break;

                    }
                    FetchOBRData();

                    this.Cursor = Cursors.Arrow;

                    break;
                case "FetchBudgetRevision":
                    XDocument oDocKeyResultsFetchBudgetRevision = XDocument.Parse(_results);
                    var _dataListsFetchBudgetRevision = from info in oDocKeyResultsFetchBudgetRevision.Descendants("Table")
                                                        select new BudgetRevisionItems
                                                        {
                                                            division_pap = Convert.ToString(info.Element("division_pap").Value),
                                                            division_year = Convert.ToString(info.Element("division_year").Value),
                                                            from_total = Convert.ToString(info.Element("from_total").Value),
                                                            from_uacs = Convert.ToString(info.Element("from_uacs").Value),
                                                            fundsource = Convert.ToString(info.Element("fundsource").Value),
                                                            to_uacs = Convert.ToString(info.Element("to_uacs").Value),
                                                            total_alignment = Convert.ToString(info.Element("total_alignment").Value),
                                                            from_expense_item = Convert.ToString(info.Element("from_expense_item").Value),
                                                            to_expense_item = Convert.ToString(info.Element("to_expense_item").Value),
                                                            is_approved = Convert.ToString(info.Element("is_approved").Value)
                                                        };

                    _BudgetRevision.Clear();

                    foreach (var item in _dataListsFetchBudgetRevision)
                    {

                        BudgetRevisionItems _varProf = new BudgetRevisionItems();

                        _varProf.division_pap = item.division_pap;
                        _varProf.division_year = item.division_year;
                        _varProf.from_total = item.from_total;
                        _varProf.from_uacs = item.from_uacs;
                        _varProf.fundsource = item.fundsource;
                        _varProf.to_uacs = item.to_uacs;
                        _varProf.total_alignment = item.total_alignment;
                        _varProf.from_expense_item = item.from_expense_item;
                        _varProf.to_expense_item = item.to_expense_item;
                        _varProf.is_approved = item.is_approved;
                        _BudgetRevision.Add(_varProf);

                    }

                    FetchRequestRevision();
                    this.Cursor = Cursors.Arrow;

                    break;
                case "FetchBudgetAllocationData":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new BudgetRevision
                                     {
                                         Uacs = Convert.ToString(info.Element("uacs_code").Value),
                                         ExpenseItem = Convert.ToString(info.Element("expense_item").Value),
                                         Amount = Convert.ToString(info.Element("Amount").Value)

                                     };

                    _BudgetItems.Clear();

                    foreach (var item in _dataLists)
                    {

                        BudgetRevision _varProf = new BudgetRevision();

                        _varProf.Uacs = item.Uacs;
                        _varProf.ExpenseItem = item.ExpenseItem;
                        _varProf.Amount = Convert.ToDouble(item.Amount).ToString("#,##0.00"); ;

                        _BudgetItems.Add(_varProf);

                    }


                    this.Cursor = Cursors.Arrow;

                    FetchFundSource();
                    break;
                case "FetchFundSource":
                    XDocument oDocKeyResultsFetchFundSource = XDocument.Parse(_results);
                    var _dataListsFetchFundSource = from info in oDocKeyResultsFetchFundSource.Descendants("Table")
                                                    select new BudgetRevisionFundSource
                                                    {
                                                        Code = Convert.ToString(info.Element("code").Value),
                                                        Name = Convert.ToString(info.Element("respo_name").Value)
                                                    };



                    foreach (var item in _dataListsFetchFundSource)
                    {
                        //if (item.Name.Contains("CONT"))
                        //{
                        //    continue;   
                        //}
                        FundSource = item.Code;
                        FundName = item.Name;
                   

                    }

                    this.Cursor = Cursors.Arrow;
                    FetchBudgetRAFNo();
                    break;
                case "FetchBudgetRAFNo":
                    XDocument oDocKeyResultsFetchBudgetRAFNo = XDocument.Parse(_results);
                    var _dataListsFetchBudgetRAFNo = from info in oDocKeyResultsFetchBudgetRAFNo.Descendants("Table")
                                                     select new DivisionRafNo
                                                     {
                                                         Raf = Convert.ToString(info.Element("number").Value)

                                                     };

                    ListRafNo.Clear();

                    foreach (var item in _dataListsFetchBudgetRAFNo)
                    {

                        DivisionRafNo _varProf = new DivisionRafNo();

                        _varProf.Raf = item.Raf;


                        ListRafNo.Add(_varProf);

                    }


                    this.Cursor = Cursors.Arrow;
                    FetchBudgetAdjustments();
                    //  FetchActivityDataMonths();

                    break;
                case "FetchBudgetRAFNoForApproval":
                    XDocument oDocKeyResultsFetchBudgetRafNoForApproval = XDocument.Parse(_results);
                    var _dataListsFetchBudgetRafNoForApproval = from info in oDocKeyResultsFetchBudgetRafNoForApproval.Descendants("Table")
                                                                select new RafForApproval
                                                     {
                                                         Raf = Convert.ToString(info.Element("number").Value)

                                                     };

                    _ForApprovalRAF.Clear();

                    foreach (var item in _dataListsFetchBudgetRafNoForApproval)
                    {

                        RafForApproval _varProf = new RafForApproval();
                        
                        _varProf.Raf = item.Raf;


                        _ForApprovalRAF.Add(_varProf);

                    }


                    this.Cursor = Cursors.Arrow;
                    FetchActivityDataMonths();

                    break;
                case "FetchBudgetAdjustments":
                    XDocument oDocKeyResultsFetchBudgetAdjustments = XDocument.Parse(_results);
                    var _dataListsFetchBudgetAdjustments = from info in oDocKeyResultsFetchBudgetAdjustments.Descendants("Table")
                                                           select new DivisionAdjustment
                                                           {
                                                               Raf = Convert.ToString(info.Element("number").Value),
                                                               Mooe_Id = Convert.ToString(info.Element("mooe_id").Value),
                                                               Adjustments = Convert.ToString(info.Element("adjustment").Value),
                                                               PlusMinus = Convert.ToString(info.Element("plusminus").Value)

                                                           };

                    ListAdjustments.Clear();

                    foreach (var item in _dataListsFetchBudgetAdjustments)
                    {

                        DivisionAdjustment _varProf = new DivisionAdjustment();

                        _varProf.Adjustments = item.Adjustments;
                        _varProf.Mooe_Id = item.Mooe_Id;
                        _varProf.PlusMinus = item.PlusMinus;
                        _varProf.Raf = item.Raf;

                        ListAdjustments.Add(_varProf);

                    }

                    foreach (DivisionRafNo item in ListRafNo)
                    {
                        List<DivisionAdjustment> _filtered = ListAdjustments.Where(x => x.Raf == item.Raf).ToList();

                        foreach (BudgetRevision item_f in _BudgetItems)
                        {
                            List<DivisionAdjustment> _selection = _filtered.Where(x => x.Mooe_Id == item_f.Uacs).ToList();
                            if (_selection.Count != 0)
                            {
                                foreach (DivisionAdjustment item_s in _selection)
                                {
                                    double _adjustment = Convert.ToDouble(item_s.Adjustments);
                                    double _allocation = Convert.ToDouble(item_f.Amount);
                                    double _new_allocation = 0.00;

                                    switch (item_s.PlusMinus)
                                    {
                                        case "+":

                                            _new_allocation = _allocation + _adjustment;
                                            break;

                                        case "-":
                                            _new_allocation = _allocation - _adjustment;
                                            break;

                                    }

                                    item_f.Amount = _new_allocation.ToString("#,##0.00");
                                }
                            }
                        }

                    }




                    this.Cursor = Cursors.Arrow;
                    FetchBudgetRAFNoForApproval();
                    //FetchActivityDataMonths();

                    break;
                case "FetchActivityDataMonths":
                    XDocument oDocKeyResultsFetchActivityDataMonths = XDocument.Parse(_results);
                    var _dataListsFetchActivityDataMonths = from info in oDocKeyResultsFetchActivityDataMonths.Descendants("Table")
                                                            select new BudgetActivityItemsMonths
                                                            {
                                                                name = Convert.ToString(info.Element("name").Value),
                                                                total = Convert.ToString(info.Element("total").Value),
                                                                month = Convert.ToString(info.Element("month").Value),
                                                                uacs_code = Convert.ToString(info.Element("uacs_code").Value)

                                                            };



                    _BudgetActivityItemsMonths.Clear();
                    foreach (var item in _dataListsFetchActivityDataMonths)
                    {

                        BudgetActivityItemsMonths _varProf = new BudgetActivityItemsMonths();

                        _varProf.name = item.name;
                        _varProf.total = item.total;
                        _varProf.month = item.month;
                        _varProf.uacs_code = item.uacs_code;

                        _BudgetActivityItemsMonths.Add(_varProf);

                    }

                    this.Cursor = Cursors.Arrow;
                    FetchBudgetRevisionTitles();

                    break;
                case "FetchActivityData":
                    XDocument oDocKeyResultsFetchActivityData = XDocument.Parse(_results);
                    var _dataListsFetchActivityData = from info in oDocKeyResultsFetchActivityData.Descendants("Table")
                                                      select new BudgetActivityItems
                                                      {
                                                          name = Convert.ToString(info.Element("name").Value),
                                                          total = Convert.ToString(info.Element("total").Value),
                                                          uacs_code = Convert.ToString(info.Element("uacs_code").Value)


                                                      };


                    _BudgetActivityItems.Clear();
                    foreach (var item in _dataListsFetchActivityData)
                    {

                        BudgetActivityItems _varProf = new BudgetActivityItems();

                        _varProf.name = item.name;
                        _varProf.total = item.total;
                        _varProf.uacs_code = item.uacs_code;

                        _BudgetActivityItems.Add(_varProf);

                    }
                    _Main.Clear();

                    foreach (BudgetRevision item in _BudgetItems)
                    {
                        BudgetRevisionMain _items = new BudgetRevisionMain();
                        _items.UACS = item.Uacs;
                        _items.ExpenseItem = item.ExpenseItem;
                        //List<BudgetRevisionItems> _revised = _BudgetRevision.Where(x => x.from_expense_item == item.ExpenseItem).ToList();

                        //if (_revised.Count != 0)
                        //{
                        //    double _totalMinus = 0.00;
                        //    foreach (BudgetRevisionItems itemData in _revised)
                        //    {
                        //        _totalMinus += Convert.ToDouble(itemData.total_alignment);
                        //    }

                        //    _items.Allocation = (Convert.ToDouble(item.Amount) - _totalMinus).ToString("#,##0.00");
                        //}
                        //else
                        //{
                        _items.Allocation = Convert.ToDouble(item.Amount).ToString("#,##0.00");
                        // }




                        List<BudgetActivityItems> _act_items = _BudgetActivityItems.Where(x => x.name == item.ExpenseItem).ToList();

                        double _total = 0.00;

                        foreach (BudgetActivityItems item_budget in _act_items)
                        {
                            _total += Convert.ToDouble(item_budget.total);
                        }
                        _items.TotalExpense = _total.ToString("#,##0.00");
                        //     _items.BalanceContigency = (Convert.ToDouble(item.Amount) - _total).ToString("#,##0.00");




                        _Main.Add(_items);

                    }

                    foreach (BudgetRevisionMain _DataDetail in _Main)
                    {
                        List<BudgetActivityItemsMonths> item_months = _BudgetActivityItemsMonths.Where(x => x.name == _DataDetail.ExpenseItem).ToList();
                        double _janTotal = 0.00;
                        double _febTotal = 0.00;
                        double _marTotal = 0.00;
                        double _aprTotal = 0.00;
                        double _mayTotal = 0.00;
                        double _junTotal = 0.00;
                        double _julTotal = 0.00;
                        double _augTotal = 0.00;
                        double _sepTotal = 0.00;
                        double _octTotal = 0.00;
                        double _novTotal = 0.00;
                        double _decTotal = 0.00;
                        double _LineTotal = 0.00;

                        foreach (BudgetActivityItemsMonths item_data in item_months)
                        {

                            _LineTotal += Convert.ToDouble(item_data.total);
                            switch (item_data.month)
                            {
                                case "Jan":
                                    _janTotal += Convert.ToDouble(item_data.total);
                                    _DataDetail.Jan = _janTotal.ToString("#,##0.00");

                                    break;
                                case "Feb":
                                    _febTotal += Convert.ToDouble(item_data.total);
                                    _DataDetail.Feb = _febTotal.ToString("#,##0.00");

                                    break;
                                case "Mar":
                                    _marTotal += Convert.ToDouble(item_data.total);
                                    _DataDetail.Mar = _marTotal.ToString("#,##0.00");
                                    break;
                                case "Apr":
                                    _aprTotal += Convert.ToDouble(item_data.total);
                                    _DataDetail.Apr = _aprTotal.ToString("#,##0.00");
                                    break;
                                case "May":
                                    _mayTotal += Convert.ToDouble(item_data.total);
                                    _DataDetail.May = _mayTotal.ToString("#,##0.00");
                                    break;
                                case "Jun":
                                    _junTotal += Convert.ToDouble(item_data.total);
                                    _DataDetail.Jun = _junTotal.ToString("#,##0.00");
                                    break;
                                case "Jul":
                                    _julTotal += Convert.ToDouble(item_data.total);
                                    _DataDetail.Jul = _julTotal.ToString("#,##0.00");
                                    break;

                                case "Aug":
                                    _augTotal += Convert.ToDouble(item_data.total);
                                    _DataDetail.Aug = _augTotal.ToString("#,##0.00");
                                    break;
                                case "Sep":
                                    _sepTotal += Convert.ToDouble(item_data.total);
                                    _DataDetail.Sep = _sepTotal.ToString("#,##0.00");
                                    break;
                                case "Oct":
                                    _octTotal += Convert.ToDouble(item_data.total);
                                    _DataDetail.Oct = _octTotal.ToString("#,##0.00");
                                    break;
                                case "Nov":
                                    _novTotal += Convert.ToDouble(item_data.total);
                                    _DataDetail.Nov = _novTotal.ToString("#,##0.00");
                                    break;
                                case "Dec":
                                    _decTotal += Convert.ToDouble(item_data.total);
                                    _DataDetail.Dec = _decTotal.ToString("#,##0.00");
                                    break;
                            }
                            _DataDetail.TotalAmount = _LineTotal.ToString("#,##0.00");
                        }
                    }

                    if (_BudgetRevision.Count != 0)
                    {
                        // Switch Main Computation Variable To Refference BudgetRevision Data
                        var results = from p in _BudgetRevision
                                      group p by p.to_expense_item into g
                                      select new
                                      {
                                          Expense = g.Key
                                      };


                        foreach (var item in results)
                        {
                            List<BudgetRevisionMain> _found = _Main.Where(x => x.ExpenseItem == item.Expense.ToString()).ToList();

                            if (_found.Count == 0)
                            {
                                BudgetRevisionMain _AddedItems = new BudgetRevisionMain();
                                String _UACS = _BudgetRevision.Where(x => x.to_expense_item == item.Expense).ToList()[0].to_uacs;
                                _AddedItems.ExpenseItem = item.Expense.ToString();
                                _AddedItems.Status = "New";
                                _AddedItems.UACS = _UACS;
                                _Main.Add(_AddedItems);
                                continue;
                            }
                        }

                        foreach (var item in _Main)
                        {
                            List<BudgetRevisionItems> xdata_from = _BudgetRevision.Where(x => x.from_expense_item == item.ExpenseItem &&  x.is_approved =="false").ToList();
                            List<BudgetRevisionItems> xdata_to = _BudgetRevision.Where(x => x.to_expense_item == item.ExpenseItem && x.is_approved =="false").ToList();
                            List<OBR_Details> xdata_obr = ListGridDataDetails.Where(x => x.AccountCode == item.UACS).ToList();

                            double _totalOBR = 0.00;
                            double _totalMinus = 0.00;
                            double _totalPlus = 0.00;
                            Boolean _isApproved = false;

                            foreach (var itemobr in xdata_obr)
                            {
                                _totalOBR += Convert.ToDouble(itemobr.Total);

                            }

                            foreach (var itemfrom in xdata_from)
                            {
                                _totalMinus += Convert.ToDouble(itemfrom.total_alignment);
                                _isApproved = Convert.ToBoolean(itemfrom.is_approved);

                            }

                            foreach (var itemto in xdata_to)
                            {
                                _totalPlus += Convert.ToDouble(itemto.total_alignment);
                                _isApproved = Convert.ToBoolean(itemto.is_approved);

                            }
                            if (_isApproved)
                            {

                                isApproved = true;
                                hasUnApproved = false;
                                if (_totalMinus != 0)
                                {
                                    String _converted = _totalMinus.ToString();

                                    double _totalsMin = (Convert.ToDouble(item.Allocation) - (Convert.ToDouble(_converted)));
                                    double _totalsTot =  (Convert.ToDouble(item.Allocation) - _totalOBR);
                                    if (_totalsMin < 0)
                                    {
                                        _totalsMin = 0;
                                    }
                                    if (_totalsTot < 0)
                                    {
                                        _totalsTot = 0;
                                    }

                                    item.Allocation =_totalsMin.ToString("#,##0.00");

                                    item.TotalExpense = _totalsTot.ToString("#,##0.00");
                                }
                                else if (_totalPlus != 0)
                                {
                                    String _converted = _totalPlus.ToString();

                                    double _totalsMin = (Convert.ToDouble(item.Allocation) + (Convert.ToDouble(_converted)));
                                    double _totalsTot =  (Convert.ToDouble(item.Allocation) - _totalOBR);
                                    if (_totalsMin < 0)
                                    {
                                        _totalsMin = 0;
                                    }
                                    if (_totalsTot < 0)
                                    {
                                        _totalsTot = 0;
                                    }

                                    item.Allocation = _totalsMin.ToString("#,##0.00");

                                    item.TotalExpense = _totalsTot.ToString("#,##0.00");

                                }
                                item.Adjustment = "0.00";
                                item.TotalOBR = _totalOBR.ToString("#,##0.00");
                                item.RemainingBalance = (Convert.ToDouble(item.Allocation) - _totalOBR).ToString("#,##0.00");

                            }
                            else
                            {
                                isApproved = false;
                                hasUnApproved = true;
                                if (_totalMinus != 0)
                                {
                                    item.Adjustment = "(" + _totalMinus.ToString("#,##0.00") + ")";
                                }
                                else
                                {

                                    item.Adjustment = _totalPlus.ToString("#,##0.00");
                                }

                                if (item.Status == "New")
                                {
                                    item.TotalOBR = _totalOBR.ToString("#,##0.00");
                                    if (_totalPlus!=0)
                                    {
                                        item.RemainingBalance = ((Convert.ToDouble(item.Allocation) + _totalPlus) - _totalOBR).ToString("#,##0.00");
                                    }
                                    else
                                    {
                                        item.RemainingBalance = ((Convert.ToDouble(item.Allocation)) - _totalOBR).ToString("#,##0.00");
                                    }
                                   
                                }
                                else
                                {
                                    item.TotalOBR = _totalOBR.ToString("#,##0.00");
                                    if (_totalMinus != 0)
                                    {
                                        item.RemainingBalance = ((Convert.ToDouble(item.Allocation) - _totalMinus) - _totalOBR).ToString("#,##0.00");
                                    }
                                    else
                                    {
                                        if (_totalPlus!=0)
                                        {
                                             item.RemainingBalance = ((Convert.ToDouble(item.Allocation) + _totalPlus ) - _totalOBR).ToString("#,##0.00");
                                        }
                                        else
                                        {
                                            item.RemainingBalance = ((Convert.ToDouble(item.Allocation)) - _totalOBR).ToString("#,##0.00");
                                        }
                                       
                                    }

                                }

                                if (item.Adjustment.Contains("("))
                                {
                                    item.TotalExpense = ((Convert.ToDouble(item.Allocation) - _totalMinus)).ToString("#,##0.00");
                                }
                                else
                                {
                                    item.TotalExpense = ((Convert.ToDouble(item.Allocation) + Convert.ToDouble(item.Adjustment) )).ToString("#,##0.00");
                                    
                                }
                            }

                        }


                        this.Cursor = Cursors.Arrow;





                    }
                    else
                    {
                        foreach (var item in _Main)
                        {

                            List<OBR_Details> xdata_obr = ListGridDataDetails.Where(x => x.AccountCode == item.UACS).ToList();

                            double _totalOBR = 0.00;
                            double _totalMinus = 0.00;
                            double _totalPlus = 0.00;

                            foreach (var itemobr in xdata_obr)
                            {
                                _totalOBR += Convert.ToDouble(itemobr.Total);
                            }




                            item.TotalOBR = _totalOBR.ToString("#,##0.00");
                            item.RemainingBalance = (Convert.ToDouble(item.Allocation) - _totalOBR).ToString("#,##0.00");

                            item.TotalExpense = "0.00";

                        }



                        this.Cursor = Cursors.Arrow;
                    }
                    BudgetRevisionMain _totals_line = new BudgetRevisionMain();
                    _totals_line.ExpenseItem = "---------------------------------------------------";
                    _totals_line.Allocation = "-----------------";
                    _totals_line.TotalExpense = "-----------------";
                    _totals_line.Adjustment = "-----------------";
                    _totals_line.TotalOBR = "-----------------";
                    _totals_line.RemainingBalance = "-----------------";
                    _totals_line.Jan = "-----------------";
                    _totals_line.Feb = "-----------------";
                    _totals_line.Mar = "-----------------";
                    _totals_line.Apr = "-----------------";
                    _totals_line.May = "-----------------";
                    _totals_line.Jun = "-----------------";
                    _totals_line.Jul = "-----------------";
                    _totals_line.Aug = "-----------------";
                    _totals_line.Sep = "-----------------";
                    _totals_line.Oct = "-----------------";
                    _totals_line.Nov = "-----------------";
                    _totals_line.Dec = "-----------------";
                    _totals_line.TotalAmount = "-----------------";


                    BudgetRevisionMain _totals_ = new BudgetRevisionMain();
                    double _TotalAllocation = 0.00;
                    double _TotalExpense = 0.00;
                    double _TotalOBR = 0.00;
                    double _TotalBalance = 0.00;
                    double _TotalAmount = 0.00;

                    double _Jan = 0.00;
                    double _Feb = 0.00;
                    double _Mar = 0.00;
                    double _Apr = 0.00;
                    double _May = 0.00;
                    double _Jun = 0.00;
                    double _Jul = 0.00;
                    double _Aug = 0.00;
                    double _Sep = 0.00;
                    double _Oct = 0.00;
                    double _Nov = 0.00;
                    double _Dec = 0.00;

                    foreach (var item in _Main)
                    {
                        if (item.Allocation == null) { item.Allocation = "0.00"; }
                        if (item.TotalExpense == null) { item.TotalExpense = "0.00"; }
                        if (item.TotalOBR == null) { item.TotalOBR = "0.00"; }
                        if (item.RemainingBalance == null) { item.RemainingBalance = "0.00"; }
                        if (item.Adjustment == null) { item.Adjustment = "0.00"; }
                        if (item.Jan == null) { item.Jan = "0.00"; }
                        if (item.Feb == null) { item.Feb = "0.00"; }
                        if (item.Mar == null) { item.Mar = "0.00"; }
                        if (item.Apr == null) { item.Apr = "0.00"; }
                        if (item.May == null) { item.May = "0.00"; }
                        if (item.Jun == null) { item.Jun = "0.00"; }
                        if (item.Jul == null) { item.Jul = "0.00"; }
                        if (item.Aug == null) { item.Aug = "0.00"; }
                        if (item.Sep == null) { item.Sep = "0.00"; }
                        if (item.Oct == null) { item.Oct = "0.00"; }
                        if (item.Nov == null) { item.Nov = "0.00"; }
                        if (item.Dec == null) { item.Dec = "0.00"; }
                    }

                    foreach (var item in _Main)
                    {
                        _TotalAllocation += Convert.ToDouble(item.Allocation);
                        _TotalExpense += Convert.ToDouble(item.TotalExpense);
                        _TotalOBR += Convert.ToDouble(item.TotalOBR);
                        _TotalBalance += Convert.ToDouble(item.RemainingBalance);

                        String _val = "";
                        if (item.Jan.Contains("("))
                        {
                            _val = item.Jan;
                            _val = item.Jan.Replace("(", "");
                            _val = _val.Replace(")", "");
                            _Jan += Convert.ToDouble(_val);
                        }
                        else
                        {
                            _val = item.Jan;
                            _Jan += Convert.ToDouble(_val);
                        }
                        _TotalAmount += Convert.ToDouble(_val);
                        if (item.Feb.Contains("("))
                        {
                            _val = item.Feb;
                            _val = item.Feb.Replace("(", "");
                            _val = _val.Replace(")", "");
                            _Feb += Convert.ToDouble(_val);
                        }
                        else
                        {
                            _val = item.Feb;
                            _Feb += Convert.ToDouble(_val);
                        }
                        _TotalAmount += Convert.ToDouble(_val);
                        if (item.Mar.Contains("("))
                        {
                            _val = item.Mar;
                            _val = item.Mar.Replace("(", "");
                            _val = _val.Replace(")", "");
                            _Mar += Convert.ToDouble(_val);
                        }
                        else
                        {
                            _val = item.Mar;
                            _Mar += Convert.ToDouble(_val);
                        }
                        _TotalAmount += Convert.ToDouble(_val);
                        if (item.Apr.Contains("("))
                        {
                            _val = item.Apr;
                            _val = item.Apr.Replace("(", "");
                            _val = _val.Replace(")", "");
                            _Apr += Convert.ToDouble(_val);
                        }
                        else
                        {
                            _val = item.Apr;
                            _Apr += Convert.ToDouble(_val);
                        }
                        _TotalAmount += Convert.ToDouble(_val);
                        if (item.May.Contains("("))
                        {
                            _val = item.May;
                            _val = item.May.Replace("(", "");
                            _val = _val.Replace(")", "");
                            _May += Convert.ToDouble(_val);
                        }
                        else
                        {
                            _val = item.May;
                            _May += Convert.ToDouble(_val);
                        }
                        _TotalAmount += Convert.ToDouble(_val);
                        if (item.Jun.Contains("("))
                        {
                            _val = item.Jun;
                            _val = item.Jun.Replace("(", "");
                            _val = _val.Replace(")", "");
                            _Jun += Convert.ToDouble(_val);
                        }
                        else
                        {
                            _val = item.Jun;
                            _Jun += Convert.ToDouble(_val);
                        }
                        _TotalAmount += Convert.ToDouble(_val);
                        if (item.Jul.Contains("("))
                        {
                            _val = item.Jul;
                            _val = item.Jul.Replace("(", "");
                            _val = _val.Replace(")", "");
                            _Jul += Convert.ToDouble(_val);
                        }
                        else
                        {
                            _val = item.Jul;
                            _Jul += Convert.ToDouble(_val);
                        }
                        _TotalAmount += Convert.ToDouble(_val);
                        if (item.Aug.Contains("("))
                        {
                            _val = item.Aug;
                            _val = item.Aug.Replace("(", "");
                            _val = _val.Replace(")", "");
                            _Aug += Convert.ToDouble(_val);
                        }
                        else
                        {
                            _val = item.Aug;
                            _Aug += Convert.ToDouble(_val);
                        }
                        _TotalAmount += Convert.ToDouble(_val);
                        if (item.Sep.Contains("("))
                        {
                            _val = item.Sep;
                            _val = item.Sep.Replace("(", "");
                            _val = _val.Replace(")", "");
                            _Sep += Convert.ToDouble(_val);
                        }
                        else
                        {
                            _val = item.Sep;
                            _Sep += Convert.ToDouble(_val);
                        }
                        _TotalAmount += Convert.ToDouble(_val);
                        if (item.Oct.Contains("("))
                        {
                            _val = item.Oct;
                            _val = item.Oct.Replace("(", "");
                            _val = _val.Replace(")", "");
                            _Oct += Convert.ToDouble(_val);
                        }
                        else
                        {
                            _val = item.Oct;
                            _Oct += Convert.ToDouble(_val);
                        }
                        _TotalAmount += Convert.ToDouble(_val);
                        if (item.Nov.Contains("("))
                        {
                            _val = item.Nov;
                            _val = item.Nov.Replace("(", "");
                            _val = _val.Replace(")", "");
                            _Nov += Convert.ToDouble(_val);
                        }
                        else
                        {
                            _val = item.Nov;
                            _Nov += Convert.ToDouble(_val);
                        }
                        _TotalAmount += Convert.ToDouble(_val);
                        if (item.Dec.Contains("("))
                        {
                            _val = item.Dec;
                            _val = item.Dec.Replace("(", "");
                            _val = _val.Replace(")", "");
                            _Dec += Convert.ToDouble(_val);
                        }
                        else
                        {
                            _val = item.Dec;
                            _Dec += Convert.ToDouble(_val);
                        }
                        _TotalAmount += Convert.ToDouble(_val);
                        item.TotalAmount = _TotalAmount.ToString("#,##0.00");
                        _TotalAmount = 0;

                    }
                    double _PostiveVal = 0.00;
                    double _NegativeVal = 0.00;


                    foreach (var item in _Main)
                    {
                        String _val = "";
                        if (item.Adjustment.Contains("("))
                        {
                            _val = item.Adjustment;
                            _val = item.Adjustment.Replace("(", "");
                            _val = _val.Replace(")", "");
                            _NegativeVal += Convert.ToDouble(_val);
                        }
                        else
                        {
                            _val = item.Adjustment;
                            _PostiveVal += Convert.ToDouble(_val);
                        }
                    }
                    _totals_.ExpenseItem = "Total";
                    _totals_.Allocation = _TotalAllocation.ToString("#,##0.00");
                    _totals_.TotalExpense = _TotalExpense.ToString("#,##0.00");
                    _totals_.TotalOBR = _TotalOBR.ToString("#,##0.00");
                    _totals_.RemainingBalance = _TotalBalance.ToString("#,##0.00");
                    _totals_.Adjustment = (_PostiveVal - _NegativeVal).ToString("#,##0.00");
                    _totals_.Jan = _Jan.ToString("#,##0.00");
                    _totals_.Feb = _Feb.ToString("#,##0.00");
                    _totals_.Mar = _Mar.ToString("#,##0.00");
                    _totals_.Apr = _Apr.ToString("#,##0.00");
                    _totals_.May = _May.ToString("#,##0.00");
                    _totals_.Jun = _Jun.ToString("#,##0.00");
                    _totals_.Jul = _Jul.ToString("#,##0.00");
                    _totals_.Aug = _Aug.ToString("#,##0.00");
                    _totals_.Sep = _Sep.ToString("#,##0.00");
                    _totals_.Oct = _Oct.ToString("#,##0.00");
                    _totals_.Nov = _Nov.ToString("#,##0.00");
                    _totals_.Dec = _Dec.ToString("#,##0.00");
                    _totals_.TotalAmount = (_Jan + _Feb + _Mar + _Apr + _May + _Jun + _Jul + _Aug + _Sep + _Oct + _Nov + _Dec).ToString("#,##0.00");
                    _Main.Add(_totals_line);
                    _Main.Add(_totals_);

                    grdData.ItemsSource = null;
                    grdData.ItemsSource = _Main;
                    FixColumns();
                    FetchRequestRevisions();
                    if (_ForApprovalRAF.Count==0)
                    {
                        btnApprove.IsEnabled = false;
                    }
                    else
                    {
                        btnApprove.IsEnabled = true;
                    }
                    this.Cursor = Cursors.Arrow;

                    break;
            }
        }
        private void FetchBudgetAdjustments()
        {
            c_ppmp.Process = "FetchBudgetAdjustments";
            svc_service.ExecuteSQLAsync(c_ppmp.FetchBudgetAdjustments(this.DivId, this.SelectedYear, this.FundSource));
        }
        private void FetchBudgetRAFNo()
        {
            string f_type = "";
            List<App_FundSources> _data = ListFundSourceType.Where(x => x.Name == cmbFundSource.SelectedItem.ToString()).ToList();
            if (_data.Count!=0)
            {
                f_type = _data[0].Code;
            }
            c_ppmp.Process = "FetchBudgetRAFNo";
            svc_service.ExecuteSQLAsync(c_ppmp.FetchBudgetRafNo(this.DivId, this.SelectedYear, f_type));
        }
        private void FetchBudgetRAFNoForApproval()
        {
            c_ppmp.Process = "FetchBudgetRAFNoForApproval";
            svc_service.ExecuteSQLAsync(c_ppmp.FetchBudgetRafNoForApproval(this.DivId, this.SelectedYear));
        }
        private void FetchRequestRevisions()
        {
            c_ppmp.Process = "FetchRequestRevisions";
            svc_service.ExecuteSQLAsync(c_ppmp.FetchRequestRevisions(this.DivId));
        }
        private void FetchRequestRevision()
        {
            c_ppmp.Process = "FetchRequestRevision";
            svc_service.ExecuteSQLAsync(c_ppmp.FetchForRevision(this.DivId));
        }
        private void FetchApprovedRevisions()
        {
            c_ppmp.Process = "FetchApprovedRevisions";
            svc_service.ExecuteSQLAsync(c_ppmp.FetchApprovedRevisions(this.DivId));
        }
        private void FetchBudgetAllocationData()
        {
            string f_type = "";
            List<App_FundSources> _data = ListFundSourceType.Where(x => x.Name == cmbFundSource.SelectedItem.ToString()).ToList();

            if (_data.Count!=0)
            {
                f_type = _data[0].Code;
            }

            this.DivId = grdDivision.Rows[grdDivision.ActiveCell.Row.Index].Cells[0].Value.ToString();
            this.SelectedYear = this.WorkingYear;
            
            c_ppmp.Process = "FetchBudgetAllocationData";
            svc_service.ExecuteSQLAsync(c_ppmp.FetchBudgetAllocationRevApproval(this.DivId, this.SelectedYear, f_type));
        }

        private void FetchBudgetAllocationDataProject()
        {
            //this.DivId = grdSubUnit.Rows[grdSubUnit.ActiveCell.Row.Index].Cells[0].Value.ToString();
            //this.SelectedYear = "2017";
            //c_ppmp.Process = "FetchBudgetAllocationData";
            //svc_service.ExecuteSQLAsync(c_ppmp.FetchBudgetAllocation(this.DivId, this.SelectedYear));
            string f_type = "";
            List<App_FundSources> _data = ListFundSourceType.Where(x => x.Name == cmbFundSource.SelectedItem.ToString()).ToList();

            if (_data.Count != 0)
            {
                f_type = _data[0].Code;
            }

            this.DivId = grdSubUnit.Rows[grdSubUnit.ActiveCell.Row.Index].Cells[0].Value.ToString();
            this.SelectedYear = "2017";

            c_ppmp.Process = "FetchBudgetAllocationData";
            svc_service.ExecuteSQLAsync(c_ppmp.FetchBudgetAllocationRevApproval(this.DivId, this.SelectedYear, f_type));
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            String _results = e.Result.ToString();

            switch (c_finance.Process)
            {
                case "LoadPAP":
                    XDocument oDocKeyFetchListOffice = XDocument.Parse(_results);
                    var _dataListsFetchListOffice = from info in oDocKeyFetchListOffice.Descendants("Table")
                                                    select new OfficesPAP
                                                    {
                                                        DBM_Pap_Id = Convert.ToString(info.Element("DBM_Pap_Id").Value),
                                                        DBM_Sub_Pap_Code = Convert.ToString(info.Element("DBM_Sub_Pap_Code").Value),
                                                        DBM_Sub_Pap_Desc = Convert.ToString(info.Element("DBM_Sub_Pap_Desc").Value),
                                                        DBM_Sub_Pap_id = Convert.ToString(info.Element("DBM_Sub_Pap_id").Value),

                                                    };

                    ListOffice.Clear();
                    cmbPAP.Items.Clear();
                    foreach (var item in _dataListsFetchListOffice)
                    {
                        OfficesPAP _varDetails = new OfficesPAP();



                        _varDetails.DBM_Pap_Id = item.DBM_Pap_Id;
                        _varDetails.DBM_Sub_Pap_Desc = item.DBM_Sub_Pap_Code + " - " + item.DBM_Sub_Pap_Desc;
                        _varDetails.DBM_Sub_Pap_Code = item.DBM_Sub_Pap_Code;
                        _varDetails.DBM_Sub_Pap_id = item.DBM_Sub_Pap_id;

                        cmbPAP.Items.Add(item.DBM_Sub_Pap_Code + " - " + item.DBM_Sub_Pap_Desc);
                        ListOffice.Add(_varDetails);

                    }


                    break;
                case "LoadSubPAP":
                    XDocument oDocKeyFetchLoadSubPAP = XDocument.Parse(_results);
                    var _dataListsFetchLoadSubPAP = from info in oDocKeyFetchLoadSubPAP.Descendants("Table")
                                                    select new SubPAP_Division
                                                    {
                                                        DBM_Sub_Id = Convert.ToString(info.Element("DBM_Sub_Id").Value),
                                                        Description = Convert.ToString(info.Element("Description").Value),
                                                        PAP = Convert.ToString(info.Element("PAP").Value),
                                                        Sub_Id = Convert.ToString(info.Element("Sub_Id").Value),
                                                    };

                    ListSubPap.Clear();
                    cmbSubPap.Items.Clear();
                    foreach (var item in _dataListsFetchLoadSubPAP)
                    {
                        SubPAP_Division _varDetails = new SubPAP_Division();



                        _varDetails.DBM_Sub_Id = item.DBM_Sub_Id;
                        _varDetails.Description = item.Description;
                        _varDetails.PAP = item.PAP;
                        _varDetails.Sub_Id = item.Sub_Id;

                        cmbSubPap.Items.Add(item.PAP + " - " + item.Description);
                        ListSubPap.Add(_varDetails);

                    }


                    break;
                case "LoadFundSource":
                    XDocument oDocKeyFetchLoadFundSource = XDocument.Parse(_results);
                    var _dataListsFetchLoadFundSource = from info in oDocKeyFetchLoadFundSource.Descendants("Table")
                                                        select new App_FundSources
                                                        {
                                                            Code = Convert.ToString(info.Element("code").Value),
                                                            Id = Convert.ToString(info.Element("id").Value),
                                                            Name = Convert.ToString(info.Element("description").Value),
                                                        };

                    ListFundSourceType.Clear();
                    cmbFundSource.Items.Clear();
                    foreach (var item in _dataListsFetchLoadFundSource)
                    {
                        App_FundSources _varDetails = new App_FundSources();



                        _varDetails.Code = item.Code;
                        _varDetails.Id = item.Id;
                        _varDetails.Name = item.Name;


                        cmbFundSource.Items.Add(item.Name);
                        ListFundSourceType.Add(_varDetails);

                    }
                    //cmbExpenditure.Items.Clear();
                    //cmbExpenditure.Items.Add("PS");
                    //cmbExpenditure.Items.Add("MOOE");
                    //cmbExpenditure.Items.Add("CO");

                    break;
                case "LoadDivision":
                    XDocument oDocKeyFetchLoadDivision = XDocument.Parse(_results);
                    var _dataListsFetchLoadDivision = from info in oDocKeyFetchLoadDivision.Descendants("Table")
                                                      select new Divisions
                                                      {
                                                          DBM_Sub_Pap_Id = Convert.ToString(info.Element("DBM_Sub_Pap_Id").Value),
                                                          Division_Code = Convert.ToString(info.Element("Division_Code").Value),
                                                          Division_Desc = Convert.ToString(info.Element("Division_Desc").Value),
                                                          Division_Id = Convert.ToString(info.Element("Division_Id").Value)

                                                      };

                    ListDivision.Clear();
                    //   cmbDivision.Items.Clear();
                    foreach (var item in _dataListsFetchLoadDivision)
                    {
                        Divisions _varDetails = new Divisions();



                        _varDetails.DBM_Sub_Pap_Id = item.DBM_Sub_Pap_Id;
                        _varDetails.Division_Code = item.Division_Code;
                        _varDetails.Division_Desc = item.Division_Desc;
                        _varDetails.Division_Id = item.Division_Id;

                        //  cmbDivision.Items.Add(item.Division_Desc);
                        ListDivision.Add(_varDetails);

                    }
                    grdDivision.ItemsSource = null;
                    grdDivision.ItemsSource = ListDivision;
                    grdDivision.Columns["DBM_Sub_Pap_Id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdDivision.Columns["Division_Id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdDivision.Columns["Division_Code"].HeaderText = "PAP";
                    grdDivision.Columns["Division_Desc"].HeaderText = "Division";

                    //Column col_uacs = grdDivision.Columns.DataColumns["Division_Code"];
                    //col_uacs.Width = new ColumnWidth(100, false);


                    //  LoadModeExpenditure();

                    this.Cursor = Cursors.Arrow;
                    break;



                    break;
                case "LoadDivisionUnit":
                    XDocument oDocKeyFetchLoadDivisionUnit = XDocument.Parse(_results);
                    var _dataListsFetchLoadDivisionUnit = from info in oDocKeyFetchLoadDivisionUnit.Descendants("Table")
                                                          select new Divisions
                                                          {
                                                              DBM_Sub_Pap_Id = Convert.ToString(info.Element("DBM_Sub_Pap_Id").Value),
                                                              Division_Code = Convert.ToString(info.Element("Division_Code").Value),
                                                              Division_Desc = Convert.ToString(info.Element("Division_Desc").Value),
                                                              Division_Id = Convert.ToString(info.Element("Division_Id").Value)

                                                          };

                    ListDivisionUnit.Clear();
                    //   cmbDivision.Items.Clear();
                    foreach (var item in _dataListsFetchLoadDivisionUnit)
                    {
                        Divisions _varDetails = new Divisions();



                        _varDetails.DBM_Sub_Pap_Id = item.DBM_Sub_Pap_Id;
                        _varDetails.Division_Code = item.Division_Code;
                        _varDetails.Division_Desc = item.Division_Desc;
                        _varDetails.Division_Id = item.Division_Id;

                        //  cmbDivision.Items.Add(item.Division_Desc);
                        ListDivisionUnit.Add(_varDetails);

                    }
                    grdSubUnit.ItemsSource = null;
                    grdSubUnit.ItemsSource = ListDivisionUnit;
                    grdSubUnit.Columns["DBM_Sub_Pap_Id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdSubUnit.Columns["Division_Id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdSubUnit.Columns["Division_Code"].HeaderText = "PAP";
                    grdSubUnit.Columns["Division_Desc"].HeaderText = "Division";

                    //Column col_uacs_unit = grdSubUnit.Columns.DataColumns["Division_Code"];
                    //col_uacs_unit.Width = new ColumnWidth(100, false);

                    FetchBudgetAllocationData();
                    //  LoadModeExpenditure();
                   // FetchExpenditureDetailsItems();
                    this.Cursor = Cursors.Arrow;
                    break;



                    break;
                case "FetchOfficeDetails":
                    XDocument oDocFetchOfficeDetails = XDocument.Parse(_results);
                    var _dataFetchOfficeDetails = from info in oDocFetchOfficeDetails.Descendants("Table")
                                                  select new OfficeDetails
                                                  {
                                                      fund_source_id = Convert.ToString(info.Element("fund_source_id").Value),
                                                      budget_allocation = Convert.ToString(info.Element("budget_allocation").Value),
                                                      division_id = Convert.ToString(info.Element("div_id").Value),
                                                      Fund_Source = Convert.ToString(info.Element("Fund_Name").Value),
                                                      balance = "0.00",
                                                      total_program_budget = "0.00",
                                                      pap_code = ""
                                                  };



                    BalanceDetails.Clear();

                    foreach (var item in _dataFetchOfficeDetails)
                    {
                        OfficeDetails _varData = new OfficeDetails();

                        _varData.budget_allocation = item.budget_allocation;
                        _varData.division_id = item.division_id;
                        _varData.Fund_Source = item.Fund_Source;
                        _varData.total_program_budget = item.total_program_budget;
                        _varData.pap_code = item.pap_code;
                        _varData.fund_source_id = item.fund_source_id;
                        BalanceDetails.Add(_varData);

                    }
                    //  FetchOfficeTotals();
                    FetchBudgetAllocationList();
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchExpenditureDetails":
                    XDocument oDocFetchExpenditureDetails = XDocument.Parse(_results);
                    var _dataFetchExpenditureDetails = from info in oDocFetchExpenditureDetails.Descendants("Table")
                                                       select new ChiefData_ListExpenditure
                                                       {
                                                           expenditure = Convert.ToString(info.Element("expenditure").Value),
                                                           fund_source_id = Convert.ToString(info.Element("fund_source_id").Value),
                                                           total = Convert.ToString(info.Element("total").Value)

                                                       };



                    cd_ExpenditureLists.Clear();

                    foreach (var item in _dataFetchExpenditureDetails)
                    {
                        ChiefData_ListExpenditure _varData = new ChiefData_ListExpenditure();

                        _varData.expenditure = item.expenditure;
                        _varData.fund_source_id = item.fund_source_id;
                        _varData.total = item.total;

                        cd_ExpenditureLists.Add(_varData);

                    }
                    FetchApprovedFinance();

                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchApprovedFinance":
                    XDocument oDocFetchApprovedFinance = XDocument.Parse(_results);
                    var _dataFetchApprovedFinance = from info in oDocFetchApprovedFinance.Descendants("Table")
                                                    select new Approved_Data
                                                    {
                                                        Code = Convert.ToString(info.Element("code").Value)

                                                    };



                    cd_Approved.Clear();

                    foreach (var item in _dataFetchApprovedFinance)
                    {
                        Approved_Data _varData = new Approved_Data();

                        _varData.Code = item.Code;


                        cd_Approved.Add(_varData);

                    }
                    FetchFundSourceMainDetails(isUnit);
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchFundMain":
                    XDocument oDocFetchFundMain = XDocument.Parse(_results);
                    var _dataFetchFundMain = from info in oDocFetchFundMain.Descendants("Table")
                                             select new ChiefData_FundSourceMain
                                             {
                                                 id = Convert.ToString(info.Element("id").Value),
                                                 fund_source = Convert.ToString(info.Element("fund_name").Value)
                                             };



                    cd_FundSourceMain.Clear();

                    foreach (var item in _dataFetchFundMain)
                    {
                        ChiefData_FundSourceMain _varData = new ChiefData_FundSourceMain();

                        _varData.id = item.id;
                        _varData.fund_source = item.fund_source;

                        cd_FundSourceMain.Add(_varData);
                    }
                    FetchExpenditureDetails(isUnit);
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchExpenditureDetailsItemLists":
                    XDocument oDocFetchFetchExpenditureDetailsItemLists = XDocument.Parse(_results);
                    var _dataFetchFetchExpenditureDetailsItemLists = from info in oDocFetchFetchExpenditureDetailsItemLists.Descendants("Table")
                                                                     select new ChiefData_ItemLists
                                                                     {
                                                                         id = Convert.ToString(info.Element("id").Value),
                                                                         expenditure = Convert.ToString(info.Element("expenditure").Value),
                                                                         months = Convert.ToString(info.Element("month").Value),
                                                                         total = Convert.ToDouble(info.Element("total").Value)
                                                                     };



                    ListItemDetails.Clear();

                    foreach (var item in _dataFetchFetchExpenditureDetailsItemLists)
                    {
                        ChiefData_ItemLists _varData = new ChiefData_ItemLists();

                        _varData.id = item.id;
                        _varData.expenditure = item.expenditure;
                        _varData.months = item.months;
                        _varData.total = item.total;

                        ListItemDetails.Add(_varData);
                    }
                    FetchFundSourceMain(isUnit);
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchOBRData":
                    XDocument oDocKeyFetchGridData = XDocument.Parse(_results);
                    var _dataListsFetchGridData = from info in oDocKeyFetchGridData.Descendants("Table")
                                                  select new OBR_Details
                                                  {
                                                      _Month = Convert.ToString(info.Element("_Month").Value),
                                                      AccountCode = Convert.ToString(info.Element("AccountCode").Value),
                                                      DateCreated = Convert.ToString(info.Element("DateCreated").Value),
                                                      Division = Convert.ToString(info.Element("Division").Value),
                                                      Total = Convert.ToString(info.Element("Total").Value)

                                                  };

                    ListGridDataDetails.Clear();

                    foreach (var item in _dataListsFetchGridData)
                    {
                        OBR_Details _varDetails = new OBR_Details();

                        switch (item._Month)
                        {
                            case "1": _varDetails._Month = "Jan"; break;
                            case "2": _varDetails._Month = "Feb"; break;
                            case "3": _varDetails._Month = "Mar"; break;
                            case "4": _varDetails._Month = "Apr"; break;
                            case "5": _varDetails._Month = "May"; break;
                            case "6": _varDetails._Month = "Jun"; break;
                            case "7": _varDetails._Month = "Jul"; break;
                            case "8": _varDetails._Month = "Aug"; break;
                            case "9": _varDetails._Month = "Sep"; break;
                            case "10": _varDetails._Month = "Oct"; break;
                            case "11": _varDetails._Month = "Nov"; break;
                            case "12": _varDetails._Month = "Dec"; break;
                        }

                        _varDetails.AccountCode = item.AccountCode;
                        _varDetails.DateCreated = item.DateCreated;
                        _varDetails.Division = item.Division;
                        _varDetails.Total = item.Total;

                        ListGridDataDetails.Add(_varDetails);

                    }
                    List<String> _months = new List<string>();

                    for (int i = 1; i < DateTime.Now.Month; i++)
                    {
                        switch (i.ToString())
                        {
                            case "1":
                                _months.Add("Jan");
                                break;
                            case "2":
                                _months.Add("Feb");
                                break;
                            case "3":
                                _months.Add("Mar");
                                break;
                            case "4":
                                _months.Add("Apr");
                                break;
                            case "5":
                                _months.Add("May");
                                break;
                            case "6":
                                _months.Add("Jun");
                                break;
                            case "7":
                                _months.Add("Jul");
                                break;
                            case "8":
                                _months.Add("Aug");
                                break;
                            case "9":
                                _months.Add("Sep");
                                break;
                            case "10":
                                _months.Add("Oct");
                                break;
                            case "11":
                                _months.Add("Nov");
                                break;
                            case "12":
                                _months.Add("Dec");
                                break;
                        }
                    }

                    foreach (var item in _Main)
                    {
                        double _OBR = 0.00;

                        foreach (string item_months in _months)
                        {
                            double _Total = 0.00;
                            List<OBR_Details> _data = ListGridDataDetails.Where(x => x.AccountCode == item.UACS && x._Month == item_months).ToList();

                            foreach (OBR_Details item_OBR in _data)
                            {
                                _Total += Convert.ToDouble(item_OBR.Total);

                                _OBR += Convert.ToDouble(item_OBR.Total);
                            }
                            switch (item_months)
                            {
                                case "Jan":
                                    if (_Total == 0)
                                    {
                                        item.Jan = "( " + Convert.ToDouble(item.Jan).ToString("#,##0.00") + " )";
                                    }
                                    else
                                    {
                                        item.Jan = _Total.ToString("#,##0.00");
                                    }
                                    break;
                                case "Feb":
                                    if (_Total == 0)
                                    {
                                        item.Feb = "( " + Convert.ToDouble(item.Feb).ToString("#,##0.00") + " )";
                                    }
                                    else
                                    {
                                        item.Feb = _Total.ToString("#,##0.00");
                                    }
                                    break;
                                case "Mar":
                                    if (_Total == 0)
                                    {
                                        item.Mar = "( " + Convert.ToDouble(item.Mar).ToString("#,##0.00") + " )";
                                    }
                                    else
                                    {
                                        item.Mar = _Total.ToString("#,##0.00");
                                    }
                                    break;
                                case "Apr":
                                    if (_Total == 0)
                                    {
                                        item.Apr = "( " + Convert.ToDouble(item.Apr).ToString("#,##0.00") + " )";
                                    }
                                    else
                                    {
                                        item.Apr = _Total.ToString("#,##0.00");
                                    }
                                    break;
                                case "May":
                                    if (_Total == 0)
                                    {
                                        item.May = "( " + Convert.ToDouble(item.May).ToString("#,##0.00") + " )";
                                    }
                                    else
                                    {
                                        item.May = _Total.ToString("#,##0.00");
                                    }
                                    break;
                                case "Jun":
                                    if (_Total == 0)
                                    {
                                        item.Jun = "( " + Convert.ToDouble(item.Jun).ToString("#,##0.00") + " )";
                                    }
                                    else
                                    {
                                        item.Jun = _Total.ToString("#,##0.00");
                                    }
                                    break;
                                case "Jul":
                                    if (_Total == 0)
                                    {
                                        item.Jul = "( " + Convert.ToDouble(item.Jul).ToString("#,##0.00") + " )";
                                    }
                                    else
                                    {
                                        item.Jul = _Total.ToString("#,##0.00");
                                    }
                                    break;

                                case "Aug":
                                    if (_Total == 0)
                                    {
                                        item.Aug = "( " + Convert.ToDouble(item.Aug).ToString("#,##0.00") + " )";
                                    }
                                    else
                                    {
                                        item.Aug = _Total.ToString("#,##0.00");
                                    }
                                    break;
                                case "Sep":
                                    if (_Total == 0)
                                    {
                                        item.Sep = "( " + Convert.ToDouble(item.Sep).ToString("#,##0.00") + " )";
                                    }
                                    else
                                    {
                                        item.Sep = _Total.ToString("#,##0.00");
                                    }
                                    break;
                                case "Oct":
                                    if (_Total == 0)
                                    {
                                        item.Oct = "( " + Convert.ToDouble(item.Oct).ToString("#,##0.00") + " )";
                                    }
                                    else
                                    {
                                        item.Oct = _Total.ToString("#,##0.00");
                                    }
                                    break;
                                case "Nov":
                                    if (_Total == 0)
                                    {
                                        item.Nov = "( " + Convert.ToDouble(item.Nov).ToString("#,##0.00") + " )";
                                    }
                                    else
                                    {
                                        item.Nov = _Total.ToString("#,##0.00");
                                    }
                                    break;
                                case "Dec":
                                    if (_Total == 0)
                                    {
                                        item.Dec = "( " + Convert.ToDouble(item.Dec).ToString("#,##0.00") + " )";
                                    }
                                    else
                                    {
                                        item.Dec = _Total.ToString("#,##0.00");
                                    }
                                    break;
                            }


                        }
                        item.TotalOBR = _OBR.ToString("#,##0.00");
                        item.RemainingBalance = (Convert.ToDouble(item.Allocation) - _OBR).ToString("#,##0.00");
                    }



                    ComputeItem();

                    grdData.ItemsSource = null;
                    grdData.ItemsSource = _Main;
                    FixColumns();
                    break;


            }
        }

        private void ComputeItem()
        {
            foreach (var item in _Main)
            {
                if (item.Adjustment == null || item.Adjustment == "")
                {
                    item.Adjustment = "0.00";
                }
                if (item.Allocation == null || item.Allocation == "")
                {
                    item.Allocation = "0.00";
                }
                if (item.RemainingBalance == null || item.RemainingBalance == "")
                {
                    item.RemainingBalance = "0.00";
                }
                if (item.TotalAmount == null || item.TotalAmount == "")
                {
                    item.TotalAmount = "0.00";
                }
                if (item.TotalOBR == null || item.TotalOBR == "")
                {
                    item.TotalOBR = "0.00";
                }
                if (item.TotalExpense == null || item.TotalExpense == "")
                {
                    item.TotalExpense = "0.00";
                }
            }
            foreach (var item in _Main)
            {

                if (item.Adjustment == null)
                {
                    item.TotalExpense = Convert.ToDouble(item.RemainingBalance).ToString("#,##0.00");
                }
                else
                {
                    if (item.Adjustment.Contains("("))
                    {
                        String item_adjust = item.Adjustment;
                        item_adjust = item_adjust.Replace("(", "");
                        item_adjust = item_adjust.Replace(")", "");

                        item.TotalExpense = (Convert.ToDouble(item.RemainingBalance) - Convert.ToDouble(item_adjust)).ToString("#,##0.00");
                    }
                    else
                    {
                        if (item.Allocation == null)
                        {
                            item.TotalExpense = Convert.ToDouble(item.Adjustment).ToString("#,##0.00");
                        }
                        else
                        {
                            item.TotalExpense = (Convert.ToDouble(item.RemainingBalance) + Convert.ToDouble(item.Adjustment)).ToString("#,##0.00");
                        }

                    }
                }

            }
            if (HasAdjustments)
            {
                foreach (var item in _Main)
                {
                    if (item.Status == "Old")
                    {
                        item.Allocation = item.TotalExpense;
                        item.TotalExpense = "0.00";
                        item.Adjustment = "0.00";
                    }
                }
            }


        }
        private clsppmp_realign c_ppmp = new clsppmp_realign();
        public String DivId { get; set; }
        //public String SelectedYear { get; set; }
        public String FundSource { get; set; }
        public String FundName { get; set; }
        public String FundType { get; set; }
        public String PAPCODE { get; set; }

        private void FetchBudgetAllocationList()
        {
            c_ppmp.Process = "FetchBudgetAllocation";
            svc_mindaf.ExecuteSQLAsync(c_ppmp.FetchBudgetAllocation(this.DivId, this.SelectedYear));
        }


        private void FetchApprovedFinance()
        {
            c_finance.Process = "FetchApprovedFinance";
            svc_mindaf.ExecuteSQLAsync(c_finance.FetchApprovedFinance());
        }
        private void FetchFundSourceMainDetails(Boolean isUnit)
        {
            //String _desc = grdDivision.Rows[grdDivision.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
            //List<Divisions> x_data = ListDivision.Where(x => x.Division_Desc == _desc).ToList();
            String _desc = "";
            List<Divisions> x_data = new List<Divisions>();
            if (isUnit == false)
            {
                _desc = grdDivision.Rows[grdDivision.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
                x_data = ListDivision.Where(x => x.Division_Desc == _desc).ToList();
            }
            else
            {
                _desc = grdSubUnit.Rows[grdSubUnit.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
                x_data = ListDivisionUnit.Where(x => x.Division_Desc == _desc).ToList();
            }
            String OfficeId = "";

            if (x_data.Count != 0)
            {
                OfficeId = x_data[0].Division_Id;
                c_finance.Process = "FetchFundSourceMainDetails";
                svc_mindaf.ExecuteSQLAsync(c_finance.FetchFundSourceDetails(OfficeId, this.WorkingYear, cmbFundSource.SelectedItem.ToString(), "MOOE"));
            }


        }
        private void FetchExpenditureDetails(Boolean isUnit)
        {
            //String _desc = grdDivision.Rows[grdDivision.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
            //List<Divisions> x_data = ListDivision.Where(x => x.Division_Desc == _desc).ToList();

            String _desc = "";
            List<Divisions> x_data = new List<Divisions>();
            if (isUnit == false)
            {
                _desc = grdDivision.Rows[grdDivision.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
                x_data = ListDivision.Where(x => x.Division_Desc == _desc).ToList();
            }
            else
            {
                _desc = grdSubUnit.Rows[grdSubUnit.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
                x_data = ListDivisionUnit.Where(x => x.Division_Desc == _desc).ToList();
            }

            String OfficeId = "";

            if (x_data.Count != 0)
            {
                OfficeId = x_data[0].Division_Id;
                c_finance.Process = "FetchExpenditureDetails";
                svc_mindaf.ExecuteSQLAsync(c_finance.RevFetchDivisionExpenditureDetailsByDiv(OfficeId, this.WorkingYear));
            }

        }
        private void FetchExpenditureDetailsUnit(Boolean isUnit)
        {
            //String _desc = grdDivision.Rows[grdDivision.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
            //List<Divisions> x_data = ListDivision.Where(x => x.Division_Desc == _desc).ToList();

            String _desc = "";
            List<Divisions> x_data = new List<Divisions>();
            if (isUnit == false)
            {
                _desc = grdDivision.Rows[grdDivision.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
                x_data = ListDivision.Where(x => x.Division_Desc == _desc).ToList();
            }
            else
            {
                _desc = grdSubUnit.Rows[grdSubUnit.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
                x_data = ListDivisionUnit.Where(x => x.Division_Desc == _desc).ToList();
            }

            String OfficeId = "";

            if (x_data.Count != 0)
            {
                OfficeId = x_data[0].Division_Id;
                c_ppmp.Process = "FetchExpenditureDetails";
                svc_service.ExecuteSQLAsync(c_finance.RevFetchDivisionExpenditureDetailsByDiv(OfficeId, this.WorkingYear));
            }

        }
        private bool is_loaded;
        private void SubmitSelected()
        {
            is_loaded = false;
            StringBuilder sb = new StringBuilder(260);
            String SQLCommand = "";
            foreach (ChiefData_ItemLists item in ListItemDetails)
            {
                sb.AppendLine(@"UPDATE dbo.mnda_approved_projects_division SET  isapproved = 1 ,user_approve ='" + this._User.Replace("Welcome", "").Trim() + "',date_status ='" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "' WHERE  id = " + item.id + ";");
            }
            sb.Append(@"UPDATE dbo.mnda_activity_data SET status = 'FINANCE APPROVED' WHERE fund_source_id ='" + FundSource + "';");

            sb.AppendLine(@"INSERT INTO");
            sb.AppendLine(@"  dbo.mnda_approved_finance");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  code,");
            sb.AppendLine(@"  status");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '" + FundSource + "',");
            sb.AppendLine(@"  'APPROVED'");
            sb.AppendLine(@");");

            SQLCommand = sb.ToString();

            //MinDAFSVCClient _service = new MinDAFSVCClient();
            //_service.ExecuteQueryCompleted += new EventHandler<ExecuteQueryCompletedEventArgs>(_service_ExecuteQueryCompleted);
            //_service.ExecuteQueryAsync(SQLCommand);

            c_finance.Process = "ApproveFinance";
            is_loaded = false;
            svc_mindaf.ExecuteQueryListsCompleted += svc_mindaf_ExecuteQueryListsCompleted;
            svc_mindaf.ExecuteQueryListsAsync(SQLCommand);
        }

        private void SubmitDisapprove()
        {
            is_loaded = false;
            StringBuilder sb = new StringBuilder(260);
            String SQLCommand = "";
            foreach (ChiefData_ItemLists item in ListItemDetails)
            {
                sb.AppendLine(@"UPDATE dbo.mnda_approved_projects_division SET  isapproved = 0 ,user_approve ='" + this._User.Replace("Welcome", "").Trim() + "',date_status ='" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "' WHERE  id = " + item.id + ";");
            }


            SQLCommand = sb.ToString();

            //MinDAFSVCClient _service = new MinDAFSVCClient();
            //_service.ExecuteQueryCompleted += new EventHandler<ExecuteQueryCompletedEventArgs>(_service_ExecuteQueryCompleted);
            //_service.ExecuteQueryAsync(SQLCommand);

            c_finance.Process = "DisapproveFinance";
            is_loaded = false;
            svc_mindaf.ExecuteQueryListsCompleted += svc_mindaf_ExecuteQueryListsCompleted;
            svc_mindaf.ExecuteQueryListsAsync(SQLCommand);
        }

        void _service_ExecuteQueryCompleted(object sender, ExecuteQueryCompletedEventArgs e)
        {
            //bool bb = Convert.ToBoolean(e.Result);
            //if (bb == true)
            //    this.Content = new MainPage();
        }

        void svc_mindaf_ExecuteQueryListsCompleted(object sender, ExecuteQueryListsCompletedEventArgs e)
        {
            if (c_finance.Process == "ApproveFinance")
            {
                if (is_loaded == false)
                {
                    if (e.Result == true)
                    {
                        MessageBox.Show("Budget Plan has been approved succesfully");
                        is_loaded = true;
                    }
                }

            }
            else if (c_finance.Process == "DisapproveFinance")
            {
                if (is_loaded == false)
                {
                    if (e.Result == true)
                    {
                        MessageBox.Show("Budget Plan has been disapproved");
                        is_loaded = true;
                    }
                }
            }
        }
        private void ApproveExpenditure()
        {
            //c_finance.Process = "ApproveFinance";
            //c_finance.ApproveFinance(ListItemDetails);
            SubmitSelected();
        }
        private void SetAlignment()
        {
            if (grdData.Rows.Count != 0)
            {
                grdData.Rows[0].ChildBands[0].Cells["Jan"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["Feb"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["Mar"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["Apr"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["May"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["Jun"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["Jul"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["Aug"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["Sep"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["Oct"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["Nov"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["Dec"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["allocation"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["total_expenditures"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["total_balance"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["total_budget"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            }

        }


        private void FetchExpenditureDetailsItems()
        {
            String _desc = grdDivision.Rows[grdDivision.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
            List<Divisions> x_data = ListDivision.Where(x => x.Division_Desc == _desc).ToList();
            String OfficeId = "";

            if (x_data.Count != 0)
            {
                OfficeId = x_data[0].Division_Id;
                c_finance.Process = "FetchExpenditureDetailsItemLists";
                svc_mindaf.ExecuteSQLAsync(c_finance.FetchDivisionExpenditureDetailsItemByDiv(OfficeId, this.WorkingYear, cmbFundSource.SelectedItem.ToString(), "MOOE"));
            }

        }

        private void FetchExpenditureDetailsItemsUnit()
        {
            String _desc = grdSubUnit.Rows[grdSubUnit.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
            List<Divisions> x_data = ListDivisionUnit.Where(x => x.Division_Desc == _desc).ToList();
            String OfficeId = "";

            if (x_data.Count != 0)
            {
                OfficeId = x_data[0].Division_Id;
                c_finance.Process = "FetchExpenditureDetailsItemLists";
                svc_mindaf.ExecuteSQLAsync(c_finance.FetchDivisionExpenditureDetailsItemByDiv(OfficeId, this.WorkingYear, cmbFundSource.SelectedItem.ToString(), "MOOE"));
            }

        }
        private void FetchFundSourceMain(Boolean isUnit)
        {
            String _desc = "";
            List<Divisions> x_data = new List<Divisions>();
            if (isUnit == false)
            {
                _desc = grdDivision.Rows[grdDivision.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
                x_data = ListDivision.Where(x => x.Division_Desc == _desc).ToList();
            }
            else
            {
                _desc = grdSubUnit.Rows[grdSubUnit.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
                x_data = ListDivisionUnit.Where(x => x.Division_Desc == _desc).ToList();
            }
            String OfficeId = "";
            if (x_data.Count != 0)
            {
                OfficeId = x_data[0].Division_Id;
                c_finance.Process = "FetchFundMain";
                svc_mindaf.ExecuteSQLAsync(c_finance.FetchFundSource(OfficeId, this.WorkingYear, cmbFundSource.SelectedItem.ToString(), "MOOE"));
            }


        }

        private void FetchOfficeDetails()
        {
            String _desc = grdDivision.Rows[grdDivision.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
            List<Divisions> x_data = ListDivision.Where(x => x.Division_Desc == _desc).ToList();
            String OfficeId = "";

            if (x_data.Count != 0)
            {
                OfficeId = x_data[0].Division_Id;
                c_finance.Process = "FetchOfficeDetails";
                svc_mindaf.ExecuteSQLAsync(c_finance.FetchOfficeDetails(OfficeId, this.WorkingYear));
            }

        }
        private void FetchBudgetAllocation()
        {
            String _desc = grdDivision.Rows[grdDivision.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
            List<Divisions> x_data = ListDivision.Where(x => x.Division_Desc == _desc).ToList();
            String OfficeId = "";

            if (x_data.Count != 0)
            {
                OfficeId = x_data[0].Division_Id;
                SelectedYear = this.WorkingYear;
                c_finance.Process = "FetchBudgetAllocation";
                svc_mindaf.ExecuteSQLAsync(c_finance.FetchBudgetAllocation(OfficeId, SelectedYear));
            }


        }
        private void FetchDivisionFundSource()
        {
            String _desc = grdDivision.Rows[grdDivision.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
            List<Divisions> x_data = ListDivision.Where(x => x.Division_Desc == _desc).ToList();
            String OfficeId = "";

            if (x_data.Count != 0)
            {
                OfficeId = x_data[0].Division_Id;
                c_finance.Process = "FetchDivisionFundSource";
                svc_mindaf.ExecuteSQLAsync(c_finance.FetchDivisionFundSource(OfficeId, this.WorkingYear));
            }


        }
        private void FetchData()
        {
            c_finance.Process = "FetchData";
            svc_mindaf.ExecuteSQLAsync(c_finance.LoadPAP(this.WorkingYear));
        }
        private void LoadPAP()
        {
            c_finance.Process = "LoadPAP";
            svc_mindaf.ExecuteSQLAsync(c_finance.LoadPAP(this.WorkingYear));
        }
        private void LoadSubPAP()
        {
            c_finance.Process = "LoadSubPAP";
            List<OfficesPAP> x_search = ListOffice.Where(x => x.DBM_Sub_Pap_Desc == cmbPAP.SelectedItem.ToString()).ToList();
            String _id = "";
            if (x_search.Count != 0)
            {
                _id = x_search[0].DBM_Sub_Pap_id;
            }
            svc_mindaf.ExecuteSQLAsync(c_finance.LoadSubPAP(_id));
        }
        private void LoadDivision()
        {
            List<SubPAP_Division> data = ListSubPap.Where(x => (x.PAP + " - " + x.Description).ToString() == cmbSubPap.SelectedItem.ToString()).ToList();
            String Code = "";
            if (data.Count != 0)
            {
                Code = data[0].PAP;
            }
            c_finance.Process = "LoadDivision";
            svc_mindaf.ExecuteSQLAsync(c_finance.LoadDivision(Code,this.WorkingYear));

        }
        private void LoadFundSource()
        {

            c_finance.Process = "LoadFundSource";
            svc_mindaf.ExecuteSQLAsync(c_finance.LoadFundSource());

        }
        private void LoadDivisionUnit()
        {
            String _desc = grdDivision.Rows[grdDivision.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
            List<Divisions> data = ListDivision.Where(x => x.Division_Desc.ToString() == _desc).ToList();
            String Code = "";
            if (data.Count != 0)
            {
                Code = data[0].Division_Id;
            }
            c_finance.Process = "LoadDivisionUnit";
            svc_mindaf.ExecuteSQLAsync(c_finance.LoadDivisionUnit(Code,this.WorkingYear));



        }


        private void LoadModeExpenditure()
        {
            //cmbExpenditure.Items.Clear();

            //cmbExpenditure.Items.Add("MOOE");
            //cmbExpenditure.Items.Add("CO");
            //cmbExpenditure.Items.Add("PS");
        }
        private void GenerateYear()
        {
           
            //int _year = DateTime.Now.Year;
            //int _limit = _year + 100;

            //for (int i = _year; i != _limit; i++)
            //{
            //    cmbYear.Items.Add(_year);
            //    _year += 1; 
            //}

            //cmbYear.SelectedIndex = 0;
            lblyear.Content = this.WorkingYear;
            LoadPAP();

        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            ApproveExpenditure();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            SubmitDisapprove();
        }

        private void frmFinApp_Loaded(object sender, RoutedEventArgs e)
        {
            GenerateYear();
        }

        private void cmbPAP_DropDownClosed(object sender, EventArgs e)
        {
            grdDivision.ItemsSource = null;
            grdSubUnit.ItemsSource = null;
            grdData.ItemsSource = null;
            //   cmbExpenditure.SelectedIndex = -1;
            // cmbDivision.SelectedIndex = -1;
            LoadSubPAP();
            //LoadDivision();
        }

        private void cmbExpenditure_DropDownClosed(object sender, EventArgs e)
        {
            LoadDivision();


        }

        public string SelectedYear { get; set; }

        public string _User { get; set; }


        private void cmbSubPap_DropDownClosed(object sender, EventArgs e)
        {
            grdDivision.ItemsSource = null;
            grdSubUnit.ItemsSource = null;
            grdData.ItemsSource = null;
            stkApproved.Visibility = System.Windows.Visibility.Collapsed;
            LoadFundSource();
        }

        private void cmbDivision_DropDownClosed(object sender, EventArgs e)
        {

        }


        private void cmbFundSource_DropDownClosed(object sender, EventArgs e)
        {
            LoadDivision();
        }

        private void grdSubUnit_CellClicked(object sender, CellClickedEventArgs e)
        {
            isUnit = true;
            FetchExpenditureDetailsUnit(isUnit);

        }

        private void grdDivision_CellClicked(object sender, CellClickedEventArgs e)
        {
           
            LoadDivisionUnit();
            isUnit = false;
        }

        private void frmrevapp_Loaded(object sender, RoutedEventArgs e)
        {
            GenerateYear();
        }
        private void RemoveAlignments()
        {

            c_finance.Process = "RemoveAlignments";
            c_finance.RemoveAlignment(this.DivId, this.SelectedYear, this.FundSource);
            c_finance.SQLOperation += c_finance_SQLOperation;

        }
        private void btnApprove_Click(object sender, RoutedEventArgs e)
        {
            switch (btnApprove.Content.ToString())
            {
                case "Unlock":
                    RemoveAlignments();
                    break;
                case "Approve":
                    MessageBoxResult result = MessageBox.Show("Confirm Approve Revision Data",
                         "Confirmation", MessageBoxButton.OKCancel);

                    if (result == MessageBoxResult.OK)
                    {
                        c_finance.Process = "ApproveRevision";
                        c_finance.SQLOperation += c_finance_SQLOperation;
                        List<BudgetRevisionItems> _data = new List<BudgetRevisionItems>();
                        List<BudgetRevisionItems> _dataTo = new List<BudgetRevisionItems>();
                        List<BudgetRevisionItems> _New = new List<BudgetRevisionItems>();

                        foreach (var item in _BudgetItems)
                        {
                            List<BudgetRevisionItems> _totals = _BudgetRevision.Where(x => x.from_expense_item == item.ExpenseItem).ToList();
                            double _totalrevision = 0.00;
                            string _fromuacs = "";
                            if (_totals.Count != 0)
                            {

                                foreach (var itemtots in _totals)
                                {
                                    _totalrevision += Convert.ToDouble(itemtots.total_alignment);
                                    _fromuacs = itemtots.from_uacs;
                                }
                                BudgetRevisionItems _new = new BudgetRevisionItems();
                                _new.from_expense_item = item.ExpenseItem;
                                _new.from_uacs = _fromuacs;
                                _new.total_alignment = _totalrevision.ToString();
                                _data.Add(_new);
                            }
                        }
                        foreach (var item in _BudgetItems)
                        {
                            List<BudgetRevisionItems> _totals = _BudgetRevision.Where(x => x.to_expense_item == item.ExpenseItem).ToList();
                            double _totalrevision = 0.00;
                            string _touacs = "";
                            if (_totals.Count != 0)
                            {
                                foreach (var itemtots in _totals)
                                {
                                    _totalrevision += Convert.ToDouble(itemtots.total_alignment);
                                    _touacs = itemtots.to_uacs;
                                }

                                BudgetRevisionItems _new = new BudgetRevisionItems();
                                _new.to_expense_item = item.ExpenseItem;
                                _new.to_uacs = _touacs;
                                _new.total_alignment = _totalrevision.ToString();
                                _dataTo.Add(_new);
                            }
                        }
                        var results = from p in _BudgetRevision
                                      group p by p.to_expense_item into g
                                      select new
                                      {
                                          Expense = g.Key
                                      };
                        List<String> _ExpenseFiltered  = new List<string>();
                        foreach (var item in results.ToList())
	                    {
                            _ExpenseFiltered.Add(item.Expense.ToString());
	                    }
                        foreach (String item in _ExpenseFiltered)
                        {
                            List<BudgetRevision> _new = _BudgetItems.Where(x => x.ExpenseItem == item).ToList();
                            if (_new.Count == 0)
                            {
                                List<BudgetRevisionItems> _dataItem = _BudgetRevision.Where(x => x.to_expense_item == item).ToList();
                                BudgetRevisionItems _newItem = new BudgetRevisionItems();
                                _newItem.to_expense_item = _dataItem[0].to_expense_item;
                                _newItem.to_uacs = _dataItem[0].to_uacs ;
                                double _total = 0.00;
                                foreach (BudgetRevisionItems itemdata in _dataItem)
                                {
                                    _total += Convert.ToDouble(itemdata.total_alignment);
                                }


                                _newItem.total_alignment = _total.ToString();
                                _New.Add(_newItem);
                            }
                        }

                        //foreach (var item in _BudgetRevision)
                        //{
                        //    List<BudgetRevision> _new = _BudgetItems.Where(x => x.ExpenseItem == item.to_expense_item).ToList();
                        //    if (_new.Count == 0)
                        //    {
                        //        BudgetRevisionItems _newItem = new BudgetRevisionItems();
                        //        _newItem.to_expense_item = item.to_expense_item;
                        //        _newItem.to_uacs = item.to_uacs;
                        //        _newItem.total_alignment = item.total_alignment.ToString();
                        //        _New.Add(_newItem);
                        //    }
                        //}
                        String RafNo = _ForApprovalRAF[0].Raf.Trim();
                        ApproveRevision(RafNo,this.DivId, this.SelectedYear, FundSource, _dataTo, _data, _New);

                       
                       

                       // svc.appro

                    }

                    break;

            }


        }

        private void ApproveRevision(String RafNo, String _divid, String _year, String _fundsource, List<BudgetRevisionItems> _data, List<BudgetRevisionItems> _from, List<BudgetRevisionItems> _new) 
        {

            String _sqlString = "";

            var sb = new System.Text.StringBuilder(97);
            sb.AppendLine(@"UPDATE mnda_alignment_record");
            sb.AppendLine(@"SET is_approved = 1");
            sb.AppendLine(@"WHERE division_pap = '" + _divid + "' and division_year = '" + _year + "' and is_active =1 and fundsource ='" + _fundsource + "';");

            sb.AppendLine(@"UPDATE mnda_raf_no");
            sb.AppendLine(@"SET status = 'Served'");
            sb.AppendLine(@"WHERE div_id = '" + _divid + "' and year = '" + _year + "' and status ='For Approval';");


            foreach (var item in _from)
            {
                sb.AppendLine(@"INSERT INTO   dbo.mnda_fund_source_adjustment");
                sb.AppendLine(@"( fund_code,mooe_id,adjustment,plusminus,raf_no,status) ");
                sb.AppendLine(@"VALUES ");
                sb.AppendLine(@"( '" + _fundsource + "','" + item.from_uacs + "'," + item.total_alignment + ",'-','" + RafNo + "','1');");

            }
            foreach (var item in _data)
            {
                sb.AppendLine(@"INSERT INTO   dbo.mnda_fund_source_adjustment");
                sb.AppendLine(@"( fund_code,mooe_id,adjustment,plusminus,raf_no,status) ");
                sb.AppendLine(@"VALUES ");
                sb.AppendLine(@"( '" + _fundsource + "','" + item.to_uacs + "'," + item.total_alignment + ",'+','" + RafNo + "','1');");

            }

            foreach (var item in _new)
            {
                sb.AppendLine(@"INSERT INTO dbo.mnda_fund_source_line_item (Fund_Source_Id,division_id, mooe_id, Amount, Year, expenditure_type, adjustment,is_revision,plusminus) ");
                sb.AppendLine(@"VALUES ('" + _fundsource + "','" + _divid + "','" + item.to_uacs + "'," + item.total_alignment + "," + _year + ",'MOOE',0,1,'+');");

            }


            _sqlString += sb.ToString();


            MinDAFSVCClient svc = new MinDAFSVCClient();
            object _return = null;

            svc.ExecuteQueryCompleted += delegate(object sender, ExecuteQueryCompletedEventArgs e)
            {
                _return = e.Result;
                svc.CloseAsync();
                Boolean _res = (Boolean)_return;

                if (_res)
                {
                    MessageBox.Show("Revision Approved successfully", "Confirmation", MessageBoxButton.OK);
                    grdData.ItemsSource = null;
                }
                //_expenseitem =(ppmpCONS_ExpenditureItems[])_return;
                //FetchDivisions(this.WorkingYear);
                return;
            };
            svc.ExecuteQueryAsync(_sqlString);

        }

        void c_finance_SQLOperation(object sender, EventArgs e)
        {
            switch (c_finance.Process)
            {
                case "ApproveRevision":
                    MessageBox.Show("Revision Approved successfully", "Confirmation", MessageBoxButton.OK);
                    grdData.ItemsSource = null;
                    break;
                case "RemoveAlignments":
                    MessageBox.Show("Revision has been successfully", "Confirmation", MessageBoxButton.OK);
                    grdData.ItemsSource = null;
                    break;

            }
        }

        private void btnRAFReport_Click(object sender, RoutedEventArgs e)
        {
      //   HtmlPage.Window.Navigate(new Uri("http://mindafinance/downloads/raf/FinanceReport.application"), "_blank");
        HtmlPage.Window.Navigate(new Uri("http://mindafinance/help/raf/FinanceReport.application"), "_blank");
        }

        private void grdSubUnit_ActiveCellChanging(object sender, ActiveCellChangingEventArgs e)
        {

        }

        private void btnUnlock_Click(object sender, RoutedEventArgs e)
        {

        }



        public bool isApproved { get; set; }

        public bool hasUnApproved { get; set; }
    }
}

