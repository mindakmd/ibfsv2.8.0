﻿using Infragistics.Controls.Grids;
using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmConsolidated : ChildWindow
    {

        public String OfficeId { get; set; }
        public String DivisionId { get; set; }
        public String WorkingYear { get; set; }
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();

        private List<DivisionBudgetAllocationFields> ListDivision = new List<DivisionBudgetAllocationFields>();
        private List<DivisionBudgetAllocationFields> ListDivisionUnits = new List<DivisionBudgetAllocationFields>();
        private List<OfficeDataFields> ListOfficeData = new List<OfficeDataFields>();
        private clsDivisionBudgetAllocation c_div = new clsDivisionBudgetAllocation();
        private List<RespoData> ListRespo = new List<RespoData>();

        private List<lib_SUB_PAP> ListSubPap = new List<lib_SUB_PAP>();
        private List<lib_PAP> ListPap = new List<lib_PAP>();
        private List<lib_MainBudgetAllowanceConsolidated> ListBudgetMain = new List<lib_MainBudgetAllowanceConsolidated>();

        private List<lib_DivisionBudgetExpeniture> ListExpenditure = new List<lib_DivisionBudgetExpeniture>();
        private List<lib_BudgetAllocationExpenditure> ListBudgetExpenditure = new List<lib_BudgetAllocationExpenditure>();

        public frmConsolidated(String _DivisionId)
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            c_div.SQLOperation += c_div_SQLOperation;
            DivisionId = _DivisionId;
              
        }
        private void LoadExpenditure()
        {
          

        }
        void c_div_SQLOperation(object sender, EventArgs e)
        {
            switch (c_div.Process)
            {
                case "SaveDivisionBudget":
                   
                    //  FetchBudgetExpenditureAllocation();   //  FetchOfficeData();
                    break;
                case "UpdateDivisionBudget":
                    FetchBudgetExpenditureAllocation();
            
                    break;
                case "RemoveBudget":
                    FetchBudgetExpenditureAllocation();   // FetchOfficeData();
                    break;
                case "SaveDivisionBudgetExpenditure":
                    FetchBudgetExpenditureAllocation();   // FetchOfficeData();
                    break;
                case "FetchBudgetExpenditureAllocation":
                    FetchBudgetExpenditureAllocation();   // FetchOfficeData();
                    break;
            }
        }
        private void ComputeTotal()
        {
            double _totals = 0.00;
            foreach (var item in ListBudgetExpenditure)
            {
                _totals += Convert.ToDouble(item.Amount);
            }

         
        }
        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {

            var _results = e.Result.ToString();
            switch (c_div.Process)
            {
                case "FetchRespo":
                    XDocument oDocKeyResultsFetchRespo = XDocument.Parse(_results);
                    var _dataListsFetchRespo = from info in oDocKeyResultsFetchRespo.Descendants("Table")
                                               select new RespoData
                                               {
                                                   Id = Convert.ToString(info.Element("id").Value),
                                                   Name = Convert.ToString(info.Element("respo_name").Value)
                                               };

                    ListRespo.Clear();


                    foreach (var item in _dataListsFetchRespo)
                    {
                        RespoData _varDetails = new RespoData();


                        _varDetails.Id = item.Id;
                        _varDetails.Name = item.Name;


                        ListRespo.Add(_varDetails);

                    }

              

                    break;
                case "FetchPAP":
                    XDocument oDocKeyResultsFetchPAP = XDocument.Parse(_results);
                    var _dataListsFetchPAP = from info in oDocKeyResultsFetchPAP.Descendants("Table")
                                             select new lib_PAP
                                             {
                                                 DBM_Pap_Id = Convert.ToString(info.Element("DBM_Pap_Id").Value),
                                                 DBM_Sub_Pap_Code = Convert.ToString(info.Element("DBM_Sub_Pap_Code").Value),
                                                 DBM_Sub_Pap_Desc = Convert.ToString(info.Element("DBM_Sub_Pap_Desc").Value),
                                                 DBM_Sub_Pap_id = Convert.ToString(info.Element("DBM_Sub_Pap_id").Value)
                                             };

                    ListPap.Clear();

                    foreach (var item in _dataListsFetchPAP)
                    {
                        lib_PAP _varDetails = new lib_PAP();


                        _varDetails.DBM_Pap_Id = item.DBM_Pap_Id;
                        _varDetails.DBM_Sub_Pap_Code = item.DBM_Sub_Pap_Code;
                        _varDetails.DBM_Sub_Pap_Desc = item.DBM_Sub_Pap_Desc;
                        _varDetails.DBM_Sub_Pap_id = item.DBM_Sub_Pap_id;


                        ListPap.Add(_varDetails);

                    }

                    FetchSubPAP();

                    break;
                case "FetchSubPAP":
                    XDocument oDocKeyResultsFetchSubPAP = XDocument.Parse(_results);
                    var _dataListsFetchSubPAP = from info in oDocKeyResultsFetchSubPAP.Descendants("Table")
                                                select new lib_SUB_PAP
                                                {
                                                    DBM_Sub_Id = Convert.ToString(info.Element("DBM_Sub_Id").Value),
                                                    Description = Convert.ToString(info.Element("Description").Value),
                                                    PAP = Convert.ToString(info.Element("PAP").Value),
                                                    Sub_Id = Convert.ToString(info.Element("Sub_Id").Value)
                                                };

                    ListSubPap.Clear();

                    foreach (var item in _dataListsFetchSubPAP)
                    {
                        lib_SUB_PAP _varDetails = new lib_SUB_PAP();


                        _varDetails.DBM_Sub_Id = item.DBM_Sub_Id;
                        _varDetails.Description = item.Description;
                        _varDetails.PAP = item.PAP;
                        _varDetails.Sub_Id = item.Sub_Id;


                        ListSubPap.Add(_varDetails);

                    }
                    FetchDivisionUnits();


                    break;
                case "FetchDivisionUnits":
                    XDocument oDocKeyResultsFetchDivisionUnits = XDocument.Parse(_results);

                    var _dataListsFetchDivisionUnits = from info in oDocKeyResultsFetchDivisionUnits.Descendants("Table")
                                                       select new DivisionBudgetAllocationFields
                                                       {
                                                           DBM_Sub_Pap_Id = Convert.ToString(info.Element("DBM_Sub_Pap_Id").Value),
                                                           Division_Code = Convert.ToString(info.Element("Division_Code").Value),
                                                           Division_Desc = Convert.ToString(info.Element("Division_Desc").Value),
                                                           Division_Id = Convert.ToString(info.Element("Division_Id").Value),
                                                           UnitCode = Convert.ToString(info.Element("UnitCode").Value)

                                                       };


                    ListDivisionUnits.Clear();

                    foreach (var item in _dataListsFetchDivisionUnits)
                    {

                        DivisionBudgetAllocationFields _temp = new DivisionBudgetAllocationFields();


                        _temp.DBM_Sub_Pap_Id = item.DBM_Sub_Pap_Id;
                        _temp.Division_Code = item.Division_Code;
                        _temp.Division_Desc = item.Division_Desc;
                        _temp.Division_Id = item.Division_Id;
                        _temp.UnitCode = item.UnitCode;


                        ListDivisionUnits.Add(_temp);

                    }
                    FetchDivisionPerOffice();
                    break;
                case "FetchDivisionPerOffice":
                    XDocument oDocKeyResults = XDocument.Parse(_results);

                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new DivisionBudgetAllocationFields
                                     {
                                         DBM_Sub_Pap_Id = Convert.ToString(info.Element("DBM_Sub_Pap_Id").Value),
                                         Division_Code = Convert.ToString(info.Element("Division_Code").Value),
                                         Division_Desc = Convert.ToString(info.Element("Division_Desc").Value),
                                         Division_Id = Convert.ToString(info.Element("Division_Id").Value),
                                         UnitCode = Convert.ToString(info.Element("UnitCode").Value)

                                     };


                    ListDivision.Clear();
                    List<ProfData> _ComboList = new List<ProfData>();

                    foreach (var item in _dataLists)
                    {
                        ProfData _varProf = new ProfData();
                        DivisionBudgetAllocationFields _temp = new DivisionBudgetAllocationFields();

                        _varProf._Name = item.Division_Desc;

                        _temp.DBM_Sub_Pap_Id = item.DBM_Sub_Pap_Id;
                        _temp.Division_Code = item.Division_Code;
                        _temp.Division_Desc = item.Division_Desc;
                        _temp.Division_Id = item.Division_Id;
                        _temp.UnitCode = item.UnitCode;

                        _ComboList.Add(_varProf);
                        ListDivision.Add(_temp);

                    }

                    ListBudgetMain.Clear();
                    foreach (lib_PAP item in ListPap)
                    {
                        lib_MainBudgetAllowanceConsolidated _xdata = new lib_MainBudgetAllowanceConsolidated();
                        _xdata.Id = item.DBM_Sub_Pap_id;
                        _xdata.Pap = item.DBM_Sub_Pap_Code;
                        _xdata.Description = item.DBM_Sub_Pap_Desc;
                        _xdata.IsDiv = false;
                        _xdata.IsUnit = false;
                        ListBudgetMain.Add(_xdata);

                        List<lib_SUB_PAP> subpap = ListSubPap.Where(x => x.DBM_Sub_Id == item.DBM_Sub_Pap_id).ToList();

                        foreach (lib_SUB_PAP itemsubpap in subpap)
                        {
                            _xdata = new lib_MainBudgetAllowanceConsolidated();
                            _xdata.Id = itemsubpap.Sub_Id;
                            _xdata.Pap = itemsubpap.PAP;
                            _xdata.Description = "       " + itemsubpap.Description;
                            _xdata.IsDiv = false;
                            _xdata.IsUnit = false;

                            ListBudgetMain.Add(_xdata);


                            List<DivisionBudgetAllocationFields> div = ListDivision.Where(x => x.Division_Code == itemsubpap.PAP).ToList();
                            foreach (DivisionBudgetAllocationFields itemdiv in div)
                            {
                                _xdata = new lib_MainBudgetAllowanceConsolidated();
                                _xdata.Id = itemdiv.Division_Id;
                                _xdata.Pap = "";
                                _xdata.Description = "       " + itemdiv.Division_Code + " - " + itemdiv.Division_Desc;
                                _xdata.IsDiv = true;
                                _xdata.IsUnit = false;
                                ListBudgetMain.Add(_xdata);


                                List<DivisionBudgetAllocationFields> div_unit = ListDivisionUnits.Where(x => x.UnitCode == itemdiv.Division_Id).ToList();
                                foreach (DivisionBudgetAllocationFields item_unit in div_unit)
                                {
                                    _xdata = new lib_MainBudgetAllowanceConsolidated();
                                    _xdata.Id = item_unit.Division_Id;
                                    _xdata.Pap = "";
                                    _xdata.Description = "                          " + item_unit.Division_Desc;
                                    _xdata.IsDiv = false;
                                    _xdata.IsUnit = true;
                                    ListBudgetMain.Add(_xdata);
                                }
                            }
                        }
                    }

                    foreach (lib_MainBudgetAllowanceConsolidated item in ListBudgetMain)
                    {
                        
                    }

                    grdPAP.ItemsSource = null;
                    grdPAP.ItemsSource = ListBudgetMain;

                    grdPAP.Columns["Id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdPAP.Columns["IsDiv"].Visibility = System.Windows.Visibility.Collapsed;
                    grdPAP.Columns["IsUnit"].Visibility = System.Windows.Visibility.Collapsed;

                    Column col_Pap = grdPAP.Columns.DataColumns["Pap"];
                    col_Pap.Width = new ColumnWidth(80, false);
            
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchOfficeDatas":
                    XDocument oDocKeyResultsFetchOfficeData = XDocument.Parse(_results);

                    var _dataListsFetchOfficeData = from info in oDocKeyResultsFetchOfficeData.Descendants("Table")
                                                    select new OfficeDataFields
                                                    {
                                                        id = Convert.ToString(info.Element("id").Value),
                                                        Amount = Convert.ToString(info.Element("budget_allocation").Value),
                                                        entry_year = Convert.ToString(info.Element("entry_year").Value),
                                                        Fund_Name = Convert.ToString(info.Element("Fund_Name").Value),
                                                        status = Convert.ToString(info.Element("Status").Value)



                                                    };


                    ListOfficeData.Clear();


                    foreach (var item in _dataListsFetchOfficeData)
                    {

                        OfficeDataFields _temp = new OfficeDataFields();


                        _temp.Amount = Convert.ToDouble(item.Amount).ToString("#,##0.00");
                        _temp.entry_year = item.entry_year;
                        _temp.Fund_Name = item.Fund_Name;
                        _temp.status = item.status;
                        _temp.id = item.id;
                        ListOfficeData.Add(_temp);

                    }
                 
                    ComputeTotal();
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchExpenditureItems":
                    XDocument oDocKeyResultsFetchExpenditureItems = XDocument.Parse(_results);

                    var _dataListsFetchExpenditureItems = from info in oDocKeyResultsFetchExpenditureItems.Descendants("Table")
                                                          select new lib_DivisionBudgetExpeniture
                                                          {
                                                              name = Convert.ToString(info.Element("name").Value),
                                                              uacs_code = Convert.ToString(info.Element("uacs_code").Value)

                                                          };


                    ListExpenditure.Clear();
                    List<ProfData> _ComboListExpenditure = new List<ProfData>();

                    foreach (var item in _dataListsFetchExpenditureItems)
                    {
                        ProfData _varProf = new ProfData();
                        lib_DivisionBudgetExpeniture _temp = new lib_DivisionBudgetExpeniture();

                        _varProf._Name = item.name;

                        _temp.name = item.name;
                        _temp.uacs_code = item.uacs_code;

                        //  cmbExpenditures.Items.Add(item.name);
                        _ComboListExpenditure.Add(_varProf);
                        ListExpenditure.Add(_temp);

                    }
            
                    FetchBudgetExpenditureAllocation();
                    this.Cursor = Cursors.Arrow;
                    break;
               
            }
        }
        private void FetchExpenditureItems()
        {
            c_div.Process = "FetchExpenditureItems";
            svc_mindaf.ExecuteSQLAsync(c_div.FetchExpenditureItems());
        }


        private void GenerateYear()
        {
            int _year = DateTime.Now.Year;
            int _limit = _year + 100;

            for (int i = _year; i != _limit; i++)
            {
                cmbYear.Items.Add(_year);
                _year += 1;
            }
            this.cmbYear.SelectedIndex = 0;

            LoadExpenditure();

        }
        private void Removebudget(String _id)
        {
            c_div.Process = "RemoveBudget";
            c_div.RemoveBudgetAllocation(_id);

        }
        private void SaveDivisionAllocation()
        {


            



        }

     
        private void ClearData()
        {
            
        }
        private void UpdateDivisionAllocation(String _id, String _Amount)
        {
            c_div.Process = "UpdateDivisionBudget";
            c_div.UpdateDivisionBudgetLineItem(_id, _Amount);

        }
        private void FetchDivisionPerOffice()
        {
            c_div.Process = "FetchDivisionPerOffice";
            svc_mindaf.ExecuteSQLAsync(c_div.FetchDivisionPerOffice(OfficeId, WorkingYear));
        }

        private void FetchSubPAP()
        {
            c_div.Process = "FetchSubPAP";
            svc_mindaf.ExecuteSQLAsync(c_div.FetchSubPap(this.WorkingYear));
        }
        private void FetchDivisionUnits()
        {
            c_div.Process = "FetchDivisionUnits";
            svc_mindaf.ExecuteSQLAsync(c_div.FetchDivisionUnit(WorkingYear));
        }
        private void FetchPAP()
        {
            c_div.Process = "FetchPAP";
            svc_mindaf.ExecuteSQLAsync(c_div.FetchPap(this.WorkingYear));
        }
        private void FetchRespoCenter()
        {
            string _id = grdPAP.Rows[grdPAP.ActiveCell.Row.Index].Cells["Id"].Value.ToString();

            c_div.Process = "FetchRespo";

            svc_mindaf.ExecuteSQLAsync(c_div.FetchRespoLibrary(_id,"mf-1",this.WorkingYear));
        }
        private void FetchOfficeData()
        {

            String selectedItem = grdPAP.Rows[grdPAP.ActiveCell.Row.Index].Cells["Id"].Value.ToString();

            if (selectedItem != null)
            {
                c_div.Process = "FetchOfficeData";

                if ((Boolean)grdPAP.Rows[grdPAP.ActiveCell.Row.Index].Cells["IsDiv"].Value)
                {
                    var x = ListDivision.Where(items => items.Division_Id == selectedItem).ToList();
                    if (x.Count != 0)
                    {
                        svc_mindaf.ExecuteSQLAsync(c_div.FetchOfficeData(x[0].Division_Id, cmbYear.SelectedItem.ToString()));
                    }
                }
                else
                {
                    var y = ListDivisionUnits.Where(items => items.Division_Id == selectedItem).ToList();
                    if (y.Count != 0)
                    {
                        svc_mindaf.ExecuteSQLAsync(c_div.FetchOfficeData(y[0].Division_Id, cmbYear.SelectedItem.ToString()));
                    }
                }


            }


        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

      

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
           
        }

        private void cmbYear_DropDownClosed(object sender, EventArgs e)
        {
            FetchOfficeData();
        }

        private void cmbDivision_DropDownClosed(object sender, EventArgs e)
        {
            FetchExpenditureItems();
        }

        private void grdData_CellDoubleClicked(object sender, Infragistics.Controls.Grids.CellClickedEventArgs e)
        {
           
        }

        private void grdData_CellExitedEditMode(object sender, Infragistics.Controls.Grids.CellExitedEditingEventArgs e)
        {
          
        }

        private void FetchBudgetExpenditureAllocation()
        {

            String selectedItem = grdPAP.Rows[grdPAP.ActiveCell.Row.Index].Cells["Id"].Value.ToString();
            String selectedFundSource = "";


            if (selectedFundSource != null)
            {
                if ((Boolean)grdPAP.Rows[grdPAP.ActiveCell.Row.Index].Cells["IsDiv"].Value)
                {
                    var x = ListDivision.Where(items => items.Division_Id == selectedItem).ToList();
                    var xFundSource = ListRespo.Where(items => items.Name == selectedFundSource).ToList();
                    if (x.Count != 0)
                    {
                        c_div.Process = "FetchBudgetExpenditureAllocation";
                        svc_mindaf.ExecuteSQLAsync(c_div.FetchBudgetExpenditureAllocation(cmbYear.SelectedItem.ToString(), x[0].Division_Id, xFundSource[0].Name));
                    }
                }
                if ((Boolean)grdPAP.Rows[grdPAP.ActiveCell.Row.Index].Cells["IsUnit"].Value)
                {
                    var x = ListDivisionUnits.Where(items => items.Division_Id == selectedItem).ToList();
                    var xFundSource = ListRespo.Where(items => items.Name == selectedFundSource).ToList();
                    if (x.Count != 0)
                    {
                        c_div.Process = "FetchBudgetExpenditureAllocation";
                        svc_mindaf.ExecuteSQLAsync(c_div.FetchBudgetExpenditureAllocation(cmbYear.SelectedItem.ToString(), x[0].Division_Id, xFundSource[0].Name));
                    }
                }

            }



        }



        private void txtBudget_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void btnAddRespo_Click(object sender, RoutedEventArgs e)
        {
            frmLibraryFundSource f_form = new frmLibraryFundSource();
            f_form.IsDivision = false;
            f_form.Reload += f_form_Reload;
            f_form.Show();
        }

        void f_form_Reload(object sender, EventArgs e)
        {
           
        }

        private void cmbExpenditures_DropDownClosed_1(object sender, EventArgs e)
        {

        }

        private void cmbExpenditures_DropDownClosed(object sender, EventArgs e)
        {
            //txtBudget.Value = 0.00;
            //txtBudget.IsEnabled = true;
            //txtBudget.Focus();
        }

        private void grdData_CellClicked(object sender, Infragistics.Controls.Grids.CellClickedEventArgs e)
        {

        }

        private void txtBudget_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                //txtBudget.Value = 0.00;
                //cmbExpenditures.SelectedItem = null;
                //grdData.IsEnabled = true;
                //txtBudget.IsEnabled = false;
                //grdData.Focus();
                //btnAdd.Content = "Add";
            }
        }

        private void btnAdd_KeyDown(object sender, KeyEventArgs e)
        {

        }



        private void cmbExpenditureType_DropDownClosed(object sender, EventArgs e)
        {
           
            ComputeTotal();
        }

        private void DisplayGroupTotals()
        {
            List<ExpType> Main = new List<ExpType>();
            double Totals = 0.00;

            List<lib_BudgetAllocationExpenditure> _MOOE = ListBudgetExpenditure.Where(x => x.ExpType == "MOOE").ToList();
            ExpType _mooe_type = new ExpType();
            _mooe_type.ExpType_Name = "MOOE";
            foreach (lib_BudgetAllocationExpenditure item in _MOOE)
            {
                Totals += Convert.ToDouble(item.Amount);
            }
            _mooe_type.ExpType_Total = Totals.ToString("#,##0.00");
            Totals = 0;

            List<lib_BudgetAllocationExpenditure> _PS = ListBudgetExpenditure.Where(x => x.ExpType == "PS").ToList();
            ExpType _ps_type = new ExpType();
            _ps_type.ExpType_Name = "PS";
            foreach (lib_BudgetAllocationExpenditure item in _PS)
            {
                Totals += Convert.ToDouble(item.Amount);
            }
            _ps_type.ExpType_Total = Totals.ToString("#,##0.00");
            Totals = 0;

            List<lib_BudgetAllocationExpenditure> _CO = ListBudgetExpenditure.Where(x => x.ExpType == "CO").ToList();
            ExpType _ps_type_co = new ExpType();
            _ps_type_co.ExpType_Name = "CO";
            foreach (lib_BudgetAllocationExpenditure item in _CO)
            {
                Totals += Convert.ToDouble(item.Amount);
            }
            _ps_type_co.ExpType_Total = Totals.ToString("#,##0.00");
            Totals = 0;


            Main.Add(_mooe_type);
            Main.Add(_ps_type);
            Main.Add(_ps_type_co);

        }

        private void grdPAP_CellClicked(object sender, CellClickedEventArgs e)
        {
          
        }

        private void grdRespoCenter_CellClicked(object sender, CellClickedEventArgs e)
        {
            //FetchOfficeData();
            FetchExpenditureItems();
        }

        private void frmConso_Loaded(object sender, RoutedEventArgs e)
        {
            GenerateYear();

            FetchPAP();

        }
    }
}

