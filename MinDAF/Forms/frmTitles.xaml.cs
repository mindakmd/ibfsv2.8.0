﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmTitles : ChildWindow
    {

        public event EventHandler SelectedData;

        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsTitles c_titles = new clsTitles();
        private List<TitleNames> _Lists = new List<TitleNames>();


        public String DivisionId { get; set; }
        public String SelectType { get; set; }
        public String DataSelected { get; set; }
        public frmTitles()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            this.Loaded += frmTitles_Loaded;
        }

        void frmTitles_Loaded(object sender, RoutedEventArgs e)
        {
            LoadTitles();
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_titles.Process)
            {
                case "LoadTitles":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new TitleNames
                                     {
                                         List = Convert.ToString(info.Element("name").Value)

                                     };

                    _Lists.Clear();


                    foreach (var item in _dataLists)
                    {
                        TitleNames _varDetails = new TitleNames();


                        _varDetails.List = item.List;



                        _Lists.Add(_varDetails);

                    }

                    grdData.ItemsSource = null;
                    grdData.ItemsSource = _Lists;

                    this.Cursor = Cursors.Arrow;

                    break;
            }
        }
        private void LoadTitles()
        {
            c_titles.Process = "LoadTitles";
            svc_mindaf.ExecuteSQLAsync(c_titles.FetchProgramLists(DivisionId,SelectType));
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (SelectedData!=null)
            {
                DataSelected = txtData.Text;
                this.SelectedData(this, new EventArgs());
            }
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void grdData_CellClicked(object sender, Infragistics.Controls.Grids.CellClickedEventArgs e)
        {
            TitleNames _selected = (TitleNames)grdData.ActiveItem;

            txtData.Text = _selected.List;
        }
    }
}

