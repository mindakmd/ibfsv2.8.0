﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Forms
{
    public partial class approveNonPSDBM_Child : ChildWindow
    {
        public String ID { get; set; }
        public String item_specifications { get; set; }

        public approveNonPSDBM_Child()
        {
            InitializeComponent();
        }

        private void approveNonPSDBMChild_Loaded(object sender, RoutedEventArgs e)
        {
            ItemSpecification.Text = item_specifications;
        }
    }
}

