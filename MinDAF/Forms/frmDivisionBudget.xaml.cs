﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmDivisionBudget : ChildWindow
    {
        public String Division { get; set; }
        public String DivisionID { get; set; }
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private List<ExpenditureItems> ListItems = new List<ExpenditureItems>();
        private List<DivisionAllocated> ListDivBudget = new List<DivisionAllocated>();
        private clsDivisionBudget c_div = new clsDivisionBudget();

        public frmDivisionBudget()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            
            var _results = e.Result.ToString();
            switch (c_div.Process)
            {
                case "FetchExpenditures":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new ExpenditureItems
                                     {
                                         id = Convert.ToString(info.Element("id").Value),
                                        name  = Convert.ToString(info.Element("name").Value)

                                     };


                    List<ProfData> _ComboList = new List<ProfData>();

                    foreach (var item in _dataLists)
                    {
                        ProfData _varProf = new ProfData();
                        ExpenditureItems _temp = new ExpenditureItems();

                        _varProf._Name = item.name;
                        _temp.id = item.id;
                        _temp.name = item.name;

                        _ComboList.Add(_varProf);
                        ListItems.Add(_temp);

                    }
                    cmbItems.ItemsSource = null;
                    cmbItems.ItemsSource = _ComboList;
                   FetchDivisionExpenditures();
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchDivisionExpenditures":
                    XDocument oDocKeyResultsFetchDivisionExpenditures = XDocument.Parse(_results);
                    var _dataListsFetchDivisionExpenditures = from info in oDocKeyResultsFetchDivisionExpenditures.Descendants("Table")
                                                              select new DivisionAllocated
                                                             {
                                                                 id = Convert.ToString(info.Element("id").Value),
                                                                 allocated_budget = Convert.ToString(info.Element("allocated_budget").Value),
                                                                 ex_id = Convert.ToString(info.Element("ex_id").Value),
                                                                 name = Convert.ToString(info.Element("name").Value),
                                                                 year = Convert.ToString(info.Element("year").Value)
                                                             };


                   ListDivBudget.Clear();

                    foreach (var item in _dataListsFetchDivisionExpenditures)
                    {
                  
                        DivisionAllocated _data = new DivisionAllocated();

                        _data.allocated_budget = item.allocated_budget;
                        _data.ex_id = item.ex_id;
                        _data.name = item.name;
                        _data.year = item.year;
                        _data.id = item.id;
                        ListDivBudget.Add(_data);

                    }
                    grdData.ItemsSource = null;
                    grdData.ItemsSource = ListDivBudget;
                    grdData.Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["ex_id"].Visibility = System.Windows.Visibility.Collapsed;
                    ComputeTotal();
                    this.Cursor = Cursors.Arrow;
                    break;
            }
        }
        private void ComputeTotal()         
        {
            double _totals = 0.00;
            foreach (var item in ListDivBudget)
            {
                _totals += Convert.ToDouble(item.allocated_budget);
            }

            txtTotal.Value = _totals;
        }
        private void ClearData() 
        {
            txtBudget.Value = 0.00;
            cmbItems.SelectedIndex = -1;
            cmbYear.SelectedIndex = 0;
        }
        private void FetchExpenditures() 
        {
            c_div.Process = "FetchExpenditures";
            svc_mindaf.ExecuteSQLAsync(c_div.FetchExpenditureItems());
        }
        private void FetchDivisionExpenditures()
        {
            c_div.Process = "FetchDivisionExpenditures";
           
            svc_mindaf.ExecuteSQLAsync(c_div.FetchGridData(this.DivisionID,cmbYear.SelectedItem.ToString()));
        }
        private void FetchDivisionPerOffice(String _id)
        {
            c_div.Process = "FetchDivisionExpenditures";

            svc_mindaf.ExecuteSQLAsync(c_div.FetchGridData(this.DivisionID, cmbYear.SelectedItem.ToString()));
        }
        private void SaveBudget()         
        {
            String _exp_id = "";
         
                    var selectedItem = cmbItems.SelectedItem as ProfData;

                    if (selectedItem != null)
                    {
                        List<ExpenditureItems> x = ListItems.Where(item => item.name == selectedItem._Name).ToList();
                        if (x.Count != 0)
                        {
                            _exp_id = x[0].id;
                        }
                    }

                    c_div.Process = "SaveData";
                    c_div.SQLOperation += c_div_SQLOperation;
                    c_div.SaveBudget(_exp_id,"-", txtBudget.Value.ToString(), this.cmbYear.SelectedItem.ToString());
         
        
        }

        void c_div_SQLOperation(object sender, EventArgs e)
        {
            switch (c_div.Process)
            {
                case "SaveData":
                    FetchDivisionExpenditures();
                    ClearData();
                    break;
                case "UpdateData":
                    FetchDivisionExpenditures();
                    break;
                case "RemoveData":
                    FetchDivisionExpenditures();
                    break;
                default:
                    break;
            }
        }
        private void GenerateYear()
        {
            int _year = DateTime.Now.Year;
            int _limit = _year + 100;

            for (int i = _year; i != _limit; i++)
            {
                cmbYear.Items.Add(_year);
                _year += 1;
            }
            cmbYear.SelectedIndex = 0;
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            SaveBudget();

        }

        private void frm_b_div_b_Loaded(object sender, RoutedEventArgs e)
        {
            GenerateYear();
          FetchExpenditures();
         
         
        }

        private void RemoveData(List<String> _data) 
        {
            c_div.Process = "RemoveData";
            c_div.SQLOperation += c_div_SQLOperation;
            c_div.RemoveData(_data);

        }

        private void UpdateData(String _allocated,String _year) 
        {
            c_div.Process = "UpdateData";
            c_div.UpdateBudget(grdData.Rows[grdData.ActiveCell.Row.Index].Cells["id"].Value.ToString(), _allocated, _year);
        }
        private void grdData_CellExitedEditMode(object sender, Infragistics.Controls.Grids.CellExitedEditingEventArgs e)
        {
            if (e.Cell.Column.HeaderText == "allocated_budget" || e.Cell.Column.HeaderText =="year")
            {
                UpdateData(grdData.Rows[e.Cell.Row.Index].Cells["allocated_budget"].Value.ToString(), grdData.Rows[e.Cell.Row.Index].Cells["year"].Value.ToString());
            }
        }

        private void cmbYear_DropDownClosed(object sender, EventArgs e)
        {
            FetchDivisionExpenditures();
        }

        private void chkAll_Checked(object sender, RoutedEventArgs e)
        {
            cmbItems.IsEnabled = false;
        }

        private void chkAll_Unchecked(object sender, RoutedEventArgs e)
        {
            cmbItems.IsEnabled = true;
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            List<String> _id_group = new List<String>();
            foreach (var item in grdData.SelectionSettings.SelectedRows)
            {
                String _x_data = "";
                _x_data = item.Cells["id"].Value.ToString();

                _id_group.Add(_x_data);
            }
            RemoveData(_id_group);
        }

       
    }

    

}

