﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using MinDAF.Usercontrol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class AddNonPSDBMSupply : ChildWindow
    {
        public String _Year { get; set; }

        public String Expenditure_Type { get; set; }
        
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsBudgetSupplies c_budgetsupplies = new clsBudgetSupplies();
        frmNonPSDBMSupplies frm_non_ps = new frmNonPSDBMSupplies();
        public AddNonPSDBMSupply()
        {
            InitializeComponent();
            //txtExpenditureID.Text = ExpenditureType;
        }
        void c_budgetsupplies_SQLOperation(object sender, EventArgs e)
        {
            switch (c_budgetsupplies.Process)
            {
                case "SaveActivityItems":
                    //ClearData();
                    //FetchProcurementDetails();
                    break;
                case "Suspend":
                    //FetchProcurementDetails();
                    break;

            }
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        private void SaveBtn_Click(object sender, RoutedEventArgs e)
        {
            SaveActivityItems();
        }



        private void SaveActivityItems()
        {
            //c_budgetsupplies.SaveNonPSDBMSupply(txtItemName.Text, cmbSubCategory.SelectionBoxItem.ToString(), cmbUnit.SelectionBoxItem.ToString(), Convert.ToDouble(txtUnitPrice.Text), cmbExpenditureID.SelectionBoxItem.ToString(), this._Year);
            c_budgetsupplies.SaveNonPSDBMSupply(txtItemName.Text, cmbSubCategory.SelectionBoxItem.ToString(), cmbUnit.SelectionBoxItem.ToString(), Convert.ToDouble(txtUnitPrice.Text), txtExpenditureID.Text, this._Year);
            if (MessageBox.Show("Data successfully saved. Awaiting for the Supply Officer's Approval.", "Information", MessageBoxButton.OK) == System.Windows.MessageBoxResult.OK)
            {
                //FetchProcurementItems();
                frm_non_ps.RefreshData();
            }
        }

        private void AddNonPSDBMSupply_MouseEnter(object sender, MouseEventArgs e)
        {
            txtExpenditureID.Text = Expenditure_Type;
        }
    }
}

