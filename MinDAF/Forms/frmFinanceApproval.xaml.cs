﻿using Infragistics.Controls.Grids;
using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmFinanceApproval : ChildWindow
    {

        public event EventHandler Reload;
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsFinanceApproval c_finance = new clsFinanceApproval();

        private List<App_FundSources> ListFundSourceType = new List<App_FundSources>();
        private List<SubPAP_Division> ListSubPap = new List<SubPAP_Division>();
        private List<OfficesPAP> ListOffice = new List<OfficesPAP>();
        private List<Divisions> ListDivision = new List<Divisions>();
        private List<Divisions> ListDivisionUnit = new List<Divisions>();

        private List<ChiefData_FundSourceMain> cd_FundSourceMain = new List<ChiefData_FundSourceMain>();
        private List<ChiefData_FundSourceMainDetails> cd_FundSourceMainDetails = new List<ChiefData_FundSourceMainDetails>();
        private List<ChiefData_FundSource> cd_FundSource = new List<ChiefData_FundSource>();
        private List<ChiefData_ListExpenditure> cd_ExpenditureLists = new List<ChiefData_ListExpenditure>();
        private List<Approved_Data> cd_Approved = new List<Approved_Data>();
        private List<ChiefData_DivisionFundSource> cd_DivisionFundSource = new List<ChiefData_DivisionFundSource>();
        private List<OfficeDetails> BalanceDetails = new List<OfficeDetails>();
        private List<ChiefData_ItemLists> ListItemDetails = new List<ChiefData_ItemLists>();

        private String FundSource;

        private Boolean isUnit = false;
        public String WorkingYear { get; set; }
        public frmFinanceApproval()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            String _results = e.Result.ToString();

            switch (c_finance.Process)
            {
                case "LoadPAP":
                    XDocument oDocKeyFetchListOffice = XDocument.Parse(_results);
                    var _dataListsFetchListOffice = from info in oDocKeyFetchListOffice.Descendants("Table")
                                                    select new OfficesPAP
                                                  {
                                                      DBM_Pap_Id = Convert.ToString(info.Element("DBM_Pap_Id").Value),
                                                      DBM_Sub_Pap_Code = Convert.ToString(info.Element("DBM_Sub_Pap_Code").Value),
                                                      DBM_Sub_Pap_Desc = Convert.ToString(info.Element("DBM_Sub_Pap_Desc").Value),
                                                      DBM_Sub_Pap_id = Convert.ToString(info.Element("DBM_Sub_Pap_id").Value),
                              
                                                  };

                    ListOffice.Clear();
                    cmbPAP.Items.Clear();
                    foreach (var item in _dataListsFetchListOffice)
                    {
                        OfficesPAP _varDetails = new OfficesPAP();

                        

                        _varDetails.DBM_Pap_Id = item.DBM_Pap_Id;
                        _varDetails.DBM_Sub_Pap_Desc = item.DBM_Sub_Pap_Code + " - " + item.DBM_Sub_Pap_Desc;
                        _varDetails.DBM_Sub_Pap_Code = item.DBM_Sub_Pap_Code;
                        _varDetails.DBM_Sub_Pap_id = item.DBM_Sub_Pap_id;

                        cmbPAP.Items.Add( item.DBM_Sub_Pap_Code  + " - " + item.DBM_Sub_Pap_Desc);
                        ListOffice.Add(_varDetails);

                    }

                   
                    break;
                case "LoadSubPAP":
                    XDocument oDocKeyFetchLoadSubPAP = XDocument.Parse(_results);
                    var _dataListsFetchLoadSubPAP = from info in oDocKeyFetchLoadSubPAP.Descendants("Table")
                                                    select new SubPAP_Division
                                                    {
                                                        DBM_Sub_Id = Convert.ToString(info.Element("DBM_Sub_Id").Value),
                                                        Description = Convert.ToString(info.Element("Description").Value),
                                                        PAP = Convert.ToString(info.Element("PAP").Value),
                                                        Sub_Id = Convert.ToString(info.Element("Sub_Id").Value),
                                                    };

                    ListSubPap.Clear();
                    cmbSubPap.Items.Clear();
                    foreach (var item in _dataListsFetchLoadSubPAP)
                    {
                        SubPAP_Division _varDetails = new SubPAP_Division();



                        _varDetails.DBM_Sub_Id = item.DBM_Sub_Id;
                        _varDetails.Description = item.Description;
                        _varDetails.PAP = item.PAP;
                        _varDetails.Sub_Id = item.Sub_Id;

                        cmbSubPap.Items.Add(item.PAP + " - " + item.Description);
                        ListSubPap.Add(_varDetails);

                    }


                    break;
                case "LoadFundSource":
                    XDocument oDocKeyFetchLoadFundSource = XDocument.Parse(_results);
                    var _dataListsFetchLoadFundSource = from info in oDocKeyFetchLoadFundSource.Descendants("Table")
                                                        select new App_FundSources
                                                    {
                                                       Code  = Convert.ToString(info.Element("code").Value),
                                                        Id = Convert.ToString(info.Element("id").Value),
                                                        Name = Convert.ToString(info.Element("description").Value),
                                                    };

                    ListFundSourceType .Clear();
                    cmbFundSource.Items.Clear();
                    foreach (var item in _dataListsFetchLoadFundSource)
                    {
                        App_FundSources _varDetails = new App_FundSources();



                        _varDetails.Code = item.Code;
                        _varDetails.Id = item.Id;
                        _varDetails.Name = item.Name;
                 

                        cmbFundSource.Items.Add(item.Name);
                        ListFundSourceType.Add(_varDetails);

                    }
                    cmbExpenditure.Items.Clear();
                    cmbExpenditure.Items.Add("PS");
                    cmbExpenditure.Items.Add("MOOE");
                    cmbExpenditure.Items.Add("CO");

                    break;
                case "LoadDivision":
                    XDocument oDocKeyFetchLoadDivision = XDocument.Parse(_results);
                    var _dataListsFetchLoadDivision = from info in oDocKeyFetchLoadDivision.Descendants("Table")
                                                    select new Divisions
                                                    {
                                                        DBM_Sub_Pap_Id = Convert.ToString(info.Element("DBM_Sub_Pap_Id").Value),
                                                        Division_Code = Convert.ToString(info.Element("Division_Code").Value),
                                                        Division_Desc = Convert.ToString(info.Element("Division_Desc").Value),
                                                        Division_Id = Convert.ToString(info.Element("Division_Id").Value)

                                                    };

                    ListDivision.Clear();
                 //   cmbDivision.Items.Clear();
                    foreach (var item in _dataListsFetchLoadDivision)
                    {
                        Divisions _varDetails = new Divisions();



                        _varDetails.DBM_Sub_Pap_Id = item.DBM_Sub_Pap_Id;
                        _varDetails.Division_Code = item.Division_Code;
                        _varDetails.Division_Desc = item.Division_Desc;
                        _varDetails.Division_Id = item.Division_Id;

                      //  cmbDivision.Items.Add(item.Division_Desc);
                        ListDivision.Add(_varDetails);

                    }
                    grdDivision.ItemsSource = null;
                    grdDivision.ItemsSource = ListDivision;
                    grdDivision.Columns["DBM_Sub_Pap_Id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdDivision.Columns["Division_Id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdDivision.Columns["Division_Code"].HeaderText = "PAP";
                    grdDivision.Columns["Division_Desc"].HeaderText = "Division";

                    Column col_uacs = grdDivision.Columns.DataColumns["Division_Code"];
                    col_uacs.Width = new ColumnWidth(100, false);
            
                    
                        //  LoadModeExpenditure();

                    this.Cursor = Cursors.Arrow;
                    break;

                
                   
                    break;
                case "LoadDivisionUnit":
                    XDocument oDocKeyFetchLoadDivisionUnit = XDocument.Parse(_results);
                    var _dataListsFetchLoadDivisionUnit = from info in oDocKeyFetchLoadDivisionUnit.Descendants("Table")
                                                      select new Divisions
                                                      {
                                                          DBM_Sub_Pap_Id = Convert.ToString(info.Element("DBM_Sub_Pap_Id").Value),
                                                          Division_Code = Convert.ToString(info.Element("Division_Code").Value),
                                                          Division_Desc = Convert.ToString(info.Element("Division_Desc").Value),
                                                          Division_Id = Convert.ToString(info.Element("Division_Id").Value)

                                                      };

                    ListDivisionUnit.Clear();
                    //   cmbDivision.Items.Clear();
                    foreach (var item in _dataListsFetchLoadDivisionUnit)
                    {
                        Divisions _varDetails = new Divisions();



                        _varDetails.DBM_Sub_Pap_Id = item.DBM_Sub_Pap_Id;
                        _varDetails.Division_Code = item.Division_Code;
                        _varDetails.Division_Desc = item.Division_Desc;
                        _varDetails.Division_Id = item.Division_Id;

                        //  cmbDivision.Items.Add(item.Division_Desc);
                        ListDivisionUnit.Add(_varDetails);

                    }
                    grdSubUnit.ItemsSource = null;
                    grdSubUnit.ItemsSource = ListDivisionUnit;
                    grdSubUnit.Columns["DBM_Sub_Pap_Id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdSubUnit.Columns["Division_Id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdSubUnit.Columns["Division_Code"].HeaderText = "PAP";
                    grdSubUnit.Columns["Division_Desc"].HeaderText = "Division";

                    Column col_uacs_unit = grdSubUnit.Columns.DataColumns["Division_Code"];
                    col_uacs_unit.Width = new ColumnWidth(100, false);


                    //  LoadModeExpenditure();
                    FetchExpenditureDetailsItems();
                    this.Cursor = Cursors.Arrow;
                    break;



                    break;
                case "FetchOfficeDetails":
                    XDocument oDocFetchOfficeDetails = XDocument.Parse(_results);
                    var _dataFetchOfficeDetails = from info in oDocFetchOfficeDetails.Descendants("Table")
                                                  select new OfficeDetails
                                                  {
                                                      fund_source_id = Convert.ToString(info.Element("fund_source_id").Value),
                                                      budget_allocation = Convert.ToString(info.Element("budget_allocation").Value),
                                                      division_id = Convert.ToString(info.Element("div_id").Value),
                                                      Fund_Source = Convert.ToString(info.Element("Fund_Name").Value),
                                                      balance = "0.00",
                                                      total_program_budget = "0.00",
                                                      pap_code = ""
                                                  };



                    BalanceDetails.Clear();

                    foreach (var item in _dataFetchOfficeDetails)
                    {
                        OfficeDetails _varData = new OfficeDetails();

                        _varData.budget_allocation = item.budget_allocation;
                        _varData.division_id = item.division_id;
                        _varData.Fund_Source = item.Fund_Source;
                        _varData.total_program_budget = item.total_program_budget;
                        _varData.pap_code = item.pap_code;
                        _varData.fund_source_id = item.fund_source_id;
                        BalanceDetails.Add(_varData);

                    }
                    //  FetchOfficeTotals();
                    FetchBudgetAllocation();
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchExpenditureDetails":
                    XDocument oDocFetchExpenditureDetails = XDocument.Parse(_results);
                    var _dataFetchExpenditureDetails = from info in oDocFetchExpenditureDetails.Descendants("Table")
                                                       select new ChiefData_ListExpenditure
                                                       {
                                                           expenditure = Convert.ToString(info.Element("expenditure").Value),
                                                           fund_source_id = Convert.ToString(info.Element("fund_source_id").Value),
                                                           total = Convert.ToString(info.Element("total").Value)

                                                       };



                    cd_ExpenditureLists.Clear();

                    foreach (var item in _dataFetchExpenditureDetails)
                    {
                        ChiefData_ListExpenditure _varData = new ChiefData_ListExpenditure();

                        _varData.expenditure = item.expenditure;
                        _varData.fund_source_id = item.fund_source_id;
                        _varData.total = item.total;

                        cd_ExpenditureLists.Add(_varData);

                    }
                    FetchApprovedFinance();
                   
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchApprovedFinance":
                    XDocument oDocFetchApprovedFinance = XDocument.Parse(_results);
                    var _dataFetchApprovedFinance = from info in oDocFetchApprovedFinance.Descendants("Table")
                                                       select new Approved_Data
                                                       {
                                                            Code = Convert.ToString(info.Element("code").Value)
                                                          
                                                       };



                    cd_Approved.Clear();

                    foreach (var item in _dataFetchApprovedFinance)
                    {
                        Approved_Data _varData = new Approved_Data();

                        _varData.Code = item.Code;


                        cd_Approved.Add(_varData);

                    }
                    FetchFundSourceMainDetails(isUnit);
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchFundMain":
                    XDocument oDocFetchFundMain = XDocument.Parse(_results);
                    var _dataFetchFundMain = from info in oDocFetchFundMain.Descendants("Table")
                                             select new ChiefData_FundSourceMain
                                             {
                                                 id = Convert.ToString(info.Element("id").Value),
                                                 fund_source = Convert.ToString(info.Element("fund_name").Value)
                                             };



                    cd_FundSourceMain.Clear();

                    foreach (var item in _dataFetchFundMain)
                    {
                        ChiefData_FundSourceMain _varData = new ChiefData_FundSourceMain();

                        _varData.id = item.id;
                        _varData.fund_source = item.fund_source;

                        cd_FundSourceMain.Add(_varData);
                    }
                    FetchExpenditureDetails(isUnit);
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchExpenditureDetailsItemLists":
                    XDocument oDocFetchFetchExpenditureDetailsItemLists = XDocument.Parse(_results);
                    var _dataFetchFetchExpenditureDetailsItemLists = from info in oDocFetchFetchExpenditureDetailsItemLists.Descendants("Table")
                                             select new ChiefData_ItemLists
                                             {
                                                 id = Convert.ToString(info.Element("id").Value),
                                                 expenditure = Convert.ToString(info.Element("expenditure").Value),
                                                  months = Convert.ToString(info.Element("month").Value),
                                                   total = Convert.ToDouble(info.Element("total").Value)
                                             };



                    ListItemDetails.Clear();

                    foreach (var item in _dataFetchFetchExpenditureDetailsItemLists)
                    {
                        ChiefData_ItemLists _varData = new ChiefData_ItemLists();

                        _varData.id = item.id;
                        _varData.expenditure = item.expenditure;
                        _varData.months = item.months;
                        _varData.total = item.total;

                        ListItemDetails.Add(_varData);
                    }
                    FetchFundSourceMain(isUnit);
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchFundSourceMainDetails":
                    XDocument oDocFetchFundSourceMainDetails = XDocument.Parse(_results);
                    var _dataFetchFundSourceMainDetails = from info in oDocFetchFundSourceMainDetails.Descendants("Table")
                                                          select new ChiefData_FundSourceMainDetails
                                                          {
                                                              allocation = Convert.ToString(info.Element("Allocation").Value),
                                                              fund_name = Convert.ToString(info.Element("Fund_Name").Value),
                                                              id = Convert.ToString(info.Element("id").Value),
                                                              name = Convert.ToString(info.Element("Name").Value)
                                                          };



                    cd_FundSourceMainDetails.Clear();

                    foreach (var item in _dataFetchFundSourceMainDetails)
                    {
                        ChiefData_FundSourceMainDetails _varData = new ChiefData_FundSourceMainDetails();

                        _varData.allocation = Convert.ToDouble(item.allocation).ToString("#,##0.00");
                        _varData.fund_name = item.fund_name;
                        _varData.id = item.id;
                        _varData.name = item.name;

                        cd_FundSourceMainDetails.Add(_varData);
                    }


                    cd_FundSource.Clear();

                    foreach (var item in cd_FundSourceMain)
                    {
                        List<ChiefData_FundSourceMainDetails> x_details = _dataFetchFundSourceMainDetails.Where(x => x.id == item.id).ToList();
                        ChiefData_FundSource x_data = new ChiefData_FundSource();

                        x_data.FundName = item.fund_source;
                        FundSource = item.id;

                        List<Approved_Data> x_check = cd_Approved.Where(x => x.Code == FundSource).ToList();
                        bool isapprove = false;
                        if (x_check.Count!=0)
                        {
                            lblstat.Content = "This Budget has already been Approved.";
                            stkApproved.Visibility = System.Windows.Visibility.Visible;
                            btnApprove.IsEnabled = false;
                            btnApprove.IsEnabled = false;
                            CancelButton.IsEnabled = false;
                            isapprove = true;
                           // break;
                        }
                        else
                        {
                            stkApproved.Visibility = System.Windows.Visibility.Collapsed;
                            btnApprove.IsEnabled = true;
                            btnApprove.IsEnabled = true;
                            CancelButton.IsEnabled = true;
                        }
                        if (ListItemDetails.Count == 0)
                        {
                            if (isapprove==false)
                            {
                                lblstat.Content = "No Submission for this Division / Project.";
                                stkApproved.Visibility = System.Windows.Visibility.Visible;
                                btnApprove.IsEnabled = false;
                                btnApprove.IsEnabled = false;
                                CancelButton.IsEnabled = false;
                                break;
                            }
                            
                        }
                        else
                        {
                            stkApproved.Visibility = System.Windows.Visibility.Collapsed;
                            btnApprove.IsEnabled = true;
                            btnApprove.IsEnabled = true;
                            CancelButton.IsEnabled = true;
                        }

                        foreach (var item_data in x_details)
                        {
                            List<ChiefData_ListExpenditure> x_exp = cd_ExpenditureLists.Where(x => x.fund_source_id == item_data.id && x.expenditure == item_data.name).ToList();
                            List<ChiefData_ItemLists> _lists = ListItemDetails.Where(x => x.expenditure == item_data.name).ToList();

                            if (x_exp.Count != 0)
                            {
                                double _total_exp = 0.00;
                                double _JanTotalDetails = 0.00;
                                double _FebTotalsDetails = 0.00;
                                double _MarTotalsDetails = 0.00;
                                double _AprTotalsDetails = 0.00;
                                double _MayTotalsDetails = 0.00;
                                double _JunTotalsDetails = 0.00;
                                double _JulTotalsDetails = 0.00;
                                double _AugTotalsDetails = 0.00;
                                double _SepTotalsDetails = 0.00;
                                double _OctTotalsDetails = 0.00;
                                double _NovTotalsDetails = 0.00;
                                double _DecTotalsDetails = 0.00;

                                if (_lists.Count!=0)
                                {
                                    
                               
                                foreach (var item_r in x_exp)
                                {
                                    _total_exp += Convert.ToDouble(item_r.total);
                                }
                                _total_exp = 0;
                                foreach (var item_lists in _lists)
                                {

                                    switch (item_lists.months)
                                    {
                                        case "Jan":
                                            _JanTotalDetails += Convert.ToDouble(item_lists.total);
                                            _total_exp += Convert.ToDouble(item_lists.total);
                                            item_data.Jan = _JanTotalDetails.ToString("#,##0.00");
                                            break;
                                        case "Feb":
                                            _FebTotalsDetails += Convert.ToDouble(item_lists.total);
                                            _total_exp += Convert.ToDouble(item_lists.total);
                                            item_data.Feb = _FebTotalsDetails.ToString("#,##0.00");
                                            break;
                                        case "Mar":
                                            _MarTotalsDetails += Convert.ToDouble(item_lists.total);
                                            _total_exp += Convert.ToDouble(item_lists.total);
                                            item_data.Mar = _MarTotalsDetails.ToString("#,##0.00");
                                            break;
                                        case "Apr":
                                            _AprTotalsDetails += Convert.ToDouble(item_lists.total);
                                            _total_exp += Convert.ToDouble(item_lists.total);
                                            item_data.Apr = _AprTotalsDetails.ToString("#,##0.00");
                                            break;
                                        case "May":
                                            _MayTotalsDetails += Convert.ToDouble(item_lists.total);
                                            _total_exp += Convert.ToDouble(item_lists.total);
                                            item_data.May = _MayTotalsDetails.ToString("#,##0.00");
                                            break;
                                        case "Jun":
                                            _JunTotalsDetails += Convert.ToDouble(item_lists.total);
                                            _total_exp += Convert.ToDouble(item_lists.total);
                                            item_data.Jun = _JunTotalsDetails.ToString("#,##0.00");
                                            break;
                                        case "Jul":
                                            _JulTotalsDetails += Convert.ToDouble(item_lists.total);
                                            _total_exp += Convert.ToDouble(item_lists.total);
                                            item_data.Jul = _JulTotalsDetails.ToString("#,##0.00");
                                            break;
                                        case "Aug":
                                            _AugTotalsDetails += Convert.ToDouble(item_lists.total);
                                            _total_exp += Convert.ToDouble(item_lists.total);
                                            item_data.Aug = _AugTotalsDetails.ToString("#,##0.00");
                                            break;
                                        case "Sep":
                                            _SepTotalsDetails += Convert.ToDouble(item_lists.total);
                                            _total_exp += Convert.ToDouble(item_lists.total);
                                            item_data.Sep = _SepTotalsDetails.ToString("#,##0.00");
                                            break;
                                        case "Oct":
                                            _OctTotalsDetails += Convert.ToDouble(item_lists.total);
                                            _total_exp += Convert.ToDouble(item_lists.total);
                                            item_data.Oct = _OctTotalsDetails.ToString("#,##0.00");
                                            break;
                                        case "Nov":
                                            _NovTotalsDetails += Convert.ToDouble(item_lists.total);
                                            _total_exp +=  Convert.ToDouble(item_lists.total);
                                            item_data.Nov = _NovTotalsDetails.ToString("#,##0.00");
                                            break;
                                        case "Dec":
                                            _DecTotalsDetails += Convert.ToDouble(item_lists.total);
                                            _total_exp += Convert.ToDouble(item_lists.total);
                                            item_data.Dec = _DecTotalsDetails.ToString("#,##0.00");
                                            break;

                                    }
                                   
                                }
                               
                                item_data.total_expenditures = _total_exp.ToString("#,##0.00");

                                item_data.total_balance = (Convert.ToDouble(item_data.allocation.ToString()) - _total_exp).ToString("#,##0.00");

                                item_data.allocation = Convert.ToDouble(item_data.allocation).ToString("#,##0.00");
                                item_data.total_budget = (_total_exp + (Convert.ToDouble(item_data.allocation.ToString()) - _total_exp)).ToString("#,##0.00");
                                }
                                else
                                {
                                    item_data.Jan = 0.ToString("#,##0.00");
                                    item_data.Feb = 0.ToString("#,##0.00");
                                    item_data.Mar = 0.ToString("#,##0.00");
                                    item_data.Apr = 0.ToString("#,##0.00");
                                    item_data.May = 0.ToString("#,##0.00");
                                    item_data.Jun = 0.ToString("#,##0.00");
                                    item_data.Jul = 0.ToString("#,##0.00");
                                    item_data.Aug = 0.ToString("#,##0.00");
                                    item_data.Sep = 0.ToString("#,##0.00");
                                    item_data.Oct = 0.ToString("#,##0.00");
                                    item_data.Nov = 0.ToString("#,##0.00");
                                    item_data.Dec = 0.ToString("#,##0.00");
                                    item_data.total_expenditures = 0.ToString("#,##0.00");
                                    item_data.total_budget = (Convert.ToDouble(item_data.allocation.ToString()) - 0).ToString("#,##0.00");
                                    item_data.total_balance = (Convert.ToDouble(item_data.allocation.ToString()) - 0).ToString("#,##0.00");

                                    item_data.allocation = Convert.ToDouble(item_data.allocation).ToString("#,##0.00");
                                }
                            }
                            else
                            {
                                item_data.Jan = 0.ToString("#,##0.00");
                                item_data.Feb = 0.ToString("#,##0.00");
                                item_data.Mar = 0.ToString("#,##0.00");
                                item_data.Apr = 0.ToString("#,##0.00");
                                item_data.May = 0.ToString("#,##0.00");
                                item_data.Jun = 0.ToString("#,##0.00");
                                item_data.Jul = 0.ToString("#,##0.00");
                                item_data.Aug = 0.ToString("#,##0.00");
                                item_data.Sep = 0.ToString("#,##0.00");
                                item_data.Oct = 0.ToString("#,##0.00");
                                item_data.Nov = 0.ToString("#,##0.00");
                                item_data.Dec = 0.ToString("#,##0.00");
                                item_data.total_expenditures = 0.ToString("#,##0.00");
                                item_data.total_budget = (Convert.ToDouble(item_data.allocation.ToString()) - 0).ToString("#,##0.00");
                                item_data.total_balance = (Convert.ToDouble(item_data.allocation.ToString()) - 0).ToString("#,##0.00");

                                item_data.allocation = Convert.ToDouble(item_data.allocation).ToString("#,##0.00");
                            }

                           

                        }
                        ChiefData_FundSourceMainDetails x_space = new ChiefData_FundSourceMainDetails();

                        x_space.name = "-----------------------------------------";
                        x_space.fund_name = "------------";
                        x_space.allocation = "---------------------";
                        x_space.Jan = "------------";
                        x_space.Feb = "------------";
                        x_space.Mar = "------------";
                        x_space.Apr = "------------";
                        x_space.May = "------------";
                        x_space.Jun = "------------";
                        x_space.Jul = "------------";
                        x_space.Aug = "------------";
                        x_space.Sep = "------------";
                        x_space.Oct = "------------";
                        x_space.Nov = "------------";
                        x_space.Dec = "------------";
                        x_space.total_expenditures = "---------------------";
                        x_space.total_balance = "---------------------";
                        x_space.total_budget = "---------------------";
                        x_details.Add(x_space);

                        ChiefData_FundSourceMainDetails x_totals = new ChiefData_FundSourceMainDetails();


                        double _Allocation = 0.00;

                        double _JanTotal = 0.00;
                        double _FebTotals = 0.00;
                        double _MarTotals = 0.00;
                        double _AprTotals = 0.00;
                        double _MayTotals = 0.00;
                        double _JunTotals = 0.00;
                        double _JulTotals = 0.00;
                        double _AugTotals = 0.00;
                        double _SepTotals = 0.00;
                        double _OctTotals = 0.00;
                        double _NovTotals = 0.00;
                        double _DecTotals = 0.00;
                      

                        double _total_expenditures = 0.00;
                        double _total_balance = 0.00;
                        double _total_budgets = 0.00;
                        x_totals.name = "Grand Total";

                        foreach (var item_x in x_details)
                        {
                            try
                            {
                                if (item_x.Jan == null) { item_x.Jan = 0.ToString("#,##0.00"); }
                                if (item_x.Feb == null) { item_x.Feb = 0.ToString("#,##0.00"); }
                                if (item_x.Mar == null) { item_x.Mar = 0.ToString("#,##0.00"); }
                                if (item_x.Apr == null) { item_x.Apr = 0.ToString("#,##0.00"); }
                                if (item_x.May == null) { item_x.May = 0.ToString("#,##0.00"); }
                                if (item_x.Jun == null) { item_x.Jun = 0.ToString("#,##0.00"); }
                                if (item_x.Jul == null) { item_x.Jul = 0.ToString("#,##0.00"); }
                                if (item_x.Aug == null) { item_x.Aug = 0.ToString("#,##0.00"); }
                                if (item_x.Sep == null) { item_x.Sep = 0.ToString("#,##0.00"); }
                                if (item_x.Oct == null) { item_x.Oct = 0.ToString("#,##0.00"); }
                                if (item_x.Nov == null) { item_x.Nov = 0.ToString("#,##0.00"); }
                                if (item_x.Dec == null) { item_x.Dec = 0.ToString("#,##0.00"); }
                                if (item_x.total_expenditures == null) { item_x.total_expenditures = 0.ToString("#,##0.00"); }
                                if (item_x.total_budget == null) { item_x.total_budget = 0.ToString("#,##0.00"); }
                                if (item_x.total_balance == null) { item_x.total_balance = 0.ToString("#,##0.00"); }
                                 
                             
                             _Allocation += Convert.ToDouble(item_x.allocation);
                             _JanTotal += Convert.ToDouble(item_x.Jan);
                             _FebTotals += Convert.ToDouble(item_x.Feb);
                             _MarTotals += Convert.ToDouble(item_x.Mar);
                             _AprTotals += Convert.ToDouble(item_x.Apr);
                             _MayTotals += Convert.ToDouble(item_x.May);
                             _JunTotals += Convert.ToDouble(item_x.Jun);
                             _JulTotals += Convert.ToDouble(item_x.Jul);
                             _AugTotals += Convert.ToDouble(item_x.Aug);
                             _SepTotals += Convert.ToDouble(item_x.Sep);
                             _OctTotals += Convert.ToDouble(item_x.Oct);
                             _NovTotals += Convert.ToDouble(item_x.Nov);
                             _DecTotals += Convert.ToDouble(item_x.Dec);

                             _total_expenditures += Convert.ToDouble(item_x.total_expenditures);
                             _total_balance += Convert.ToDouble(item_x.total_balance);
                             _total_budgets += Convert.ToDouble(item_x.total_budget);
                            }
                            catch (Exception)
                            {
                                
                       
                            }
                       

                        }
                        x_totals.allocation = _Allocation.ToString("#,##0.00");
                        x_totals.Jan = _JanTotal.ToString("#,##0.00");
                        x_totals.Feb = _FebTotals.ToString("#,##0.00");
                        x_totals.Mar = _MarTotals.ToString("#,##0.00");
                        x_totals.Apr = _AprTotals.ToString("#,##0.00");
                        x_totals.May = _MayTotals.ToString("#,##0.00");
                        x_totals.Jun = _JunTotals.ToString("#,##0.00");
                        x_totals.Jul = _JulTotals.ToString("#,##0.00");
                        x_totals.Aug = _AugTotals.ToString("#,##0.00");
                        x_totals.Sep = _SepTotals.ToString("#,##0.00");
                        x_totals.Oct = _OctTotals.ToString("#,##0.00");
                        x_totals.Nov = _NovTotals.ToString("#,##0.00");
                        x_totals.Dec = _DecTotals.ToString("#,##0.00");
                        x_totals.total_expenditures = _total_expenditures.ToString("#,##0.00");
                        x_totals.total_balance = _total_balance.ToString("#,##0.00");
                        x_totals.total_budget = _total_budgets.ToString("#,##0.00");
                        x_details.Add(x_totals);

                        x_data.FundAllocation = x_details;

                        cd_FundSource.Add(x_data);
                    }
                    grdData.ItemsSource = null;
                    grdData.ItemsSource = cd_FundSource;
                   
                    foreach (var item in grdData.Rows)
                    {
                        if (item.HasChildren)
                        {
                            item.IsExpanded = true;
                            foreach (var item_c in item.ChildBands)
                            {
                                item_c.Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
                                item_c.Columns["fund_name"].Visibility = System.Windows.Visibility.Collapsed;
                                item_c.Columns["total_obligated"].Visibility = System.Windows.Visibility.Collapsed;

                                item_c.Columns["name"].HeaderText = "Expense Item";
                                item_c.Columns["allocation"].HeaderText = "Allocation";
                                item_c.Columns["total_expenditures"].HeaderText = "Total Plan";
                                item_c.Columns["total_balance"].HeaderText = "Contingency";

                                item_c.Columns["total_budget"].HeaderText = "Total Budget";
                            }
                        }
                    }
                    SetAlignment();

                    //   FetchFundSourceMain();

                    this.Cursor = Cursors.Arrow;
                    break;

            }
        }

        private void FetchApprovedFinance() 
        {
            c_finance.Process = "FetchApprovedFinance";
            svc_mindaf.ExecuteSQLAsync(c_finance.FetchApprovedFinance());
        }
        private void FetchFundSourceMainDetails(Boolean isUnit)
        {
            //String _desc = grdDivision.Rows[grdDivision.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
            //List<Divisions> x_data = ListDivision.Where(x => x.Division_Desc == _desc).ToList();
            String _desc = "";
            List<Divisions> x_data = new List<Divisions>();
            if (isUnit==false)
            {
                _desc = grdDivision.Rows[grdDivision.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
                x_data = ListDivision.Where(x => x.Division_Desc == _desc).ToList();
            }
            else
            {
                _desc = grdSubUnit.Rows[grdSubUnit.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
                x_data = ListDivisionUnit.Where(x => x.Division_Desc == _desc).ToList();
            }
            String OfficeId = "";

            if (x_data.Count != 0)
            {
                OfficeId = x_data[0].Division_Id;
                c_finance.Process = "FetchFundSourceMainDetails";
                svc_mindaf.ExecuteSQLAsync(c_finance.FetchFundSourceDetails(OfficeId, this.WorkingYear.ToString(),cmbFundSource.SelectedItem.ToString(),cmbExpenditure.SelectedItem.ToString()));
            }

           
        }
        private void FetchExpenditureDetails(Boolean isUnit)
        {
            //String _desc = grdDivision.Rows[grdDivision.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
            //List<Divisions> x_data = ListDivision.Where(x => x.Division_Desc == _desc).ToList();

            String _desc = "";
            List<Divisions> x_data = new List<Divisions>();
            if (isUnit==false)
            {
                _desc = grdDivision.Rows[grdDivision.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
                x_data = ListDivision.Where(x => x.Division_Desc == _desc).ToList();
            }
            else
            {
                _desc = grdSubUnit.Rows[grdSubUnit.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
                x_data = ListDivisionUnit.Where(x => x.Division_Desc == _desc).ToList();
            }

            String OfficeId = "";

            if (x_data.Count != 0)
            {
                OfficeId = x_data[0].Division_Id;
                c_finance.Process = "FetchExpenditureDetails";
               
                svc_mindaf.ExecuteSQLAsync(c_finance.FetchDivisionExpenditureDetailsByDiv(OfficeId, this.WorkingYear.ToString()));
            }
            
        }
        private bool is_loaded;
        private void SubmitSelected()
        {
            is_loaded = false;
            StringBuilder sb = new StringBuilder(260);
            String SQLCommand = "";
            foreach (ChiefData_ItemLists item in ListItemDetails)
            {
                sb.AppendLine(@"UPDATE dbo.mnda_approved_projects_division SET  isapproved = 1 ,user_approve ='"+ this._User.Replace("Welcome","").Trim() +"',date_status ='"+ DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() +"' WHERE  id = " + item.id + ";");
            }
            sb.Append(@"UPDATE dbo.mnda_activity_data SET status = 'FINANCE APPROVED' WHERE fund_source_id ='" + FundSource + "';");
            
            sb.AppendLine(@"INSERT INTO");
            sb.AppendLine(@"  dbo.mnda_approved_finance");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  code,");
            sb.AppendLine(@"  status");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '"+ FundSource +"',");
            sb.AppendLine(@"  'APPROVED'");
            sb.AppendLine(@");");

            SQLCommand= sb.ToString();

            c_finance.Process = "ApproveFinance";
            is_loaded = false;
            svc_mindaf.ExecuteQueryListsCompleted += svc_mindaf_ExecuteQueryListsCompleted;
            svc_mindaf.ExecuteQueryListsAsync(SQLCommand);
        }

        private void SubmitDisapprove()
        {
            is_loaded = false;
            StringBuilder sb = new StringBuilder(260);
            String SQLCommand = "";
            foreach (ChiefData_ItemLists item in ListItemDetails)
            {
                sb.AppendLine(@"UPDATE dbo.mnda_approved_projects_division SET  isapproved = 0 ,user_approve ='" + this._User.Replace("Welcome", "").Trim() + "',date_status ='" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "' WHERE  id = " + item.id + ";");
            }


            SQLCommand = sb.ToString();

            c_finance.Process = "DisapproveFinance";
            is_loaded = false;
            svc_mindaf.ExecuteQueryListsCompleted += svc_mindaf_ExecuteQueryListsCompleted;
            svc_mindaf.ExecuteQueryListsAsync(SQLCommand);
        }

        void svc_mindaf_ExecuteQueryListsCompleted(object sender, ExecuteQueryListsCompletedEventArgs e)
        {
            if (c_finance.Process =="ApproveFinance")
            {
                if (is_loaded == false)
                {
                    if (e.Result == true)
                    {
                        MessageBox.Show("Budget Plan has been approved succesfully");
                        is_loaded = true;
                    }
                }

            }
            else if (c_finance.Process == "DisapproveFinance")
            {
                if (is_loaded == false)
                {
                    if (e.Result == true)
                    {
                        MessageBox.Show("Budget Plan has been disapproved");
                        is_loaded = true;
                    }
                }
            }
        }
        private void ApproveExpenditure()
        {
            //c_finance.Process = "ApproveFinance";
            //c_finance.ApproveFinance(ListItemDetails);
            SubmitSelected();
        }
        private void SetAlignment()
        {
            if (grdData.Rows.Count!=0)
            {
                grdData.Rows[0].ChildBands[0].Cells["Jan"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["Feb"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["Mar"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["Apr"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["May"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["Jun"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["Jul"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["Aug"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["Sep"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["Oct"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["Nov"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["Dec"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["allocation"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["total_expenditures"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["total_balance"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdData.Rows[0].ChildBands[0].Cells["total_budget"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            }
           
        }


        private void FetchExpenditureDetailsItems()
        {
            String _desc = grdDivision.Rows[grdDivision.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
            List<Divisions> x_data = ListDivision.Where(x => x.Division_Desc == _desc).ToList();
            String OfficeId = "";

            if (x_data.Count != 0)
            {
                OfficeId = x_data[0].Division_Id;
                c_finance.Process = "FetchExpenditureDetailsItemLists";
                svc_mindaf.ExecuteSQLAsync(c_finance.FetchDivisionExpenditureDetailsItemByDiv(OfficeId, this.WorkingYear.ToString(), cmbFundSource.SelectedItem.ToString(), cmbExpenditure.SelectedItem.ToString()));
            }

        }

        private void FetchExpenditureDetailsItemsUnit()
        {
            String _desc = grdSubUnit.Rows[grdSubUnit.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
            List<Divisions> x_data = ListDivisionUnit.Where(x => x.Division_Desc == _desc).ToList();
            String OfficeId = "";

            if (x_data.Count != 0)
            {
                OfficeId = x_data[0].Division_Id;
                c_finance.Process = "FetchExpenditureDetailsItemLists";
                svc_mindaf.ExecuteSQLAsync(c_finance.FetchDivisionExpenditureDetailsItemByDiv(OfficeId, this.WorkingYear.ToString(),cmbFundSource.SelectedItem.ToString(),cmbExpenditure.SelectedItem.ToString()));
            }

        }
        private void FetchFundSourceMain(Boolean isUnit)
        {
            String _desc ="";
            List<Divisions> x_data  = new List<Divisions>();
            if (isUnit==false)
            {
                _desc = grdDivision.Rows[grdDivision.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
                x_data = ListDivision.Where(x => x.Division_Desc == _desc).ToList();
            }
            else
            {
                _desc = grdSubUnit.Rows[grdSubUnit.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
                x_data = ListDivisionUnit.Where(x => x.Division_Desc == _desc).ToList();
            }
            String OfficeId = "";
            if (x_data.Count != 0)
            {
                OfficeId = x_data[0].Division_Id;
                c_finance.Process = "FetchFundMain";
                svc_mindaf.ExecuteSQLAsync(c_finance.FetchFundSource(OfficeId, this.WorkingYear.ToString(),cmbFundSource.SelectedItem.ToString(),cmbExpenditure.SelectedItem.ToString()));
            }
          
           
        }

        private void FetchOfficeDetails()
        {
            String _desc = grdDivision.Rows[grdDivision.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
            List<Divisions> x_data = ListDivision.Where(x => x.Division_Desc == _desc).ToList();
            String OfficeId = "";

            if (x_data.Count != 0)
            {
                OfficeId = x_data[0].Division_Id;
                c_finance.Process = "FetchOfficeDetails";
                svc_mindaf.ExecuteSQLAsync(c_finance.FetchOfficeDetails(OfficeId, this.WorkingYear.ToString()));
            }
          
        }
        private void FetchBudgetAllocation()
        {
            String _desc = grdDivision.Rows[grdDivision.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
            List<Divisions> x_data = ListDivision.Where(x => x.Division_Desc == _desc).ToList();
            String OfficeId = "";

            if (x_data.Count != 0)
            {
                OfficeId = x_data[0].Division_Id;
                SelectedYear = this.WorkingYear.ToString();
                c_finance.Process = "FetchBudgetAllocation";
                svc_mindaf.ExecuteSQLAsync(c_finance.FetchBudgetAllocation(OfficeId, SelectedYear));
            }

         
        }
        private void FetchDivisionFundSource()
        {
            String _desc = grdDivision.Rows[grdDivision.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
            List<Divisions> x_data = ListDivision.Where(x => x.Division_Desc == _desc).ToList();
            String OfficeId = "";

            if (x_data.Count != 0)
            {
                OfficeId = x_data[0].Division_Id;
                c_finance.Process = "FetchDivisionFundSource";
                svc_mindaf.ExecuteSQLAsync(c_finance.FetchDivisionFundSource(OfficeId, this.WorkingYear.ToString()));
            }

           
        }
        private void FetchData()
        {
            c_finance.Process = "FetchData";
            svc_mindaf.ExecuteSQLAsync(c_finance.LoadPAP(this.WorkingYear));
        }
        private void LoadPAP() 
        {
            c_finance.Process = "LoadPAP";
            svc_mindaf.ExecuteSQLAsync(c_finance.LoadPAP(this.WorkingYear));
        }
        private void LoadSubPAP()
        {
            c_finance.Process = "LoadSubPAP";
            List<OfficesPAP> x_search = ListOffice.Where(x => x.DBM_Sub_Pap_Desc == cmbPAP.SelectedItem.ToString()).ToList();
            String _id = "";
            if (x_search.Count!=0)
            {
                _id = x_search[0].DBM_Sub_Pap_id;
            }
            svc_mindaf.ExecuteSQLAsync(c_finance.LoadSubPAP(_id));
        }
        private void LoadDivision() 
        {
            List<SubPAP_Division> data = ListSubPap.Where(x => (x.PAP +" - " + x.Description).ToString() == cmbSubPap.SelectedItem.ToString()).ToList();
            String Code = "";
            if (data.Count!=0)
            {
               Code = data[0].PAP;
            }
            c_finance.Process = "LoadDivision";
            svc_mindaf.ExecuteSQLAsync(c_finance.LoadDivision(Code,this.WorkingYear.ToString()));

        }
        private void LoadFundSource()
        {

            c_finance.Process = "LoadFundSource";
            svc_mindaf.ExecuteSQLAsync(c_finance.LoadFundSource());

        }
        private void LoadDivisionUnit()
        {
            String _desc = grdDivision.Rows[grdDivision.ActiveCell.Row.Index].Cells["Division_Desc"].Value.ToString();
            List<Divisions> data = ListDivision.Where(x => x.Division_Desc.ToString() == _desc).ToList();
            String Code = "";
            if (data.Count != 0)
            {
                Code = data[0].Division_Id;
            }
            c_finance.Process = "LoadDivisionUnit";
            svc_mindaf.ExecuteSQLAsync(c_finance.LoadDivisionUnit(Code,this.WorkingYear));

          

        }


        private void LoadModeExpenditure() 
        {
            cmbExpenditure.Items.Clear();

            cmbExpenditure.Items.Add("MOOE");
            //cmbExpenditure.Items.Add("CO");
            //cmbExpenditure.Items.Add("PS");
        }
        private void GenerateYear()
        {
            //int _year = DateTime.Now.Year;
            //int _limit = _year + 100;

            //for (int i = _year; i != _limit; i++)
            //{
            //    cmbYear.Items.Add(_year);
            //    _year += 1;
            //}

            //cmbYear.SelectedIndex = 0;

            lblYear.Content = this.WorkingYear;
            LoadPAP();

        }
      
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            ApproveExpenditure();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            SubmitDisapprove();
        }

        private void frmFinApp_Loaded(object sender, RoutedEventArgs e)
        {
            GenerateYear();
        }

        private void cmbPAP_DropDownClosed(object sender, EventArgs e)
        {
            grdDivision.ItemsSource = null;
            grdSubUnit.ItemsSource = null;
            grdData.ItemsSource = null;
            cmbExpenditure.SelectedIndex = -1;
           // cmbDivision.SelectedIndex = -1;
            LoadSubPAP();
            //LoadDivision();
        }

        private void cmbExpenditure_DropDownClosed(object sender, EventArgs e)
        {
            LoadDivision();
            
           
        }

        public string SelectedYear { get; set; }

        public string _User { get; set; }

        //private void cmbDivision_DropDownClosed(object sender, EventArgs e)
        //{
        //    grdData.ItemsSource = null;
        //    cmbExpenditure.SelectedIndex = -1;
        //}

        private void btnhideShow_Click(object sender, RoutedEventArgs e)
        {
            switch (btnhideShow.Content.ToString())
            {
                case "Hide Months":
                    grdData.Rows[0].ChildBands[0].Columns["Jan"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Rows[0].ChildBands[0].Columns["Feb"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Rows[0].ChildBands[0].Columns["Mar"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Rows[0].ChildBands[0].Columns["Apr"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Rows[0].ChildBands[0].Columns["May"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Rows[0].ChildBands[0].Columns["Jun"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Rows[0].ChildBands[0].Columns["Jul"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Rows[0].ChildBands[0].Columns["Aug"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Rows[0].ChildBands[0].Columns["Sep"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Rows[0].ChildBands[0].Columns["Oct"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Rows[0].ChildBands[0].Columns["Nov"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Rows[0].ChildBands[0].Columns["Dec"].Visibility = System.Windows.Visibility.Collapsed;
                    btnhideShow.Content = "Show Months";
                    break;
                case "Show Months":
                    grdData.Rows[0].ChildBands[0].Columns["Jan"].Visibility = System.Windows.Visibility.Visible;
                    grdData.Rows[0].ChildBands[0].Columns["Feb"].Visibility = System.Windows.Visibility.Visible;
                    grdData.Rows[0].ChildBands[0].Columns["Mar"].Visibility = System.Windows.Visibility.Visible;
                    grdData.Rows[0].ChildBands[0].Columns["Apr"].Visibility = System.Windows.Visibility.Visible;
                    grdData.Rows[0].ChildBands[0].Columns["May"].Visibility = System.Windows.Visibility.Visible;
                    grdData.Rows[0].ChildBands[0].Columns["Jun"].Visibility = System.Windows.Visibility.Visible;
                    grdData.Rows[0].ChildBands[0].Columns["Jul"].Visibility = System.Windows.Visibility.Visible;
                    grdData.Rows[0].ChildBands[0].Columns["Aug"].Visibility = System.Windows.Visibility.Visible;
                    grdData.Rows[0].ChildBands[0].Columns["Sep"].Visibility = System.Windows.Visibility.Visible;
                    grdData.Rows[0].ChildBands[0].Columns["Oct"].Visibility = System.Windows.Visibility.Visible;
                    grdData.Rows[0].ChildBands[0].Columns["Nov"].Visibility = System.Windows.Visibility.Visible;
                    grdData.Rows[0].ChildBands[0].Columns["Dec"].Visibility = System.Windows.Visibility.Visible;
                    btnhideShow.Content = "Hide Months";
                    break;
            }
        }

        private void cmbSubPap_DropDownClosed(object sender, EventArgs e)
        {
            grdDivision.ItemsSource = null;
            grdSubUnit.ItemsSource = null;
            grdData.ItemsSource = null;
            stkApproved.Visibility = System.Windows.Visibility.Collapsed;
            LoadFundSource();
        }

        private void cmbDivision_DropDownClosed(object sender, EventArgs e)
        {

        }

        private void grdData_CellClicked(object sender, CellClickedEventArgs e)
        {
          
        }

        private void grdDivision_CellClicked(object sender, CellClickedEventArgs e)
        {
            LoadDivisionUnit();
            isUnit = false;
        }

        private void grdSubUnit_CellClicked(object sender, CellClickedEventArgs e)
        {
            isUnit = true;
            FetchExpenditureDetailsItemsUnit();
            
        }

        private void cmbFundSource_DropDownClosed(object sender, EventArgs e)
        {
           // LoadDivision();
        }

        private void btnConso_Click(object sender, RoutedEventArgs e)
        {
            HtmlPage.Window.Navigate(new Uri("http://mindafinance/help/verifier/FinanceVerifier.application"), "_blank");
        }
    }
}

