﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmRevision : ChildWindow
    {
        public event EventHandler ReloadData;
        public String FROMUACS { get; set; }
        public String ExpenseItem { get; set; }
        public String Amount { get; set; }


        public String UACS { get; set; }
        public String ExpenseItemSelected { get; set; }
        public String AdjustedAmount { get; set; }

        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsppmp_realign c_ppmp = new clsppmp_realign();
        private List<BudgetReviseExpenditureItems> ExpenItems = new List<BudgetReviseExpenditureItems>();

        public frmRevision()
        {
            InitializeComponent();
          
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_ppmp.Process)
            {
                case "FetchExpenditureItems":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new BudgetReviseExpenditureItems
                                     {
                                         name = Convert.ToString(info.Element("name").Value),
                                         uacs_code = Convert.ToString(info.Element("uacs_code").Value)

                                     };

                    ExpenItems.Clear();
                    cmbExpense.Items.Clear();
                    foreach (var item in _dataLists)
                    {

                        BudgetReviseExpenditureItems _varProf = new BudgetReviseExpenditureItems();

                        _varProf.name = item.name;
                        _varProf.uacs_code = item.uacs_code;

                        if (item.name!=lblExpenseItem.Content.ToString())
                        {
                            ExpenItems.Add(_varProf);
                            cmbExpense.Items.Add(item.name);
                        }


                    }


                    this.Cursor = Cursors.Arrow;
                   
                    break;
            }
        }

        private void FetchExpenditureItems() 
        {
            c_ppmp.Process = "FetchExpenditureItems";
            svc_mindaf.ExecuteSQLAsync(c_ppmp.FetchExpenditureItems());
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void frmAdjustment_Loaded(object sender, RoutedEventArgs e)
        {
            lblExpenseItem.Content = ExpenseItem;
            lblAllocation.Value = Amount;
            FetchExpenditureItems();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            double _amountAlloc = Convert.ToDouble(lblAllocation.Text);
            double _amountTrans = Convert.ToDouble(txtAmount.Text);

            if (_amountTrans > _amountAlloc)
            {
                MessageBox.Show("Cannot Add entry , Invalid Amount");}
            else
	            {
                    if (ReloadData != null)
                    {
                        List<BudgetReviseExpenditureItems> _selected = ExpenItems.Where(x => x.name == cmbExpense.SelectedItem.ToString()).ToList();
                        if (_selected.Count != 0)
                        {
                            UACS = _selected[0].uacs_code;
                            ExpenseItemSelected = _selected[0].name;
                            AdjustedAmount = txtAmount.Value.ToString();
                        }
                        ReloadData(this, new EventArgs());
                    }
                    this.DialogResult = true;
	            }
            
           
        }
    }
}

