﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using MinDAF.Usercontrol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmBudgetSubscription : ChildWindow
    {
        public String DivisionId { get; set; }
        public String AccountableID { get; set; }
        public String MOOE_ID { get; set; }
        public String MOOE_INDEX { get; set; }
        public String ActivityID { get; set; }
        public String _Year { get; set; }
        public String _Month { get; set; }
        private List<String> ListId = new List<String>();
        public String FundSource { get; set; }
        public event EventHandler ReloadData;
        public Boolean IsRevision { get; set; }
        private List<GridData> ListGridData = new List<GridData>();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsBudgetSubscription c_dues = new clsBudgetSubscription();
        List<MonthData> _Months = new List<MonthData>();

        public frmBudgetSubscription()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }
        private Double ComputeTotal()
        {
            double _Total = 0.00;
            foreach (var item in _Months)
            {
                _Total += Convert.ToDouble(item.Jan) + Convert.ToDouble(item.Feb) + Convert.ToDouble(item.Mar) + Convert.ToDouble(item.Apr) +
                    Convert.ToDouble(item.May) + Convert.ToDouble(item.Jun) + Convert.ToDouble(item.Jul) + Convert.ToDouble(item.Aug) +
                    Convert.ToDouble(item.Sep) + Convert.ToDouble(item.Oct) + Convert.ToDouble(item.Nov) + Convert.ToDouble(item.Dec);
            }

            return _Total;
        }
        private BudgetRunningBalance _budget_bal { get; set; }
        private void LoadBudgetBalance()
        {
            BudgetRunningBalance _budget_bal = new BudgetRunningBalance(ComputeTotal(), "Subscription Expense", MOOE_ID,MOOE_INDEX);
            _budget_bal._DivisionID = DivisionId;
            _budget_bal._Year = this._Year;
            _budget_bal.WorkingYear = this._Year;
            _budget_bal._FundSource = this.FundSource;
            grdBR.Children.Clear();
            grdBR.Children.Add(_budget_bal);
        }
        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_dues.Process)
            {
                case "FetchFrequency":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new FrequencyType
                                     {

                                         item_name = Convert.ToString(info.Element("item_name").Value)

                                     };


                    List<FrequencyType> _ComboList = new List<FrequencyType>();

                    foreach (var item in _dataLists)
                    {
                        FrequencyType _varProf = new FrequencyType();


                        _varProf.item_name = item.item_name;

                        _ComboList.Add(_varProf);

                    }
                    cmbFrequency.ItemsSource = _ComboList;
                    this.Cursor = Cursors.Arrow;
                    GetGridData();
                    break;
                case "FetchGridData":
                    XDocument oDocKeyFetchGridData = XDocument.Parse(_results);
                    var _dataListsFetchGridData = from info in oDocKeyFetchGridData.Descendants("Table")
                                                  select new SubsData
                                                  {
                                                      ActId = Convert.ToString(info.Element("ActId").Value),
                                                      month = Convert.ToString(info.Element("month").Value),
                                                      rate = Convert.ToString(info.Element("rate").Value)
                                                  };

                    Boolean _HasData = false;

                    foreach (var item in _dataListsFetchGridData)
                    {

                        _HasData = true;
                        _Months[0].ActId = item.ActId;
                        ListId.Add(item.ActId);
                        switch (item.month)
                        {
                            case "Jan":
                                _Months[0].Jan = item.rate;
                                break;
                            case "Feb":
                                _Months[0].Feb = item.rate;
                                break;
                            case "Mar":
                                _Months[0].Mar = item.rate;
                                break;
                            case "Apr":
                                _Months[0].Apr = item.rate;
                                break;
                            case "May":
                                _Months[0].May = item.rate;
                                break;
                            case "Jun":
                                _Months[0].Jun = item.rate;
                                break;
                            case "Jul":
                                _Months[0].Jul = item.rate;
                                break;
                            case "Aug":
                                _Months[0].Aug = item.rate;
                                break;
                            case "Sep":
                                _Months[0].Sep = item.rate;
                                break;
                            case "Oct":
                                _Months[0].Oct = item.rate;
                                break;
                            case "Nov":
                                _Months[0].Nov = item.rate;
                                break;
                            case "Dec":
                                _Months[0].Dec = item.rate;
                                break;

                        }
                        if (_HasData)
                        {
                            lblForTheYear.Content = "Subscription For The Year : " + this._Year;
                        }
                        else
                        {
                            lblForTheYear.Content = "";
                        }

                    }
                    grdData.ItemsSource = null;
                    grdData.ItemsSource = _Months;
                    LoadBudgetBalance();
                    try
                    {
                        grdData.Columns["ActId"].Visibility = System.Windows.Visibility.Collapsed;
                    }
                    catch (Exception)
                    {

                    }
                    this.Cursor = Cursors.Arrow;
                    break;
            }
        }
        private void GetGridData()
        {
            GenerateData();
            c_dues.Process = "FetchGridData";
            svc_mindaf.ExecuteSQLAsync(c_dues.FetchLocalData(this.ActivityID, this._Year,this.FundSource));
        }
        private void SaveData()
        {
            String isRev = "0";
            String procureChoice = "0";
            if (IsRevision)
            {
                isRev = "1";
            }

            if (procureRadioBtn.IsChecked == true)
            {
                procureChoice = "1";
            }
            
            var selectedItem = cmbFrequency.SelectedItem as FrequencyType;
            c_dues.Process = "SaveData";
            c_dues.SQLOperation += c_dues_SQLOperation;
            switch (selectedItem.item_name.Trim())
            {
                case "Monthly":
                    c_dues.DeleteProjectDues(this.ActivityID, this.AccountableID, txtRemark.Text, "0", Convert.ToDouble(txtRate.Value), Convert.ToDouble(txtRate.Value), "Jan", _Year, MOOE_INDEX, "", selectedItem.item_name);

                    c_dues.SaveProjectDues(this.ActivityID, this.AccountableID, txtRemark.Text, "0", Convert.ToDouble(txtRate.Value), Convert.ToDouble(txtRate.Value), "Jan", _Year, MOOE_INDEX, "", selectedItem.item_name, this.FundSource, isRev, procureChoice);

                    c_dues.SaveProjectDues(this.ActivityID, this.AccountableID, txtRemark.Text, "0", Convert.ToDouble(txtRate.Value), Convert.ToDouble(txtRate.Value), "Feb", _Year, MOOE_INDEX, "", selectedItem.item_name, this.FundSource, isRev, procureChoice);

                    c_dues.SaveProjectDues(this.ActivityID, this.AccountableID, txtRemark.Text, "0", Convert.ToDouble(txtRate.Value), Convert.ToDouble(txtRate.Value), "Mar", _Year, MOOE_INDEX, "", selectedItem.item_name, this.FundSource, isRev, procureChoice);

                    c_dues.SaveProjectDues(this.ActivityID, this.AccountableID, txtRemark.Text, "0", Convert.ToDouble(txtRate.Value), Convert.ToDouble(txtRate.Value), "Apr", _Year, MOOE_INDEX, "", selectedItem.item_name, this.FundSource, isRev, procureChoice);

                    c_dues.SaveProjectDues(this.ActivityID, this.AccountableID, txtRemark.Text, "0", Convert.ToDouble(txtRate.Value), Convert.ToDouble(txtRate.Value), "May", _Year, MOOE_INDEX, "", selectedItem.item_name, this.FundSource, isRev, procureChoice);

                    c_dues.SaveProjectDues(this.ActivityID, this.AccountableID, txtRemark.Text, "0", Convert.ToDouble(txtRate.Value), Convert.ToDouble(txtRate.Value), "Jun", _Year, MOOE_INDEX, "", selectedItem.item_name, this.FundSource, isRev, procureChoice);

                    c_dues.SaveProjectDues(this.ActivityID, this.AccountableID, txtRemark.Text, "0", Convert.ToDouble(txtRate.Value), Convert.ToDouble(txtRate.Value), "Jul", _Year, MOOE_INDEX, "", selectedItem.item_name, this.FundSource, isRev, procureChoice);

                    c_dues.SaveProjectDues(this.ActivityID, this.AccountableID, txtRemark.Text, "0", Convert.ToDouble(txtRate.Value), Convert.ToDouble(txtRate.Value), "Aug", _Year, MOOE_INDEX, "", selectedItem.item_name, this.FundSource, isRev, procureChoice);

                    c_dues.SaveProjectDues(this.ActivityID, this.AccountableID, txtRemark.Text, "0", Convert.ToDouble(txtRate.Value), Convert.ToDouble(txtRate.Value), "Sep", _Year, MOOE_INDEX, "", selectedItem.item_name, this.FundSource, isRev, procureChoice);

                    c_dues.SaveProjectDues(this.ActivityID, this.AccountableID, txtRemark.Text, "0", Convert.ToDouble(txtRate.Value), Convert.ToDouble(txtRate.Value), "Oct", _Year, MOOE_INDEX, "", selectedItem.item_name, this.FundSource, isRev, procureChoice);

                    c_dues.SaveProjectDues(this.ActivityID, this.AccountableID, txtRemark.Text, "0", Convert.ToDouble(txtRate.Value), Convert.ToDouble(txtRate.Value), "Nov", _Year, MOOE_INDEX, "", selectedItem.item_name, this.FundSource, isRev, procureChoice);

                    c_dues.SaveProjectDues(this.ActivityID, this.AccountableID, txtRemark.Text, "0", Convert.ToDouble(txtRate.Value), Convert.ToDouble(txtRate.Value), "Dec", _Year, MOOE_INDEX, "", selectedItem.item_name, this.FundSource, isRev, procureChoice);

                    break;
                case "Quarterly":
                    c_dues.DeleteProjectDues(this.ActivityID, this.AccountableID, txtRemark.Text, "0", Convert.ToDouble(txtRate.Value), Convert.ToDouble(txtRate.Value), "Jan", _Year, MOOE_INDEX, "", selectedItem.item_name);

                    c_dues.SaveProjectDues(this.ActivityID, this.AccountableID, txtRemark.Text, "0", Convert.ToDouble(txtRate.Value), Convert.ToDouble(txtRate.Value), "Jan", _Year, MOOE_INDEX, "", selectedItem.item_name, this.FundSource, isRev, procureChoice);

                    c_dues.SaveProjectDues(this.ActivityID, this.AccountableID, txtRemark.Text, "0", Convert.ToDouble(txtRate.Value), Convert.ToDouble(txtRate.Value), "Apr", _Year, MOOE_INDEX, "", selectedItem.item_name, this.FundSource, isRev, procureChoice);

                    c_dues.SaveProjectDues(this.ActivityID, this.AccountableID, txtRemark.Text, "0", Convert.ToDouble(txtRate.Value), Convert.ToDouble(txtRate.Value), "Jul", _Year, MOOE_INDEX, "", selectedItem.item_name, this.FundSource, isRev, procureChoice);

                    c_dues.SaveProjectDues(this.ActivityID, this.AccountableID, txtRemark.Text, "0", Convert.ToDouble(txtRate.Value), Convert.ToDouble(txtRate.Value), "Oct", _Year, MOOE_INDEX, "", selectedItem.item_name, this.FundSource, isRev, procureChoice);


                    break;
                case "Semi-Annual":
                    c_dues.DeleteProjectDues(this.ActivityID, this.AccountableID, txtRemark.Text, "0", Convert.ToDouble(txtRate.Value), Convert.ToDouble(txtRate.Value), "Jan", _Year, MOOE_INDEX, "", selectedItem.item_name);

                    c_dues.SaveProjectDues(this.ActivityID, this.AccountableID, txtRemark.Text, "0", Convert.ToDouble(txtRate.Value), Convert.ToDouble(txtRate.Value), "Jan", _Year, MOOE_INDEX, "", selectedItem.item_name, this.FundSource, isRev, procureChoice);


                    c_dues.SaveProjectDues(this.ActivityID, this.AccountableID, txtRemark.Text, "0", Convert.ToDouble(txtRate.Value), Convert.ToDouble(txtRate.Value), "Jul", _Year, MOOE_INDEX, "", selectedItem.item_name, this.FundSource, isRev, procureChoice);


                    break;
                case "Annual":
                    c_dues.DeleteProjectDues(this.ActivityID, this.AccountableID, txtRemark.Text, "0", Convert.ToDouble(txtRate.Value), Convert.ToDouble(txtRate.Value), "Jan", _Year, MOOE_INDEX, "", selectedItem.item_name);

                    c_dues.SaveProjectDues(this.ActivityID, this.AccountableID, txtRemark.Text, "0", Convert.ToDouble(txtRate.Value), Convert.ToDouble(txtRate.Value), "Jan", _Year, MOOE_INDEX, "", selectedItem.item_name, this.FundSource, isRev, procureChoice);

                    break;
                default:
                    break;
            }
        }

        void c_dues_SQLOperation(object sender, EventArgs e)
        {
            GetGridData();
        }

        private void GenerateData()
        {
            _Months.Clear();

            MonthData _monthdata = new MonthData();

            _monthdata.Jan = "0.00";
            _monthdata.Feb = "0.00";
            _monthdata.Mar = "0.00";
            _monthdata.Apr = "0.00";
            _monthdata.Jun = "0.00";
            _monthdata.Jul = "0.00";
            _monthdata.Aug = "0.00";
            _monthdata.Sep = "0.00";
            _monthdata.Oct = "0.00";
            _monthdata.Nov = "0.00";
            _monthdata.Dec = "0.00";
            _monthdata.May = "0.00";

            _Months.Add(_monthdata);

            grdData.ItemsSource = null;
            grdData.ItemsSource = _Months;

        }

        private void FetchFrequency()
        {
            c_dues.Process = "FetchFrequency";
            svc_mindaf.ExecuteSQLAsync(c_dues.FetchFrequency());
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {

            if (_budget_bal.BalanceOff < Convert.ToDouble(txtRate.Value))
            {
                frmNotifyBalance fBa = new frmNotifyBalance();

                fBa.Show();
            }
            else
            {
                SaveData();
            }
        }

        private void frm_b_subs_Closed(object sender, EventArgs e)
        {
            if (ReloadData!=null)
            {
                ReloadData(this, new EventArgs());
            }
        }

        private void frm_b_subs_Loaded(object sender, RoutedEventArgs e)
        {
            GenerateData();
            FetchFrequency();
        }

        private void SuspendActivity()
        {
            String _id = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["ActId"].Value.ToString();
            c_dues.Process = "Suspend";
            c_dues.SQLOperation+=c_dues_SQLOperation;
            c_dues.UpdateSuspend(ListId, "1");

        }
        private void btnSuspend_Click(object sender, RoutedEventArgs e)
        {
            SuspendActivity();
        }
    }
}

