﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmPersonnelManagement : ChildWindow
    {
        public String DivisionId { get; set; }
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsPersonnelManagement c_personnel = new clsPersonnelManagement();
        private clsDivisionUnit c_div = new clsDivisionUnit();
        private List<PersonnelList> ListPersonnel = new List<PersonnelList>();
        private List<DivisionUnit> ListUnits = new List<DivisionUnit>();

        public frmPersonnelManagement()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            c_personnel.SQLOperation += c_personnel_SQLOperation;
        }

        void c_personnel_SQLOperation(object sender, EventArgs e)
        {
            switch (c_personnel.Process)
            {
                case "SaveData":
                    FetchPersonnel();
                    ClearData();
                    ControlStatus(false);
                    break;
                case "UpdateData":
                    FetchPersonnel();
                    ClearData();
                    ControlStatus(false);
                    break;

            }
        }
       
        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
             var _results = e.Result.ToString();
             switch (c_personnel.Process)
             {
                 case "FetchPersonnel":
                     XDocument oDocKeyResults = XDocument.Parse(_results);
                     var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                      select new PersonnelList
                                      {
                                          Division_Unit = Convert.ToString(info.Element("Division_Unit").Value),
                                          Division_Id = Convert.ToString(info.Element("Division_Id").Value),
                                          is_admin = Convert.ToString(info.Element("is_admin").Value),
                                          Password = Convert.ToString(info.Element("Password").Value),
                                          status = Convert.ToString(info.Element("status").Value),
                                          User_Contact = Convert.ToString(info.Element("User_Contact").Value),
                                          User_Fullname = Convert.ToString(info.Element("User_Fullname").Value),
                                          User_Id = Convert.ToString(info.Element("User_Id").Value),
                                          User_Position = Convert.ToString(info.Element("User_Position").Value),
                                          Username = Convert.ToString(info.Element("Username").Value),

                                      };

                     ListPersonnel.Clear();
                    

                     foreach (var item in _dataLists)
                     {
                         PersonnelList _varDetails = new PersonnelList();

                         _varDetails.Division_Id = item.Division_Id;
                         _varDetails. is_admin= item.is_admin;
                         _varDetails.Password = item.Password;
                         _varDetails.status = item.status;
                          _varDetails.User_Contact = item.User_Contact;
                         _varDetails.User_Fullname = item.User_Fullname;
                         _varDetails.User_Id = item.User_Id;
                           _varDetails.User_Position = item.User_Position;
                           _varDetails.Username = item.Username;
                           _varDetails.Division_Unit = item.Division_Unit;

                         ListPersonnel.Add(_varDetails);
                     }


                     grdData.ItemsSource = null;
                     grdData.ItemsSource = ListPersonnel;
                     grdData.Columns["Division_Id"].Visibility = System.Windows.Visibility.Collapsed;
                     grdData.Columns["User_Id"].Visibility = System.Windows.Visibility.Collapsed;
                     this.Cursor = Cursors.Arrow;
                     FetchDivisionUnits();
                     break;
                 case "FetchDivisionUnits":
                     XDocument oDocKeyResultsFetchDivisionUnits = XDocument.Parse(_results);
                     var _dataListsFetchDivisionUnits = from info in oDocKeyResultsFetchDivisionUnits.Descendants("Table")
                                      select new DivisionUnit
                                      {
                                          Code = Convert.ToString(info.Element("unit_code").Value),
                                          Name = Convert.ToString(info.Element("unit_name").Value)

                                      };

                     ListUnits.Clear();
                     cmbDivisionUnit.Items.Clear();

                     foreach (var item in _dataListsFetchDivisionUnits)
                     {
                         DivisionUnit _varDetails = new DivisionUnit();

                         _varDetails.Code = item.Code;
                         _varDetails.Name = item.Name;

                         cmbDivisionUnit.Items.Add(item.Name);

                         ListUnits.Add(_varDetails);
                     }

                     this.Cursor = Cursors.Arrow;

                     break;
             }
        }

        private void FetchDivisionUnits() 
        {
            c_personnel.Process = "FetchDivisionUnits";           
            svc_mindaf.ExecuteSQLAsync(c_personnel.FetchDivisionUnit(this.DivisionId));
        }
        private void GenerateRole() 
        {
            cmbRole.Items.Clear();
            cmbRole.Items.Add("Division Head");
            cmbRole.Items.Add("User");
        }
        private void FetchPersonnel() 
        {
            c_personnel.Process = "FetchPersonnel";
            svc_mindaf.ExecuteSQLAsync(c_personnel.FetchPersonnelLists(this.DivisionId));
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void LoadSelected() 
        {
            txtContact.Text = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Contact"].Value.ToString();
            txtFullName.Text = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
            txtPassword.Text = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["Password"].Value.ToString();
            txtPosition.Text = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Position"].Value.ToString();
            txtUsername.Text = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["Username"].Value.ToString();

            if (grdData.Rows[grdData.ActiveCell.Row.Index].Cells["is_admin"].Value.ToString() == "2")
            {
                cmbRole.SelectedIndex = 0;
            }
            else
            {
                cmbRole.SelectedIndex = 1;
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void ControlStatus(Boolean _stat) 
        {
            txtContact.IsEnabled = _stat;
            txtFullName.IsEnabled = _stat;
            txtPassword.IsEnabled = _stat;
            txtPosition.IsEnabled = _stat;
            txtUsername.IsEnabled = _stat;
            cmbRole.IsEnabled = _stat;
            cmbDivisionUnit.IsEnabled = _stat;
        }

        private void grdData_Loaded(object sender, RoutedEventArgs e)
        {
            FetchPersonnel();
            GenerateRole();
            ControlStatus(false);
        }
        private void ClearData() 
        {
            txtContact.Text = "";
            txtFullName.Text = "";
            txtPassword.Text = "";
            txtPosition.Text = "";
            txtUsername.Text = "";
            cmbRole.SelectedIndex = -1;

        }
        private void SaveData() 
        {
           String Contact =txtContact.Text;
           String FullName = txtFullName.Text;
           String Password = txtPassword.Text;
           String Position = txtPosition.Text;
           String Username = txtUsername.Text;
           String Role = "";
           String UserCode = "";
            if (cmbRole.SelectedItem.ToString() == "Division Head")
	        {
                Role = "2";
            }
            else
            {
                Role = "0";
            }
            if (cmbDivisionUnit.SelectedItem.ToString()=="")
            {
                UserCode = "-";
            }  
            else
            {
                UserCode = cmbDivisionUnit.SelectedItem.ToString();
                List<DivisionUnit> x = ListUnits.Where(xx => xx.Name == UserCode).ToList();
                if (x.Count!=0)
                {
                    UserCode = x[0].Code;
                }
            }

            c_personnel.Process = "SaveData";
            c_personnel.SavePersonnel(this.DivisionId,FullName,Contact,Position,Username,Password,"registered",Role,UserCode);
        }
        private void UpdateData(String UserId,String Division_Id, String User_Fullname, String User_Contact, String User_Position,
       String Username, String Password, String status, String is_admin) 
        {
          
            c_personnel.Process = "UpdateData";
            c_personnel.SaveUpdatePersonnel(UserId, this.DivisionId, User_Fullname, User_Contact, User_Position, Username, Password, status, is_admin);
        }
        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            switch (btnNew.Content.ToString())
            {
                case "New":
                    btnNew.Content = "Add";
                    ControlStatus(true);
                    ClearData();
                    txtFullName.Focus();
                    break;
                case "Add":
                    btnNew.Content = "New";
                    SaveData();
                    break;
            }
        }

        private void grdData_CellClicked(object sender, Infragistics.Controls.Grids.CellClickedEventArgs e)
        {
            LoadSelected();
       
        }

        private void grdData_CellExitedEditMode(object sender, Infragistics.Controls.Grids.CellExitedEditingEventArgs e)
        {
            String UserId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Id"].Value.ToString();
            String Contact = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Contact"].Value.ToString();
            String FullName = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
            String Password = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["Password"].Value.ToString();
            String Position = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Position"].Value.ToString();
            String Username = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["Username"].Value.ToString();
            String Role = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["is_admin"].Value.ToString();
            
            UpdateData(UserId,this.DivisionId,FullName,Contact,Position,Username,Password,"registered",Role);
        }

       
    }
}

