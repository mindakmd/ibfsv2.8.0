﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmRAFMonitoring : ChildWindow
    {
        public String WorkingYear { get; set; }
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsRafList c_raf = new clsRafList();
        private List<RafMonitor> ListRaf = new List<RafMonitor>();
        private List<RafDetails> ListRafDetails = new List<RafDetails>();
        public frmRAFMonitoring()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            this.Loaded += frmRAFMonitoring_Loaded;
        }

        void frmRAFMonitoring_Loaded(object sender, RoutedEventArgs e)
        {
            FetchRAFData();
        }

        private void FetchRAFData()
        {
            c_raf.Process = "FetchRAFData";
            svc_mindaf.ExecuteSQLAsync(c_raf.GetRafMonitoringData());
        }
        private void FetchRAFDetails()
        {
            c_raf.Process = "FetchRAFDetails";
            svc_mindaf.ExecuteSQLAsync(c_raf.GetRafDetails());
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_raf.Process)
            {

                case "FetchRAFData":
                    XDocument oDocKeyFetchExpDetails = XDocument.Parse(_results);
                    var _dataListsFetchExpDetails = from info in oDocKeyFetchExpDetails.Descendants("Table")
                                                    select new RafMonitor
                                                    {
                                                       
                                                        raf_no = Convert.ToString(info.Element("raf_no").Value),
                                                        respo_center = Convert.ToString(info.Element("respo_center").Value),
                                                        status = Convert.ToString(info.Element("status").Value),
                                                    };

                    ListRaf.Clear();


                    foreach (var item in _dataListsFetchExpDetails)
                    {
                        RafMonitor _varDetails = new RafMonitor();


                        _varDetails.raf_no = item.raf_no;
                        _varDetails.respo_center = item.respo_center;
                        _varDetails.status = item.status;


                        ListRaf.Add(_varDetails);

                    }

                    FetchRAFDetails();
                    this.Cursor = Cursors.Arrow;

                    break;
                case "FetchRAFDetails":
                    XDocument oDocKeyFetchRAFDetails = XDocument.Parse(_results);
                    var _dataListsFetchRAFDetails = from info in oDocKeyFetchRAFDetails.Descendants("Table")
                                                    select new RafDetails
                                                    {
                                                       alignment_id  = Convert.ToString(info.Element("alignment_id").Value),
                                                       cancelled  = Convert.ToString(info.Element("cancelled").Value),
                                                       Division_Desc  = Convert.ToString(info.Element("Division_Desc").Value),
                                                       is_approved  = Convert.ToString(info.Element("is_approved").Value),
                                                        raf_no = Convert.ToString(info.Element("raf_no").Value),
                                                    };

                    ListRafDetails.Clear();


                    foreach (var item in _dataListsFetchRAFDetails)
                    {
                        RafDetails _varDetails = new RafDetails();

                        _varDetails.alignment_id =item.alignment_id;
                        _varDetails.cancelled = item.cancelled;
                        _varDetails.Division_Desc = item.Division_Desc;
                        _varDetails.is_approved = item.is_approved;
                        _varDetails.raf_no = item.raf_no;

                        ListRafDetails.Add(_varDetails);

                    }

                   

                    grdData.ItemsSource = null;
                    grdData.ItemsSource = ListRaf;

                    this.Cursor = Cursors.Arrow;

                    break;
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

