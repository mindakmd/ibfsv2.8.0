﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Forms
{
    public partial class frmNewPap : ChildWindow
    {
        public String WorkingYear { get; set; }

        public event EventHandler ReloadData;

        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsPAP c_pap = new clsPAP();
        public frmNewPap()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void SaveData() 
        {
            c_pap.Process = "SaveNewPap";
            c_pap.SQLOperation += c_pap_SQLOperation;
            c_pap.SaveNewPap(txtPapCode.Text, txtPapName.Text, txtAccro.Text, this.WorkingYear);
        }

        void c_pap_SQLOperation(object sender, EventArgs e)
        {
            switch (c_pap.Process)
            {
                case "SaveNewPap":
                    if (ReloadData!=null)
                    {
                        ReloadData(this, new EventArgs());

                    }
                    break;
                default:
                    break;
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
            this.DialogResult = true;
        }
    }
}

