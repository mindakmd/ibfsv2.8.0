﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmRAFList : ChildWindow
    {
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsRafList c_raf = new clsRafList();
        private List<RafData> ListRaf = new List<RafData>();
        public String DivId {get;set;}
        public String RafDescription { get; set; }
        public frmRAFList()
        {
           
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            this.Loaded += frmRAFList_Loaded;
        }

        void frmRAFList_Loaded(object sender, RoutedEventArgs e)
        {
            FetchRAFData();
        }

        private void FetchRAFData() 
        {
            c_raf.Process = "FetchRAFData";
            svc_mindaf.ExecuteSQLAsync(c_raf.GetRafData(this.DivId));
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_raf.Process)
            {
                
                case "FetchRAFData":
                    XDocument oDocKeyFetchExpDetails = XDocument.Parse(_results);
                    var _dataListsFetchExpDetails = from info in oDocKeyFetchExpDetails.Descendants("Table")
                                                    select new RafData
                                                    {
                                                        Description = Convert.ToString(info.Element("").Value),
                                                      Id   = Convert.ToString(info.Element("").Value),
                                                      RafNo   = Convert.ToString(info.Element("").Value),
                                                       Status  = Convert.ToString(info.Element("").Value),
                                                    };

                    ListRaf.Clear();


                    foreach (var item in _dataListsFetchExpDetails)
                    {
                        RafData _varDetails = new RafData();

                        _varDetails.Description = item.Description;
                        _varDetails.Id = item.Id;
                        _varDetails.RafNo = item.RafNo;
                        _varDetails.Status = item.Status;
                       

                        ListRaf.Add(_varDetails);

                    }

                    grdData.ItemsSource = null;
                    grdData.ItemsSource = ListRaf;
                    grdData.Columns["Id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["Status"].Visibility = System.Windows.Visibility.Collapsed;
                    this.Cursor = Cursors.Arrow;

                    break;
            }
        }
     
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

