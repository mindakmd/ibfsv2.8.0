﻿using Infragistics.Controls.Grids;
using MinDAF.Class;
using MinDAF.MinDAFS;
using MinDAF.Usercontrol.Budget_Allocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmPAFManager : ChildWindow
    {
        public String OfficeId { get; set; }
        public String DivisionId { get; set; }

        public string WorkingYear { get; set; }

        private clsDivisionBudgetAllocation c_div = new clsDivisionBudgetAllocation();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();

        private List<lib_SUB_PAP> ListSubPap = new List<lib_SUB_PAP>();
        private List<lib_PAP> ListPap = new List<lib_PAP>();
        private List<lib_MainBudgetAllowance> ListBudgetMain = new List<lib_MainBudgetAllowance>();
        private List<DivisionBudgetAllocationFields> ListDivision = new List<DivisionBudgetAllocationFields>();
        private List<DivisionBudgetAllocationFields> ListDivisionUnits = new List<DivisionBudgetAllocationFields>();
        public frmPAFManager()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            this.Loaded += frmPAFManager_Loaded;
        }

        void frmPAFManager_Loaded(object sender, RoutedEventArgs e)
        {
            this.FetchPAP();
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {

            var _results = e.Result.ToString();
            switch (c_div.Process)
            {
         
               
                case "FetchPAP":
                    XDocument oDocKeyResultsFetchPAP = XDocument.Parse(_results);
                    var _dataListsFetchPAP = from info in oDocKeyResultsFetchPAP.Descendants("Table")
                                             select new lib_PAP
                                             {
                                                 DBM_Pap_Id = Convert.ToString(info.Element("DBM_Pap_Id").Value),
                                                 DBM_Sub_Pap_Code = Convert.ToString(info.Element("DBM_Sub_Pap_Code").Value),
                                                 DBM_Sub_Pap_Desc = Convert.ToString(info.Element("DBM_Sub_Pap_Desc").Value),
                                                 DBM_Sub_Pap_id = Convert.ToString(info.Element("DBM_Sub_Pap_id").Value)
                                             };

                    ListPap.Clear();

                    foreach (var item in _dataListsFetchPAP)
                    {
                        lib_PAP _varDetails = new lib_PAP();


                        _varDetails.DBM_Pap_Id = item.DBM_Pap_Id;
                        _varDetails.DBM_Sub_Pap_Code = item.DBM_Sub_Pap_Code;
                        _varDetails.DBM_Sub_Pap_Desc = item.DBM_Sub_Pap_Desc;
                        _varDetails.DBM_Sub_Pap_id = item.DBM_Sub_Pap_id;


                        ListPap.Add(_varDetails);

                    }

                    FetchSubPAP();

                    break;
                case "FetchSubPAP":
                    XDocument oDocKeyResultsFetchSubPAP = XDocument.Parse(_results);
                    var _dataListsFetchSubPAP = from info in oDocKeyResultsFetchSubPAP.Descendants("Table")
                                                select new lib_SUB_PAP
                                                {
                                                    DBM_Sub_Id = Convert.ToString(info.Element("DBM_Sub_Id").Value),
                                                    Description = Convert.ToString(info.Element("Description").Value),
                                                    PAP = Convert.ToString(info.Element("PAP").Value),
                                                    Sub_Id = Convert.ToString(info.Element("Sub_Id").Value)
                                                };

                    ListSubPap.Clear();

                    foreach (var item in _dataListsFetchSubPAP)
                    {
                        lib_SUB_PAP _varDetails = new lib_SUB_PAP();


                        _varDetails.DBM_Sub_Id = item.DBM_Sub_Id;
                        _varDetails.Description = item.Description;
                        _varDetails.PAP = item.PAP;
                        _varDetails.Sub_Id = item.Sub_Id;


                        ListSubPap.Add(_varDetails);

                    }
                    FetchDivisionUnits();


                    break;
                case "FetchDivisionUnits":
                    XDocument oDocKeyResultsFetchDivisionUnits = XDocument.Parse(_results);

                    var _dataListsFetchDivisionUnits = from info in oDocKeyResultsFetchDivisionUnits.Descendants("Table")
                                                       select new DivisionBudgetAllocationFields
                                                       {
                                                           DBM_Sub_Pap_Id = Convert.ToString(info.Element("DBM_Sub_Pap_Id").Value),
                                                           Division_Code = Convert.ToString(info.Element("Division_Code").Value),
                                                           Division_Desc = Convert.ToString(info.Element("Division_Desc").Value),
                                                           Division_Id = Convert.ToString(info.Element("Division_Id").Value),
                                                           UnitCode = Convert.ToString(info.Element("UnitCode").Value)

                                                       };


                    ListDivisionUnits.Clear();

                    foreach (var item in _dataListsFetchDivisionUnits)
                    {

                        DivisionBudgetAllocationFields _temp = new DivisionBudgetAllocationFields();


                        _temp.DBM_Sub_Pap_Id = item.DBM_Sub_Pap_Id;
                        _temp.Division_Code = item.Division_Code;
                        _temp.Division_Desc = item.Division_Desc;
                        _temp.Division_Id = item.Division_Id;
                        _temp.UnitCode = item.UnitCode;


                        ListDivisionUnits.Add(_temp);

                    }
                    FetchDivisionPerOffice();
                    break;
                case "FetchDivisionPerOffice":
                    XDocument oDocKeyResults = XDocument.Parse(_results);

                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new DivisionBudgetAllocationFields
                                     {
                                         DBM_Sub_Pap_Id = Convert.ToString(info.Element("DBM_Sub_Pap_Id").Value),
                                         Division_Code = Convert.ToString(info.Element("Division_Code").Value),
                                         Division_Desc = Convert.ToString(info.Element("Division_Desc").Value),
                                         Division_Id = Convert.ToString(info.Element("Division_Id").Value),
                                         UnitCode = Convert.ToString(info.Element("UnitCode").Value)

                                     };


                    ListDivision.Clear();
                    List<ProfData> _ComboList = new List<ProfData>();

                    foreach (var item in _dataLists)
                    {
                        ProfData _varProf = new ProfData();
                        DivisionBudgetAllocationFields _temp = new DivisionBudgetAllocationFields();

                        _varProf._Name = item.Division_Desc;

                        _temp.DBM_Sub_Pap_Id = item.DBM_Sub_Pap_Id;
                        _temp.Division_Code = item.Division_Code;
                        _temp.Division_Desc = item.Division_Desc;
                        _temp.Division_Id = item.Division_Id;
                        _temp.UnitCode = item.UnitCode;

                        _ComboList.Add(_varProf);
                        ListDivision.Add(_temp);

                    }

                    ListBudgetMain.Clear();
                    foreach (lib_PAP item in ListPap)
                    {
                        lib_MainBudgetAllowance _xdata = new lib_MainBudgetAllowance();
                        _xdata.Id = item.DBM_Sub_Pap_id;
                        _xdata.Pap = item.DBM_Sub_Pap_Code;
                        _xdata.Description = item.DBM_Sub_Pap_Desc;
                        _xdata.IsDiv = false;
                        _xdata.IsUnit = false;
                        _xdata.IsSubPAP = false;
                        ListBudgetMain.Add(_xdata);

                        List<lib_SUB_PAP> subpap = ListSubPap.Where(x => x.DBM_Sub_Id == item.DBM_Sub_Pap_id).ToList();

                        foreach (lib_SUB_PAP itemsubpap in subpap)
                        {
                            _xdata = new lib_MainBudgetAllowance();
                            _xdata.Id = itemsubpap.Sub_Id;
                            _xdata.Pap = itemsubpap.PAP;
                            _xdata.Description = "       " + itemsubpap.Description;
                            _xdata.IsDiv = false;
                            _xdata.IsUnit = false;
                            _xdata.IsSubPAP = true;
                            ListBudgetMain.Add(_xdata);


                            List<DivisionBudgetAllocationFields> div = ListDivision.Where(x => x.Division_Code == itemsubpap.PAP).ToList();
                            foreach (DivisionBudgetAllocationFields itemdiv in div)
                            {
                                _xdata = new lib_MainBudgetAllowance();
                                _xdata.Id = itemdiv.Division_Id;
                                _xdata.Pap = itemdiv.Division_Code;
                                _xdata.Description = "       " + itemdiv.Division_Code + " - " + itemdiv.Division_Desc;
                                _xdata.IsDiv = true;
                                _xdata.IsUnit = false;
                                _xdata.IsSubPAP = false;
                                ListBudgetMain.Add(_xdata);


                                List<DivisionBudgetAllocationFields> div_unit = ListDivisionUnits.Where(x => x.UnitCode == itemdiv.Division_Id).ToList();
                                foreach (DivisionBudgetAllocationFields item_unit in div_unit)
                                {
                                    _xdata = new lib_MainBudgetAllowance();
                                    _xdata.Id = item_unit.Division_Id;
                                    _xdata.Pap = item_unit.Division_Code;
                                    _xdata.Description = "                          " + item_unit.Division_Desc;
                                    _xdata.IsDiv = false;
                                    _xdata.IsUnit = true;
                                    _xdata.IsSubPAP = false;
                                    ListBudgetMain.Add(_xdata);
                                }
                            }
                        }
                    }
                    grdPAP.ItemsSource = null;
                    grdPAP.ItemsSource = ListBudgetMain;

                    grdPAP.Columns["Id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdPAP.Columns["IsDiv"].Visibility = System.Windows.Visibility.Collapsed;
                    grdPAP.Columns["IsUnit"].Visibility = System.Windows.Visibility.Collapsed;
                    grdPAP.Columns["IsSubPAP"].Visibility = System.Windows.Visibility.Collapsed;
                    Column col_Pap = grdPAP.Columns.DataColumns["Pap"];
                    col_Pap.Width = new ColumnWidth(125, false);
                    //if (ListDivision.Count==0)
                    //{
                    //    btnTransfer.IsEnabled = false;
                    //    btnAssign.IsEnabled = true;
                    //}
                    //else
                    //{
                    //    btnTransfer.IsEnabled = true;
                    //    btnAssign.IsEnabled = false;
                    //}
                    //cmbDivision.ItemsSource = null;
                    //cmbDivision.ItemsSource = _ComboList;
                    //cmbYear.SelectedIndex = 0;
                    //cmbDivision.SelectedIndex = 0;
                    //FetchRespoCenter();
                    
                    this.Cursor = Cursors.Arrow;
                    break;
               
              
            }
        }
        private void FetchPAP()
        {
            c_div.Process = "FetchPAP";
            svc_mindaf.ExecuteSQLAsync(c_div.FetchPap(this.WorkingYear));
        }
        private void FetchSubPAP()
        {
            c_div.Process = "FetchSubPAP";
            svc_mindaf.ExecuteSQLAsync(c_div.FetchSubPap(this.WorkingYear));
        }
        private void FetchDivisionUnits()
        {
            c_div.Process = "FetchDivisionUnits";
            svc_mindaf.ExecuteSQLAsync(c_div.FetchDivisionUnit(this.WorkingYear));
        }
  
        private void FetchDivisionPerOffice()
        {
            c_div.Process = "FetchDivisionPerOffice";
            svc_mindaf.ExecuteSQLAsync(c_div.FetchDivisionPerOffice(OfficeId,this.WorkingYear));
        }

   
        private ctrlPAP_transfer _pap = new ctrlPAP_transfer();
        private ctrlPAPNewSetup _papnew = new ctrlPAPNewSetup();

        private void btnTransfer_Click(object sender, RoutedEventArgs e)
        {
            _pap = new ctrlPAP_transfer();
            var _selected = (lib_MainBudgetAllowance)grdPAP.ActiveItem;
            if (_selected.IsDiv.ToString().ToLower() == "true")
            {
                _pap.TypeDepartment = "Office";
            }
            else if (_selected.IsDiv.ToString().ToLower() == "true") 
            {
                _pap.TypeDepartment = "Unit";
            }
            else
            {
                MessageBox.Show("Function not yet available", "No Functio", MessageBoxButton.OK);
                return; 
            }

            _pap.DivisionId = _selected.Id;
            _pap.Width = this.grdControl.Width;
            _pap.Height = this.grdControl.Height;
            _pap.lblSelected.Content = _selected.Description;
            _pap.RefreshData += _pap_RefreshData;
            grdControl.Children.Clear();
            grdControl.Children.Add(_pap);

        }

        void _pap_RefreshData(object sender, EventArgs e)
        {
            grdPAP.ItemsSource = null;
            this.FetchPAP();
            MessageBox.Show("Transfer has been saved.", "Success", MessageBoxButton.OK);
            grdControl.Children.Clear();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnAssign_Click(object sender, RoutedEventArgs e)
        {
    
            _papnew = new ctrlPAPNewSetup();
            var _selected = (lib_MainBudgetAllowance)grdPAP.ActiveItem;

            if (_selected.IsDiv.ToString().ToLower() == "false" && _selected.IsUnit.ToString().ToLower() == "false" && _selected.IsSubPAP.ToString().ToLower() == "false")
            {
                _papnew.TypeDepartment = "SubPAP";
            }
            else if (_selected.IsSubPAP.ToString().ToLower() == "true")
            {
                _papnew.TypeDepartment = "Division";
            }
            else if (_selected.IsDiv.ToString().ToLower() == "true")
            {
                _papnew.TypeDepartment = "Unit";
            }
            else if (_selected.IsUnit.ToString().ToLower() == "true")
            {
                _papnew.TypeDepartment = "Division";
                
            }
            else
            {
                _papnew.TypeDepartment = "Assign";
                
            }

           // _papnew.Excempted = _Excempted;
            _papnew.DBM_Sub_Id = _selected.Id;
            _papnew.PAP = _selected.Pap;
            _papnew.DivisionId = _selected.Id;
            _papnew.Width = this.grdControl.Width;
            _papnew.Height = this.grdControl.Height;
            _papnew.lblSelected.Content = _selected.Description.Trim();
            _papnew.RefreshData += _papnew_RefreshData;
            _papnew.WorkingYear = this.WorkingYear;
            grdControl.Children.Clear();
            grdControl.Children.Add(_papnew);
        }

        void _papnew_RefreshData(object sender, EventArgs e)
        {
            grdPAP.ItemsSource = null;
            this.FetchPAP();
            MessageBox.Show("Item has been saved.", "Success", MessageBoxButton.OK);
            grdControl.Children.Clear();
        }

        private void grdPAP_CellClicked(object sender, CellClickedEventArgs e)
        {
            grdControl.Children.Clear();
            lib_MainBudgetAllowance _selected = (lib_MainBudgetAllowance)grdPAP.ActiveItem;
            if (_selected.IsDiv.ToString().ToLower() == "false" && _selected.IsUnit.ToString().ToLower()=="false")
            {
                
            }
            //if (_selected.IsDiv.ToString().ToLower() == "true")
            //{
            //    btnAssign.IsEnabled = false;
            //    btnTransfer.IsEnabled = true;
            //}
            //else if (_selected.IsUnit.ToString().ToLower() == "true")
            //{
            //    btnAssign.IsEnabled = false;
            //    btnTransfer.IsEnabled = true;
            //}
            //else
            //{
            //    btnAssign.IsEnabled = true;
            //    btnTransfer.IsEnabled = false;
            //}
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            lib_MainBudgetAllowance _selected = (lib_MainBudgetAllowance)grdPAP.ActiveItem;
            if (_selected!=null)
            {
                if (_selected.IsUnit || _selected.IsDiv)
                {
                    c_div.Process = "RemoveData";
                    c_div.SQLOperation += c_div_SQLOperation;
                    c_div.RemoveData(_selected.Id, this.WorkingYear);
                }
                else
                {
                    MessageBox.Show("Cannot remove top level item", "Remove failed", MessageBoxButton.OK);
                }
              
            }
        }

        void c_div_SQLOperation(object sender, EventArgs e)
        {
            grdPAP.ItemsSource = null;
            this.FetchPAP();
            MessageBox.Show("Item has been removed.", "Success", MessageBoxButton.OK);
            grdControl.Children.Clear();
        }

        private frmNewPap f_new = new frmNewPap();


        private void btnPapNew_Click(object sender, RoutedEventArgs e)
        {
            f_new = new frmNewPap();
            f_new.WorkingYear = this.WorkingYear;
            f_new.ReloadData += f_new_ReloadData;
         
            f_new.Show();
        }

      

        void f_new_ReloadData(object sender, EventArgs e)
        {
            grdPAP.ItemsSource = null;
            this.FetchPAP();
        }
    }
}

