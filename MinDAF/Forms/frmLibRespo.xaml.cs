﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmLibRespo : ChildWindow
    {
        public event EventHandler SelectedUpdate;

        public List<RespoData> ActiveList { get; set; }
        public String SetSource { get; set; }
        public String FundSource { get; set; }

        public String WorkingYear { get; set; }

        public RespoDBLib SelectedData { get; set; }

        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsRespoActualLibrary c_respo = new clsRespoActualLibrary();

        private List<RespoDBLib> ListRespoActual = new List<RespoDBLib>();
        public frmLibRespo()
        {
            InitializeComponent();
            //svc_mindaf.ExecuteImportDataSQLCompleted += svc_mindaf_ExecuteImportDataSQLCompleted;
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            this.Loaded += frmLibRespo_Loaded;
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_respo.Process)
            {
                case "FetchActualRespo":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new RespoDBLib
                                     {
                                         FundSource = Convert.ToString(info.Element("respo_name").Value),
                                         Code = Convert.ToString(info.Element("code").Value)

                                     };

                    ListRespoActual.Clear();


                    foreach (var item in _dataLists)
                    {
                        List<RespoData> _Count = ActiveList.Where(x => x.Name == item.FundSource).ToList();
                        if (_Count.Count == 0)
                        {
                            RespoDBLib _varDetails = new RespoDBLib();

                            _varDetails.Code = item.Code;
                            _varDetails.FundSource = item.FundSource;
                            ListRespoActual.Add(_varDetails);
                        }

                    }


                    grdData.ItemsSource = null;
                    grdData.ItemsSource = ListRespoActual;

                    grdData.Columns["Code"].Visibility = System.Windows.Visibility.Collapsed;
                    this.Cursor = Cursors.Arrow;
                    break;

            }
        }

      

      
        void frmLibRespo_Loaded(object sender, RoutedEventArgs e)
        {
            FetchActualRespo();
        }

      
        private void FetchActualRespo() 
        {
            c_respo.Process = "FetchActualRespo";
            switch (SetSource)
            {
                case "Current Appropriation":
                    svc_mindaf.ExecuteSQLAsync(c_respo.FetchDBLibCURRENT(this.WorkingYear));
                    break;
                case "Continuing Appropriation":
                    svc_mindaf.ExecuteSQLAsync(c_respo.FetchDBLibCONT(this.WorkingYear));
                    break;

            }
          
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SelectedData = new RespoDBLib();
                SelectedData.Code = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["Code"].Value.ToString();
                SelectedData.FundSource = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["FundSource"].Value.ToString();

            }
            catch (Exception)
            {

                SelectedData = null;
            }

            if (SelectedData == null)
            {
                MessageBox.Show("No Source Selected");
                return;
            }
            else
            {
                if (SelectedUpdate!=null)
                {
                    SelectedUpdate(this, new EventArgs());
                }
                this.DialogResult = true;
            }

        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

