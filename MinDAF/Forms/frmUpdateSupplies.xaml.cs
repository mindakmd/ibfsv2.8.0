﻿using Infragistics.Controls.Grids;
using MinDAF.Class;
using MinDAF.MinDAFS;
using MinDAF.Usercontrol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using Infragistics;
namespace MinDAF.Forms
{
    public partial class frmUpdateSupplies : ChildWindow
    {
        
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsBudgetSupplies c_budgetsupplies = new clsBudgetSupplies();
        private List<ProcurementSupplies> ListSupplies = new List<ProcurementSupplies>();
        private List<ProcurementDetails> ListDetails = new List<ProcurementDetails>();
        DispatcherTimer timer = null;
        int i = 0;
        public frmUpdateSupplies()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            c_budgetsupplies.SQLOperation += c_budgetsupplies_SQLOperation;
        }
        private Double ComputeTotalExpenditure()
        {
            double _Total = 0.00;
            foreach (var item in ListDetails)
            {
                _Total += Convert.ToDouble(item.total.ToString());
            }

            return _Total;
        }
        private BudgetRunningBalance _budget_bal { get; set; }
        
        void c_budgetsupplies_SQLOperation(object sender, EventArgs e)
        {
            
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_budgetsupplies.Process)
            {
                case "FetchProcurementItems":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new ProcurementSupplies
                                     {
                                         general_category = Convert.ToString(info.Element("general_category").Value),
                                         id = Convert.ToString(info.Element("id").Value),
                                         item_specifications = Convert.ToString(info.Element("item_specifications").Value),
                                         minda_inventory = Convert.ToString(info.Element("minda_inventory").Value),
                                         price = Convert.ToString(info.Element("price").Value),
                                         sub_category = Convert.ToString(info.Element("sub_category").Value),
                                         unit_of_measure = Convert.ToString(info.Element("unit_of_measure").Value)

                                     };

                    ListSupplies.Clear();
                    List<_SupplyComboItem> _dataCombo = new List<_SupplyComboItem>();
                    foreach (var item in _dataLists)
                    {
                        ProcurementSupplies _varDetails = new ProcurementSupplies();
                        _SupplyComboItem _varCombo = new _SupplyComboItem();

                        _varDetails.general_category = item.general_category;
                        _varDetails.id = item.id;
                        _varDetails.item_specifications = item.item_specifications;
                        _varDetails.minda_inventory = item.minda_inventory;
                        _varDetails.price = item.price;
                        _varDetails.sub_category = item.sub_category;
                        _varDetails.unit_of_measure = item.unit_of_measure;

                        _varCombo._Name = item.item_specifications;

                        ListSupplies.Add(_varDetails);
                        _dataCombo.Add(_varCombo);

                    }

                    cmbSpecifications.ItemsSource = _dataCombo;
                    grdItems.ItemsSource = null;
                    grdItems.ItemsSource = _dataCombo;
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchNonPSDBMItems":
                    XDocument oDocKeyResultsNonPSDBM = XDocument.Parse(_results);
                    var _dataListss = from info in oDocKeyResultsNonPSDBM.Descendants("Table")
                                      select new ProcurementSupplies
                                      {
                                          general_category = Convert.ToString(info.Element("general_category").Value),
                                          id = Convert.ToString(info.Element("id").Value),
                                          item_specifications = Convert.ToString(info.Element("item_specifications").Value),
                                          minda_inventory = Convert.ToString(info.Element("minda_inventory").Value),
                                          price = Convert.ToString(info.Element("price").Value),
                                          sub_category = Convert.ToString(info.Element("sub_category").Value),
                                          unit_of_measure = Convert.ToString(info.Element("unit_of_measure").Value)

                                      };

                    ListSupplies.Clear();
                    List<_SupplyComboItem> _dataCombos = new List<_SupplyComboItem>();
                    foreach (var item in _dataListss)
                    {
                        ProcurementSupplies _varDetails = new ProcurementSupplies();
                        _SupplyComboItem _varCombo = new _SupplyComboItem();

                        _varDetails.general_category = item.general_category;
                        _varDetails.id = item.id;
                        _varDetails.item_specifications = item.item_specifications;
                        _varDetails.minda_inventory = item.minda_inventory;
                        _varDetails.price = item.price;
                        _varDetails.sub_category = item.sub_category;
                        _varDetails.unit_of_measure = item.unit_of_measure;

                        _varCombo._Name = item.item_specifications;

                        ListSupplies.Add(_varDetails);
                        _dataCombos.Add(_varCombo);
                    }

                    cmbSpecifications.ItemsSource = _dataCombos;
                    grdItems.ItemsSource = null;
                    grdItems.ItemsSource = _dataCombos;
                    this.Cursor = Cursors.Arrow;
                    break;
            }
        }


        private void FetchPSDBMItems()
        {
            c_budgetsupplies.Process = "FetchProcurementItems";
            svc_mindaf.ExecuteSQLAsync(c_budgetsupplies.FetchProcurementSuppliesPSDBM());
        }

        private void FetchNonPSDBMItems()
        {
            c_budgetsupplies.Process = "FetchNonPSDBMItems";
            svc_mindaf.ExecuteSQLAsync(c_budgetsupplies.FetchProcurementSuppliesNonPSDBM());
        }


        private void frmUpdateSupplies_Loaded(object sender, RoutedEventArgs e)
        {
            //FetchNonPSDBMItems();
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TabItem selectedTab = e.AddedItems[0] as TabItem;
            if (selectedTab.Name == "tab_psdbm")
            {
                //MessageBox.Show("Tab 1 is selected");
                FetchPSDBMItems();
            }
            else if (selectedTab.Name == "tab_nonpsdbm")
            {
                //MessageBox.Show("Tab 2 is selected");
                FetchNonPSDBMItems();
            }
        }

       
        private void cmbSpecifications_DropDownClosed(object sender, EventArgs e)
        {
            var selectedItem = cmbSpecifications.SelectedItem as _SupplyComboItem;

            if (selectedItem != null)
            {
                List<ProcurementSupplies> x = ListSupplies.Where(item => item.item_specifications == selectedItem._Name).ToList();
                if (x.Count != 0)
                {
                    txtSubCategory.Text = x[0].sub_category;
                    txtInventoryItem.Text = x[0].minda_inventory;
                    txtUnit.Text = x[0].unit_of_measure;
                    txtUnitPrice.Value = x[0].price;
                }
            }
        }


        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtSearch.Text != "")
            {
                grdItems.FilteringSettings.RowFiltersCollection.Clear();

                FilterText(txtSearch.Text);

            }
            else
            {
                grdItems.FilteringSettings.RowFiltersCollection.Clear();

            }
        }

        private void FilterText(String itemsearch)
        {

            grdItems.FilteringSettings.RowFiltersCollection.Clear();

            Column colText = (Column)grdItems.Columns["_Name"];
            RowsFilter rfText = new RowsFilter(typeof(List<_SupplyComboItem>), colText);
            ComparisonCondition compCondText = new ComparisonCondition();
            compCondText.Operator = ComparisonOperator.Contains;
            compCondText.FilterValue = itemsearch;

            rfText.Conditions.Add(compCondText);
            grdItems.FilteringSettings.RowFiltersCollection.Add(rfText);


        }

        private void grdItems_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {

            }
        }

        private void grdItems_ActiveCellChanged(object sender, EventArgs e)
        {

        }

        private void grdItems_CellClicked(object sender, CellClickedEventArgs e)
        {
            _SupplyComboItem selectedItem = (_SupplyComboItem)grdItems.ActiveItem;

            if (selectedItem != null)
            {
                List<ProcurementSupplies> x = ListSupplies.Where(item => item.item_specifications == selectedItem._Name).ToList();
                if (x.Count != 0)
                {
                    txtSubCategory.Text = x[0].sub_category;
                    txtInventoryItem.Text = x[0].minda_inventory;
                    txtUnit.Text = x[0].unit_of_measure;
                    txtUnitPrice.Value = x[0].price;
                }
            }
        }
    }

    public class _SupplyComboItem
    {
        public String _Name { get; set; }
    }
}

