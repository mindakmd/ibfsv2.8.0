﻿using Infragistics.Controls.Grids;
using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmFinanceNotify : ChildWindow
    {
        public event EventHandler CheckData;

        public Boolean has_new { get; set; }

        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsNotifications c_notif = new clsNotifications();

        private List<ListNotifications> _ListNotification = new List<ListNotifications>();
        private List<ListNotificationData> _ListNotificationData = new List<ListNotificationData>();
        public frmFinanceNotify()
        {
            InitializeComponent();
            this.Loaded += frmFinanceNotify_Loaded;
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
           
        }


        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_notif.Process)
            {
                case "FetchCountNew":
                    XDocument oDocKeyResultsFetchCountNew = XDocument.Parse(_results);
                    var _dataListsFetchCountNew = from info in oDocKeyResultsFetchCountNew.Descendants("Table")
                                       select new ListCountNew
                                     {
                                         Count = Convert.ToString(info.Element("counter").Value)

                                     };


                    _ListNotification.Clear();
                    _ListNotificationData.Clear();
                    Int32 _Count = 0;
                    foreach (var item in _dataListsFetchCountNew)
                    {

                        _Count += Convert.ToInt32(item.Count.ToString());

                    }

                    if (_Count!=0)
                    {
                        has_new = true;
                    }
                    if (CheckData!=null)
                    {
                        this.CheckData(this, new EventArgs());
                    }
                    this.Cursor = Cursors.Arrow;
                    break;

                case "FetchNotifications":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new ListNotifications
                                     {
                                         DateSubmitted = Convert.ToString(info.Element("date_time").Value),
                                         Division = Convert.ToString(info.Element("FromDiv").Value),
                                         Status = Convert.ToString(info.Element("Status").Value),
                                         Subject = Convert.ToString(info.Element("subject").Value)

                                     };


                    _ListNotification.Clear();
                    _ListNotificationData.Clear();

                    foreach (var item in _dataLists)
                    {
                        ListNotificationData _varD = new ListNotificationData();
                        ListNotifications _varData = new ListNotifications();

                        _varData.DateSubmitted = item.DateSubmitted;
                        _varData.Division = item.Division;
                        _varData.Status = item.Status;
                        _varData.Subject = item.Subject;

                        _varD.DateSubmitted =Convert.ToDateTime(item.DateSubmitted).ToShortDateString();
                        _varD.Status = item.Status;
                        _varD.Subject = item.Subject;


                        _ListNotification.Add(_varData);
                        _ListNotificationData.Add(_varD);


                    }
                    grdData.ItemsSource = null;
                    grdData.ItemsSource = _ListNotificationData;

                    try
                    {
                        Column col_project_Status = grdData.Columns.DataColumns["Status"];
                        col_project_Status.Width = new ColumnWidth(50, false);

                        Column col_project_Subject = grdData.Columns.DataColumns["Subject"];
                        col_project_Subject.Width = new ColumnWidth(300, true);

                        Column col_project_dateSub = grdData.Columns.DataColumns["DateSubmitted"];
                        col_project_dateSub.Width = new ColumnWidth(70, false);

                    }
                    catch (Exception)
                    {
                        

                    }

                  
                    this.Cursor = Cursors.Arrow;
                    break;
             
            }
        }


        private void UpdateNotifications() 
        {
            c_notif.Process = "UpdateNotifications";
            c_notif.UpdateNotifications();
            c_notif.SQLOperation += c_notif_SQLOperation;
              
        }

        void c_notif_SQLOperation(object sender, EventArgs e)
        {
            
        }


        private void FetchNotifications() 
        {
            c_notif.Process = "FetchNotifications";
            svc_mindaf.ExecuteSQLAsync(c_notif.FetchNotifications());
        }

        public void FetchCountNew()         
        {
            c_notif.Process = "FetchCountNew";
            svc_mindaf.ExecuteSQLAsync(c_notif.FetchNew());
        }

        private void CheckUnread() 
        {

        }

        void frmFinanceNotify_Loaded(object sender, RoutedEventArgs e)
        {
            FetchNotifications();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateNotifications();
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

