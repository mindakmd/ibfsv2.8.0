﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using MinDAF.Usercontrol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmBudgetRental : ChildWindow
    {
        public String DivisionId { get; set; }
        public String AccountableID { get; set; }
        public String MOOE_ID { get; set; }
        public String MOOE_INDEX { get; set; }
        public String ActivityID { get; set; }
        public String _Year { get; set; }
        public String _Month { get; set; }
        public String FundSource { get; set; }
        public event EventHandler ReloadData;
        private List<GridData> ListGridData = new List<GridData>();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsBudgetRental c_rental = new clsBudgetRental();
        public Boolean IsRevision { get; set; }
        public frmBudgetRental()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }
        private Double ComputeTotal()
        {
            double _Total = 0.00;
            foreach (var item in ListGridData)
            {
                _Total += Convert.ToDouble(item.Total.ToString());
            }

            return _Total;
        }
        private BudgetRunningBalance _budget_bal { get; set; }
        private void LoadBudgetBalance()
        {
            _budget_bal = new BudgetRunningBalance(ComputeTotal(), " Rental Fee", MOOE_ID, MOOE_INDEX);
            _budget_bal._DivisionID = DivisionId;
            _budget_bal._Year = this._Year;
            _budget_bal.WorkingYear = this._Year;
            _budget_bal._FundSource = this.FundSource;
            grdBR.Children.Clear();
            grdBR.Children.Add(_budget_bal);
        }
        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_rental.Process)
            {
                case "FetchRentalTypes":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new TypeRental
                                     {
                                         item_code = Convert.ToString(info.Element("item_code").Value),
                                         item_name = Convert.ToString(info.Element("item_name").Value),
                                         rate = Convert.ToString(info.Element("rate").Value),
                                         rate_year = Convert.ToString(info.Element("rate_year").Value)

                                     };


                    List<ProfData> _ComboList = new List<ProfData>();

                    foreach (var item in _dataLists)
                    {
                        ProfData _varProf = new ProfData();

                        _varProf._Name = item.item_name;


                        _ComboList.Add(_varProf);

                    }
                    cmbRentalType.ItemsSource = _ComboList;
                    this.Cursor = Cursors.Arrow;
                    GetGridData();
                    break;
                case "FetchGridData":
                    XDocument oDocKeyFetchGridData = XDocument.Parse(_results);
                    var _dataListsFetchGridData = from info in oDocKeyFetchGridData.Descendants("Table")
                                                  select new GridData
                                                  {
                                                      ActId = Convert.ToString(info.Element("ActId").Value),
                                                      Activity = Convert.ToString(info.Element("Activity").Value),
                                                      Assigned = Convert.ToString(info.Element("Assigned").Value),
                                                      Destination = Convert.ToString(info.Element("Destination").Value),
                                                      DateEnd = Convert.ToString(info.Element("DateEnd").Value),
                                                      Fare_Rate = Convert.ToString(info.Element("Fare_Rate").Value),
                                                      No_Staff = Convert.ToString(info.Element("No_Staff").Value),
                                                      Remarks = Convert.ToString(info.Element("Remarks").Value),
                                                      DateStart = Convert.ToString(info.Element("DateStart").Value),
                                                      Total = Convert.ToString(info.Element("Total").Value),
                                                      Travel_Allowance = Convert.ToString(info.Element("Travel_Allowance").Value),
                                                      Service_Type = Convert.ToString(info.Element("Service_Type").Value),
                                                      Breakfast = Convert.ToString(info.Element("Breakfast").Value),
                                                      AM_Snacks = Convert.ToString(info.Element("AM_Snacks").Value),
                                                      Lunch = Convert.ToString(info.Element("Lunch").Value),
                                                      PM_Snacks = Convert.ToString(info.Element("PM_Snacks").Value),
                                                      Dinner = Convert.ToString(info.Element("Dinner").Value),
                                                      No_Days = Convert.ToString(info.Element("No_Days").Value),

                                                  };

                    ListGridData.Clear();

                    foreach (var item in _dataListsFetchGridData)
                    {
                        GridData _varDetails = new GridData();

                        _varDetails.ActId = item.ActId;
                        _varDetails.Activity = item.Activity;
                        _varDetails.Assigned = item.Assigned;
                        _varDetails.Destination = item.Destination;
                        _varDetails.DateEnd = item.DateEnd;
                        _varDetails.Fare_Rate = item.Fare_Rate;
                        _varDetails.No_Staff = item.No_Staff;
                        _varDetails.Remarks = item.Remarks;
                        _varDetails.DateStart = item.DateStart;
                        _varDetails.Total = item.Total;
                        _varDetails.Travel_Allowance = item.Travel_Allowance;
                        _varDetails.Service_Type = item.Service_Type;
                        _varDetails.Breakfast = item.Breakfast;
                        _varDetails.AM_Snacks = item.AM_Snacks;
                        _varDetails.Lunch = item.Lunch;
                        _varDetails.PM_Snacks = item.PM_Snacks;
                        _varDetails.Dinner = item.Dinner;
                        _varDetails.No_Days = item.No_Days;
                        ListGridData.Add(_varDetails);

                    }
                    grdData.ItemsSource = null;
                    grdData.ItemsSource = ListGridData;
                    grdData.Columns["ActId"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["DateStart"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["DateEnd"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["Travel_Allowance"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["Destination"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["DateEnd"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["DateStart"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["Travel_Allowance"].Visibility = System.Windows.Visibility.Collapsed;
                    // grdData.Columns["Service_Type"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["Breakfast"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["AM_Snacks"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["Lunch"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["Dinner"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["PM_Snacks"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["Fare_Rate"].HeaderText = "Rate";
                    grdData.Columns["No_Staff"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["Quantity"].Visibility = System.Windows.Visibility.Collapsed;
                    LoadBudgetBalance();
                    this.Cursor = Cursors.Arrow;
                    break;
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        private void SaveData()
        {
            String isRev = "0";
            String procureChoice = "0";
            if (IsRevision)
            {
                isRev = "1";
            }
            if (procureRadioBtn.IsChecked == true)
            {
                procureChoice = "1";
            }
            
            var selectedItem = cmbRentalType.SelectedItem as ProfData;
            String _selectedData = "";
            if (selectedItem != null)
            {
                _selectedData = selectedItem._Name;
            }
            double _rate = Convert.ToDouble(txtRate.Value);
            double _noDay = nudDays.Value;
   

            c_rental.Process = "SaveData";
            c_rental.SQLOperation += c_rental_SQLOperation;
            c_rental.SaveProjectAdvertising(this.ActivityID, this.AccountableID, txtRemark.Text,
               "", _rate, (_rate * _noDay), this._Month, this._Year,
                this.MOOE_INDEX, _noDay.ToString(),"Rental Fee " + _selectedData,FundSource,isRev,procureChoice);


        }
        private void ComputeTotals()
        {
            Double _rate = 0;

            Double _numDays = 0;

            try
            {
                _rate = Convert.ToDouble(txtRate.Value.ToString());
            }
            catch (Exception)
            {
            }

            try
            {
                _numDays = nudDays.Value;
            }
            catch (Exception)
            {
            }

            Double Totals = _rate  * _numDays;
            try
            {
                txtTotal.Value = Totals;
            }
            catch (Exception)
            {
            }

        }
        void c_rental_SQLOperation(object sender, EventArgs e)
        {
            GetGridData();
        }

        private void GetGridData()
        {
            c_rental.Process = "FetchGridData";
            svc_mindaf.ExecuteSQLAsync(c_rental.FetchLocalData(this.ActivityID, this._Month, this._Year, this.MOOE_INDEX,this.FundSource));
        }
        private void FetchRentalTypes()
        {
            c_rental.Process = "FetchRentalTypes";
            svc_mindaf.ExecuteSQLAsync(c_rental.FetchRentalTypes());
        }
        private void frm_b_rental_Closed(object sender, EventArgs e)
        {
            if (ReloadData!=null)
            {
                ReloadData(this, new EventArgs());
            }
        }
        private void SuspendActivity()
        {
            String _id = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["ActId"].Value.ToString();
            c_rental.Process = "Suspend";
            c_rental.SQLOperation+=c_rental_SQLOperation;
            c_rental.UpdateSuspend(_id, "1");

        }
        private void btnSuspend_Click(object sender, RoutedEventArgs e)
        {
            SuspendActivity();
        }
        private void frm_b_rental_Loaded(object sender, RoutedEventArgs e)
        {
            FetchRentalTypes();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {

            if (_budget_bal.BalanceOff < Convert.ToDouble(txtTotal.Value))
            {
                frmNotifyBalance fBa = new frmNotifyBalance();

                fBa.Show();
            }
            else
            {
                SaveData();
            }
        }

        private void nudDays_ValueChanging(object sender, RoutedPropertyChangingEventArgs<double> e)
        {
            ComputeTotals();
        }

        private void txtRate_ValueChanged(object sender, EventArgs e)
        {
            ComputeTotals();
        }
    }
}

