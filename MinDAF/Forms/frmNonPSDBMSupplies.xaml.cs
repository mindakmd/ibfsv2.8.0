﻿using Infragistics.Controls.Grids;
using MinDAF.Class;
using MinDAF.MinDAFS;
using MinDAF.Usercontrol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using Infragistics;
namespace MinDAF.Forms
{
    public partial class frmNonPSDBMSupplies : ChildWindow
    {
        public String AccountableID { get; set; }
        public String MOOE_ID { get; set; }
        public String MOOE_INDEX { get; set; }
        public String ActivityID { get; set; }
        public String _Year { get; set; }
        public String _Month { get; set; }         
        public String DivisionId { get; set; }
        public String FundSource { get; set; }
        public Boolean IsRevision { get; set; }
        public String ExpenditureType { get; set; }

        public event EventHandler ReloadData;


        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsBudgetSupplies c_budgetsupplies = new clsBudgetSupplies();
        private List<ProcurementSupplies> ListSupplies = new List<ProcurementSupplies>();
        private List<ProcurementDetails> ListDetails = new List<ProcurementDetails>();
        public frmNonPSDBMSupplies()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            c_budgetsupplies.SQLOperation += c_budgetsupplies_SQLOperation;
        }
        private Double ComputeTotalExpenditure()
        {
            double _Total = 0.00;
            foreach (var item in ListDetails)
            {
                _Total += Convert.ToDouble(item.total.ToString());
            }

            return _Total;
        }
        private BudgetRunningBalance _budget_bal { get; set; }
        private void LoadBudgetBalance()
        {
            String _title = "";
            switch (ExpenditureType)
            {
                case "ICT Office Equipment": _title = "ICT Office Equipment"; break;
                case "Office Supplies Expenses": _title = "Office Supplies Expenses"; break;
                case "ICT Office Supplies": _title = "ICT Office Supplies"; break;
                case "Other Supplies and Materials Expenses": _title = "Other Supplies and Materials Expenses"; break;
                case "Non-PS DBM Supplies": _title = "Non-PS DBM Supplies"; break;

            }
            _budget_bal = new BudgetRunningBalance(ComputeTotalExpenditure(), _title, MOOE_ID,MOOE_INDEX);
            _budget_bal._DivisionID = DivisionId;
            _budget_bal._Year = this._Year;
            _budget_bal.WorkingYear = this._Year;
            _budget_bal._FundSource = this.FundSource;
            grdBR.Children.Clear();
            grdBR.Children.Add(_budget_bal);
        }
        void c_budgetsupplies_SQLOperation(object sender, EventArgs e)
        {
            switch (c_budgetsupplies.Process)
            {
                case "SaveActivityItems":
                    ClearData();
                    FetchProcurementDetails();
                    break;
                case "Suspend":
                    FetchProcurementDetails();
                    break;
           
            }
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_budgetsupplies.Process)
            {
                case "FetchProcurementItems":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new ProcurementSupplies
                                     {
                                         general_category = Convert.ToString(info.Element("general_category").Value),
                                         id = Convert.ToString(info.Element("id").Value),
                                         item_specifications = Convert.ToString(info.Element("item_specifications").Value),
                                         minda_inventory = Convert.ToString(info.Element("minda_inventory").Value),
                                         price = Convert.ToString(info.Element("price").Value),
                                         sub_category = Convert.ToString(info.Element("sub_category").Value),
                                         unit_of_measure = Convert.ToString(info.Element("unit_of_measure").Value)

                                     };

                    ListSupplies.Clear();
                    List<SupplyComboItems> _dataCombo = new List<SupplyComboItems>();
                    foreach (var item in _dataLists)
                    {
                        ProcurementSupplies _varDetails = new ProcurementSupplies();
                        SupplyComboItems _varCombo = new SupplyComboItems();

                        _varDetails.general_category = item.general_category;
                        _varDetails.id = item.id;
                        _varDetails.item_specifications = item.item_specifications;
                        _varDetails.minda_inventory = item.minda_inventory;
                        _varDetails.price = item.price;
                        _varDetails.sub_category = item.sub_category;
                        _varDetails.unit_of_measure = item.unit_of_measure;

                        _varCombo._Name = item.item_specifications;

                        ListSupplies.Add(_varDetails);
                        _dataCombo.Add(_varCombo);
                        
                    }

                    cmbSpecifications.ItemsSource = _dataCombo;
                    grdItems.ItemsSource = null;
                    grdItems.ItemsSource = _dataCombo;
                    this.Cursor = Cursors.Arrow;
                    //FetchProcurementDetails();
                    break;
                case "FetchNonPSDBMItems":
                    XDocument oDocKeyResultsNonPSDBM = XDocument.Parse(_results);
                    var _dataListss = from info in oDocKeyResultsNonPSDBM.Descendants("Table")
                                      select new ProcurementSupplies
                                      {
                                          general_category = Convert.ToString(info.Element("general_category").Value),
                                          id = Convert.ToString(info.Element("id").Value),
                                          item_specifications = Convert.ToString(info.Element("item_specifications").Value),
                                          minda_inventory = Convert.ToString(info.Element("minda_inventory").Value),
                                          price = Convert.ToString(info.Element("price").Value),
                                          sub_category = Convert.ToString(info.Element("sub_category").Value),
                                          unit_of_measure = Convert.ToString(info.Element("unit_of_measure").Value)

                                      };

                    ListSupplies.Clear();
                    List<SupplyComboItems> _dataCombos = new List<SupplyComboItems>();
                    foreach (var item in _dataListss)
                    {
                        ProcurementSupplies _varDetails = new ProcurementSupplies();
                        SupplyComboItems _varCombo = new SupplyComboItems();

                        _varDetails.general_category = item.general_category;
                        _varDetails.id = item.id;
                        _varDetails.item_specifications = item.item_specifications;
                        _varDetails.minda_inventory = item.minda_inventory;
                        _varDetails.price = item.price;
                        _varDetails.sub_category = item.sub_category;
                        _varDetails.unit_of_measure = item.unit_of_measure;

                        _varCombo._Name = item.item_specifications;

                        ListSupplies.Add(_varDetails);
                        _dataCombos.Add(_varCombo);
                    }

                    cmbSpecifications.ItemsSource = _dataCombos;
                    grdItems.ItemsSource = null;
                    grdItems.ItemsSource = _dataCombos;
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchProcurementDetails":

                    XDocument oDocKeyFetchProcurementDetails = XDocument.Parse(_results);
                    var _dataListsFetchProcurementDetails = from info in oDocKeyFetchProcurementDetails.Descendants("Table")
                                     select new ProcurementDetails
                                     {
                                         type_service = Convert.ToString(info.Element("type_service").Value),
                                         accountable_id = Convert.ToString(info.Element("accountable_id").Value),
                                         activity_id = Convert.ToString(info.Element("activity_id").Value),
                                         ActId = Convert.ToString(info.Element("ActId").Value),
                                         month = Convert.ToString(info.Element("month").Value),
                                         procurement_id = Convert.ToString(info.Element("procurement_id").Value),
                                         quantity = Convert.ToString(info.Element("quantity").Value),
                                         rate = Convert.ToString(info.Element("rate").Value),
                                         remarks = Convert.ToString(info.Element("remarks").Value),
                                         total = Convert.ToString(info.Element("total").Value),
                                         year = Convert.ToString(info.Element("year").Value)
                                     };

                    ListDetails.Clear();
                    double _total = 0.00;
                    foreach (var item in _dataListsFetchProcurementDetails)
                    {
                        ProcurementDetails _varDetailss = new ProcurementDetails();

                        _varDetailss.type_service = item.type_service;
                        _varDetailss.accountable_id = item.accountable_id;
                        _varDetailss.activity_id = item.activity_id;
                        _varDetailss.ActId = item.ActId;
                        _varDetailss.month = item.month;
                        _varDetailss.procurement_id = item.procurement_id;
                        _varDetailss.quantity = item.quantity;
                        _varDetailss.rate= item.rate;
                         _varDetailss.remarks = item.remarks;
                        _varDetailss.total = item.total ;
                        _varDetailss.year = item.year;
                        _total += Convert.ToDouble(item.total.ToString());
                        ListDetails.Add(_varDetailss);
             
                    }

                    txtOverTotal.Text = _total.ToString("#,##0.00");
                    grdData.ItemsSource = null;
                    grdData.ItemsSource = ListDetails;

                    foreach (var item in grdData.Rows)
                    {
                        if (item.HasChildren)
                        {
                            item.IsExpanded = true;
                        }
                    }

                    grdData.Columns["ActId"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["activity_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["procurement_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["accountable_id"].Visibility = System.Windows.Visibility.Collapsed;

                     Column col_type_service = grdData.Columns.DataColumns["type_service"];
                     Column col_remarks = grdData.Columns.DataColumns["remarks"];
                     col_type_service.Width = new ColumnWidth(400, false);
                     col_remarks.Width = new ColumnWidth(250, false);
                    LoadBudgetBalance();

                    this.Cursor = Cursors.Arrow;

                    break;
            }
        }


        public void FetchProcurementItems() 
        {
            c_budgetsupplies.Process = "FetchProcurementItems";
            switch (ExpenditureType)
            {
                case "ICT Office Equipment": ExpenditureType = "EXP-ICTOE"; break;
                case "Office Supplies Expenses": ExpenditureType = "EXP-OS"; break;
                case "ICT Office Supplies": ExpenditureType = "EXP-ICT"; break;
                case "Other Supplies and Materials Expenses": ExpenditureType = "EXP-OTS"; break;
                case "Non-PS DBM Supplies": ExpenditureType = "EXP-OTS"; break;
            }
            svc_mindaf.ExecuteSQLAsync(c_budgetsupplies.FetchProcurementSupplies(ExpenditureType));
            //c_budgetsupplies.Process = "FetchNonPSDBMItems";
            //svc_mindaf.ExecuteSQLAsync(c_budgetsupplies.FetchProcurementSuppliesNonPSDBM());
        }

        public void FetchNonPSDBMItems()
        {
            c_budgetsupplies.Process = "FetchNonPSDBMItems";
            svc_mindaf.ExecuteSQLAsync(c_budgetsupplies.FetchProcurementSuppliesNonPSDBM());
        }

        private void FetchProcurementDetails()
        {
            c_budgetsupplies.Process = "FetchProcurementDetails";
            svc_mindaf.ExecuteSQLAsync(c_budgetsupplies.FetchProcurementDetails(this.ActivityID,this.FundSource,this._Month,this.MOOE_INDEX));
        }

        private void SaveActivityItems()
        {
            SupplyComboItems selectedItem = (SupplyComboItems)grdItems.ActiveItem;
           String _idItem = "";
           String _TypeService = "";
           if (selectedItem != null)
           {
               List<ProcurementSupplies> x = ListSupplies.Where(item => item.item_specifications == selectedItem._Name).ToList();
               if (x.Count != 0)
               {
                   _idItem = x[0].id;
                   _TypeService = x[0].item_specifications;
                   _TypeService = _TypeService.Replace("'","");
               }
           }
           String isRev = "0";
           String procureChoice = "0";
           if (IsRevision)
           {
               isRev = "1";
           }

           if (procureRadioBtn.IsChecked == true)
           {
               procureChoice = "1";
           }
            
            c_budgetsupplies.Process = "SaveActivityItems";
            c_budgetsupplies.SaveExpenditureLibrary(this.ActivityID, _idItem, this.AccountableID, txtRemarks.Text, nudQuantity.Value.ToString(), txtTotal.Value.ToString(), this._Month, this._Year, txtUnitPrice.Value.ToString(), _TypeService,this.FundSource,this.MOOE_INDEX,isRev,procureChoice);
           
        }
        private void ClearData() 
        {
            txtInventoryItem.Text = "";
            txtRemarks.Text = "";
            txtSubCategory.Text = "";
            txtTotal.Value = 0;
            txtUnit.Text ="";
            txtUnitPrice.Value = 0;
            cmbSpecifications.SelectedIndex = -1;

        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {

            if (_budget_bal.BalanceOff < Convert.ToDouble(txtTotal.Value))
            {
                frmNotifyBalance fBa = new frmNotifyBalance();

                fBa.Show();
            }
            else
            {
                SaveActivityItems();
            }
        }

        private void frmNonPSDBMSuppliess_Loaded(object sender, RoutedEventArgs e)
        {
            FetchProcurementItems();
            FetchProcurementDetails();
        }

        private void UpdateItemData(object sender, RoutedEventArgs e)
        {
            FetchProcurementItems();

        }

        private void ComputeTotals() 
        {
            double _qty = 0;
            double _total = 0;
            double _price = 0;

            try
            {
                _qty = nudQuantity.Value;
            }
            catch (Exception)
            {
                
            }
            try
            {
                _price = Convert.ToDouble(txtUnitPrice.Value);
            }
            catch (Exception)
            {
            }
            _total = _qty * _price;

            txtTotal.Value = _total;
        }
        private void cmbSpecifications_DropDownClosed(object sender, EventArgs e)
        {
            var selectedItem = cmbSpecifications.SelectedItem as SupplyComboItems;

            if (selectedItem!=null)
            {
                List<ProcurementSupplies> x = ListSupplies.Where(item => item.item_specifications == selectedItem._Name).ToList();
                if (x.Count != 0)
                {
                    txtSubCategory.Text = x[0].sub_category;
                    txtInventoryItem.Text = x[0].minda_inventory;
                    txtUnit.Text = x[0].unit_of_measure;
                    txtUnitPrice.Value = x[0].price;

                    ComputeTotals();
                }
            }
 
           
        }

        private void nudQuantity_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                ComputeTotals();
            }
            catch (Exception)
            {
     
            }
        }


        private void nudQuantity_ValueChanging(object sender, RoutedPropertyChangingEventArgs<double> e)
        {
            ComputeTotals();
        }

        private void frmNonPSDBMSuppliess_Closed(object sender, EventArgs e)
        {
            if (ReloadData!=null)
            {
                ReloadData(this, new EventArgs());
            }
        }

        private void SuspendActivity()
        {
            String _id = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["ActId"].Value.ToString();
            c_budgetsupplies.Process = "Suspend";
            c_budgetsupplies.SQLOperation+=c_budgetsupplies_SQLOperation;
            c_budgetsupplies.UpdateSuspend(_id, "1");

        }

        private void btnSuspend_Click(object sender, RoutedEventArgs e)
        {
            SuspendActivity();
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtSearch.Text != "")
            {
                grdItems.FilteringSettings.RowFiltersCollection.Clear();
          
                FilterText(txtSearch.Text);

            }
            else
            {
                grdItems.FilteringSettings.RowFiltersCollection.Clear();

            }
        }

        private void FilterText(String itemsearch)
        {
            
                grdItems.FilteringSettings.RowFiltersCollection.Clear();

                Column colText = (Column)grdItems.Columns["_Name"];
                RowsFilter rfText = new RowsFilter(typeof(List<SupplyComboItems>), colText);
                ComparisonCondition compCondText = new ComparisonCondition();
                compCondText.Operator = ComparisonOperator.Contains;
                compCondText.FilterValue = itemsearch;

                rfText.Conditions.Add(compCondText);
                grdItems.FilteringSettings.RowFiltersCollection.Add(rfText);
           

        }

        private void grdItems_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                
            }
        }

        private void grdItems_ActiveCellChanged(object sender, EventArgs e)
        {
           
        }

        private void grdItems_CellClicked(object sender, CellClickedEventArgs e)
        {
            SupplyComboItems selectedItem = (SupplyComboItems)grdItems.ActiveItem;

            if (selectedItem != null)
            {
                List<ProcurementSupplies> x = ListSupplies.Where(item => item.item_specifications == selectedItem._Name).ToList();
                if (x.Count != 0)
                {
                    txtSubCategory.Text = x[0].sub_category;
                    txtInventoryItem.Text = x[0].minda_inventory;
                    txtUnit.Text = x[0].unit_of_measure;
                    txtUnitPrice.Value = x[0].price;

                    ComputeTotals();
                }
            }
        }

        private void btnAddItems_Click(object sender, RoutedEventArgs e)
        {
            AddNonPSDBMSupply addnonpsdbm = new AddNonPSDBMSupply();
            addnonpsdbm._Year = this._Year;
            addnonpsdbm.Show();
        }

        private void btnRefreshData_Click(object sender, RoutedEventArgs e)
        {
            FetchNonPSDBMItems();
        }

        private void grdItems_Click(object sender, RoutedEventArgs e)
        {
            FetchNonPSDBMItems();
        }

        public void RefreshData()
        {
            btnRefreshData_Click(this, null);
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TabItem selectedTab = e.AddedItems[0] as TabItem;
            if (selectedTab.Name == "tab_psdbm")
            {
                //MessageBox.Show("Tab 1 is selected");
                FetchProcurementItems();
            }
            else if (selectedTab.Name == "tab_nonpsdbm")
            {
                //MessageBox.Show("Tab 2 is selected");
                FetchNonPSDBMItems();
            }
            //switch (tabControl.SelectedIndex == 1)
            //{
            //    case 0: MessageBox.Show("Tab 1 is selected"); break;
            //    case 1: MessageBox.Show("Tab 2 is selected"); break;
            //}
        }

        //private void tab_psdbm_Enter(object sender, EventArgs e)
        //{
        //    MessageBox.Show("Tab 1 is selected");
        //}

        //private void tab_nonpsdbm_Enter(object sender, EventArgs e)
        //{
        //    MessageBox.Show("Tab 2 is selected");
        //}
    }

    public class SupplyComboItems
    {
        public String _Name { get; set; }
    }
}

