﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Forms
{
    public partial class frmTitleRevision : ChildWindow
    {
        public String _Name {get;set;}
        public event EventHandler SaveData;
        public frmTitleRevision()
        {
            InitializeComponent();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)        
        {
            if (SaveData!=null)
            {
                _Name = txtName.Text;
                if (MessageBox.Show("Save Revision: '" + _Name + "'?", "Confirmation", MessageBoxButton.OKCancel) == System.Windows.MessageBoxResult.OK)
                {
                    SaveData(this, new EventArgs());
                    MessageBox.Show("Revision Title saved. You may now start revising.");
                }
            }
            
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

