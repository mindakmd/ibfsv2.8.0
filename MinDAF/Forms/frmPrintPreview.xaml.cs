﻿using MinDAF.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Forms
{
    public partial class frmPrintPreview : ChildWindow
    {
        public String Division { get; set; }
        public String FundSource { get; set; }
        public String ReportType { get; set; }
        public String PAP { get; set; }
        public List<PPMPFormat> PPMPReport { get; set; }
        public List<ReportMBA> ReportData = new List<ReportMBA>();
        private clsPPMP c_ppmp = new clsPPMP();
        private clsPrintOut c_printout = new clsPrintOut();
        public frmPrintPreview()
        {
            InitializeComponent();
        }


        private void LoadReportDataPPMP()
        {
            c_printout.Process = "SaveReportPrintOutPPMP";
            //c_printout.SaveReportPrintOutPPMP(PPMPReport);
            //c_printout.SQLOperation+=c_printout_SQLOperation;
            RetrievePrintDataPPMP();
        }

        private void LoadReportDataMAO()
        {
            c_printout.Process = "SaveReportPrintOutMAO";
            //c_printout.SaveReportPrintOutMAO(ReportData,Division,FundSource,PAP);
            //c_printout.SQLOperation += c_printout_SQLOperation;
            RetrievePrintDataMAO();
        }

        void c_printout_SQLOperation(object sender, EventArgs e)
        {
            switch (c_printout.Process)
            {
                case "SaveReportPrintOutPPMP":
                    RetrievePrintDataPPMP();
                    break;
                case "SaveReportPrintOutMAO":
                    RetrievePrintDataMAO();
                    break;
            }
        }

        private void RetrievePrintDataPPMP()
        {
            var svc = new C1ReportServiceReference.C1ReportServiceClient();
            svc.GetReportAsync("PPMP");
        
            svc.GetReportCompleted += (s, re) =>
            {

                var ms = new System.IO.MemoryStream(re.Result);
                _c1rv.LoadDocument(ms);
            };
        }

        //void svc_GetReportCompleted(object sender, C1ReportServiceReference.GetReportCompletedEventArgs e)
        //{
        //   var ms = new System.IO.MemoryStream(e.Result);
        //        _c1rv.LoadDocument(ms);
        //}
        private void RetrievePrintDataMAO()
        {
            var svc = new C1ReportServiceReference.C1ReportServiceClient();
            svc.GetReportAsync("mao_urs");
            svc.GetReportCompleted += (s, re) =>
            {

                var ms = new System.IO.MemoryStream(re.Result);
                _c1rv.LoadDocument(ms);
            };
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void frm_p_prev_Loaded(object sender, RoutedEventArgs e)
        {
            _c1rv.ViewMode = C1.Silverlight.ReportViewer.ViewMode.FitWidth;
            switch (this.ReportType)
            {
                case "PPMP":
                    LoadReportDataPPMP();
                    break;
                case "mao_urs":
                    LoadReportDataMAO();
                    break;
            }
        }
    }
}

