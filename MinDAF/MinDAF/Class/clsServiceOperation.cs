﻿
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF
{
    public class clsServiceOperation
    {
        MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();

        public event EventHandler DataReturn;

        public String  ReturnCode { get; set; }
        public Boolean isProcessing { get; set; }
        public Object TableData { get; set; }
    
        public void InstantiateService() 
        {
            TimeSpan x = new TimeSpan(0,10,0);
           
            svc_mindaf.InnerChannel.OperationTimeout = x;
            svc_mindaf.ExecuteQueryCompleted += svc_mindaf_ExecuteQueryCompleted;
            svc_mindaf.FetchOfficeCompleted += svc_mindaf_FetchOfficeCompleted;
            svc_mindaf.ExecuteQueryCompleted+=svc_mindaf_ExecuteQueryCompleted;
         //   svc_mindaf.ExecuteNonQueryCompleted += svc_mindaf_ExecuteNonQueryCompleted;
        }

        void svc_mindaf_FetchOfficeCompleted(object sender, FetchOfficeCompletedEventArgs e)
        {
          
        }

        //void svc_mindaf_ExecuteNonQueryCompleted(object sender, ExecuteNonQueryCompletedEventArgs e)
        //{
        //    TableData = e.Result;
        //    if (DataReturn!=null)
        //    {
        //        DataReturn(sender, e);
        //    }
        //}

        public void ExecuteSQL(string _sql) 
        {
            isProcessing = true;
            
            svc_mindaf.ExecuteQueryAsync(_sql);
        }
        public void ExecuteListSQL(object _sql)
        {
            isProcessing = true;
            svc_mindaf.ExecuteQueryListsAsync("");
        }
        public void ExecuteSQLReturnId(string _sql)
        {
            isProcessing = true;
            svc_mindaf.ExecuteQueryReturnIDAsync(_sql);
        }
      
        void svc_mindaf_ExecuteQueryCompleted(object sender, ExecuteQueryCompletedEventArgs e)
        {
            var _result = e.Result;
            ReturnCode = _result.ToString();
            if (DataReturn!=null)
            {
                DataReturn(this, new EventArgs());
            }
           
        }
    }
}
