﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsFundSource
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();



        public String FetchFundsource(String DivId,String Working_year)
        {
            var sb = new System.Text.StringBuilder(95);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"respo.code,");
            sb.AppendLine(@"respo.respo_name,");
            sb.AppendLine(@"respo.fund_type");
            sb.AppendLine(@"FROM mnda_respo_library respo");
            sb.AppendLine(@"WHERE respo.div_id  ='"+ DivId +"' AND working_year ='"+ Working_year +"'");



            return sb.ToString();
        }
    }
    public class FundSourceDataList    
    {
        public String Code { get; set; }
        public String FundSource { get; set; }
        public String FundType { get; set; }
    }
}
