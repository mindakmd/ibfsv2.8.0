﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsRespoActualLibrary
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();

        public String FetchActualLibCONT()
        {
            var sb = new System.Text.StringBuilder(115);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"lib_res.Division as fund_source");
            sb.AppendLine(@"FROM lib_ResponsibilityCenter lib_res");
            sb.AppendLine(@"WHERE lib_res.Division LIKE '%CONT'");
            sb.AppendLine(@"GROUP BY lib_res.Division ");
            sb.AppendLine(@"ORDER BY lib_res.Division ASC");


            return sb.ToString();
        }
        public String FetchActualLib()
        {
            var sb = new System.Text.StringBuilder(165);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"lib_res.Division as fund_source");
            sb.AppendLine(@"FROM lib_ResponsibilityCenter lib_res");
            sb.AppendLine(@"WHERE NOT lib_res.Division LIKE '%CONT%' AND NOT lib_res.Division LIKE '%Fixed Cost%'");
            sb.AppendLine(@"GROUP BY lib_res.Division ");
            sb.AppendLine(@"ORDER BY lib_res.Division ASC");



            return sb.ToString();

        }
        public String FetchDBLibCURRENT(String _working_year)
        {
            var sb = new System.Text.StringBuilder(109);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mrl.respo_name,mrl.code");
            sb.AppendLine(@"FROM mnda_respo_library mrl");
            sb.AppendLine(@"WHERE mrl.div_id ='-' and mrl.fund_type ='mf-1' and working_year ='" + _working_year + "'");



            return sb.ToString();
        }

        public String FetchDBLibCONT(String _working_year)
        {
            var sb = new System.Text.StringBuilder(109);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mrl.respo_name,mrl.code");
            sb.AppendLine(@"FROM mnda_respo_library mrl");
            sb.AppendLine(@"WHERE mrl.div_id ='-' and mrl.fund_type ='mf-2' and working_year ='" + _working_year + "'");



            return sb.ToString();
        }

    }

    public class RespoDBLib 
    {
        public String FundSource { get; set; }
        public String Code { get; set; }
    }
    public class RespoActual 
    {
        public String FundSource { get; set; }
    }
}
