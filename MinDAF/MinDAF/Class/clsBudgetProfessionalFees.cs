﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsBudgetProfessionalFees
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();

        public String FetchRateProfessionalExpense()
        {
            StringBuilder sb = new StringBuilder(237);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mel.item_code,");
            sb.AppendLine(@" CONCAT(mel.item_name,' - ',FORMAT(mel.rate,'N2')) as item_name,");
            sb.AppendLine(@"mel.rate,");
            sb.AppendLine(@"mel.rate_year");
            sb.AppendLine(@"FROM mnda_mooe_sub_expenditures mmo_sub");
            sb.AppendLine(@"INNER JOIN mnda_expenditures_library mel on mel.sub_expenditure_code = mmo_sub.id");
            sb.AppendLine(@"WHERE mmo_sub.id = 51 and mmo_sub.is_active = 1 ORDER BY mel.item_name ASC");


            return sb.ToString();
        }

        public void SaveProjectProfessionalFee(String _activity_id, String _accountable_id, String _remarks,String _no_staff, double _rate,double _total, String _month, String _year, string mooe_id, string no_days,string fund_source_id,string isrev
          )
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(285);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  mnda_activity_data");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  activity_id,");
            sb.AppendLine(@"  accountable_id,");
            sb.AppendLine(@"  remarks,");
            sb.AppendLine(@"  start,");
            sb.AppendLine(@"  [end],");
            sb.AppendLine(@"  destination,");
            sb.AppendLine(@"  no_staff,");
            sb.AppendLine(@"  rate,");
            sb.AppendLine(@"  travel_allowance,");
            sb.AppendLine(@"  total,");
            sb.AppendLine(@"  month,");
            sb.AppendLine(@"  year,mooe_sub_expenditure_id,no_days,fund_source_id,is_revision");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '" + _activity_id + "',");
            sb.AppendLine(@"  '" + _accountable_id + "',");
            sb.AppendLine(@" '" + _remarks + "',");
            sb.AppendLine(@" NULL,");
            sb.AppendLine(@" NULL,");
            sb.AppendLine(@"  '',");
            sb.AppendLine(@" " + _no_staff + ",");
            sb.AppendLine(@" " + _rate + ",");
            sb.AppendLine(@"  0,");
            sb.AppendLine(@"  " + _total + ",");
            sb.AppendLine(@" '" + _month + "',");
            sb.AppendLine(@" " + _year + ",");
            sb.AppendLine(@" '" + mooe_id + "',");
            sb.AppendLine(@" '" + no_days + "',");
            sb.AppendLine(@" '" + fund_source_id + "',");
            sb.AppendLine(@" " + isrev + "");
            sb.AppendLine(@");");


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public String FetchLocalData(string _activity_id, string _month, string year, String mooe_id,String fund_source_id)
        {
            StringBuilder sb = new StringBuilder(526);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mdad.id as ActId,");
            sb.AppendLine(@"mda.description as Activity,");
            sb.AppendLine(@"mdad.accountable_id as Assigned,");
            sb.AppendLine(@"mdad.remarks as Remarks,");
            sb.AppendLine(@"ISNULL(mdad.start,'') as DateStart,");
            sb.AppendLine(@"ISNULL(mdad.[end],'') as DateEnd,");
            sb.AppendLine(@"mdad.destination as Destination,");
            sb.AppendLine(@"ISNULL(mdad.type_service,'') as Service_Type,");
            sb.AppendLine(@"ISNULL(mdad.stat_1,'') as Breakfast,");
            sb.AppendLine(@"ISNULL(mdad.stat_2,'') as AM_Snacks,");
            sb.AppendLine(@"ISNULL(mdad.stat_3,'') as Lunch,");
            sb.AppendLine(@"ISNULL(mdad.stat_4,'') as PM_Snacks,");
            sb.AppendLine(@"ISNULL(mdad.stat_5,'') as Dinner,");
            sb.AppendLine(@"ISNULL(mdad.no_days,'') as No_Days,");
            sb.AppendLine(@"mdad.no_staff as No_Staff,");
            sb.AppendLine(@"mdad.rate as Fare_Rate,");
            sb.AppendLine(@"mdad.travel_allowance as Travel_Allowance,");
            sb.AppendLine(@"mdad.total as Total");
            sb.AppendLine(@"FROM mnda_activity_data mdad");
            sb.AppendLine(@"INNER JOIN mnda_activity mda on mda.id = mdad.activity_id");
            sb.AppendLine(@"WHERe mdad.fund_source_id = '" + fund_source_id + "' AND mdad.activity_id = '" + _activity_id + "' and mdad.month = '" + _month + "' and mdad.year = " + year + " and mooe_sub_expenditure_id =" + mooe_id + " and is_suspended = 0");


            return sb.ToString();
        }
        public void UpdateSuspend(String _id, String _val)
        {


            String _sqlString = "";

            StringBuilder sb = new StringBuilder(79);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_activity_data  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  is_suspended =" + _val + ",status ='SUSPENDED-REVISION'");
           // sb.AppendLine(@"  is_suspended =" + _val + ",activity_id=activity_id + '-suspended',status ='SUSPENDED'");
            sb.AppendLine(@"WHERE id = " + _id + ";");
            //StringBuilder sb = new StringBuilder(79);
            //sb.AppendLine(@"UPDATE ");
            //sb.AppendLine(@"  dbo.mnda_activity_data  ");
            //sb.AppendLine(@"SET ");
            //sb.AppendLine(@"  is_suspended =" + _val + "");
            //sb.AppendLine(@"WHERE id = " + _id + ";");


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;

        }
        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "SaveData":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "Suspend":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;

            }
        }
    }
    public class ProfessionalRate
    {
        public String item_code { get; set; }
        public String item_name { get; set; }
        public Decimal rate { get; set; }
        public Decimal rate_year { get; set; }
    }
}
