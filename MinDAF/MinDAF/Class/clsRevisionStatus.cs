﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsRevisionStatus
    {

        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();

        public String FetchRevisionList() 
        {
            var sb = new System.Text.StringBuilder(309);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"");
            sb.AppendLine(@"div.Division_Desc as divname,");
            sb.AppendLine(@"CASE WHEN mar.is_approved=1  THEN 'APPROVED - For REVISION PPMP'");
            sb.AppendLine(@" ELSE  'FOR APPROVAL'  END as status");
            sb.AppendLine(@"");
            sb.AppendLine(@"FROM mnda_alignment_record mar");
            sb.AppendLine(@"INNER JOIN Division div on div.Division_Id = mar.division_pap");
            sb.AppendLine(@"WHERE mar.is_active = 1");
            sb.AppendLine(@"GROUP BY div.Division_Desc,mar.is_approved");


            return sb.ToString();
        }

    }

    public class RevisionStatus 
    {
        public String DivisionName { get; set; }
        public String Status { get; set; }
    }

}
