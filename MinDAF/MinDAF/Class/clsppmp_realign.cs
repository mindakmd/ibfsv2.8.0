﻿using MinDAF.MinDAFS;
using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsppmp_realign
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();
        MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();

        public String FetchBudgetAllocation(String _division_id,String _year)
        {
            var sb = new System.Text.StringBuilder(230);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"msub.uacs_code,");
            sb.AppendLine(@"msub.name as expense_item,");
             sb.AppendLine(@"( CASE WHEN mfsl.plusminus ='+' ");
            sb.AppendLine(@"		THEN (mfsl.Amount + mfsl.adjustment) ");
            sb.AppendLine(@"        ELSE (mfsl.Amount - mfsl.adjustment) End ) as Amount");
            sb.AppendLine(@"FROM mnda_fund_source_line_item mfsl ");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures msub on msub.uacs_code = mfsl.mooe_id");
            sb.AppendLine(@"WHERE mfsl.division_id =" + _division_id + " and mfsl.Year = " + _year + " AND is_revision =0");
            sb.AppendLine(@"UNION");
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"msub.uacs_code,");
            sb.AppendLine(@"msub.name as expense_item,");
            sb.AppendLine(@"( CASE WHEN mfsl.plusminus ='+' ");
            sb.AppendLine(@"		THEN (mfsl.Amount + mfsl.adjustment) ");
            sb.AppendLine(@"        ELSE (mfsl.Amount - mfsl.adjustment) End ) as Amount");
            sb.AppendLine(@"FROM mnda_fund_source_line_item mfsl ");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures msub on msub.uacs_code = mfsl.mooe_id");
            sb.AppendLine(@"WHERE mfsl.division_id =" + _division_id + " and mfsl.Year = " + _year + " AND is_revision =1");

            return sb.ToString();
        }
        public String FetchBudgetAllocationRevApproval(String _division_id, String _year,String fund_type)
        {
            var sb = new System.Text.StringBuilder(230);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"msub.uacs_code,");
            sb.AppendLine(@"msub.name as expense_item,");
            sb.AppendLine(@"mfsl.Amount");
            sb.AppendLine(@"FROM mnda_fund_source_line_item mfsl ");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures msub on msub.uacs_code = mfsl.mooe_id");
            sb.AppendLine(@"INNER JOIN mnda_respo_library respo on respo.code = mfsl.Fund_Source_Id");
            sb.AppendLine(@"WHERE mfsl.division_id =" + _division_id + " and mfsl.Year = " + _year + " AND is_revision =0");
            sb.AppendLine(@"AND respo.fund_type = '" + fund_type + "'");
            sb.AppendLine(@"UNION");
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"msub.uacs_code,");
            sb.AppendLine(@"msub.name as expense_item,");
            sb.AppendLine(@"mfsl.Amount");
            sb.AppendLine(@"FROM mnda_fund_source_line_item mfsl ");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures msub on msub.uacs_code = mfsl.mooe_id");
            sb.AppendLine(@"INNER JOIN mnda_respo_library respo on respo.code = mfsl.Fund_Source_Id");
            sb.AppendLine(@"WHERE mfsl.division_id =" + _division_id + " and mfsl.Year = " + _year + " AND is_revision =1");
            sb.AppendLine(@"AND respo.fund_type = '" + fund_type + "'");
            return sb.ToString();
        }
        public String FetchBudgetAllocationV2(String _fund_source, String _year)
        {           
            var sb = new System.Text.StringBuilder(651);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"msub.uacs_code,");
            sb.AppendLine(@"msub.name as expense_item,");
            sb.AppendLine(@"mfsl.Amount");
            sb.AppendLine(@"FROM mnda_fund_source_line_item mfsl ");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures msub on msub.uacs_code = mfsl.mooe_id");
            sb.AppendLine(@"WHERE mfsl.Fund_Source_Id ='"+ _fund_source +"' ");
            sb.AppendLine(@"and mfsl.Year = "+ _year +" AND is_revision =0");
            sb.AppendLine(@"UNION");
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"msub.uacs_code,");
            sb.AppendLine(@"msub.name as expense_item,");
            sb.AppendLine(@"mfsl.Amount");
            sb.AppendLine(@"FROM mnda_fund_source_line_item mfsl ");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures msub on msub.uacs_code = mfsl.mooe_id");
            sb.AppendLine(@"WHERE mfsl.Fund_Source_Id ='"+ _fund_source +"' ");
            sb.AppendLine(@"and mfsl.Year = "+ _year +" AND is_revision =1");

            return sb.ToString();
        }
        public String FetchBudgetAdjustments(String _divid, String _year)
        {
            var sb = new System.Text.StringBuilder(229);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"raf.number,");
            sb.AppendLine(@"mfsa.mooe_id,");
            sb.AppendLine(@"mfsa.adjustment,");
            sb.AppendLine(@"mfsa.plusminus");
            sb.AppendLine(@"FROM mnda_raf_no raf");
            sb.AppendLine(@"INNER JOIN mnda_fund_source_adjustment mfsa on mfsa.raf_no = raf.number");
            sb.AppendLine(@"WHERE raf.div_id = "+ _divid +"  AND raf.year = "+ _year +" AND raf.cancelled =0");


            return sb.ToString();
        }
        public String FetchBudgetRafNo(String div_id, String _year,String mfType)
        {
            var sb = new System.Text.StringBuilder(105);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"raf.number");
            sb.AppendLine(@"FROM mnda_raf_no raf");
            sb.AppendLine(@"WHERE raf.div_id = "+div_id +"  AND raf.year = "+ _year +"  AND raf.cancelled =0 AND raf.mf_type ='"+ mfType +"' and raf.status ='Served'");

            return sb.ToString();
        }
        public String FetchBudgetRafNoForApproval(String div_id, String _year)
        {
            var sb = new System.Text.StringBuilder(105);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"raf.number");
            sb.AppendLine(@"FROM mnda_raf_no raf");
            sb.AppendLine(@"WHERE raf.div_id = " + div_id + "  AND raf.year = " + _year + "  AND raf.cancelled =0 AND raf.status ='For Approval'");

            return sb.ToString();
        }
        public String FetchRequestRevisions(String _division_id)
        {
            var sb = new System.Text.StringBuilder(181);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mar.from_uacs,");
            sb.AppendLine(@"mar.from_total,");
            sb.AppendLine(@"mar.to_uacs,");
            sb.AppendLine(@"mar.total_alignment");
            sb.AppendLine(@"FROM mnda_alignment_record mar");
            sb.AppendLine(@"WHERE mar.division_pap = "+ _division_id +" and mar.is_approved = 1 and mar.is_active = 1");


            return sb.ToString();
        }
        public String FetchApprovedRevisions(String _division_id)
        {
            var sb = new System.Text.StringBuilder(119);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"COUNT(mfn.id) as _count");
            sb.AppendLine(@"FROM   dbo.mnda_finance_notification mfn");
            sb.AppendLine(@"WHERE mfn.div_id ='" + _division_id + "';");

            return sb.ToString();
        }
          public String FetchExpenditureItems()
                {
                    var sb = new System.Text.StringBuilder(120);
                    sb.AppendLine(@"SELECT");
                    sb.AppendLine(@"msub.uacs_code,");
                    sb.AppendLine(@"msub.name");
                    sb.AppendLine(@"FROM mnda_mooe_sub_expenditures msub");
                    sb.AppendLine(@"WHERE msub.is_active = 1");
                    sb.AppendLine(@"ORDER BY msub.id ASC");



                    return sb.ToString();
                }

         public String FetchFundSource(String _division_id,String WorkingYear,String FSource)
                {
                    var sb = new System.Text.StringBuilder(94);
                    sb.AppendLine(@"SELECT");
                    sb.AppendLine(@"respo.respo_name,");
                    sb.AppendLine(@"respo.code,");
                    sb.AppendLine(@"respo.fund_type");
                    sb.AppendLine(@"FROM mnda_respo_library respo ");
                    sb.AppendLine(@"WHERE respo.div_id = " + _division_id + " AND respo.working_year ='" + WorkingYear + "' and respo.code= '"+ FSource +"' ");


                    return sb.ToString();
                }
         public String FetchFundSourceApproval(String _division_id, String fund_type,String WorkingYear)
         {
             var sb = new System.Text.StringBuilder(94);
             sb.AppendLine(@"SELECT");
             sb.AppendLine(@"respo.respo_name,");
             sb.AppendLine(@"respo.code");
             sb.AppendLine(@"FROM mnda_respo_library respo ");
             sb.AppendLine(@"WHERE respo.div_id = " + _division_id + " AND fund_type='" + fund_type + "' AND working_year ='"+ WorkingYear +"'");


             return sb.ToString();
         }
         public String FetchForRevision(String DivID)
         {
             var sb = new System.Text.StringBuilder(99);
             sb.AppendLine(@"SELECT ");
             sb.AppendLine(@"COUNT(mfn.id) as _count");
             sb.AppendLine(@"FROM   dbo.mnda_finance_notification mfn");
             sb.AppendLine(@"WHERE mfn.div_id ='" + DivID + "' AND mfn.stats ='Active' AND mfn.message ='Request for Revision Modification From ';");


             return sb.ToString();
         }
         public String FetchRevisionStatus(String DivID,String _Year)
         {
             var sb = new System.Text.StringBuilder(103);
             sb.AppendLine(@"SELECT");
             sb.AppendLine(@"raf.status");
             sb.AppendLine(@"FROm mnda_raf_no raf");
             sb.AppendLine(@"WHERE raf.div_id ="+ DivID +" and raf.year="+ _Year +" ");
             sb.AppendLine(@"and raf.cancelled = 0");


             return sb.ToString();
         }
         public String FetchRAFRevision(String DivID)
         {
             //var sb = new System.Text.StringBuilder(66);
             //sb.AppendLine(@"SELECT ");
             //sb.AppendLine(@"  id,");
             //sb.AppendLine(@"  number,");
             //sb.AppendLine(@"  entry_date,");
             //sb.AppendLine(@"  alignment_id");
             //sb.AppendLine(@"FROM ");
             //sb.AppendLine(@"  dbo.mnda_raf_no WHERE div_id ='"+DivID +"';");
             var sb = new System.Text.StringBuilder(253);
             sb.AppendLine(@"SELECT ");
             sb.AppendLine(@"  mrn.id,");
             sb.AppendLine(@"  mrn.number,");
             sb.AppendLine(@"  mrn.entry_date,");
             sb.AppendLine(@"  mrn.alignment_id,");
             sb.AppendLine(@"  mrn.status");
             sb.AppendLine(@"FROM    dbo.mnda_raf_no mrn");
             sb.AppendLine(@"INNER JOIN mnda_finance_notification mfn on mfn.div_id = mrn.div_id");
             sb.AppendLine(@"WHERE mrn.div_id ='"+ DivID +"' AND mfn.message ='Request for Revision Modification From ';");



             return sb.ToString();
         }
         public String FetchRevisions(String fundSource,String DivID,String _Year)
         {
             var sb = new System.Text.StringBuilder(403);
             sb.AppendLine(@"SELECT");
             sb.AppendLine(@"mar.division_pap,");
             sb.AppendLine(@"mar.division_year,");
             sb.AppendLine(@"mar.from_total,");
             sb.AppendLine(@"mar.from_uacs,");
             sb.AppendLine(@"mo_sub_from.name as from_expense_item,");
             sb.AppendLine(@"mar.fundsource,");
             sb.AppendLine(@"mar.to_uacs,");
             sb.AppendLine(@"mo_sub.name as to_expense_item,");
             sb.AppendLine(@"mar.total_alignment,");
             sb.AppendLine(@"mar.is_approved");
             sb.AppendLine(@"FROM mnda_alignment_record mar");
             sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mo_sub on mo_sub.uacs_code =mar.to_uacs");
             sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mo_sub_from on mo_sub_from.uacs_code =mar.from_uacs");
             sb.AppendLine(@"WHERE mar.division_pap = " + DivID + " AND mar.division_year = " + _Year + "");
             sb.AppendLine(@"AND mar.fundsource ='" + fundSource + "' AND mar.is_active = 1");// AND mar.is_approved = 0");

             return sb.ToString();
         }
         public String FetchRevisionsApproval(String fundSource, String DivID, String _Year)
         {
             var sb = new System.Text.StringBuilder(587);
             sb.AppendLine(@"SELECT");
             sb.AppendLine(@"mar.division_pap,");
             sb.AppendLine(@"mar.division_year,");
             sb.AppendLine(@"mar.from_total,");
             sb.AppendLine(@"mar.from_uacs,");
             sb.AppendLine(@"mo_sub_from.name as from_expense_item,");
             sb.AppendLine(@"mar.fundsource,");
             sb.AppendLine(@"mar.to_uacs,");
             sb.AppendLine(@"mo_sub.name as to_expense_item,");
             sb.AppendLine(@"mar.total_alignment,");
             sb.AppendLine(@"mar.is_approved");
             sb.AppendLine(@"FROM mnda_alignment_record mar");
             sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mo_sub on mo_sub.uacs_code =mar.to_uacs");
             sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mo_sub_from on mo_sub_from.uacs_code =mar.from_uacs");
             sb.AppendLine(@"WHERE mar.division_pap = " + DivID + " AND mar.division_year = " + _Year + "");
             sb.AppendLine(@"AND mar.fundsource ='" + fundSource + "' AND mar.is_active = 1");// AND mar.is_approved = 0");
             sb.AppendLine(@"GROUP BY ");
             sb.AppendLine(@"mar.division_pap,");
             sb.AppendLine(@"mar.division_year,");
             sb.AppendLine(@"mar.from_total,");
             sb.AppendLine(@"mar.from_uacs,");
             sb.AppendLine(@"mo_sub_from.name,");
             sb.AppendLine(@"mar.fundsource,");
             sb.AppendLine(@"mar.to_uacs,");
             sb.AppendLine(@"mo_sub.name,");
             sb.AppendLine(@"mar.total_alignment,");
             sb.AppendLine(@"mar.is_approved");

             return sb.ToString();
         }
         public String FetchRevisionsForApproval(String fundSource, String DivID, String _Year)
         {
             var sb = new System.Text.StringBuilder(403);
             sb.AppendLine(@"SELECT");
             sb.AppendLine(@"mar.division_pap,");
             sb.AppendLine(@"mar.division_year,");
             sb.AppendLine(@"mar.from_total,");
             sb.AppendLine(@"mar.from_uacs,");
             sb.AppendLine(@"mo_sub_from.name as from_expense_item,");
             sb.AppendLine(@"mar.fundsource,");
             sb.AppendLine(@"mar.to_uacs,");
             sb.AppendLine(@"mo_sub.name as to_expense_item,");
             sb.AppendLine(@"mar.total_alignment,");
             sb.AppendLine(@"mar.is_approved");
             sb.AppendLine(@"FROM mnda_alignment_record mar");
             sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mo_sub on mo_sub.uacs_code =mar.to_uacs");
             sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mo_sub_from on mo_sub_from.uacs_code =mar.from_uacs");
             sb.AppendLine(@"WHERE mar.division_pap = " + DivID + " AND mar.division_year = " + _Year + "");
             sb.AppendLine(@"AND mar.fundsource ='" + fundSource + "' AND mar.is_active = 1");// AND mar.is_approved = 0");

             return sb.ToString();
         }
        public String FetchRevisionTitles(String fundSource,String DivID,String _Year)
         {
             var sb = new System.Text.StringBuilder(234);
             sb.AppendLine(@"SELECT");
             sb.AppendLine(@"mar.title");            
             sb.AppendLine(@"FROM mnda_alignment_record mar");
             sb.AppendLine(@"WHERE mar.division_pap = " + DivID + " AND mar.division_year = " + _Year + " and is_active =1 AND mar.is_approved = 0");
             sb.AppendLine(@"AND mar.fundsource ='"+ fundSource +"'");
             sb.AppendLine(@"GROUP BY mar.title");

             return sb.ToString();
         }
         public String FetchActivityData(String fundSource)
         {
             var sb = new System.Text.StringBuilder(375);
             sb.AppendLine(@"SELECT");
             sb.AppendLine(@"msub.uacs_code,");
             sb.AppendLine(@"msub.name,");
             sb.AppendLine(@"SUM(mad.total) as total");
             sb.AppendLine(@"FROM mnda_activity_data mad ");
             sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures msub on msub.id = mad.mooe_sub_expenditure_id");
             sb.AppendLine(@"WHERE mad.fund_source_id = '"+ fundSource +"'");
             sb.AppendLine(@"and mad.status = 'FINANCE APPROVED'");
             sb.AppendLine(@"GROUP BY msub.uacs_code,msub.name,msub.id ORDER BY msub.id ASC");


             return sb.ToString();
         }

         public String FetchActivityDataWithMonths(String fundSource)
         {
             var sb = new System.Text.StringBuilder(375);
             sb.AppendLine(@"SELECT");
             sb.AppendLine(@"msub.uacs_code,");
             sb.AppendLine(@"msub.name,");
             sb.AppendLine(@"mad.month,");
             sb.AppendLine(@"mad.total");
             sb.AppendLine(@"FROM mnda_activity_data mad ");
             sb.AppendLine(@"LEFT JOIN mnda_activity mda on mda.id = mad.activity_id");
             sb.AppendLine(@"LEFT JOIN mnda_project_output mpo on mpo.id = mda.output_id");
             sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
             sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures msub on msub.id = mad.mooe_sub_expenditure_id");
             sb.AppendLine(@"WHERE mad.fund_source_id = '" + fundSource + "'");
             sb.AppendLine(@"and mad.status = 'FINANCE APPROVED'");


             return sb.ToString();
         }
    }

    public class DivisionAdjustment 
    {
        public String Raf { get; set; }
        public String Mooe_Id { get; set; }
        public String Adjustments { get; set; }
        public String PlusMinus { get; set; }

    }

    public class RafForApproval 
    {
        public String Raf { get; set; }
    }

    public class DivisionRafNo 
    {
        public String Raf { get; set; }
    }
    public class RafStatus 
    {
        public String Status { get; set; }
    }
    public class RAFApprovedRevisions 
    {
           public String from_uacs{ get; set; }
           public String from_total{ get; set; }
           public String to_uacs{ get; set; }
           public String total_alignment { get; set; }
    }
    public class RafRevision 
    {
        public String Id { get; set; }
        public String RafNo { get; set; }
        public String EntryDate { get; set; }
        public String AlignId { get; set; }
    }
    public class BudgetRevisionCount 
    {
        public String _Count { get; set; }
    }
    public class BudgetRevisionTitles 
    {
        public String Title { get; set; }
    }
    public class BudgetRevisionItems 
    {
          public String division_pap{ get; set; }
          public String division_year{ get; set; }
          public String from_total{ get; set; }
          public String from_uacs{ get; set; }
         public String from_expense_item { get; set; }
          public String fundsource{ get; set; }
          public String to_uacs{ get; set; }
         public String to_expense_item{ get; set; }
          public String total_alignment { get; set; }
          public String expense_item { get; set; }
          public String is_approved { get; set; }
    }
    public class BudgetRevisedData 
    {
        public String from_uacs { get; set; }
        public String from_total { get; set; }
        public String to_uacs { get; set; }
        public String total_alignment { get; set; }
        public String division_pap { get; set; }
        public String division_year { get; set; }
        public String months { get; set; }
        public String f_source { get; set; }
    }

    public class BudgetReviseExpenditureItems
    {
        
        public String  uacs_code{get;set;}
        public String  name{get;set;}
    }

    public class BudgetActivityItems 
    {
        public String uacs_code { get; set; }
        public String name { get; set; }
        public String total { get; set; }

    }

    public class BudgetActivityItemsMonths
    {
        public String uacs_code { get; set; }
        public String name { get; set; }
        public String month { get; set; }
        public String total { get; set; }

    }
    public class BudgetRevision 
    {
        public String Uacs { get; set; }
        public String ExpenseItem { get; set; }
        public String Amount { get; set; }
    }
    public class BudgetRevisionFundSource
    {
        public String Name { get; set; }
        public String Code { get; set; }
        public String FundType { get; set; }
    }

    public class BudgetRevisionMain
    {
        public String Status { get; set; }
        public String UACS { get; set; }
        public String ExpenseItem { get; set; }

        public String Allocation { get; set; }
        public String TotalOBR { get; set; }
        public String RemainingBalance { get; set; }
        public String Adjustment { get; set; }
        public String TotalExpense { get; set; }
       

       

        public String Jan { get; set; }
        public String Feb { get; set; }
        public String Mar { get; set; }
        public String Apr { get; set; }
        public String May { get; set; }
        public String Jun { get; set; }
        public String Jul { get; set; }
        public String Aug { get; set; }
        public String Sep { get; set; }
        public String Oct { get; set; }
        public String Nov { get; set; }
        public String Dec { get; set; }

        public String TotalAmount { get; set; }


    }
}
