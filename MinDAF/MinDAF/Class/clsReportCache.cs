﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsReportCache
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();

        public void CacheFDReport(string report_command,string division_id,string _year)
        {
            String _sqlString = "";

            var sb = new System.Text.StringBuilder(179);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_fd_report");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  report_command,");
            sb.AppendLine(@"  division_id,");
            sb.AppendLine(@"  requested_year");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '"+ report_command +"',");
            sb.AppendLine(@"  '"+ division_id +"',");
            sb.AppendLine(@"  '"+ _year +"'");
            sb.AppendLine(@");");

            _sqlString += sb.ToString();


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }

        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "Division Distribution":
                    if (SQLOperation!=null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
