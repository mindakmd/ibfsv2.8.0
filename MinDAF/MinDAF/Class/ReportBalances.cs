﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class ReportReportBalances
    {
        public String UACS { get; set; }
        public String ExpenseItem { get; set; }
        public String Allocation { get; set; }
        public String ActualOBR { get; set; }
        public String Balances { get; set; }
    }
}
