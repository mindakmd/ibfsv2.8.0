﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsreAlignment
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();
        private clsServiceOperation c_ops_new = new clsServiceOperation();



        public String FetchProcuredBudget(string _div_id, string fund_id)
        {


            var sb = new System.Text.StringBuilder(393);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mad.accountable_id as User_Fullname,");
            sb.AppendLine(@"mad.year as fiscal_year,");
            sb.AppendLine(@"mad.activity_id,");
            sb.AppendLine(@"moe.uacs_code,");
            sb.AppendLine(@"moe.name,");
            sb.AppendLine(@"SUM(mad.total) as total");
            sb.AppendLine(@"FROM mnda_activity_data mad ");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures moe on moe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_activity ma on ma.id = mad.activity_id");
            sb.AppendLine(@"LEFT JOIN mnda_project_output mpo on mpo.id = ma.output_id");
            sb.AppendLine(@"LEFT JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
            sb.AppendLine(@"WHERE mpe.acountable_division_code = "+ _div_id +"");
            sb.AppendLine(@" AND mad.fund_source_id = '" + fund_id + "'");
            sb.AppendLine(@"GROUP BY mad.accountable_id,mad.year,mad.activity_id,moe.uacs_code, moe.name");


            return sb.ToString();
        }
        public String FetchDataMBASummary(String _year, String pap,string _fundsource)
        {
            StringBuilder sb = new StringBuilder(320);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"MONTH(obr.DateCreated) as _month,");
            sb.AppendLine(@"lrc.Division as FundSource,");
            sb.AppendLine(@"lppa.PPACode as Office,");
            sb.AppendLine(@"la.AccountCode as uacs,");
            sb.AppendLine(@"la.AccountDescription as name,");
            sb.AppendLine(@"oorp.Amount as Total");
            sb.AppendLine(@"FROM or_ObligationRequestParticular oorp");
            sb.AppendLine(@"LEFT  JOIN or_ObligationRequest obr on obr.id = oorp.ObligationRequestID");
            sb.AppendLine(@"LEFT  JOIN lib_ResponsibilityCenter lrc on lrc.ID = oorp.ResponsibilityCenterID");
            sb.AppendLine(@"LEFT  JOIN lib_PPA lppa ON lppa.ID = oorp.PPAID");
            sb.AppendLine(@"LEFT  JOIN lib_Account la on la.ID = oorp.AcctID");
            sb.AppendLine(@"WHERE YEAR(obr.DateCreated) = " + _year + "   AND lppa.PPACode ='" + pap + "' AND lrc.Division ='" + _fundsource + "'");


            return sb.ToString();
        }
        public String FetchMooeList( )
                {

                    var sb = new System.Text.StringBuilder(323);
                    sb.AppendLine(@"SELECT ");
                    sb.AppendLine(@"  mooe.id,");
                    sb.AppendLine(@"  mooe.mooe_id,");
                    sb.AppendLine(@"  mooe.uacs_code,");
                    sb.AppendLine(@"  mooe.name,");
                    sb.AppendLine(@"  mooe.is_active,");
                    sb.AppendLine(@"  mooe.is_dynamic");
                    sb.AppendLine(@"FROM mnda_mooe_sub_expenditures mooe");
                    sb.AppendLine(@"LEFT JOIN mnda_mooe_expenditures mo on mo.code = mooe.mooe_id");
                    sb.AppendLine(@"LEFT JOIN mnda_type_service mts on mts.uacs = mo.type_service");
                    sb.AppendLine(@"WHERE mooe.is_active = 1 and mts.uacs ='50200000';");


                    return sb.ToString();
                }
        public String FetchFundSource(String div_id,String years)
        {

            var sb = new System.Text.StringBuilder(164);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@" code as Fund_Source_Id,");
            sb.AppendLine(@"  Fund_Name");
            sb.AppendLine(@"FROM mnda_fund_source mfs");
            sb.AppendLine(@"WHERE mfs.division_id =" + div_id + " AND mfs.Year = " + years  + ";");

            return sb.ToString();
        }
        public String FetchFundType()
        {

            var sb = new System.Text.StringBuilder(57);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  uacs,");
            sb.AppendLine(@"  name");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_type_service;");


            return sb.ToString();
        }
        public String FetchActivities(String div_id)
        {
            var sb = new System.Text.StringBuilder(215);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"ma.id,");
            sb.AppendLine(@"ma.description,mua.User_Fullname");
            sb.AppendLine(@"FROM mnda_activity ma");
            sb.AppendLine(@"LEFT JOIN mnda_project_output po on po.id = ma.output_id");
            sb.AppendLine(@"LEFT JOIN mnda_program_encoded mpe on mpe.id = po.program_code");
            sb.AppendLine(@"LEFT JOIN mnda_user_accounts mua  on mua.User_Id = ma.accountable_member_id");
            sb.AppendLine(@"WHERE mpe.acountable_division_code = "+ div_id +"");


            return sb.ToString();
        }

        public String FetchRealigned(String div_id,String _year)
        {
            var sb = new System.Text.StringBuilder(482);

            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  mar.id,");
            sb.AppendLine(@"  mar.from_uacs,");
            sb.AppendLine(@"  mooe.name name_from,");
            sb.AppendLine(@"  mar.from_total,");
            sb.AppendLine(@"  mar.to_uacs,");
            sb.AppendLine(@"  mooo.name name_to,");
            sb.AppendLine(@"  mar.total_alignment,");
            sb.AppendLine(@"  mar.division_pap,");
            sb.AppendLine(@"  mar.division_year,");
            sb.AppendLine(@"  mar.is_approved");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_alignment_record mar ");
            sb.AppendLine(@"  LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.uacs_code = mar.from_uacs");
            sb.AppendLine(@"  LEFT JOIN mnda_mooe_sub_expenditures mooo on mooo.uacs_code = mar.to_uacs");
            sb.AppendLine(@"WHERE mar.division_pap ='"+ div_id +"'  AND mar.division_year ='"+_year+"';");

            return sb.ToString();
        }

        public void RemoveAlignment(String _divid,String _years)
        {
            String _sqlString = "";
            var sb = new System.Text.StringBuilder(94);
            sb.AppendLine(@"UPDATE mnda_alignment_record ");
            sb.AppendLine(@"SET is_active = 0");
            sb.AppendLine(@"WHERE division_pap ='" + _divid + "' and division_year ='" + _years + "';");
            sb.AppendLine(@"DELETE FROM mnda_fund_source_line_item  WHERE division_id ='"+  _divid +"' AND is_revision = 1 AND Year ="+_years +";");
            sb.AppendLine(@"UPDATE mnda_fund_source_line_item SET adjustment =0 ,plusminus ='' WHERE division_id ='" + _divid + "' AND is_revision = 1 AND Year =" + _years + "");



            _sqlString = sb.ToString();

            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
     


        }
        public void RecordNewRafNo(String _year, String _month,String _alignment_id,String _div_id,String RafNo ="",String DateStamp ="",Boolean isRevision= false,String mf_type="")
        {
            String _sqlString = "";
            var sb = new System.Text.StringBuilder(163);
            if (isRevision)
            {
                sb.AppendLine(@"INSERT INTO ");
                sb.AppendLine(@"  dbo.mnda_raf_no");
                sb.AppendLine(@"(");
                sb.AppendLine(@"  year,");
                sb.AppendLine(@"  month,");
                sb.AppendLine(@"  number,");
                sb.AppendLine(@"  alignment_id,div_id,entry_date,mf_type,status");
                sb.AppendLine(@") ");
                sb.AppendLine(@"VALUES (");
                sb.AppendLine(@"  " + _year + ",");
                sb.AppendLine(@"  " + _month + ",");
                sb.AppendLine(@"  "+ RafNo +",");
                sb.AppendLine(@"  '" + _alignment_id + "','" + _div_id + "','"+ Convert.ToDateTime(DateStamp).ToString() +"',");
                sb.AppendLine(@"  '" + mf_type + "',");
                sb.AppendLine(@"  'For Approval'");
                sb.AppendLine(@");");

            }
            else
            {
                sb.AppendLine(@"INSERT INTO ");
                sb.AppendLine(@"  dbo.mnda_raf_no");
                sb.AppendLine(@"(");
                sb.AppendLine(@"  year,");
                sb.AppendLine(@"  month,");
                sb.AppendLine(@"  number,");
                sb.AppendLine(@"  alignment_id,div_id,mf_type,status");
                sb.AppendLine(@") ");
                sb.AppendLine(@"VALUES (");
                sb.AppendLine(@"  " + _year + ",");
                sb.AppendLine(@"  " + _month + ",");
                sb.AppendLine(@"  (SELECT (COUNT(number) + 1) FROM mnda_raf_no WHERE mf_type ='"+ mf_type+"'),");
                sb.AppendLine(@"  '" + _alignment_id + "','" + _div_id + "',");
                sb.AppendLine(@"  '" + mf_type + "',");
                sb.AppendLine(@"  'For Approval'");
                sb.AppendLine(@");");

            }
        


            _sqlString = sb.ToString();

            c_ops_new.InstantiateService();
            c_ops_new.ExecuteSQL(_sqlString);



        }
        public void RequestMod(String _div_id)
        {
            String _sqlString = "";
            var sb = new System.Text.StringBuilder(120);
            sb.AppendLine(@"UPDATE dbo.mnda_finance_notification SET stats = '-' WHERE div_id ='"+ _div_id +"';");
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_finance_notification");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  div_id,");
            sb.AppendLine(@"  message,");
            sb.AppendLine(@"  is_read,stats");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '" + _div_id + "',");
            sb.AppendLine(@" 'Request for Revision Modification From ',");
            sb.AppendLine(@"  0,'Active'");
            sb.AppendLine(@");");



            _sqlString = sb.ToString();

            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
    

        }
        public void SendNotifyRevision(String _div_id)
        {
            String _sqlString = "";
            var sb = new System.Text.StringBuilder(120);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_finance_notification");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  div_id,");
            sb.AppendLine(@"  message,");
            sb.AppendLine(@"  is_read,stats");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '" + _div_id + "',");
            sb.AppendLine(@" 'New Approval For Realignment from ',");
            sb.AppendLine(@"  0,'Active'");
            sb.AppendLine(@");");



            _sqlString = sb.ToString();

            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }

        void c_ops_new_DataReturn(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public void UpdateAlignment(String DivId,String Years)
        {
            String _sqlString = "";
            var sb = new System.Text.StringBuilder(126);
            sb.AppendLine(@"UPDATE");
            sb.AppendLine(@"mnda_alignment_record");
            sb.AppendLine(@"SET is_approved =0,is_active = 0");
            sb.AppendLine(@"WHERE division_pap ='"+ DivId +"' and division_year='"+  Years +"' ");


            _sqlString = sb.ToString();

            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn+=c_ops_DataReturn;
          


        }
        public void SaveAlignment(String from_uacs,
            String from_total,
            String to_uacs,
            String total_alignment,
            String division_pap,
            String division_year, String months, String fundsource,String title = "")
        {
            String _sqlString = "";
            var sb = new System.Text.StringBuilder(291);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_alignment_record");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  from_uacs,");
            sb.AppendLine(@"  from_total,");
            sb.AppendLine(@"  to_uacs,");
            sb.AppendLine(@"  total_alignment,");
            sb.AppendLine(@"  division_pap,");
            sb.AppendLine(@"  division_year,");
            sb.AppendLine(@"  is_approved,months,fundsource,title,is_active");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@" '"+from_uacs+"',");
            sb.AppendLine(@"  '"+from_total+"',");
            sb.AppendLine(@"  '"+to_uacs+"',");
            sb.AppendLine(@"  '"+total_alignment+"',");
            sb.AppendLine(@"  '"+division_pap+"',");
            sb.AppendLine(@"  '"+division_year+"',");
            sb.AppendLine(@"  0,");
            sb.AppendLine(@"  '" + months + "',");
            sb.AppendLine(@"  '" + fundsource + "',");
            sb.AppendLine(@"  '" + title + "',");
            sb.AppendLine(@"  '1'");
            sb.AppendLine(@");");


            _sqlString = sb.ToString();

            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void RemoveAlignment(String id)
        {
            String _sqlString = "";
            var sb = new System.Text.StringBuilder(56);
            sb.AppendLine(@"DELETE FROM ");
            sb.AppendLine(@"  dbo.mnda_alignment_record ");
            sb.AppendLine(@"WHERE id = "+ id +";");



            _sqlString = sb.ToString();

            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        private Boolean isProcessed = false;
        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "UpdateAlignment":
                    if (SQLOperation != null)
                    {
                        if (isProcessed)
                        {
                            isProcessed = false;
                            return;
                        }
                        SQLOperation(this, new EventArgs());
                        isProcessed = true;
                    }
                    break;
                case "SaveAlignment":
                    if (SQLOperation!=null)
                    {
                        if (isProcessed)
                        {
                            isProcessed = false;
                            return;
                        }
                        SQLOperation(this, new EventArgs());
                        isProcessed = true;
                    }
                    break;
                case "RemoveAlignment":
                    if (SQLOperation!=null)
                    {
                        if (isProcessed)
                        {
                            isProcessed = false;
                            return;
                        }
                        SQLOperation(this, new EventArgs());
                        isProcessed = true;
                    }
                    break;
                case "RequestRevision":
                    if (SQLOperation != null)
                    {
                        if (isProcessed)
                        {
                            isProcessed = false;
                            return;
                        }
                        SQLOperation(this, new EventArgs());
                        isProcessed = true;
                    }
                    break;
                default:
                    break;
            }
        }


    }

    public class rel_ProcuredItems
    {
        public String fiscal_year { get; set; }
        public String User_Fullname { get; set; }
             public String activity_id {get;set;}
             public String uacs_code { get; set; }
        public String name  {get;set;}
        public String total { get; set; }
    }
    public class rel_MOOELIST
    {
          public String     id { get; set; }
          public String  mooe_id { get; set; }
          public String uacs_code { get; set; }
          public String name { get; set; }
          public String is_active { get; set; }
          public String is_dynamic { get; set; }
    }
    public class rel_FundSource 
    {
         public String      Fund_Source_Id { get; set; }
         public String    Fund_Name { get; set; }
    }
    public class rel_Activity
    {
        public String Id { get; set; }
        public String Name { get; set; }
        public String User { get; set; }
    }
    public class rel_ForAlignment
    {
        public String Uacs { get; set; }
        public String Name { get; set; }
        public String Total { get; set; }
        public String Months { get; set; }
    }
    public class rel_AlignmentData
    {
        public String id { get; set; }
        public String from_uacs { get; set; }
        public String name_from { get; set; }
        public String from_total { get; set; }
        public String to_uacs { get; set; }
        public String name_to { get; set; }
         
        
         
         public String  total_alignment { get; set; }
          public String  division_pap { get; set; }
         public String  division_year { get; set; }
         public String is_approved { get; set; }

    }
}
