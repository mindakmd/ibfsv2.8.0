﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsRafList
    {

        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();


        public String GetRafData(string _divId)
        {
            var sb = new System.Text.StringBuilder(175);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"	raf.id,CONCAT(raf.year,'-',FORMAT(raf.month,'00'),'-',FORMAT(raf.number,'0000')) as raf_no,");
            sb.AppendLine(@"    raf.alignment_id as description,raf.cancelled");
            sb.AppendLine(@"FROM mnda_raf_no raf");
            sb.AppendLine(@"WHERE raf.div_id = " + _divId + "");



            return sb.ToString();
        }
        public String GetRafMonitoringData()
        {
            var sb = new System.Text.StringBuilder(341);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@" CONCAT(raf.year,'-',FORMAT(raf.month,'00'),'-',FORMAT(raf.number,'0000')) as raf_no,");
            sb.AppendLine(@" div.Division_Desc as respo_center,");
            sb.AppendLine(@" (CASE WHEN raf.cancelled =0");
            sb.AppendLine(@"       THEN 'Recorded'");
            sb.AppendLine(@" 	  ELSE");
            sb.AppendLine(@"      	'Cancelled' END) as status");
            sb.AppendLine(@"FROM  dbo.mnda_raf_no raf ");
            sb.AppendLine(@"INNER JOIN Division div on div.Division_Id = raf.div_id");
            sb.AppendLine(@"order by raf.number ASC");

            return sb.ToString();
        }
        public String GetRafDetails()
        {
      
            var sb = new System.Text.StringBuilder(469);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"CONCAT(raf.year,'-',FORMAT(raf.month,'00'),'-',FORMAT(raf.number,'0000')) as raf_no,");
            sb.AppendLine(@"div.Division_Desc,");
            sb.AppendLine(@"raf.alignment_id,");
            sb.AppendLine(@"mar.is_approved,");
            sb.AppendLine(@"raf.cancelled");
            sb.AppendLine(@"FROM mnda_raf_no raf");
            sb.AppendLine(@"INNER JOIN mnda_alignment_record mar on mar.title = raf.alignment_id");
            sb.AppendLine(@"INNER JOIN Division div on div.Division_Id = raf.div_id");
            sb.AppendLine(@"GROUP BY CONCAT(raf.year,'-',FORMAT(raf.month,'00'),'-',FORMAT(raf.number,'0000')) ,");
            sb.AppendLine(@"div.Division_Desc,");
            sb.AppendLine(@"raf.alignment_id,");
            sb.AppendLine(@"mar.is_approved,raf.cancelled");



            return sb.ToString();
        }

    }

    public class RafDetails 
    {
        public String raf_no { get; set; }
        public String Division_Desc { get; set; }
        public String alignment_id { get; set; }
        public String is_approved { get; set; }
        public String cancelled { get; set; }
    }

    public class RafData 
    {
        public String Id { get; set; }
        public String RafNo { get; set; }
        public String Description { get; set; }
        public String Status { get; set; }
    }
    public class RafMonitor 
    {
        public String raf_no { get; set; }
    
        public String respo_center { get; set; }
        public String status { get; set; }
    }


}
