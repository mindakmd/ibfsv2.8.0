﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsProcurement
    {

        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();


        public String FetchUnitOfMeasure() 
        {
            StringBuilder sb = new StringBuilder(96);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  mpi.unit_of_measure");
            sb.AppendLine(@"FROM mnda_procurement_items mpi");
            sb.AppendLine(@"GROUP BY mpi.unit_of_measure");
            sb.AppendLine(@";");

            return sb.ToString();
        }

        public String FetchMindaInventory() 
        {
            StringBuilder sb = new StringBuilder(134);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  mpi.minda_inventory");
            sb.AppendLine(@"FROM mnda_procurement_items mpi");
            sb.AppendLine(@"WHERE NOT mpi.minda_inventory =  'Y'");
            sb.AppendLine(@"GROUP BY mpi.minda_inventory");
            sb.AppendLine(@";");

            return sb.ToString();
        }
        public String FetchSubCategory() 
        {
            StringBuilder sb = new StringBuilder(90);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  mpi.sub_category");
            sb.AppendLine(@"FROM mnda_procurement_items mpi");
            sb.AppendLine(@"GROUP BY mpi.sub_category");
            sb.AppendLine(@";");

            return sb.ToString();
        }

        public String FetchGeneralCategory() 
        {
            StringBuilder sb = new StringBuilder(94);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  general_category");
            sb.AppendLine(@"FROM mnda_procurement_items mpi");
            sb.AppendLine(@"GROUP BY mpi.general_category");
            sb.AppendLine(@";");

            return sb.ToString();
        }
        public String FetchProcurementItems() 
        {
            StringBuilder sb = new StringBuilder(163);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  item_specifications,");
            sb.AppendLine(@"  general_category,");
            sb.AppendLine(@"  sub_category,");
            sb.AppendLine(@"  minda_inventory,");
            sb.AppendLine(@"  unit_of_measure,");
            sb.AppendLine(@"  price");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_procurement_items;");

            return sb.ToString();
        }

        public void SaveProjectRepresentation(String _item_specifications,String _general_category,String _subcategory,String _minda_inventory,String _unit_of_measure,String _price)
        {
            String _sqlString = "";
            StringBuilder sb = new StringBuilder(214);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_procurement_items");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  item_specifications,");
            sb.AppendLine(@"  general_category,");
            sb.AppendLine(@"  sub_category,");
            sb.AppendLine(@"  minda_inventory,");
            sb.AppendLine(@"  unit_of_measure,");
            sb.AppendLine(@"  price");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '"+ _item_specifications +"',");
            sb.AppendLine(@"  '"+ _general_category +"',");
            sb.AppendLine(@"  '"+ _subcategory +"',");
            sb.AppendLine(@"  '"+ _minda_inventory +"',");
            sb.AppendLine(@"  '"+ _unit_of_measure +"',");
            sb.AppendLine(@"  "+ _price +"");
            sb.AppendLine(@");");

    

            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void UpdatProcurementItem(String _Id, String _item_specifications, String _general_category, String _subcategory, String _minda_inventory, String _unit_of_measure, String _price)
        {
            String _sqlString = "";
            StringBuilder sb = new StringBuilder(203);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_procurement_items  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@" ");
            sb.AppendLine(@"  item_specifications = '"+ _item_specifications +"',");
            sb.AppendLine(@"  general_category = '"+ _general_category +"',");
            sb.AppendLine(@"  sub_category = '"+ _subcategory +"',");
            sb.AppendLine(@"  minda_inventory = '"+ _minda_inventory +"',");
            sb.AppendLine(@"  unit_of_measure = '"+ _unit_of_measure +"',");
            sb.AppendLine(@"  price = "+ _price +"");
            sb.AppendLine(@"WHERE  id = " + _Id + " ;");


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "SaveData":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateData":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                default:
                    break;
            }
        }
    }
    public class ProcurementUnitOfMeasure 
    {
        public String unit_of_measure { get; set; }
    }
    public class ProcurementMindaInventory
    {
        public String minda_inventory { get; set; }
    }
    public class ProcurementSubCategory 
    {
        public String sub_category { get; set; }
    }
    public class ProcurementGeneralCategory 
    {
        public String general_category { get; set; }
    }
    public class ProcurementItemLists 
    {
            public Int32  id {get;set;}
            public String  item_specifications {get;set;}
            public String  general_category {get;set;}
            public String  sub_category {get;set;}
            public String  minda_inventory {get;set;}
            public String  unit_of_measure {get;set;}
            public Decimal  price { get; set; }
    }
}
