﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsFinanceApproval
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();

        public String LoadPAP()
        {

            StringBuilder sb = new StringBuilder(109);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  DBM_Sub_Pap_id,");
            sb.AppendLine(@"  DBM_Pap_Id,");
            sb.AppendLine(@"  DBM_Sub_Pap_Code,");
            sb.AppendLine(@"  DBM_Sub_Pap_Desc");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.DBM_Sub_Pap;");

            return sb.ToString();

        }

        public String LoadSubPAP(String _pap)
        {

            var sb = new System.Text.StringBuilder(86);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Sub_Id,");
            sb.AppendLine(@"  DBM_Sub_Id,");
            sb.AppendLine(@"  Description,");
            sb.AppendLine(@"  PAP");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.DBM_Sub_Office WHERE DBM_Sub_Id ="+ _pap +";");


            return sb.ToString();

        }
        public String LoadDivision(string _PAP,String _year)
        {
            StringBuilder sb = new StringBuilder(101);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Division_Id,");
            sb.AppendLine(@"  DBM_Sub_Pap_Id,");
            sb.AppendLine(@"  Division_Code,");
            sb.AppendLine(@"  Division_Desc");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.Division WHERE Division_Code = '" + _PAP + "'  AND is_sub_division =0 AND year_setup ='"+ _year +"';");


            return sb.ToString();

        }
        public String LoadFundSource()
        {
            var sb = new System.Text.StringBuilder(76);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  code,");
            sb.AppendLine(@"  description");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_fund_source_types;");


            return sb.ToString();


        }
        public String LoadDivisionUnit(string _Id)
        {
            StringBuilder sb = new StringBuilder(101);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Division_Id,");
            sb.AppendLine(@"  DBM_Sub_Pap_Id,");
            sb.AppendLine(@"  Division_Code,");
            sb.AppendLine(@"  Division_Desc");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.Division WHERE UnitCode = '" + _Id + "'  AND is_sub_division =1;");


            return sb.ToString();


        }
        public String FetchDivisionExpenditureDetails(String _Assigned, String _year)
        {
          
            var sb = new System.Text.StringBuilder(480);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mad.fund_source_id,");
            sb.AppendLine(@"mosub.name as expenditure,");
            sb.AppendLine(@"SUM(mad.total) as total");
            sb.AppendLine(@"FROM mnda_activity_data mad");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.id = mad.activity_id");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.id = ma.output_id");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mosub on mosub.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_alignment_record mar on mar.fundsource = mad.fund_source_id");
            sb.AppendLine(@"WHERE   mad.accountable_id ='" + _Assigned + "'");
            sb.AppendLine(@"and mad.year = " + _year + " and mad.is_suspended = 0 and mar.is_approved = 0");
            sb.AppendLine(@"GROUP BY mad.fund_source_id,mosub.name");
            
            return sb.ToString();
        }
        public String FetchDivisionExpenditureDetailsByDiv(String _Assigned, String _year)
        {

            var sb = new System.Text.StringBuilder(566);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mosub.id,mad.fund_source_id,");
            sb.AppendLine(@"mosub.name as expenditure,");
            sb.AppendLine(@"SUM(mad.total) as total,");
            sb.AppendLine(@"mpe.acountable_division_code");
            sb.AppendLine(@"FROM mnda_activity_data mad");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.id = mad.activity_id");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.id = ma.output_id");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mosub on mosub.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
            sb.AppendLine(@"LEFT JOIN mnda_alignment_record mar on mar.fundsource = mad.fund_source_id");
            sb.AppendLine(@"WHERE  mad.year = " + _year + " and mad.is_suspended = 0 and mpe.acountable_division_code = " + _Assigned + "");
            sb.AppendLine(@"GROUP BY mosub.id,mpe.acountable_division_code, mad.fund_source_id,mosub.name");
            sb.AppendLine(@"ORDER BY mosub.id");

            return sb.ToString();
        }
        public String RevFetchDivisionExpenditureDetailsByDiv(String _Assigned, String _year)
        {

            var sb = new System.Text.StringBuilder(566);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mosub.id,mad.fund_source_id,");
            sb.AppendLine(@"mosub.name as expenditure,");
            sb.AppendLine(@"SUM(mad.total) as total,");
            sb.AppendLine(@"mpe.acountable_division_code");
            sb.AppendLine(@"FROM mnda_activity_data mad");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.id = mad.activity_id");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.id = ma.output_id");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mosub on mosub.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
            sb.AppendLine(@"LEFT JOIN mnda_alignment_record mar on mar.fundsource = mad.fund_source_id");
            sb.AppendLine(@"WHERE  mad.year = " + _year + " and mad.is_suspended = 0 and mpe.acountable_division_code = " + _Assigned + " and mar.is_approved = 0");
            sb.AppendLine(@"GROUP BY mosub.id,mpe.acountable_division_code, mad.fund_source_id,mosub.name");
            sb.AppendLine(@"ORDER BY mosub.id");

            return sb.ToString();
        }
        public void ApproveFinance(List<ChiefData_ItemLists> _Data)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(260);
            foreach (ChiefData_ItemLists item in _Data)
            {
                sb.AppendLine(@"UPDATE dbo.mnda_approved_projects_division SET  isapproved = 1 WHERE  id = " + item.id + ";");           
            }

           


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }

        public void RemoveAlignment(String _divid, String _years,string _fundsource)
        {
            String _sqlString = "";
            var sb = new System.Text.StringBuilder(94);
            sb.AppendLine(@"UPDATE mnda_alignment_record ");
            sb.AppendLine(@"SET is_approved = 0,is_active=0");
            sb.AppendLine(@"WHERE division_pap ='" + _divid + "' and division_year ='" + _years + "';");
            sb.AppendLine(@"DELETE FROM mnda_fund_source_line_item  WHERE division_id ='" + _divid + "' AND is_revision = 1 AND Year =" + _years + ";");
            sb.AppendLine(@"UPDATE mnda_fund_source_line_item SET adjustment =0 ,plusminus ='' WHERE division_id ='" + _divid + "' AND is_revision = 0 AND Year =" + _years + "");
            sb.AppendLine(@"UPDATE mnda_activity_data SET status ='FINANCE APPROVED', is_suspended = 0 WHERE fund_source_id ='" + _fundsource + "' AND status = 'SUSPENDED-REVISION' AND is_submitted = 1 AND is_approved = 1");




            _sqlString = sb.ToString();

            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn+=c_ops_DataReturn;


        }

        public void ApproveRevision(String RafNo, String _divid, String _year, String _fundsource, List<BudgetRevisionItems> _data, List<BudgetRevisionItems> _from, List<BudgetRevisionItems> _new)
        {
            String _sqlString = "";
            
            var sb = new System.Text.StringBuilder(97);
            sb.AppendLine(@"UPDATE mnda_alignment_record");
            sb.AppendLine(@"SET is_approved = 1");
            sb.AppendLine(@"WHERE division_pap = '"+ _divid +"' and division_year = '"+ _year +"' and is_active =1 and fundsource ='"+ _fundsource +"';");

            sb.AppendLine(@"UPDATE mnda_raf_no");
            sb.AppendLine(@"SET status = 'Served'");
            sb.AppendLine(@"WHERE div_id = '" + _divid + "' and year = '" + _year + "' and status ='For Approval';");


            foreach (var item in _from)
            {
                sb.AppendLine(@"INSERT INTO   dbo.mnda_fund_source_adjustment");
                sb.AppendLine(@"( fund_code,mooe_id,adjustment,plusminus,raf_no,status) ");
                sb.AppendLine(@"VALUES ");
                sb.AppendLine(@"( '" + _fundsource + "','" + item.from_uacs + "'," + item.total_alignment + ",'-','" + RafNo + "','1');");
                    
                //_sqlString += sb.ToString();
            }
            foreach (var item in _data)
            {
                sb.AppendLine(@"INSERT INTO   dbo.mnda_fund_source_adjustment");
                sb.AppendLine(@"( fund_code,mooe_id,adjustment,plusminus,raf_no,status) ");
                sb.AppendLine(@"VALUES ");
                sb.AppendLine(@"( '" + _fundsource + "','" + item.to_uacs + "'," + item.total_alignment + ",'+','" + RafNo + "','1');");
                    

                //sb.AppendLine(@"UPDATE mnda_fund_source_line_item");
                //sb.AppendLine(@"SET Amount = (CASE WHEN plusminus ='-' THEN (Amount-adjustment) ELSE (Amount+adjustment)END),");
                //sb.AppendLine(@"adjustment = " + item.total_alignment + ",plusminus='+'");
                //sb.AppendLine(@"WHERE Fund_Source_Id = '" + _fundsource + "' and division_id = '" + _divid + "'");
                //sb.AppendLine(@"and mooe_id = '" + item.to_uacs + "' and  Year =" + _year + ";");
                //_sqlString += sb.ToString();
            }

            foreach (var item in _new)
            {
                sb.AppendLine(@"INSERT INTO dbo.mnda_fund_source_line_item (Fund_Source_Id,division_id, mooe_id, Amount, Year, expenditure_type, adjustment,is_revision,plusminus) ");
                sb.AppendLine(@"VALUES ('" + _fundsource + "','" + _divid + "','" + item.to_uacs + "'," + item.total_alignment + "," + _year + ",'MOOE',0,1,'+');");

            }

          
            _sqlString += sb.ToString();

            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }

        private Boolean isProcessed = false;
        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "ApproveRevision":
                    if (SQLOperation!=null)
                    {
                        if (isProcessed)
                        {
                            isProcessed = false;
                            return;
                        }
                        SQLOperation(this, new EventArgs());
                        isProcessed = true;
                    }
                    break;
                case "RemoveAlignments":
                    if (SQLOperation != null)
                    {
                        if (isProcessed)
                        {
                            isProcessed = false;
                            return;
                        }
                        SQLOperation(this, new EventArgs());
                        isProcessed = true;
                    }
                    break;
                default:
                    break;
            }
            //throw new NotImplementedException();
        }

        public String FetchDivisionExpenditureDetailsItemByDiv(String _Assigned, String _year,String FundType,String ExpType)
        {

            var sb = new System.Text.StringBuilder(566);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mad.id ,");
            sb.AppendLine(@"mosub.name as expenditure,");
            sb.AppendLine(@"mad.total as total,");
            sb.AppendLine(@"mad.month");
            sb.AppendLine(@"FROM mnda_activity_data mad");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.id = mad.activity_id");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.id = ma.output_id");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mosub on mosub.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
            sb.AppendLine(@"INNER JOIN mnda_respo_library res on res.code = mad.fund_source_id");
            sb.AppendLine(@"INNER JOIN mnda_fund_source_types mfst on mfst.code = res.fund_type");
            sb.AppendLine(@"WHERE  mad.year = " + _year + " and mad.is_suspended = 0 and mpe.acountable_division_code = " + _Assigned + "");
            sb.AppendLine(@"AND  mfst.description ='" + FundType + "' AND mosub.expense_type ='MOOE' ");
            sb.AppendLine(@"AND mad.status ='For Approval By Finance' OR mad.status ='REVISION'");
            sb.AppendLine(@"ORDER BY mosub.id ASC");


            //sb.AppendLine(@"SELECT");
            //sb.AppendLine(@"mapd.id as id,");
            //sb.AppendLine(@"mosub.name as expenditure,");
            //sb.AppendLine(@"mad.total as total,");
            //sb.AppendLine(@"mad.month");
            //sb.AppendLine(@"FROM mnda_activity_data mad");
            //sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.id = mad.activity_id");
            //sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.id = ma.output_id");
            //sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mosub on mosub.id = mad.mooe_sub_expenditure_id");
            //sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
            //sb.AppendLine(@"INNER JOIN mnda_approved_projects_division mapd on mapd.activity_id = mad.id");
            //sb.AppendLine(@"INNER JOIN mnda_respo_library res on res.code = mad.fund_source_id");
            //sb.AppendLine(@"INNER JOIN mnda_fund_source_types mfst on mfst.code = res.fund_type");
            //sb.AppendLine(@"WHERE  mad.year = " + _year + " and mad.is_suspended = 0 and mpe.acountable_division_code = " + _Assigned + "");
            //sb.AppendLine(@"AND  mfst.description ='" + FundType + "' AND mosub.expense_type ='" + ExpType + "' ");
            //sb.AppendLine(@"AND mad.status ='For Approval By Finance' OR mad.status ='REVISION'");
            //sb.AppendLine(@"ORDER BY mosub.id ASC");
            return sb.ToString();
        }
        public String FetchFundSourceDetails(String DivisionId, String _year,String FundType,String ExpType)
        {
            var sb = new System.Text.StringBuilder(417);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"  ");
            sb.AppendLine(@"   ISNULL(mfsl.Fund_Source_Id,'') as id,");
            sb.AppendLine(@"   ISNULL(mfs.Fund_Name,'') as Fund_Name,");
            sb.AppendLine(@"   ISNULL(mooe.name,'')  as Name,");
            sb.AppendLine(@"CASE WHEN mfsl.plusminus='-'  THEN SUM(ISNULL(mfsl.Amount - ISNULL(mfsl.adjustment,0) ,'0')) ELSE SUM(ISNULL(mfsl.Amount + ISNULL(mfsl.adjustment,0) ,'0'))  END  as Allocation");
            sb.AppendLine(@"FROM mnda_fund_source mfs");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source_line_item mfsl on mfsl.Fund_Source_Id = mfs.code");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.uacs_code = mfsl.mooe_id");
            sb.AppendLine(@"INNER JOIN mnda_respo_library mrl on mrl.code = mfsl.Fund_Source_Id");
            sb.AppendLine(@"INNER JOIN mnda_fund_source_types mfst on mfst.code = mrl.fund_type");
            sb.AppendLine(@"WHERE mfs.division_id = " + DivisionId + " and mfs.Year = " + _year + "  AND mfst.description ='"+ FundType +"' AND mooe.expense_type ='"+ ExpType +"'");
            sb.AppendLine(@"GROUP BY mooe.id,mfsl.Fund_Source_Id, mfs.Fund_Name,mooe.name,mfsl.plusminus");
            sb.AppendLine(@"ORDER BY mooe.id ASC");

            return sb.ToString();
        }

        public String FetchApprovedFinance()
        {
            var sb = new System.Text.StringBuilder(70);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  code,");
            sb.AppendLine(@"  status");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_approved_finance;");


            return sb.ToString();
        }
        public String FetchFundSource(String DivisionId, String _year,String FundType,String ExpType)
        {
            var sb = new System.Text.StringBuilder(134);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mfs.code as id,");
            //  sb.AppendLine(@"mfs.Fund_Source_Id as id,");
            sb.AppendLine(@"mfs.Fund_Name as fund_name");
            sb.AppendLine(@"FROM mnda_fund_source mfs");
            sb.AppendLine(@"INNER JOIN mnda_respo_library mrl on mrl.code = mfs.code");
            sb.AppendLine(@"INNER JOIN mnda_fund_source_types mfst on mfst.code = mrl.fund_type");
            sb.AppendLine(@"WHERE mfs.division_id = " + DivisionId + " and mfs.Year =" + _year + " ");
            sb.AppendLine(@"AND  mfst.description ='" + FundType + "'");
            sb.AppendLine(@"ORDER BY id ASC");
            return sb.ToString();
        }
        public String FetchOfficeDetails(String _office_id, String _year)
        {
            StringBuilder sb = new StringBuilder(294);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mfs.Fund_Source_Id as fund_source_id,mfs.division_id as div_id,");
            //    sb.AppendLine(@"mfs.code as fund_source_id,mfs.division_id as div_id,");
            sb.AppendLine(@"mfs.Fund_Name,");
            sb.AppendLine(@"mfs.Amount as budget_allocation,");
            sb.AppendLine(@"0.00 as total_program_budget,");
            sb.AppendLine(@"0.00 as balance");
            sb.AppendLine(@"FROM  Division div ");
            sb.AppendLine(@"INNER JOIN DBM_Sub_Pap dsp on dsp.DBM_Sub_Pap_id = div.DBM_Sub_Pap_Id");
            sb.AppendLine(@"INNER JOIN mnda_fund_source mfs on mfs.division_id = div.Division_Id");
            sb.AppendLine(@"WHERE dsp.DBM_Pap_Id = " + _office_id + " and mfs.Year = " + _year + "");
            sb.AppendLine(@"ORDER BY mfs.Fund_Name");


            return sb.ToString();
        }
        public String FetchBudgetAllocation(String division_id, String _year)
        {
            var sb = new System.Text.StringBuilder(431);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"ISNULL(mooe.code,'') as pap_code,");
            sb.AppendLine(@"ISNULL(msub.uacs_code,'') as uacs_code,");
            sb.AppendLine(@"ISNULL(mfs.Fund_Name,'') as fund_name,");
            sb.AppendLine(@"ISNULL(mooe.name,'') as mo_name,");
            sb.AppendLine(@"ISNULL(msub.mooe_id,'') as pap_sub,");
            sb.AppendLine(@"ISNULL(msub.name,'') as Name,");
            sb.AppendLine(@"ISNULL(mfsl.Amount,'0') as Amount");
            sb.AppendLine(@"FROM mnda_fund_source mfs");
            sb.AppendLine(@"LEFT JOIN Division div on div.Division_Id = mfs.division_id");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source_line_item mfsl on mfsl.Fund_Source_Id = mfs.code");
            //   sb.AppendLine(@"LEFT JOIN mnda_fund_source mf on mf.Fund_Source_Id = mfsl.Fund_Source_Id");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures msub on msub.uacs_code = mfsl.mooe_id");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_expenditures mooe on mooe.code = msub.mooe_id");
            sb.AppendLine(@"WHERE div.Division_Id =" + division_id + " AND mfs.Year ='" + _year + "'");

            return sb.ToString();
        }

        public String FetchDivisionFundSource(String division_id, String _year)
        {
            var sb = new System.Text.StringBuilder(95);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mfs.Fund_Name");
            sb.AppendLine(@"FROM mnda_fund_source mfs");
            sb.AppendLine(@"WHERE mfs.division_id = " + division_id + " and mfs.Year = " + _year + "");


            return sb.ToString();
        }
        public String FetchData(string _division_id,string _year)
        {
            StringBuilder sb = new StringBuilder(101);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Division_Id,");
            sb.AppendLine(@"  DBM_Sub_Pap_Id,");
            //sb.AppendLine(@"  Division_Code,");
            //sb.AppendLine(@"  Division_Desc");
            //sb.AppendLine(@"FROM ");
            //sb.AppendLine(@"  dbo.Division WHERE DBM_Sub_Pap_Id = " + _PAP + ";");


            return sb.ToString();


        }

    }
}
