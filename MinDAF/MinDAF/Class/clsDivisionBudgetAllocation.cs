﻿using System;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsDivisionBudgetAllocation
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        public String FundCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();

        public String FetchRespoLibrary(String _divid,String _ftype,String _working_year)
        {
            //var sb = new System.Text.StringBuilder(62);
            //sb.AppendLine(@"SELECT ");
            //sb.AppendLine(@"  code as id,");
            //sb.AppendLine(@"  respo_name");
            //sb.AppendLine(@"FROM ");
            //sb.AppendLine(@"  dbo.mnda_respo_library WHERE is_division =0;");
            //var sb = new System.Text.StringBuilder(142);
            //sb.AppendLine(@"SELECT");
            //sb.AppendLine(@"mrl.code as id,");
            //sb.AppendLine(@"mrl.respo_name");
            //sb.AppendLine(@"FROM mnda_respo_library mrl");
            //sb.AppendLine(@"INNER JOIN Division div on div.Division_Id= mrl.div_id");
            //sb.AppendLine(@"WHERE mrl.div_id  ='"+ _divid +"'");
            var sb = new System.Text.StringBuilder(176);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mrl.code as id,");
            sb.AppendLine(@"mrl.respo_name");
            sb.AppendLine(@"FROM mnda_respo_library mrl");
            sb.AppendLine(@"INNER JOIN Division div on div.Division_Id= mrl.div_id");
            sb.AppendLine(@"WHERE mrl.div_id  ='" + _divid + "' AND mrl.fund_type = '" + _ftype + "'  AND mrl.working_year ='" + _working_year + "'");
            sb.AppendLine(@"AND div.year_setup = '"+ _working_year +"'");

            return sb.ToString();
        }
        public String FetchDivisionPerOffice(String _id,String _year)
        {
            StringBuilder sb = new StringBuilder(130);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Division_Id,");
            sb.AppendLine(@"  DBM_Sub_Pap_Id,");
            sb.AppendLine(@"  Division_Code,");
            sb.AppendLine(@"  Division_Desc,UnitCode");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.Division  WHERE is_sub_division = 0 and year_setup ='"+ _year +"'");

            return sb.ToString();
        }
        public String FetchDivisionUnit(String _year)
        {
            StringBuilder sb = new StringBuilder(130);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Division_Id,");
            sb.AppendLine(@"  DBM_Sub_Pap_Id,");
            sb.AppendLine(@"  Division_Code,");
            sb.AppendLine(@"  Division_Desc,UnitCode");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.Division WHERE is_sub_division = 1 and year_setup ='"+ _year +"'");
            //sb.AppendLine(@"  WHERE DBM_Sub_Pap_Id ='"+ _id +"';");


            return sb.ToString();
        }
        public String FetchSubPap()
        {
            var sb = new System.Text.StringBuilder(86);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Sub_Id,");
            sb.AppendLine(@"  DBM_Sub_Id,");
            sb.AppendLine(@"  Description,");
            sb.AppendLine(@"  PAP");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.DBM_Sub_Office;");

            return sb.ToString();
        }
        public String FetchPap()
        {
            var sb = new System.Text.StringBuilder(109);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  DBM_Sub_Pap_id,");
            sb.AppendLine(@"  DBM_Pap_Id,");
            sb.AppendLine(@"  DBM_Sub_Pap_Code,");
            sb.AppendLine(@"  DBM_Sub_Pap_Desc");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.DBM_Sub_Pap;");


            return sb.ToString();
        }

        public String FetchExpenditureItems()
        {
            var sb = new System.Text.StringBuilder(132);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  mooe_id,");
            sb.AppendLine(@"  uacs_code,");
            sb.AppendLine(@"  name,");
            sb.AppendLine(@"  is_active,");
            sb.AppendLine(@"  is_dynamic");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_mooe_sub_expenditures WHERE is_active = 1 ORDER BY name ASC;");


            return sb.ToString();
        }
        public String FetchOfficeData(String _id, String _year)
        {
            var sb = new System.Text.StringBuilder(355);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  mfs.Fund_Source_Id as id,");
            sb.AppendLine(@"  mfs.Fund_Name,");
            sb.AppendLine(@"  SUM(ISNULL(mfsl.Amount,0.00)) as budget_allocation,");
            sb.AppendLine(@"  mfs.Status,");
            sb.AppendLine(@"  mfs.Year as entry_year,mfsl.expenditure_type as exp_type");
            sb.AppendLine(@"FROM  mnda_fund_source mfs");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source_line_item mfsl on mfsl.Fund_Source_Id = mfs.code");
            sb.AppendLine(@"WHERE mfs.division_id ="+ _id +" and  mfs.Year ="+ _year +"");
            sb.AppendLine(@"GROUP BY mfs.Fund_Source_Id,mfs.Fund_Name,mfs.Status, mfs.Year,mfsl.expenditure_type");



            return sb.ToString();
        }
        public String FetchOfficeData(String _id)
        {
        

            StringBuilder sb = new StringBuilder(238);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  mfs.Fund_Source_Id as id,");
            sb.AppendLine(@"  mfs.Fund_Name,");
            sb.AppendLine(@"  mfs.Amount as budget_allocation,");
            sb.AppendLine(@"  mfs.status,");
            sb.AppendLine(@"  mfs.Year as entry_year");
            sb.AppendLine(@"FROM  mnda_fund_source mfs");
            sb.AppendLine(@"INNER JOIN Division div on div.Division_Id = mfs.division_id");
            sb.AppendLine(@"WHERE mfs.division_id =" + _id + "");


            return sb.ToString();
        }
        public void UpdateSuspend(String _id, String _val)
        {


            String _sqlString = "";

            StringBuilder sb = new StringBuilder(79);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_activity_data  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  is_suspended =" + _val + ",status ='SUSPENDED-REVISION'");//sb.AppendLine(@"  is_suspended =" + _val + ",activity_id=activity_id + '-suspended',status ='SUSPENDED'");
            sb.AppendLine(@"WHERE id = " + _id + ";");
            //StringBuilder sb = new StringBuilder(79);
            //sb.AppendLine(@"UPDATE ");
            //sb.AppendLine(@"  dbo.mnda_activity_data  ");
            //sb.AppendLine(@"SET ");
            //sb.AppendLine(@"  is_suspended =" + _val + "");
            //sb.AppendLine(@"WHERE id = " + _id + ";");


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;

        }

        public void RemoveData(String _id, String _working_year)
        {


            String _sqlString = "";

            var sb = new System.Text.StringBuilder(119);
            sb.AppendLine(@"DELETE FROM ");
            sb.AppendLine(@"  dbo.Division ");
            sb.AppendLine(@"WHERE  Division_Id ='"+ _id +"' AND year_setup ='"+ _working_year +"';");


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;

        }
        public String FetchBudgetExpenditureAllocation(String _year,String _id,String FundSource)
        {
            var sb = new System.Text.StringBuilder(276);
    
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mfsl.id,");
            sb.AppendLine(@"mooe_sub.name,");
            sb.AppendLine(@"mfsl.Amount,mfsl.expenditure_type as exp_type");
            sb.AppendLine(@"FROM mnda_fund_source_line_item mfsl");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source mfs on mfs.code = mfsl.Fund_Source_Id");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe_sub on mooe_sub.uacs_code = mfsl.mooe_id");
            sb.AppendLine(@"WHERE mooe_sub.is_active = 1 AND mfsl.Year =" + _year + " AND mfsl.division_id =" + _id + " AND mfs.Fund_Name ='"+ FundSource +"'");
            sb.AppendLine(@"GROUP BY mfsl.id,mooe_sub.name,mfsl.Amount,mfsl.expenditure_type");



            return sb.ToString();
        }

        public String FetchFundSourceType()
        {
            var sb = new System.Text.StringBuilder(76);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  code,");
            sb.AppendLine(@"  description");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_fund_source_types;");



            return sb.ToString();
        }

       
        public static string getHashSha256(string text)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }
        public void SaveDivisionBudget(String division_id,String Fund_Code, String Fund_Name, String Amount, String entry_year)
        {
            String _sqlString = "";
            FundCode = Fund_Code;
            StringBuilder sb = new StringBuilder(132);
            sb.AppendLine(@"DELETE FROM dbo.mnda_fund_source WHERE code ='"+ Fund_Code +"';");
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_fund_source");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  division_id,");
            sb.AppendLine(@"  Fund_Name,");
            sb.AppendLine(@"  details,");
            sb.AppendLine(@"  Amount,");
            sb.AppendLine(@"  Year,code");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"'"+ division_id +"',");
            sb.AppendLine(@"'"+ Fund_Name +"',");
            sb.AppendLine(@"'"+ Fund_Name +"',");
            sb.AppendLine(@""+ Amount +",");
            sb.AppendLine(@""+ entry_year +",");
            sb.AppendLine(@"'" + FundCode + "'");
            sb.AppendLine(@");");



           
            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void SaveDivisionBudgetExpenditure(String Fund_Source_Id, String division_id, String mooe_id, String Amount, String entry_year,String fund_type)
        {
            String _sqlString = "";

            
            var sb = new System.Text.StringBuilder(204);
            sb.AppendLine(@"DELETE FROM dbo.mnda_fund_source_line_item WHERE Fund_Source_Id ='"+ Fund_Source_Id +"' AND division_id ='"+  division_id +"' AND mooe_id = '"+ mooe_id +"' AND Year = '"+ entry_year +"';");
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_fund_source_line_item");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  Fund_Source_Id,");
            sb.AppendLine(@"  division_id,");
            sb.AppendLine(@"  mooe_id,");
            sb.AppendLine(@"  Amount,");
            sb.AppendLine(@"  Year,");
            sb.AppendLine(@"  expenditure_type");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '" + Fund_Source_Id +"',");
            sb.AppendLine(@"  '" + division_id +"',");
            sb.AppendLine(@"  '" + mooe_id +"',");
            sb.AppendLine(@"  '" + Amount +"',");
            sb.AppendLine(@"  '" + entry_year +"',");
            sb.AppendLine(@"  '" + fund_type + "'");
            sb.AppendLine(@");");




            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void UpdateDivisionBudget(String fund_source_id,string _amount)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(96);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_fund_source  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  Amount =" + _amount +"");
            sb.AppendLine(@"WHERE ");
            sb.AppendLine(@"  Fund_Source_Id ="+ fund_source_id +"");




            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void UpdateDivisionBudgetLineItem(String id, string _amount)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(96);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_fund_source_line_item  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  Amount =" + _amount + "");
            sb.AppendLine(@"WHERE ");
            sb.AppendLine(@"  id =" + id + "");




            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
      public void RemoveBudget(String id)
            {
                String _sqlString = "";
                StringBuilder sb = new StringBuilder(56);
                sb.AppendLine(@"DELETE FROM ");
                sb.AppendLine(@"  dbo.mnda_fund_source_line_item ");
                sb.AppendLine(@"WHERE Fund_Source_Id =" + id + ";");


                _sqlString += sb.ToString();

                c_ops.InstantiateService();
                c_ops.ExecuteSQL(_sqlString);
                c_ops.DataReturn += c_ops_DataReturn;


            }
      public void UpdateAmountExpenditure(String id,String amount)
      {
          String _sqlString = "";
          var sb = new System.Text.StringBuilder(186);
          sb.AppendLine(@"UPDATE ");
          sb.AppendLine(@"  dbo.mnda_fund_source_line_item  ");
          sb.AppendLine(@"SET ");
          sb.AppendLine(@"  Amount = "  + amount +  "");
          sb.AppendLine(@"WHERE ");
          sb.AppendLine(@"  id ="+ id +"");

          _sqlString += sb.ToString();

          c_ops.InstantiateService();
          c_ops.ExecuteSQL(_sqlString);
          c_ops.DataReturn += c_ops_DataReturn;


      }
      public void RemoveBudgetAllocation(String id)
      {
          String _sqlString = "";
          StringBuilder sb = new StringBuilder(56);
          sb.AppendLine(@"DELETE FROM ");
          sb.AppendLine(@"  dbo.mnda_fund_source_line_item ");
          sb.AppendLine(@"WHERE id =" + id + ";");


          _sqlString += sb.ToString();

          c_ops.InstantiateService();
          c_ops.ExecuteSQL(_sqlString);
          c_ops.DataReturn += c_ops_DataReturn;


      }

      public void UpdateSource(String DivId,String FundSource)
      {
          String _sqlString = "";
          var sb = new System.Text.StringBuilder(77);
          sb.AppendLine(@"UPDATE   dbo.mnda_respo_library  SET  div_id = '"+DivId +"' ");
          sb.AppendLine(@"WHERE  code = '"+ FundSource +"';");


          _sqlString += sb.ToString();

          c_ops.InstantiateService();
          c_ops.ExecuteSQL(_sqlString);
          c_ops.DataReturn += c_ops_DataReturn;


      }
      public void RemoveSource(String DivId, String RespoName)
      {
          String _sqlString = "";
          var sb = new System.Text.StringBuilder(77);
          sb.AppendLine(@"UPDATE   dbo.mnda_respo_library  SET  div_id = '-' ");
          sb.AppendLine(@"WHERE respo_name ='" + RespoName + "';");


          _sqlString += sb.ToString();

          c_ops.InstantiateService();
          c_ops.ExecuteSQL(_sqlString);
          c_ops.DataReturn += c_ops_DataReturn;


      }
      public void RemoveBudgetExpenditure(String id, String div_id, String mooe, String _year)
      {
          String _sqlString = "";
          StringBuilder sb = new StringBuilder(56);
          sb.AppendLine(@"DELETE FROM ");
          sb.AppendLine(@"  dbo.mnda_fund_source_line_item ");
          sb.AppendLine(@"WHERE Fund_Source_Id =" + id + "  AND division_id =" + div_id + " AND mooe_id = " + mooe + " AND Year = " + _year + ";");


          _sqlString += sb.ToString();

          c_ops.InstantiateService();
          c_ops.ExecuteSQL(_sqlString);
          c_ops.DataReturn += c_ops_DataReturn;


      }

      private Boolean isDone = false;

        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "RemoveData":
                    if (isDone)
                    {
                        isDone = true;
                        break;
                    }
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                        isDone = true;
                    }
                    break;
                case "RemoveSourceDB":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateSourceDB":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "SaveDivisionBudget":
                    if (SQLOperation!=null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "SaveDivisionBudgetExpenditure":
                    if (SQLOperation!=null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateDivisionBudget":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateAmount":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "RemoveBudget":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "RemoveBudgetExpenditure":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public class lib_FundSourceType 
    {
      
          public String id{ get; set; }
          public String code{ get; set; }
          public String description { get; set; }
    }
    public class lib_MainBudgetAllowance
    {
        public String Id { get; set; }
        public String Pap { get; set; }
        public String Description { get; set; }
        public Boolean IsDiv { get; set; }
        public Boolean IsUnit { get; set; }
    }
    public class lib_MainBudgetAllowanceConsolidated
    {
        public String Id { get; set; }
        public String Pap { get; set; }
        public String Description { get; set; }
        public Boolean IsDiv { get; set; }
        public Boolean IsUnit { get; set; }
        public String FirstQuarter { get; set; }
        public String SecondQuarter { get; set; }
        public String ThirdQuarter { get; set; }
        public String FourthQuarter { get; set; }

        public String Total { get; set; }
    
    }

    public class lib_PAP
    {
        public String DBM_Sub_Pap_id { get; set; }
        public String DBM_Pap_Id { get; set; }
        public String DBM_Sub_Pap_Code { get; set; }
        public String DBM_Sub_Pap_Desc { get; set; }
    }

    public class lib_SUB_PAP 
    {
           public String Sub_Id { get; set; }
           public String DBM_Sub_Id { get; set; }
           public String  Description { get; set; }
           public String PAP { get; set; }
    }

    public class lib_BudgetAllocationExpenditure 
    {
        public String Id { get; set; }
        public String Name { get; set; }
        public String Amount { get; set; }
        public String ExpType { get; set; }
    }

    public class lib_DivisionBudgetExpeniture
    {
          public String  mooe_id {get;set;}
             public String  uacs_code{get;set;}
           public String  name {get;set;}
              public String  is_active {get;set;}
              public String is_dynamic { get; set; }
    }
    public class DivisionBudgetAllocationFields     
    {
      public String Division_Id {get;set;}
      public String DBM_Sub_Pap_Id {get;set;}
      public String Division_Code {get;set;}
      public String Division_Desc { get; set; }
      public String UnitCode { get; set; }
    }

    public class OfficeDataFields

    {
        public String id { get; set; }
        public String Fund_Name { get; set; }
         public String Amount { get; set; }
         public String status { get; set; }
         public String entry_year { get; set; }
    }
}
