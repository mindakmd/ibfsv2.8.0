﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsOfficeDivision
    {
        public event EventHandler DataLoaded;
        public event EventHandler DivisionDataLoded;
        public event EventHandler OfficeBudgetData;

        private clsServiceOperation c_ops = new clsServiceOperation();
        public Object DataReturn { get; set; }
        public String Process = "";

        public Boolean SaveData(String _Name) 
        {
            

            StringBuilder sb = new StringBuilder(157);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_office");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  Office_Code,");
            sb.AppendLine(@"  Office_Desc");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '" + _Name +"',");
            sb.AppendLine(@"  'OFFICE'");
            sb.AppendLine(@");");

            c_ops.InstantiateService();
            c_ops.ExecuteSQL(sb.ToString());
            c_ops.DataReturn+=c_ops_DataReturn;
           

            if (c_ops.ReturnCode=="1")
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public Boolean SaveOfficeBudget(String _office_id, String _name, String _division_desc)
        {
            StringBuilder sb = new StringBuilder(123);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_division");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  Office_Id,");
            sb.AppendLine(@"  Division_Code,");
            sb.AppendLine(@"  Division_Desc");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '" + _office_id + "',");
            sb.AppendLine(@"  '" + _name + "',");
            sb.AppendLine(@"  '" + _division_desc + "'");
            sb.AppendLine(@");");


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(sb.ToString());
            c_ops.DataReturn += c_ops_DataReturn;


            if (c_ops.ReturnCode == "1")
            {
                return true;
            }
            else
            {
                return false;
            }

        }


        public Boolean SaveDivisionData(String _office_id, String _name,String _division_desc)
        {
            StringBuilder sb = new StringBuilder(123);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_division");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  Office_Id,");
            sb.AppendLine(@"  Division_Code,");
            sb.AppendLine(@"  Division_Desc");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '"+ _office_id +"',");
            sb.AppendLine(@"  '"+ _name +"',");
            sb.AppendLine(@"  '"+ _division_desc +"'");
            sb.AppendLine(@");");


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(sb.ToString());
            c_ops.DataReturn += c_ops_DataReturn;


            if (c_ops.ReturnCode == "1")
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public Boolean SaveBudgetOffice(String _office_id, Double _budget , String _status)
        {
            StringBuilder sb = new StringBuilder(130);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_budget_allocation");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  office_id,");
            sb.AppendLine(@"  budget_allocation,");
            sb.AppendLine(@"  status");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@" "+ _office_id +",");
            sb.AppendLine(@"  "+ _budget +",");
            sb.AppendLine(@"  "+_status +"");
            sb.AppendLine(@");");


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(sb.ToString());
            c_ops.DataReturn += c_ops_DataReturn;


            if (c_ops.ReturnCode == "1")
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public void LoadOfficeData() 
        {

            c_ops.DataReturn += c_ops_DataReturn;
            c_ops.ExecuteSQL("SELECT * FROM mnda_office");
           
        
        }

        public void LoadDivisionData(String _office_id)
        {

            c_ops.DataReturn += c_ops_DataReturn;
            c_ops.ExecuteSQL("SELECT * FROM mnda_division WHERE Office_ID ='"+ _office_id +"'");
           
        }

        public String LoadBudgetData(String _office_id)
        {
            StringBuilder sb = new StringBuilder(215);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"Year(mba.entry_date) as Year,");
            sb.AppendLine(@"mo.Office_Code as Office,");
            sb.AppendLine(@"mo.Office_Desc as Description,");
            sb.AppendLine(@"mba.budget_allocation as Budget");
            sb.AppendLine(@"FROM mnda_office mo");
            sb.AppendLine(@"INNER JOIN mnda_budget_allocation mba on mba.office_id = mo.Office_Id");
            sb.AppendLine(@"WHERe mo.Office_Id = '"+_office_id +"';");

            return sb.ToString();

        }


        void c_ops_DataReturn(object sender, EventArgs e)
        {
           
            DataReturn = c_ops.TableData;
            switch (Process)
            {
                case "OfficeData":
                    if (DataLoaded != null)
                    {
                        DataLoaded(this, new EventArgs());
                    }
                    break;
                case "DivisionData":
                    if (DivisionDataLoded != null)
                    {
                        DivisionDataLoded(this, new EventArgs());
                    }
                    break;
                case "SaveOfficeBudget":
                    if (OfficeBudgetData!=null)
                    {
                        OfficeBudgetData(this, new EventArgs());
                    }
                    break;
                default:
                    break;
            }
            
        }

    }

  
}
