﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmYear : ChildWindow
    {
        public string Process { get; set; }
        public string SelectedYear { get; set; }
        public string SelectedDivisionID { get; set; }
        public string SelectedDivisionPAP { get; set; }
        public string SelectedDivision  { get; set; }
        public string SelectedFundSource { get; set; }

        public event EventHandler ProcessReport;
        public event EventHandler ProcessBudget;
        public event EventHandler ProcessSummary;
        public event EventHandler ProcessMAOSummary;

        private List<Divisions> ListDivision = new List<Divisions>();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private List<OfficesPAP> ListOfficesPap = new List<OfficesPAP>();
        private clsBudgetPapOfficeManagement c_pap_office = new clsBudgetPapOfficeManagement();
        private List<FundSourceLib> ListFundSource = new List<FundSourceLib>();

        public frmYear()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
           var _results = e.Result.ToString();
           switch (c_pap_office.Process)
           {
               case "FetchDivision":
                   XDocument oDocKeyResultsFetchDivision = XDocument.Parse(_results);
                   var _dataListsFetchDivision = from info in oDocKeyResultsFetchDivision.Descendants("Table")
                                                 select new Divisions
                                                 {
                                                     DBM_Sub_Pap_Id = Convert.ToString(info.Element("DBM_Sub_Pap_Id").Value),
                                                     Division_Code = Convert.ToString(info.Element("Division_Code").Value),
                                                     Division_Desc = Convert.ToString(info.Element("Division_Desc").Value),
                                                     Division_Id = Convert.ToString(info.Element("Division_Id").Value)
                                                 };


                   ListDivision.Clear();
                   cmbDivision.Items.Clear();
                   foreach (var item in _dataListsFetchDivision)
                   {
                       Divisions _varProf = new Divisions();

                       _varProf.DBM_Sub_Pap_Id = item.DBM_Sub_Pap_Id;
                       _varProf.Division_Code = item.Division_Code;
                       _varProf.Division_Desc = item.Division_Desc;
                       _varProf.Division_Id = item.Division_Id;

                       ListDivision.Add(_varProf);
                       cmbDivision.Items.Add(item.Division_Desc);

                   }
                 
                   this.Cursor = Cursors.Arrow;
                   break;
               case "FetchSubPap":
                   XDocument oDocKeyResultsFetchSubPap = XDocument.Parse(_results);
                   var _dataListsFetchSubPap = from info in oDocKeyResultsFetchSubPap.Descendants("Table")
                                                 select new OfficesPAP
                                                 {
                                                     DBM_Pap_Id = Convert.ToString(info.Element("DBM_Pap_Id").Value),
                                                     DBM_Sub_Pap_Code = Convert.ToString(info.Element("DBM_Sub_Pap_Code").Value),
                                                     DBM_Sub_Pap_Desc = Convert.ToString(info.Element("DBM_Sub_Pap_Desc").Value),
                                                     DBM_Sub_Pap_id = Convert.ToString(info.Element("DBM_Sub_Pap_id").Value)
                                                 };


                   ListOfficesPap.Clear();
                   cmbDivision.Items.Clear();
                   foreach (var item in _dataListsFetchSubPap)
                   {
                       OfficesPAP _varProf = new OfficesPAP();

                       _varProf.DBM_Pap_Id = item.DBM_Pap_Id;
                       _varProf.DBM_Sub_Pap_Code = item.DBM_Sub_Pap_Code;
                       _varProf.DBM_Sub_Pap_Desc = item.DBM_Sub_Pap_Desc;
                       _varProf.DBM_Sub_Pap_id = item.DBM_Sub_Pap_id;

                       ListOfficesPap.Add(_varProf);
                       cmbDivision.Items.Add(item.DBM_Sub_Pap_Desc);

                   }

                   this.Cursor = Cursors.Arrow;
                   break;
               case "FetchFundSource":
                   XDocument oDocKeyResultsFetchFundSource = XDocument.Parse(_results);
                   var _dataListsFetchFundSource = from info in oDocKeyResultsFetchFundSource.Descendants("Table")
                                                   select new FundSourceLib
                                                   {
                                                       Fund_Name = Convert.ToString(info.Element("Fund_Name").Value),
                                                       Fund_Source_Id = Convert.ToString(info.Element("Fund_Source_Id").Value)
                                                   };

                   ListFundSource.Clear();

                   cmbFundSource.Items.Clear();

                   foreach (var item in _dataListsFetchFundSource)
                   {

                       FundSourceLib _varDetails = new FundSourceLib();


                       _varDetails.Fund_Name = item.Fund_Name;
                       _varDetails.Fund_Source_Id = item.Fund_Source_Id;



                       ListFundSource.Add(_varDetails);
                       cmbFundSource.Items.Add(item.Fund_Name);
                   }
               
                   this.Cursor = Cursors.Arrow;

                   break;
           }
              
        }
        private void FetchFundSource()
        {
            String DivId = "";
            if (Process =="Division")
                {
                    List<Divisions> x = ListDivision.Where(item => item.Division_Desc == cmbDivision.SelectedItem.ToString()).ToList();
                    foreach (var item in x)
                    {

                        DivId = item.Division_Id;
                        break;
                    }
                    c_pap_office.Process = "FetchFundSource";
                    svc_mindaf.ExecuteSQLAsync(c_pap_office.FetchFundsSource(DivId));
                }
            else if (Process == "SUBPAP") 
            {

            }
          

        }
        private void GenerateYear()
        {
            int _year = DateTime.Now.Year;
            int _limit = _year + 100;

            for (int i = _year; i != _limit; i++)
            {
                cmbYear.Items.Add(_year);
                _year += 1;
            }
            cmbYear.SelectedIndex = 0;
        }
        private void FetchDivision()
        {
            c_pap_office.Process = "FetchDivision";
            svc_mindaf.ExecuteSQLAsync(c_pap_office.FetchDivisionAll());
        }
        private void FetchSubPap()
        {
            c_pap_office.Process = "FetchSubPap";
            svc_mindaf.ExecuteSQLAsync(c_pap_office.FetchSubPAP());
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (cmbDivision.SelectedItem !=null)
            {
                if (Process =="Division")
                {
                    List<Divisions> x = ListDivision.Where(item => item.Division_Desc == cmbDivision.SelectedItem.ToString()).ToList();
                    foreach (var item in x)
                    {
                        SelectedDivision = item.Division_Desc;
                        SelectedDivisionID = item.Division_Id;
                        SelectedDivisionPAP = item.Division_Code;
                        break;
                    }
                }
                else if (Process == "SUBPAP")
                {
                    List<OfficesPAP> x = ListOfficesPap.Where(item => item.DBM_Sub_Pap_Desc == cmbDivision.SelectedItem.ToString()).ToList();
                    foreach (var item in x)
                    {
                        SelectedDivision = item.DBM_Sub_Pap_Desc;
                        SelectedDivisionID = item.DBM_Sub_Pap_id;
                        SelectedDivisionPAP ="";
                        break;
                    }
                }
                else if (Process == "SUMMARY")
                {
                    List<OfficesPAP> x = ListOfficesPap.Where(item => item.DBM_Sub_Pap_Desc == cmbDivision.SelectedItem.ToString()).ToList();
                    foreach (var item in x)
                    {
                        SelectedDivision = item.DBM_Sub_Pap_Desc;
                        SelectedDivisionID = item.DBM_Sub_Pap_id;
                        SelectedDivisionPAP = item.DBM_Sub_Pap_Code;
                        break;
                    }
                }
            }
           
            SelectedYear = cmbYear.SelectedItem.ToString();
            if (ProcessReport != null)
            {
                ProcessReport(this, new EventArgs());
            }

            if (ProcessBudget!=null)
            {
                ProcessBudget(this, new EventArgs());
            }
            if (ProcessSummary!=null)
            {
                ProcessSummary(this, new EventArgs());
            }
            if (ProcessMAOSummary!=null)
            {
                ProcessMAOSummary(this, new EventArgs());
            }
          
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void frm_year_Loaded(object sender, RoutedEventArgs e)
        {
            GenerateYear();
            switch (this.Process)
            {
                case "Division":
                    FetchDivision();
                    lblOffice.Content = "Select Division";
                    break;
                case "SUBPAP":
                    lblOffice.Content = "Select Office";
                    FetchSubPap();
                    break;
                case "SUMMARY":
                    lblOffice.Content = "Select Office";
                    FetchSubPap();
                    break;
               
            }
            
        }

        private void cmbYear_DropDownClosed(object sender, EventArgs e)
        {
            this.SelectedYear = cmbYear.SelectedItem.ToString();
        }

        private void cmbDivision_DropDownClosed(object sender, EventArgs e)
        {
            FetchFundSource();
        }

        private void cmbFundSource_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                if (cmbFundSource.SelectedItem.ToString() != "")
                {
                    SelectedFundSource = cmbFundSource.SelectedItem.ToString();
                    OKButton.IsEnabled = true;
                }
                else
                {
                    MessageBox.Show("Select Fund Source");
                }
            }
            catch (Exception)
            {


            }
        }

        
    }
}

