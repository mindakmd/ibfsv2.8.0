﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmMainExpenditure : ChildWindow
    {
        private List<MainExpenditures> ListMainExpenditures = new List<MainExpenditures>();
   
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsExpenditureManagement c_expen = new clsExpenditureManagement();

        public frmMainExpenditure()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_expen.Process)
            {
            
                case "FetchMainExpenditure":
                    XDocument oDocKeyResultsFetchMainExpenditure = XDocument.Parse(_results);
                    var _dataListsFetchMainExpenditure = from info in oDocKeyResultsFetchMainExpenditure.Descendants("Table")
                                                         select new MainExpenditures
                                                         {

                                                             id = Convert.ToString(info.Element("id").Value),
                                                             code = Convert.ToString(info.Element("code").Value),
                                                             name = Convert.ToString(info.Element("name").Value),
                                                             clone = Convert.ToString(info.Element("code").Value),
                                                             uacs_code = Convert.ToString(info.Element("uacs_code").Value),
                                                             expenditure = Convert.ToString(info.Element("expenditure").Value)
                                                         };


                    ListMainExpenditures.Clear();


                    List<MainExpenditures> _list = new List<MainExpenditures>();

                    foreach (var item in _dataListsFetchMainExpenditure)
                    {
                        MainExpenditures _varProf = new MainExpenditures();


                        _varProf.id = item.id;
                        _varProf.code = item.code;
                        _varProf.name = item.name;
                        _varProf.uacs_code = item.uacs_code;
                        _varProf.expenditure = item.expenditure;
                     
                        _varProf.clone = item.clone;

                        _list.Add(_varProf);
                    }

                    var results = from p in _list
                                  group p by p.code into g
                                  select new
                                  {
                                      PAPCode = g.Key,
                                      ExpenseName = g.Select(m => m.name)
                                  };

                    foreach (var items in results)
                    {
                        MainExpenditures _data = new MainExpenditures();
                        foreach (var itemdata in items.ExpenseName.ToList())
                        {
                            _data.code = items.PAPCode;
                            _data.name = itemdata.ToString();
                            break;
                        }

                        var _listexp = _list.Where(iteml => iteml.code == items.PAPCode).ToList();
                        List<SubLibrary> _sublib = new List<SubLibrary>(); 
                        foreach (var itemexp in _listexp)
                        {
                            SubLibrary __sublib = new SubLibrary();
                            __sublib.Expenditure = itemexp.expenditure;
                            __sublib.UACS = itemexp.uacs_code;
                            _sublib.Add(__sublib);
                            
                        }
                        _data.ExpendituresItems = _sublib;
                        ListMainExpenditures.Add(_data);
                    }

                    grdData.ItemsSource = null;

                    grdData.ItemsSource = ListMainExpenditures;
                    try
                    {
                        grdData.Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
                    }
                    catch (Exception)
                    {
                        
      
                    }
                    try
                    {
                        grdData.Columns["clone"].Visibility = System.Windows.Visibility.Collapsed;
                    }
                    catch (Exception)
                    {
                 
                    }
                    try
                    {
                        grdData.Columns["code"].Visibility = System.Windows.Visibility.Collapsed;
                        grdData.Columns["expenditure"].Visibility = System.Windows.Visibility.Collapsed;
                        grdData.Columns["uacs_code"].Visibility = System.Windows.Visibility.Collapsed;
                        grdData.Columns["name"].HeaderText = "UACS Library";
                    }
                    catch (Exception)
                    {
                        
                       
                    }
                    
                
                   
                    foreach (var item in grdData.Rows)
                    {
                        if (item.HasChildren)
                        {

                            item.ChildBands[0].ColumnLayout.HeaderVisibility = Visibility.Collapsed;
                            item.IsExpanded = true;
                           
                           
                        }
                    }

                    this.Cursor = Cursors.Arrow;

                    break;
            }
        }
        private void SaveMainExpenditure()
        {
            c_expen.Process = "SaveMainExpenditure";
            c_expen.SQLOperation += c_expen_SQLOperation;
            c_expen.SaveMainExpenditure(txtCode.Text, txtName.Text);

        }
        private void UpdateExpenditure(String _id, String _code, String _name,String _oldVal)
        {
            c_expen.Process = "UpdateMainExpenditure";
            c_expen.SQLOperation += c_expen_SQLOperation;
            c_expen.UpdateMainExpenditure(_id, _code, _name, _oldVal);
        }

        void c_expen_SQLOperation(object sender, EventArgs e)
        {
            switch (c_expen.Process)
            {
                case "SaveMainExpenditure":
                    FetchMainExpenditure();
                    break;              
                case "UpdateMainExpenditure":
                    FetchMainExpenditure();
                    break;
                case "SaveDataExpenseItem":
                    FetchMainExpenditure();
                    ClearData();
                    break;
                case "RemoveExpenditure":
                    FetchMainExpenditure();
                    break;
                case "RemoveMainExpenditure":
                    FetchMainExpenditure();
                    break;
            }
        }
        private void FetchMainExpenditure()
        {
            c_expen.Process = "FetchMainExpenditure";
            svc_mindaf.ExecuteSQLAsync(c_expen.FetchMainExpenditureLists());

        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void frm_b_expen_Loaded(object sender, RoutedEventArgs e)
        {
            FetchMainExpenditure();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            SaveMainExpenditure();
        }

        private void grdData_CellExitedEditMode(object sender, Infragistics.Controls.Grids.CellExitedEditingEventArgs e)
        {
            try
            {

                UpdateExpenditure(grdData.Rows[e.Cell.Row.Index].Cells["id"].Value.ToString(), grdData.Rows[e.Cell.Row.Index].Cells["code"].Value.ToString(), grdData.Rows[e.Cell.Row.Index].Cells["name"].Value.ToString(), grdData.Rows[e.Cell.Row.Index].Cells["clone"].Value.ToString());
            }
            catch (Exception)
            {


            }
        }
        private void ClearData() 
        {
            txtExpenditureID.Text = "";
            txtExpenseName.Text = "";
            txtUACS.Text = "";


        }

        private void AddExpenditure()
        {

            c_expen.Process = "SaveDataExpenseItem";
            c_expen.SQLOperation += c_expen_SQLOperation;
            c_expen.SaveExpenditure(txtExpenditureID.Text, txtExpenseName.Text, txtUACS.Text);
        }

        private void btnAddExpenseItem_Click(object sender, RoutedEventArgs e)
        {
            if (txtExpenditureID.Text == "")
            {
                MessageBox.Show("Please Select Main Expenditure");
            }
            else
            {
                tbExpenseItem.IsEnabled = true;
            }
          
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            tbExpenseItem.IsEnabled = false;
            ClearData();
            grdData.Focus();
        }

        private void grdData_ActiveCellChanged(object sender, EventArgs e)
        {
           
                try
                {
                    
                        txtExpenditureID.Text = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["code"].Value.ToString();
                    
                   
                }
                catch (Exception)
                {


                }
          
           
        }
        private void RemoveMainExpenditure()
        {
            try
            {
                string _code = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["code"].Value.ToString();
                c_expen.Process = "RemoveMainExpenditure";
                c_expen.SQLOperation += c_expen_SQLOperation;
                c_expen.RemoveMainExpenditure(_code);
              //  _selected_uacs = "";
            }
            catch (Exception)
            {

            }

        }
        private void RemoveExpenditure() 
        {
            try
            {
             
                c_expen.Process = "RemoveExpenditure";
                c_expen.SQLOperation+=c_expen_SQLOperation;
                c_expen.RemoveExpenditure(_selected_uacs);
                _selected_uacs = "";
            }
            catch (Exception)
            {
                
            }
          
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
           
            AddExpenditure();
        }

        private void btnRemoveExpenditure_Click(object sender, RoutedEventArgs e)
        {
            RemoveExpenditure();
        }
        private String _selected_uacs = "";
        private void grdData_CellClicked(object sender, Infragistics.Controls.Grids.CellClickedEventArgs e)
        {
            try
            {
                _selected_uacs = grdData.Rows[grdData.ActiveCell.Row.Index].ChildBands[0].Rows[e.Cell.Row.Index].Cells["UACS"].Value.ToString();
           
            }
            catch (Exception)
            {

            }
           
        }

        private void btnRemoveMainExp_Click(object sender, RoutedEventArgs e)
        {
            RemoveMainExpenditure();
        }
    }
}

