﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmListExpenditures : ChildWindow
    {
        public String AccountableID { get; set; }
        public String ActivityID { get; set; }
        public String _Year { get; set; }
        public String _Month { get; set; }
        public String DivisionId { get; set; }
        public String FundSourceId { get; set; }
        public String FundName { get; set; }
        public Boolean IsDynamic { get; set; }

        public event EventHandler SelectedExpenditure;
        public String SelectedCode { get; set; }
        public String SelectedName { get; set; }
        public String MOOE_ID { get; set; }
        private  MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsExpenditures c_expenditures = new clsExpenditures();
        private List<Expenditures> ListExpenditures = new List<Expenditures>();
        private List<FundSourceLib> ListFundSource = new List<FundSourceLib>();

        public frmListExpenditures(String _div_id = "")
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            this.DivisionId = _div_id;
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_expenditures.Process)
            {
                case "FetchExpenditures":
                     XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new Expenditures
                                     {
                                         id = Convert.ToInt32(info.Element("id").Value),
                                         mooe_id = Convert.ToString(info.Element("mooe_id").Value),
                                         name = Convert.ToString(info.Element("name").Value),
                                         isdynamic = Convert.ToBoolean(info.Element("is_dynamic").Value)
                                   
                                     };

                    ListExpenditures.Clear();
               

                    foreach (var item in _dataLists)
                    {
                        if (item.id == 40 || item.id == 41 || item.id == 42)
                        {
                            
                        }
                        else
                        {
                            Expenditures _varDetails = new Expenditures();


                            _varDetails.name = item.name;
                            _varDetails.mooe_id = item.mooe_id;
                            _varDetails.id = item.id;
                            _varDetails.isdynamic = item.isdynamic;


                            ListExpenditures.Add(_varDetails);
                        }
                       
                    }

                    FetchFundSource();
                    this.Cursor = Cursors.Arrow;

                    break;
                case "FetchFundSource":
                    XDocument oDocKeyResultsFetchFundSource = XDocument.Parse(_results);
                    var _dataListsFetchFundSource = from info in oDocKeyResultsFetchFundSource.Descendants("Table")
                                     select new FundSourceLib
                                     {
                                         Fund_Name = Convert.ToString(info.Element("Fund_Name").Value),
                                         Fund_Source_Id = Convert.ToString(info.Element("Fund_Source_Id").Value)
                                     };

                    ListFundSource.Clear();

                    cmbFundSource.Items.Clear();

                    foreach (var item in _dataListsFetchFundSource)
                    {
                        
                            FundSourceLib _varDetails = new FundSourceLib();


                            _varDetails.Fund_Name = item.Fund_Name;
                            _varDetails.Fund_Source_Id = item.Fund_Source_Id;



                            ListFundSource.Add(_varDetails);
                            cmbFundSource.Items.Add(item.Fund_Name);
                    }
                    
                    grdData.ItemsSource = ListExpenditures;
                    grdData.Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["mooe_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["isdynamic"].Visibility = System.Windows.Visibility.Collapsed;
                    this.Cursor = Cursors.Arrow;
                    if (FundName != null)
                    {
                        for (int i = 0; i < cmbFundSource.Items.Count; i++)
                        {
                            if (FundName == cmbFundSource.Items[i].ToString())
                            {
                                cmbFundSource.SelectedIndex = i;
                                cmbFundSource.IsEnabled = false;
                                OKButton.IsEnabled = true;
                                OKButton.Focus();
                                break;
                            }
                        }
                    }
                    break;
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var y = ListFundSource.Where(items => items.Fund_Name == cmbFundSource.SelectedItem.ToString());
                foreach (var item in y)
                {
                    FundSourceId = item.Fund_Source_Id;
                    break;
                }
            }
            catch (Exception)
            {

            }
            if (SelectedExpenditure!=null)
            {
                this.SelectedExpenditure(sender, new EventArgs());
            }
            this.DialogResult = true;
        }
        private void FetchExpenditures() 
        {
            c_expenditures.Process = "FetchExpenditures";
            svc_mindaf.ExecuteSQLAsync(c_expenditures.FetchExpenditures());

        }
        private void FetchFundSource()
        {
            c_expenditures.Process = "FetchFundSource";
            svc_mindaf.ExecuteSQLAsync(c_expenditures.FetchFundsSource(this.DivisionId));

        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void frmExpenditures_Loaded(object sender, RoutedEventArgs e)
        {
            FetchExpenditures();
        }

        private void grdData_CellClicked(object sender, Infragistics.Controls.Grids.CellClickedEventArgs e)
        {
            SelectedCode = e.Cell.Value.ToString();
            List<Expenditures> x = ListExpenditures.Where(item => item.name == SelectedCode).ToList();
           
           
            foreach (var item in x)
            {
                MOOE_ID = item.id.ToString();
                IsDynamic =  item.isdynamic;
                SelectedName = item.name;
            }

            
        }

        private void cmbFundSource_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                if (cmbFundSource.SelectedItem.ToString() != "")
                {
                    OKButton.IsEnabled = true;
                }
                else
                {
                    MessageBox.Show("Select Fund Source");
                }
            }
            catch (Exception)
            {
                
                
            }
         
        }
    }
}

