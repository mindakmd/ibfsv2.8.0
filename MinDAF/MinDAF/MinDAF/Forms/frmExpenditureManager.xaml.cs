﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmExpenditureManager : ChildWindow
    {

        private List<MainExpenditures> ListMainExpenditures = new List<MainExpenditures>();
        private List<SubExpenditures> ListGridData = new List<SubExpenditures>();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsExpenditureManagement c_expen = new clsExpenditureManagement();

        public frmExpenditureManager()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_expen.Process)
            {
                case "FetchExpenditure":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new SubExpenditures
                                     {

                                         id = Convert.ToString(info.Element("id").Value),
                                         is_active = Convert.ToString(info.Element("is_active").Value),
                                         mooe_id = Convert.ToString(info.Element("mooe_id").Value),
                                         name = Convert.ToString(info.Element("name").Value),
                                         uacs_code = Convert.ToString(info.Element("uacs_code").Value)
                                     };


                    ListGridData.Clear();
                    foreach (var item in _dataLists)
                    {
                        SubExpenditures _varProf = new SubExpenditures();


                        _varProf.id = item.id;
                        _varProf.is_active = item.is_active;
                        _varProf.mooe_id = item.mooe_id;
                        _varProf.name = item.name;
                        _varProf.uacs_code = item.uacs_code;

                        ListGridData.Add(_varProf);

                    }

                    this.Cursor = Cursors.Arrow;
                    grdData.ItemsSource = null;
                    grdData.ItemsSource = ListGridData;
                    grdData.Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["is_active"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["mooe_id"].Visibility = System.Windows.Visibility.Collapsed;
                    FetchMainExpenditure();
                    break;
                case "FetchMainExpenditure":
                    XDocument oDocKeyResultsFetchMainExpenditure = XDocument.Parse(_results);
                    var _dataListsFetchMainExpenditure = from info in oDocKeyResultsFetchMainExpenditure.Descendants("Table")
                                     select new MainExpenditures
                                     {

                                         id = Convert.ToString(info.Element("id").Value),
                                         code = Convert.ToString(info.Element("code").Value),
                                         name = Convert.ToString(info.Element("name").Value)
                                     };


                    cmbExpenditures.Items.Clear();
                    foreach (var item in _dataListsFetchMainExpenditure)
                    {
                        MainExpenditures _varProf = new MainExpenditures();


                        _varProf.id = item.id;
                        _varProf.code = item.code;
                        _varProf.name = item.name;

                        cmbExpenditures.Items.Add(item.name);
                        ListMainExpenditures.Add(_varProf);
                    }

                    cmbExpenditures.SelectedIndex = -1;
                    this.Cursor = Cursors.Arrow;
              
                    break;
            }
        }

        private void FetchExpenditure() 
        {
            c_expen.Process = "FetchExpenditure";
            svc_mindaf.ExecuteSQLAsync(c_expen.FetchExpenditureLists());
          
        }
        private void FetchMainExpenditure()
        {
            c_expen.Process = "FetchMainExpenditure";
            svc_mindaf.ExecuteSQLAsync(c_expen.FetchMainExpenditureLists());

        }
        private void AddExpenditure() 
        {

            c_expen.Process = "SaveData";
            c_expen.SQLOperation += c_expen_SQLOperation;
            c_expen.SaveExpenditure(txtCode.Text, txtName.Text,txtUACS.Text);
        }
        private void UpdateExpenditure(String _id,String _code,String _name,String _uacs)
        {
            c_expen.Process = "UpdateData";
            c_expen.SQLOperation += c_expen_SQLOperation;
            c_expen.UpdateExpenditure(_id,_code,_name,_uacs);
        }

        void c_expen_SQLOperation(object sender, EventArgs e)
        {
            switch (c_expen.Process)
            {
                case "SaveData":
                    FetchExpenditure();
                      txtCode.Text = "";
                    txtName.Text = "";
                    break;
                case "UpdateData":
                    FetchExpenditure();
                
                    break;
                default:
                    break;
            }
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void frm_b_expenm_Loaded(object sender, RoutedEventArgs e)
        {
            FetchExpenditure();
        }

        private void cmbExpenditure_DropDownClosed(object sender, EventArgs e)
        {
          
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            AddExpenditure();
        }

        private void grdData_CellExitedEditMode(object sender, Infragistics.Controls.Grids.CellExitedEditingEventArgs e)
        {
            try
            {
                UpdateExpenditure(grdData.Rows[e.Cell.Row.Index].Cells["id"].Value.ToString(), grdData.Rows[e.Cell.Row.Index].Cells["mooe_id"].Value.ToString(), grdData.Rows[e.Cell.Row.Index].Cells["name"].Value.ToString(), grdData.Rows[e.Cell.Row.Index].Cells["uacs_code"].Value.ToString());
            }
            catch (Exception)
            {
                
              
            }
        }

        private void cmbExpenditures_DropDownClosed(object sender, EventArgs e)
        {
            if (cmbExpenditures.SelectedItem != null)
            {
                List<MainExpenditures> x = ListMainExpenditures.Where(items => items.name == cmbExpenditures.SelectedItem.ToString()).ToList();
                foreach (var item in x)
                {
                    txtCode.Text = item.code;
                }
            }
        }
    }
}

