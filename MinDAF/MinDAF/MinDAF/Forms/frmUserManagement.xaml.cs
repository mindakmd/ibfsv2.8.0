﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmUserManagement : ChildWindow
    {
        public String DivisionId { get; set; }
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsPersonnelManagement c_personnel = new clsPersonnelManagement();
        private List<PersonnelList> ListPersonnel = new List<PersonnelList>();
        private List<UserDivisions> ListDivisions = new List<UserDivisions>();
        public frmUserManagement()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            c_personnel.SQLOperation += c_personnel_SQLOperation;
        }

        void c_personnel_SQLOperation(object sender, EventArgs e)
        {
            switch (c_personnel.Process)
            {
                case "SaveData":
                    FetchPersonnel();
                    ClearData();
                    ControlStatus(false);
                    break;
                case "UpdateData":
                    FetchPersonnel();
                    ClearData();
                    ControlStatus(false);
                    break;

            }
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_personnel.Process)
            {
                case "FetchPersonnel":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new PersonnelList
                                     {
                                         Division_Id = Convert.ToString(info.Element("Division_Id").Value),
                                         is_admin = Convert.ToString(info.Element("is_admin").Value),
                                         Password = Convert.ToString(info.Element("Password").Value),
                                         status = Convert.ToString(info.Element("status").Value),
                                         User_Contact = Convert.ToString(info.Element("User_Contact").Value),
                                         User_Fullname = Convert.ToString(info.Element("User_Fullname").Value),
                                         User_Id = Convert.ToString(info.Element("User_Id").Value),
                                         User_Position = Convert.ToString(info.Element("User_Position").Value),
                                         Username = Convert.ToString(info.Element("Username").Value),

                                     };

                    ListPersonnel.Clear();


                    foreach (var item in _dataLists)
                    {
                        PersonnelList _varDetails = new PersonnelList();

                        _varDetails.Division_Id = item.Division_Id;
                        _varDetails.is_admin = item.is_admin;
                        _varDetails.Password = item.Password;
                        _varDetails.status = item.status;
                        _varDetails.User_Contact = item.User_Contact;
                        _varDetails.User_Fullname = item.User_Fullname;
                        _varDetails.User_Id = item.User_Id;
                        _varDetails.User_Position = item.User_Position;
                        _varDetails.Username = item.Username;

                        ListPersonnel.Add(_varDetails);
                    }


                    grdData.ItemsSource = null;
                    grdData.ItemsSource = ListPersonnel;
                    grdData.Columns["Division_Id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["User_Id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["is_admin"].Visibility = System.Windows.Visibility.Collapsed;
                    this.Cursor = Cursors.Arrow;
                 
                    break;
                case "FetchUserDivisions":
                    XDocument oDocKeyResultsFetchUserDivisions = XDocument.Parse(_results);
                    var _dataListsFetchUserDivisions = from info in oDocKeyResultsFetchUserDivisions.Descendants("Table")
                                     select new UserDivisions
                                     {
                                         DBM_Sub_Pap_Id = Convert.ToString(info.Element("DBM_Sub_Pap_Id").Value),
                                         Division_Code = Convert.ToString(info.Element("Division_Code").Value),
                                         Division_Desc = Convert.ToString(info.Element("Division_Desc").Value),
                                         Division_Id = Convert.ToString(info.Element("Division_Id").Value)
                         
                                     };

                    ListDivisions.Clear();
                    cmbDivision.Items.Clear();

                    foreach (var item in _dataListsFetchUserDivisions)
                    {
                        UserDivisions _varDetails = new UserDivisions();

                        _varDetails.DBM_Sub_Pap_Id = item.DBM_Sub_Pap_Id;
                        _varDetails.Division_Code = item.Division_Code;
                        _varDetails.Division_Desc = item.Division_Desc;
                        _varDetails.Division_Id = item.Division_Id;
                      
                        ListDivisions.Add(_varDetails);
                        cmbDivision.Items.Add(item.Division_Desc);
                    }


                  
                    this.Cursor = Cursors.Arrow;

                    break;
            }
        }
        private void GenerateRole()
        {
            cmbRole.Items.Clear();
            cmbRole.Items.Add("Administrator");
            cmbRole.Items.Add("Division Head");
            cmbRole.Items.Add("User");
        }

        private void FetchPersonnel()
        {

            List<UserDivisions> _data = ListDivisions.Where(items => items.Division_Desc == cmbDivision.SelectedItem.ToString()).ToList();
            foreach (var item in _data)
            {
                c_personnel.Process = "FetchPersonnel";
                svc_mindaf.ExecuteSQLAsync(c_personnel.FetchPersonnelListsAdmin(item.Division_Id));
                break;
            }
        }   
           
           
        private void FetchUserDivisions()
        {
            c_personnel.Process = "FetchUserDivisions";
            svc_mindaf.ExecuteSQLAsync(c_personnel.FetchDivisions());
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void LoadSelected()
        {
            txtContact.Text = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Contact"].Value.ToString();
            txtFullName.Text = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
            txtPassword.Text = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["Password"].Value.ToString();
            txtPosition.Text = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Position"].Value.ToString();
            txtUsername.Text = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["Username"].Value.ToString();

            if (grdData.Rows[grdData.ActiveCell.Row.Index].Cells["is_admin"].Value.ToString() == "1")
            {
                cmbRole.SelectedIndex = 0;
            }
            else
            {
                cmbRole.SelectedIndex = 1;
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void ControlStatus(Boolean _stat)
        {
            txtContact.IsEnabled = _stat;
            txtFullName.IsEnabled = _stat;
            txtPassword.IsEnabled = _stat;
            txtPosition.IsEnabled = _stat;
            txtUsername.IsEnabled = _stat;
            cmbRole.IsEnabled = _stat;
        }
     
        private void ClearData()
        {
            txtContact.Text = "";
            txtFullName.Text = "";
            txtPassword.Text = "";
            txtPosition.Text = "";
            txtUsername.Text = "";
            cmbRole.SelectedIndex = -1;

        }
        private void SaveData()
        {
            String Contact = txtContact.Text;
            String FullName = txtFullName.Text;
            String Password = txtPassword.Text;
            String Position = txtPosition.Text;
            String Username = txtUsername.Text;
            String Role = "";

            if (cmbRole.SelectedItem.ToString() == "Administrator")
            {
                Role = "1";
            }
            else if (cmbRole.SelectedItem.ToString() == "Division Head")
            {
                Role = "2";
            }
            else
            {
                Role = "0";
            }
            List<UserDivisions> _data = ListDivisions.Where(items => items.Division_Desc == cmbDivision.SelectedItem.ToString()).ToList();
            foreach (var item in _data)
            {
                c_personnel.Process = "SaveData";
                c_personnel.SavePersonnel(item.Division_Id, FullName, Contact, Position, Username, Password, "registered", Role);
                break;
            }
          
        }
        private void UpdateData(String UserId, String Division_Id, String User_Fullname, String User_Contact, String User_Position,
        String Username, String Password, String status, String is_admin)
        {

            c_personnel.Process = "UpdateData";
            c_personnel.SaveUpdatePersonnel(UserId, Division_Id, User_Fullname, User_Contact, User_Position, Username, Password, status, is_admin);
        }
       

        private void grdData_CellClicked(object sender, Infragistics.Controls.Grids.CellClickedEventArgs e)
        {
            LoadSelected();

        }

        private void grdData_CellExitedEditMode(object sender, Infragistics.Controls.Grids.CellExitedEditingEventArgs e)
        {
            String UserId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Id"].Value.ToString();
            String Contact = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Contact"].Value.ToString();
            String FullName = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
            String Password = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["Password"].Value.ToString();
            String Position = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Position"].Value.ToString();
            String Username = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["Username"].Value.ToString();
            String Role = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["is_admin"].Value.ToString();

            List<UserDivisions> _data = ListDivisions.Where(items => items.Division_Desc == cmbDivision.SelectedItem.ToString()).ToList();
            foreach (var item in _data)
            {
                UpdateData(UserId, item.Division_Id, FullName, Contact, Position, Username, Password, "registered", Role);
                break;
            }
           
        }

        private void frm_user_mangement_Loaded(object sender, RoutedEventArgs e)
        {
            FetchUserDivisions();
            GenerateRole();
        }

        private void cmbDivision_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                FetchPersonnel();
            }
            catch (Exception)
            {

            }
        }
       
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
        }

    }
}

