﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmBudgetAllocationMooe : ChildWindow
    {
        public String OfficeId { get; set; }
        public String DivisionId { get; set; }
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();

        private List<DivisionBudgetAllocationFields> ListDivision = new List<DivisionBudgetAllocationFields>();
        private List<lib_DivisionBudgetExpeniture> ListExpenditure = new List<lib_DivisionBudgetExpeniture>();
        private List<OfficeDataFields> ListOfficeData = new List<OfficeDataFields>();
        private List<lib_BudgetAllocationExpenditure> ListBudgetExpenditure = new List<lib_BudgetAllocationExpenditure>();
        private clsDivisionBudgetAllocation c_div = new clsDivisionBudgetAllocation();

        public frmBudgetAllocationMooe(String _DivisionId)
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {

            var _results = e.Result.ToString();
            switch (c_div.Process)
            {
                case "FetchDivisionPerOffice":
                    XDocument oDocKeyResults = XDocument.Parse(_results);

                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new DivisionBudgetAllocationFields
                                     {
                                         DBM_Sub_Pap_Id = Convert.ToString(info.Element("DBM_Sub_Pap_Id").Value),
                                         Division_Code = Convert.ToString(info.Element("Division_Code").Value),
                                         Division_Desc = Convert.ToString(info.Element("Division_Desc").Value),
                                         Division_Id = Convert.ToString(info.Element("Division_Id").Value)

                                     };


                    ListDivision.Clear();
                    List<ProfData> _ComboList = new List<ProfData>();

                    foreach (var item in _dataLists)
                    {
                        ProfData _varProf = new ProfData();
                        DivisionBudgetAllocationFields _temp = new DivisionBudgetAllocationFields();

                        _varProf._Name = item.Division_Desc;

                        _temp.DBM_Sub_Pap_Id = item.DBM_Sub_Pap_Id;
                        _temp.Division_Code = item.Division_Code;
                        _temp.Division_Desc = item.Division_Desc;
                        _temp.Division_Id = item.Division_Id;

                        _ComboList.Add(_varProf);
                        ListDivision.Add(_temp);

                    }
                    cmbDivision.ItemsSource = null;
                    cmbDivision.ItemsSource = _ComboList;
                  //  cmbYear.SelectedIndex = 0;
                    cmbDivision.SelectedIndex = 0;
                    FetchExpenditureItems();
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchExpenditureItems":
                    XDocument oDocKeyResultsFetchExpenditureItems = XDocument.Parse(_results);

                    var _dataListsFetchExpenditureItems = from info in oDocKeyResultsFetchExpenditureItems.Descendants("Table")
                                     select new lib_DivisionBudgetExpeniture
                                     {
                                         name = Convert.ToString(info.Element("name").Value),
                                         uacs_code = Convert.ToString(info.Element("uacs_code").Value)
                                       
                                     };


                    ListExpenditure.Clear();
                    List<ProfData> _ComboListExpenditure = new List<ProfData>();

                    foreach (var item in _dataListsFetchExpenditureItems)
                    {
                        ProfData _varProf = new ProfData();
                        lib_DivisionBudgetExpeniture _temp = new lib_DivisionBudgetExpeniture();

                        _varProf._Name = item.name;

                        _temp.name = item.name;
                        _temp.uacs_code = item.uacs_code;

                        cmbExpenditures.Items.Add(item.name);
                     //   _ComboListExpenditure.Add(_varProf);
                        ListExpenditure.Add(_temp);

                    }
                    //cmbExpenditures.ItemsSource = null;
                    //cmbExpenditures.ItemsSource = _ComboListExpenditure;
                    //cmbYear.SelectedIndex = 0;
                    cmbDivision.SelectedIndex = 0;
                     FetchOfficeData();
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchBudgetExpenditureAllocation":
                    try
                    {
                        XDocument oDocKeyResultsFetchBudgetExpenditureAllocation = XDocument.Parse(_results);

                        var _dataListsFetchBudgetExpenditureAllocation = from info in oDocKeyResultsFetchBudgetExpenditureAllocation.Descendants("Table")
                                                                         select new lib_BudgetAllocationExpenditure
                                                                         {
                                                                             Id = Convert.ToString(info.Element("id").Value),
                                                                             Name = Convert.ToString(info.Element("name").Value),
                                                                             Amount = Convert.ToString(info.Element("Amount").Value)

                                                                         };


                        ListBudgetExpenditure.Clear();

                        foreach (var item in _dataListsFetchBudgetExpenditureAllocation)
                        {

                            lib_BudgetAllocationExpenditure _temp = new lib_BudgetAllocationExpenditure();


                            _temp.Id = item.Id;
                            _temp.Name = item.Name;
                            _temp.Amount = Convert.ToDouble(item.Amount).ToString("#,##0.00");


                            ListBudgetExpenditure.Add(_temp);

                        }
                        grdData.ItemsSource = null;
                        grdData.ItemsSource = ListBudgetExpenditure;
                        grdData.Columns["Id"].Visibility = System.Windows.Visibility.Collapsed;
                        ComputeTotal();
                    }
                    catch (Exception)
                    {
                        grdData.ItemsSource = null;
                        grdData.ItemsSource = ListBudgetExpenditure;
                    }
                   
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchOfficeData":
                    XDocument oDocKeyResultsFetchOfficeData = XDocument.Parse(_results);

                    var _dataListsFetchOfficeData = from info in oDocKeyResultsFetchOfficeData.Descendants("Table")
                                                    select new OfficeDataFields
                                                    {
                                                        id = Convert.ToString(info.Element("id").Value),
                                                        Amount = Convert.ToString(info.Element("budget_allocation").Value),
                                                        entry_year = Convert.ToString(info.Element("entry_year").Value),
                                                        Fund_Name = Convert.ToString(info.Element("Fund_Name").Value),
                                                        status = Convert.ToString(info.Element("status").Value)



                                                    };


                    ListOfficeData.Clear();
                    List<ProfData> _ComboListFundSource = new List<ProfData>();

                    foreach (var item in _dataListsFetchOfficeData)
                    {
                        ProfData _varProf = new ProfData();

                        OfficeDataFields _temp = new OfficeDataFields();

                        _varProf._Name = item.Fund_Name;

                        _temp.Amount = item.Amount;
                        _temp.entry_year = item.entry_year;
                        _temp.Fund_Name = item.Fund_Name;
                        _temp.status = item.status;
                        _temp.id = item.id;

                        _ComboListFundSource.Add(_varProf);
                        ListOfficeData.Add(_temp);

                    }
                    cmbFundSource.ItemsSource = null;
                    cmbFundSource.ItemsSource = _ComboListFundSource;
                    this.Cursor = Cursors.Arrow;
                  
                  //  FetchExpenditureItems();
                    break;
            }
        }
        private void RemoveBudget()
        {
            var selectedItem = cmbDivision.SelectedItem as ProfData;
            var selectedFundSource = cmbFundSource.SelectedItem as ProfData;
            String selectedExpenditure = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["Name"].Value.ToString();

            if (selectedItem != null)
            {
                var xSearch = ListDivision.Where(items => items.Division_Desc == selectedItem._Name).ToList();
                var xFundSource = ListOfficeData.Where(items => items.Fund_Name == selectedFundSource._Name).ToList();
                var xEdpenditure = ListExpenditure.Where(items => items.name == selectedExpenditure).ToList();
                c_div.Process = "RemoveBudgetExpenditure";
                c_div.SQLOperation+=c_div_SQLOperation;
                c_div.RemoveBudgetExpenditure(xFundSource[0].id,xSearch[0].Division_Id,xEdpenditure[0].uacs_code,lblYear.Content.ToString());
            }

        }
        private void GenerateYear()
        {
            //int _year = DateTime.Now.Year;
            //int _limit = _year + 100;

            //for (int i = _year; i != _limit; i++)
            //{
            //    cmbYear.Items.Add(_year);
            //    _year += 1;
            //}

        }
        private void FetchDivisionPerOffice()
        {
            
                c_div.Process = "FetchDivisionPerOffice";
                svc_mindaf.ExecuteSQLAsync(c_div.FetchDivisionPerOffice(OfficeId));
       
        }
        private void ComputeTotal()
        {
            double _total = 0.00;
            
            foreach (var item in grdData.Rows)
            {
                _total += Convert.ToDouble(item.Cells["Amount"].Value.ToString()); 
            }

            txtTotal.Value = _total;
        }
        private void SelectedDivision()
        {
            //var selectedItem = cmbDivision.SelectedItem as ProfData;
            //if (selectedItem != null)
            //{
            //    var x = ListDivision.Where(items => items.Division_Desc == selectedItem._Name).ToList();
            //    if (x.Count!=0)
            //    {
            //        c_div.Process = "FetchDivisionPerOffice";
            //        svc_mindaf.ExecuteSQLAsync(c_div.FetchDivisionPerOffice(x[0].Division_Id));
            //    }
               
            //}
            FetchOfficeData();
            cmbExpenditures.IsEnabled = false;
            txtBudget.IsEnabled = false;
            grdData.ItemsSource = null;
            lblFundSource.Content = "";
            lblYear.Content = "";
            txtBudget.Value = 0.00;
            txtTotal.Value = 0.00;
            try
            {
                cmbExpenditures.SelectedIndex = -1;
            }
            catch (Exception)
            {
                
      
            }

        }
        private void FetchBudgetExpenditureAllocation()
        {
            var selectedItem = cmbDivision.SelectedItem as ProfData;
            var selectedFundSource = cmbFundSource.SelectedItem as ProfData;
            if (selectedFundSource !=null)
            {
                var x = ListDivision.Where(items => items.Division_Desc == selectedItem._Name).ToList();
                var xFundSource = ListOfficeData.Where(items => items.Fund_Name == selectedFundSource._Name).ToList();
                if (x.Count != 0)
                {
                    c_div.Process = "FetchBudgetExpenditureAllocation";
                    svc_mindaf.ExecuteSQLAsync(c_div.FetchBudgetExpenditureAllocation(lblYear.Content.ToString(), x[0].Division_Id, xFundSource[0].id));
                }
            }
            

         
        }
        private void UpdateAmount(string _id,string _amount) 
        {
            c_div.Process = "UpdateAmount";
            c_div.SQLOperation+=c_div_SQLOperation;
            c_div.UpdateAmountExpenditure(_id,_amount);
        }
        private void FetchExpenditureItems()
        {
            c_div.Process = "FetchExpenditureItems";
            svc_mindaf.ExecuteSQLAsync(c_div.FetchExpenditureItems());
        }
        private void FetchOfficeData()
        {

            var selectedItem = cmbDivision.SelectedItem as ProfData;

            if (selectedItem != null)
            {
                c_div.Process = "FetchOfficeData";
                var x = ListDivision.Where(items => items.Division_Desc == selectedItem._Name).ToList();
                if (x.Count != 0)
                {
                    svc_mindaf.ExecuteSQLAsync(c_div.FetchOfficeData(x[0].Division_Id));
                }

            }


        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void ctrl_budget_alloc_mooe_Loaded(object sender, RoutedEventArgs e)
        {
            GenerateYear();
            FetchDivisionPerOffice();
        }
        private void SaveBudgetExpenditure()
        {

            //String DivId = "";
          //  Boolean EntryExists = false;
            var selectedItem = cmbDivision.SelectedItem as ProfData;
            var selectedFundSource = cmbFundSource.SelectedItem as ProfData;
            var selectedExpenditure = cmbExpenditures.SelectedItem as ProfData;

            if (selectedItem != null)
            {
                var xSearch = ListDivision.Where(items => items.Division_Desc == selectedItem._Name).ToList();
                var xFundSource = ListOfficeData.Where(items => items.Fund_Name == selectedFundSource._Name).ToList();
                var xEdpenditure = ListExpenditure.Where(items => items.name == cmbExpenditures.SelectedItem.ToString()).ToList();
                
                c_div.Process = "SaveDivisionBudgetExpenditure";
                c_div.SQLOperation += c_div_SQLOperation;
                c_div.SaveDivisionBudgetExpenditure(xFundSource[0].id, xSearch[0].Division_Id, xEdpenditure[0].uacs_code, txtBudget.Value.ToString(), lblYear.Content.ToString());
                ClearData();
            }

          
        }
        private void ClearData() 
        {
            cmbExpenditures.Focus();
            txtBudget.Value = 0.00;
        }

        void c_div_SQLOperation(object sender, EventArgs e)
        {

            switch (c_div.Process)
            {
                case "UpdateAmount":
                    FetchBudgetExpenditureAllocation();
                    break;
                case "SaveDivisionBudgetExpenditure":
                    FetchBudgetExpenditureAllocation();
                    break;
                case "RemoveBudgetExpenditure":
                    FetchBudgetExpenditureAllocation();
                    break;
            }
        }
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            SaveBudgetExpenditure();
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            RemoveBudget();
        }

        private void cmbDivision_DropDownClosed(object sender, EventArgs e)
        {
            SelectedDivision();
        }

        private void cmbFundSource_DropDownClosed(object sender, EventArgs e)
        {
            var selectedItem = cmbFundSource.SelectedItem as ProfData;

            if (selectedItem != null)
            {
               
                var x = ListOfficeData.Where(items => items.Fund_Name == selectedItem._Name).ToList();
                if (x.Count != 0)
                {
                    lblFundSource.Content = x[0].Fund_Name;
                    lblYear.Content = x[0].entry_year;

                    cmbExpenditures.IsEnabled = true;
                    txtBudget.IsEnabled = true;
                    cmbExpenditures.Focus();
                    FetchBudgetExpenditureAllocation();
                }

            }
        }

      

        private void txtBudget_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {

                btnAdd.Focus();
            
            }
        }

        private void cmbExpenditures_DropDownClosed(object sender, EventArgs e)
        {
            txtBudget.SelectAll();
            txtBudget.Focus();
           
        }

        private void grdData_CellExitedEditMode(object sender, Infragistics.Controls.Grids.CellExitedEditingEventArgs e)
        {
            if (e.Cell.Column.HeaderText =="Amount")
            {
                try
                {
                    string _id = grdData.Rows[e.Cell.Row.Index].Cells["Id"].Value.ToString();
                    string _amount = e.Cell.Value.ToString();
                    UpdateAmount(_id, _amount);
                }
                catch (Exception)
                {
                    
                }
            }
        }

    }
}

