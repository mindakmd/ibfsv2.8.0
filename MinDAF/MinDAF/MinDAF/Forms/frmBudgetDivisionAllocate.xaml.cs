﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmBudgetDivisionAllocate : ChildWindow
    {
        public String OfficeId { get; set; }
        public String DivisionId { get; set; }
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();

        private List<DivisionBudgetAllocationFields> ListDivision = new List<DivisionBudgetAllocationFields>();
        private List<OfficeDataFields> ListOfficeData = new List<OfficeDataFields>();
        private clsDivisionBudgetAllocation c_div = new clsDivisionBudgetAllocation();
        private List<RespoData> ListRespo = new List<RespoData>();
        public frmBudgetDivisionAllocate(String _DivisionId)
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            c_div.SQLOperation += c_div_SQLOperation;
            DivisionId = _DivisionId;
        }

        void c_div_SQLOperation(object sender, EventArgs e)
        {
            switch (c_div.Process)
            {
                case "SaveDivisionBudget":
                    FetchOfficeData();
                    break;
                case "UpdateDivisionBudget":
                    FetchOfficeData();
                    break;
                case "RemoveBudget":
                    FetchOfficeData();
                    break;
            }
        }
        private void ComputeTotal()
        {
            double _totals = 0.00;
            foreach (var item in ListOfficeData)
            {
                _totals += Convert.ToDouble(item.Amount);
            }

            txtTotal.Value = _totals;
        }
        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
             
            var _results = e.Result.ToString();
            switch (c_div.Process)
            {
                case "FetchRespo":
                    XDocument oDocKeyResultsFetchRespo = XDocument.Parse(_results);
                    var _dataListsFetchRespo = from info in oDocKeyResultsFetchRespo.Descendants("Table")
                                     select new RespoData
                                     {
                                         Id = Convert.ToString(info.Element("id").Value),
                                         Name = Convert.ToString(info.Element("respo_name").Value)
                                     };

                    ListRespo.Clear();
                    List<ProfData> _ComboListRespo = new List<ProfData>();

                    foreach (var item in _dataListsFetchRespo)
                    {
                        RespoData _varDetails = new RespoData();
                        ProfData _varProf = new ProfData();

                        _varProf._Name = item.Name;

                        _varDetails.Id = item.Id;
                        _varDetails.Name = item.Name;

                        _ComboListRespo.Add(_varProf);
                        ListRespo.Add(_varDetails);

                    }

                     cmbRespo.ItemsSource = null;
                     cmbRespo.ItemsSource = _ComboListRespo;
                     FetchOfficeData();
                    break;
                case "FetchDivisionPerOffice":
                    XDocument oDocKeyResults = XDocument.Parse(_results);

                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new DivisionBudgetAllocationFields
                                     {
                                         DBM_Sub_Pap_Id = Convert.ToString(info.Element("DBM_Sub_Pap_Id").Value),
                                         Division_Code = Convert.ToString(info.Element("Division_Code").Value),
                                         Division_Desc = Convert.ToString(info.Element("Division_Desc").Value),
                                         Division_Id = Convert.ToString(info.Element("Division_Id").Value)

                                     };


                    ListDivision.Clear();
                    List<ProfData> _ComboList = new List<ProfData>();

                    foreach (var item in _dataLists)
                    {
                        ProfData _varProf = new ProfData();
                        DivisionBudgetAllocationFields _temp = new DivisionBudgetAllocationFields();

                        _varProf._Name = item.Division_Desc;

                        _temp.DBM_Sub_Pap_Id = item.DBM_Sub_Pap_Id;
                        _temp.Division_Code = item.Division_Code;
                        _temp.Division_Desc = item.Division_Desc;
                        _temp.Division_Id = item.Division_Id;

                        _ComboList.Add(_varProf);
                        ListDivision.Add(_temp);

                    }
                    cmbDivision.ItemsSource = null;
                    cmbDivision.ItemsSource = _ComboList;
                    cmbYear.SelectedIndex = 0;
                    cmbDivision.SelectedIndex = 0;
                    FetchRespoCenter();
                    this.Cursor = Cursors.Arrow;
                    break; 
                case "FetchOfficeData":
                    XDocument oDocKeyResultsFetchOfficeData = XDocument.Parse(_results);

                    var _dataListsFetchOfficeData = from info in oDocKeyResultsFetchOfficeData.Descendants("Table")
                                     select new OfficeDataFields
                                     {
                                         id = Convert.ToString(info.Element("id").Value),
                                         Amount = Convert.ToString(info.Element("budget_allocation").Value),
                                         entry_year = Convert.ToString(info.Element("entry_year").Value),
                                         Fund_Name = Convert.ToString(info.Element("Fund_Name").Value),
                                         status = Convert.ToString(info.Element("Status").Value)

                                         

                                     };


                    ListOfficeData.Clear();
                   

                    foreach (var item in _dataListsFetchOfficeData)
                    {
                    
                        OfficeDataFields _temp = new OfficeDataFields();


                        _temp.Amount = Convert.ToDouble(item.Amount).ToString("#,##0.00");
                         _temp.entry_year = item.entry_year;
                         _temp.Fund_Name = item.Fund_Name;
                         _temp.status = item.status;
                         _temp.id = item.id;
                        ListOfficeData.Add(_temp);

                    }
                    grdData.ItemsSource = null;
                    grdData.ItemsSource = ListOfficeData;
                  //  grdData.Columns["division_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["entry_year"].Visibility = System.Windows.Visibility.Collapsed;
                  //  grdData.Columns["office_id"].Visibility = System.Windows.Visibility.Collapsed;
                   grdData.Columns["status"].Visibility = System.Windows.Visibility.Collapsed;
                   grdData.Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
                    ComputeTotal();
                    this.Cursor = Cursors.Arrow;
                    break;
            }
        }
        private void GenerateYear()
        {
            int _year = DateTime.Now.Year;
            int _limit = _year + 100;

            for (int i = _year; i != _limit; i++)
            {
                cmbYear.Items.Add(_year);
                _year += 1;
            }
          
        }
        private void Removebudget(String _id)
        {
            c_div.Process = "RemoveBudget";
            c_div.RemoveBudgetAllocation(_id);

        }
        private void SaveDivisionAllocation() 
        {
           

            //String DivId = "";
            Boolean EntryExists = false;
            var selectedItem = cmbDivision.SelectedItem as ProfData;
            var selectedRespo = cmbRespo.SelectedItem as ProfData;
            if (selectedItem!=null)
            {
                var xSearch = ListDivision.Where(items => items.Division_Desc == selectedItem._Name).ToList();
                var xSearchRespo = ListRespo.Where(items => items.Name == selectedRespo._Name).ToList();
                c_div.Process = "SaveDivisionBudget";
                c_div.SaveDivisionBudget(xSearch[0].Division_Id, xSearchRespo[0].Name, txtBudget.Value.ToString(), cmbYear.SelectedItem.ToString());
              
            }

          
           

        } 
        private void UpdateDivisionAllocation(String _id,String _Amount) 
        {
             
                c_div.Process = "UpdateDivisionBudget";
                c_div.UpdateDivisionBudget(_id, _Amount);
              
        
           

        }
        private void FetchDivisionPerOffice() 
        {
            c_div.Process = "FetchDivisionPerOffice";
            svc_mindaf.ExecuteSQLAsync(c_div.FetchDivisionPerOffice(OfficeId));
        }
        private void FetchRespoCenter()
        {
            c_div.Process = "FetchRespo";
            svc_mindaf.ExecuteSQLAsync(c_div.FetchRespoLibrary());
        }
        private void FetchOfficeData()
        {

            var selectedItem = cmbDivision.SelectedItem as ProfData;
         
            if (selectedItem != null)
            {
                c_div.Process = "FetchOfficeData";
                var x = ListDivision.Where(items => items.Division_Desc == selectedItem._Name).ToList();
                if (x.Count!=0)
                {
                    svc_mindaf.ExecuteSQLAsync(c_div.FetchOfficeData(x[0].Division_Id, cmbYear.SelectedItem.ToString()));
                }
              
            }

            
        }
      
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            SaveDivisionAllocation();
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            String _id = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["id"].Value.ToString();

            Removebudget(_id);
        }

        private void cmbYear_DropDownClosed(object sender, EventArgs e)
        {
            FetchOfficeData();
        }

        private void frm_dba_alloc_Loaded(object sender, RoutedEventArgs e)
        {
            GenerateYear();
            FetchDivisionPerOffice();
        }

        private void cmbDivision_DropDownClosed(object sender, EventArgs e)
        {
            FetchOfficeData();
        }

        private void grdData_CellDoubleClicked(object sender, Infragistics.Controls.Grids.CellClickedEventArgs e)
        {
           
        }

        private void grdData_CellExitedEditMode(object sender, Infragistics.Controls.Grids.CellExitedEditingEventArgs e)
        {
            if (e.Cell.Column.HeaderText =="Amount")
            {
                UpdateDivisionAllocation(grdData.Rows[e.Cell.Row.Index].Cells["id"].Value.ToString(),e.Cell.Value.ToString());
            }
        }
    }
}

