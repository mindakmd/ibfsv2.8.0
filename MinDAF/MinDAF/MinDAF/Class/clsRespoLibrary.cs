﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsRespoLibrary
    {

        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();

        public String FetchRespoLibrary()
        {
            var sb = new System.Text.StringBuilder(62);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  respo_name");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_respo_library;");


            return sb.ToString();
        }
        public void SaveRespo(String _name)
        {
            String _sqlString = "";

            var sb = new System.Text.StringBuilder(88);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_respo_library");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  respo_name");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '"+ _name +"'");
            sb.AppendLine(@");");



            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void UpdateRespo(String _id,String _name)
        {
            String _sqlString = "";

            var sb = new System.Text.StringBuilder(91);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_respo_library  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  respo_name ='"+ _name +"'");
            sb.AppendLine(@"WHERE ");
            sb.AppendLine(@"  id = "+_id +"");



            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void DeleteRespo(String _id)
        {
            String _sqlString = "";

            var sb = new System.Text.StringBuilder(48);
            sb.AppendLine(@"DELETE FROM ");
            sb.AppendLine(@"  dbo.mnda_respo_library ");
            sb.AppendLine(@"WHERE id ="+ _id +" ;");


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "SaveRespo":
                    if (SQLOperation!=null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateRespo":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "DeleteRespo":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
            }
        }

    }

    public class RespoData 
    {
        public String Id { get; set; }
        public String Name { get; set; }
    }
}
