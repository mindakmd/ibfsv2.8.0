﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsBudgetSupplies
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();

        public void UpdateSuspend(String _id, String _val)
        {


            String _sqlString = "";

            StringBuilder sb = new StringBuilder(79);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_activity_data  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  is_suspended =" + _val + "");
            sb.AppendLine(@"WHERE id = " + _id + ";");


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;

        }
        public String FetchProcurementSupplies() 
        {
            StringBuilder sb = new StringBuilder(163);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  item_specifications,");
            sb.AppendLine(@"  general_category,");
            sb.AppendLine(@"  sub_category,");
            sb.AppendLine(@"  minda_inventory,");
            sb.AppendLine(@"  unit_of_measure,");
            sb.AppendLine(@"  price");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_procurement_items;");

            return sb.ToString();
        }
        public String FetchProcurementDetails(String _activity_id,String fund_source_id,String _month)
        {
            StringBuilder sb = new StringBuilder(177);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  type_service,");
            sb.AppendLine(@"  activity_id,");
            sb.AppendLine(@"  ISNULL(procurement_id,'') as procurement_id,");
            sb.AppendLine(@"  accountable_id,");
            sb.AppendLine(@"  remarks,");
            sb.AppendLine(@"   ISNULL(quantity,0) as quantity,");
            sb.AppendLine(@"  rate,");
            sb.AppendLine(@"  month,");
            sb.AppendLine(@"  year,");
            sb.AppendLine(@" total");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_activity_data WHERE activity_id = " + _activity_id + " and mooe_sub_expenditure_id = '43' AND fund_source_id =" + fund_source_id + " and is_suspended =0 and month ='" + _month + "' ORDER By id DESC;;");

            return sb.ToString();
        }
        public void SaveExpenditureLibrary(String activity_id, String procurement_id, 
            String accountable_name, String remarks, String quantity, string total, String _month,
            String _year ,String _rate,String _type_service,string fund_source_id )
        {
            String _sqlString = "";
            StringBuilder sb = new StringBuilder(264);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_activity_data");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  activity_id,");
            sb.AppendLine(@"  procurement_id,");
            sb.AppendLine(@"  accountable_id,");
            sb.AppendLine(@"  mooe_sub_expenditure_id,");
            sb.AppendLine(@"  remarks,");
            sb.AppendLine(@"  quantity,");
            sb.AppendLine(@"  rate,");
            sb.AppendLine(@"  total,");
            sb.AppendLine(@"  month,");
            sb.AppendLine(@"  year,");
            sb.AppendLine(@"  type_service,");
            sb.AppendLine(@"  entry_date,");
            sb.AppendLine(@"  fund_source_id");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '" + activity_id + "',");
            sb.AppendLine(@"  '" + procurement_id + "',");
            sb.AppendLine(@"  '" + accountable_name + "',");
            sb.AppendLine(@"  '43',");
            sb.AppendLine(@"  '" + remarks + "',");
            sb.AppendLine(@"  " + quantity + ",");
            sb.AppendLine(@"  " + _rate + ",");
            sb.AppendLine(@"  " + total + ",");
            sb.AppendLine(@"  '" + _month + "',");
            sb.AppendLine(@"  " + _year +",");
            sb.AppendLine(@"  '" + _type_service + "',");
            sb.AppendLine(@"  " + DateTime.Now.ToShortDateString() +",");
            sb.AppendLine(@"  '" + fund_source_id + "'");
            sb.AppendLine(@");");



            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }

        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "SaveActivityItems":
                    if (SQLOperation!=null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "Suspend":
                    if (SQLOperation!=null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                default:
                    break;
            }
        }

    }
    public class ProcurementDetails 
    {
        public String id {get;set;}
        public String type_service { get; set; }
        public String activity_id {get;set;}
        public String procurement_id {get;set;}
        public String accountable_id { get; set; }
        public String remarks {get;set;}
        public String quantity {get;set;}
        public String rate {get;set;}
        public String month {get;set;}
        public String year {get;set;}
        public String total { get; set; }
    }
    public class ProcurementSupplies 
    {
     public String     id {get;set;}
     public String     item_specifications {get;set;}
     public String     general_category {get;set;}
     public String     sub_category {get;set;}
     public String     minda_inventory {get;set;}
     public String     unit_of_measure {get;set;}
     public String     price { get; set; }
    }
}
