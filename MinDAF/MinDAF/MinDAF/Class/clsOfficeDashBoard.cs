﻿
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsOfficeDashBoard
    {
        public event EventHandler ReceiveDivision;
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();
        MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();

        public String FetchOffice(String _office_id)
        {
            StringBuilder sb = new StringBuilder(147);
            sb.AppendLine(@"");
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"{fn CONCAT(dsp.DBM_Sub_Pap_Desc, {fn CONCAT(' - ', dsp.DBM_Sub_Pap_Code)}) } as office,dsp.DBM_Sub_Pap_Code as pap");
            sb.AppendLine(@"FROM DBM_Sub_Pap dsp");
            sb.AppendLine(@"WHERE dsp.DBM_Sub_Pap_id= " + _office_id + "");
            sb.AppendLine(@";");


            return sb.ToString();
        }
        public String FetchDivision(String _office_id)
        {
            StringBuilder sb = new StringBuilder(140);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"div.Division_Id as id, {fn CONCAT(div.Division_Desc, {fn CONCAT(' - ', div.Division_Code)}) } as Division");
            sb.AppendLine(@"FROM Division div");
            sb.AppendLine(@"WHERE div.DBM_Sub_Pap_Id= "+ _office_id +"");
            sb.AppendLine(@";");
            
            return sb.ToString();
        }
        public String FetchOfficeTotals(String _office_id,String _year)
        {
            StringBuilder sb = new StringBuilder(471);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"div.Division_Id as id,");
            sb.AppendLine(@"div.Division_Desc as division,div.Division_Code as pap_code,");
            sb.AppendLine(@"mba.budget_allocation,");
            sb.AppendLine(@"0.00 as total_program_budget,");
            sb.AppendLine(@"0.00 as balance");
            sb.AppendLine(@"FROM   Division div ");
            sb.AppendLine(@"INNER JOIN DBM_Sub_Pap dsp on dsp.DBM_Sub_Pap_id = div.DBM_Sub_Pap_Id");
            sb.AppendLine(@"INNER JOIN mnda_budget_allocation mba on mba.division_id  = div.Division_Id");
            sb.AppendLine(@"WHERE dsp.DBM_Pap_Id ="+ _office_id +" and mba.entry_year = "+ _year +"");
            sb.AppendLine(@"GROUP BY div.Division_Id,div.Division_Code,div.Division_Desc,mba.budget_allocation");

            return sb.ToString();
        }
        public String FetchBudgetAllocation(String division_id, String _year)
        {
            var sb = new System.Text.StringBuilder(431);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mooe.code as pap_code,");
            sb.AppendLine(@"msub.uacs_code as uacs_code,");
            sb.AppendLine(@"mooe.name as mo_name,");
            sb.AppendLine(@"msub.mooe_id as pap_sub,");
            sb.AppendLine(@"msub.name as Name,");
            sb.AppendLine(@"mfsl.Amount");
            sb.AppendLine(@"FROM mnda_fund_source mfs");
            sb.AppendLine(@"LEFT JOIN Division div on div.Division_Id = mfs.division_id");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source_line_item mfsl on mfsl.Fund_Source_Id = mfs.Fund_Source_Id");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures msub on msub.uacs_code = mfsl.mooe_id");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_expenditures mooe on mooe.code = msub.mooe_id");
            sb.AppendLine(@"WHERE div.Division_Id ="+ division_id +" AND mfs.Year ='"+ _year +"'");

        return sb.ToString();
        }

        public String FetchBalances(String _pap, String _year)
        {
            StringBuilder sb = new StringBuilder(424);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"lrc.Division,");
            sb.AppendLine(@"lppa.PPACode as Office,");
            sb.AppendLine(@"SUM(oorp.Amount) as Total");
            sb.AppendLine(@"FROM or_ObligationRequestParticular oorp");
            sb.AppendLine(@"LEFT  JOIN or_ObligationRequest obr on obr.id = oorp.ObligationRequestID");
            sb.AppendLine(@"LEFT  JOIN lib_ResponsibilityCenter lrc on lrc.ID = oorp.ResponsibilityCenterID");
            sb.AppendLine(@"LEFT  JOIN lib_PPA lppa ON lppa.ID = oorp.PPAID");
            sb.AppendLine(@"WHERE lppa.PPACode ='"+ _pap +"'");
            sb.AppendLine(@"AND YEAR(obr.DateCreated) = "+ _year +"");
            sb.AppendLine(@"GROUP BY lrc.Division ,lppa.PPACode");


            return sb.ToString();
        }

        public String FetchBalancesDB(String _pap, String _year)
        {
            StringBuilder sb = new StringBuilder(424);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  UACS,");
            sb.AppendLine(@"  PAP,");
            sb.AppendLine(@"  Jan,");
            sb.AppendLine(@"  Feb,");
            sb.AppendLine(@"  Mar,");
            sb.AppendLine(@"  Apr,");
            sb.AppendLine(@"  May,");
            sb.AppendLine(@"  June,");
            sb.AppendLine(@"  July,");
            sb.AppendLine(@"  Aug,");
            sb.AppendLine(@"  Sep,");
            sb.AppendLine(@"  Oct,");
            sb.AppendLine(@"  Nov,");
            sb.AppendLine(@"  Dec,");
            sb.AppendLine(@"  Years");
            sb.AppendLine(@"FROM dvDB dv");
            sb.AppendLine(@"WHERE dv.PAP = '"+ _pap +"' AND dv.Years = '"+ _year +"';");

            return sb.ToString();
        }
        public String FetchOfficeDetails(String _office_id, String _year)
        {
            StringBuilder sb = new StringBuilder(294);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mfs.Fund_Source_Id as fund_source_id,mfs.division_id as div_id,");
            sb.AppendLine(@"mfs.Fund_Name,");
            sb.AppendLine(@"mfs.Amount as budget_allocation,");
            sb.AppendLine(@"0.00 as total_program_budget,");
            sb.AppendLine(@"0.00 as balance");
            sb.AppendLine(@"FROM  Division div ");
            sb.AppendLine(@"INNER JOIN DBM_Sub_Pap dsp on dsp.DBM_Sub_Pap_id = div.DBM_Sub_Pap_Id");
            sb.AppendLine(@"INNER JOIN mnda_fund_source mfs on mfs.division_id = div.Division_Id");
            sb.AppendLine(@"WHERE dsp.DBM_Pap_Id = " + _office_id + " and mfs.Year = "+ _year +"");
            sb.AppendLine(@"ORDER BY mfs.Fund_Name");


            return sb.ToString();
        }
        
        
        public String FetchOfficeExpenditures(String _office_id, String _year)
        {
            StringBuilder sb = new StringBuilder(742);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mad.fund_source_id,mpe.acountable_division_code division_id,");
            sb.AppendLine(@"div.Division_Desc as division,");
            sb.AppendLine(@"mp.id as program_id,");
            sb.AppendLine(@"mp.name as program,");
            sb.AppendLine(@"mooe.name as expenditure_name,");
            sb.AppendLine(@"mooe.code as expen_code,");
            sb.AppendLine(@"moe.mooe_id,");
            sb.AppendLine(@"moe.name as expense_name,");
            sb.AppendLine(@"moe.uacs_code,mad.month,mad.is_suspended as status,");
            sb.AppendLine(@"SUM(mad.total) as total");
            sb.AppendLine(@"FROM DBM_Sub_Pap sub");
            sb.AppendLine(@"INNER JOIN  Division div on div.DBM_Sub_Pap_Id = sub.DBM_Sub_Pap_id");
            sb.AppendLine(@"INNER JOIN mnda_program_encoded  mpe on mpe.acountable_division_code = div.Division_Id");
            sb.AppendLine(@"INNER JOIN mnda_programs mp  on mp.id = mpe.main_program_id");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.output_id = mpo.id");
            sb.AppendLine(@"INNER JOIN mnda_activity_data mad on mad.activity_id = ma.id");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures moe on moe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"INNER JOIN mnda_mooe_expenditures mooe on mooe.code = moe.mooe_id");
            sb.AppendLine(@"WHERE mad.year ="+ _year +" AND sub.DBM_Pap_Id = "+ _office_id +"  AND mad.id IN (");
            sb.AppendLine(@"SELECT mapd.activity_id FROM mnda_approved_projects_division mapd");
            sb.AppendLine(@")");
            sb.AppendLine(@"GROUP BY mad.fund_source_id, mpe.acountable_division_code ,");
            sb.AppendLine(@"div.Division_Desc,mp.id,mp.name,");
            sb.AppendLine(@"mooe.code,");
            sb.AppendLine(@"moe.mooe_id,");
            sb.AppendLine(@"mooe.name,");
            sb.AppendLine(@"moe.name,");
            sb.AppendLine(@"moe.uacs_code,mad.month,mad.is_suspended ");



            return sb.ToString();
        }
    }

    public class BudgetExpenditureList 
    {
        public String uacs_code { get; set; }
         public String pap_code {get;set;}
         public String  mo_name {get;set;}
         public String pap_sub {get;set;}
         public String Name {get;set;}
         public String Amount { get; set; }
    }
    public class BudgetExpenitureMain 
    {
        public String MOOE_NAME { get; set; }
       
        public List<BudgetExpenditureDashBoard> ListExpenditure{get;set;}
    }

    public class BudgetExpenditureDashBoard     
    {
        public String UACS { get; set; }
        public String Expenditure { get; set; }
        public String Total { get; set; }
    }
    public class BudgetBalances 
    {
      public String Division {get;set;}
        public String  Office {get;set;}
        public String Total { get; set; }
    }
    public class OfficeTotals 
    {
             public String id {get;set;}
             public String  division  {get;set;}
             public String pap_code { get; set; }
             public String budget_allocation {get;set;}
             public String total_program_planned { get; set; }
             public String  total_program_budget {get;set;}
             public String balance { get; set; }
             public List<OfficeDetails> ListDetails { get; set; }
    }
    public class OfficeDetails
    {
        public String fund_source_id { get; set; }
        public String division_id { get; set; }
        public String Fund_Source { get; set; }
        public String pap_code { get; set; }
        public String budget_allocation { get; set; }
        public String total_program_planned { get; set; }
        public String total_program_budget { get; set; }
        public String balance { get; set; }
      
    }
    public class OfficeLevel 
    {
        public String Office { get; set; }
        public String Pap { get; set; }
        public List<DivisionLevel> DivisionList { get; set; }
        public String UACS { get; set; }
      //  public string Alloted { get; set; }
        public string Jan { get; set; }
        public string Feb { get; set; }
        public string Mar { get; set; }
        public string Apr { get; set; }
        public string May { get; set; }
        public string Jun { get; set; }
        public string Jul { get; set; }
        public string Aug { get; set; }
        public string Sep { get; set; }
        public string Oct { get; set; }
        public string Nov { get; set; }
        public string Dec { get; set; }
        public string Total { get; set; }
   //     public string Balance { get; set; }

    }

    public class DivisionLevel
    {
        public String id { get; set; }
        public String Division { get; set; }
        public List<ProgramLevel> Programs { get; set; }
        public String UACS { get; set; }
     //   public string Alloted { get; set; }
        public string Jan { get; set; }
        public string Feb { get; set; }
        public string Mar { get; set; }
        public string Apr { get; set; }
        public string May { get; set; }
        public string Jun { get; set; }
        public string Jul { get; set; }
        public string Aug { get; set; }
        public string Sep { get; set; }
        public string Oct { get; set; }
        public string Nov { get; set; }
        public string Dec { get; set; }
        public string Total { get; set; }
      //  public string Balance { get; set; }
   
    }
    public class ProgramLevel
    {
        public String id { get; set; }
        public String Program { get; set; }
        public List<ExpenditureTitles> Expenditures { get; set; }
        public String UACS { get; set; }
     //   public string Alloted { get; set; }
        public string Jan { get; set; }
        public string Feb { get; set; }
        public string Mar { get; set; }
        public string Apr { get; set; }
        public string May { get; set; }
        public string Jun { get; set; }
        public string Jul { get; set; }
        public string Aug { get; set; }
        public string Sep { get; set; }
        public string Oct { get; set; }
        public string Nov { get; set; }
        public string Dec { get; set; }
        public string Total { get; set; }
      //  public string Balance { get; set; }

    }
    public class ExpenditureTitles 
    {
        public String Code { get; set; }
        public String Expenditures { get; set; }
        public List<ExpenseItems> ExpenseList {get;set;}
        public String UACS { get; set; }
     //   public string Alloted { get; set; }
        public string Jan { get; set; }
        public string Feb { get; set; }
        public string Mar { get; set; }
        public string Apr { get; set; }
        public string May { get; set; }
        public string Jun { get; set; }
        public string Jul { get; set; }
        public string Aug { get; set; }
        public string Sep { get; set; }
        public string Oct { get; set; }
        public string Nov { get; set; }
        public string Dec { get; set; }
        public string Total { get; set; }
      //  public string Balance { get; set; }
    }
    public class ExpenseItemsList
    {
        public String fund_source_id { get; set; }
          public String  program_id { get; set; }
          public String program { get; set; }   
        public String month { get; set; }
            public String status { get; set; }
            public String  expen_code { get; set; }
            public String  mooe_id { get; set; }
            public String  division_id { get; set; }
            public String  division { get; set; }
            public String  expenditure_name { get; set; }
            public String  expense_name { get; set; }
            public String  uacs_code { get; set; }
            public String  total { get; set; }
    }
    public class ExpenseItems
    {
        public String id { get; set; }
        public String ExpenseType { get; set; }
        public String UACS { get; set; }
    //    public string Alloted { get; set; }
        public string Jan { get; set; }
        public string Feb { get; set; }
        public string Mar { get; set; }
        public string Apr { get; set; }
        public string May { get; set; }
        public string Jun { get; set; }
        public string Jul { get; set; }
        public string Aug { get; set; }
        public string Sep { get; set; }
        public string Oct { get; set; }
        public string Nov { get; set; }
        public string Dec { get; set; }
        public string Total { get; set; }
      //  public string Balance { get; set; }
    }

    public class BalancesDB
    {

        public String UACS { get; set; }
        public string PAP { get; set; }
        public string Jan { get; set; }
        public string Feb { get; set; }
        public string Mar { get; set; }
        public string Apr { get; set; }
        public string May { get; set; }
        public string Jun { get; set; }
        public string Jul { get; set; }
        public string Aug { get; set; }
        public string Sep { get; set; }
        public string Oct { get; set; }
        public string Nov { get; set; }
        public string Dec { get; set; }
        public string Years { get; set; }
    }
}
