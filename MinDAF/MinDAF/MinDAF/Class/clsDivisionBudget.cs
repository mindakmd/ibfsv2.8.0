﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsDivisionBudget
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();
       


        public String FetchExpenditureItems()
        {
            StringBuilder sb = new StringBuilder(87);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"sub.DBM_Sub_Pap_id as id,");
            sb.AppendLine(@"sub.DBM_Sub_Pap_Desc as name");
            sb.AppendLine(@"FROM DBM_Sub_Pap sub");


            return sb.ToString();
        }
      
        public String FetchGridData(String _id,String _year)
        {
            StringBuilder sb = new StringBuilder(214);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mbe.id as id,");
            sb.AppendLine(@"mbe.expenditure_id as ex_id,");
            sb.AppendLine(@"mbe.year,");
            sb.AppendLine(@"moo.DBM_Sub_Pap_Desc as name,");
            sb.AppendLine(@"mbe.allocated_budget");
            sb.AppendLine(@"FROM mnda_budget_expenditure mbe");
            sb.AppendLine(@"LEFT JOIN DBM_Sub_Pap moo on moo.DBM_Pap_Id = mbe.office_id");
            sb.AppendLine(@"WHERE mbe.year =" + _year + "");
        

            return sb.ToString();
        }  

        public String FetchGridDataDivision(String _id)
        {
            StringBuilder sb = new StringBuilder(101);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Division_Id,");
            sb.AppendLine(@"  DBM_Sub_Pap_Id,");
            sb.AppendLine(@"  Division_Code,");
            sb.AppendLine(@"  Division_Desc");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.Division WHERE DBM_Sub_Pap_Id = "+ _id +" ;");


            return sb.ToString();
        }
        public void RemoveData(List<String> _data)
        {
            String _sqlString = "";

            foreach (var item in _data)
            {
                StringBuilder sb = new StringBuilder(95);
                sb.AppendLine(@"DELETE FROM ");
                sb.AppendLine(@"  dbo.mnda_budget_expenditure  ");
                sb.AppendLine(@"WHERE ");
                sb.AppendLine(@"  id =" + item + ";");

                _sqlString += sb.ToString();
            }


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }
        public void UpdateBudget(String _id, String _allocated_budget, string _year)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(95);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_budget_expenditure  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  allocated_budget ="+ _allocated_budget +",");
            sb.AppendLine(@"  year = " + _year + "");
            sb.AppendLine(@"WHERE  id ="+ _id +";");

            _sqlString += sb.ToString();

            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void SaveBudgetAll(String _division_id, String _expenditure_id, String _allocated_budget, string _year,List<ExpenditureItems> _items)
        {
            String _sqlString = "";


            foreach (var item in _items)
            {

                StringBuilder sb = new StringBuilder(147);

                sb.AppendLine(@"INSERT INTO ");
                sb.AppendLine(@"  dbo.mnda_budget_expenditure");
                sb.AppendLine(@"(");
                sb.AppendLine(@"  division_id,");
                sb.AppendLine(@"  expenditure_id,");
                sb.AppendLine(@"  allocated_budget,");
                sb.AppendLine(@"  year");
                sb.AppendLine(@") ");
                sb.AppendLine(@"VALUES (");
                sb.AppendLine(@"  '" + _division_id + "',");
                sb.AppendLine(@"  '" + item.id + "',");
                sb.AppendLine(@"  " + _allocated_budget + ",");
                sb.AppendLine(@"  " + _year + "");
                sb.AppendLine(@");");


                _sqlString += sb.ToString();
            }

        




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void SaveBudget(String _office_id,String _expenditure_id,String _allocated_budget,string _year )
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(147);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_budget_expenditure");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  office_id,");
            sb.AppendLine(@"  expenditure_id,");
            sb.AppendLine(@"  allocated_budget,");
            sb.AppendLine(@"  year");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '" + _office_id + "',");
            sb.AppendLine(@"  '"+ _expenditure_id +"',");
            sb.AppendLine(@"  "+ _allocated_budget +",");
            sb.AppendLine(@"  "+ _year +"");
            sb.AppendLine(@");");


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }

        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (Process)
            {
                case "SaveData":
                    if (SQLOperation!=null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateData":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "RemoveData":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public class ExpenditureItems 
    {
        public String id { get; set; }
        public String name {get;set;}
    }
    public class DivisionAllocated
    {
            public string id { get; set; }
            public string ex_id {get;set;}
            public string year{get;set;}
            public string name{get;set;}
            public string allocated_budget{get;set;}
    }  
    public class DivisionAllocatedLocation
    {
        public String  Division_Id {get;set;}
        public String  DBM_Sub_Pap_Id {get;set;}
        public String  Division_Code {get;set;}
        public String  Division_Desc { get; set; }
    }
}
