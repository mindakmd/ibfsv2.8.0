﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsBudgetSubscription
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();

        public String FetchFrequency()
        {
            StringBuilder sb = new StringBuilder(237);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mel.item_name");
            sb.AppendLine(@"FROM mnda_mooe_sub_expenditures mmo_sub");
            sb.AppendLine(@"INNER JOIN mnda_expenditures_library mel on mel.sub_expenditure_code = mmo_sub.id");
            sb.AppendLine(@"WHERE mmo_sub.id = 48 and mmo_sub.is_active = 1");


            return sb.ToString();
        }
        public void DeleteProjectDues(String _activity_id, String _accountable_id, String _remarks, String _quantity, double _rate, double _total, String _month, String _year, string mooe_id, string no_days,
       string _type)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(285);
            sb.AppendLine(@"DELETE FROM mnda_activity_data WHERE year =" + _year + " AND activity_id ='" + _activity_id + "' AND mooe_sub_expenditure_id ='48';");
           

            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }

        public void UpdateSuspend(List<String> _id, String _val)
        {


            String _sqlString = "";

            foreach (String item in _id)
            {
                StringBuilder sb = new StringBuilder(79);
                sb.AppendLine(@"UPDATE ");
                sb.AppendLine(@"  dbo.mnda_activity_data  ");
                sb.AppendLine(@"SET ");
                sb.AppendLine(@"  is_suspended =" + _val + "");
                sb.AppendLine(@"WHERE id = " + item + ";");


                _sqlString += sb.ToString();
            }
            


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;

        }

        public void SaveProjectDues(String _activity_id, String _accountable_id, String _remarks, String _quantity, double _rate, double _total, String _month, String _year, string mooe_id, string no_days,
         string _type,String fund_source_id )
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(285);
            sb.AppendLine(@" INSERT INTO ");
            sb.AppendLine(@"  mnda_activity_data");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  activity_id,");
            sb.AppendLine(@"  accountable_id,");
            sb.AppendLine(@"  remarks,");
            sb.AppendLine(@"  start,");
            sb.AppendLine(@"  [end],");
            sb.AppendLine(@"  destination,");
            sb.AppendLine(@"  quantity,");
            sb.AppendLine(@"  rate,");
            sb.AppendLine(@"  travel_allowance,");
            sb.AppendLine(@"  total,");
            sb.AppendLine(@"  month,");
            sb.AppendLine(@"  year,mooe_sub_expenditure_id,no_days,type_service,fund_source_id");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '" + _activity_id + "',");
            sb.AppendLine(@"  '" + _accountable_id + "',");
            sb.AppendLine(@" '" + _remarks + "',");
            sb.AppendLine(@" NULL,");
            sb.AppendLine(@" NULL,");
            sb.AppendLine(@"  '',");
            sb.AppendLine(@" 0,");
            sb.AppendLine(@" " + _rate + ",");
            sb.AppendLine(@"  0,");
            sb.AppendLine(@"  " + _rate + ",");
            sb.AppendLine(@" '" + _month + "',");
            sb.AppendLine(@" " + _year + ",");
            sb.AppendLine(@" '" + mooe_id + "',");
            sb.AppendLine(@" 0,");
            sb.AppendLine(@" '" + _type + "',");
            sb.AppendLine(@" '" + fund_source_id + "'");
            sb.AppendLine(@");");


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public String FetchLocalData(string _activity_id, string year,string fund_source_id)
        {
            StringBuilder sb = new StringBuilder(125);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mad.id as ActId,");
            sb.AppendLine(@"mad.month,");
            sb.AppendLine(@"mad.rate");
            sb.AppendLine(@"FROM mnda_activity_data mad");
            sb.AppendLine(@"WHERE mad.fund_source_id = "+ fund_source_id +" AND  mad.year = " + year + " and mad.activity_id = " + _activity_id + " and mad.mooe_sub_expenditure_id ='48' and is_suspended =0");
            sb.AppendLine(@"ORDER BY mad.id ASC");


            return sb.ToString();
        }
        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "SaveData":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "Suspend":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
            }
        }
    }
    public class SubsData 
    {
        public String ActId { get; set; }
        public String month { get; set; }
        public String rate { get; set; }
    }

    
}
