﻿using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsActivity
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();
        MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
       

        public String FetchEmployees(String _division_id) 
        {
            StringBuilder sb = new StringBuilder(93);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  User_Id as Id,");
            sb.AppendLine(@"  User_Fullname as Fullname");
            sb.AppendLine(@"FROM mnda_user_accounts mua");
            sb.AppendLine(@"WHERE mua.Division_Id ='"+ _division_id +"';");

            return sb.ToString();
        }
        public String FetchCategory()
        {
            StringBuilder sb = new StringBuilder(45);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  name,weight");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"mnda_category;");


            return sb.ToString();
        
            
        }

        public String FetchActivity(String _outputId)
        {
            StringBuilder sb = new StringBuilder(432);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  ma.id,");
            sb.AppendLine(@"   ma.output_id,");
            sb.AppendLine(@"   ma.description,");
            sb.AppendLine(@"   mua.User_Fullname as accountable_member,");
            sb.AppendLine(@"   mc.name as member_category,");
            sb.AppendLine(@"   ma.weight,");
            sb.AppendLine(@"  ma.date_start,");
            sb.AppendLine(@"  ma.date_end,");
            sb.AppendLine(@"  ma.completion_rate,");
            sb.AppendLine(@"  ma.support_needed,");
            sb.AppendLine(@"  ma.is_approved");
            sb.AppendLine(@"FROM mnda_activity ma");
            sb.AppendLine(@"INNER JOIN mnda_user_accounts mua on mua.User_Id = ma.accountable_member_id");
            sb.AppendLine(@"INNER JOIN mnda_category mc on mc.id = ma.member_category_id");
            sb.AppendLine(@"WHERE ma.output_id ='"+ _outputId +"';");



            return sb.ToString();


        }

        public void SaveOutputActivity(String output_id,
                                        String description,
                                        String accountable_member_id,
                                        String member_category_id,
                                        String weight,
                                        DateTime date_start,
                                        DateTime date_end,
                                        String completion_rate,
                                        String support_needed )
        {
            String _sqlString = "";
           

                StringBuilder sb = new StringBuilder(289);
                sb.AppendLine(@"INSERT INTO ");
                sb.AppendLine(@"  mnda_activity");
                sb.AppendLine(@"(");
                sb.AppendLine(@"  output_id,");
                sb.AppendLine(@"  description,");
                sb.AppendLine(@"  accountable_member_id,");
                sb.AppendLine(@"  member_category_id,");
                sb.AppendLine(@"  weight,");
                sb.AppendLine(@"  date_start,");
                sb.AppendLine(@"  date_end,");
                sb.AppendLine(@"  completion_rate,");
                sb.AppendLine(@"  support_needed");
                sb.AppendLine(@") ");
                sb.AppendLine(@"VALUES (");
                sb.AppendLine(@"  '"+ output_id +"',");
                sb.AppendLine(@"  '"+ description +"',");
                sb.AppendLine(@"  '"+ accountable_member_id +"',");
                sb.AppendLine(@"  '"+ member_category_id +"',");
                sb.AppendLine(@"  '"+ weight +"',");
                sb.AppendLine(@"  '"+ date_start +"',");
                sb.AppendLine(@"  '"+ date_end +"',");
                sb.AppendLine(@"  '"+ completion_rate +"',");
                sb.AppendLine(@"  '"+ support_needed +"'");
                sb.AppendLine(@"  ");
                sb.AppendLine(@");");


                _sqlString += sb.ToString();
  


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }

        void c_ops_DataReturn(object sender, EventArgs e)
        {
            ReturnCode = c_ops.ReturnCode;
            switch (Process)
            {
                case "SaveActivity":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
            }
        }

    }
    public class ActivityDetails 
    {
        public String id { get; set; }
        public String output_id { get; set; }
        public String description { get; set; }
        public String accountable_member { get; set; }
        public String member_category { get; set; }
        public Decimal weight { get; set; }
        public DateTime date_start { get; set; }
        public DateTime date_end { get; set; }
        public int completion_rate { get; set; }
        public String support_needed { get; set; }
        public Boolean is_approved { get; set; }
    }
    public class ActivityUserData 
    {
        public String Id { get; set; }
        public String Fullname { get; set; }
    }
}
