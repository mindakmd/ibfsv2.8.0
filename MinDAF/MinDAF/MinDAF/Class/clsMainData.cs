﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsMainData
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();

        public void AddNewRow(String Division_ID)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(256);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_main_program_encoded");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  fiscal_year,");
            sb.AppendLine(@"  major_final_output_code,");
            sb.AppendLine(@"  performance_indicator_code,");
            sb.AppendLine(@"  program_code,");
            sb.AppendLine(@"  category_code,");
            sb.AppendLine(@"  accountable_office_code,accountable_division");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  NULL,");
            sb.AppendLine(@"  NULL,");
            sb.AppendLine(@"  NULL,");
            sb.AppendLine(@"  NULL,");
            sb.AppendLine(@"  NULL,");
            sb.AppendLine(@"  NULL,'"+ Division_ID +"'");
            sb.AppendLine(@");");



            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void UpdateProgramID(String _program_id, String _id)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(81);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_main_program_encoded  ");
            sb.AppendLine(@"SET   ");
            sb.AppendLine(@"  program_code ='" + _program_id + "' ");
            sb.AppendLine(@"WHERE id =" + _id + ";");

            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }
        public void UpdateProjectID(String _project_id, String _id)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(81);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_main_program_encoded  ");
            sb.AppendLine(@"SET   ");
            sb.AppendLine(@"  program_code ='" + _project_id + "' ");
            sb.AppendLine(@"WHERE id =" + _id + ";");

            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }

        public void UpdateProgram(String _id, String _program) 
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(63);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  mnda_programs");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  name");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@" '"+ _program +"'");
            sb.AppendLine(@");");


            _sqlString = sb.ToString();


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }
        public void UpdateProjectOutput(String _output_id, String _program_id, String _project_output,String _assigned)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(98);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_project_output");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  program_code,");
            sb.AppendLine(@"  name,assigned_user");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '"+ _program_id +"',");
            sb.AppendLine(@"  '" + _project_output + "','" + _assigned + "'");
            sb.AppendLine(@");");



            _sqlString = sb.ToString();


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }
        public void UpdateDeleteProjectOutput(String _output_id, String _program_id, String _project_output,string Assigned_ID ="Null")
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(98);
            sb.AppendLine(@"DELETE FROM mnda_project_output WHERE id ="+ _output_id +"; INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_project_output");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  program_code,");
            sb.AppendLine(@"  name,assigned_user");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '" + _program_id + "',");
            sb.AppendLine(@"  '" + _project_output + "','"+ Assigned_ID +"'");
            sb.AppendLine(@");");



            _sqlString = sb.ToString();


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }
        public void UpdateAddOutputActivity(String output_id, String _activity,String _accountable_id="0")
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(286);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_activity");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  output_id,");
            sb.AppendLine(@"  description,");
            sb.AppendLine(@"  accountable_member_id");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '" + output_id + "',");
            sb.AppendLine(@"  '" + _activity + "',");
            sb.AppendLine(@"  '" + _accountable_id + "'");
            sb.AppendLine(@");");



            _sqlString = sb.ToString();


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }
        public void UpdateOutputActivity(String _activity_id,String output_id, String _activity,String _accountable_id ="-")
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(286);
            sb.AppendLine(@"DELETE FROM mnda_activity WHERE id = "+ _activity_id + ";  INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_activity");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  output_id,");
            sb.AppendLine(@"  description,");
            sb.AppendLine(@"  accountable_member_id");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '"+ output_id +"',");
            sb.AppendLine(@"  '"  + _activity  + "',");
            sb.AppendLine(@"  '" + _accountable_id + "'");
            sb.AppendLine(@");");



            _sqlString = sb.ToString();


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }
        public void UpdateProject(String _program_id, String _project,String _year,String _division)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(284);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_program_encoded");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  main_program_id,");
            sb.AppendLine(@"  key_result_code,");
            sb.AppendLine(@"  mindanao_2020_code,");
            sb.AppendLine(@"  scope_code,");
            sb.AppendLine(@"  fund_source_code,");
            sb.AppendLine(@"  acountable_division_code,");
            sb.AppendLine(@"  project_name,");
            sb.AppendLine(@"  project_year");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '" + _program_id + "',");
            sb.AppendLine(@" Null,");
            sb.AppendLine(@"  Null,");
            sb.AppendLine(@"  Null,");
            sb.AppendLine(@"  Null,");
            sb.AppendLine(@"  '"+ _division +"',");
            sb.AppendLine(@"  '"+ _project +"',");
            sb.AppendLine(@"  "+ _year +"");
            sb.AppendLine(@");");


            _sqlString = sb.ToString();


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }
        public void UpdateAssignedEmployee(String _user_id, String _activity_id,String _output_id)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(81);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_activity");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  accountable_member_id = '"+ _user_id +"'");
            sb.AppendLine(@"WHERE id ="+ _activity_id +";");
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_project_output");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  assigned_user = '" + _user_id + "'");
            sb.AppendLine(@"WHERE id =" + _output_id + ";");

            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }
        public void UpdateYear(String _year , String _id) 
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(81);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_main_program_encoded  ");
            sb.AppendLine(@"SET   ");
            sb.AppendLine(@"  fiscal_year ="+ _year +" ");
            sb.AppendLine(@"WHERE id ="+ _id +";");

            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }

        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "AddNewRow":
                    if (this.SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateRowYear":
                    if (this.SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateRowProgram":
                    if (this.SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateMainProgramID":
                    if (this.SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateRowProject":
                    if (this.SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateProjectOutput":
                    if (this.SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateOutputActivity":
                    if (this.SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateAssignedEmployee":
                    if (this.SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                default:
                    break;
            }
        }


        public String FetchOverAllYear() 
        {
            StringBuilder sb = new StringBuilder(86);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mnd.fiscal_year as Fiscal_Year");
            sb.AppendLine(@"FROM mnda_main_program_encoded mnd");
            sb.AppendLine(@"GROUP BY mnd.fiscal_year");

            return sb.ToString();
        }

        public String FetchProgramId(String _program)
        {
            StringBuilder sb = new StringBuilder(57);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id");
            sb.AppendLine(@"FROM  mnda_programs mp");
            sb.AppendLine(@"WHERE mp.name ='" + _program + "';");


            return sb.ToString();
        }

        public String FetchActivityStatus(string _year, string _month,String _user,String _act_id)
        {
            StringBuilder sb = new StringBuilder(475);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mad.activity_id,");
            sb.AppendLine(@"mad.accountable_id,");
            sb.AppendLine(@"mad.remarks,");
            sb.AppendLine(@"mooe.name,");
            sb.AppendLine(@"mad.type_service,");
            sb.AppendLine(@"mad.destination,");
            sb.AppendLine(@"mad.no_days,");
            sb.AppendLine(@"mad.no_staff,");
            sb.AppendLine(@"mad.quantity,");
            sb.AppendLine(@"mad.total");
            sb.AppendLine(@"FROM mnda_activity_data mad ");
            sb.AppendLine(@"LEFT JOIN mnda_activity mda on mda.id = mad.activity_id");
           // sb.AppendLine(@"LEFT JOIN mnda_project_output mpo on mpo.id = mda.output_id");
           // sb.AppendLine(@"left JOIN mnda_main_program_encoded mpe on mpe.program_code = mpo.program_code");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"WHERE mad.year = " + _year + " and mad.month = '" + _month + "' AND  mad.accountable_id ='" + _user + "' AND mad.activity_id = '"+ _act_id+"' and mad.is_suspended = 0");


            return sb.ToString();
        }

        public String FetchProjectId(String _project)
        {
            StringBuilder sb = new StringBuilder(57);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id");
            sb.AppendLine(@"FROM  mnda_programs mp");
            sb.AppendLine(@"WHERE mp.name ='" + _project + "';");


            return sb.ToString();
        }

        public String FetchYearPrograms() 
                {
                    StringBuilder sb = new StringBuilder(167);
                    sb.AppendLine(@"SELECT");
                    sb.AppendLine(@"mnd.fiscal_year,");
                    sb.AppendLine(@"mnd.id, ");
                    sb.AppendLine(@"mp.name");
                    sb.AppendLine(@"FROM mnda_main_program_encoded mnd");
                    sb.AppendLine(@"INNER JOIN mnda_programs mp  ON mp.id = mnd.program_code");
                    sb.AppendLine(@"ORDER BY mnd.fiscal_year ASC");

                    return sb.ToString();
                }
      public String FetchYearProjects() 
                    {
                        StringBuilder sb = new StringBuilder(308);
                        sb.AppendLine(@"SELECT");
                        sb.AppendLine(@"mnd.id,");
                        sb.AppendLine(@"mpe.project_year,");
                        sb.AppendLine(@"mpe.project_name,");
                        sb.AppendLine(@"ISNULL(div.Division_Desc,'N/A') as Division");
                        sb.AppendLine(@"FROM mnda_main_program_encoded mnd");
                        sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.main_program_id = mnd.id");
                        sb.AppendLine(@"LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
                        sb.AppendLine(@"ORDER BY mpe.project_year ASC");

                        return sb.ToString();
                    } 
        public String FetchYearActivity() 
                    {
                        StringBuilder sb = new StringBuilder(627);
                        sb.AppendLine(@"SELECT");
                        sb.AppendLine(@"ma.id as activity_id,");
                        sb.AppendLine(@"mnd.id as program_id,");
                        sb.AppendLine(@"mpe.id as project_id,");
                        sb.AppendLine(@"mpe.project_year,");
                        sb.AppendLine(@"mpe.project_name,");
                        sb.AppendLine(@"ISNULL(div.Division_Desc,'N/A') as Division,");
                        sb.AppendLine(@"ma.description as activity,");
                        sb.AppendLine(@"mua.User_Fullname as assigned");
                        sb.AppendLine(@"FROM mnda_main_program_encoded mnd");
                        sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.main_program_id = mnd.id");
                        sb.AppendLine(@"INNER JOIN Division div on div.Division_Id = mpe.acountable_division_code");
                        sb.AppendLine(@"INNER JOiN mnda_project_output  mpo on mpo.program_code = mpe.id");
                        sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.output_id = mpo.id");
                        sb.AppendLine(@"INNER JOIN mnda_user_accounts mua on mua.User_Id = ma.accountable_member_id");
                        sb.AppendLine(@"ORDER BY mpe.project_year ASC");


                        return sb.ToString();
                    }

        public String FetchData(String Division_Id) 
        {
            StringBuilder sb = new StringBuilder(1608);
            sb.AppendLine(@"   SELECT ");
            sb.AppendLine(@"                        ISNULL(mnd.id,'') as program_id,ISNULL(mpe.id,'') as project_id,ISNULL(mpo.id,'') as output_id,");
            sb.AppendLine(@"                        ISNULL(mnd.fiscal_year,'') as fiscal_year,");
            sb.AppendLine(@"                        ISNULL(mp.name,'') as program_name,");
            sb.AppendLine(@"                        ISNULL(mpo.name,'') as output,");
            sb.AppendLine(@"                        ISNULL(mpe.project_name,'') as project_name,");
            sb.AppendLine(@"                        ISNULL(ma.description,'') as activity,");
            sb.AppendLine(@"                        ISNULL(ma.id,'') as activity_id,");
            sb.AppendLine(@"                        ISNULL(mua.User_Fullname,'') as User_Fullname,");
            sb.AppendLine(@"                        0.00 as Jan,");
            sb.AppendLine(@"                        0.00 as Feb,");
            sb.AppendLine(@"                        0.00 as Mar,");
            sb.AppendLine(@"                        0.00 as Apr,");
            sb.AppendLine(@"                        0.00 as May,");
            sb.AppendLine(@"                        0.00 as Jun,");
            sb.AppendLine(@"                        0.00 as Jul,");
            sb.AppendLine(@"                        0.00 as Aug,");
            sb.AppendLine(@"                        0.00 as Sep,");
            sb.AppendLine(@"                        0.00 as Oct,");
            sb.AppendLine(@"                        0.00 as Nov,");
            sb.AppendLine(@"                        0.00 as Dec                                           ");
            sb.AppendLine(@"                        FROM mnda_main_program_encoded mnd");
            sb.AppendLine(@"                        LEFT JOIN mnda_programs mp on mp.id = mnd.program_code");
            sb.AppendLine(@"                        LEFT JOIN mnda_program_encoded mpe on mpe.main_program_id = mnd.id");
            sb.AppendLine(@"                        LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@"                        LEFT JOiN mnda_project_output  mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@"                        LEFT JOIN mnda_activity ma on ma.output_id = mpo.id");
            sb.AppendLine(@"                        LEFT JOIN mnda_activity_data mad on mad.activity_id = ma.id");
            sb.AppendLine(@"                        LEFT JOIN mnda_user_accounts mua on mua.User_Id = ma.accountable_member_id");
            sb.AppendLine(@"                        WHERE mnd.accountable_division = "+ Division_Id +" AND mad.is_suspended = 0");
            sb.AppendLine(@"                        GROUP BY mnd.id,mpe.id,mpo.id, mnd.fiscal_year,mp.name,mpo.name,mpe.project_name,ma.description,ma.id,mua.User_Fullname"); 
            sb.AppendLine(@"                        ORDER BY  mnd.fiscal_year ASC");


            return sb.ToString();
        }
        public String FetchDataPersonal(String Division_Id,String Accountable_Id,String AssignedName)
        {
            StringBuilder sb = new StringBuilder(1608);
            sb.AppendLine(@"   SELECT ");
            sb.AppendLine(@"                        ISNULL(mnd.id,'') as program_id,ISNULL(mpe.id,'') as project_id,ISNULL(mpo.id,'') as output_id,");
            sb.AppendLine(@"                        ISNULL(mnd.fiscal_year,'') as fiscal_year,");
            sb.AppendLine(@"                        ISNULL(mp.name,'') as program_name,");
            sb.AppendLine(@"                        ISNULL(mpo.name,'') as output,");
            sb.AppendLine(@"                        ISNULL(mpe.project_name,'') as project_name,");
            sb.AppendLine(@"                        ISNULL(ma.description,'') as activity,");
            sb.AppendLine(@"                        ISNULL(ma.id,'') as activity_id,");
            sb.AppendLine(@"                        ISNULL(mua.User_Fullname,'') as User_Fullname,");
            sb.AppendLine(@"                        0.00 as Jan,");
            sb.AppendLine(@"                        0.00 as Feb,");
            sb.AppendLine(@"                        0.00 as Mar,");
            sb.AppendLine(@"                        0.00 as Apr,");
            sb.AppendLine(@"                        0.00 as May,");
            sb.AppendLine(@"                        0.00 as Jun,");
            sb.AppendLine(@"                        0.00 as Jul,");
            sb.AppendLine(@"                        0.00 as Aug,");
            sb.AppendLine(@"                        0.00 as Sep,");
            sb.AppendLine(@"                        0.00 as Oct,");
            sb.AppendLine(@"                        0.00 as Nov,");
            sb.AppendLine(@"                        0.00 as Dec                                           ");
            sb.AppendLine(@"                        FROM mnda_main_program_encoded mnd");
            sb.AppendLine(@"                        LEFT JOIN mnda_programs mp on mp.id = mnd.program_code");
            sb.AppendLine(@"                        LEFT JOIN mnda_program_encoded mpe on mpe.main_program_id = mnd.id");
            sb.AppendLine(@"                        LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@"                        LEFT JOiN mnda_project_output  mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@"                        LEFT JOIN mnda_activity ma on ma.output_id = mpo.id");
            sb.AppendLine(@"                        LEFT JOIN mnda_activity_data mad on mad.activity_id = ma.id");
            sb.AppendLine(@"                        LEFT JOIN mnda_user_accounts mua on mua.User_Id = ma.accountable_member_id");
            sb.AppendLine(@"                        WHERE mnd.accountable_division =" + Division_Id + " ");
            sb.AppendLine(@"                        GROUP BY mnd.id,mpe.id,mpo.id, mnd.fiscal_year,mp.name,mpo.name,mpe.project_name,ma.description,ma.id,mua.User_Fullname");
            sb.AppendLine(@"                        ORDER BY  ma.id,mnd.fiscal_year ASC");


            //sb.AppendLine(@"                        WHERE mnd.accountable_division =" + Division_Id + " AND mpo.assigned_user =" + Accountable_Id + " ");
            //sb.AppendLine(@"                        and mua.User_Fullname = '" + AssignedName.Replace("Welcome ", "").Trim() + "' ");
            //sb.AppendLine(@"                        ORDER BY  mnd.fiscal_year ASC");


            return sb.ToString();
        }
        public String FetchTotals() 
        {
            StringBuilder sb = new StringBuilder(302);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mdad.activity_id ,");
            sb.AppendLine(@"mdad.month ,");
            sb.AppendLine(@"mdad.total");
            sb.AppendLine(@"FROM mnda_activity_data mdad");
            sb.AppendLine(@"INNER JOIN mnda_activity mda on mda.id = mdad.activity_id");
            sb.AppendLine(@"WHERE mdad.is_suspended = 0");
            //sb.AppendLine(@"UNION");
            //sb.AppendLine(@"SELECT");
            //sb.AppendLine(@"madp.activity_id,");
            //sb.AppendLine(@"madp.month,");
            //sb.AppendLine(@"madp.total");
            //sb.AppendLine(@"FROM mnda_activity_data_procurement madp");
            //sb.AppendLine(@"INNER JOIN mnda_activity mda on mda.id = madp.activity_id");


            return sb.ToString();
        }
    }

    public class MainDashboardActivityData 
    {
             public String activity_id { get; set; }
             public String accountable_id { get; set; }
             public String remarks { get; set; }
             public String name { get; set; }
             public String type_service { get; set; }
             public String destination { get; set; }
             public String no_days { get; set; }
             public String no_staff { get; set; }
             public String quantity { get; set; }
             public String total { get; set; }
    }
    public class FetchProjectId
    {
        public String id { get; set; }
    }
    public class FetchProgramId 
    {
        public String id { get; set; }
    }
    public class MainTotals
    {
        public String activity_id {get;set;}
        public String month {get;set;}
        public String total {get;set;}
    }

    public class MainData 
    {
                 public String div_id { get; set; }
                 public String project_id { get; set; }
                 public String output_id { get; set; }
                 public Int32 		program_id {get;set;}
                 public String      fiscal_year {get;set;}
                 public String      program_name {get;set;}
                 public String      output {get;set;}
                 public String      project_name {get;set;}
                 public String      activity {get;set;}
                 public String      activity_id { get; set; }
                 public String      User_Fullname {get;set;}
                 public String     Jan {get;set;}
                 public String Feb { get; set; }
                 public String Mar { get; set; }
                 public String Apr { get; set; }
                 public String May { get; set; }
                 public String Jun { get; set; }
                 public String Jul { get; set; }
                 public String Aug { get; set; }
                 public String Sep { get; set; }
                 public String Oct { get; set; }
                 public String Nov { get; set; }
                 public String Dec { get; set; }
    }
    public class MainDataFinance
    {
        public String div_id { get; set; }
        public String project_id { get; set; }
        public String output_id { get; set; }
        public Int32 program_id { get; set; }
        public String Division { get; set; }
        public String fiscal_year { get; set; }
        public String program_name { get; set; }
        public String output { get; set; }
        public String project_name { get; set; }
        public String activity { get; set; }
        public String activity_id { get; set; }
        public String User_Fullname { get; set; }
        public String Jan { get; set; }
        public String Feb { get; set; }
        public String Mar { get; set; }
        public String Apr { get; set; }
        public String May { get; set; }
        public String Jun { get; set; }
        public String Jul { get; set; }
        public String Aug { get; set; }
        public String Sep { get; set; }
        public String Oct { get; set; }
        public String Nov { get; set; }
        public String Dec { get; set; }
    }

    public class Main_OverAllYear 
    {
        public String Fiscal_Year { get; set; }
    }
    public class Main_YearPrograms 
    {
        public String fiscal_year { get; set; }
        public String id { get; set; }
        public String name { get; set; }
    }
    public class Main_YearProjects 
    {
        public String id {get;set;}
        public String project_year {get;set;}
        public String project_name {get;set;}
        public String Division { get; set; }
    }
    public class Main_YearActivities 
    {
        public String activity_id {get;set;}
        public String program_id {get;set;}
        public String project_id {get;set;}
        public String project_year {get;set;}
        public String project_name {get;set;}
        public String Division {get;set;}
        public String activity {get;set;}
        public String assigned {get;set;}
    }
}
