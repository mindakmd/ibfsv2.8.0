﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsBudgetReportMBA
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();

        public void SaveReportPrintOutMAO(List<ReportMBA> _data, String Division, String FundSource, String Pap)
        {


            c_ops.InstantiateService();
            String _sqlString = "";
            StringBuilder sb = new StringBuilder(1000);
            sb.AppendLine(@"DELETE FROM dbo.mnda_report_data_mao_urs;");
            foreach (var item in _data)
            {
                _sqlString = "";
                sb.AppendLine(@" INSERT INTO ");
                sb.AppendLine(@"  dbo.mnda_report_data_mao_urs ");
                sb.AppendLine(@"( ");
                sb.AppendLine(@"  ReportHeader, ");
                sb.AppendLine(@"  UACS, ");
                sb.AppendLine(@"  Allocation, ");
                sb.AppendLine(@"  Jan, ");
                sb.AppendLine(@"  Feb, ");
                sb.AppendLine(@"  Mar, ");
                sb.AppendLine(@"  Apr, ");
                sb.AppendLine(@"  May, ");
                sb.AppendLine(@"  Jun, ");
                sb.AppendLine(@"  Jul, ");
                sb.AppendLine(@"  Aug, ");
                sb.AppendLine(@"  Sep, ");
                sb.AppendLine(@"  Octs, ");
                sb.AppendLine(@"  Nov, ");
                sb.AppendLine(@"  Dec, ");
                sb.AppendLine(@"  Total, ");
                sb.AppendLine(@"  Balance, ");
                sb.AppendLine(@"  Divisions, ");
                sb.AppendLine(@"  FundSource, ");
                sb.AppendLine(@"  PAP ");
                sb.AppendLine(@")  ");
                sb.AppendLine(@"VALUES ( ");
                sb.AppendLine(@"  '" + item.ReportHeader + "', ");
                sb.AppendLine(@"  '" + item.UACS + "', ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  0, ");
                sb.AppendLine(@"  '" + Division + "', ");
                sb.AppendLine(@"  '" + FundSource + "', ");
                sb.AppendLine(@"  '" + Pap + "' ");
                sb.AppendLine(@"  ) ");

                Int32 count = 0;
                foreach (var itemdetail in item.ReportDetail)
                {
                    if (count == 3)
                    {
                        break;
                    }
                    sb.AppendLine(@" INSERT INTO ");
                    sb.AppendLine(@"  dbo.mnda_report_data_mao_urs ");
                    sb.AppendLine(@"( ");
                    sb.AppendLine(@"  ReportHeader, ");
                    sb.AppendLine(@"  UACS, ");
                    sb.AppendLine(@"  Allocation, ");
                    sb.AppendLine(@"  Jan, ");
                    sb.AppendLine(@"  Feb, ");
                    sb.AppendLine(@"  Mar, ");
                    sb.AppendLine(@"  Apr, ");
                    sb.AppendLine(@"  May, ");
                    sb.AppendLine(@"  Jun, ");
                    sb.AppendLine(@"  Jul, ");
                    sb.AppendLine(@"  Aug, ");
                    sb.AppendLine(@"  Sep, ");
                    sb.AppendLine(@"  Octs, ");
                    sb.AppendLine(@"  Nov, ");
                    sb.AppendLine(@"  Dec, ");
                    sb.AppendLine(@"  Total, ");
                    sb.AppendLine(@"  Balance, ");
                    sb.AppendLine(@"  Divisions, ");
                    sb.AppendLine(@"  FundSource, ");
                    sb.AppendLine(@"  PAP ");
                    sb.AppendLine(@")  ");
                    sb.AppendLine(@"VALUES ( ");
                    sb.AppendLine(@"  '          " + itemdetail.Name + "', ");
                    sb.AppendLine(@"  '" + itemdetail.UACS + "', ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Alloted) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Jan) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Feb) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Mar) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Apr) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.May) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Jun) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Jul) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Aug) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Sep) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Oct) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Nov) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Dec) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Total) + ", ");
                    sb.AppendLine(@"  " + Convert.ToDouble(itemdetail.Balance) + ", ");
                    sb.AppendLine(@"  '" + Division + "', ");
                    sb.AppendLine(@"  '" + FundSource + "', ");
                    sb.AppendLine(@"  '" + Pap + "' ");
                    sb.AppendLine(@"  ); ");
                    _sqlString += sb.ToString();
                    count += 1;
                }

            }




            c_ops.ExecuteSQL(_sqlString);
       

        }
        public String FetchExpenditureTemplate()
        {
            var sb = new System.Text.StringBuilder(318);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  code as id,");
            sb.AppendLine(@"  name as Name,");
            sb.AppendLine(@"  code as UACS,");
            sb.AppendLine(@"  0.00 as Alloted,");
            sb.AppendLine(@"  0.00 as Jan,");
            sb.AppendLine(@"  0.00 as Feb,");
            sb.AppendLine(@"  0.00 as Mar,");
            sb.AppendLine(@"  0.00 as Apr,");
            sb.AppendLine(@"  0.00 as May,");
            sb.AppendLine(@"  0.00 as Jun,");
            sb.AppendLine(@"  0.00 as Jul,");
            sb.AppendLine(@"  0.00 as Aug,");
            sb.AppendLine(@"  0.00 as Sep,");
            sb.AppendLine(@"  0.00 as Oct,");
            sb.AppendLine(@"  0.00 as Nov,");
            sb.AppendLine(@"  0.00 as Dec,");
            sb.AppendLine(@"  0.00 as Total,");
            sb.AppendLine(@"  0.00 as Balance");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_mooe_expenditures;");


            return sb.ToString();
        }
        public String FetchExpenditureTemplateSumm()
        {
            var sb = new System.Text.StringBuilder(318);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  code as id,");
            sb.AppendLine(@"  name as Name,");
            sb.AppendLine(@"  code as UACS,");
            sb.AppendLine(@"  0.00 as Alloted,");
            sb.AppendLine(@"  0.00 as Jan,");
            sb.AppendLine(@"  0.00 as Feb,");
            sb.AppendLine(@"  0.00 as Mar,");
            sb.AppendLine(@"  0.00 as Quarter_1,");
            sb.AppendLine(@"  0.00 as Apr,");
            sb.AppendLine(@"  0.00 as May,");
            sb.AppendLine(@"  0.00 as Jun,");
            sb.AppendLine(@"  0.00 as Quarter_2,");
            sb.AppendLine(@"  0.00 as Jul,");
            sb.AppendLine(@"  0.00 as Aug,");
            sb.AppendLine(@"  0.00 as Sep,");
            sb.AppendLine(@"  0.00 as Quarter_3,");
            sb.AppendLine(@"  0.00 as Oct,");
            sb.AppendLine(@"  0.00 as Nov,");
            sb.AppendLine(@"  0.00 as Dec,");
            sb.AppendLine(@"  0.00 as Quarter_4,");
            sb.AppendLine(@"  0.00 as Total,");
            sb.AppendLine(@"  0.00 as Balance");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_mooe_expenditures;");


            return sb.ToString();
        } 
        public String FetchAllocationExpenditure(String _div_id ,String _year,String _fundid)
        {
            //StringBuilder sb = new StringBuilder(254);
            //sb.AppendLine(@"SELECT");
            //sb.AppendLine(@"sub.name,");
            //sb.AppendLine(@"mbe.allocated_budget");
            //sb.AppendLine(@"FROM Division div");
            //sb.AppendLine(@"LEFT JOIN mnda_budget_expenditure mbe on mbe.division_id = div.Division_Id");
            //sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures sub on sub.id = mbe.expenditure_id");
            //sb.AppendLine(@"WHERE div.Division_Id = "+ _div_id +" AND mbe.year = "+ _year +"");
            StringBuilder sb = new StringBuilder(503);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"sub.name,");
            sb.AppendLine(@"SUM(mad.total) as allocated_budget");
            sb.AppendLine(@"FROM mnda_activity_data mad");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures sub on sub.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_activity ma on ma.id =mad.activity_id");
            sb.AppendLine(@"LEFT JOIN mnda_project_output po on po.id = ma.output_id");
            sb.AppendLine(@"LEFT JOIN mnda_program_encoded mpe on mpe.id = po.program_code");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source mfs on mfs.Fund_Source_Id = mad.fund_source_id");
            sb.AppendLine(@"WHERE mad.year = " + _year + " and mpe.acountable_division_code= " + _div_id + "");
            sb.AppendLine(@"AND mfs.Fund_Name ='"+ _fundid +"'");
            sb.AppendLine(@"GROUP BY sub.name");



            return sb.ToString();
        }
        public String FetchAllocationExpenditureSummary(String _div_id, String _year)
        {
            //StringBuilder sb = new StringBuilder(254);
            //sb.AppendLine(@"SELECT");
            //sb.AppendLine(@"sub.name,");
            //sb.AppendLine(@"mbe.allocated_budget");
            //sb.AppendLine(@"FROM Division div");
            //sb.AppendLine(@"LEFT JOIN mnda_budget_expenditure mbe on mbe.division_id = div.Division_Id");
            //sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures sub on sub.id = mbe.expenditure_id");
            //sb.AppendLine(@"WHERE div.Division_Id = "+ _div_id +" AND mbe.year = "+ _year +"");
            StringBuilder sb = new StringBuilder(503);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"sub.name,");
            sb.AppendLine(@"SUM(mad.total) as allocated_budget");
            sb.AppendLine(@"FROM mnda_activity_data mad");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures sub on sub.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_activity ma on ma.id =mad.activity_id");
            sb.AppendLine(@"LEFT JOIN mnda_project_output po on po.id = ma.output_id");
            sb.AppendLine(@"LEFT JOIN mnda_program_encoded mpe on mpe.id = po.program_code");
            sb.AppendLine(@"LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@"LEFT JOIN DBM_Sub_Pap dsp on dsp.DBM_Sub_Pap_Code=div.Division_Code	");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source mfs on mfs.Fund_Source_Id = mad.fund_source_id");
            sb.AppendLine(@"WHERE mad.year = " + _year + " and dsp.DBM_Sub_Pap_Code = " + _div_id + "");
            sb.AppendLine(@"GROUP BY sub.name");



            return sb.ToString();
        }
        public String FetchTypeService()
        {

            var sb = new System.Text.StringBuilder(57);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  uacs,");
            sb.AppendLine(@"  name");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_type_service;");



            return sb.ToString();
        }
        public String FetchAllocationAll(String _div_id, String _year)
        {
            //StringBuilder sb = new StringBuilder(392);
            //sb.AppendLine(@"SELECT");
            //sb.AppendLine(@"sub.name,");
            //sb.AppendLine(@"SUM(mbe.allocated_budget) as allocated_budget");
            //sb.AppendLine(@"FROM Division div");
            //sb.AppendLine(@"LEFT JOIN DBM_Sub_Pap dsp on dsp.DBM_Sub_Pap_id = div.DBM_Sub_Pap_Id");
            //sb.AppendLine(@"LEFT JOIN mnda_budget_expenditure mbe on mbe.division_id = div.Division_Id");
            //sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures sub on sub.id = mbe.expenditure_id");
            //sb.AppendLine(@"WHERE mbe.year = " + _year + " and div.DBM_Sub_Pap_Id = "+ _div_id+"");
            //sb.AppendLine(@"GROUP BY div.DBM_Sub_Pap_Id,sub.name");
            //StringBuilder sb = new StringBuilder(422);
            //sb.AppendLine(@"");
            //sb.AppendLine(@"SELECT ");
            //sb.AppendLine(@"sub.name,");
            //sb.AppendLine(@"SUM(mad.total) as allocated_budget");
            //sb.AppendLine(@"FROM mnda_activity_data mad");
            //sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures sub on sub.id = mad.mooe_sub_expenditure_id");
            //sb.AppendLine(@"LEFT JOIN mnda_activity ma on ma.id =mad.activity_id");
            //sb.AppendLine(@"LEFT JOIN mnda_project_output po on po.id = ma.output_id");
            //sb.AppendLine(@"LEFT JOIN mnda_program_encoded mpe on mpe.id = po.program_code");
            //sb.AppendLine(@"WHERE mad.year = "+ _year +" and mpe.acountable_division_code = "+ _div_id +"");
            //sb.AppendLine(@"GROUP BY sub.name");

            StringBuilder sb = new StringBuilder(503);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"sub.name,");
            sb.AppendLine(@"SUM(mad.total) as allocated_budget");
            sb.AppendLine(@"FROM mnda_activity_data mad");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures sub on sub.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_activity ma on ma.id =mad.activity_id");
            sb.AppendLine(@"LEFT JOIN mnda_project_output po on po.id = ma.output_id");
            sb.AppendLine(@"LEFT JOIN mnda_program_encoded mpe on mpe.id = po.program_code");
            sb.AppendLine(@"LEFT JOIN mnda_main_program_encoded mmpe  on mmpe.accountable_division = mpe.acountable_division_code");
            sb.AppendLine(@"WHERE mad.year = "+ _year +" and mmpe.id= "+ _div_id +"");
            sb.AppendLine(@"GROUP BY sub.name");


            return sb.ToString();
        }
        public String FetchExpenditureDetailTemplate()
        {
            var sb = new System.Text.StringBuilder(345);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  sub_ex.mooe_id as id,");
            sb.AppendLine(@"  sub_ex.name as Name,");
            sb.AppendLine(@"  sub_ex.uacs_code as UACS,");
            sb.AppendLine(@"  0.00 as Alloted,");
            sb.AppendLine(@"  0.00 as Jan,");
            sb.AppendLine(@"  0.00 as Feb,");
            sb.AppendLine(@"  0.00 as Mar,");
            sb.AppendLine(@"  0.00 as Apr,");
            sb.AppendLine(@"  0.00 as May,");
            sb.AppendLine(@"  0.00 as Jun,");
            sb.AppendLine(@"  0.00 as Jul,");
            sb.AppendLine(@"  0.00 as Aug,");
            sb.AppendLine(@"  0.00 as Sep,");
            sb.AppendLine(@"  0.00 as Oct,");
            sb.AppendLine(@"  0.00 as Nov,");
            sb.AppendLine(@"  0.00 as Dec,");
            sb.AppendLine(@"  0.00 as Total,");
            sb.AppendLine(@"  0.00 as Balance");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  mnda_mooe_sub_expenditures sub_ex ;");

       


            return sb.ToString();
        }
        public String FetchExpenditureDetailTemplateSumm()
        {
            var sb = new System.Text.StringBuilder(345);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  sub_ex.mooe_id as id,");
            sb.AppendLine(@"  sub_ex.name as Name,");
            sb.AppendLine(@"  sub_ex.uacs_code as UACS,");
            sb.AppendLine(@"  0.00 as Alloted,");
            sb.AppendLine(@"  0.00 as Jan,");
            sb.AppendLine(@"  0.00 as Feb,");
            sb.AppendLine(@"  0.00 as Mar,");
            sb.AppendLine(@"  0.00 as Quarter_1,");
            sb.AppendLine(@"  0.00 as Apr,");
            sb.AppendLine(@"  0.00 as May,");
            sb.AppendLine(@"  0.00 as Jun,");
            sb.AppendLine(@"  0.00 as Quarter_2,");
            sb.AppendLine(@"  0.00 as Jul,");
            sb.AppendLine(@"  0.00 as Aug,");
            sb.AppendLine(@"  0.00 as Sep,");
            sb.AppendLine(@"  0.00 as Quarter_3,");
            sb.AppendLine(@"  0.00 as Oct,");
            sb.AppendLine(@"  0.00 as Nov,");
            sb.AppendLine(@"  0.00 as Dec,");
            sb.AppendLine(@"  0.00 as Quarter_4,");
            sb.AppendLine(@"  0.00 as Total,");
            sb.AppendLine(@"  0.00 as Balance");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  mnda_mooe_sub_expenditures sub_ex ;");

            return sb.ToString();
        }
        public String FetchDataMBA(String _year,String _fund_source)
        {
            StringBuilder sb = new StringBuilder(320);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"MONTH(obr.DateCreated) as _month,");
            sb.AppendLine(@"lrc.Division as FundSource,");
            sb.AppendLine(@"lppa.PPACode as Office,");
            sb.AppendLine(@"la.AccountCode as uacs,");
            sb.AppendLine(@"la.AccountDescription as name,");
            sb.AppendLine(@"oorp.Amount as Total");
            sb.AppendLine(@"FROM or_ObligationRequestParticular oorp");
            sb.AppendLine(@"LEFT  JOIN or_ObligationRequest obr on obr.id = oorp.ObligationRequestID");
            sb.AppendLine(@"LEFT  JOIN lib_ResponsibilityCenter lrc on lrc.ID = oorp.ResponsibilityCenterID");
            sb.AppendLine(@"LEFT  JOIN lib_PPA lppa ON lppa.ID = oorp.PPAID");
            sb.AppendLine(@"LEFT  JOIN lib_Account la on la.ID = oorp.AcctID");
            sb.AppendLine(@"WHERE YEAR(obr.DateCreated) = " + _year + "  and lrc.Division ='"+ _fund_source +"'");
   

            return sb.ToString();
        }

        public String FetchDataMBASummary(String _year, String pap)
        {
            StringBuilder sb = new StringBuilder(320);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"MONTH(obr.DateCreated) as _month,");
            sb.AppendLine(@"lrc.Division as FundSource,");
            sb.AppendLine(@"lppa.PPACode as Office,");
            sb.AppendLine(@"la.AccountCode as uacs,");
            sb.AppendLine(@"la.AccountDescription as name,");
            sb.AppendLine(@"oorp.Amount as Total");
            sb.AppendLine(@"FROM or_ObligationRequestParticular oorp");
            sb.AppendLine(@"LEFT  JOIN or_ObligationRequest obr on obr.id = oorp.ObligationRequestID");
            sb.AppendLine(@"LEFT  JOIN lib_ResponsibilityCenter lrc on lrc.ID = oorp.ResponsibilityCenterID");
            sb.AppendLine(@"LEFT  JOIN lib_PPA lppa ON lppa.ID = oorp.PPAID");
            sb.AppendLine(@"LEFT  JOIN lib_Account la on la.ID = oorp.AcctID");
            sb.AppendLine(@"WHERE YEAR(obr.DateCreated) = " + _year + "   AND lppa.PPACode ='" + pap + "'");


            return sb.ToString();
        }
        public String FetchDataDB(String _YEAR, String PAP,String Fund_Source)
        {
            StringBuilder sb = new StringBuilder(320);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@" m.name,");
            sb.AppendLine(@"moe.code as id,dv.UACS,");
            sb.AppendLine(@" dv.FUND_SOURCE as fund_source,");
            sb.AppendLine(@"moe.name as mooe,");
            sb.AppendLine(@"dv.PAP,");
            sb.AppendLine(@"dv.month,");
            sb.AppendLine(@"dv.total,");
            sb.AppendLine(@"dv.year");
            sb.AppendLine(@"FROM dbo.dvDB dv");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures m on m.uacs_code = dv.UACS ");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_expenditures moe on moe.code  = m.mooe_id ");
            sb.AppendLine(@"WHERE dv.PAP ='"+ PAP +"' AND dv.year='"+ _YEAR +"' AND dv.FUND_SOURCE ='"+ Fund_Source +"';");
    


            return sb.ToString();
        }
        public String FetchDataSummary(String _year, String _id)
        {
            StringBuilder sb = new StringBuilder(564);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"div.Division_Id as id,div.Division_Desc as division,div.Division_Code as pap_code,");
            sb.AppendLine(@"mad.year,");
            sb.AppendLine(@"mad.month,");
            sb.AppendLine(@"mooe.name,");
            sb.AppendLine(@"mad.total");
            sb.AppendLine(@"FROM mnda_approved_projects_division mapd");
            sb.AppendLine(@"LEFT JOIN mnda_activity_data mad ON mad.id = mapd.activity_id");
            sb.AppendLine(@"LEFT JOIN mnda_main_program_encoded mpe on mpe.id = mapd.program_id");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN Division div on div.Division_Id = mpe.accountable_division");
            sb.AppendLine(@"LEFT JOIN DBM_Sub_Pap dsp on dsp.DBM_Sub_Pap_id = div.DBM_Sub_Pap_Id");
            sb.AppendLine(@"WHERE mapd.isapproved = 1");
            sb.AppendLine(@"AND mad.year = "+ _year +" AND dsp.DBM_Sub_Pap_id= "+ _id +"");

            return sb.ToString();
        }
    }

    public class AllocationExpenditure     
    {
            public string name {get;set;}
            public string allocated_budget { get; set; }
    }
    public class ReportProjectStatus
    {

        public string id { get; set; }
        public string Name { get; set; }
        public string UACS { get; set; }
        public string Jan { get; set; }
        public string Feb { get; set; }
        public string Mar { get; set; }
        public string Apr { get; set; }
        public string May { get; set; }
        public string Jun { get; set; }
        public string Jul { get; set; }
        public string Aug { get; set; }
        public string Sep { get; set; }
        public string Oct { get; set; }
        public string Nov { get; set; }
        public string Dec { get; set; }
        public string Total { get; set; }
        public string Status { get; set; }

    }
    public class ReportMBAHeaderSumm
    {
        public string id { get; set; }
        public string Division { get; set; }
        public string Name { get; set; }
        public string UACS { get; set; }
        public string Alloted { get; set; }
        public string Jan { get; set; }
        public string Feb { get; set; }
        public string Mar { get; set; }
        public string Quarter_1 { get; set; }
        public string Apr { get; set; }
        public string May { get; set; }
        public string Jun { get; set; }
        public string Quarter_2 { get; set; }
        public string Jul { get; set; }
        public string Aug { get; set; }
        public string Sep { get; set; }
        public string Quarter_3 { get; set; }
        public string Oct { get; set; }
        public string Nov { get; set; }
        public string Dec { get; set; }
        public string Quarter_4 { get; set; }
        public string Total { get; set; }
        public string Balance { get; set; }
        public string Status { get; set; }
    }
    public class ReportMBADetailSumm
    {
        public string id { get; set; }
        public string Division { get; set; }
        public string Name { get; set; }
        public string Alloted { get; set; }
        public string Jan { get; set; }
        public string Feb { get; set; }
        public string Mar { get; set; }
        public string Quarter_1 { get; set; }
        public string Apr { get; set; }
        public string May { get; set; }
        public string Jun { get; set; }
        public string Quarter_2 { get; set; }
        public string Jul { get; set; }
        public string Aug { get; set; }
        public string Sep { get; set; }
        public string Quarter_3 { get; set; }
        public string Oct { get; set; }
        public string Nov { get; set; }
        public string Dec { get; set; }
        public string Quarter_4 { get; set; }
        public string Total { get; set; }
        public string Balance { get; set; }
        public string Status { get; set; }
    }
    public class ReportMBAHeader
    {  
           public string   id {get;set;}
           public string   Name {get;set;}
           public string   UACS { get; set; }
           public string   Alloted {get;set;}
           public string   Jan {get;set;}
           public string   Feb {get;set;}
           public string   Mar {get;set;}
           public string   Apr {get;set;}
           public string   May {get;set;}
           public string   Jun {get;set;}
           public string   Jul {get;set;}
           public string   Aug {get;set;}
           public string     Sep {get;set;}
           public string     Oct {get;set;}
           public string     Nov {get;set;}
           public string     Dec {get;set;}
           public string     Total {get;set;}
           public string     Balance {get;set;}
    }
    public class ReportMBADetail
    {
        public string TypeService { get; set; }
        public string id { get; set; }
        public string Name { get; set; }
        public string UACS { get; set; }
        public string Alloted { get; set; }
        public string Jan { get; set; }
        public string Feb { get; set; }
        public string Mar { get; set; }
        public string Apr { get; set; }
        public string May { get; set; }
        public string Jun { get; set; }
        public string Jul { get; set; }
        public string Aug { get; set; }
        public string Sep { get; set; }
        public string Oct { get; set; }
        public string Nov { get; set; }
        public string Dec { get; set; }
        public string Total { get; set; }
        public string Balance { get; set; }
    }
    public class ReportMBASumm
    {
        public String ReportHeader { get; set; }
        public List<ReportMBADetailSumm> ReportDetail { get; set; }
        public string Alloted { get; set; }
        public string Jan { get; set; }
        public string Feb { get; set; }
        public string Mar { get; set; }
        public string Quarter_1 { get; set; }
        public string Apr { get; set; }
        public string May { get; set; }
        public string Jun { get; set; }
        public string Quarter_2 { get; set; }
        public string Jul { get; set; }
        public string Aug { get; set; }
        public string Sep { get; set; }
        public string Quarter_3 { get; set; }
        public string Oct { get; set; }
        public string Nov { get; set; }
        public string Dec { get; set; }
        public string Quarter_4 { get; set; }
        public string Total { get; set; }
        public string Balance { get; set; }
    }
    public class ReportMBA 
    {
        public String ReportHeader { get; set; }
        public List<ReportMBADetail> ReportDetail { get; set; }
        public string UACS { get; set; }
        public string Alloted { get; set; }     
        public string Jan { get; set; }
        public string Feb { get; set; }
        public string Mar { get; set; }
        public string Apr { get; set; }
        public string May { get; set; }
        public string Jun { get; set; }
        public string Jul { get; set; }
        public string Aug { get; set; }
        public string Sep { get; set; }
        public string Oct { get; set; }
        public string Nov { get; set; }
        public string Dec { get; set; }
        public string Total { get; set; }
        public string Balance { get; set; }
    }

    public class ReportDataMBA
    {
        public string type_service { get; set; }
        public string fund_source { get; set; }
        public string mooe { get; set; }
        public string uacs { get; set; }
        public string pap_code { get; set; }
        public string division { get; set; }
        public string id { get; set; }
        public string year { get; set; }
        public string month { get; set; }
        public string name { get; set; }
        public string total { get; set; }
        public string status { get; set; }
    }
    public class ReportTypeService
    {
        public string code { get; set; }
        public string uacs { get; set; }
   
        public string name { get; set; }
      
    }
}
