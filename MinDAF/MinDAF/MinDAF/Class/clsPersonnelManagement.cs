﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsPersonnelManagement
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();

        public String FetchPersonnelLists(String Division_Id) 
        {
            var sb = new System.Text.StringBuilder(171);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  User_Id,");
            sb.AppendLine(@"  Division_Id,");
            sb.AppendLine(@"  User_Fullname,");
            sb.AppendLine(@"  User_Contact,");
            sb.AppendLine(@"  User_Position,");
            sb.AppendLine(@"  Username,");
            sb.AppendLine(@"  Password,");
            sb.AppendLine(@"  status,");
            sb.AppendLine(@"  is_admin");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_user_accounts WHERE Division_Id ="+ Division_Id +" AND is_admin <>1;");

            return sb.ToString();
        }
        public String FetchPersonnelListsAdmin(String Division_Id)
        {
            var sb = new System.Text.StringBuilder(171);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  User_Id,");
            sb.AppendLine(@"  Division_Id,");
            sb.AppendLine(@"  User_Fullname,");
            sb.AppendLine(@"  User_Contact,");
            sb.AppendLine(@"  User_Position,");
            sb.AppendLine(@"  Username,");
            sb.AppendLine(@"  Password,");
            sb.AppendLine(@"  status,");
            sb.AppendLine(@"  is_admin");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_user_accounts WHERE Division_Id =" + Division_Id + ";");

            return sb.ToString();
        }
        public String FetchDivisions()
        {
            StringBuilder sb = new StringBuilder(101);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Division_Id,");
            sb.AppendLine(@"  DBM_Sub_Pap_Id,");
            sb.AppendLine(@"  Division_Code,");
            sb.AppendLine(@"  Division_Desc");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.Division;");


            return sb.ToString();
        }
        public void SavePersonnel(String Division_Id, String User_Fullname, String User_Contact, String User_Position,
            String Username, String Password, String status, String is_admin)
        {
            String _sqlString = "";

            var sb = new System.Text.StringBuilder(228);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_user_accounts");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  Division_Id,");
            sb.AppendLine(@"  User_Fullname,");
            sb.AppendLine(@"  User_Contact,");
            sb.AppendLine(@"  User_Position,");
            sb.AppendLine(@"  Username,");
            sb.AppendLine(@"  Password,");
            sb.AppendLine(@"  status,");
            sb.AppendLine(@"  is_admin");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  "+ Division_Id +",");
            sb.AppendLine(@"  '"+ User_Fullname +"',");
            sb.AppendLine(@"  '"+User_Contact  +"',");
            sb.AppendLine(@"  '"+ User_Position +"',");
            sb.AppendLine(@"  '"+ Username +"',");
            sb.AppendLine(@"  '"+ Password +"',");
            sb.AppendLine(@"  '" + status + "',");
            sb.AppendLine(@"  " + is_admin + "");
            sb.AppendLine(@"  ");
            sb.AppendLine(@");");




            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void SaveUpdatePersonnel(String UserId,String Division_Id, String User_Fullname, String User_Contact, String User_Position,
       String Username, String Password, String status, String is_admin)
        {
            String _sqlString = "";

            var sb = new System.Text.StringBuilder(219);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_user_accounts  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  Division_Id = "+ Division_Id +" ,");
            sb.AppendLine(@"  User_Fullname = '"+ User_Fullname +"',");
            sb.AppendLine(@"  User_Contact = '"+ User_Contact +"',");
            sb.AppendLine(@"  User_Position = '" + User_Position + "',");
            sb.AppendLine(@"  Username = '" + Username + "',");
            sb.AppendLine(@"  Password ='"+ Password +"',");
            sb.AppendLine(@"  status = '"+ status +"',");
            sb.AppendLine(@"  is_admin = "+ is_admin +"");
            sb.AppendLine(@"WHERE ");
            sb.AppendLine(@"  User_Id = " + UserId + ";");




            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "SaveData":
                    if (SQLOperation!=null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateData":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public class UserDivisions 
    {
        public String  Division_Id{get;set;}
        public String  DBM_Sub_Pap_Id{get;set;}
        public String  Division_Code{get;set;}
        public String  Division_Desc { get; set; }
    }
    public class PersonnelList 
    {
            public String  User_Id {get;set;}
            public String  Division_Id {get;set;}
            public String  User_Fullname {get;set;}
            public String  User_Contact {get;set;}
            public String  User_Position {get;set;}
            public String  Username {get;set;}
            public String  Password {get;set;}
            public String  status {get;set;}
            public String is_admin { get; set; }
    }
}
