﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsAPPData
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();

        public String GetData(String _year)
        {


            var sb = new System.Text.StringBuilder(1679);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"'M' as code,");
            sb.AppendLine(@"div.Division_Code as pap,");
            sb.AppendLine(@"mpe.project_name,");
            sb.AppendLine(@"div.DivisionAccro as pmo,");
            sb.AppendLine(@"'' as procurement_project,");
            sb.AppendLine(@"'' as mode_procurement,");
            sb.AppendLine(@"'' as ad_post_rei,");
            sb.AppendLine(@"'' as sub_open,");
            sb.AppendLine(@"'' as notice_award,");
            sb.AppendLine(@"'' as contract_signing,");
            sb.AppendLine(@"'' as source_funds,");
            sb.AppendLine(@"0.00 as total,");
            sb.AppendLine(@"0.00 as mooe_total,");
            sb.AppendLine(@"0.00 as co,");
            sb.AppendLine(@"'' as remarks");
            sb.AppendLine(@"FROM mnda_approved_projects_division madp");
            sb.AppendLine(@"LEFT JOIN mnda_activity_data mad on mad.id = madp.activity_id");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_program_encoded mpe on mpe.id = madp.program_id");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source mfs on mfs.Fund_Source_Id = mad.fund_source_id");
            sb.AppendLine(@"LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@"WHERE mad.year = "+ _year +" and madp.is_submitted =0 ");
            sb.AppendLine(@"and mfs.service_type = 1");
            sb.AppendLine(@"UNION");
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"'S' as code,");
            sb.AppendLine(@"div.Division_Code as pap,");
            sb.AppendLine(@"mooe.name as project_name,");
            sb.AppendLine(@"div.DivisionAccro as pmo,");
            sb.AppendLine(@"''  as procurement_project,");
            sb.AppendLine(@"'' as mode_procurement,");
            sb.AppendLine(@"'' as ad_post_rei,");
            sb.AppendLine(@"'' as sub_open,");
            sb.AppendLine(@"'' as notice_award,");
            sb.AppendLine(@"'' as contract_signing,");
            sb.AppendLine(@"'' as source_funds,");
            sb.AppendLine(@"0.00 as total,");
            sb.AppendLine(@"0.00 as mooe_total,");
            sb.AppendLine(@"0.00 as co,");
            sb.AppendLine(@"'' as remarks");
            sb.AppendLine(@"FROM mnda_approved_projects_division madp");
            sb.AppendLine(@"LEFT JOIN mnda_activity_data mad on mad.id = madp.activity_id");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_program_encoded mpe on mpe.id = madp.program_id");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source mfs on mfs.Fund_Source_Id = mad.fund_source_id");
            sb.AppendLine(@"LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@"WHERE mad.year = "+ _year +" and madp.is_submitted =0 ");
            sb.AppendLine(@"and mfs.service_type = 1");


            return sb.ToString();
        }

        public String GetPrograms(string _year)
        {

            var sb = new System.Text.StringBuilder(554);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mpe.id,");
            sb.AppendLine(@"div.Division_Code as pap,");
            sb.AppendLine(@"mpe.project_name as project_name,");
            sb.AppendLine(@"div.DivisionAccro as pmo");
            sb.AppendLine(@"FROM mnda_approved_projects_division madp");
            sb.AppendLine(@"LEFT JOIN mnda_activity_data mad on mad.id = madp.activity_id");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_program_encoded mpe on mpe.id = madp.program_id");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source mfs on mfs.Fund_Source_Id = mad.fund_source_id");
            sb.AppendLine(@"LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@"WHERE mad.year = "+ _year +" and madp.is_submitted =0 ");
            sb.AppendLine(@"and mfs.service_type = 1");


            return sb.ToString();
        }
        public String GetDivisionList()
        {

            var sb = new System.Text.StringBuilder(102);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"div.Division_Id,");
            sb.AppendLine(@"div.Division_Code,");
            sb.AppendLine(@"div.DivisionAccro,");
            sb.AppendLine(@"div.Division_Desc");
            sb.AppendLine(@"FROM Division div");

            return sb.ToString();
        }
        public String GetMOOE()
        {

            var sb = new System.Text.StringBuilder(59);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mo.code,");
            sb.AppendLine(@" mo.name");
            sb.AppendLine(@"FROM mnda_mooe_expenditures mo");

            return sb.ToString();
        }
        public String GetMOOESub()
        {

            var sb = new System.Text.StringBuilder(133);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mosub.uacs_code,");
            sb.AppendLine(@"mosub.name");
            sb.AppendLine(@"FROM mnda_mooe_sub_expenditures mosub ");
            sb.AppendLine(@"WHERE mosub.is_active = 1 and not mosub.uacs_code ='-'");

            return sb.ToString();
        }
    }


    public class APP_PROGRAMS 
    {
        public String ID { get; set; }
        public String PAPCODE { get; set; }
        public String Programs { get; set; }
        public String PMO { get; set; }
   
    }

    public class APP_MOOEList 
    {
        public String Code {get;set;} 
        public String Name {get;set;}

    }
    public class APP_MOEESUBList 
    {
        public String Code { get; set; }
        public String MooeSub { get; set; }

    }

    public class APPData_Expenditures 
    {
         public String  id {get;set;}
         public String  pap{get;set;}
         public String  expenditure_name { get; set; }
    }
    public class APPData_Programs
    {
        public String id { get; set; }
        public String pap { get; set; }
        public String program_name { get; set; }
    }

    public class APPData_List 
    {
             public String code {get;set;}
             public String pap{get;set;}
             public String project_name{get;set;}
             public String pmo{get;set;}
             public String procurement_project{get;set;}
             public String mode_procurement{get;set;}
             public String ad_post_rei{get;set;}
             public String sub_open{get;set;}
             public String notice_award{get;set;}
             public String contract_signing{get;set;}
             public String source_funds{get;set;}
             public String total{get;set;}
             public String mooe_total{get;set;}
             public String co{get;set;}
             public String remarks { get; set; }
    }
    public class APPData_Division
    {
      public String  Division_Id {get;set;}
      public String  Division_Code{get;set;}
      public String  DivisionAccro{get;set;}
      public String  Division_Desc { get; set; }
    }
}
