﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using Infragistics.Controls.Grids;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol.Budget_Allocation
{
    public partial class BudgetApprovals : UserControl
    {
        public String DivisionID { get; set; }
        public String Process { get; set; }
        private BudgetApprovalsList c_approval = new BudgetApprovalsList();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        PagedCollectionView MainData = null;

        private List<ApprovalData> ListApprovalData = new List<ApprovalData>();
        private List<MainData> _ListMainData = new List<MainData>();
        private List<MainTotals> _ListMainTotals = new List<MainTotals>();
        private List<Main_OverAllYear> _ListOverAllYear = new List<Main_OverAllYear>();

        public BudgetApprovals()
        {
            InitializeComponent();

            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            c_approval.SQLOperation += c_approval_SQLOperation;
        }

        void c_approval_SQLOperation(object sender, EventArgs e)
        {
            switch (c_approval.Process)
            {
                case"UpdateActivity":
                  //  FetchApprovalLists();
                    FetchActivities(grdProjectDetails.Rows[grdProjectDetails.ActiveCell.Row.Index].Cells["output_id"].Value.ToString(), grdProjectDetails.Rows[grdProjectDetails.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString(), grdProjectDetails.ActiveCell.Column.HeaderText.ToString(), grdProjectDetails.Rows[grdProjectDetails.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString());
               
                    break;
                case "DisapproveActivity":
                  //  FetchApprovalLists();
                    FetchActivities(grdProjectDetails.Rows[grdProjectDetails.ActiveCell.Row.Index].Cells["output_id"].Value.ToString(), grdProjectDetails.Rows[grdProjectDetails.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString(), grdProjectDetails.ActiveCell.Column.HeaderText.ToString(), grdProjectDetails.Rows[grdProjectDetails.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString());
                   
                    break;
                default:
                    break;
            }
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();

            switch (c_approval.Process)
            {
                case "FetchApprovalLists":
                     XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new MainData
                                     {
                                         div_id = Convert.ToString(info.Element("div_id").Value),
                                         Apr = Convert.ToString(info.Element("Apr").Value),
                                         Aug = Convert.ToString(info.Element("Aug").Value),
                                         Dec = Convert.ToString(info.Element("Dec").Value),
                                         activity = Convert.ToString(info.Element("activity").Value),
                                         Feb = Convert.ToString(info.Element("Feb").Value),
                                         fiscal_year = Convert.ToString(info.Element("fiscal_year").Value),
                                         Jan = Convert.ToString(info.Element("Jan").Value),
                                         Jul = Convert.ToString(info.Element("Jul").Value),
                                         Jun = Convert.ToString(info.Element("Jun").Value),
                                         Mar = Convert.ToString(info.Element("Mar").Value),
                                         May = Convert.ToString(info.Element("May").Value),
                                         Nov = Convert.ToString(info.Element("Nov").Value),
                                         Oct = Convert.ToString(info.Element("Oct").Value),
                                         output = Convert.ToString(info.Element("output").Value),
                                         program_id = Convert.ToInt32(info.Element("program_id").Value),
                                         program_name = Convert.ToString(info.Element("program_name").Value),
                                         project_name = Convert.ToString(info.Element("project_name").Value),
                                         Sep = Convert.ToString(info.Element("Sep").Value),
                                         User_Fullname = Convert.ToString(info.Element("User_Fullname").Value),
                                         activity_id = Convert.ToString(info.Element("activity_id").Value),
                                         project_id = Convert.ToString(info.Element("project_id").Value),
                                         output_id = Convert.ToString(info.Element("output_id").Value)
                                     };

                   
                    _ListMainData.Clear();

                    foreach (var item in _dataLists)
                    {
                        MainData _varDetails = new MainData();
                        if (item.div_id == this.DivisionID)
                        {
                            _varDetails.div_id = item.div_id;
                             _varDetails.activity_id = item.activity_id;
                             _varDetails.Apr = item.Apr;
                             _varDetails.Aug = item.Aug;
                             _varDetails.Dec = item.Dec;
                             _varDetails.activity = item.activity;
                             _varDetails.Feb = item.Feb;
                             _varDetails.fiscal_year = item.fiscal_year;
                             _varDetails.Jan = item.Jan;
                             _varDetails.Jul = item.Jul;
                             _varDetails.Jun = item.Jun;
                             _varDetails.Mar = item.Mar;
                             _varDetails.May = item.May;
                             _varDetails.Nov = item.Nov;
                             _varDetails.Oct = item.Oct;
                             _varDetails.output = item.output;
                             _varDetails.program_id = item.program_id;
                             _varDetails.program_name = item.program_name;
                             _varDetails.project_name = item.project_name;
                             _varDetails.Sep = item.Sep;
                             _varDetails.User_Fullname = item.User_Fullname;
                             _varDetails.project_id = item.project_id;
                             _varDetails.output_id = item.output_id;
                            _ListMainData.Add(_varDetails);
                        }
                    }

                  
                   
                    this.Cursor = Cursors.Arrow;

                    GetTotals();
                    break;
                case "FetchTotals":
                    XDocument oDocKeyFetchTotals = XDocument.Parse(_results);
                    var _dataListsFetchTotals = from info in oDocKeyFetchTotals.Descendants("Table")
                                                select new MainTotals
                                                {
                                                    activity_id = Convert.ToString(info.Element("activity_id").Value),
                                                    month = Convert.ToString(info.Element("month").Value),
                                                    total = Convert.ToString(info.Element("total").Value)

                                                };

                    _ListMainTotals.Clear();


                    foreach (var item in _dataListsFetchTotals)
                    {
                        MainTotals _varDetails = new MainTotals();

                        _varDetails.activity_id = item.activity_id;
                        _varDetails.month = item.month;
                        _varDetails.total = item.total;

                        _ListMainTotals.Add(_varDetails);
                    }

                    DistributeData();
                    this.Cursor = Cursors.Arrow;

                    grdProjectDetails.ItemsSource = null;
                    grdProjectDetails.ItemsSource = _ListMainData;

                    Column col_project_name = grdProjectDetails.Columns.DataColumns["project_name"];
                    Column col_program_name = grdProjectDetails.Columns.DataColumns["program_name"];
                    Column col_User_Fullname = grdProjectDetails.Columns.DataColumns["User_Fullname"];
                    Column col_fiscal_year = grdProjectDetails.Columns.DataColumns["fiscal_year"];
                    Column col_activity = grdProjectDetails.Columns.DataColumns["activity"];
                    Column col_output = grdProjectDetails.Columns.DataColumns["output"];

                    col_fiscal_year.Width = new ColumnWidth(70, false);
                    col_program_name.Width = new ColumnWidth(250, false);
                    col_User_Fullname.Width = new ColumnWidth(100, false);
                    col_activity.Width = new ColumnWidth(120, false);
                    col_output.Width = new ColumnWidth(120, false);
                    col_project_name.Width = new ColumnWidth(150, false);

                    grdProjectDetails.FixedColumnSettings.FixedColumnsLeft.Add(col_fiscal_year);
                    grdProjectDetails.FixedColumnSettings.FixedColumnsLeft.Add(col_program_name);
                    grdProjectDetails.FixedColumnSettings.FixedColumnsLeft.Add(col_project_name);
                    grdProjectDetails.FixedColumnSettings.FixedColumnsLeft.Add(col_output);
                    grdProjectDetails.FixedColumnSettings.FixedColumnsLeft.Add(col_activity);
                    grdProjectDetails.FixedColumnSettings.FixedColumnsLeft.Add(col_User_Fullname);




                    grdProjectDetails.Columns["activity_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdProjectDetails.Columns["program_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdProjectDetails.Columns["project_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdProjectDetails.Columns["output_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdProjectDetails.Columns["div_id"].Visibility = System.Windows.Visibility.Collapsed;
                    break;
                case "FetchActivities":
                    XDocument oDocKeyFetchActivities = XDocument.Parse(_results);
                    var _dataListsFetchActivities = from info in oDocKeyFetchActivities.Descendants("Table")
                                                    select new ApprovalData
                                                {
                                                    Item = Convert.ToString(info.Element("Item").Value),
                                                    Accountable = Convert.ToString(info.Element("Accountable").Value),
                                                    Date_End = Convert.ToDateTime(info.Element("Date_End").Value).ToShortDateString(),                                                 
                                                    Date_Start = Convert.ToDateTime(info.Element("Date_Start").Value).ToShortDateString(),
                                                    Id = Convert.ToString(info.Element("Id").Value),
                                                    IsApproved = Convert.ToBoolean(info.Element("IsApproved").Value),
                                                    Name = Convert.ToString(info.Element("Name").Value),
                                                    No_Days = Convert.ToString(info.Element("No_Days").Value),
                                                    No_Staff = Convert.ToString(info.Element("No_Staff").Value),
                                                    Qty = Convert.ToString(info.Element("Qty").Value),
                                                    Rate = Convert.ToString(info.Element("Rate").Value),
                                                    Remarks = Convert.ToString(info.Element("Remarks").Value),
                                                    Total = Convert.ToString(info.Element("Total").Value)

                                                };

                     ListApprovalData.Clear();


                    foreach (var item in _dataListsFetchActivities)
                    {
                        ApprovalData _varDetails = new ApprovalData();

                        _varDetails.Accountable = item.Accountable;
                        if (item.Date_Start.Contains("1900"))
                        {
                            _varDetails.Date_End = "-";
                            _varDetails.Date_Start = "-";
                        }
                        else
                        {
                            _varDetails.Date_End = item.Date_End;
                            _varDetails.Date_Start = item.Date_Start;
                        }

                        _varDetails.Item = item.Item;
                        _varDetails.Id = item.Id;
                        _varDetails.IsApproved = item.IsApproved;
                        _varDetails.Name = item.Name;
                        _varDetails.No_Days = item.No_Days;
                        _varDetails.No_Staff = item.No_Staff;
                        _varDetails.Qty = item.Qty;
                        _varDetails.Rate = item.Rate;
                        _varDetails.Remarks = item.Remarks;
                        _varDetails.Total = item.Total;
                        _varDetails.Disapprove = false;

                        ListApprovalData.Add(_varDetails);
                    }

                   
                    this.Cursor = Cursors.Arrow;

                    grdActivities.ItemsSource = null;
                    grdActivities.ItemsSource = ListApprovalData;
                    grdActivities.Columns["Id"].Visibility = System.Windows.Visibility.Collapsed;

                    break;
                
                case "FetchActivitiesUpdate":
                    XDocument oDocKeyFetchActivitiesUpdate = XDocument.Parse(_results);
                    var _dataListFetchActivitiesUpdate = from info in oDocKeyFetchActivitiesUpdate.Descendants("Table")
                                                                    select new ApprovalData
                                                                {
                                                                    Item = Convert.ToString(info.Element("Item").Value),
                                                                    Accountable = Convert.ToString(info.Element("Accountable").Value),
                                                                    Date_End = Convert.ToDateTime(info.Element("Date_End").Value).ToShortDateString(),                                                 
                                                                    Date_Start = Convert.ToDateTime(info.Element("Date_Start").Value).ToShortDateString(),
                                                                    Id = Convert.ToString(info.Element("Id").Value),
                                                                    IsApproved = Convert.ToBoolean(info.Element("IsApproved").Value),
                                                                    Name = Convert.ToString(info.Element("Name").Value),
                                                                    No_Days = Convert.ToString(info.Element("No_Days").Value),
                                                                    No_Staff = Convert.ToString(info.Element("No_Staff").Value),
                                                                    Qty = Convert.ToString(info.Element("Qty").Value),
                                                                    Rate = Convert.ToString(info.Element("Rate").Value),
                                                                    Remarks = Convert.ToString(info.Element("Remarks").Value),
                                                                    Total = Convert.ToString(info.Element("Total").Value)

                                                                };

                                     ListApprovalData.Clear();


                                     foreach (var item in _dataListFetchActivitiesUpdate)
                                    {
                                        ApprovalData _varDetails = new ApprovalData();

                                        _varDetails.Accountable = item.Accountable;
                                        if (item.Date_Start.Contains("1900"))
                                        {
                                            _varDetails.Date_End = "-";
                                            _varDetails.Date_Start = "-";
                                        }
                                        else
                                        {
                                            _varDetails.Date_End = item.Date_End;
                                            _varDetails.Date_Start = item.Date_Start;
                                        }

                                        _varDetails.Item = item.Item;
                                        _varDetails.Id = item.Id;
                                        _varDetails.IsApproved = item.IsApproved;
                                        _varDetails.Name = item.Name;
                                        _varDetails.No_Days = item.No_Days;
                                        _varDetails.No_Staff = item.No_Staff;
                                        _varDetails.Qty = item.Qty;
                                        _varDetails.Rate = item.Rate;
                                        _varDetails.Remarks = item.Remarks;
                                        _varDetails.Total = item.Total;
                                        _varDetails.Disapprove = false;

                                        ListApprovalData.Add(_varDetails);
                                    }

                   
                                    this.Cursor = Cursors.Arrow;
                                    
                                    grdActivities.ItemsSource = null;
                                    grdActivities.ItemsSource = ListApprovalData;
                                    grdActivities.Columns["Id"].Visibility = System.Windows.Visibility.Collapsed;
                                    this.FetchApprovalLists();
                                    break;
                  

            }
        }

        private void DistributeData()
        {
            Decimal _Jan = 0;
            Decimal _Feb = 0;
            Decimal _Mar = 0;
            Decimal _Apr = 0;
            Decimal _May = 0;
            Decimal _Jun = 0;
            Decimal _Jul = 0;
            Decimal _Aug = 0;
            Decimal _Sep = 0;
            Decimal _Oct = 0;
            Decimal _Nov = 0;
            Decimal _Dec = 0;
            foreach (var item in _ListMainData)
            {

                List<MainTotals> y = _ListMainTotals.Where(items => items.activity_id == item.activity_id).ToList();
                foreach (var item_details in y)
                {
                    switch (item_details.month)
                    {
                        case "Jan":
                            item.Jan = (Convert.ToDecimal(item.Jan) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                            _Jan += Convert.ToDecimal(item_details.total);
                            break;
                        case "Feb":
                            item.Feb = (Convert.ToDecimal(item.Feb) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                            _Feb += Convert.ToDecimal(item_details.total);
                            break;
                        case "Mar":
                            item.Mar = (Convert.ToDecimal(item.Mar) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                            _Mar += Convert.ToDecimal(item_details.total);
                            break;
                        case "Apr":
                            item.Apr = (Convert.ToDecimal(item.Apr) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                            _Apr += Convert.ToDecimal(item_details.total);
                            break;
                        case "May":
                            item.May = (Convert.ToDecimal(item.May) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                            _May += Convert.ToDecimal(item_details.total);
                            break;
                        case "Jun":
                            item.Jun = (Convert.ToDecimal(item.Jun) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                            _Jun += Convert.ToDecimal(item_details.total);
                            break;
                        case "Jul":
                            item.Jul = (Convert.ToDecimal(item.Jul) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                            _Jul += Convert.ToDecimal(item_details.total);
                            break;
                        case "Aug":
                            item.Aug = (Convert.ToDecimal(item.Aug) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                            _Aug += Convert.ToDecimal(item_details.total);
                            break;
                        case "Sep":
                            item.Sep = (Convert.ToDecimal(item.Sep) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                            _Sep += Convert.ToDecimal(item_details.total);
                            break;
                        case "Oct":
                            item.Oct = (Convert.ToDecimal(item.Oct) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                            _Oct += Convert.ToDecimal(item_details.total);
                            break;
                        case "Nov":
                            item.Nov = (Convert.ToDecimal(item.Nov) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                            _Nov += Convert.ToDecimal(item_details.total);
                            break;
                        case "Dec":
                            item.Dec = (Convert.ToDecimal(item.Dec) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                            _Dec += Convert.ToDecimal(item_details.total);
                            break;
                        default:
                            break;
                    }
                }
            }
            AddGrandTotals(_Jan, _Feb, _Mar, _Apr, _May, _Jun, _Jul, _Aug, _Sep, _Oct, _Nov, _Dec);
        }

        private void AddGrandTotals(
            Decimal _jan, Decimal _feb, Decimal _mar,
            Decimal _apr, Decimal _may, Decimal _jun,
            Decimal _jul, Decimal _aug, Decimal _sep,
            Decimal _oct, Decimal _nov, Decimal _dec
            )
        {
            MainData _varDetails;



            _varDetails = new MainData();
            _varDetails.output = "=============";
            _varDetails.program_id = 0;
            _varDetails.program_name = "==============================================";
            _varDetails.project_name = "=============";
            _varDetails.User_Fullname = "=============";
            _varDetails.fiscal_year = "=============";
            _varDetails.activity = "=============";
            _varDetails.activity_id = "=============";
            _varDetails.Jan = "=============";
            _varDetails.Feb = "=============";
            _varDetails.Mar = "=============";
            _varDetails.Apr = "=============";
            _varDetails.May = "=============";
            _varDetails.Jun = "=============";
            _varDetails.Jul = "=============";
            _varDetails.Aug = "=============";
            _varDetails.Sep = "=============";
            _varDetails.Oct = "=============";
            _varDetails.Nov = "=============";
            _varDetails.Dec = "=============";

            _ListMainData.Add(_varDetails);

            _varDetails = new MainData();

            _varDetails.output = "";
            _varDetails.program_id = 0;
            _varDetails.program_name = "";
            _varDetails.project_name = "";
            _varDetails.User_Fullname = "Grand Total :";
            _varDetails.fiscal_year = "";
            _varDetails.activity = "";
            _varDetails.activity_id = "";
            _varDetails.Jan = Convert.ToDouble(_jan).ToString("#,##0.00");
            _varDetails.Feb = Convert.ToDouble(_feb).ToString("#,##0.00");
            _varDetails.Mar = Convert.ToDouble(_mar).ToString("#,##0.00");
            _varDetails.Apr = Convert.ToDouble(_apr).ToString("#,##0.00");
            _varDetails.May = Convert.ToDouble(_may).ToString("#,##0.00");
            _varDetails.Jun = Convert.ToDouble(_jun).ToString("#,##0.00");
            _varDetails.Jul = Convert.ToDouble(_jul).ToString("#,##0.00");
            _varDetails.Aug = Convert.ToDouble(_aug).ToString("#,##0.00");
            _varDetails.Sep = Convert.ToDouble(_sep).ToString("#,##0.00");
            _varDetails.Oct = Convert.ToDouble(_oct).ToString("#,##0.00");
            _varDetails.Nov = Convert.ToDouble(_nov).ToString("#,##0.00");
            _varDetails.Dec = Convert.ToDouble(_dec).ToString("#,##0.00");

            _ListMainData.Add(_varDetails);
        }

      

        private void GetTotals()
        {
            c_approval.Process = "FetchTotals";
            svc_mindaf.ExecuteSQLAsync(c_approval.FetchTotals());

        }
        private void FetchApprovalLists()
        {
            c_approval.Process = "FetchApprovalLists";
            svc_mindaf.ExecuteSQLAsync(c_approval.FetchApprovalDataList());
        }
        private void FetchActivities(String _output_id,String _year, String _month,String _activity_id)
        {
            c_approval.Process = "FetchActivities";
            svc_mindaf.ExecuteSQLAsync(c_approval.FetchActivities(_output_id, _year, _month, _activity_id));
        }
        private void FetchActivitiesUpdate(String _output_id, String _year, String _month, String _activity_id)
        {
            c_approval.Process = "FetchActivitiesUpdate";
            svc_mindaf.ExecuteSQLAsync(c_approval.FetchActivities(_output_id, _year, _month, _activity_id));
        }
        
        private void ComputeProjectCost() 
        {
            //double _total = 0.00;

            //foreach (var item in LocalProjectDetailLists)
            //{
            //    _total +=Convert.ToDouble( item.Amount);
            //}

            //txtTotalProjectCost.Value = _total;
        }
       

        private void ExpandAll()
        {
             foreach (var item in grdProjectDetails.Rows)
                    {
                        
                        if (item.HasChildren)
                        {
                            item.IsExpanded = true;
                            foreach (var items in item.ChildBands)
                            {
                                if (items.HasChildren)
                                {
                                    items.IsExpanded = true;

                                    foreach (var itemaa in items.Rows)
                                    {

                                        itemaa.ColumnLayout.HeaderVisibility = System.Windows.Visibility.Visible;
                                        if (itemaa.HasChildren)
                                        {
                                            itemaa.IsExpanded = true;
                                            foreach (var itemaaaa in itemaa.ChildBands)
                                            {
                                                if (itemaaaa.HasChildren)
                                                {
                                                    itemaaaa.IsExpanded = true;
                                                    foreach (var xitem in  itemaaaa.Rows)
                                                    {
                                                        if (xitem.HasChildren)
                                                        {
                                                            xitem.IsExpanded = true;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                              
                            }


                        }

                        break;
                    }
        }
    
        private void btnVerify_Click(object sender, RoutedEventArgs e)
        {
          
          
        }

        private void grdProjectList_ActiveCellChanged(object sender, EventArgs e)
        {
            grdProjectDetails.ItemsSource = null;
        }

        private void usrApprovals_Loaded(object sender, RoutedEventArgs e)
        {
            FetchApprovalLists();
        }


        private String _MonthClick = "";
  

        private void grdProjectDetails_CellClicked(object sender, Infragistics.Controls.Grids.CellClickedEventArgs e)
        {
            if (e.Cell.Column.HeaderText.ToString() == "Jan" ||
                            e.Cell.Column.HeaderText.ToString() == "Feb" ||
                            e.Cell.Column.HeaderText.ToString() == "Mar" ||
                            e.Cell.Column.HeaderText.ToString() == "Apr" ||
                            e.Cell.Column.HeaderText.ToString() == "May" ||
                            e.Cell.Column.HeaderText.ToString() == "Jun" ||
                            e.Cell.Column.HeaderText.ToString() == "Jul" ||
                            e.Cell.Column.HeaderText.ToString() == "Aug" ||
                            e.Cell.Column.HeaderText.ToString() == "Sep" ||
                            e.Cell.Column.HeaderText.ToString() == "Oct" ||
                            e.Cell.Column.HeaderText.ToString() == "Nov" ||
                            e.Cell.Column.HeaderText.ToString() == "Dec"
                        )
            {
                if (e.Cell.Row.Cells["fiscal_year"].Value.ToString() != null || e.Cell.Row.Cells["fiscal_year"].Value.ToString() != "")
                {
                    FetchActivities(e.Cell.Row.Cells["output_id"].Value.ToString(), e.Cell.Row.Cells["fiscal_year"].Value.ToString(), e.Cell.Column.HeaderText, e.Cell.Row.Cells["activity_id"].Value.ToString());
                }
                _MonthClick = e.Cell.Column.HeaderText.ToString();
                      

            }
                        
                  
        }

        private void UpdateActivity(String _Id, String _Value,String _ProgramId,String _ProjectId,String _OutputId,String MainActivityId)
        {
            c_approval.Process = "UpdateActivity";
            c_approval.UpdateActivity(_Id, _Value, _ProgramId, _ProjectId, _OutputId, MainActivityId);
        }
        private void DisapproveActivity(String _Id)
        {
            c_approval.Process = "DisapproveActivity";
            c_approval.DisapproveActivity(_Id);
        }

        private void grdActivities_CellClicked(object sender, CellClickedEventArgs e)
        {
            switch (e.Cell.Column.HeaderText)
            {

                case "IsApproved":
                    Boolean _stat = Convert.ToBoolean(e.Cell.Value.ToString());
                    if (_stat == false)
                    {
                        String _Id = e.Cell.Row.Cells["Id"].Value.ToString();


                        List<MainData> x = _ListMainData.Where(item => item.activity_id == grdProjectDetails.Rows[grdProjectDetails.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString()).ToList();
                        String _ProjectId = "";
                        String _ProgramId = "";
                        String _OutputId = "";
                        String _MainId = "";
                        foreach (var item in x)
                        {
                            _ProjectId = item.project_id;
                            _ProgramId = item.program_id.ToString();
                            _OutputId = item.output_id;
                            _MainId = item.activity_id;
                        }

                        UpdateActivity(_Id, "1", _ProgramId, _ProjectId, _OutputId,_MainId);
                    }
                    else if (_stat == true)
                    {
                        String _Id = e.Cell.Row.Cells["Id"].Value.ToString();

                        List<MainData> x = _ListMainData.Where(item => item.activity_id == grdProjectDetails.Rows[grdProjectDetails.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString()).ToList();
                        String _ProjectId = "";
                        String _ProgramId = "";
                        String _OutputId = "";
                        String _MainId = "";
                        foreach (var item in x)
                        {
                            _ProjectId = item.project_id;
                            _ProgramId = item.program_id.ToString();
                            _OutputId = item.output_id;
                            _MainId = item.activity_id;
                        }

                        UpdateActivity(_Id, "0", _ProgramId, _ProjectId, _OutputId, _MainId);
                    }


                   
                      
                    break;

                case "Disapprove":
                    Boolean _statData = Convert.ToBoolean(e.Cell.Value.ToString());
                    if (_statData == false)
                    {
                        String _Id = e.Cell.Row.Cells["Id"].Value.ToString();


                        List<MainData> x = _ListMainData.Where(item => item.activity_id == grdProjectDetails.Rows[grdProjectDetails.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString()).ToList();
                        String _ProjectId = "";
                        String _ProgramId = "";
                        String _OutputId = "";
                        String _MainId = "";
                        foreach (var item in x)
                        {
                            _ProjectId = item.project_id;
                            _ProgramId = item.program_id.ToString();
                            _OutputId = item.output_id;
                            _MainId = item.activity_id;
                        }

                        DisapproveActivity(_Id);
                    }



                    break;
               
            }
        }

    }

    public class ProcurementMainData 
    {
        public List<PYear> _FiscalYear { get; set; }   
    }

    public class PYear
    {
        public String _Year {get;set;}
        public String _Program { get; set; }
        public String _Project { get; set; }
        public List<OutputData> Output { get; set; }
    }

    public class OutputData 
    {
        public String Output { get; set; }
        public List<ActivityData> Activities { get; set; }
    }

    public class ActivityData 
    {
        public String Activity { get; set; }
        public List<ActivityDetails> Details { get; set; }
    }

    public class ActivityDetails
    {
        public String Id { get; set; }
        public String Months { get; set; }
        public String Assigned { get; set; }
        public String Expenditure { get; set; }
        public String Remarks { get; set; }
        public String Total { get; set; }
        public Boolean isApprove { get; set; }
    }
}
