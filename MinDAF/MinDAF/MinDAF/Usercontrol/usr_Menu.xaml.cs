﻿using MinDAF.Forms;
using MinDAF.Usercontrol.Budget_Allocation;
using MinDAF.Usercontrol.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Usercontrol
{
    public partial class usr_Menu : UserControl
    {
        public String Division { get; set; }
        public String DivisionID { get; set; }
        public String OfficeID { get; set; }
        public String UserId { get; set; }
        public String User { get; set; }
        public String IsAdmin { get; set; }
        public String PAP { get; set; }

        private usr_budget_creation ctrl_budget = new usr_budget_creation();
        private usrCreateProject ctrl_CreateProject = new usrCreateProject();

        private BudgetApprovals ctrl_budget_approvals = new BudgetApprovals();
        private usrCreateProject ctrl_create_project = new usrCreateProject();
        private usrCreateProgram ctrl_create_program = new usrCreateProgram();
        private ppmp_report_user ctrl_ppmp_generate = new ppmp_report_user();
        private usr_division_project_status ctrl_project_status = new usr_division_project_status();

        private usr_main_dashboard ctrl_main_dashboard = new usr_main_dashboard();

        private BudgetGraph ctrl_Graph = new BudgetGraph();


        public usr_Menu()
        {
            InitializeComponent();
            App.Current.Host.Content.Resized += Content_Resized;
          
        }

        void Content_Resized(object sender, EventArgs e)
        {
            this.Width = App.Current.Host.Content.ActualWidth;

            this.Height = App.Current.Host.Content.ActualHeight;
        }

        private void usrMenu_Loaded(object sender, RoutedEventArgs e)
        {
            lblDivision.Content = Division;
            lblUser.Content = User;
            ctrl_budget.DivisionID = DivisionID;
            ctrl_Graph.DivisionID = DivisionID;

            ctrl_create_program.Width = stkChild.Width;
            ctrl_create_program.Height = stkChild.Height;
            
            ctrl_ppmp_generate.Width = stkChild.Width;
            ctrl_ppmp_generate.Height = stkChild.Height;

            ctrl_project_status.Width = stkChild.Width;
            ctrl_project_status.Height = stkChild.Height;

            ctrl_main_dashboard.Width = stkChild.Width;
            ctrl_main_dashboard.Height = stkChild.Height;

            ctrl_main_dashboard.DivisionId = DivisionID;
            ctrl_main_dashboard.UserId = UserId;
            ctrl_main_dashboard.IsAdmin = this.IsAdmin;
            ctrl_main_dashboard.Username = this.User;

            stkChild.Children.Clear();
            stkChild.Children.Add(ctrl_main_dashboard);

            if (IsAdmin=="2")
            {
                m_Menu.Visibility = System.Windows.Visibility.Visible;
                mnuExpenditure.Visibility = System.Windows.Visibility.Collapsed;
                btnAddNew.IsEnabled = true;
                mnuBudgetAllocation.Visibility = System.Windows.Visibility.Collapsed;
           
            }
            else if (IsAdmin == "1")
            {
                m_Menu.Visibility = System.Windows.Visibility.Visible;
                mnuExpenditure.Visibility = System.Windows.Visibility.Visible;
                btnAddNew.IsEnabled = true;
                mnuBudgetAllocation.Visibility = System.Windows.Visibility.Visible;
     
            }
            else
            {
                mnuDivisionPerson.Visibility = System.Windows.Visibility.Collapsed;
                mnuExpenditure.Visibility = System.Windows.Visibility.Collapsed;
                btnAddNew.IsEnabled = false;
                mnuBudgetAllocation.Visibility = System.Windows.Visibility.Collapsed;
            }

        }

        public void AddNewRow() 
        {
            ctrl_main_dashboard.AddNewData();
        }

        private void mnuBudgetProposal_Click(object sender, EventArgs e)
        {
            stkChild.Children.Clear();
            stkChild.Children.Add(ctrl_budget);
        }

        private void mnuBudgetApprovals_Click(object sender, EventArgs e)
        {
            stkChild.Children.Clear();
            stkChild.Children.Add(ctrl_budget_approvals);
        }

        private void mnuProgramEncoding_Click(object sender, EventArgs e)
        {
            ctrl_create_program.DivisionID = this.DivisionID;
            stkChild.Children.Clear();
            stkChild.Children.Add(ctrl_create_program);
        }

        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {
            AddNewRow();
        }

  

        private void mnu_mooe_lib_Click(object sender, EventArgs e)
        {
            frmExpenditureLibrary frm_exp_lib = new frmExpenditureLibrary();
            frm_exp_lib.Show();
        }

        private void mnu_procurement_lib_Click(object sender, EventArgs e)
        {
            frmProcurementManager frm_proc_lib = new frmProcurementManager();
            frm_proc_lib.Show();
        }

        private void mnu_manage_personel_Click(object sender, EventArgs e)
        {
            frmPersonnelManagement frm_person = new frmPersonnelManagement();
            frm_person.DivisionId = this.DivisionID;
            frm_person.Show();
        }

        private void mnuProjStat_Click(object sender, EventArgs e)
        {
            ctrl_project_status.DivisionId = this.DivisionID;
            ctrl_project_status.User = this.User;
            ctrl_project_status.Level = Convert.ToInt32(this.IsAdmin);
            stkChild.Children.Clear();
            stkChild.Children.Add(ctrl_project_status);
           
        }

        private void mnu_expen_Click(object sender, EventArgs e)
        {
            frmExpenditureManager f_exp = new frmExpenditureManager();
            f_exp.Show();
        }

        private void mnu_expen_main_Click(object sender, EventArgs e)
        {
           
        }

        private void btnNotify_Click(object sender, RoutedEventArgs e)
        {
            double width = App.Current.Host.Content.ActualWidth;
            double height = App.Current.Host.Content.ActualHeight;


            
        }

        private void mnuBudgetAllocation_Click(object sender, EventArgs e)
        {
          

        }

        private void mnuPPMP_Click(object sender, EventArgs e)
        {
            btnAddNew.Visibility = System.Windows.Visibility.Collapsed;
      
            ctrl_ppmp_generate.DivisionId = this.DivisionID;
            ctrl_ppmp_generate.Division = this.Division;
            ctrl_ppmp_generate.PAP = this.PAP;

            stkChild.Children.Clear();
            stkChild.Children.Add(ctrl_ppmp_generate);
        }

        private void mnureal_Click(object sender, EventArgs e)
        {
            frmreAlignment f_rel = new frmreAlignment();
            f_rel.DivisionId = this.DivisionID;
            f_rel.PaP = this.PAP;
            f_rel.Show();
        }

        private void mnu_b_nep_Click(object sender, EventArgs e)
        {
            frmBudgetDivisionAllocate f_bda = new frmBudgetDivisionAllocate(this.DivisionID);
            f_bda.OfficeId = this.OfficeID;
            f_bda.Show();
        }

        private void mnu_b_mooe_Click(object sender, EventArgs e)
        {
            frmBudgetAllocationMooe f_bda = new frmBudgetAllocationMooe(this.DivisionID);
            f_bda.OfficeId = this.OfficeID;
            f_bda.Show();
        }

        private void mnu_respo_lib_Click(object sender, EventArgs e)
        {
            frmLibraryFundSource f_form = new frmLibraryFundSource();
            f_form.Show();
        }

        private void mnuExpenditureManage_Click(object sender, EventArgs e)
        {
            frmMainExpenditure f_exp_main = new frmMainExpenditure();
            f_exp_main.Show();
        }

       
     
    }
}
