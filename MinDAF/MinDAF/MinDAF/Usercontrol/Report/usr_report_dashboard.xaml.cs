﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using Infragistics.Controls.Grids;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol.Report
{
    public partial class usr_report_dashboard : UserControl
    {
        public String Division { get; set; }
        public String DivisionID { get; set; }
        public String OfficeID { get; set; }
        public String PAPCODE { get; set; }
        public String UserId { get; set; }
        public String User { get; set; }
        public String IsAdmin { get; set; }
        public String SelectedYear { get; set; }

        private Double _TotalAllocation = 0;
        private Double _TotalAmount = 0;
        private Double _TotalCumulative = 0;
        private double _TotalBalance = 0;

        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsOfficeDashBoard c_office_dash = new clsOfficeDashBoard();
        private List<OfficeLevel> OfficeData = new List<OfficeLevel>();
        private List<BudgetExpenitureMain> ListBudgetExpense = new List<BudgetExpenitureMain>();
        private List<DivisionLevel> DivisionData = new List<DivisionLevel>();
        private List<OfficeDetails> BalanceDetails = new List<OfficeDetails>();
        private List<BudgetExpenitureMain> ListBudgetAllocated = new List<BudgetExpenitureMain>();
        private List<BudgetBalances> BBDetails = new List<BudgetBalances>();
        private List<ProgramLevel> ProgramData = new List<ProgramLevel>();
        private List<ExpenseItemsList> ExpenseItemData = new List<ExpenseItemsList>();
        public usr_report_dashboard()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            svc_mindaf.ExecuteImportDataSQLCompleted += svc_mindaf_ExecuteImportDataSQLCompleted;
        }

        void svc_mindaf_ExecuteImportDataSQLCompleted(object sender, ExecuteImportDataSQLCompletedEventArgs e)
        {
            string _result = e.Result.ToString();
            
            switch (c_office_dash.Process)
	        {
                case "FetchBalances":
                    XDocument oDocFetchBalances = XDocument.Parse(_result);
                    var _dataFetchBalances = from info in oDocFetchBalances.Descendants("Table")
                                             select new BudgetBalances
                                             {
                                                 Division = Convert.ToString(info.Element("Division").Value),
                                                 Office = Convert.ToString(info.Element("Office").Value),
                                                 Total = Convert.ToString(info.Element("Total").Value)

                                             };



                    BBDetails.Clear();

                    foreach (var item in _dataFetchBalances)
                    {
                        BudgetBalances _varData = new BudgetBalances();

                        _varData.Division = item.Division;
                        _varData.Office = item.Office;
                        _varData.Total = item.Total;

                        BBDetails.Add(_varData);

                    }
                    FetchOfficeTotals();


                    this.Cursor = Cursors.Arrow;
                    break;
	        }
             
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
          
            string _result = e.Result.ToString();
            switch (c_office_dash.Process)
            {
                case "FetchBudgetAllocation":
                    try
                    {
                        

                        XDocument oDocFetchBudgetAllocation = XDocument.Parse(_result);
                        var _dataFetchBudgetAllocation = from info in oDocFetchBudgetAllocation.Descendants("Table")
                                                         select new BudgetExpenditureList
                                                         {
                                                             mo_name = Convert.ToString(info.Element("mo_name").Value),
                                                             Amount = Convert.ToString(info.Element("Amount").Value),
                                                             Name = Convert.ToString(info.Element("Name").Value),
                                                             pap_code = Convert.ToString(info.Element("pap_code").Value),
                                                             pap_sub = Convert.ToString(info.Element("pap_sub").Value),
                                                             uacs_code = Convert.ToString(info.Element("uacs_code").Value),
                                                         };

                        List<BudgetExpenditureList> _list = new List<BudgetExpenditureList>();
                        List<BudgetExpenditureList> _list_sub = new List<BudgetExpenditureList>();
                        //ListBudgetAllocated.Clear();

                        ListBudgetExpense.Clear();

                        foreach (var item in _dataFetchBudgetAllocation)
                        {
                            BudgetExpenditureList _varData = new BudgetExpenditureList();

                            _varData.Amount = item.Amount;
                            _varData.mo_name = item.mo_name;
                            _varData.Name = item.Name;
                            _varData.pap_code = item.pap_code;
                            _varData.pap_sub = item.pap_sub;
                            _varData.uacs_code = item.uacs_code;


                            _list.Add(_varData);
                            _list_sub.Add(_varData);
                        }

                        var results = from p in _list
                                      group p by p.pap_code into g
                                      select new
                                      {
                                          PAPCode = g.Key,
                                          ExpenseName = g.Select(m => m.mo_name)
                                      };

                        foreach (var item in results)
                        {
                            BudgetExpenitureMain x_datas = new BudgetExpenitureMain();

                            var x = item.ExpenseName.ToList();

                            x_datas.MOOE_NAME =x[0].ToString();
                           
                            List<BudgetExpenditureList> _xList = _list_sub.Where(items => items.pap_sub ==item.PAPCode).ToList();
                            List<BudgetExpenditureDashBoard> _subdata = new List<BudgetExpenditureDashBoard>();

                            foreach (var itemList in _xList)
                            {
                                BudgetExpenditureDashBoard _xxdata = new BudgetExpenditureDashBoard();

                                _xxdata.Expenditure = itemList.Name;
                                _xxdata.UACS = itemList.uacs_code;
                                _xxdata.Total = Convert.ToDouble(itemList.Amount).ToString("#,##0.00");

                                _subdata.Add(_xxdata);
                            }
                            x_datas.ListExpenditure=_subdata;

                            ListBudgetExpense.Add(x_datas);
                           
                        }


                        grdBudget.ItemsSource = null;
                        grdBudget.ItemsSource = ListBudgetExpense;

                        foreach (var item in grdBudget.Rows)
                        {
                            if (item.HasChildren)
                            {
                                item.IsExpanded = true;
                            }
                        }
                        grdBudget.Columns["MOOE_NAME"].HeaderText = "Mode of Expenditure";
                        //lblNep.Content = cmbYear.SelectedItem.ToString() + " Budgeting by MOOE Object of Expenditures";
                        //Column col_exp_name = grdBudget.Columns.DataColumns["Expenditure"];
                        //col_exp_name.Width = new ColumnWidth(500, false);
                    }
                    catch (Exception)
                    {
                        grdBudget.ItemsSource = null;
                        grdBudget.ItemsSource = ListBudgetAllocated;
                    }

                    GenerateData();
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchOffice":
                    XDocument oDoc = XDocument.Parse(_result);
                    var _data = from info in oDoc.Descendants("Table")
                                select new OfficeLevel
                                {
                                    Office = Convert.ToString(info.Element("office").Value),
                                    Pap =  Convert.ToString(info.Element("pap").Value)
                                };


                    OfficeData.Clear();

                    foreach (var item in _data)
                    {
                        OfficeLevel _varData = new OfficeLevel();


                        this.PAPCODE = item.Pap;
                        _varData.Office = item.Office;

                        OfficeData.Add(_varData);
  
                    }

               
                     FetchDivisionData();
                   
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchDivisions":
                    XDocument oDocFetchDivisions = XDocument.Parse(_result);
                    var _dataFetchDivisions = from info in oDocFetchDivisions.Descendants("Table")
                                select new DivisionLevel
                                {
                                    Division = Convert.ToString(info.Element("Division").Value),
                                    id = Convert.ToString(info.Element("id").Value)

                                };
                  


                    DivisionData.Clear();

                    foreach (var item in _dataFetchDivisions)
                    {
                        DivisionLevel _varData = new DivisionLevel();


                        _varData.id = item.id;
                         _varData.Division = item.Division;

                        DivisionData.Add(_varData);
                       
                    }


                   
                   FetchOfficeExpenditures();
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchOfficeExpenditures":
                    XDocument oDocFetchOfficeExpenditures = XDocument.Parse(_result);
                    var _dataFetchOfficeExpenditures = from info in oDocFetchOfficeExpenditures.Descendants("Table")
                                              select new ExpenseItemsList
                                              {
                                                  fund_source_id = Convert.ToString(info.Element("fund_source_id").Value),
                                                  program = Convert.ToString(info.Element("program").Value),
                                                  program_id = Convert.ToString(info.Element("program_id").Value),
                                                  status = Convert.ToString(info.Element("status").Value),
                                                  month = Convert.ToString(info.Element("month").Value),
                                                  expen_code = Convert.ToString(info.Element("expen_code").Value),
                                                  mooe_id = Convert.ToString(info.Element("mooe_id").Value),
                                                  division= Convert.ToString(info.Element("division").Value),
                                                   division_id= Convert.ToString(info.Element("division_id").Value),
                                                   expenditure_name= Convert.ToString(info.Element("expenditure_name").Value),
                                                   expense_name = Convert.ToString(info.Element("expense_name").Value),
                                                   total  = Convert.ToString(info.Element("total").Value),
                                                   uacs_code  = Convert.ToString(info.Element("uacs_code").Value)

                                              };



                    ExpenseItemData.Clear();

                    foreach (var item in _dataFetchOfficeExpenditures)
                    {
                        ExpenseItemsList _varData = new ExpenseItemsList();

                        _varData.fund_source_id = item.fund_source_id;
                        _varData.month = item.month;
                        _varData.expen_code = item.expen_code;
                        _varData.mooe_id = item.mooe_id;
                         _varData.division = item.division;
                         _varData.division_id = item.division_id;
                         _varData.expenditure_name = item.expenditure_name;
                         _varData.expense_name = item.expense_name;
                         _varData.total = item.total;
                         _varData.uacs_code = item.uacs_code;
                         _varData.status = item.status;
                         _varData.program = item.program;
                         _varData.program_id = item.program_id;
                        ExpenseItemData.Add(_varData);

                    }

                    FetchBudgetAllocation();


                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchOfficeDetails":
                    XDocument oDocFetchOfficeDetails = XDocument.Parse(_result);
                    var _dataFetchOfficeDetails = from info in oDocFetchOfficeDetails.Descendants("Table")
                                                       select new OfficeDetails
                                                       {
                                                           fund_source_id = Convert.ToString(info.Element("fund_source_id").Value),
                                                           budget_allocation = Convert.ToString(info.Element("budget_allocation").Value),
                                                           division_id = Convert.ToString(info.Element("div_id").Value),
                                                           Fund_Source = Convert.ToString(info.Element("Fund_Name").Value),
                                                           balance = "0.00",
                                                           total_program_budget = "0.00",
                                                           pap_code = ""
                                                       };



                    BalanceDetails.Clear();

                    foreach (var item in _dataFetchOfficeDetails)
                    {
                        OfficeDetails _varData = new OfficeDetails();

                        _varData.budget_allocation = item.budget_allocation;
                        _varData.division_id = item.division_id;
                        _varData.Fund_Source = item.Fund_Source;
                        _varData.total_program_budget = item.total_program_budget;
                        _varData.pap_code = item.pap_code;
                        _varData.fund_source_id = item.fund_source_id;
                        BalanceDetails.Add(_varData);

                    }
                  //  FetchOfficeTotals();
                    FetchBalances();
                    this.Cursor = Cursors.Arrow;
                    break;
              
                case "FetchOfficeTotals":
                    XDocument oDocFetchOfficeTotals = XDocument.Parse(_result);
                    var _dataFetchOfficeTotals = from info in oDocFetchOfficeTotals.Descendants("Table")
                                                       select new OfficeTotals
                                                       {
                                                           balance = Convert.ToString(info.Element("balance").Value),
                                                           budget_allocation = Convert.ToString(info.Element("budget_allocation").Value),
                                                           division = Convert.ToString(info.Element("division").Value),
                                                           id = Convert.ToString(info.Element("id").Value),
                                                           pap_code = Convert.ToString(info.Element("pap_code").Value),
                                                           total_program_budget = Convert.ToString(info.Element("total_program_budget").Value)
                                                         
                                                       };


                    List<OfficeTotals> x_data = new List<OfficeTotals>();
                 
                    double _TotalAllocation = 0.00;
                    double _TotalExpense = 0.00;
                    double _TotalBalance = 0.00;

                    foreach (var item in _dataFetchOfficeTotals)
                    {
                        OfficeTotals _varData = new OfficeTotals();
                        double _TotalDiv = 0.00;
                        double _Balance = 0.00;
                        List<OfficeDetails> x_detail = new List<OfficeDetails>();

                     
                       
                        _varData.budget_allocation = "-";
                        _varData.division = item.division;
                        //var x_expense = ExpenseItemData.Where(items => items.division_id == item.id).ToList();
                        //foreach (var itemex in x_expense)
                        //{
                        //    _TotalDiv += Convert.ToDouble(itemex.total);
                        //     _TotalExpense += Convert.ToDouble(itemex.total);
                        //}

                        _varData.id = item.id;
                        _varData.pap_code = item.pap_code;
                        _varData.total_program_budget = "-";
                        _varData.total_program_planned = "-";
                        _Balance =Convert.ToDouble(item.budget_allocation) - _TotalDiv;
                        _varData.balance = "-";

                        var _datavalue = BalanceDetails.Where(_datcomp => _datcomp.division_id == item.id).ToList();

                        if (_datavalue.Count!=0)
                        {
                            foreach (var itemss in _datavalue)
                            {
                                OfficeDetails _varItems = new OfficeDetails();
                                _TotalAllocation += Convert.ToDouble(itemss.budget_allocation);

                                _varItems.budget_allocation = Convert.ToDouble(itemss.budget_allocation).ToString("#,##0.00"); 
                                _varItems.division_id = itemss.division_id;
                                _varItems.Fund_Source = itemss.Fund_Source;
                               

                                var _bb_data = BBDetails.Where(bbitem => bbitem.Division == itemss.Fund_Source).ToList();
                                var _total_fund_source = ExpenseItemData.Where(eid => eid.fund_source_id == itemss.fund_source_id).ToList();
                                double _totalBudget = 0.00;
                                foreach (var itemBud in _total_fund_source)
                                {
                                    _totalBudget += Convert.ToDouble(itemBud.total);
                                    _TotalExpense += _totalBudget;
                                }
                                _varItems.total_program_planned = _totalBudget.ToString("#,##0.00");
                                if (_bb_data.Count!=0)
                                {
                                    foreach (var itembb in _bb_data)
                                    {
                                        _varItems.total_program_budget = Convert.ToDouble(itembb.Total).ToString("#,##0.00");
                                        _TotalExpense += Convert.ToDouble(itembb.Total);
                                    }
                                }
                                else
                                {
                                    _varItems.total_program_budget = "0.00";
                                }
                              
                                _varItems.balance = (Convert.ToDouble( _varItems.budget_allocation) - (_totalBudget + Convert.ToDouble(_varItems.total_program_budget))).ToString("#,##0.00");
                                x_detail.Add(_varItems);

                            }

                        }

                        _varData.ListDetails = x_detail;
               
                        x_data.Add(_varData);

                    }
                 
                    txtTotalAllocation.Value = _TotalAllocation;
                    txtBudgetAmount.Value = _TotalExpense;
                    txtRemainingBalance.Value = _TotalAllocation - _TotalExpense;

                    grdDivisionData.ItemsSource = null;
                    grdDivisionData.ItemsSource = x_data;
                    try
                    {

                   
                    grdDivisionData.Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdDivisionData.Columns["pap_code"].Visibility = System.Windows.Visibility.Collapsed;
                    Column col_div = grdDivisionData.Columns.DataColumns["division"];
                    col_div.Width = new ColumnWidth(300, false);
                    col_div.IsFixed = FixedState.Left;
                    Column col_ba = grdDivisionData.Columns.DataColumns["budget_allocation"];
                    col_ba.HeaderText = "Allocated";

                    Column col_tpp = grdDivisionData.Columns.DataColumns["total_program_planned"];
                    col_tpp.HeaderText = "Total Expenditures";
                    Column col_tpb = grdDivisionData.Columns.DataColumns["total_program_budget"];
                    col_tpb.HeaderText = "Total OBR Encoded";
                    Column col_bal = grdDivisionData.Columns.DataColumns["balance"];
                    col_bal.HeaderText = "Total Remaining Balance";
            
                    foreach (var item in grdDivisionData.Rows)
                    {
                        if (item.HasChildren)
                        {
                            item.ChildBands[0].ColumnLayout.HeaderVisibility = Visibility.Collapsed;
                            Column col_child = item.ChildBands[0].Columns.DataColumns["Fund_Source"];
                            col_child.Width = new ColumnWidth(300, false);
                            col_child.IsFixed = FixedState.Left;
                            item.IsExpanded = true;

                            Column col_childid = item.ChildBands[0].Columns.DataColumns["division_id"];
                            col_childid.Visibility = System.Windows.Visibility.Collapsed;

                            Column col_fd = item.ChildBands[0].Columns.DataColumns["fund_source_id"];
                            col_fd.Visibility = System.Windows.Visibility.Collapsed;

                            Column col_pap = item.ChildBands[0].Columns.DataColumns["pap_code"];
                            col_pap.Visibility = System.Windows.Visibility.Collapsed;
                        }
                            
                    }
                    }
                    catch (Exception)
                    {


                    }
                    this.Cursor = Cursors.Arrow;
                    
                    break;
            }
          
        }

        private void GenerateData() 
        {
            double _JanTotal = 0.00;
            double _FebTotal = 0.00;
            double _MarTotal = 0.00;
            double _AprTotal = 0.00;
            double _MayTotal = 0.00;
            double _JunTotal = 0.00;
            double _JulTotal = 0.00;
            double _AugTotal = 0.00;
            double _SepTotal = 0.00;
            double _OctTotal = 0.00;
            double _NovTotal = 0.00;
            double _DecTotal = 0.00;
            double _LineTotal = 0.00;
            double _AllTotal = 0.00;
            double _AllBalance = 0.00;
            double _Allocation =0.00;

            foreach (var itemDiv in DivisionData)
            {
                List<ProgramLevel> _ProgData = new List<ProgramLevel>();
                List<ExpenseItemsList> x_prog = ExpenseItemData.Where(itemexpense => itemexpense.division_id == itemDiv.id).ToList();

                foreach (var itemprog in x_prog)
	            {

		 
                            if (x_prog.Count!=0)
                            {
                                var x_find = _ProgData.Where(x => x.id == itemprog.program_id).ToList();
                                if (x_find.Count==0)
                                {
                                    ProgramLevel x_prog_data = new ProgramLevel();
                                    x_prog_data.id = itemprog.program_id;
                                    x_prog_data.Program ="Program : " + @""" " +  itemprog.program + @""" ";

                                    _ProgData.Add(x_prog_data);
                                }

                                foreach (var itemprogdata in _ProgData)
                                {
                                    List<ExpenditureTitles> _ExpenditureTitles = new List<ExpenditureTitles>();
                                    List<ExpenseItemsList> x_data = ExpenseItemData.Where(itemexpense => itemexpense.division_id == itemDiv.id && itemexpense.program_id == itemprogdata.id).ToList();

                                    if (x_data.Count != 0)
                                    {
                                        foreach (var itemList in x_data)
                                        {
                                            
                                            var y_data = _ExpenditureTitles.Where(itm => itm.Expenditures == itemList.expenditure_name).ToList();
                                            if (y_data.Count==0)
                                            {
                                                ExpenditureTitles x_titles = new ExpenditureTitles();
                                                x_titles.Code = itemList.mooe_id;
                                                x_titles.Expenditures = itemList.expenditure_name;

                                                _ExpenditureTitles.Add(x_titles);
                                            }
                                           
                                        }

                                        foreach (var itemExpense in _ExpenditureTitles)
                                        {
                                            List<ExpenseItems> x_items = new List<ExpenseItems>();
                                            List<ExpenseItemsList> x_expense = ExpenseItemData.Where(itemexp => itemexp.division_id == itemDiv.id && itemexp.expen_code == itemExpense.Code && itemexp.program_id == itemprogdata.id).ToList();
                                            if (x_expense.Count != 0)
                                            {
                                                var results = from p in x_expense
                                                              group p by p.expense_name into g
                                                              select new {
                                                                  ExpenseName = g.Key, ExpenseItems = g.Select(m => m.expense_name)
                                                              };
                                                ExpenseItems _var = null;

                                                foreach (var item in results)
                                                {
                                                    _var = new ExpenseItems();
                                                    _var.ExpenseType = item.ExpenseName;
                                               //     _var.Alloted = "0.00";
                                                    _var.Apr = "0.00";
                                                    _var.Aug = "0.00";
                                                    _var.Dec = "0.00";
                                                    _var.Feb = "0.00";
                                                    _var.Jan = "0.00";
                                                    _var.Jul = "0.00";
                                                    _var.Jun = "0.00";
                                                    _var.Mar = "0.00";
                                                    _var.May = "0.00";
                                                    _var.Nov = "0.00";
                                                    _var.Oct = "0.00";
                                                    _var.Sep = "0.00";
                                                    _var.Total = "0.00";
                                                //    _var.Balance = "0.00";
                                                    _LineTotal = 0.00;

                                                    List<ExpenseItemsList> x_expense_data = x_expense.Where(itemsData => itemsData.expense_name == item.ExpenseName).ToList();

                                                    foreach (var item_data in x_expense_data)
                                                    {

                                                        //  _var.ExpenseType = item_data.expense_name;
                                                        _var.UACS = item_data.uacs_code;



                                                        switch (item_data.month)
                                                        {
                                                            case "Jan":
                                                                double _valJan = Convert.ToDouble(item_data.total);
                                                                _JanTotal += Convert.ToDouble(item_data.total);
                                                                _LineTotal += _valJan;
                                                                if (item_data.status == "1")
                                                                {
                                                                    _JanTotal -= _valJan;
                                                                    _valJan = 0;

                                                                }
                                                                else
                                                                {
                                                                    if (Convert.ToDouble(_var.Jan) < 0)
                                                                    {
                                                                        _var.Jan = 0.ToString("#,##0.00");
                                                                    }


                                                                }
                                                                _var.Jan = (Convert.ToDouble(_var.Jan) + _valJan).ToString("#,##0.00");
                                                                break;
                                                            case "Feb":
                                                                double _valFeb = Convert.ToDouble(item_data.total);
                                                                _FebTotal += Convert.ToDouble(item_data.total);
                                                                _LineTotal += _valFeb;
                                                                if (item_data.status == "1")
                                                                {
                                                                    _FebTotal -= _valFeb;
                                                                    _valFeb = 0;
                                                                }
                                                                else
                                                                {
                                                                    if (Convert.ToDouble(_var.Feb) < 0)
                                                                    {
                                                                        _var.Feb = 0.ToString("#,##0.00");
                                                                    }

                                                                }
                                                                _var.Feb = (Convert.ToDouble(_var.Feb) + _valFeb).ToString("#,##0.00");
                                                                break;
                                                            case "Mar":
                                                                double _valMar = Convert.ToDouble(item_data.total);
                                                                _MarTotal += Convert.ToDouble(item_data.total);
                                                                _LineTotal += _valMar;
                                                                if (item_data.status == "1")
                                                                {
                                                                    _MarTotal -= _valMar;
                                                                    _valMar = 0;
                                                                }
                                                                else
                                                                {
                                                                    if (Convert.ToDouble(_var.Mar) < 0)
                                                                    {
                                                                        _var.Mar = 0.ToString("#,##0.00");
                                                                    }
                                                                }
                                                                _var.Mar = (Convert.ToDouble(_var.Mar) + _valMar).ToString("#,##0.00");
                                                                break;
                                                            case "Apr":
                                                                double _valApr = Convert.ToDouble(item_data.total);
                                                                _AprTotal += Convert.ToDouble(item_data.total);
                                                                _LineTotal += _valApr;
                                                                if (item_data.status == "1")
                                                                {
                                                                    _AprTotal -= _valApr;
                                                                    _valApr = 0;
                                                                }
                                                                else
                                                                {
                                                                    if (Convert.ToDouble(_var.Apr) < 0)
                                                                    {
                                                                        _var.Apr = 0.ToString("#,##0.00");
                                                                    }
                                                                }
                                                                _var.Apr = (Convert.ToDouble(_var.Apr) + _valApr).ToString("#,##0.00");
                                                                break;
                                                            case "May":
                                                                double _valMay = Convert.ToDouble(item_data.total);
                                                                _MayTotal += Convert.ToDouble(item_data.total);
                                                                _LineTotal += _valMay;
                                                                if (item_data.status == "1")
                                                                {
                                                                    _MayTotal -= _valMay;
                                                                    _valMay = 0;
                                                                }
                                                                else
                                                                {
                                                                    if (Convert.ToDouble(_var.May) < 0)
                                                                    {
                                                                        _var.May = 0.ToString("#,##0.00");
                                                                    }
                                                                }
                                                                _var.May = (Convert.ToDouble(_var.May) + _valMay).ToString("#,##0.00");
                                                                break;
                                                            case "Jun":
                                                                double _valJun = Convert.ToDouble(item_data.total);
                                                                _JunTotal += Convert.ToDouble(item_data.total);
                                                                _LineTotal += _valJun;
                                                                if (item_data.status == "1")
                                                                {
                                                                    _JunTotal -= _valJun;
                                                                    _valJun = 0;
                                                                }
                                                                else
                                                                {
                                                                    if (Convert.ToDouble(_var.Jun) < 0)
                                                                    {
                                                                        _var.Jun = 0.ToString("#,##0.00");
                                                                    }
                                                                }
                                                                _var.Jun = (Convert.ToDouble(_var.Jun) + _valJun).ToString("#,##0.00");
                                                                break;
                                                            case "Jul":
                                                                double _valJul = Convert.ToDouble(item_data.total);
                                                                _JulTotal += Convert.ToDouble(item_data.total);
                                                                _LineTotal += _valJul;
                                                                if (item_data.status == "1")
                                                                {
                                                                    _JulTotal -= _valJul;
                                                                    _valJul = 0;
                                                                }
                                                                else
                                                                {
                                                                    if (Convert.ToDouble(_var.Jul) < 0)
                                                                    {
                                                                        _var.Jul = 0.ToString("#,##0.00");
                                                                    }
                                                                }
                                                                _var.Jul = (Convert.ToDouble(_var.Jul) + _valJul).ToString("#,##0.00");
                                                                break;

                                                            case "Aug":
                                                                double _valAug = Convert.ToDouble(item_data.total);
                                                                _AugTotal += Convert.ToDouble(item_data.total);
                                                                _LineTotal += _valAug;
                                                                if (item_data.status == "1")
                                                                {
                                                                    _AugTotal -= _valAug;
                                                                    _valAug = 0;
                                                                }
                                                                else
                                                                {
                                                                    if (Convert.ToDouble(_var.Aug) < 0)
                                                                    {
                                                                        _var.Aug = 0.ToString("#,##0.00");
                                                                    }
                                                                }
                                                                _var.Aug = (Convert.ToDouble(_var.Aug) + _valAug).ToString("#,##0.00");
                                                                break;
                                                            case "Sep":
                                                                double _valSep = Convert.ToDouble(item_data.total);
                                                                _SepTotal += Convert.ToDouble(item_data.total);
                                                                _LineTotal += _valSep;
                                                                if (item_data.status == "1")
                                                                {
                                                                    _SepTotal -= _valSep;
                                                                    _valSep = 0;
                                                                }
                                                                else
                                                                {
                                                                    if (Convert.ToDouble(_var.Sep) < 0)
                                                                    {
                                                                        _var.Sep = 0.ToString("#,##0.00");
                                                                    }
                                                                }
                                                                _var.Sep = (Convert.ToDouble(_var.Sep) + _valSep).ToString("#,##0.00");
                                                                break;
                                                            case "Oct":
                                                                double _valOct = Convert.ToDouble(item_data.total);
                                                                _OctTotal += Convert.ToDouble(item_data.total);
                                                                _LineTotal += _valOct;
                                                                if (item_data.status == "1")
                                                                {
                                                                    _OctTotal -= _valOct;
                                                                    _valOct = 0;
                                                                }
                                                                else
                                                                {
                                                                    if (Convert.ToDouble(_var.Oct) < 0)
                                                                    {
                                                                        _var.Oct = 0.ToString("#,##0.00");
                                                                    }
                                                                }
                                                                _var.Oct = (Convert.ToDouble(_var.Oct) + _valOct).ToString("#,##0.00");
                                                                break;
                                                            case "Nov":
                                                                double _valNov = Convert.ToDouble(item_data.total);
                                                                _NovTotal += Convert.ToDouble(item_data.total);
                                                                _LineTotal += _valNov;
                                                                if (item_data.status == "1")
                                                                {
                                                                    _NovTotal -= _valNov;
                                                                    _valNov = 0;
                                                                }
                                                                else
                                                                {
                                                                    if (Convert.ToDouble(_var.Nov) < 0)
                                                                    {
                                                                        _var.Nov = 0.ToString("#,##0.00");
                                                                    }
                                                                }
                                                                _var.Nov = (Convert.ToDouble(_var.Nov) + _valNov).ToString("#,##0.00");
                                                                break;
                                                            case "Dec":
                                                                double _valDec = Convert.ToDouble(item_data.total);
                                                                _DecTotal += Convert.ToDouble(item_data.total);
                                                                _LineTotal += _valDec;
                                                                if (item_data.status == "1")
                                                                {
                                                                    _DecTotal -= _valDec;
                                                                    _valDec = 0;
                                                                }
                                                                else
                                                                {
                                                                    if (Convert.ToDouble(_var.Dec) < 0)
                                                                    {
                                                                        _var.Dec = 0.ToString("#,##0.00");
                                                                    }
                                                                }
                                                                _var.Dec = (Convert.ToDouble(_var.Dec) + _valDec).ToString("#,##0.00");
                                                                break;

                                                        }
                                                        _var.Total = _LineTotal.ToString("#,##0.00");
                                                     //   _var.Balance = "0.00";

                                                    }
                                                    x_items.Add(_var);
                                                }

                                               
                                               
                                            }
                                            itemExpense.ExpenseList = x_items;
                                          
                                        }
                                       
                                        itemprogdata.Expenditures = _ExpenditureTitles;
                                    }
                                    //item.Expenditures = _ExpenditureTitles;
                                }

                              
               
                            }

	            }
                itemDiv.Programs = _ProgData;
            }

             _JanTotal = 0.00;
             _FebTotal = 0.00;
             _MarTotal = 0.00;
             _AprTotal = 0.00;
             _MayTotal = 0.00;
             _JunTotal = 0.00;
             _JulTotal = 0.00;
             _AugTotal = 0.00;
             _SepTotal = 0.00;
             _OctTotal = 0.00;
             _NovTotal = 0.00;
             _DecTotal = 0.00;
             _LineTotal = 0.00;
             _AllTotal = 0.00;
             _AllBalance = 0.00;

            foreach (var item in DivisionData)
            {
                foreach (var itemdetails in item.Programs)
                {
                    foreach (var itemExp in itemdetails.Expenditures)
                    {
                        foreach (var itemexpList in itemExp.ExpenseList)
                        {
                            _JanTotal += Convert.ToDouble(itemexpList.Jan);
                            _FebTotal += Convert.ToDouble(itemexpList.Feb);
                            _MarTotal += Convert.ToDouble(itemexpList.Mar);
                            _AprTotal += Convert.ToDouble(itemexpList.Apr);
                            _MayTotal += Convert.ToDouble(itemexpList.May);
                            _JunTotal += Convert.ToDouble(itemexpList.Jun);
                            _JulTotal += Convert.ToDouble(itemexpList.Jul);
                            _AugTotal += Convert.ToDouble(itemexpList.Aug);
                            _SepTotal += Convert.ToDouble(itemexpList.Sep);
                            _OctTotal += Convert.ToDouble(itemexpList.Oct);
                            _NovTotal += Convert.ToDouble(itemexpList.Nov);
                            _DecTotal += Convert.ToDouble(itemexpList.Dec);
                            _TotalAmount += Convert.ToDouble(itemexpList.Total);
                        
                        }
                    }
                }
            }


            List<OfficeLevel> _Totals = new List<OfficeLevel>();
            OfficeLevel _totalValue = new OfficeLevel();

            _totalValue.Office = "Total Amount";
           // _totalValue.Alloted = "0.00";
            _totalValue.Jan = _JanTotal.ToString("#,##0.00");
            _totalValue.Feb = _FebTotal.ToString("#,##0.00");
            _totalValue.Mar = _MarTotal.ToString("#,##0.00");
            _totalValue.Apr = _AprTotal.ToString("#,##0.00");
            _totalValue.May = _MayTotal.ToString("#,##0.00");
            _totalValue.Jun = _JunTotal.ToString("#,##0.00");
            _totalValue.Jul = _JulTotal.ToString("#,##0.00");
            _totalValue.Aug = _AugTotal.ToString("#,##0.00");
            _totalValue.Sep = _SepTotal.ToString("#,##0.00");
            _totalValue.Oct = _OctTotal.ToString("#,##0.00");
            _totalValue.Nov = _NovTotal.ToString("#,##0.00");
            _totalValue.Dec = _DecTotal.ToString("#,##0.00");
            _totalValue.Total = _TotalAmount.ToString("#,##0.00");
         //   _totalValue.Balance = "0.00";

            _Totals.Add(_totalValue);

          
            OfficeData[0].DivisionList = DivisionData;

            grdData.ItemsSource = null;
            grdData.ItemsSource = OfficeData;

            grdTotals.ItemsSource = null;
            grdTotals.ItemsSource = _Totals;
          
            double _widthdefault = 350;
            grdData.Columns.DataColumns["Pap"].Visibility = System.Windows.Visibility.Collapsed;
            Column col_project_name = grdData.Columns.DataColumns["Office"];
            col_project_name.Width = new ColumnWidth(_widthdefault, false);
            col_project_name.IsFixed = FixedState.Left;

            Column col_totals = grdTotals.Columns.DataColumns["Office"];
            col_totals.Width = new ColumnWidth(_widthdefault, false);
            col_totals.IsFixed = FixedState.Left;
            grdTotals.ColumnLayouts[0].HeaderVisibility = Visibility.Collapsed;
            grdTotals.Columns.DataColumns["Pap"].Visibility = System.Windows.Visibility.Collapsed;

            Column col_uacs = grdData.Columns.DataColumns["UACS"];
            Column col_jan = grdData.Columns.DataColumns["Jan"];
            Column col_feb = grdData.Columns.DataColumns["Feb"];
            Column col_mar = grdData.Columns.DataColumns["Mar"];
            Column col_apr = grdData.Columns.DataColumns["Apr"];
            Column col_may = grdData.Columns.DataColumns["May"];
            Column col_jun = grdData.Columns.DataColumns["Jun"];
            Column col_jul = grdData.Columns.DataColumns["Jul"];
            Column col_aug = grdData.Columns.DataColumns["Aug"];
            Column col_sep = grdData.Columns.DataColumns["Sep"];
            Column col_oct = grdData.Columns.DataColumns["Oct"];
            Column col_nov = grdData.Columns.DataColumns["Nov"];
            Column col_dec = grdData.Columns.DataColumns["Dec"];

            col_uacs.Width = new ColumnWidth(100, false);
            col_jan.Width = new ColumnWidth(100, false);
            col_feb.Width = new ColumnWidth(100, false);
            col_mar.Width = new ColumnWidth(100, false);
            col_apr.Width = new ColumnWidth(100, false);
            col_may.Width = new ColumnWidth(100, false);
            col_jun.Width = new ColumnWidth(100, false);
            col_jul.Width = new ColumnWidth(100, false);
            col_aug.Width = new ColumnWidth(100, false);
            col_sep.Width = new ColumnWidth(100, false);
            col_oct.Width = new ColumnWidth(100, false);
            col_nov.Width = new ColumnWidth(100, false);
            col_dec.Width = new ColumnWidth(100, false);

            foreach (var item in grdData.Rows)
            {
                if (item.HasChildren)
                {
                    Column col_uacs_1 = item.ChildBands[0].Columns.DataColumns["UACS"];
                    Column col_jan_1 = item.ChildBands[0].Columns.DataColumns["Jan"];
                    Column col_feb_1 = item.ChildBands[0].Columns.DataColumns["Feb"];
                    Column col_mar_1 = item.ChildBands[0].Columns.DataColumns["Mar"];
                    Column col_apr_1 = item.ChildBands[0].Columns.DataColumns["Apr"];
                    Column col_may_1 = item.ChildBands[0].Columns.DataColumns["May"];
                    Column col_jun_1 = item.ChildBands[0].Columns.DataColumns["Jun"];
                    Column col_jul_1 = item.ChildBands[0].Columns.DataColumns["Jul"];
                    Column col_aug_1 = item.ChildBands[0].Columns.DataColumns["Aug"];
                    Column col_sep_1 = item.ChildBands[0].Columns.DataColumns["Sep"];
                    Column col_oct_1 = item.ChildBands[0].Columns.DataColumns["Oct"];
                    Column col_nov_1 = item.ChildBands[0].Columns.DataColumns["Nov"];
                    Column col_dec_1 = item.ChildBands[0].Columns.DataColumns["Dec"];

                    Double col_def_w = 100;
                    col_uacs_1.Width = new ColumnWidth(col_def_w, false);
                    col_jan_1.Width = new ColumnWidth(col_def_w, false);
                    col_feb_1.Width = new ColumnWidth(col_def_w, false);
                    col_mar_1.Width = new ColumnWidth(col_def_w, false);
                    col_apr_1.Width = new ColumnWidth(col_def_w, false);
                    col_may_1.Width = new ColumnWidth(col_def_w, false);
                    col_jun_1.Width = new ColumnWidth(col_def_w, false);
                    col_jul_1.Width = new ColumnWidth(col_def_w, false);
                    col_aug_1.Width = new ColumnWidth(col_def_w, false);
                    col_sep_1.Width = new ColumnWidth(col_def_w, false);
                    col_oct_1.Width = new ColumnWidth(col_def_w, false);
                    col_nov_1.Width = new ColumnWidth(col_def_w, false);
                    col_dec_1.Width = new ColumnWidth(col_def_w, false);

                    Column col_division = item.ChildBands[0].Columns.DataColumns["Division"];
                   col_division.IsFixed = FixedState.Left;
                    double _orig_w = Convert.ToDouble(col_division.WidthResolved.Value);
                    double _precise = _widthdefault - _orig_w;

                    col_division.Width = new ColumnWidth((_orig_w + _precise) -30, false);


                   item.ChildBands[0].Columns["id"].Visibility= System.Windows.Visibility.Collapsed;
                   
                    item.IsExpanded = true;
                    item.ChildBands[0].ColumnLayout.HeaderVisibility = Visibility.Collapsed;
                    try
                    {
                        foreach (var itemLevel1 in item.ChildBands[0].Rows)
                        {

                            if (itemLevel1.HasChildren)
                            {
                                Column col_uacs_2 = itemLevel1.ChildBands[0].Columns.DataColumns["UACS"];
                                Column col_jan_2 = itemLevel1.ChildBands[0].Columns.DataColumns["Jan"];
                                Column col_feb_2 = itemLevel1.ChildBands[0].Columns.DataColumns["Feb"];
                                Column col_mar_2 = itemLevel1.ChildBands[0].Columns.DataColumns["Mar"];
                                Column col_apr_2 = itemLevel1.ChildBands[0].Columns.DataColumns["Apr"];
                                Column col_may_2 = itemLevel1.ChildBands[0].Columns.DataColumns["May"];
                                Column col_jun_2 = itemLevel1.ChildBands[0].Columns.DataColumns["Jun"];
                                Column col_jul_2 = itemLevel1.ChildBands[0].Columns.DataColumns["Jul"];
                                Column col_aug_2 = itemLevel1.ChildBands[0].Columns.DataColumns["Aug"];
                                Column col_sep_2 = itemLevel1.ChildBands[0].Columns.DataColumns["Sep"];
                                Column col_oct_2 = itemLevel1.ChildBands[0].Columns.DataColumns["Oct"];
                                Column col_nov_2 = itemLevel1.ChildBands[0].Columns.DataColumns["Nov"];
                                Column col_dec_2 = itemLevel1.ChildBands[0].Columns.DataColumns["Dec"];

                                Double col_def_w_2 = 100;

                                col_uacs_2.Width = new ColumnWidth(col_def_w_2, false);
                                col_jan_2.Width = new ColumnWidth(col_def_w_2, false);
                                col_feb_2.Width = new ColumnWidth(col_def_w_2, false);
                                col_mar_2.Width = new ColumnWidth(col_def_w_2, false);
                                col_apr_2.Width = new ColumnWidth(col_def_w_2, false);
                                col_may_2.Width = new ColumnWidth(col_def_w_2, false);
                                col_jun_2.Width = new ColumnWidth(col_def_w_2, false);
                                col_jul_2.Width = new ColumnWidth(col_def_w_2, false);
                                col_aug_2.Width = new ColumnWidth(col_def_w_2, false);
                                col_sep_2.Width = new ColumnWidth(col_def_w_2, false);
                                col_oct_2.Width = new ColumnWidth(col_def_w_2, false);
                                col_nov_2.Width = new ColumnWidth(col_def_w_2, false);
                                col_dec_2.Width = new ColumnWidth(col_def_w_2, false);


                                Column col_exp = itemLevel1.ChildBands[0].Columns.DataColumns["Program"];
                                col_exp.IsFixed = FixedState.Left;

                                col_exp.Width = new ColumnWidth(290, false);

                      

                               // itemLevel1.Cells["Program"].Control.Background = Colors.DarkGray;


                                itemLevel1.ChildBands[0].Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
                                itemLevel1.IsExpanded = true;
                                itemLevel1.ChildBands[0].ColumnLayout.HeaderVisibility = Visibility.Collapsed;
                                foreach (var itemLevel2 in itemLevel1.ChildBands[0].Rows)
                                {

                                    if (itemLevel2.HasChildren)
                                    {
                                        Column col_uacs_3 = itemLevel2.ChildBands[0].Columns.DataColumns["UACS"];
                                        Column col_jan_3 = itemLevel2.ChildBands[0].Columns.DataColumns["Jan"];
                                        Column col_feb_3 = itemLevel2.ChildBands[0].Columns.DataColumns["Feb"];
                                        Column col_mar_3 = itemLevel2.ChildBands[0].Columns.DataColumns["Mar"];
                                        Column col_apr_3 = itemLevel2.ChildBands[0].Columns.DataColumns["Apr"];
                                        Column col_may_3 = itemLevel2.ChildBands[0].Columns.DataColumns["May"];
                                        Column col_jun_3 = itemLevel2.ChildBands[0].Columns.DataColumns["Jun"];
                                        Column col_jul_3 = itemLevel2.ChildBands[0].Columns.DataColumns["Jul"];
                                        Column col_aug_3 = itemLevel2.ChildBands[0].Columns.DataColumns["Aug"];
                                        Column col_sep_3 = itemLevel2.ChildBands[0].Columns.DataColumns["Sep"];
                                        Column col_oct_3 = itemLevel2.ChildBands[0].Columns.DataColumns["Oct"];
                                        Column col_nov_3 = itemLevel2.ChildBands[0].Columns.DataColumns["Nov"];
                                        Column col_dec_3 = itemLevel2.ChildBands[0].Columns.DataColumns["Dec"];

                                        Double col_def_w_3 = 100;

                                        col_uacs_3.Width = new ColumnWidth(col_def_w_3, false);
                                        col_jan_3.Width = new ColumnWidth(col_def_w_3, false);
                                        col_feb_3.Width = new ColumnWidth(col_def_w_3, false);
                                        col_mar_3.Width = new ColumnWidth(col_def_w_3, false);
                                        col_apr_3.Width = new ColumnWidth(col_def_w_3, false);
                                        col_may_3.Width = new ColumnWidth(col_def_w_3, false);
                                        col_jun_3.Width = new ColumnWidth(col_def_w_3, false);
                                        col_jul_3.Width = new ColumnWidth(col_def_w_3, false);
                                        col_aug_3.Width = new ColumnWidth(col_def_w_3, false);
                                        col_sep_3.Width = new ColumnWidth(col_def_w_3, false);
                                        col_oct_3.Width = new ColumnWidth(col_def_w_3, false);
                                        col_nov_3.Width = new ColumnWidth(col_def_w_3, false);
                                        col_dec_3.Width = new ColumnWidth(col_def_w_3, false);

                                        Column col_exp_type = itemLevel2.ChildBands[0].Columns.DataColumns["Expenditures"];

                                        col_exp_type.IsFixed = FixedState.Left;
                                        col_exp_type.Width = new ColumnWidth(260, false);

                                        itemLevel2.ChildBands[0].Columns["Code"].Visibility = System.Windows.Visibility.Collapsed;
                                    //    itemLevel2.IsExpanded = true;
                                        itemLevel2.ChildBands[0].ColumnLayout.HeaderVisibility = Visibility.Collapsed;

                                        foreach (var itemLevel3 in itemLevel2.ChildBands[0].Rows)
                                        {
                                            Column col_ = itemLevel3.ChildBands[0].Columns.DataColumns[1];

                                              col_.IsFixed = FixedState.Left;
                                              col_.Width = new ColumnWidth(260, false);

                                              itemLevel3.ChildBands[0].Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
                                              itemLevel3.IsExpanded = true;
                                              itemLevel3.ChildBands[0].ColumnLayout.HeaderVisibility = Visibility.Collapsed;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {
                        
                    }
                   
                }
                break;
            }
            FetchOfficeDetails();
        }

        private void FetchOfficeData() 
        {
            c_office_dash.Process = "FetchOffice";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchOffice(OfficeID));   
        }

        private void FetchOfficeDetails()
        {
            c_office_dash.Process = "FetchOfficeDetails";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchOfficeDetails( OfficeID, cmbYear.SelectedItem.ToString()));
        }
        private void FetchBalances()
        {
            c_office_dash.Process = "FetchBalances";
            svc_mindaf.ExecuteImportDataSQLAsync(c_office_dash.FetchBalances(PAPCODE, cmbYear.SelectedItem.ToString()));
        }
        private void FetchDivisionData()
        {
            c_office_dash.Process = "FetchDivisions";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchDivision(OfficeID));
        }
        private void FetchOfficeTotals()
        {
            c_office_dash.Process = "FetchOfficeTotals";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchOfficeTotals(OfficeID,cmbYear.SelectedItem.ToString()));
        }
        private void FetchBudgetAllocation()
        {

            c_office_dash.Process = "FetchBudgetAllocation";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchBudgetAllocation(DivisionID, cmbYear.SelectedItem.ToString()));
        }

        private void FetchOfficeExpenditures()
        {
            c_office_dash.Process = "FetchOfficeExpenditures";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchOfficeExpenditures(OfficeID,cmbYear.SelectedItem.ToString()));
        }
       
        private void GenerateYear()
        {
            int _year = DateTime.Now.Year;
            int _limit = _year + 100;

            for (int i = _year; i != _limit; i++)
            {
                cmbYear.Items.Add(_year);
                _year += 1;
            }
            cmbYear.SelectedIndex = 0;
            this.SelectedYear = cmbYear.SelectedItem.ToString();
        }
        private bool firstLoad = true;
        private void usr_b_rep_Loaded(object sender, RoutedEventArgs e)
        {
            if (firstLoad ==false)
            {
                GenerateYear();
                FetchOfficeData();
                firstLoad = true;   
            }
            else
            {
                firstLoad = false;
            }
           
        }

        private void cmbYear_DropDownClosed(object sender, EventArgs e)
        {
            FetchOfficeData();
        }

       
    }
}
