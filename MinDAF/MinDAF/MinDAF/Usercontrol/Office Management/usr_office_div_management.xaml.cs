﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using MinDAF.Class;
using System.IO;
using System.Xml.Linq;
using MinDAF.MinDAFS;


namespace MinDAF.Usercontrol.Office_Management
{
    public partial class usr_office_div_management : UserControl
    {
        public event EventHandler CancelProcess;

        private clsOfficeDivision cls_office = new clsOfficeDivision();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private List<Office> OfficeDetails  = new List<Office>();

        public usr_office_div_management()
        {

            InitializeComponent();

            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
         
        }

        private void FetchDivisionData(String _id) 
        {
            cls_office.Process = "DivisionData";
            svc_mindaf.ExecuteSQLAsync("SELECT Division_Code as Code, Division_Desc as Division FROM mnda_division WHERE Office_Id='"+ _id +"'");

        }
        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            string _result = e.Result.ToString();

            switch (cls_office.Process)
            {
                case "OfficeData":
                    XDocument oDoc = XDocument.Parse(_result);
                    var _data = from info in oDoc.Descendants("Table")
                                 select new Office
                                 {
                                     Office_Id = Convert.ToString(info.Element("Office_Id").Value),
                                     Office_Code = Convert.ToString(info.Element("Office_Code").Value),
                                     Office_Desc = Convert.ToString(info.Element("Office_Desc").Value),
                        
                                 };


                    cmbOffice.Items.Clear();

                    List<String> ComboItems = new List<string>();
                    foreach (var item in _data)
                    {
                        Office _varDetails = new Office();

                        _varDetails.Office_Id = item.Office_Id;
                        _varDetails.Office_Code = item.Office_Code;
                        _varDetails.Office_Desc = item.Office_Desc;

                        ComboItems.Add(item.Office_Code);

                        OfficeDetails.Add(_varDetails);
                    }

                    cmbOffice.ItemsSource = ComboItems;
                    grdOffice.ItemsSource = OfficeDetails;

                    this.Cursor = Cursors.Arrow;
                    break;
                case "DivisionData":
                    XDocument oDocDivision = XDocument.Parse(_result);
                    var _dataDivision = from info in oDocDivision.Descendants("Table")
                                 select new Division
                                 {
                                    
                                      Division_Code = Convert.ToString(info.Element("Code").Value),
                                     Division_Desc = Convert.ToString(info.Element("Division").Value),
                        
                                 };


                    List<Division> DivisionDetails = new List<Division>();

                    foreach (var item in _dataDivision)
                    {
                        Division _varDetails = new Division();

                   
                        _varDetails.Division_Desc = item.Division_Desc;
                        _varDetails.Division_Code = item.Division_Code;



                        DivisionDetails.Add(_varDetails);
                    }


                    grdDivision.ItemsSource = DivisionDetails;

                    this.Cursor = Cursors.Arrow;
                    break;
            }
          

           

        }

        private void btnOffice_Click(object sender, RoutedEventArgs e)
        {
            cls_office.Process = "OfficeData";
            btnOffice.IsEnabled = false;
            btnDivision.IsEnabled = true;

            txtCode.IsEnabled = false;
            cmbOffice.IsEnabled = false;
            txtCeiling.IsEnabled = true;
            txtName.Focus();
            lblProcess.Content = "OFFICE PROCESS";
        }

        private void btnDivision_Click(object sender, RoutedEventArgs e)
        {
            cls_office.Process = "DivisionData";
            btnOffice.IsEnabled = true;
            btnDivision.IsEnabled = false;

            txtCode.IsEnabled = true;
            cmbOffice.IsEnabled = true;
            txtCeiling.IsEnabled = false;
            lblProcess.Content = "DIVISION PROCESS";
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            switch (cls_office.Process)
            {
                case "OfficeData":
                    SaveData();
                    break;
                case "DivisionData":
                    SaveDataDivision();
                    break;
            }
          
            
        }

        private void SaveData() 
        {
           
            cls_office.SaveData(txtName.Text);
            cls_office.DataLoaded+=cls_office_DataLoaded;
            ClearData();
            
        }
        private void  SaveDataDivision()
        {
            string _office_id = "";
            foreach (var item in OfficeDetails)
            {
                if (item.Office_Code ==cmbOffice.SelectedItem.ToString())
                {
                    _office_id = item.Office_Id;
                    break;
                }
            }
            cls_office.SaveOfficeBudget(_office_id, txtCode.Text,txtName.Text);
            cls_office.DataLoaded += cls_office_DataLoaded;
            ClearData();

        }
        private void usr_off_div_Loaded(object sender, RoutedEventArgs e)
        {
            cls_office.Process = "OfficeData";

            svc_mindaf.ExecuteSQLAsync("SELECT * FROM mnda_office");
          
        }

        void cls_office_DataLoaded(object sender, EventArgs e)
        {
            cls_office.Process = "OfficeData";

            svc_mindaf.ExecuteSQLAsync("SELECT * FROM mnda_office");
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            grdScreener.Visibility = System.Windows.Visibility.Visible;
        }

        private void ClearData() 
        {
            txtCeiling.Value = 0.00;
            txtCode.Text = "";
            txtName.Text = "";

        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            ClearData();
        }


      
        private void grdOffice_CellClicked(object sender, Infragistics.Controls.Grids.CellClickedEventArgs e)
        {
           
          var x = grdOffice.SelectionSettings.SelectedRows.ToList();
       
          FetchDivisionData(x[0].Cells[0].Value.ToString());

        }

     

        private void Image_MouseEnter_1(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void Image_MouseLeave_1(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            if (CancelProcess != null)
            {
                CancelProcess(this, new EventArgs());
            }
        }

        private void imgMenu_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            grdScreener.Visibility = System.Windows.Visibility.Collapsed;
        }

    }
    public class Office 
    {
        public String Office_Id { get; set; }
        public String Office_Code { get; set; }
        public String Office_Desc { get; set; }
    }
    public class Division 
    {
        public String Division_Code { get; set; }
        public String Division_Desc { get; set; }
    }

   
}
