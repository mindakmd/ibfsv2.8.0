﻿using MinDAF;
using ReportTool.MinDAF;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ReportTool.Class
{
    class clsReportFinance
    {
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        public List<DivisionList> DivisionData = new List<DivisionList>();
        public UserDetail UserInfo = new UserDetail();

        public DataSet FetchDivisionDistribution()
        {
            DataSet _dsData = new DataSet();
            DataTable dtHeader = new DataTable();

            var sb = new System.Text.StringBuilder(711);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mooe.code,");
            sb.AppendLine(@"mooe.name,");
            sb.AppendLine(@"msub.name as _expenditure,");
            sb.AppendLine(@"0.00 as _oc,");
            sb.AppendLine(@"0.00 as _oed,");
            sb.AppendLine(@"0.00 as _oed_iso,");
            sb.AppendLine(@"0.00 as _ad,");
            sb.AppendLine(@"0.00 as _ad_office_transfer,");
            sb.AppendLine(@"0.00 as _kmd_issp,");
            sb.AppendLine(@"0.00 as _fd,");
            sb.AppendLine(@"0.00 as _gms,");
            sb.AppendLine(@"0.00 as _prd,");
            sb.AppendLine(@"0.00 as _mdc,");
            sb.AppendLine(@"0.00 as _kmd,");
            sb.AppendLine(@"0.00 as _kmd_thematic_map,");
            sb.AppendLine(@"0.00 as _dpk,");
            sb.AppendLine(@"0.00 as _pfd,");
            sb.AppendLine(@"0.00 as _drpa,");
            sb.AppendLine(@"0.00 as _pdd,");
            sb.AppendLine(@"0.00 as _pdd_now,");
            sb.AppendLine(@"0.00 as _pdd_ppp,");
            sb.AppendLine(@"0.00 as _pdrg,");
            sb.AppendLine(@"0.00 as _amo,");
            sb.AppendLine(@"0.00 as _mirpp,");
            sb.AppendLine(@"0.00 as _ipd,");
            sb.AppendLine(@"0.00 as _ipd_cacao,");
            sb.AppendLine(@"0.00 as _purd,");
            sb.AppendLine(@"0.00 as _ipd_mpmc,");
            sb.AppendLine(@"0.00 as _ippr,");
            sb.AppendLine(@"0.00 as _ird,");
            sb.AppendLine(@"0.00 as _ird_eaga,");
            sb.AppendLine(@"0.00 as _ird_mct,");
            sb.AppendLine(@"0.00 as _mebeaga");
            sb.AppendLine(@"FROM mnda_mooe_expenditures mooe");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures msub on msub.mooe_id = mooe.code");
            sb.AppendLine(@"WHERE msub.is_active = 1");

            _dsData = new DataSet();
            _dsData = svc_mindaf.ExecuteSQLReportQuery(sb.ToString());
         
            dtHeader.TableName = "Header";


            return _dsData;
        }
        public DataSet FetchDataPPMP(string PAP, string _year)
        {
            DataSet _dsData = new DataSet();
            DataTable dtHeader = new DataTable();

            var sb = new System.Text.StringBuilder(300);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Uacs,");
            sb.AppendLine(@"  Description,");
            sb.AppendLine(@"  Quantity,");
            sb.AppendLine(@"  EstimateBudget,");
            sb.AppendLine(@"  ModeOfProcurement,");
            sb.AppendLine(@"  Jan,");
            sb.AppendLine(@"  Feb,");
            sb.AppendLine(@"  Mar,");
            sb.AppendLine(@"  Apr,");
            sb.AppendLine(@"  May,");
            sb.AppendLine(@"  Jun,");
            sb.AppendLine(@"  Jul,");
            sb.AppendLine(@"  Aug,");
            sb.AppendLine(@"  Sep,");
            sb.AppendLine(@"  Octs,");
            sb.AppendLine(@"  Nov,");
            sb.AppendLine(@"  Dec,");
            sb.AppendLine(@"  Total,");
            sb.AppendLine(@"  Division,");
            sb.AppendLine(@"  Yearssss,");
            sb.AppendLine(@"  PAP,");
            sb.AppendLine(@"  approved,");
            sb.AppendLine(@"  header,");
            sb.AppendLine(@"  revision");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_report_data_ppmp WHERE PAP = '"+ PAP +"' AND Yearssss ='"+ _year +"';");

            _dsData = new DataSet();
            _dsData = svc_mindaf.ExecuteSQLReportQuery(sb.ToString());

            dtHeader.TableName = "Header";


            return _dsData;
        }

        public DataSet FetchDataAPP(string PAP, string _year)
        {
            DataSet _dsData = new DataSet();
            DataTable dtHeader = new DataTable();

            var sb = new System.Text.StringBuilder(362);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@" app.code,");
            sb.AppendLine(@" app.procurement_project,");
            sb.AppendLine(@" app.pmo_end_user,");
            sb.AppendLine(@" app.mode_of_procurement,");
            sb.AppendLine(@" app.ads_post_rei,");
            sb.AppendLine(@" app.sub_open_bids,");
            sb.AppendLine(@" app.notice_award,");
            sb.AppendLine(@" app.contract_signing,");
            sb.AppendLine(@" app.source_fund,");
            sb.AppendLine(@" app.eb_total,");
            sb.AppendLine(@" app.eb_mooe,");
            sb.AppendLine(@" app.eb_co,");
            sb.AppendLine(@" app.remarks,");
            sb.AppendLine(@" app.pap_code,");
            sb.AppendLine(@" app.yearsss");
            sb.AppendLine(@"FROM mnda_report_data_app app");
            sb.AppendLine(@"WHERE app.pap_code ='"+ PAP +"' and app.yearsss ='"+ _year +"';");


            _dsData = new DataSet();
            _dsData = svc_mindaf.ExecuteSQLReportQuery(sb.ToString());

            dtHeader.TableName = "Header";


            return _dsData;
        }
        public Boolean LoginUser(String Password) 
        {
            DataTable dtData = new DataTable();
            var sb = new System.Text.StringBuilder(228);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  div.Division_Id,");
            sb.AppendLine(@"  div.Division_Code,");
            sb.AppendLine(@"  div.Division_Desc,");
            sb.AppendLine(@"  ua.Username,");
            sb.AppendLine(@"  ua.is_admin as UserLevel");
            sb.AppendLine(@"FROM mnda_user_accounts ua");
            sb.AppendLine(@"INNER JOIN Division div on div.Division_Id = ua.Division_Id");
            sb.AppendLine(@" WHERE Password='"+ Password +"'");


            dtData = svc_mindaf.ExecuteSQLReportQuery(sb.ToString()).Tables[0].Copy();
            if (dtData.Rows.Count!=0)
            {
                foreach (DataRow item in dtData.Rows)
                {
                    UserInfo = new UserDetail();

                    UserInfo.DivisionCode = item["Division_Code"].ToString();
                    UserInfo.DivisionId = item["Division_Id"].ToString();
                    UserInfo.DivisionName = item["Division_Desc"].ToString();
                    UserInfo.UserLevel = item["UserLevel"].ToString();
                    UserInfo.Username = item["Username"].ToString();
                    break;
                }
                return true;
            }else
	        {
                return false;
	        }
        }

        public DataTable FetchDivisions() 
        {
            DataTable dtData = new DataTable();
            var sb = new System.Text.StringBuilder(29);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"*");
            sb.AppendLine(@"FROM Division div");

            dtData = svc_mindaf.ExecuteSQLReportQuery(sb.ToString()).Tables[0].Copy();
            DivisionData.Clear();

            foreach (DataRow item in dtData.Rows)
            {
                DivisionList _vData = new DivisionList();
                _vData.DivisionId = item["Division_Id"].ToString();
                _vData.DivisionCode = item["Division_Code"].ToString();
                _vData.DivisionName = item["Division_Desc"].ToString();
                
                DivisionData.Add(_vData);
            }
            return dtData;
        }
    }

    public class DivisionList 
    {
        public String DivisionId { get; set; }
        public String DivisionCode { get; set; }
        public String DivisionName { get; set; }
        
    }

    public class UserDetail 
    {
        public String DivisionId { get; set; }
        public String DivisionCode { get; set; }
        public String DivisionName { get; set; }
        public String Username { get; set; }
        public String UserLevel { get; set; }
    }
}
