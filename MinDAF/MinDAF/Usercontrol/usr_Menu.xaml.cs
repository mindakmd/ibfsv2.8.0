﻿using MinDAF.Class;
using MinDAF.Forms;
using MinDAF.MinDAFS;
using MinDAF.Usercontrol.Budget_Allocation;
using MinDAF.Usercontrol.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol
{
    public partial class usr_Menu : UserControl
    {
        public String Division { get; set; }
        public String DivisionID { get; set; }
        public String OfficeID { get; set; }
        public String UserId { get; set; }
        public String User { get; set; }
        public String IsAdmin { get; set; }
        public String PAP { get; set; }
        public String FundSource { get; set; }

        public String WorkingYear { get; set; }

        public Boolean IsUnit { get; set; }

        private usr_budget_creation ctrl_budget = new usr_budget_creation();
        private usrCreateProject ctrl_CreateProject = new usrCreateProject();

        private BudgetApprovals ctrl_budget_approvals = new BudgetApprovals("");
        private usrCreateProject ctrl_create_project = new usrCreateProject();
        private usrCreateProgram ctrl_create_program = new usrCreateProgram();
        private ppmp_report_revision ctrl_ppmp_generate = new ppmp_report_revision();
        private usr_division_project_status ctrl_project_status = new usr_division_project_status();

        private usr_main_dashboard ctrl_main_dashboard = new usr_main_dashboard();

        private BudgetGraph ctrl_Graph = new BudgetGraph();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsMenu c_menu = new clsMenu();
        private List<MenuFundSource> f_source = new List<MenuFundSource>();

        public usr_Menu()
        {
            InitializeComponent();
            App.Current.Host.Content.Resized += Content_Resized;
            ctrl_budget_approvals = new BudgetApprovals(FundSource);
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            this.Loaded += usr_Menu_Loaded;
        }

        void usr_Menu_Loaded(object sender, RoutedEventArgs e)
        {
            FetchFundSource();
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            String _results = e.Result.ToString();

            switch (c_menu.Process)
            {
                case "FetchFundSource":
                    XDocument oDocKeyFetchGridData = XDocument.Parse(_results);
                    var _dataListsFetchGridData = from info in oDocKeyFetchGridData.Descendants("Table")
                                                  select new MenuFundSource
                                                  {
                                                      code = Convert.ToString(info.Element("code").Value),
                                                      respo_name = Convert.ToString(info.Element("respo_name").Value)
                                                    

                                                  };

                    f_source.Clear();
                    cmbFundSource.Items.Clear();

                    foreach (var item in _dataListsFetchGridData)
                    {
                        //if (item.respo_name.Contains("CONT"))
                        //{
                        //    continue;
                        //}
                        //else
                        //{
                            MenuFundSource _varDetails = new MenuFundSource();

                            _varDetails.code = item.code;
                            _varDetails.respo_name = item.respo_name;

                            cmbFundSource.Items.Add(item.respo_name);

                            f_source.Add(_varDetails);
                        //}
                       


                    }
                    cmbFundSource.SelectedIndex = 0;

                    LoadData();

                    break;


            }
        }

        private void LoadData() 
        {

            lblDivision.Content = Division;
            lblUser.Content = User;
            ctrl_budget.DivisionID = DivisionID;
            ctrl_Graph.DivisionID = DivisionID;

            ctrl_create_program.Width = stkChild.Width;
            ctrl_create_program.Height = stkChild.Height;

            ctrl_ppmp_generate.Width = stkChild.Width;
            ctrl_ppmp_generate.Height = stkChild.Height;

            ctrl_project_status.Width = stkChild.Width;
            ctrl_project_status.Height = stkChild.Height;

            ctrl_main_dashboard.Width = stkChild.Width;
            ctrl_main_dashboard.Height = stkChild.Height;

            ctrl_main_dashboard.DivisionId = DivisionID;
            ctrl_main_dashboard.UserId = UserId;
            ctrl_main_dashboard.IsAdmin = this.IsAdmin;
            ctrl_main_dashboard.Username = this.User;
            ctrl_main_dashboard.WorkingYear = this.WorkingYear;
            List<MenuFundSource> _list = f_source.Where(x => x.respo_name == cmbFundSource.SelectedItem.ToString()).ToList();

            if (_list.Count != 0)
            {
                ctrl_main_dashboard.FundName = _list[0].respo_name;
                ctrl_main_dashboard.FundSource = _list[0].code;
            }

           

            stkChild.Children.Clear();
            stkChild.Children.Add(ctrl_main_dashboard);

            if (IsAdmin == "2")
            {
                m_Menu.Visibility = System.Windows.Visibility.Visible;
                mnuExpenditure.Visibility = System.Windows.Visibility.Collapsed;
                btnAddNew.Visibility = System.Windows.Visibility.Visible;
                btnRemoveData.Visibility = System.Windows.Visibility.Visible;
                mnuBudgetAllocation.Visibility = System.Windows.Visibility.Collapsed;
                mnuPafManager.Visibility = System.Windows.Visibility.Collapsed;
                mnuDivUnit.Visibility = System.Windows.Visibility.Visible;

                if (IsUnit)
                {
                    mnuDivUnit.Visibility = System.Windows.Visibility.Collapsed;
                    mnuDivisionPerson.Visibility = System.Windows.Visibility.Collapsed;
                }

            }
            else if (IsAdmin == "1")
            {
                m_Menu.Visibility = System.Windows.Visibility.Visible;
                mnuExpenditure.Visibility = System.Windows.Visibility.Visible;
                btnAddNew.Visibility = System.Windows.Visibility.Visible;
                btnRemoveData.Visibility = System.Windows.Visibility.Visible;
                if (this.DivisionID != "3")
                {
                    mnuBudgetAllocation.Visibility = System.Windows.Visibility.Collapsed;
                    mnuPafManager.Visibility = System.Windows.Visibility.Collapsed;
                }
                else
                {
                    mnuBudgetAllocation.Visibility = System.Windows.Visibility.Visible;
                    mnuPafManager.Visibility = System.Windows.Visibility.Visible;
                }

                mnuDivUnit.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                mnuDivisionPerson.Visibility = System.Windows.Visibility.Collapsed;
                mnuExpenditure.Visibility = System.Windows.Visibility.Collapsed;
                btnAddNew.Visibility = System.Windows.Visibility.Collapsed;
                btnRemoveData.Visibility = System.Windows.Visibility.Collapsed;
                mnuBudgetAllocation.Visibility = System.Windows.Visibility.Collapsed;
                mnuPafManager.Visibility = System.Windows.Visibility.Collapsed;
                mnuDivUnit.Visibility = System.Windows.Visibility.Collapsed;
            }

        }

        void Content_Resized(object sender, EventArgs e)
        {
            this.Width = App.Current.Host.Content.ActualWidth;

            this.Height = App.Current.Host.Content.ActualHeight;
        }

        private void usrMenu_Loaded(object sender, RoutedEventArgs e)
        {
           
        }

        public void FetchFundSource() 
        {
            c_menu.Process = "FetchFundSource";
            svc_mindaf.ExecuteSQLAsync(c_menu.GetFundSource(DivisionID,WorkingYear));
        }
        public void AddNewRow() 
        {
            ctrl_main_dashboard.AddNewData();
        }
        public void RemoveData()
        {
            ctrl_main_dashboard.RemoveData();
        }

        private void mnuBudgetProposal_Click(object sender, EventArgs e)
        {
            stkChild.Children.Clear();
            stkChild.Children.Add(ctrl_budget);
        }

        private void mnuBudgetApprovals_Click(object sender, EventArgs e)
        {
            stkChild.Children.Clear();
            stkChild.Children.Add(ctrl_budget_approvals);
        }

        private void mnuProgramEncoding_Click(object sender, EventArgs e)
        {
            ctrl_create_program.DivisionID = this.DivisionID;
            stkChild.Children.Clear();
            stkChild.Children.Add(ctrl_create_program);
        }

        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {
            if (cmbFundSource.Items.Count !=0)
            {
                AddNewRow();
            }
            else
            {
                MessageBox.Show("Cannot add entry without Fundsource.");
            }
            
        }

  

        private void mnu_mooe_lib_Click(object sender, EventArgs e)
        {
            frmExpenditureLibrary frm_exp_lib = new frmExpenditureLibrary();
            frm_exp_lib.Show();
        }

        private void mnu_procurement_lib_Click(object sender, EventArgs e)
        {
            frmProcurementManager frm_proc_lib = new frmProcurementManager();
            frm_proc_lib.Show();
        }

        private void mnu_manage_personel_Click(object sender, EventArgs e)
        {
            frmPersonnelManagement frm_person = new frmPersonnelManagement();
            frm_person.DivisionId = this.DivisionID;
            frm_person.Show();
        }

        private void mnuProjStat_Click(object sender, EventArgs e)
        {
            ctrl_project_status.DivisionId = this.DivisionID;
            ctrl_project_status.User = this.User;
            ctrl_project_status.Level = Convert.ToInt32(this.IsAdmin);
            stkChild.Children.Clear();
            stkChild.Children.Add(ctrl_project_status);
           
        }

        private void mnu_expen_Click(object sender, EventArgs e)
        {
            frmExpenditureManager f_exp = new frmExpenditureManager();
            f_exp.Show();
        }

        private void mnu_expen_main_Click(object sender, EventArgs e)
        {
           
        }

        private void btnNotify_Click(object sender, RoutedEventArgs e)
        {
            double width = App.Current.Host.Content.ActualWidth;
            double height = App.Current.Host.Content.ActualHeight;


            
        }

        private void mnuBudgetAllocation_Click(object sender, EventArgs e)
        {
          

        }

        private void mnuPPMP_Click(object sender, EventArgs e)
        {
            btnAddNew.Visibility = System.Windows.Visibility.Collapsed;
      
            //ctrl_ppmp_generate.DivisionId = this.DivisionID;
            //ctrl_ppmp_generate.Division = this.Division;
            ctrl_ppmp_generate.WorkingYear = this.WorkingYear;

            stkChild.Children.Clear();
            stkChild.Children.Add(ctrl_ppmp_generate);
        }

        private void mnureal_Click(object sender, EventArgs e)
        {
            frmreAlignment f_rel = new frmreAlignment();
            f_rel.DivisionId = this.DivisionID;
            f_rel.PaP = this.PAP;
            f_rel.Show();
        }

        private void mnu_b_nep_Click(object sender, EventArgs e)
        {
            frmBudgetDivisionAllocate f_bda = new frmBudgetDivisionAllocate(this.DivisionID);
            f_bda.WorkingYear = this.WorkingYear;
            f_bda.OfficeId = this.OfficeID;
            f_bda.Show();
        }

        private void mnu_b_mooe_Click(object sender, EventArgs e)
        {
            frmBudgetAllocationMooe f_bda = new frmBudgetAllocationMooe(this.DivisionID);
            f_bda.WorkingYear = this.WorkingYear;
            f_bda.OfficeId = this.OfficeID;
            f_bda.Show();
        }

        private void mnu_respo_lib_Click(object sender, EventArgs e)
        {
            frmLibraryFundSource f_form = new frmLibraryFundSource();
            f_form.WorkingYear = this.WorkingYear;
            f_form.Show();
        }

        private void mnuExpenditureManage_Click(object sender, EventArgs e)
        {
            frmMainExpenditure f_exp_main = new frmMainExpenditure();
            f_exp_main.Show();
        }

        private void btnRemoveOutput_Click(object sender, RoutedEventArgs e)
        {
           // RemoveOutput();
        }

        private void btnRemoveData_Click(object sender, RoutedEventArgs e)
        {
            RemoveData();
        }

        private void mnuDivUnit_Click(object sender, EventArgs e)
        {
            frm_division_unit _div_unit = new frm_division_unit();
            _div_unit.DivisionId = this.DivisionID;
            _div_unit.Show();
        }

        private void mnuUserAllocate_Click(object sender, EventArgs e)
        {
            frmBudgetUnitAllocate f_alloc = new frmBudgetUnitAllocate();
            f_alloc.DivisionId = this.DivisionID;
            f_alloc.Show();
        }

        private void mnuProjActi_Click(object sender, EventArgs e)
        {
            frmActivityReportPerTotal f_div = new frmActivityReportPerTotal();

            f_div._Year = WorkingYear;
            f_div.DivisionId = this.DivisionID;

            f_div.Show();
        }

        private void mnuPR_Click(object sender, EventArgs e)
        {
            frmPRRequest f_request = new frmPRRequest();

            f_request._Year = WorkingYear;
            f_request.DivisionID = this.DivisionID;

            f_request.Show();
        }
        frmFundSourceList f_list = new frmFundSourceList();

        private void mnuPMPRevision_Click(object sender, EventArgs e)
        {
            f_list.WorkingYear = this.WorkingYear;
            f_list.DivisionId = this.DivisionID;
            f_list.Selected += f_list_Selected;
            f_list.Show();
          
        }

        void f_list_Selected(object sender, EventArgs e)
        {
            btnAddNew.Visibility = System.Windows.Visibility.Collapsed;

            ctrl_ppmp_generate.DivId = this.DivisionID;
            ctrl_ppmp_generate.SelectedYear = WorkingYear;
            ctrl_ppmp_generate.WorkingYear = WorkingYear;
            ctrl_ppmp_generate.FundName = f_list.SelectedFundSource;
            ctrl_ppmp_generate.FundSource = f_list.SelectedCode;
            ctrl_ppmp_generate.FundType = f_list.SelectedFundType;
            stkChild.Children.Clear();
            stkChild.Children.Add(ctrl_ppmp_generate);
        }

        private void mnuPMPData_Click(object sender, EventArgs e)
        {
            HtmlPage.Window.Navigate(new Uri("http://mindafinance/Downloads/ReportTool.application"), "_blank");
        }

        private void cmbFundSource_DropDownClosed(object sender, EventArgs e)
        {
            LoadData();
           // ctrl_main_dashboard.RefreshData(this.DivisionID);
        }

        private void mnuReportPrintBalances_Click(object sender, EventArgs e)
        {
            //frmReportBalances f_report = new frmReportBalances();
            //f_report.FundSource = this.FundSource;
            //f_report.Show();
        }

        private void mnu_manage_paf_Click(object sender, EventArgs e)
        {

            frmPAFManager frm_pap_office = new frmPAFManager();
            frm_pap_office.WorkingYear = this.WorkingYear;
            frm_pap_office.OfficeId = this.OfficeID;
            frm_pap_office.DivisionId = this.DivisionID;
            frm_pap_office.Show();
        }

       
     
    }
}
