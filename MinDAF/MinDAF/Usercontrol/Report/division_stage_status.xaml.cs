﻿using Infragistics.Controls.Grids;
using MinDAF.Class;
using MinDAF.Forms;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol.Report
{
    public partial class division_stage_status : UserControl
    {
        private clsDataStatus c_app = new clsDataStatus();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private List<DataStatus> _ListMainData = new List<DataStatus>();
        private List<DivisionPrograms> _ListDivisionPrograms = new List<DivisionPrograms>();
        private List<DivisionActivities> _ListDivisionActivities = new List<DivisionActivities>();
        public String Division { get; set; }
    
        public division_stage_status()
        {
            InitializeComponent();

            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;    
        }
        private void FetchDivisionLists() 
        {
            c_app.Process = "FetchDivisionLists";
            svc_mindaf.ExecuteSQLAsync(c_app.GetDivisions());

        }
        private void FetchDivisionPrograms()
        {
            c_app.Process = "FetchDivisionPrograms";
            svc_mindaf.ExecuteSQLAsync(c_app.GetDivisionPrograms());
        }

        private void FetchDivisionActivities()
        {
            c_app.Process = "FetchDivisionActivities";
            svc_mindaf.ExecuteSQLAsync(c_app.GetDivisionActivities());
        }

      

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
           var _results = e.Result.ToString();

           switch (c_app.Process)
           {
               case "FetchDivisionActivities":
                   XDocument oDocKeyResultsFetchDivisionActivities = XDocument.Parse(_results);
                   var _dataListsFetchDivisionActivities = from info in oDocKeyResultsFetchDivisionActivities.Descendants("Table")
                                                           select new DivisionActivities
                                                         {
                                                             activity_id = Convert.ToString(info.Element("activity_id").Value),
                                                             status = Convert.ToString(info.Element("status").Value)
                                                         };


                   _ListDivisionActivities.Clear();

                   foreach (var item in _dataListsFetchDivisionActivities)
                   {
                       DivisionActivities _varDetails = new DivisionActivities();

                       _varDetails.activity_id = item.activity_id;
                       _varDetails.status = item.status;
                    

                       _ListDivisionActivities.Add(_varDetails);
                   }
                   //---------------------------------------------------------------
                   foreach (DataStatus item in _ListMainData)
                   {
                       List<DivisionPrograms> xdata = _ListDivisionPrograms.Where(x => x.Division_Desc == item.Division).ToList();
                       double _main_percentage = (((xdata.Count / 0.10) / 100) * 100);
                       double _main_counter_percent = 0.0;
                       Int32 _count_second = 0;
                       Int32 _count_third = 0;
                       if (xdata.Count > 0)
                       {
                           item.Planning_Stage = "Done";
                       }
                       else
                       {
                           item.Planning_Stage = "Not Started";
                       }

                       foreach (DivisionPrograms items in xdata)
                       {
                           List<DivisionActivities> div_act = _ListDivisionActivities.Where(x => x.activity_id == items.act_id).ToList();


                           List<DivisionActivities> act_submitted = div_act.Where(x => x.status == "Submitted").ToList();
                           List<DivisionActivities> act_divhead = div_act.Where(x => x.status == "For Approval By Division Head").ToList();
                           List<DivisionActivities> act_finhead = div_act.Where(x => x.status == "For Approval By Finance").ToList();


                           if (act_submitted.Count!= 0)
                           {
                               _main_counter_percent += 1;
                           }
                           if (act_divhead.Count != 0)
                           {
                               _count_second += 1;
                           }
                           if (act_finhead.Count !=0)
                           {
                               _count_third += 1;
                           }
                           

                       }
                       if (_main_counter_percent != 0)
                       {
                           item.Submit_Stage = "Submitted";
                        }
                       else if (_count_second !=0)                     
                       {
                           item.Submit_Stage = "Submitted";

                       }
                       else if (_count_third != 0)
                       {
                           item.Submit_Stage = "Submitted";
                           item.Division_Approval_Stage = "Done";
                       }

                       else
                       {
                           item.Submit_Stage = "Not Submitted";
                       }
                   }
                   
                     foreach (DataStatus item in _ListMainData)
                                       {
                       
                                               List<DivisionPrograms> xdata = _ListDivisionPrograms.Where(x => x.Division_Desc == item.Division).ToList();
                                               double _main_counter_percent = 0.0;
                                               Int32 _counter_to_finance = 0;
                                               Int32 _counter_submitted = 0;
                                               foreach (DivisionPrograms items in xdata)
                                               {
                                                   List<DivisionActivities> div_act = _ListDivisionActivities.Where(x => x.activity_id == items.act_id).ToList();


                                                   List<DivisionActivities> act_submitted = div_act.Where(x => x.status == "For Approval By Division Head").ToList();
                                                   List<DivisionActivities> act_submitted_flag = div_act.Where(x => x.status == "Submitted").ToList();
                                                   List<DivisionActivities> act_finance = div_act.Where(x => x.status == "For Approval By Finance").ToList();


                                                   if (act_submitted.Count != 0)
                                                   {
                                                       _main_counter_percent += 1;
                                                   }
                                                   if (act_finance.Count!=0)
                                                   {
                                                       _counter_to_finance += 1;
                                                   }
                                                   if (act_submitted_flag.Count!=0)
                                                   {
                                                       _counter_submitted += 1;
                                                   }

                                               }
                                               if (_main_counter_percent != 0)
                                               {
                                                   if (item.Submit_Stage=="Submitted")
                                                   {
                                                      
                                                           item.Division_Approval_Stage = "On Going Approval";                                                      
                                                   }
                                                   else
                                                   {
                                                       if (_counter_submitted!=0)
                                                       {
                                                           item.Division_Approval_Stage = "On Going Approval";
                                                       }
                                                       else
                                                       {
                                                           item.Division_Approval_Stage = "Done";
                                                       }
                                                       
                                                   }
                                                     
                                                      
                                                  
                                               }else if (_counter_to_finance != 0)
                                               {
                                                   if (_counter_submitted != 0)
                                                   {
                                                       item.Division_Approval_Stage = "On Going Approval";
                                                   }
                                                   else 
                                                   { 
                                                   
                                                           item.Division_Approval_Stage = "Done";
                                                   }
                                               }
                     
                     

                                       }
                   foreach (DataStatus item in _ListMainData)
                   {
                       
                           List<DivisionPrograms> xdata = _ListDivisionPrograms.Where(x => x.Division_Desc == item.Division).ToList();
                           double _main_counter_percent = 0.0;

                           foreach (DivisionPrograms items in xdata)
                           {
                               List<DivisionActivities> div_act = _ListDivisionActivities.Where(x => x.activity_id == items.act_id).ToList();


                               List<DivisionActivities> act_submitted = div_act.Where(x => x.status == "For Approval By Finance").ToList();


                               if (act_submitted.Count != 0)
                               {
                                   _main_counter_percent += 1;
                               }


                           }
                           if (_main_counter_percent != 0)
                           {
                               if (item.Submit_Stage =="Submitted" && item.Division_Approval_Stage =="Done")
                               {
                                   item.Division_Approval_Stage = "Done";
                                   item.Budget_Approval_Stage = "For Approval By Budget Officer";
                               }
                               
                           }
                     
                     

                   }
                   foreach (DataStatus item in _ListMainData)
                   {

                       List<DivisionPrograms> xdata = _ListDivisionPrograms.Where(x => x.Division_Desc == item.Division).ToList();
                       double _main_counter_percent = 0.0;

                       foreach (DivisionPrograms items in xdata)
                       {
                           List<DivisionActivities> div_act = _ListDivisionActivities.Where(x => x.activity_id == items.act_id).ToList();


                           List<DivisionActivities> act_submitted = div_act.Where(x => x.status == "FINANCE APPROVED").ToList();


                           if (act_submitted.Count != 0)
                           {
                               _main_counter_percent += 1;
                           }


                       }
                       if (_main_counter_percent != 0)
                       {
                           item.Planning_Stage = "Done";
                           item.Submit_Stage = "Submitted";
                           item.Division_Approval_Stage = "Done";
                           item.Budget_Approval_Stage = "Done";
                           item.PPMP_Stage = "Ready For PPMP";

                       }



                   }



                   grdData.ItemsSource = null;
                   grdData.ItemsSource = _ListMainData;

                   //Column col_pap = grdData.Columns.DataColumns["PAP"];
                   //Column col_division = grdData.Columns.DataColumns["Division"];

                   //col_pap.Width = new ColumnWidth(100, false);
                   //col_division.Width = new ColumnWidth(300, false);
                   //grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_pap);
                   //grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_division);
                   break;
               case "FetchDivisionPrograms":
                   XDocument oDocKeyResultsFetchDivisionPrograms = XDocument.Parse(_results);
                   var _dataListsFetchDivisionPrograms = from info in oDocKeyResultsFetchDivisionPrograms.Descendants("Table")
                                    select new DivisionPrograms
                                    {
                                        accountable_division = Convert.ToString(info.Element("accountable_division").Value),
                                        description = Convert.ToString(info.Element("description").Value),
                                        Division_Desc = Convert.ToString(info.Element("Division_Desc").Value),
                                        act_id = Convert.ToString(info.Element("act_id").Value) 
                                    };


                   _ListDivisionPrograms.Clear();

                   foreach (var item in _dataListsFetchDivisionPrograms)
                   {
                       DivisionPrograms _varDetails = new DivisionPrograms();

                       _varDetails.accountable_division  = item.accountable_division;
                       _varDetails.Division_Desc = item.Division_Desc;
                       _varDetails.description = item.description;
                       _varDetails.act_id = item.act_id;

                       _ListDivisionPrograms.Add(_varDetails);
                   }
                   //---------------------------------------------------------------
                  


                   FetchDivisionActivities();
                   break;

               case "FetchDivisionLists":
                   XDocument oDocKeyResults = XDocument.Parse(_results);
                   var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                    select new DataStatus
                                    {
                                        Budget_Approval_Stage = Convert.ToString(info.Element("Budget_Approval_Stage").Value),
                                        Division = Convert.ToString(info.Element("Division").Value),
                                        Division_Approval_Stage = Convert.ToString(info.Element("Division_Approval_Stage").Value),
                                        PAP = Convert.ToString(info.Element("PAP").Value),
                                        Planning_Stage = Convert.ToString(info.Element("Planning_Stage").Value),
                                        PPMP_Stage = Convert.ToString(info.Element("PPMP_Stage").Value),
                                        Submit_Stage = Convert.ToString(info.Element("Submit_Stage").Value),
                                    };


                   _ListMainData.Clear();

                   foreach (var item in _dataLists)
                   {
                       DataStatus _varDetails = new DataStatus();

                       _varDetails.Budget_Approval_Stage = item.Budget_Approval_Stage;
                       _varDetails.Division = item.Division;
                       _varDetails.Division_Approval_Stage = item.Division_Approval_Stage;
                       _varDetails.PAP = item.PAP;
                       _varDetails.Planning_Stage = item.Planning_Stage;
                       _varDetails.PPMP_Stage = item.PPMP_Stage;
                       _varDetails.Submit_Stage = item.Submit_Stage;
                       _ListMainData.Add(_varDetails);
                   }

                   FetchDivisionPrograms();

                   this.Cursor = Cursors.Arrow;
               
                   break;
           }
        }

        private void ctrlDivStat_Loaded(object sender, RoutedEventArgs e)
        {
            FetchDivisionLists();
        }

        private void btnViewStatus_Click(object sender, RoutedEventArgs e)
        {
            frmRevisionStatus f_rev = new frmRevisionStatus();

            f_rev.Show();
        }
    }
}
