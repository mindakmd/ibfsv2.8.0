﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using Infragistics.Controls.Grids;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol.Report
{
    public partial class usr_mba_summary_mooe_divisions : UserControl
    {
        public String DivisionName { get; set; }
        public String SelectedYear { get; set; }
        public String DivisionId { get; set; }

        private List<ReportMBASumm> ReportData = new List<ReportMBASumm>();
        private List<ReportMBAHeaderSumm> ListHeaderTemplate = new List<ReportMBAHeaderSumm>();
        private List<ReportMBADetailSumm> ListHeaderDetail = new List<ReportMBADetailSumm>();
        private List<ReportMBAHeaderSumm> ListTotals = new List<ReportMBAHeaderSumm>();
        private List<ReportDataMBA> ListReportData = new List<ReportDataMBA>();
        private List<AllocationExpenditure> ListAllocation = new List<AllocationExpenditure>();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsBudgetReportMBA c_rep_mba = new clsBudgetReportMBA();

        public usr_mba_summary_mooe_divisions()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_rep_mba.Process)
            {
                case "FetchExpenditureTemplate":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new ReportMBAHeaderSumm
                                     {
                                         Division = "",
                                         Alloted = Convert.ToString(info.Element("Alloted").Value),
                                         Apr = Convert.ToString(info.Element("Apr").Value),
                                         Aug = Convert.ToString(info.Element("Aug").Value),
                                         Balance = Convert.ToString(info.Element("Balance").Value),
                                         Dec = Convert.ToString(info.Element("Dec").Value),
                                         Feb = Convert.ToString(info.Element("Feb").Value),
                                         id = Convert.ToString(info.Element("id").Value),
                                         Jan = Convert.ToString(info.Element("Jan").Value),
                                         Jul = Convert.ToString(info.Element("Jul").Value),
                                         Jun = Convert.ToString(info.Element("Jun").Value),
                                         Mar = Convert.ToString(info.Element("Mar").Value),
                                         May = Convert.ToString(info.Element("May").Value),
                                         Name = Convert.ToString(info.Element("Name").Value),
                                         Nov = Convert.ToString(info.Element("Nov").Value),
                                         Oct = Convert.ToString(info.Element("Oct").Value),
                                         Sep = Convert.ToString(info.Element("Sep").Value),
                                         Total = Convert.ToString(info.Element("Total").Value),
                                         Quarter_1 = Convert.ToString(info.Element("Quarter_1").Value),
                                         Quarter_2 = Convert.ToString(info.Element("Quarter_2").Value),
                                         Quarter_3 = Convert.ToString(info.Element("Quarter_3").Value),
                                         Quarter_4 = Convert.ToString(info.Element("Quarter_4").Value)
                                     };


                    ListHeaderTemplate.Clear();

                    foreach (var item in _dataLists)
                    {
                        ReportMBAHeaderSumm _varProf = new ReportMBAHeaderSumm();

                        _varProf.Alloted = item.Alloted;
                        _varProf.Apr = item.Apr;
                        _varProf.Aug = item.Aug;
                        _varProf.Balance = item.Balance;
                        _varProf.Dec = item.Dec;
                        _varProf.Feb = item.Feb;
                        _varProf.id = item.id;
                        _varProf.Jan = item.Jan;
                        _varProf.Jul = item.Jul;
                        _varProf.Jun = item.Jun;
                        _varProf.Mar = item.Mar;
                        _varProf.May = item.May;
                        _varProf.Name = item.Name;
                        _varProf.Nov = item.Nov;
                        _varProf.Oct = item.Oct;
                        _varProf.Sep = item.Sep;
                        _varProf.Total = item.Total;
                        _varProf.Quarter_1 = item.Quarter_1;
                        _varProf.Quarter_2 = item.Quarter_2;
                        _varProf.Quarter_3 = item.Quarter_3;
                        _varProf.Quarter_4 = item.Quarter_4;

                        ListHeaderTemplate.Add(_varProf);

                    }

                    FetchExpenditureDetail();
                    this.Cursor = Cursors.Arrow;

                    break;
                case "FetchExpenditureDetail":
                    XDocument oDocKeyResultsFetchExpenditureDetail = XDocument.Parse(_results);
                    var _dataListsFetchExpenditureDetail = from info in oDocKeyResultsFetchExpenditureDetail.Descendants("Table")
                                                           select new ReportMBADetailSumm
                                                           {
                                                               Division  = "",
                                                               Alloted = Convert.ToString(info.Element("Alloted").Value),
                                                               Apr = Convert.ToString(info.Element("Apr").Value),
                                                               Aug = Convert.ToString(info.Element("Aug").Value),
                                                               Balance = Convert.ToString(info.Element("Balance").Value),
                                                               Dec = Convert.ToString(info.Element("Dec").Value),
                                                               Feb = Convert.ToString(info.Element("Feb").Value),
                                                               id = Convert.ToString(info.Element("id").Value),
                                                               Jan = Convert.ToString(info.Element("Jan").Value),
                                                               Jul = Convert.ToString(info.Element("Jul").Value),
                                                               Jun = Convert.ToString(info.Element("Jun").Value),
                                                               Mar = Convert.ToString(info.Element("Mar").Value),
                                                               May = Convert.ToString(info.Element("May").Value),
                                                               Name = Convert.ToString(info.Element("Name").Value),
                                                               Nov = Convert.ToString(info.Element("Nov").Value),
                                                               Oct = Convert.ToString(info.Element("Oct").Value),
                                                               Sep = Convert.ToString(info.Element("Sep").Value),
                                                               Total = Convert.ToString(info.Element("Total").Value),
                                                               Quarter_1 = Convert.ToString(info.Element("Quarter_1").Value),
                                                               Quarter_2 = Convert.ToString(info.Element("Quarter_2").Value),
                                                               Quarter_3 = Convert.ToString(info.Element("Quarter_3").Value),
                                                               Quarter_4 = Convert.ToString(info.Element("Quarter_4").Value)
                                                           };


                    ListHeaderDetail.Clear();

                    foreach (var item in _dataListsFetchExpenditureDetail)
                    {
                        ReportMBADetailSumm _varProf = new ReportMBADetailSumm();

                        _varProf.Alloted = item.Alloted;
                        _varProf.Apr = item.Apr;
                        _varProf.Aug = item.Aug;
                        _varProf.Balance = item.Balance;
                        _varProf.Dec = item.Dec;
                        _varProf.Feb = item.Feb;
                        _varProf.id = item.id;
                        _varProf.Jan = item.Jan;
                        _varProf.Jul = item.Jul;
                        _varProf.Jun = item.Jun;
                        _varProf.Mar = item.Mar;
                        _varProf.May = item.May;
                        _varProf.Name = item.Name;
                        _varProf.Nov = item.Nov;
                        _varProf.Oct = item.Oct;
                        _varProf.Sep = item.Sep;
                        _varProf.Total = item.Total;
                        _varProf.Quarter_1 = item.Quarter_1;
                        _varProf.Quarter_2 = item.Quarter_2;
                        _varProf.Quarter_3 = item.Quarter_3;
                        _varProf.Quarter_4 = item.Quarter_4;

                        ListHeaderDetail.Add(_varProf);

                    }
                    FetchData();
                    this.Cursor = Cursors.Arrow;

                    break;
                case "FetchData":
                    XDocument oDocKeyResultsFetchData = XDocument.Parse(_results);
                    var _dataListsFetchData = from info in oDocKeyResultsFetchData.Descendants("Table")
                                              select new ReportDataMBA
                                              {
                                                  pap_code = Convert.ToString(info.Element("pap_code").Value),
                                                  division = Convert.ToString(info.Element("division").Value),
                                                  id = Convert.ToString(info.Element("id").Value),
                                                  month = Convert.ToString(info.Element("month").Value),
                                                  total = Convert.ToString(info.Element("total").Value),
                                                  year = Convert.ToString(info.Element("year").Value),
                                                  name = Convert.ToString(info.Element("name").Value)
                                              };


                    ListReportData.Clear();

                    foreach (var item in _dataListsFetchData)
                    {
                        ReportDataMBA _varProf = new ReportDataMBA();

                        _varProf.id = item.id;
                        _varProf.month = item.month;
                        _varProf.total = item.total;
                        _varProf.year = item.year;
                        _varProf.name = item.name;
                        _varProf.division = item.division;
                        _varProf.pap_code = item.pap_code;
                        ListReportData.Add(_varProf);

                    }

                    this.Cursor = Cursors.Arrow;
                    FetchAllocation();
                    break;
                case "FetchAllocation":
                    XDocument oDocKeyResultsFetchAllocation = XDocument.Parse(_results);
                    var _dataListsFetchAllocation = from info in oDocKeyResultsFetchAllocation.Descendants("Table")
                                                    select new AllocationExpenditure
                                                    {
                                                        name = Convert.ToString(info.Element("name").Value),
                                                        allocated_budget = Convert.ToString(info.Element("allocated_budget").Value)

                                                    };


                    ListAllocation.Clear();

                    foreach (var item in _dataListsFetchAllocation)
                    {
                        AllocationExpenditure _varProf = new AllocationExpenditure();

                        _varProf.name = item.name;
                        _varProf.allocated_budget = item.allocated_budget;

                        ListAllocation.Add(_varProf);

                    }
                    GenerateTemplate();
                    this.Cursor = Cursors.Arrow;

                    break;
            }
        }

          private void GenerateTemplate()
        {
            double _JanTotal = 0.00;
            double _FebTotal = 0.00;
            double _MarTotal = 0.00;
            double _AprTotal = 0.00;
            double _MayTotal = 0.00;
            double _JunTotal = 0.00;
            double _JulTotal = 0.00;
            double _AugTotal = 0.00;
            double _SepTotal = 0.00;
            double _OctTotal = 0.00;
            double _NovTotal = 0.00;
            double _DecTotal = 0.00;
            double _LineTotal = 0.00;
            double _AllTotal = 0.00;
            double _AllBalance = 0.00;
            foreach (var item in ListHeaderDetail)
            {
                List<AllocationExpenditure> x_data = ListAllocation.Where(_filter => _filter.name == item.Name).ToList();
                if (x_data.Count!=0)
                {
                    double _all = Convert.ToDouble(x_data[0].allocated_budget);
                    item.Alloted = _all.ToString("#,##0.00");
                }
            }
            foreach (var item in ListHeaderDetail)
            {
                double _JanTotalDetails = 0.00;
                double _FebTotalsDetails = 0.00;
                double _MarTotalsDetails = 0.00;
                double _AprTotalsDetails = 0.00;
                double _MayTotalsDetails = 0.00;
                double _JunTotalsDetails = 0.00;
                double _JulTotalsDetails = 0.00;
                double _AugTotalsDetails = 0.00;
                double _SepTotalsDetails = 0.00;
                double _OctTotalsDetails = 0.00;
                double _NovTotalsDetails = 0.00;
                double _DecTotalsDetails = 0.00;

                double _Quarter1 = 0.00;
                double _Quarter2 = 0.00;
                double _Quarter3 = 0.00;
                double _Quarter4 = 0.00;

                List<ReportDataMBA> x_data = ListReportData.Where(_filter => _filter.name ==item.Name).ToList();
                
                foreach (var item_data in x_data)
                {
                    item.Division = item_data.division + " - " + item_data.pap_code;
                    switch (item_data.month)
                    {
                        case "Jan":
                            _JanTotal += Convert.ToDouble(item_data.total);
                            _JanTotalDetails += Convert.ToDouble(item_data.total);
                            item.Jan = _JanTotalDetails.ToString("#,##0.00");
                            break;
                        case "Feb":
                            _FebTotal += Convert.ToDouble(item_data.total);
                            _FebTotalsDetails += Convert.ToDouble(item_data.total);
                            item.Feb = _FebTotalsDetails.ToString("#,##0.00");
                            break;
                         case "Mar":
                            _MarTotal+= Convert.ToDouble(item_data.total);
                            _MarTotalsDetails += Convert.ToDouble(item_data.total);
                            item.Mar = _MarTotalsDetails.ToString("#,##0.00");
                            break;
                         case "Apr":
                             _AprTotal+= Convert.ToDouble(item_data.total);
                             _AprTotalsDetails += Convert.ToDouble(item_data.total);
                             item.Apr = _AprTotalsDetails.ToString("#,##0.00");
                            break;
                         case "May":
                             _MayTotal+= Convert.ToDouble(item_data.total);
                             _MayTotalsDetails += Convert.ToDouble(item_data.total);
                             item.May = _MayTotalsDetails.ToString("#,##0.00");
                            break;
                         case "Jun":
                             _JunTotal+= Convert.ToDouble(item_data.total);
                             _JunTotalsDetails += Convert.ToDouble(item_data.total);
                             item.Jun = _JunTotalsDetails.ToString("#,##0.00");
                            break;
                         case "Jul":
                             _JulTotal+= Convert.ToDouble(item_data.total);
                             _JulTotalsDetails += Convert.ToDouble(item_data.total);
                             item.Jul = _JulTotalsDetails.ToString("#,##0.00");
                            break;
                         case "Aug":
                             _AugTotal+= Convert.ToDouble(item_data.total);
                             _AugTotalsDetails += Convert.ToDouble(item_data.total);
                             item.Aug = _AugTotalsDetails.ToString("#,##0.00");
                            break;
                         case "Sep":
                             _SepTotal+= Convert.ToDouble(item_data.total);
                             _SepTotalsDetails += Convert.ToDouble(item_data.total);
                             item.Sep = _SepTotalsDetails.ToString("#,##0.00");
                            break;
                         case "Oct":
                             _OctTotal+= Convert.ToDouble(item_data.total);
                             _OctTotalsDetails += Convert.ToDouble(item_data.total);
                             item.Oct = _OctTotalsDetails.ToString("#,##0.00");
                            break;
                         case "Nov":
                             _NovTotal+= Convert.ToDouble(item_data.total);
                             _NovTotalsDetails += Convert.ToDouble(item_data.total);
                             item.Nov = _NovTotalsDetails.ToString("#,##0.00");
                            break;
                         case "Dec":
                             _DecTotal+= Convert.ToDouble(item_data.total);
                             _DecTotalsDetails += Convert.ToDouble(item_data.total);
                             item.Dec = _DecTotalsDetails.ToString("#,##0.00");
                            break;
          
                    }
                    item.Quarter_1 = (_JanTotalDetails + _FebTotalsDetails + _MarTotalsDetails).ToString("#,##0.00");
                    item.Quarter_2 = (_AprTotalsDetails + _MayTotalsDetails + _JunTotalsDetails).ToString("#,##0.00");
                    item.Quarter_3 = (_JulTotalsDetails + _AugTotalsDetails + _SepTotalsDetails).ToString("#,##0.00");
                    item.Quarter_4 = (_OctTotalsDetails + _NovTotalsDetails + _DecTotalsDetails).ToString("#,##0.00");
                }
                
            }
            //Computer For Total and Balance
            foreach (var item in ListHeaderDetail)
            {
                double _lineTotal = Convert.ToDouble(item.Jan) + Convert.ToDouble(item.Feb) + Convert.ToDouble(item.May) + Convert.ToDouble(item.Mar) + Convert.ToDouble(item.Apr) +
                                    Convert.ToDouble(item.Jun) + Convert.ToDouble(item.Jul) + Convert.ToDouble(item.Aug) + Convert.ToDouble(item.Sep) + Convert.ToDouble(item.Oct) +
                                    Convert.ToDouble(item.Nov) + Convert.ToDouble(item.Dec);
                double _lineAllotted = Convert.ToDouble(item.Alloted);
                double _balance = _lineAllotted - _lineTotal;

                item.Total = _lineTotal.ToString("#,##0.00");
                item.Balance = _balance.ToString("#,##0.00");
                _LineTotal += _lineTotal;

                if (_lineTotal>0)
                {
                    if (_lineAllotted>0)
                    {
                        _AllTotal += _lineAllotted;
                        _AllBalance += _balance;
                    }
                }
               
            }

            ReportMBAHeaderSumm _Totals = new ReportMBAHeaderSumm();
            _Totals.Name = "Totals";
            _Totals.Alloted = _AllTotal.ToString("#,##0.00");
            _Totals.Jan = _JanTotal.ToString("#,##0.00");
            _Totals.Feb = _FebTotal.ToString("#,##0.00");
            _Totals.Mar = _MarTotal.ToString("#,##0.00");
            _Totals.Quarter_1 = (_JanTotal + _FebTotal + _MarTotal ).ToString();
            _Totals.Apr = _AprTotal.ToString("#,##0.00");
            _Totals.May = _MayTotal.ToString("#,##0.00");
            _Totals.Jun = _JunTotal.ToString("#,##0.00");
            _Totals.Quarter_2 = (_AprTotal + _MayTotal + _JunTotal).ToString();
            _Totals.Jul = _JulTotal.ToString("#,##0.00");
            _Totals.Aug = _AugTotal.ToString("#,##0.00");
            _Totals.Sep = _SepTotal.ToString("#,##0.00");
            _Totals.Quarter_3 = (_JulTotal + _AugTotal + _SepTotal).ToString();
            _Totals.Oct = _OctTotal.ToString("#,##0.00");
            _Totals.Nov = _NovTotal.ToString("#,##0.00");
            _Totals.Dec = _DecTotal.ToString("#,##0.00");
            _Totals.Quarter_4 = (_OctTotal + _NovTotal + _DecTotal).ToString();
            _Totals.Total = _LineTotal.ToString("#,##0.00");
            _Totals.Balance = _AllBalance.ToString("#,##0.00");
            ListTotals.Clear();
            ListTotals.Add(_Totals);
            foreach (var item in ListHeaderTemplate)
            {
                ReportMBASumm _final = new ReportMBASumm();

                List<ReportMBADetailSumm> _data = ListHeaderDetail.Where(itemDetail => itemDetail.id == item.id).ToList();
                List<ReportMBADetailSumm> _dataFinal = _data.Where(itemDetail => itemDetail.Total != "0.00").ToList();

                foreach (var item_data in _dataFinal)
                {
                    if (item_data.Total !="0.00")
                    {
                        _final.ReportHeader = item.Name;
                        _final.ReportDetail = _dataFinal;
                        ReportData.Add(_final);
                        break;
                    }
                }
              
              

            }
            grdData.ItemsSource = null;
            grdData.ItemsSource = ReportData;
            
            grdTotals.ItemsSource = null;
            grdTotals.ItemsSource = ListTotals;

            grdTotals.Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
            grdTotals.Columns["UACS"].Visibility = System.Windows.Visibility.Collapsed;

            Column col_project_name = grdData.Columns.DataColumns["ReportHeader"];
            col_project_name.Width = new ColumnWidth(298.5, false);

            Column col_project_name_t = grdTotals.Columns.DataColumns["Name"];
            col_project_name_t.Width = new ColumnWidth(325, false);

            foreach (var item in grdData.Rows)
            {
                if (item.HasChildren)
                {
                    item.IsExpanded = true;
                    item.ChildBands[0].ColumnLayout.HeaderVisibility = Visibility.Collapsed;
                    item.ChildBands[0].Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
                    foreach (var child in item.ChildBands)
                    {
                        Column col_project_name_child = child.Columns.DataColumns["Name"];
                        col_project_name_child.Width = new ColumnWidth(300, false);
                        
                    }
                }
            }
        }

        private void FetchExpenditureHeader()
        {
            c_rep_mba.Process = "FetchExpenditureTemplate";
            svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchExpenditureTemplateSumm());
        }
        private void FetchAllocation()
        {
            c_rep_mba.Process = "FetchAllocation";
            svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchAllocationAll(this.DivisionId, this.SelectedYear));
        }
        private void FetchExpenditureDetail()
        {
            c_rep_mba.Process = "FetchExpenditureDetail";
            svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchExpenditureDetailTemplateSumm());
        }
        private void FetchData()
        {
            c_rep_mba.Process = "FetchData";
            svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchDataSummary(this.SelectedYear,this.DivisionId));
        }

        private void usr_b_sum_Loaded(object sender, RoutedEventArgs e)
        {
            FetchExpenditureHeader();
            lblOffice.Content = this.DivisionName;
        }

    }
}
