﻿using Infragistics.Controls.Grids;
using MinDAF.Class;
using MinDAF.Forms;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol.Report
{
    public partial class ppmp_report_revision : UserControl
    {
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        public String DivId { get; set; }
        public String SelectedYear { get; set; }
        public String FundSource { get; set; }
        public String FundName { get; set; }
        public String FundType { get; set; }
        public String PAPCODE { get; set; }
        public string WorkingYear { get; set; }
        private List<BudgetRevisionMain> _Main = new List<BudgetRevisionMain>();
        private List<BudgetRevision> _BudgetItems = new List<BudgetRevision>();
        private List<BudgetActivityItems> _BudgetActivityItems = new List<BudgetActivityItems>();
        private List<BudgetActivityItemsMonths> _BudgetActivityItemsMonths = new List<BudgetActivityItemsMonths>();
        private List<BudgetRevisedData> _BudgetListAdjustment = new List<BudgetRevisedData>();
        private List<BudgetRevisionItems> _BudgetRevision = new List<BudgetRevisionItems>();
        private List<BudgetRevisionTitles> _BudgetRevisionTitles = new List<BudgetRevisionTitles>();
        private List<OBR_Details> ListGridDataDetails = new List<OBR_Details>();
        private List<DivisionRafNo> ListRafNo = new List<DivisionRafNo>();
        private List<DivisionAdjustment> ListAdjustments = new List<DivisionAdjustment>();
        private clsppmp_realign c_ppmp = new clsppmp_realign();
        public ppmp_report_revision()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            svc_mindaf.ExecuteImportDataSQLCompleted += svc_mindaf_ExecuteImportDataSQLCompleted;
        }

        void svc_mindaf_ExecuteImportDataSQLCompleted(object sender, ExecuteImportDataSQLCompletedEventArgs e)
        {
            String _results = e.Result.ToString();

            switch (c_obr.Process)
            {
                case "FetchOBRData":
                    XDocument oDocKeyFetchGridData = XDocument.Parse(_results);
                    var _dataListsFetchGridData = from info in oDocKeyFetchGridData.Descendants("Table")
                                                  select new OBR_Details
                                                  {
                                                      _Month = Convert.ToString(info.Element("_Month").Value),
                                                      AccountCode = Convert.ToString(info.Element("AccountCode").Value),
                                                      DateCreated = Convert.ToString(info.Element("DateCreated").Value),
                                                      Division = Convert.ToString(info.Element("Division").Value),
                                                      Total = Convert.ToString(info.Element("Total").Value)

                                                  };

                    ListGridDataDetails.Clear();

                    foreach (var item in _dataListsFetchGridData)
                    {
                        OBR_Details _varDetails = new OBR_Details();

                        switch (item._Month)
                        {
                            case "1": _varDetails._Month = "Jan"; break;
                            case "2": _varDetails._Month = "Feb"; break;
                            case "3": _varDetails._Month = "Mar"; break;
                            case "4": _varDetails._Month = "Apr"; break;
                            case "5": _varDetails._Month = "May"; break;
                            case "6": _varDetails._Month = "Jun"; break;
                            case "7": _varDetails._Month = "Jul"; break;
                            case "8": _varDetails._Month = "Aug"; break;
                            case "9": _varDetails._Month = "Sep"; break;
                            case "10": _varDetails._Month = "Oct"; break;
                            case "11": _varDetails._Month = "Nov"; break;
                            case "12": _varDetails._Month = "Dec"; break;
                        }

                        _varDetails.AccountCode = item.AccountCode;
                        _varDetails.DateCreated = item.DateCreated;
                        _varDetails.Division = item.Division;
                        _varDetails.Total = item.Total;

                        ListGridDataDetails.Add(_varDetails);

                    }
                    List<String> _months = new List<string>();

                    for (int i = 1; i < DateTime.Now.Month; i++)
                    {
                        switch (i.ToString())
                        {
                            case "1":
                               _months.Add("Jan");
                                break;
                            case "2":
                                _months.Add("Feb");
                                break;
                            case "3":
                                _months.Add("Mar");
                                break;
                            case "4":
                                _months.Add("Apr");
                                break;
                            case "5":
                                _months.Add("May");
                                break;
                            case "6":
                                _months.Add("Jun");
                                break;
                            case "7":
                                _months.Add("Jul");
                                break;
                            case "8":
                                _months.Add("Aug");
                                break;
                            case "9":
                                _months.Add("Sep");
                                break;
                            case "10":
                                _months.Add("Oct");
                                break;
                            case "11":
                                _months.Add("Nov");
                                break;
                            case "12":
                                _months.Add("Dec");
                                break;
                        }
                    }

                    foreach (var item in _Main)
                    {
                        if (item.TotalAmount !=null)
                        {
                            if (item.TotalAmount.Contains("-----------------"))
                            {
                                break;
                            }
                            double _OBR = 0.00;
                   
                            foreach (string item_months in _months)
                            {
                                double _Total = 0.00;
                                List<OBR_Details> _data = ListGridDataDetails.Where(x => x.AccountCode == item.UACS && x._Month == item_months).ToList();
                          
                                foreach (OBR_Details item_OBR in _data)
                                {
                                    _Total += Convert.ToDouble(item_OBR.Total);

                                    _OBR += Convert.ToDouble(item_OBR.Total);
                                }
                                switch (item_months)
                                {
                                    case "Jan":
                                        if (_Total == 0)
                                        {
                                            item.Jan = "(" + Convert.ToDouble(item.Jan).ToString("#,##0.00") + ")";
                                        }
                                        else
                                        {
                                            item.Jan = _Total.ToString("#,##0.00");
                                        }
                                        break;
                                    case "Feb":
                                        if (_Total == 0)
                                        {
                                            item.Feb = "(" + Convert.ToDouble(item.Feb).ToString("#,##0.00") + ")";
                                        }
                                        else
                                        {
                                            item.Feb = _Total.ToString("#,##0.00");
                                        }
                                        break;
                                    case "Mar":
                                        if (_Total == 0)
                                        {
                                            item.Mar = "(" + Convert.ToDouble(item.Mar).ToString("#,##0.00") + ")";
                                        }
                                        else
                                        {
                                            item.Mar = _Total.ToString("#,##0.00");
                                        }
                                        break;
                                    case "Apr":
                                        if (_Total == 0)
                                        {
                                            item.Apr = "(" + Convert.ToDouble(item.Apr).ToString("#,##0.00") + ")";
                                        }
                                        else
                                        {
                                            item.Apr = _Total.ToString("#,##0.00");
                                        }
                                        break;
                                    case "May":
                                        if (_Total == 0)
                                        {
                                            item.May = "(" + Convert.ToDouble(item.May).ToString("#,##0.00") + ")";
                                        }
                                        else
                                        {
                                            item.May = _Total.ToString("#,##0.00");
                                        }
                                        break;
                                    case "Jun":
                                        if (_Total == 0)
                                        {
                                            item.Jun = "(" + Convert.ToDouble(item.Jun).ToString("#,##0.00") + ")";
                                        }
                                        else
                                        {
                                            item.Jun = _Total.ToString("#,##0.00");
                                        }
                                        break;
                                    case "Jul":
                                        if (_Total == 0)
                                        {
                                            item.Jul = "(" + Convert.ToDouble(item.Jul).ToString("#,##0.00") + ")";
                                        }
                                        else
                                        {
                                            item.Jul = _Total.ToString("#,##0.00");
                                        }
                                        break;

                                    case "Aug":
                                        if (_Total == 0)
                                        {
                                            item.Aug = "(" + Convert.ToDouble(item.Aug).ToString("#,##0.00") + ")";
                                        }
                                        else
                                        {
                                            item.Aug = _Total.ToString("#,##0.00");
                                        }
                                        break;
                                    case "Sep":
                                        if (_Total == 0)
                                        {
                                            item.Sep = "(" + Convert.ToDouble(item.Sep).ToString("#,##0.00") + ")";
                                        }
                                        else
                                        {
                                            item.Sep = _Total.ToString("#,##0.00");
                                        }
                                        break;
                                    case "Oct":
                                        if (_Total == 0)
                                        {
                                            item.Oct = "(" + Convert.ToDouble(item.Oct).ToString("#,##0.00") + ")";
                                        }
                                        else
                                        {
                                            item.Oct = _Total.ToString("#,##0.00");
                                        }
                                        break;
                                    case "Nov":
                                        if (_Total == 0)
                                        {
                                            item.Nov = "(" + Convert.ToDouble(item.Nov).ToString("#,##0.00") + ")";
                                        }
                                        else
                                        {
                                            item.Nov = _Total.ToString("#,##0.00");
                                        }
                                        break;
                                    case "Dec":
                                        if (_Total == 0)
                                        {
                                            item.Dec = "(" + Convert.ToDouble(item.Dec).ToString("#,##0.00") + ")";
                                        }
                                        else
                                        {
                                            item.Dec = _Total.ToString("#,##0.00");
                                        }
                                        break;
                                }
                          
                           
                            }
                            item.TotalOBR = _OBR.ToString("#,##0.00");
                            item.RemainingBalance = (Convert.ToDouble(item.Allocation) - _OBR).ToString("#,##0.00");
                        }
                    }

                    FetchActivityData();

                  
                 
                    break;


            }
        }

        private void FetchForRevision() 
        {
            c_ppmp.Process = "FetchForRevision";
            svc_mindaf.ExecuteSQLAsync(c_ppmp.FetchForRevision(this.DivId));
        }

        private void FetchRevisionStatus()
        {
            c_ppmp.Process = "FetchRevisionStatus";
            svc_mindaf.ExecuteSQLAsync(c_ppmp.FetchRevisionStatus(this.DivId,this.WorkingYear));
        }

        private void FetchRAF()
        {
            c_ppmp.Process = "FetchRAF";
            svc_mindaf.ExecuteSQLAsync(c_ppmp.FetchRAFRevision(this.DivId));
        }
        private Boolean IsApproved = false;
        private Boolean ForRevision = false;
        private String RafStatus = "";
        private RafRevision RafData;
        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
           var _results = e.Result.ToString();
           switch (c_ppmp.Process)
           {
               case "FetchRAF":
                   XDocument oDocKeyResultsFetchRAF = XDocument.Parse(_results);
                   var _dataListsFetchRAF = from info in oDocKeyResultsFetchRAF.Descendants("Table")
                                                    select new RafRevision
                                                    {
                                                        EntryDate = Convert.ToString(info.Element("entry_date").Value),
                                                        Id = Convert.ToString(info.Element("id").Value),
                                                        RafNo = Convert.ToString(info.Element("number").Value),
                                                        AlignId = Convert.ToString(info.Element("alignment_id").Value)
                                                    };

                   String _RafData = "";
                   RafData = new RafRevision();
                   foreach (var item in _dataListsFetchRAF)
                   {
                       RafData.EntryDate = item.EntryDate;
                       RafData.Id = item.Id;
                       RafData.RafNo = item.RafNo;
                       RafData.AlignId = item.AlignId;
                       _RafData = "RAF No :  " + item.RafNo + "  Dated :  " + Convert.ToDateTime(item.EntryDate).ToShortDateString();
                       break;
                   }
                   if (ForRevision)
                   {
                       if (btnSave.Content.ToString() =="Request Modification" && RafData.AlignId !=null)
                       {
                           lblRaf.Content = "Revision is being submitted for approval.";
                           lblRaf.Visibility = System.Windows.Visibility.Visible;
                           btnSave.IsEnabled = false;
                           return;
                       }
                       else
                       {
                           lblRaf.Content = _RafData;
                           lblRaf.Visibility = System.Windows.Visibility.Visible;
                       }
                 
                   }

                   this.Cursor = Cursors.Arrow;

                   break;
               case "FetchRevisionStatus":
                   XDocument oDocKeyResultsFetchRevisionStatus = XDocument.Parse(_results);
                   var _dataListsFetchRevisionStatus = from info in oDocKeyResultsFetchRevisionStatus.Descendants("Table")
                                                    select new RafStatus
                                                             {
                                                                 Status = Convert.ToString(info.Element("status").Value)

                                                             };

                   ForRevision = false;
                   String RafStatus = "";
                   foreach (var item in _dataListsFetchRevisionStatus)
                   {
                       RafStatus = item.Status;
                       break;

                   }

                  

                   if (ForRevision)
                   {
                       FetchRAF();
                   }

                   switch (RafStatus)
	                {
                       case "Served":
                            grdData.IsEnabled = false;
                            btnSave.Content = "New Revision";                           
                            break;
		        
	                }

                   //if (_BudgetRevision.Count!=0)
                   //{
                   //    btnSave.Content = "Request Modification";
                   //}

                   BudgetRevisionMain _totals_line = new BudgetRevisionMain();
                   _totals_line.ExpenseItem = "---------------------------------------------------";
                   _totals_line.Allocation = "-----------------";
                   _totals_line.TotalExpense = "-----------------";
                   _totals_line.Adjustment = "-----------------";
                   _totals_line.TotalOBR = "-----------------";
                   _totals_line.RemainingBalance = "-----------------";
                   _totals_line.Jan = "-----------------";
                   _totals_line.Feb = "-----------------";
                   _totals_line.Mar = "-----------------";
                   _totals_line.Apr = "-----------------";
                   _totals_line.May = "-----------------";
                   _totals_line.Jun = "-----------------";
                   _totals_line.Jul = "-----------------";
                   _totals_line.Aug = "-----------------";
                   _totals_line.Sep = "-----------------";
                   _totals_line.Oct = "-----------------";
                   _totals_line.Nov = "-----------------";
                   _totals_line.Dec = "-----------------";
                   _totals_line.TotalAmount = "-----------------";


                   BudgetRevisionMain _totals_ = new BudgetRevisionMain();
                   double _TotalAllocation = 0.00;
                   double _TotalExpense = 0.00;
                   double _TotalOBR = 0.00;
                   double _TotalBalance = 0.00;
                   double _TotalAmount = 0.00;

                   double _Jan = 0.00;
                   double _Feb = 0.00;
                   double _Mar = 0.00;
                   double _Apr = 0.00;
                   double _May = 0.00;
                   double _Jun = 0.00;
                   double _Jul = 0.00;
                   double _Aug = 0.00;
                   double _Sep = 0.00;
                   double _Oct = 0.00;
                   double _Nov = 0.00;
                   double _Dec = 0.00;

                   foreach (var item in _Main)
                   {
                       if (item.Allocation == null) { item.Allocation = "0.00"; }
                       if (item.TotalExpense == null) { item.TotalExpense = "0.00"; }
                       if (item.TotalOBR == null) { item.TotalOBR = "0.00"; }
                       if (item.RemainingBalance == null) { item.RemainingBalance = "0.00"; }
                       if (item.Adjustment == null) { item.Adjustment = "0.00"; }
                       if (item.Jan == null) { item.Jan = "0.00"; }
                       if (item.Feb == null) { item.Feb = "0.00"; }
                       if (item.Mar == null) { item.Mar = "0.00"; }
                       if (item.Apr == null) { item.Apr = "0.00"; }
                       if (item.May == null) { item.May = "0.00"; }
                       if (item.Jun == null) { item.Jun = "0.00"; }
                       if (item.Jul == null) { item.Jul = "0.00"; }
                       if (item.Aug == null) { item.Aug = "0.00"; }
                       if (item.Sep == null) { item.Sep = "0.00"; }
                       if (item.Oct == null) { item.Oct = "0.00"; }
                       if (item.Nov == null) { item.Nov = "0.00"; }
                       if (item.Dec == null) { item.Dec = "0.00"; }
                   }

                   foreach (var item in _Main)
                   {
                       _TotalAllocation += Convert.ToDouble(item.Allocation);
                       _TotalExpense += Convert.ToDouble(item.TotalExpense);
                       _TotalOBR += Convert.ToDouble(item.TotalOBR);
                       _TotalBalance += Convert.ToDouble(item.RemainingBalance);

                       String _val = "";
                       if (item.Jan.Contains("("))
                       {
                           _val = item.Jan;
                           _val = item.Jan.Replace("(", "");
                           _val = _val.Replace(")", "");
                           _Jan += Convert.ToDouble(_val);
                       }
                       else
                       {
                           _val = item.Jan;
                           _Jan += Convert.ToDouble(_val);
                       }
                       _TotalAmount += Convert.ToDouble(_val);
                       if (item.Feb.Contains("("))
                       {
                           _val = item.Feb;
                           _val = item.Feb.Replace("(", "");
                           _val = _val.Replace(")", "");
                           _Feb += Convert.ToDouble(_val);
                       }
                       else
                       {
                           _val = item.Feb;
                           _Feb += Convert.ToDouble(_val);
                       }
                       _TotalAmount += Convert.ToDouble(_val);
                       if (item.Mar.Contains("("))
                       {
                           _val = item.Mar;
                           _val = item.Mar.Replace("(", "");
                           _val = _val.Replace(")", "");
                           _Mar += Convert.ToDouble(_val);
                       }
                       else
                       {
                           _val = item.Mar;
                           _Mar += Convert.ToDouble(_val);
                       }
                       _TotalAmount += Convert.ToDouble(_val);
                       if (item.Apr.Contains("("))
                       {
                           _val = item.Apr;
                           _val = item.Apr.Replace("(", "");
                           _val = _val.Replace(")", "");
                           _Apr += Convert.ToDouble(_val);
                       }
                       else
                       {
                           _val = item.Apr;
                           _Apr += Convert.ToDouble(_val);
                       }
                       _TotalAmount += Convert.ToDouble(_val);
                       if (item.May.Contains("("))
                       {
                           _val = item.May;
                           _val = item.May.Replace("(", "");
                           _val = _val.Replace(")", "");
                           _May += Convert.ToDouble(_val);
                       }
                       else
                       {
                           _val = item.May;
                           _May += Convert.ToDouble(_val);
                       }
                       _TotalAmount += Convert.ToDouble(_val);
                       if (item.Jun.Contains("("))
                       {
                           _val = item.Jun;
                           _val = item.Jun.Replace("(", "");
                           _val = _val.Replace(")", "");
                           _Jun += Convert.ToDouble(_val);
                       }
                       else
                       {
                           _val = item.Jun;
                           _Jun += Convert.ToDouble(_val);
                       }
                       _TotalAmount += Convert.ToDouble(_val);
                       if (item.Jul.Contains("("))
                       {
                           _val = item.Jul;
                           _val = item.Jul.Replace("(", "");
                           _val = _val.Replace(")", "");
                           _Jul += Convert.ToDouble(_val);
                       }
                       else
                       {
                           _val = item.Jul;
                           _Jul += Convert.ToDouble(_val);
                       }
                       _TotalAmount += Convert.ToDouble(_val);
                       if (item.Aug.Contains("("))
                       {
                           _val = item.Aug;
                           _val = item.Aug.Replace("(", "");
                           _val = _val.Replace(")", "");
                           _Aug += Convert.ToDouble(_val);
                       }
                       else
                       {
                           _val = item.Aug;
                           _Aug += Convert.ToDouble(_val);
                       }
                       _TotalAmount += Convert.ToDouble(_val);
                       if (item.Sep.Contains("("))
                       {
                           _val = item.Sep;
                           _val = item.Sep.Replace("(", "");
                           _val = _val.Replace(")", "");
                           _Sep += Convert.ToDouble(_val);
                       }
                       else
                       {
                           _val = item.Sep;
                           _Sep += Convert.ToDouble(_val);
                       }
                       _TotalAmount += Convert.ToDouble(_val);
                       if (item.Oct.Contains("("))
                       {
                           _val = item.Oct;
                           _val = item.Oct.Replace("(", "");
                           _val = _val.Replace(")", "");
                           _Oct += Convert.ToDouble(_val);
                       }
                       else
                       {
                           _val = item.Oct;
                           _Oct += Convert.ToDouble(_val);
                       }
                       _TotalAmount += Convert.ToDouble(_val);
                       if (item.Nov.Contains("("))
                       {
                           _val = item.Nov;
                           _val = item.Nov.Replace("(", "");
                           _val = _val.Replace(")", "");
                           _Nov += Convert.ToDouble(_val);
                       }
                       else
                       {
                           _val = item.Nov;
                           _Nov += Convert.ToDouble(_val);
                       }
                       _TotalAmount += Convert.ToDouble(_val);
                       if (item.Dec.Contains("("))
                       {
                           _val = item.Dec;
                           _val = item.Dec.Replace("(", "");
                           _val = _val.Replace(")", "");
                           _Dec += Convert.ToDouble(_val);
                       }
                       else
                       {
                           _val = item.Dec;
                           _Dec += Convert.ToDouble(_val);
                       }
                       _TotalAmount += Convert.ToDouble(_val);
                       item.TotalAmount = _TotalAmount.ToString("#,##0.00");
                       _TotalAmount = 0;

                   }
                   double _PostiveVal = 0.00;
                   double _NegativeVal = 0.00;


                   foreach (var item in _Main)
                   {
                       String _val = "";
                       if (item.Adjustment.Contains("("))
                       {
                           _val = item.Adjustment;
                           _val = item.Adjustment.Replace("(", "");
                           _val = _val.Replace(")", "");
                           _NegativeVal += Convert.ToDouble(_val);
                       }
                       else
                       {
                           _val = item.Adjustment;
                           _PostiveVal += Convert.ToDouble(_val);
                       }
                   }
                   _totals_.ExpenseItem = "Total";
                   _totals_.Allocation = _TotalAllocation.ToString("#,##0.00");
                   _totals_.TotalExpense = _TotalExpense.ToString("#,##0.00");
                   _totals_.TotalOBR = _TotalOBR.ToString("#,##0.00");
                   _totals_.RemainingBalance = _TotalBalance.ToString("#,##0.00");
                   _totals_.Adjustment = (_PostiveVal - _NegativeVal).ToString("#,##0.00");
                   _totals_.Jan = _Jan.ToString("#,##0.00");
                   _totals_.Feb = _Feb.ToString("#,##0.00");
                   _totals_.Mar = _Mar.ToString("#,##0.00");
                   _totals_.Apr = _Apr.ToString("#,##0.00");
                   _totals_.May = _May.ToString("#,##0.00");
                   _totals_.Jun = _Jun.ToString("#,##0.00");
                   _totals_.Jul = _Jul.ToString("#,##0.00");
                   _totals_.Aug = _Aug.ToString("#,##0.00");
                   _totals_.Sep = _Sep.ToString("#,##0.00");
                   _totals_.Oct = _Oct.ToString("#,##0.00");
                   _totals_.Nov = _Nov.ToString("#,##0.00");
                   _totals_.Dec = _Dec.ToString("#,##0.00");
                   _totals_.TotalAmount = (_Jan + _Feb + _Mar + _Apr + _May + _Jun + _Jul + _Aug + _Sep + _Oct + _Nov + _Dec).ToString("#,##0.00");
                   _Main.Add(_totals_line);
                   _Main.Add(_totals_);

                   grdData.ItemsSource = null;
                   grdData.ItemsSource = _Main;
                   FixColumns();


                   //if (IsApproved)
                   //{
                   //    btnSave.IsEnabled = false;
                   //    //btnSave.Content = "Request Modification";
                   //    //btnClearEntry.IsEnabled = false;
                   //    //  grdData.IsEnabled = false;
                   //}
                   //else
                   //{
                   //    // grdData.IsEnabled = true;
                   //    btnClearEntry.IsEnabled = true;
                   //    btnSave.Content = "Save Revision";
                   //}


                   this.Cursor = Cursors.Arrow;

                   break;
               case "FetchBudgetRevisionTitles":
                   XDocument oDocKeyResultsFetchBudgetRevisionTitles = XDocument.Parse(_results);
                   var _dataListsFetchBudgetRevisionTitles = from info in oDocKeyResultsFetchBudgetRevisionTitles.Descendants("Table")
                                                       select new BudgetRevisionTitles
                                                       {
                                                           Title = Convert.ToString(info.Element("title").Value)
                                                          
                                                       };

                   _BudgetRevisionTitles.Clear();
                  
                   foreach (var item in _dataListsFetchBudgetRevisionTitles)
                   {

                       BudgetRevisionTitles _varProf = new BudgetRevisionTitles();

                       _varProf.Title = item.Title;
                 
                       _BudgetRevisionTitles.Add(_varProf);
                    
                   }

                   FetchBudgetRevision();
                   this.Cursor = Cursors.Arrow;

                   break;
               case "FetchBudgetRevision":
                   XDocument oDocKeyResultsFetchBudgetRevision = XDocument.Parse(_results);
                   var _dataListsFetchBudgetRevision = from info in oDocKeyResultsFetchBudgetRevision.Descendants("Table")
                                     select new BudgetRevisionItems
                                    {
                                        division_pap = Convert.ToString(info.Element("division_pap").Value),
                                        division_year = Convert.ToString(info.Element("division_year").Value),
                                        from_total = Convert.ToString(info.Element("from_total").Value),
                                        from_uacs = Convert.ToString(info.Element("from_uacs").Value),
                                        fundsource = Convert.ToString(info.Element("fundsource").Value),
                                        to_uacs = Convert.ToString(info.Element("to_uacs").Value),
                                        total_alignment = Convert.ToString(info.Element("total_alignment").Value),
                                        from_expense_item = Convert.ToString(info.Element("from_expense_item").Value),
                                        to_expense_item = Convert.ToString(info.Element("to_expense_item").Value),
                                        is_approved = Convert.ToString(info.Element("is_approved").Value)
                                    };

                   _BudgetRevision.Clear();

                   foreach (var item in _dataListsFetchBudgetRevision)
                   {

                       BudgetRevisionItems _varProf = new BudgetRevisionItems();

                       _varProf.division_pap = item.division_pap;
                       _varProf.division_year = item.division_year;
                        _varProf.from_total = item.from_total;
                       _varProf.from_uacs = item.from_uacs;
                        _varProf.fundsource = item.fundsource;
                       _varProf.to_uacs = item.to_uacs;
                        _varProf.total_alignment = item.total_alignment;
                        _varProf.from_expense_item = item.from_expense_item;
                        _varProf.to_expense_item = item.to_expense_item;
                        _varProf.is_approved = item.is_approved;
                       _BudgetRevision.Add(_varProf);

                   }

                   FetchFundSource();
                   this.Cursor = Cursors.Arrow;
                  
                   break;
               case "FetchBudgetAllocation":
                   XDocument oDocKeyResults = XDocument.Parse(_results);
                   var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                    select new BudgetRevision
                                    {
                                        Uacs = Convert.ToString(info.Element("uacs_code").Value),
                                        ExpenseItem = Convert.ToString(info.Element("expense_item").Value),
                                        Amount = Convert.ToString(info.Element("Amount").Value)
                           
                                    };

                   _BudgetItems.Clear();

                   foreach (var item in _dataLists)
                   {

                       BudgetRevision _varProf = new BudgetRevision();

                       _varProf.Uacs = item.Uacs;
                           _varProf.ExpenseItem = item.ExpenseItem;
                           _varProf.Amount = Convert.ToDouble(item.Amount).ToString("#,##0.00");;

                           _BudgetItems.Add(_varProf);
                   
                   }

                 
                   this.Cursor = Cursors.Arrow;
                   FetchBudgetRAFNo();
                 //  FetchActivityDataMonths();
               
                   break;
               case "FetchBudgetRAFNo":
                   XDocument oDocKeyResultsFetchBudgetRAFNo = XDocument.Parse(_results);
                   var _dataListsFetchBudgetRAFNo = from info in oDocKeyResultsFetchBudgetRAFNo.Descendants("Table")
                                    select new DivisionRafNo
                                    {
                                        Raf = Convert.ToString(info.Element("number").Value)

                                    };

                   ListRafNo.Clear();

                   foreach (var item in _dataListsFetchBudgetRAFNo)
                   {

                       DivisionRafNo _varProf = new DivisionRafNo();

                       _varProf.Raf = item.Raf;


                       ListRafNo.Add(_varProf);

                   }


                   this.Cursor = Cursors.Arrow;
                   FetchBudgetAdjustments();
                   //  FetchActivityDataMonths();

                   break;
               case "FetchBudgetAdjustments":
                   XDocument oDocKeyResultsFetchBudgetAdjustments = XDocument.Parse(_results);
                   var _dataListsFetchBudgetAdjustments = from info in oDocKeyResultsFetchBudgetAdjustments.Descendants("Table")
                                                          select new DivisionAdjustment
                                                    {
                                                        Raf = Convert.ToString(info.Element("number").Value),
                                                        Mooe_Id = Convert.ToString(info.Element("mooe_id").Value),
                                                        Adjustments = Convert.ToString(info.Element("adjustment").Value),
                                                        PlusMinus = Convert.ToString(info.Element("plusminus").Value)

                                                    };

                   ListAdjustments.Clear();

                   foreach (var item in _dataListsFetchBudgetAdjustments)
                   {

                       DivisionAdjustment _varProf = new DivisionAdjustment();

                       _varProf.Adjustments = item.Adjustments;
                       _varProf.Mooe_Id = item.Mooe_Id;
                       _varProf.PlusMinus = item.PlusMinus;
                       _varProf.Raf = item.Raf;

                       ListAdjustments.Add(_varProf);

                   }
                   
                   foreach (DivisionRafNo item in ListRafNo)
                   {
                       List<DivisionAdjustment>  _filtered = ListAdjustments.Where(x=>x.Raf==item.Raf).ToList();

                       foreach (BudgetRevision item_f in _BudgetItems)
	                    {
                            List<DivisionAdjustment> _selection = _filtered.Where(x => x.Mooe_Id == item_f.Uacs).ToList();
                            if (_selection.Count!=0)
                            {
                                foreach (DivisionAdjustment item_s in _selection)
                                {
                                    double _adjustment = Convert.ToDouble(item_s.Adjustments);
                                    double _allocation = Convert.ToDouble(item_f.Amount);
                                    double _new_allocation = 0.00;

                                    switch (item_s.PlusMinus)
                                    {
                                        case "+":

                                            _new_allocation = _allocation + _adjustment;
                                            break;

                                        case "-":
                                            _new_allocation = _allocation - _adjustment;
                                            break;
                                        
                                    }

                                    item_f.Amount = _new_allocation.ToString("#,##0.00");
                                }
                            }
	                    }

                   }
                   
                   


                   this.Cursor = Cursors.Arrow;
                    FetchActivityDataMonths();

                   break;
               case "FetchFundSource":
                   XDocument oDocKeyResultsFetchFundSource = XDocument.Parse(_results);
                   var _dataListsFetchFundSource = from info in oDocKeyResultsFetchFundSource.Descendants("Table")
                                                   select new BudgetRevisionFundSource
                                    {
                                        Code = Convert.ToString(info.Element("code").Value),
                                        Name = Convert.ToString(info.Element("respo_name").Value),
                                        FundType = Convert.ToString(info.Element("fund_type").Value)
                                    };



                   foreach (var item in _dataListsFetchFundSource)
                   {
                       
                       FundSource = item.Code;
                       FundName = item.Name;
              
                   }

                   this.Cursor = Cursors.Arrow;
                   FetchOBRData();
                   break;
               case "FetchActivityDataMonths":
                   XDocument oDocKeyResultsFetchActivityDataMonths = XDocument.Parse(_results);
                   var _dataListsFetchActivityDataMonths = from info in oDocKeyResultsFetchActivityDataMonths.Descendants("Table")
                                                           select new BudgetActivityItemsMonths
                                                   {
                                                       name = Convert.ToString(info.Element("name").Value),
                                                       total = Convert.ToString(info.Element("total").Value),
                                                        month = Convert.ToString(info.Element("month").Value),
                                                       uacs_code = Convert.ToString(info.Element("uacs_code").Value)

                                                   };



                   _BudgetActivityItemsMonths.Clear();
                   foreach (var item in _dataListsFetchActivityDataMonths)
                   {

                       BudgetActivityItemsMonths _varProf = new BudgetActivityItemsMonths();

                       _varProf.name = item.name;
                       _varProf.total =item.total ;
                       _varProf.month = item.month;
                       _varProf.uacs_code = item.uacs_code;

                       _BudgetActivityItemsMonths.Add(_varProf);

                   }

                   this.Cursor = Cursors.Arrow;
                   FetchBudgetRevisionTitles();
                 
                   break;
               case "FetchActivityData":
                   XDocument oDocKeyResultsFetchActivityData = XDocument.Parse(_results);
                   var _dataListsFetchActivityData = from info in oDocKeyResultsFetchActivityData.Descendants("Table")
                                                   select new BudgetActivityItems
                                                   {
                                                      name = Convert.ToString(info.Element("name").Value),
                                                      total  = Convert.ToString(info.Element("total").Value),
                                                      uacs_code  = Convert.ToString(info.Element("uacs_code").Value)


                                                   };


                   _BudgetActivityItems.Clear();
                   foreach (var item in _dataListsFetchActivityData)
                   {

                       BudgetActivityItems _varProf = new BudgetActivityItems();

                       _varProf.name = item.name;
                       _varProf.total =item.total ;
                       _varProf.uacs_code = item.uacs_code;

                       _BudgetActivityItems.Add(_varProf);

                   }
                   _Main.Clear();

                   foreach (BudgetRevision item in _BudgetItems)
                   {
                       BudgetRevisionMain _items = new BudgetRevisionMain();
                       _items.UACS = item.Uacs;
                       _items.ExpenseItem = item.ExpenseItem;
                       //List<BudgetRevisionItems> _revised = _BudgetRevision.Where(x => x.from_expense_item == item.ExpenseItem).ToList();

                       //if (_revised.Count != 0)
                       //{
                       //    double _totalMinus = 0.00;
                       //    foreach (BudgetRevisionItems itemData in _revised)
                       //    {
                       //        _totalMinus += Convert.ToDouble(itemData.total_alignment);
                       //    }

                       //    _items.Allocation = (Convert.ToDouble(item.Amount) - _totalMinus).ToString("#,##0.00");
                       //}
                       //else
                       //{
                           _items.Allocation = Convert.ToDouble(item.Amount).ToString("#,##0.00");
                      // }
                      

                     

                       List<BudgetActivityItems> _act_items = _BudgetActivityItems.Where(x => x.name == item.ExpenseItem).ToList();

                       double _total = 0.00;

                       foreach (BudgetActivityItems item_budget in _act_items)
                       {
                           _total += Convert.ToDouble(item_budget.total);    
                       }
                       _items.TotalExpense = _total.ToString("#,##0.00");
                  //     _items.BalanceContigency = (Convert.ToDouble(item.Amount) - _total).ToString("#,##0.00");




                       _Main.Add(_items);

                   }

                   foreach (BudgetRevisionMain _DataDetail in _Main)
                   {
                       List<BudgetActivityItemsMonths> item_months = _BudgetActivityItemsMonths.Where(x => x.name == _DataDetail.ExpenseItem).ToList();
                       double _janTotal = 0.00;
                       double _febTotal = 0.00;
                       double _marTotal = 0.00;
                       double _aprTotal = 0.00;
                       double _mayTotal = 0.00;
                       double _junTotal = 0.00;
                       double _julTotal = 0.00;
                       double _augTotal = 0.00;
                       double _sepTotal = 0.00;
                       double _octTotal = 0.00;
                       double _novTotal = 0.00;
                       double _decTotal = 0.00;
                       double _LineTotal = 0.00;

                       foreach (BudgetActivityItemsMonths item_data in item_months)
                       {
                          
                            _LineTotal+= Convert.ToDouble(item_data.total);
                           switch (item_data.month)
                           {
                               case "Jan":                                 
                                   _janTotal += Convert.ToDouble(item_data.total);
                                   _DataDetail.Jan = _janTotal.ToString("#,##0.00");
                                   
                                   break;
                               case "Feb":
                                   _febTotal += Convert.ToDouble(item_data.total);
                                   _DataDetail.Feb = _febTotal.ToString("#,##0.00");
                                  
                                   break;
                               case "Mar":
                                   _marTotal += Convert.ToDouble(item_data.total);
                                   _DataDetail.Mar = _marTotal.ToString("#,##0.00");
                                   break;
                               case "Apr":
                                    _aprTotal += Convert.ToDouble(item_data.total);
                                    _DataDetail.Apr = _aprTotal.ToString("#,##0.00");
                                   break;
                               case "May":
                                   _mayTotal += Convert.ToDouble(item_data.total);
                                    _DataDetail.May = _mayTotal.ToString("#,##0.00");
                                   break;
                               case "Jun":
                                    _junTotal += Convert.ToDouble(item_data.total);
                                    _DataDetail.Jun = _junTotal.ToString("#,##0.00");
                                   break;
                               case "Jul":
                                   _julTotal += Convert.ToDouble(item_data.total);
                                    _DataDetail.Jul = _julTotal.ToString("#,##0.00");
                                   break;

                               case "Aug":
                                    _augTotal += Convert.ToDouble(item_data.total);
                                    _DataDetail.Aug = _augTotal.ToString("#,##0.00");
                                   break;
                               case "Sep":
                                     _sepTotal += Convert.ToDouble(item_data.total);
                                    _DataDetail.Sep = _sepTotal.ToString("#,##0.00");
                                   break;
                               case "Oct":
                                    _octTotal += Convert.ToDouble(item_data.total);
                                    _DataDetail.Oct = _octTotal.ToString("#,##0.00");
                                   break;
                               case "Nov":
                                   _novTotal += Convert.ToDouble(item_data.total);
                                    _DataDetail.Nov = _novTotal.ToString("#,##0.00");
                                   break;
                               case "Dec":
                                   _decTotal += Convert.ToDouble(item_data.total);
                                   _DataDetail.Dec = _decTotal.ToString("#,##0.00");
                                   break;
                           }
                           _DataDetail.TotalAmount = _LineTotal.ToString("#,##0.00");
                       }
                   }

                   if (_BudgetRevision.Count!=0)
                   {
                       // Switch Main Computation Variable To Refference BudgetRevision Data
                       var results = from p in _BudgetRevision
                                     group p by p.to_expense_item into g
                                     select new
                                     {
                                         Expense = g.Key
                                     };


                       foreach (var item in results)
                       {
                           List<BudgetRevisionMain> _found = _Main.Where(x => x.ExpenseItem == item.Expense.ToString()).ToList();
                           
                           if (_found.Count==0)
                           {
                               BudgetRevisionMain _AddedItems = new BudgetRevisionMain();
                               String _UACS= _BudgetRevision.Where(x => x.to_expense_item == item.Expense).ToList()[0].to_uacs;
                               _AddedItems.ExpenseItem = item.Expense.ToString();
                               _AddedItems.Status = "New";
                               _AddedItems.UACS = _UACS;
                               _Main.Add(_AddedItems);
                               continue;
                           }
                       }

                       foreach (var item in _Main)
                       {
                           List<BudgetRevisionItems> xdata_from = _BudgetRevision.Where(x => x.from_expense_item == item.ExpenseItem && x.is_approved == "false").ToList();
                           List<BudgetRevisionItems> xdata_to = _BudgetRevision.Where(x => x.to_expense_item == item.ExpenseItem && x.is_approved=="false").ToList();
                           List<OBR_Details> xdata_obr = ListGridDataDetails.Where(x => x.AccountCode == item.UACS).ToList();

                           double _totalOBR = 0.00;
                           double _totalMinus = 0.00;
                           double _totalPlus = 0.00;
                           Boolean _isApproved = false;

                           foreach (var itemobr in xdata_obr)
                           {
                               _totalOBR += Convert.ToDouble(itemobr.Total);   
                             
                           }

                           foreach (var itemfrom in xdata_from)
                           {
                               _totalMinus += Convert.ToDouble(itemfrom.total_alignment);
                               _isApproved = Convert.ToBoolean(itemfrom.is_approved);
                               IsApproved = true;
                           }

                           foreach (var itemto in xdata_to)
                           {
                               _totalPlus += Convert.ToDouble(itemto.total_alignment);
                               _isApproved = Convert.ToBoolean(itemto.is_approved);
                               IsApproved = true;
                           }
                           if (_isApproved)
                           {
                               
                            
                               if (_totalMinus!=0)
                               {
                                   //String _converted = _totalMinus.ToString();
                                   
                                   
                                   //item.Allocation = (Convert.ToDouble(item.Allocation) - (Convert.ToDouble(_converted))).ToString("#,##0.00");

                                   item.TotalExpense = (Convert.ToDouble(item.Allocation) - _totalOBR).ToString("#,##0.00");
                               }
                               else if(_totalPlus!=0)
                               {
                                   //String _converted = _totalPlus.ToString();
                                

                                   //item.Allocation = (Convert.ToDouble(item.Allocation) + (Convert.ToDouble(_converted))).ToString("#,##0.00");

                                   item.TotalExpense = (Convert.ToDouble(item.Allocation) - _totalOBR).ToString("#,##0.00");
                                   
                               }
                               item.Adjustment = "0.00";
                               item.TotalOBR = _totalOBR.ToString("#,##0.00");
                               item.RemainingBalance = (Convert.ToDouble(item.Allocation) - _totalOBR).ToString("#,##0.00");

                           }
                           else
                           {
                               if (_totalMinus != 0)
                               {
                                   item.Adjustment = "(" + _totalMinus.ToString("#,##0.00") + ")";
                               }
                               else
                               {
                                   item.Adjustment = _totalPlus.ToString("#,##0.00");
                               }


                               item.TotalOBR = _totalOBR.ToString("#,##0.00");
                               item.RemainingBalance = (Convert.ToDouble(item.Allocation) - _totalOBR).ToString("#,##0.00");
                               if (item.Adjustment.Contains("("))
                               {
                                   item.TotalExpense = ((Convert.ToDouble(item.Allocation) - _totalMinus) - _totalOBR).ToString("#,##0.00");
                               }
                               else
                               {
                                   item.TotalExpense = ((Convert.ToDouble(item.Allocation) + _totalPlus) - _totalOBR).ToString("#,##0.00");
                               }
                           }
                          
                       }

                       FetchRevisionStatus();
                    //   FetchForRevision();

                       this.Cursor = Cursors.Arrow;



                       break;

                   }
                   else
                   {
                       foreach (var item in _Main)
                       {
                          
                           List<OBR_Details> xdata_obr = ListGridDataDetails.Where(x => x.AccountCode == item.UACS).ToList();

                           double _totalOBR = 0.00;
                           double _totalMinus = 0.00;
                           double _totalPlus = 0.00;

                           foreach (var itemobr in xdata_obr)
                           {
                               _totalOBR += Convert.ToDouble(itemobr.Total);
                           }

                  


                           item.TotalOBR = _totalOBR.ToString("#,##0.00");
                           item.RemainingBalance = (Convert.ToDouble(item.Allocation) - _totalOBR).ToString("#,##0.00");

                           item.TotalExpense = "0.00";

                       }
                       FetchRevisionStatus();
                     //  FetchForRevision();

                       this.Cursor = Cursors.Arrow;
                   }
                

                 //  FetchForRevision();
                 ////  FetchOBRData();
                 //  this.Cursor = Cursors.Arrow;
         
                   break;
           }

         
        }

        private void FixColumns()
        {

          //  Column col_Status = grdData.Columns.DataColumns["Status"];
            Column col_ExpenseItem = grdData.Columns.DataColumns["ExpenseItem"];
            Column col_Allocation = grdData.Columns.DataColumns["Allocation"];
            Column col_TotalExpense = grdData.Columns.DataColumns["TotalExpense"];
         //   Column col_BalanceContigency = grdData.Columns.DataColumns["BalanceContigency"];
            Column col_Adjustment = grdData.Columns.DataColumns["Adjustment"];
            Column col_TotalOBR = grdData.Columns.DataColumns["TotalOBR"];
            Column col_RemainingBalance = grdData.Columns.DataColumns["RemainingBalance"];
            Column col_TotalAmount = grdData.Columns.DataColumns["TotalAmount"];

            Column col_Jan = grdData.Columns.DataColumns["Jan"];
            Column col_Feb = grdData.Columns.DataColumns["Feb"];
            Column col_Mar = grdData.Columns.DataColumns["Mar"];
            Column col_Apr = grdData.Columns.DataColumns["Apr"];
            Column col_May = grdData.Columns.DataColumns["May"];
            Column col_Jun = grdData.Columns.DataColumns["Jun"];
            Column col_Jul = grdData.Columns.DataColumns["Jul"];
            Column col_Aug = grdData.Columns.DataColumns["Aug"];
            Column col_Sep = grdData.Columns.DataColumns["Sep"];
            Column col_Oct = grdData.Columns.DataColumns["Oct"];
            Column col_Nov = grdData.Columns.DataColumns["Nov"];
            Column col_Dec = grdData.Columns.DataColumns["Dec"];

            col_ExpenseItem.Width = new ColumnWidth(200, false);
            col_Allocation.Width = new ColumnWidth(100, false);

            col_TotalOBR.Width = new ColumnWidth(100, false);
            col_TotalExpense.Width = new ColumnWidth(100, false);
       //     col_BalanceContigency.Width = new ColumnWidth(100, false);
            col_Adjustment.Width = new ColumnWidth(100, false);
            col_RemainingBalance.Width = new ColumnWidth(100, false);

             col_Jan.Width = new ColumnWidth(100, false);
             col_Feb.Width = new ColumnWidth(100, false);
             col_Mar.Width = new ColumnWidth(100, false);
             col_Apr.Width = new ColumnWidth(100, false);
             col_May.Width = new ColumnWidth(100, false);
             col_Jun.Width = new ColumnWidth(100, false);
             col_Jul.Width = new ColumnWidth(100, false);
             col_Aug.Width = new ColumnWidth(100, false);
             col_Sep.Width = new ColumnWidth(100, false);
             col_Oct.Width = new ColumnWidth(100, false);
             col_Nov.Width = new ColumnWidth(100, false);
             col_Dec.Width = new ColumnWidth(100, false);

             col_TotalExpense.HeaderText = "Adjusted";
             col_RemainingBalance.HeaderText = "Balance";
             col_TotalOBR.HeaderText = "Total OBR";

            grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_ExpenseItem);
            grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_Allocation);
            grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_TotalOBR);
            grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_RemainingBalance);
            grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_Adjustment);
            grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_TotalExpense);

            grdData.Columns["UACS"].Visibility = System.Windows.Visibility.Collapsed;
            grdData.Columns["Status"].Visibility = System.Windows.Visibility.Collapsed;
          //  col_TotalAmount.Visibility = System.Windows.Visibility.Collapsed;
          //  grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_BalanceContigency);
       
        }
        private clsOBRData c_obr = new clsOBRData();
        private void FetchOBRData()
        {
           // lblTitle.Content = "Obligation Expense and Balances for Fund Source : " + _FundSource;
            c_obr.Process = "FetchOBRData";
            svc_mindaf.ExecuteImportDataSQLAsync(c_obr.FetchOBRData(this.SelectedYear, this.FundName));
        }
        private void FetchBudgetAllocation() 
        {
            c_ppmp.Process = "FetchBudgetAllocation";
            svc_mindaf.ExecuteSQLAsync(c_ppmp.FetchBudgetAllocationV2(this.FundSource, this.SelectedYear));
        }

        private void FetchBudgetAdjustments()
        {
            c_ppmp.Process = "FetchBudgetAdjustments";
            svc_mindaf.ExecuteSQLAsync(c_ppmp.FetchBudgetAdjustments(this.DivId, this.SelectedYear));
        }
        private void FetchBudgetRAFNo()
        {
            
            c_ppmp.Process = "FetchBudgetRAFNo";
            svc_mindaf.ExecuteSQLAsync(c_ppmp.FetchBudgetRafNo(this.DivId, this.SelectedYear,FundType));
        }
        private void FetchBudgetRevisionTitles()
        {
            c_ppmp.Process = "FetchBudgetRevisionTitles";
            svc_mindaf.ExecuteSQLAsync(c_ppmp.FetchRevisionTitles(this.FundSource, this.DivId, this.SelectedYear));
        }
        private void FetchBudgetRevision() 
        {
            c_ppmp.Process = "FetchBudgetRevision";
            svc_mindaf.ExecuteSQLAsync(c_ppmp.FetchRevisions(this.FundSource,this.DivId, this.SelectedYear));
        }
        private void FetchFundSource()
        {
            c_ppmp.Process = "FetchFundSource";
            svc_mindaf.ExecuteSQLAsync(c_ppmp.FetchFundSource(this.DivId,this.WorkingYear,this.FundSource));
        }

        private void FetchActivityData()
        {
            c_ppmp.Process = "FetchActivityData";
            svc_mindaf.ExecuteSQLAsync(c_ppmp.FetchActivityData(this.FundSource));
        }
        private void FetchRevisions()
        {
            c_ppmp.Process = "FetchRevisions";
            svc_mindaf.ExecuteSQLAsync(c_ppmp.FetchActivityData(this.FundSource));
        }

        private void FetchActivityDataMonths()
        {
            c_ppmp.Process = "FetchActivityDataMonths";
            svc_mindaf.ExecuteSQLAsync(c_ppmp.FetchActivityDataWithMonths(this.FundSource));
        }
        private void usr_b_revision_Loaded(object sender, RoutedEventArgs e)
        {
            FetchBudgetAllocation();
        }
        private clsreAlignment c_realign = new clsreAlignment();
        private String Title = "";
        private void SaveAlignment()
        {
            grdData.ItemsSource = null;
            btnSave.IsEnabled = false;
            btnClearEntry.IsEnabled = false;


            c_realign.Process = "UpdateAlignment";
            c_realign.UpdateAlignment(this.DivId,this.WorkingYear);
            c_realign.SQLOperation+=c_realign_SQLOperation;

           
            

            // c_realign.SQLOperation += c_realign_SQLOperation;
        }

        private void RequestModification(String Title)
        {

            c_realign.Process = "RequestRevision";
            c_realign.RequestMod( DivId);
            c_realign.SQLOperation+=c_realign_SQLOperation;

            // c_realign.SQLOperation += c_realign_SQLOperation;
        }
        private void SendNotify()
        {

           // c_realign.Process = "SendNotify";
            c_realign.SendNotifyRevision(this.DivId);
           

        }
        private void RemoveAlignments()
        {

            c_realign.Process = "RemoveAlignments";
            c_realign.RemoveAlignment(this.DivId,this.SelectedYear);
            c_realign.SQLOperation+=c_realign_SQLOperation;

        }
        private bool IsSaved = false;
        void c_realign_SQLOperation(object sender, EventArgs e)
        {
            switch (c_realign.Process)
            {
                case "UpdateAlignment":
                     foreach (var item in _BudgetListAdjustment)
                    {
                        String from_uacs = item.from_uacs;
                        String from_total = item.from_total;
                        String to_uacs = item.to_uacs;
                        String total_alignment = item.total_alignment;
                        String division_pap = item.division_pap;
                        String division_year = item.division_year;
                        String months = "";
                        String f_source = item.f_source;
                        c_realign.Process = "SaveAlignment";
                        c_realign.SaveAlignment(from_uacs, from_total, to_uacs, total_alignment, division_pap, division_year, months, f_source, Title);
                        c_realign.SQLOperation+=c_realign_SQLOperation;
                    }
                     String _title = Title;
                    if (ForRevision)
	                {
                        c_realign.RecordNewRafNo(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), _title, DivId, RafData.RafNo, RafData.EntryDate, ForRevision,FundType);
                        SendNotify();
	                }else
	                {
                        c_realign.RecordNewRafNo(DateTime.Now.Year.ToString(), DateTime.Now.Month.ToString(), _title, DivId, "", "", ForRevision,FundType);
                        SendNotify();
	                }
                    Title = "";
                    break;

                case "RemoveAlignments":
                    FetchBudgetAllocation();
                    break;
                case "RequestRevision":
                    MessageBox.Show("Request has been submitted");
                    break;
                case "SaveAlignment":
                    if (IsSaved ==false)
                    {
                        IsSaved = true;
                        MessageBox.Show("Data has been successfully saved.");
                        FetchBudgetAllocation();
                    }
                   
                    break;
                default:
                    break;
            }
        }

        private bool isNew = false;
        frmRevision frev;
        private void grdData_CellDoubleClicked(object sender, CellClickedEventArgs e)
        {
            //if (isNew)
            //{
            //    btnSave.Content = "Save Revision";
            //    btnSave.IsEnabled = true;
            //    isNew = true;
            //    goto x;
            //}
            if (RafStatus =="Served")
            {
                MessageBoxResult result = MessageBox.Show("Create New Revision?",
                 "Revision", MessageBoxButton.OKCancel);

                if (result == MessageBoxResult.OK)
                {
                    btnSave.Content = "Save Revision";
                    isNew = true;
                    goto x;
                }
                else
                {
                    MessageBox.Show("Data has been locked. Cannot modify Re-aligned Data." + Environment.NewLine + "For modification click Request Modification button.");
                }
                 
                return;
            }
            x:
            if (e.Cell.Column.HeaderText =="Adjustment")
            {
                String _UACS = grdData.Rows[e.Cell.Row.Index].Cells["UACS"].Value.ToString();
                String _ExpenseItem = grdData.Rows[e.Cell.Row.Index].Cells["ExpenseItem"].Value.ToString();
                String _Amount = grdData.Rows[e.Cell.Row.Index].Cells["RemainingBalance"].Value.ToString();
                String _Adjustment ="";
                if ( grdData.Rows[e.Cell.Row.Index].Cells["Adjustment"].Value!=null)
                {
                  _Adjustment = grdData.Rows[e.Cell.Row.Index].Cells["Adjustment"].Value.ToString();
                }
               

                frev = new frmRevision();
                frev.ExpenseItem = _ExpenseItem;
                frev.FROMUACS = _UACS;
                if (_Adjustment!=null)
                {
                    if (_Adjustment.Contains("("))
                    {
                        _Adjustment = _Adjustment.Replace("(", "");
                        _Adjustment = _Adjustment.Replace(")", "");
                        if (_Adjustment == "")
                        {
                            _Adjustment = "0";
                        }
                        frev.Amount = (Convert.ToDouble(_Amount) - Convert.ToDouble(_Adjustment)).ToString("#,##0.00");
                    }
                    else
                    {
                        _Adjustment = _Adjustment.Replace("(", "");
                        _Adjustment = _Adjustment.Replace(")", "");
                        if (_Adjustment == "")
                        {
                            _Adjustment = "0";
                        }
                        frev.Amount = (Convert.ToDouble(_Amount) + Convert.ToDouble(_Adjustment)).ToString("#,##0.00");
                    }
                   

                }
                else
                {
                    frev.Amount = _Amount;
                }
                
                frev.ReloadData += frev_ReloadData;
                frev.Show();

            }
        }
        private void ComputeItem() 
        {
            foreach (var item in _Main)
            {
                //if (item.Adjustment.Contains("-"))
                //{
                //    break;
                //}
                if (item.Adjustment==null)
                {
                     item.TotalExpense = Convert.ToDouble(item.RemainingBalance).ToString("#,##0.00");
                }
                else
                {
                   
                    if (item.Adjustment.Contains("("))
                    {
                        String item_adjust = item.Adjustment;
                        item_adjust = item_adjust.Replace("(", "");
                        item_adjust = item_adjust.Replace(")","");

                        item.TotalExpense = (Convert.ToDouble(item.RemainingBalance) - Convert.ToDouble(item_adjust)).ToString("#,##0.00");
                    }
                    else
                    {
                        if (item.Allocation==null)
                        {
                            item.TotalExpense = Convert.ToDouble(item.Adjustment).ToString("#,##0.00");
                        }else
	                    {
                            item.TotalExpense = (Convert.ToDouble(item.RemainingBalance) + Convert.ToDouble(item.Adjustment)).ToString("#,##0.00");
	                    }
                       
                    }
                }
               
            }
        }
        void frev_ReloadData(object sender, EventArgs e)
        {
            int _idx1 = _Main.Count-1;
            int _idx2 = _idx1 - 1;
            _Main.RemoveAt(_idx1);
            _Main.RemoveAt(_idx2);
            foreach (var item in _Main)
            {
                if (item.ExpenseItem == frev.ExpenseItem)
                {
                    if ( item.Adjustment==null)
                    {
                        item.Adjustment = "0.00";
                    }
                    String _adjusted = item.Adjustment;
                    _adjusted = _adjusted.Replace("(", "");
                    _adjusted = _adjusted.Replace(")", "");
                    if (_adjusted =="")
                    {
                        _adjusted = "0.00";
                    }

                    double _total = Convert.ToDouble(_adjusted) + Convert.ToDouble(frev.AdjustedAmount);

                    item.Adjustment = "(" + _total.ToString("#,##0.00") + ")";
                    break;   
                }
            }
            bool isFound = false;
            foreach (var item in _Main)
            {
                if (item.ExpenseItem == frev.ExpenseItemSelected)
                {
                    if (item.Adjustment == null)
                    {
                        item.Adjustment = "0.00";
                    }
                    String _adjusted = item.Adjustment;
                    _adjusted = _adjusted.Replace("(", "");
                    _adjusted = _adjusted.Replace(")", "");
                    if (_adjusted == "")
                    {
                        _adjusted = "0.00";
                    }

                    double _total = Convert.ToDouble(_adjusted) + Convert.ToDouble(frev.AdjustedAmount);

                    item.Adjustment = _total.ToString("#,##0.00");
                    isFound = true;
                    break;
                }
            }

            if (isFound==false)
            {
                BudgetRevisionMain _adjusted = new BudgetRevisionMain();

                _adjusted.ExpenseItem = frev.ExpenseItemSelected;
                _adjusted.Adjustment = Convert.ToDouble(frev.AdjustedAmount).ToString("#,##0.00");
                _Main.Add(_adjusted);
            }
            BudgetRevisedData _data = new BudgetRevisedData();
            _data.division_pap=DivId;
            _data.division_year ="2017";
            _data.f_source = FundSource;
            _data.from_total = frev.Amount;
            _data.from_uacs = frev.FROMUACS;
            _data.months = "";
            _data.to_uacs = frev.UACS;
            _data.total_alignment = frev.AdjustedAmount;

            _BudgetListAdjustment.Add(_data);

            ComputeItem();

            BudgetRevisionMain _totals_line = new BudgetRevisionMain();
            _totals_line.ExpenseItem = "---------------------------------------------------";
            _totals_line.Allocation = "-----------------";
            _totals_line.TotalExpense = "-----------------";
            _totals_line.Adjustment = "-----------------";
            _totals_line.TotalOBR = "-----------------";
            _totals_line.RemainingBalance = "-----------------";


            BudgetRevisionMain _totals = new BudgetRevisionMain();
            double _TotalAllocation = 0.00;
            double _TotalExpense = 0.00;
            double _TotalOBR = 0.00;
            double _TotalBalance = 0.00;

            foreach (var item in _Main)
            {
                _TotalAllocation += Convert.ToDouble(item.Allocation);
                _TotalExpense += Convert.ToDouble(item.TotalExpense);
                _TotalOBR += Convert.ToDouble(item.TotalOBR);
                _TotalBalance += Convert.ToDouble(item.RemainingBalance);
            }
            _totals.ExpenseItem = "Total";
            _totals.Allocation = _TotalAllocation.ToString("#,##0.00");
            _totals.TotalExpense = _TotalExpense.ToString("#,##0.00");
            _totals.TotalOBR = _TotalOBR.ToString("#,##0.00");
            _totals.RemainingBalance = _TotalBalance.ToString("#,##0.00");
            _Main.Add(_totals_line);
            _Main.Add(_totals);


            grdData.ItemsSource = null;
            grdData.ItemsSource = _Main;
            FixColumns();

            if (isNew)
            {
                btnSave.Content = "Save Revision";
                btnSave.IsEnabled = true;
                isNew = false;
            }
        }
        frmTitleRevision ftrev;
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            switch (btnSave.Content.ToString())
            {
                case "New Revision":
                    grdData.IsEnabled = true;
                    btnSave.Content = "Save Revision";
                    break;
                case "Save Revision":

                    if (_BudgetListAdjustment.Count == 0)
                    {
                        MessageBox.Show("No changes made to this data", "No Revision", MessageBoxButton.OK);
                        return;
                    }
                    else
                    {
                        if (ForRevision)
                        {
                           // SaveAlignment(RafData.AlignId);
                        }
                        else
                        {
                            MessageBoxResult result = MessageBox.Show("Confirm Save Revision Data",
                            "Save Entry", MessageBoxButton.OKCancel);

                            if (result == MessageBoxResult.OK)
                            {
                                ftrev = new frmTitleRevision();
                                ftrev.SaveData += ftrev_SaveData;
                                ftrev.Show();
                            }
                        }
                   
                    }
                    break;
                case "Request Modification":
                       MessageBoxResult results = MessageBox.Show("Confirm Request Revision ",
                     "Save Entry", MessageBoxButton.OKCancel);

                        if (results == MessageBoxResult.OK)
                        {
                            RequestModification(DivId);
                        }
                    break;
            }
            
            
            
        }

        void ftrev_SaveData(object sender, EventArgs e)
        {
            Title = ftrev._Name;
            SaveAlignment();
        }

        private void btnClearRevision_Click(object sender, RoutedEventArgs e)
        {
            RemoveAlignments();
        }

        private void btnClearEntry_Click(object sender, RoutedEventArgs e)
        {
            FetchBudgetAllocation();
        }

       
    }
}
