﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol.Budget_Allocation
{
    public partial class ctrlPAPNewSetup : UserControl
    {
        public event EventHandler RefreshData;
        public String TypeDepartment { get; set; }
        public String DBM_Sub_Id { get; set; }
        public String PAP { get; set; }
        public String WorkingYear { get; set; }
        public String DivisionId { get; set; }
        public List<String> Excempted { get; set; }
        private clsOfficeDashBoard c_office_dash = new clsOfficeDashBoard();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private List<DivisionAssign> _Data = new List<DivisionAssign>();

        private List<DivisionData> _DivList = new List<DivisionData>();


        public ctrlPAPNewSetup()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            this.Loaded += ctrlPAPNewSetup_Loaded;
        }

        void ctrlPAPNewSetup_Loaded(object sender, RoutedEventArgs e)
        {
            switch (this.TypeDepartment)
            {
                case "Assign":
                    FetchDivisionData();
                    break;
                case "Unit":
                    FetchDivisionUnits();
                    break;
            }
        }

        private void FetchDivisionData()
        {
            c_office_dash.Process = "FetchDivisionData";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchDivisionAsign(this.WorkingYear));
        }

        private void FetchDivisionUnits()
        {
            c_office_dash.Process = "FetchDivisionUnits";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchDivisionUnits(this.WorkingYear));
        }
        private List<OfficePaps> _DataUnits = new List<OfficePaps>();
        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            string _result = e.Result.ToString();

            switch (c_office_dash.Process)
            {
                case "FetchDivisionData":
                    XDocument oDocFetchBalances = XDocument.Parse(_result);
                    var _dataFetchBalances = from info in oDocFetchBalances.Descendants("Table")
                                             select new DivisionAssign
                                             {
                                                 Division_Id = Convert.ToString(info.Element("Division_Id").Value),
                                                 Division_Desc = Convert.ToString(info.Element("Division_Desc").Value),
                                                 DivisionAccro = Convert.ToString(info.Element("DivisionAccro").Value)

                                             };



                    _Data.Clear();

                    foreach (var item in _dataFetchBalances)
                    {
                        
                            DivisionAssign _varData = new DivisionAssign();

                            _varData.Division_Id = item.Division_Id;
                            _varData.Division_Desc = item.Division_Desc;
                            _varData.DivisionAccro = item.DivisionAccro;

                            _Data.Add(_varData);
                   

                    }
                    grdPaps.ItemsSource = null;
                    grdPaps.ItemsSource = _Data;
                   


                    grdPaps.Columns["Division_Id"].Visibility = System.Windows.Visibility.Collapsed;
                  //  grdPaps.Columns["Division_Desc"].Visibility = System.Windows.Visibility.Collapsed;
                    grdPaps.Columns["DivisionAccro"].Visibility = System.Windows.Visibility.Collapsed;

                   
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchDivisionUnits":
                    XDocument oDocFetchDivisionUnits = XDocument.Parse(_result);
                    var _dataFetchDivisionUnits = from info in oDocFetchDivisionUnits.Descendants("Table")
                                             select new DivisionAssign
                                             {
                                                 Division_Id = Convert.ToString(info.Element("Division_Id").Value),
                                                 Division_Desc = Convert.ToString(info.Element("Division_Desc").Value),
                                                 DivisionAccro = Convert.ToString(info.Element("DivisionAccro").Value)

                                             };



                    _Data.Clear();

                    foreach (var item in _dataFetchDivisionUnits)
                    {
                        DivisionAssign _varData = new DivisionAssign();

                        _varData.Division_Id = item.Division_Id;
                        _varData.Division_Desc = item.Division_Desc;
                        _varData.DivisionAccro = item.DivisionAccro;

                        _Data.Add(_varData);

                    }
                    grdPaps.ItemsSource = null;
                    grdPaps.ItemsSource = _Data;
                   


                    grdPaps.Columns["Division_Id"].Visibility = System.Windows.Visibility.Collapsed;
                  //  grdPaps.Columns["Division_Desc"].Visibility = System.Windows.Visibility.Collapsed;
                    grdPaps.Columns["DivisionAccro"].Visibility = System.Windows.Visibility.Collapsed;

                   
                    this.Cursor = Cursors.Arrow;
                    break;
               
            }
        }
        private void AddNewData() 
        {
            switch (this.TypeDepartment)
            {
                case "Assign":
                    
                    DivisionAssign _selected = (DivisionAssign)grdPaps.ActiveItem;
                    DivisionData _new = new DivisionData();

                    _new.DBM_SUP_PAP_ID = this.DBM_Sub_Id;
                    _new.Division_Code = this.PAP;
                    _new.Division_Desc = _selected.Division_Desc;
                    _new.Division_Accro = _selected.DivisionAccro;
                    _new.Is_Sub_Division = "0";
                    _new.Year_Setup = this.WorkingYear;
                    _new.Division_Id = _selected.Division_Id;

                    c_office_dash.Process = "AssignNew";
                    c_office_dash.SQLOperation+=c_office_dash_SQLOperation;
                    c_office_dash.AddDivisionCode("Assign", _new);
           
                    break;
                case "Unit":

                    DivisionAssign _selected_d = (DivisionAssign)grdPaps.ActiveItem;
                    DivisionData _new_d = new DivisionData();

                    _new_d.DBM_SUP_PAP_ID = this.DBM_Sub_Id;
                    _new_d.Division_Code = this.PAP;
                    _new_d.Division_Desc = _selected_d.Division_Desc;
                    _new_d.Division_Accro = _selected_d.DivisionAccro;
                    _new_d.Is_Sub_Division = "1";
                    _new_d.Year_Setup = this.WorkingYear;
                    _new_d.Unit_Code = DivisionId;
                    _new_d.Division_Id = _selected_d.Division_Id;

                    c_office_dash.Process = "AssignNew";
                    c_office_dash.SQLOperation += c_office_dash_SQLOperation;
                    c_office_dash.AddDivisionCode("Assign", _new_d);

                    break;
            }

      
            
        }

    
        void c_office_dash_SQLOperation(object sender, EventArgs e)
        {
            switch (c_office_dash.Process)
            {
                case "AssignNew":
                    if (this.RefreshData != null)
                    {
                        this.RefreshData(this, new EventArgs());
                    }
                    break;
                default:
                    break;
            }
        }


        //private void btnTransfer_Click(object sender, RoutedEventArgs e)
        //{
        //    var x_result = MessageBox.Show("Confirm transfer.", "Transfer Confirmation", MessageBoxButton.OKCancel);
        //    if (x_result.ToString() == "OK")
        //    {
        //        switch (this.TypeDepartment)
        //        {
        //            case "Office":
        //                UpdateOffice();
        //                break;
        //            case "Unit":
        //                UpdateDivision();
        //                break;
        //            default:
        //                break;
        //        }
        //    }
        //}

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            AddNewData();
        }
    }
}
