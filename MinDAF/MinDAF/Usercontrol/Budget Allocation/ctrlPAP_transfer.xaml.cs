﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol.Budget_Allocation
{
    public partial class ctrlPAP_transfer : UserControl
    {
        public event EventHandler RefreshData;
        public String TypeDepartment { get; set; }
        public String DivisionId { get; set; }
        public String WorkingYear { get; set; }
        private clsOfficeDashBoard c_office_dash = new clsOfficeDashBoard();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private List<OfficePaps> _Data = new List<OfficePaps>();

        public ctrlPAP_transfer()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            this.Loaded += ctrlPAP_transfer_Loaded;
        }

        void ctrlPAP_transfer_Loaded(object sender, RoutedEventArgs e)
        {
            switch (this.TypeDepartment)
            {
                case "Office":
                    FetchOffice();
                    break;
                case "Unit":
                    FetchDivision();
                    break;
            }
        }

        private void FetchOffice() 
        {
            c_office_dash.Process = "FetchOfficePap";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchOfficePAPS());
        }
        private void FetchDivision()
        {
            c_office_dash.Process = "FetchDivisionPap";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchDivisionPAPS());
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            string _result = e.Result.ToString();

            switch (c_office_dash.Process)
            {
                case "FetchOfficePap":
                    XDocument oDocFetchBalances = XDocument.Parse(_result);
                    var _dataFetchBalances = from info in oDocFetchBalances.Descendants("Table")
                                             select new OfficePaps
                                             {
                                                 Id = Convert.ToString(info.Element("Id").Value),
                                                 Code = Convert.ToString(info.Element("Code").Value),
                                                 Office = Convert.ToString(info.Element("Office").Value)

                                             };



                    _Data.Clear();

                    foreach (var item in _dataFetchBalances)
                    {
                        OfficePaps _varData = new OfficePaps();

                        _varData.Id = item.Id;
                        _varData.Code = item.Code;
                        _varData.Office = item.Office;
                      
                        _Data.Add(_varData);

                    }
                    grdPaps.ItemsSource = null;
                    grdPaps.ItemsSource = _Data;

                    grdPaps.Columns["Code"].Visibility = System.Windows.Visibility.Collapsed;
                    grdPaps.Columns["Id"].Visibility = System.Windows.Visibility.Collapsed;

                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchDivisionPap":
                    XDocument oDocFetchDivisionPap = XDocument.Parse(_result);
                    var _dataFetchDivisionPap = from info in oDocFetchDivisionPap.Descendants("Table")
                                             select new OfficePaps
                                             {
                                                 Id = Convert.ToString(info.Element("Id").Value),
                                                 Code = Convert.ToString(info.Element("Code").Value),
                                                 Office = Convert.ToString(info.Element("Office").Value)

                                             };



                    _Data.Clear();

                    foreach (var item in _dataFetchDivisionPap)
                    {
                        OfficePaps _varData = new OfficePaps();
                        _varData.Id = item.Id;
                        _varData.Code = item.Code;
                        _varData.Office = item.Office;

                        _Data.Add(_varData);

                    }
                    grdPaps.ItemsSource = null;
                    grdPaps.ItemsSource = _Data;

                    grdPaps.Columns["Code"].Visibility = System.Windows.Visibility.Collapsed;
                    grdPaps.Columns["Id"].Visibility = System.Windows.Visibility.Collapsed;

                    this.Cursor = Cursors.Arrow;
                    break;
            }
        }

        private void UpdateOffice() 
        {
            OfficePaps _selected = (OfficePaps)grdPaps.ActiveItem;
            c_office_dash.Process = "Update";
            c_office_dash.SQLOperation += c_office_dash_SQLOperation;
            c_office_dash.UpdateDivisionCode("Office", this.DivisionId, _selected.Code);
        }
        private void UpdateDivision()
        {
            OfficePaps _selected = (OfficePaps)grdPaps.ActiveItem;
            c_office_dash.Process = "Update";
            c_office_dash.SQLOperation += c_office_dash_SQLOperation;
            c_office_dash.UpdateDivisionCode("Unit", this.DivisionId, _selected.Code);
        }
        void c_office_dash_SQLOperation(object sender, EventArgs e)
        {
            switch (c_office_dash.Process)
            {
                case "Update":
                    if (this.RefreshData!=null)
                    {
                        this.RefreshData(this, new EventArgs());
                    }
                    break;
                default:
                    break;
            }
        }


        private void btnTransfer_Click(object sender, RoutedEventArgs e)
        {
            var x_result = MessageBox.Show("Confirm transfer.", "Transfer Confirmation", MessageBoxButton.OKCancel);
            if (x_result.ToString()=="OK")
            {
                switch (this.TypeDepartment)
                {
                    case "Office":
                        UpdateOffice();
                        break;
                    case "Unit":
                        UpdateDivision();
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
