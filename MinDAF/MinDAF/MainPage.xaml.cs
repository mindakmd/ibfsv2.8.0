﻿using MinDAF.Class;
using MinDAF.Forms;
using MinDAF.MinDAFS;
using MinDAF.Usercontrol;
using MinDAF.Usercontrol.Budget_Allocation;
using MinDAF.Usercontrol.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF
{
    public partial class MainPage : UserControl
    {
        MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private Boolean isVerified = false;
        private usr_Menu ctrlMenu = new usr_Menu();
        private usr_Menu_Admin ctrlAdmin = new usr_Menu_Admin();
        private clsMain c_main = new clsMain();

        private usr_bottom_menu ctrlBottom = new usr_bottom_menu();

        private usr_office_management ctrlOffice = new usr_office_management();
        private usrChiefData ctrlCData = new usrChiefData();
        private usr_Menu_Approvals ctrlApprovals = new usr_Menu_Approvals("");
        private List<DivisionFields> ListDivisions = new List<DivisionFields>();
        private usr_report_dashboard usr_dashboard = new usr_report_dashboard();
        private String FundId = "";
        private String Username = "";
        private String UnitCode = "";
        private String UserID = "";
        private String Division = "";
        private String DivisionId = "";
        private String IsAdmin = "";
        private String OfficeId = "";
        private String PAP= "";
        public MainPage()
        {
            InitializeComponent();
            App.Current.Host.Content.Resized += Content_Resized;
            svc_mindaf.VerifyUserCompleted += new EventHandler<VerifyUserCompletedEventArgs>(svc_mindaf_VerifyUserCompleted);
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            ctrlBottom.MenuClick += ctrlBottom_MenuClick;
            ctrlBottom.SignOut += ctrlBottom_SignOut;
            ctrlApprovals = new usr_Menu_Approvals(this.FundId);
           
            ctrlApprovals.Level = this.IsAdmin;
         //   string version = Assembly.GetExecutingAssembly().

           // lblVersion.Content = String.Format("IBFS - v {0}", version);
        }

        void ctrlBottom_SignOut(object sender, EventArgs e)
        {
            stkBottomMenu.Children.Clear();
            stkChild.Children.Clear();
            grdLogin.Visibility = System.Windows.Visibility.Visible;
            grdInfo.Children.Clear();
            txtUsername.Text = "";
            txtPassword.Password = "";
            txtUsername.Focus();
        }


        private void LoadDivisions()
        {

            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            svc_mindaf.ExecuteSQLAsync(c_main.FetchDivisionData());
        }
        frmFinanceNotify f_not = new frmFinanceNotify();
        private bool isLoaded = false;
        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            string _result = e.Result.ToString();
            switch (c_main.Process)
            {
                case "VerifyUser":
                    //if (isLoaded)
                    //{
                    //    break;
                    //}
                     XDocument oDoc = XDocument.Parse(_result);
                    var _data = from info in oDoc.Descendants("Table")
                                select new UserVerify
                                {

                                    Division_Id = Convert.ToString(info.Element("Division_Id").Value),
                                    Division_Desc = Convert.ToString(info.Element("Division_Desc").Value),
                                    User_Fullname = Convert.ToString(info.Element("User_Fullname").Value),
                                    IsAdmin = Convert.ToString(info.Element("IsAdmin").Value),
                                    UserId = Convert.ToString(info.Element("UserId").Value),
                                    OfficeId = Convert.ToString(info.Element("OfficeId").Value),
                                    PAP = Convert.ToString(info.Element("PAP").Value),
                                    UnitCode = Convert.ToString(info.Element("UnitCode").Value),
                                    AttachedFundId = Convert.ToString(info.Element("FundId").Value)
                                };



                    List<UserVerify> _user = new List<UserVerify>();
                    foreach (var item in _data)
                    {
                        UserVerify _dataUser = new UserVerify();

                        this.Username = item.User_Fullname;
                        this.Division = item.Division_Desc;
                        this.DivisionId = item.Division_Id;
                        this.UserID = item.UserId;
                        this.IsAdmin = item.IsAdmin;
                        this.OfficeId = item.OfficeId;
                        this.PAP = item.PAP;
                        this.UnitCode = item.UnitCode;
                        this.FundId  = item.AttachedFundId;

                        _dataUser.Division_Id = item.Division_Id;
                        _dataUser.Division_Desc = item.Division_Desc;
                        _dataUser.User_Fullname = item.User_Fullname;
                        _dataUser.IsAdmin = item.IsAdmin;
                        _dataUser.UserId = item.UserId;
                        _dataUser.OfficeId = item.OfficeId;
                        _dataUser.UnitCode = item.UnitCode;
                        _dataUser.AttachedFundId = item.AttachedFundId;
                        _user.Add(_dataUser);

                        break;
                    }

                    if (_user.Count != 0)
                    {
                        if (this.UnitCode!="-")
                        {
                            ctrlBottom.Height = stkBottomMenu.ActualHeight;
                            ctrlBottom.Width = stkBottomMenu.ActualWidth;
                            ctrlBottom.btnBudgetApproval.Visibility = System.Windows.Visibility.Visible;
                            ctrlBottom.btnAdmin.Visibility = System.Windows.Visibility.Collapsed;
                            ctrlBottom.btnBudgetDivison.Visibility = System.Windows.Visibility.Visible;


                            ctrlCData.Height = grdInfo.ActualHeight;
                            ctrlCData.Width = grdInfo.ActualWidth;
                            ctrlCData.OfficeId = this.OfficeId;
                            ctrlCData.DivisionId = this.DivisionId;
                            ctrlCData.PAPCODE = this.PAP;
                            ctrlCData._User = this.Username;
                            ctrlCData.AttachedFundId = this.FundId;
                            grdInfo.Children.Clear();
                            grdInfo.Children.Add(ctrlCData);
                            //grdLogin.Visibility = System.Windows.Visibility.Collapsed;

                            //ctrlBottom.Height = stkBottomMenu.ActualHeight;
                            //ctrlBottom.Width = stkBottomMenu.ActualWidth;

                            //ctrlBottom.btnBudgetApproval.Visibility = System.Windows.Visibility.Visible;
                            //ctrlBottom.btnAdmin.Visibility = System.Windows.Visibility.Collapsed;
                            //ctrlBottom.btnBudgetDivison.Visibility = System.Windows.Visibility.Visible;
                            //ctrlBottom.IsUnit = true;
                            isLoaded = true;
                        }
                        else
                        {
                            Reload();
                            isLoaded = true;
                        }
                       


                        stkBottomMenu.Children.Clear();
                        stkBottomMenu.Children.Add(ctrlBottom);

                    }
                    else
                    {
                        MessageBox.Show("Login Failed");
                    }



                    this.Cursor = Cursors.Arrow;
                    break;
            }

        }
        private void Reload() 
        {
            grdLogin.Visibility = System.Windows.Visibility.Collapsed;

            ctrlBottom.Height = stkBottomMenu.ActualHeight;
            ctrlBottom.Width = stkBottomMenu.ActualWidth;

            if (IsAdmin == "0")
            {
                ctrlBottom.btnBudgetApproval.Visibility = System.Windows.Visibility.Collapsed;
                ctrlBottom.btnAdmin.Visibility = System.Windows.Visibility.Collapsed;
                ctrlBottom.btnBudgetDivison.Visibility = System.Windows.Visibility.Visible;

                ctrlCData.Height = grdInfo.ActualHeight;
                ctrlCData.Width = grdInfo.ActualWidth;
                ctrlCData.OfficeId = this.OfficeId;
                ctrlCData.DivisionId = this.DivisionId;
                ctrlCData.PAPCODE = this.PAP;
                ctrlCData._User = this.Username;
                ctrlCData.AttachedFundId = this.FundId;
                grdInfo.Children.Clear();
                grdInfo.Children.Add(ctrlCData);
            }
            else if (IsAdmin == "2")
            {
                ctrlBottom.btnBudgetApproval.Visibility = System.Windows.Visibility.Visible;
                ctrlBottom.btnAdmin.Visibility = System.Windows.Visibility.Collapsed;
                ctrlBottom.btnBudgetDivison.Visibility = System.Windows.Visibility.Visible;


                ctrlCData.Height = grdInfo.ActualHeight;
                ctrlCData.Width = grdInfo.ActualWidth;
                ctrlCData.OfficeId = this.OfficeId;
                ctrlCData.DivisionId = this.DivisionId;
                ctrlCData.PAPCODE = this.PAP;
                ctrlCData._User = this.Username;
                ctrlCData.AttachedFundId = this.FundId;
                grdInfo.Children.Clear();
                grdInfo.Children.Add(ctrlCData);

            }
            else if (IsAdmin == "1")
            {
                if (this.DivisionId == "3")
                {
                    f_not = new frmFinanceNotify();
                    f_not.FetchCountNew();
                    f_not.CheckData += f_not_CheckData;

                }
                //ctrlBottom.btnBudgetApproval.Visibility = System.Windows.Visibility.Visible;
                //ctrlBottom.btnBudgetDivison.Visibility = System.Windows.Visibility.Visible;
                //ctrlBottom.btnAdmin.Visibility = System.Windows.Visibility.Visible;

                //usr_dashboard.Width = grdChild.ActualWidth;
                //usr_dashboard.Height = grdChild.ActualHeight;
                //usr_dashboard.OfficeID = this.OfficeId;
                //usr_dashboard.DivisionID = this.DivisionId;
                //usr_dashboard.User = this.Username;
                //ctrlCData.AttachedFundId = this.FundId;

                //stkChild.Children.Clear();
                //stkChild.Children.Add(usr_dashboard);
                if (this.DivisionId=="3")
                {
                    ctrlBottom.btnBudgetApproval.IsEnabled = true;
                    ctrlBottom.btnAdmin.IsEnabled = false;
                    ctrlBottom.btnBudgetDivison.IsEnabled = true;
                }
                else if(this.DivisionId=="4")
                {
                    ctrlBottom.btnBudgetApproval.IsEnabled = false;
                    ctrlBottom.btnAdmin.IsEnabled = true;
                    ctrlBottom.btnBudgetDivison.IsEnabled = true;
                }
               


                ctrlCData.Height = grdInfo.ActualHeight;
                ctrlCData.Width = grdInfo.ActualWidth;
                ctrlCData.OfficeId = this.OfficeId;
                ctrlCData.DivisionId = this.DivisionId;
                ctrlCData.PAPCODE = this.PAP;
                ctrlCData._User = this.Username;
                ctrlCData.AttachedFundId = this.FundId;
                grdInfo.Children.Clear();
                grdInfo.Children.Add(ctrlCData);
            }
        }
        private Boolean isShown = false;
        void f_not_CheckData(object sender, EventArgs e)
        {
            if (f_not.has_new)
            {
                if (isShown)
                {
                    isShown = false;
                    return;
                }
                f_not.Show();
                isShown = true;
            }
        }


      private  frmYearSelection frm_year = new frmYearSelection();
        void ctrlBottom_MenuClick(object sender, EventArgs e)
        {
            switch (ctrlBottom.MenuType)
            {
                case "Admin":
                    ctrlBottom.btnBack.IsEnabled = true;
                    grdInfo.Children.Clear();
                    ctrlAdmin = new usr_Menu_Admin();
                    ctrlAdmin.Width = grdChild.ActualWidth;
                    ctrlAdmin.Height = grdChild.ActualHeight;
                    ctrlAdmin.Division = this.Division;
                    ctrlAdmin.IsAdmin = this.IsAdmin;
                    ctrlAdmin.UserId = this.UserID;
                    ctrlAdmin.DivisionID = this.DivisionId;
                    ctrlAdmin.User = this.Username;

                    grdLogin.Visibility = System.Windows.Visibility.Collapsed;
                    stkChild.Children.Clear();
                    stkChild.Children.Add(ctrlAdmin);
                    break;
                case "BudgetDivision":
                     frm_year = new frmYearSelection();
                     frm_year.SelectedData += frm_year_SelectedData;
                     frm_year.Show();
                     break;
                case "BudgetApproval":
                    ctrlBottom.btnBack.IsEnabled = true;
                    grdInfo.Children.Clear();
                    ctrlApprovals = new usr_Menu_Approvals(this.FundId);
                    ctrlApprovals.Width = grdChild.ActualWidth;
                    ctrlApprovals.Height = grdChild.ActualHeight;
                    ctrlApprovals.User = "Welcome " + this.Username;
                    ctrlApprovals.Level = IsAdmin;
                    ctrlApprovals.Division = Division;
                    ctrlApprovals.DivisionID = this.DivisionId;
                    ctrlApprovals.PaP = this.PAP;

                    grdLogin.Visibility = System.Windows.Visibility.Collapsed;
                    stkChild.Children.Clear();
                    stkChild.Children.Add(ctrlApprovals);
                    break;
                case "Back":
                    ctrlBottom.btnBack.IsEnabled = false;
                    grdInfo.Children.Clear();
                    stkChild.Children.Clear();
                    Reload();
                    break;
            }
        }

        void frm_year_SelectedData(object sender, EventArgs e)
        {
            ctrlBottom.btnBack.IsEnabled = true;
            grdInfo.Children.Clear();
            ctrlMenu = new usr_Menu();
            ctrlMenu.WorkingYear = frm_year.SelectedYear;
            ctrlMenu.Width = grdChild.ActualWidth;
            ctrlMenu.Height = grdChild.ActualHeight;
            ctrlMenu.Division = this.Division;
            ctrlMenu.IsAdmin = this.IsAdmin;
            ctrlMenu.UserId = this.UserID;
            ctrlMenu.DivisionID = this.DivisionId;
            ctrlMenu.OfficeID = this.OfficeId;
            ctrlMenu.User = "Welcome " + this.Username;
            ctrlMenu.PAP = this.PAP;
            ctrlMenu.IsUnit = ctrlBottom.IsUnit;
            ctrlMenu.FundSource = this.FundId;
            grdLogin.Visibility = System.Windows.Visibility.Collapsed;
            stkChild.Children.Clear();
            stkChild.Children.Add(ctrlMenu);
        }



        void svc_mindaf_VerifyUserCompleted(object sender, VerifyUserCompletedEventArgs e)
        {
            var _result = e.Result;
            isVerified = _result;

            if (isVerified)
            {

                grdLogin.Visibility = System.Windows.Visibility.Collapsed;

                ctrlBottom.Height = stkBottomMenu.ActualHeight;
                ctrlBottom.Width = stkBottomMenu.ActualWidth;

                stkBottomMenu.Children.Add(ctrlBottom);

            }
            else
            {
                MessageBox.Show("Login Failed");
            }
        }

        void Content_Resized(object sender, EventArgs e)
        {
            this.Width = App.Current.Host.Content.ActualWidth;

            this.Height = App.Current.Host.Content.ActualHeight;

            ctrlBottom.Height = stkBottomMenu.ActualHeight;
            ctrlBottom.Width = stkBottomMenu.ActualWidth;


        }

        private void lblUser_MouseLeave(object sender, MouseEventArgs e)
        {

        }

        private void lblUser_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void VerifyUserAccount()
        {
            c_main.Process = "VerifyUser";
            svc_mindaf.ExecuteSQLAsync(c_main.LoginUserAccount(txtUsername.Text, txtPassword.Password));
        }
        private void btnSignIn_Click(object sender, RoutedEventArgs e)
        {
            // svc_mindaf.VerifyUserAsync(txtUsername.Text, txtPassword.Password);
            VerifyUserAccount();

        }

        private void Grid_Loaded_1(object sender, RoutedEventArgs e)
        {
            LoadDivisions();
            //frmReportDownloadTool f_repo = new frmReportDownloadTool();
            //f_repo.Show();
        }

        private void btnHr_Click(object sender, RoutedEventArgs e)
        {
            HtmlPage.Window.Navigate(new Uri("http://192.167.1.57:8080/mindata"), "_blank");
        }

        private void btnAccounting_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnAccounting_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

        private void btnHr_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnHr_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

        private void btnAccounting_Click(object sender, RoutedEventArgs e)
        {
            HtmlPage.Window.Navigate(new Uri("http://eibanez/"), "_blank");
        }

        private void btnSettings_Click(object sender, RoutedEventArgs e)
        {
            frmSettingEibanez fset = new frmSettingEibanez();

            fset.Show();
        }

        private void btnSettings_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnSettings_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

        private void btnHelp_Click(object sender, RoutedEventArgs e)
        {
            frmVideoHelpFile f_vid = new frmVideoHelpFile();

            f_vid.Show();

        }

        private void btnPPMP_Click(object sender, RoutedEventArgs e)
        {
            HtmlPage.Window.Navigate(new Uri("http://mindafinance/help/ppmp/ppmp.html"), "_blank");
        }

        private void btnBudgetHelp_Click(object sender, RoutedEventArgs e)
        {
            HtmlPage.Window.Navigate(new Uri("http://mindafinance/help/budget/budget.html"), "_blank");
        }

        private void btnPPMPs_Click(object sender, RoutedEventArgs e)
        {
            HtmlPage.Window.Navigate(new Uri("http://mindafinance/help/ppmp-revisions/ppmp-revisions.html"), "_blank");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //frmReportBalances f_report = new frmReportBalances();
          //  f_report.Show();
        }
    }
}
