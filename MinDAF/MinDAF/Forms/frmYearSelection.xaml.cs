﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Forms
{
    public partial class frmYearSelection : ChildWindow
    {
        public event EventHandler SelectedData;
        public String SelectedYear { get; set; }
        public frmYearSelection()
        {
            InitializeComponent();
            this.Loaded += frmYearSelection_Loaded;
        }

        void frmYearSelection_Loaded(object sender, RoutedEventArgs e)
        {
            CreateYear();
        }

        private void CreateYear() 
        {
            Int32 _year = DateTime.Now.Year;
            cmbYear.Items.Clear();
            cmbYear.Items.Add(_year);
            for (int i = 0; i < 20; i++)
            {
                _year += 1;
                cmbYear.Items.Add(_year);
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            
            SelectedYear = cmbYear.SelectedItem.ToString();
            if (SelectedData!=null)
            {
                this.SelectedData(this, new EventArgs());
            }
            this.DialogResult = true;
        }

      
    }
}

