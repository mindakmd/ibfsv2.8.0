﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmBudgetUnitAllocate : ChildWindow
    {
        public event EventHandler ReloadData;

        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsUserAllocate c_unit = new clsUserAllocate();
        private List<UnitLists> _division_units = new List<UnitLists>();
        private List<RespoData> ListRespo = new List<RespoData>();
        private List<lib_DivisionBudgetExpeniture> ListExpenditure = new List<lib_DivisionBudgetExpeniture>();
        public String DivisionId { get; set; }
        public frmBudgetUnitAllocate()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_unit.Process)
            {
                case "FetchRespo":
                    XDocument oDocKeyResultsFetchRespo = XDocument.Parse(_results);
                    var _dataListsFetchRespo = from info in oDocKeyResultsFetchRespo.Descendants("Table")
                                               select new RespoData
                                               {
                                                   Id = Convert.ToString(info.Element("id").Value),
                                                   Name = Convert.ToString(info.Element("respo_name").Value)
                                               };

                    ListRespo.Clear();
                    List<ProfData> _ComboListRespo = new List<ProfData>();

                    foreach (var item in _dataListsFetchRespo)
                    {
                        RespoData _varDetails = new RespoData();
                        ProfData _varProf = new ProfData();

                        _varProf._Name = item.Name;

                        _varDetails.Id = item.Id;
                        _varDetails.Name = item.Name;

                        _ComboListRespo.Add(_varProf);
                        ListRespo.Add(_varDetails);

                    }

                    cmbRespo.ItemsSource = null;
                    cmbRespo.ItemsSource = _ComboListRespo;
                    GenerateYear();
                    break;

                case "LoadDivisionUnit":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new UnitLists
                                     {
                                         DivisionId = Convert.ToString(info.Element("Division_Id").Value),
                                         Name = Convert.ToString(info.Element("Division_Desc").Value),
                                         UnitCode = Convert.ToString(info.Element("Division_Code").Value)
                                     };

                    _division_units.Clear();
                   List<ProfData> _ComboList = new List<ProfData>();
                    
                    foreach (var item in _dataLists)
                    {
                        ProfData _varProf = new ProfData();
                        UnitLists _varDetails = new UnitLists();

                        _varProf._Name = item.Name;
                        _varDetails.DivisionId = item.DivisionId;
                        _varDetails.Name = item.Name;
                        _varDetails.UnitCode = item.UnitCode;


                        _ComboList.Add(_varProf);
                        _division_units.Add(_varDetails);

                    }
                    cmbDivision.ItemsSource = null;
                    cmbDivision.ItemsSource = _ComboList;

                    FetchRespoCenter();
                    this.Cursor = Cursors.Arrow;

                    break;
                case "FetchExpenditureItems":
                    XDocument oDocKeyResultsFetchExpenditureItems = XDocument.Parse(_results);

                    var _dataListsFetchExpenditureItems = from info in oDocKeyResultsFetchExpenditureItems.Descendants("Table")
                                                          select new lib_DivisionBudgetExpeniture
                                                          {
                                                              name = Convert.ToString(info.Element("name").Value),
                                                              uacs_code = Convert.ToString(info.Element("uacs_code").Value)

                                                          };


                    ListExpenditure.Clear();
                    List<ProfData> _ComboListExpenditure = new List<ProfData>();

                    foreach (var item in _dataListsFetchExpenditureItems)
                    {
                        ProfData _varProf = new ProfData();
                        lib_DivisionBudgetExpeniture _temp = new lib_DivisionBudgetExpeniture();

                        _varProf._Name = item.name;

                        _temp.name = item.name;
                        _temp.uacs_code = item.uacs_code;

                        //  cmbExpenditures.Items.Add(item.name);
                        _ComboListExpenditure.Add(_varProf);
                        ListExpenditure.Add(_temp);

                    }
                    //cmbExpenditures.ItemsSource = null;
                    //cmbExpenditures.ItemsSource = _ComboListExpenditure;
                    cmbExpenditures.ItemsSource = _ComboListExpenditure;
                    //   cmbYear.SelectedIndex = 0;
                    //   cmbDivision.SelectedIndex = 0;
                    cmbExpenditures.IsEnabled = true;
                    cmbExpenditures.Focus();
                    //FetchOfficeData();
                  //  FetchBudgetExpenditureAllocation();
                    this.Cursor = Cursors.Arrow;
                    break;
            }
        }

        private void LoadExpenditure()
        {
            cmbExpenditureType.Items.Clear();

            cmbExpenditureType.Items.Add("PS");
            cmbExpenditureType.Items.Add("MOOE");
            cmbExpenditureType.Items.Add("CO");

        }
        private void GenerateYear()
        {
            int _year = DateTime.Now.Year;
            int _limit = _year + 100;

            for (int i = _year; i != _limit; i++)
            {
                cmbYear.Items.Add(_year);
                _year += 1;
            }
            LoadExpenditure();

        }
        private void FetchExpenditures()
        {
            c_unit.Process = "FetchExpenditures";
            svc_mindaf.ExecuteSQLAsync(c_unit.FetchExpenditureItems());
        }
        private void LoadDivisionUnit() 
        {
            c_unit.Process = "LoadDivisionUnit";
            svc_mindaf.ExecuteSQLAsync(c_unit.FetchDivisionUnits(this.DivisionId));
        }
        private void FetchRespoCenter()
        {
            c_unit.Process = "FetchRespo";
            svc_mindaf.ExecuteSQLAsync(c_unit.FetchRespoLibrary());
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void frmuserall_Loaded(object sender, RoutedEventArgs e)
        {
            LoadDivisionUnit();
        }

        private void btnAdd_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnAddRespo_Click(object sender, RoutedEventArgs e)
        {
            frmLibraryFundSource f_form = new frmLibraryFundSource();
            f_form.IsDivision = true;
            f_form.Reload += f_form_Reload;
            f_form.Show();
        }

        void f_form_Reload(object sender, EventArgs e)
        {
            LoadDivisionUnit();
        }

        private void cmbDivision_DropDownClosed(object sender, EventArgs e)
        {
            FetchExpenditures();
        }

        private void cmbYear_DropDownClosed(object sender, EventArgs e)
        {
          
            cmbRespo.IsEnabled = true;
        }
        private void FetchExpenditureItems()
        {
            c_unit.Process = "FetchExpenditureItems";
            svc_mindaf.ExecuteSQLAsync(c_unit.FetchExpenditureItems());
        }

        private void cmbExpenditures_DropDownClosed(object sender, EventArgs e)
        {
            txtBudget.Value = 0.00;
            txtBudget.IsEnabled = true;
            txtBudget.Focus();
        }

        private void grdData_CellClicked(object sender, Infragistics.Controls.Grids.CellClickedEventArgs e)
        {

        }

        private void grdData_CellDoubleClicked(object sender, Infragistics.Controls.Grids.CellClickedEventArgs e)
        {

        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {

        }

        private void cmbRespo_DropDownClosed(object sender, EventArgs e)
        {
            FetchExpenditureItems();
        }
    }
}

