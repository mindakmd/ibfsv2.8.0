﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmModeProcurementManager : ChildWindow
    {

        MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsProcurementMode c_procure = new clsProcurementMode();
        private List<ProcurementData> ListProcurementMode = new List<ProcurementData>();

        public frmModeProcurementManager()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_procure.Process)
            {
                case "LoadProcurementData":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new ProcurementData
                                     {
                                         Id = Convert.ToString(info.Element("Id").Value),
                                         Procurement = Convert.ToString(info.Element("Procurement").Value)
                                     };

                    ListProcurementMode.Clear();

                    foreach (var item in _dataLists)
                    {
                        ProcurementData _varDetails = new ProcurementData();


                        _varDetails.Id = item.Id;
                        _varDetails.Procurement = item.Procurement;


                        ListProcurementMode.Add(_varDetails);
                    }

                    this.Cursor = Cursors.Arrow;

                    grdData.ItemsSource = null;
                    grdData.ItemsSource = ListProcurementMode;

                    grdData.Columns["Id"].Visibility = System.Windows.Visibility.Collapsed;
                    break;
            }
        }
        private void LoadProcurementData() 
        {
            c_procure.Process = "LoadProcurementData";
            svc_mindaf.ExecuteSQLAsync(c_procure.LoadProcurementLibrary());
        }

        private void AddNewRow()
        {
            c_procure.Process = "AddNewRow";
            c_procure.AddNewRow();
            c_procure.SQLOperation += c_procure_SQLOperation;
        }

        private void UpdateRow(String Id, String Procurement)
        {
            c_procure.Process= "UpdateRow";
            c_procure.UpdateRow(Id, Procurement);
            c_procure.SQLOperation+=c_procure_SQLOperation;
        }

        void c_procure_SQLOperation(object sender, EventArgs e)
        {
            switch (c_procure.Process)
            {
                case "AddNewRow":
                    LoadProcurementData();
                    break;
                case "UpdateRow":
                    LoadProcurementData();
                    break;

            }
        }
        
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            AddNewRow();
        }

        
        private void frmProcmode_Loaded(object sender, RoutedEventArgs e)
        {
            LoadProcurementData();
        }

        private void grdData_CellExitedEditMode(object sender, Infragistics.Controls.Grids.CellExitedEditingEventArgs e)
        {
            try 
	        {	        
                String _id = grdData.Rows[grdData.ActiveCell.Row.Index].Cells[0].Value.ToString();
                String _procurement = grdData.Rows[grdData.ActiveCell.Row.Index].Cells[1].Value.ToString();
                UpdateRow(_id, _procurement);
	        }
	        catch (Exception ex)
	        {
                MessageBox.Show(ex.InnerException.Message);
		      
	        }
            
        }

        private void frmProcmode_Closed(object sender, EventArgs e)
        {

        }
    }
}

