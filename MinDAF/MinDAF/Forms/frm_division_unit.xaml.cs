﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frm_division_unit : ChildWindow
    {
        public event EventHandler ReloadData;
        public event EventHandler CloseRealignment;

        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsDivisionUnit c_div_units = new clsDivisionUnit();
        private List<DivisionUnit> _division_units = new List<DivisionUnit>();

        public String DivisionId { get; set; }
        public String PAP { get; set; }

        public frm_division_unit()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
              var _results = e.Result.ToString();
              switch (c_div_units.Process)
              {
                  case "LoadDivisionUnit":
                      XDocument oDocKeyResults = XDocument.Parse(_results);
                      var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                       select new DivisionUnit
                                       {
                                           Code = Convert.ToString(info.Element("unit_code").Value),
                                           Name = Convert.ToString(info.Element("unit_name").Value)
                                        
                                       };

                      _division_units.Clear();
                  

                      foreach (var item in _dataLists)
                      {
                          DivisionUnit _varDetails = new DivisionUnit();


                          _varDetails.Code = item.Code;
                          _varDetails.Name = item.Name;



                          _division_units.Add(_varDetails);

                      }

                      grdData.ItemsSource = null;
                      grdData.ItemsSource = _division_units;

                      grdData.Columns["Code"].Visibility = System.Windows.Visibility.Collapsed;
                      this.Cursor = Cursors.Arrow;

                      break;
              }
        }

        private void LoadDivisionUnit()
        {
            c_div_units.Process = "LoadDivisionUnit";
            svc_mindaf.ExecuteSQLAsync(c_div_units.GetDivisionUnits(DivisionId));
        }
        private void FetchPapCode()
        {
            c_div_units.Process = "FetchPapCode";
            svc_mindaf.ExecuteSQLAsync(c_div_units.GetDivisionUnits(DivisionId));
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void frmDiviUnit_Loaded(object sender, RoutedEventArgs e)
        {
            LoadDivisionUnit();
        }
        private void SaveDivisionUnit() 
        {
            c_div_units.Process = "SaveUnit";
            c_div_units.SQLOperation += c_div_units_SQLOperation;
            c_div_units.SaveDivisionUnit(DivisionId, txtName.Text);
            
        }
        private void RemoveDivisionUnit()
        {
            string _code = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["Code"].Value.ToString();

            c_div_units.Process = "RemoveUnit";
            c_div_units.SQLOperation += c_div_units_SQLOperation;
            c_div_units.RemoveDivisionUnit(_code);

        }
        void c_div_units_SQLOperation(object sender, EventArgs e)
        {
            switch (c_div_units.Process)
            {
                case "SaveUnit": case "RemoveUnit":
                    LoadDivisionUnit();
                    txtName.Text = "";
                    txtName.IsEnabled = false;
                    btnAdd.Content = "Add";
                    btnRemove.Content = "Remove";
                    grdData.IsEnabled = true;
                    break;
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            switch (btnAdd.Content.ToString())
            {
                case"Add":
                    grdData.IsEnabled = false;
                    txtName.IsEnabled = true;
                    btnAdd.Content = "Save";
                    btnRemove.Content = "Cancel";
                    txtName.Text = "";
                    txtName.Focus();
                    break;
                case "Save":
                    SaveDivisionUnit();
                    break;
            }
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            switch (btnRemove.Content.ToString())
            {
                case "Cancel":
                    txtName.Text = "";
                    txtName.IsEnabled = false;
                    btnAdd.Content = "Add";
                    btnRemove.Content = "Remove";
                    grdData.IsEnabled = true;
                    break;
                case "Remove":
                    RemoveDivisionUnit();
                    break;
            }
        }
    }
}

