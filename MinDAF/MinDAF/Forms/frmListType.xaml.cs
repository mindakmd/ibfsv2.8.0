﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmListType : ChildWindow
    {
        public event EventHandler SelectedUpdate;

        public List<RespoData> ActiveList { get; set; }
        public String SetSource { get; set; }
        public String FundSource { get; set; }

        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsRespoActualLibrary c_respo = new clsRespoActualLibrary();

        private List<RespoActual> ListRespoActual = new List<RespoActual>();
        public frmListType()
        {
            InitializeComponent();
            svc_mindaf.ExecuteImportDataSQLCompleted += svc_mindaf_ExecuteImportDataSQLCompleted;
            this.Loaded += frmListType_Loaded;
        }

        void frmListType_Loaded(object sender, RoutedEventArgs e)
        {
            FetchActualRespo();
        }
        private void FetchActualRespo()
        {
            c_respo.Process = "FetchActualRespo";
            switch (SetSource)
            {
                case "Current Appropriation":
                    svc_mindaf.ExecuteImportDataSQLAsync(c_respo.FetchActualLib());
                    break;
                case "Continuing Appropriation":
                    svc_mindaf.ExecuteImportDataSQLAsync(c_respo.FetchActualLibCONT());
                    break;

            }

        }
        void svc_mindaf_ExecuteImportDataSQLCompleted(object sender, ExecuteImportDataSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_respo.Process)
            {
                case "FetchActualRespo":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new RespoActual
                                     {
                                         FundSource = Convert.ToString(info.Element("fund_source").Value)

                                     };

                    ListRespoActual.Clear();


                    foreach (var item in _dataLists)
                    {
                        
                        List<RespoData> _Count = ActiveList.Where(x => x.Name == item.FundSource).ToList();
                        if (_Count.Count == 0)
                        {
                            RespoActual _varDetails = new RespoActual();

                            _varDetails.FundSource = item.FundSource;

                            ListRespoActual.Add(_varDetails);
                        }

                    }


                    grdData.ItemsSource = null;
                    grdData.ItemsSource = ListRespoActual;

                    this.Cursor = Cursors.Arrow;
                    break;

            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                FundSource = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["FundSource"].Value.ToString();

            }
            catch (Exception)
            {

                FundSource = "";
            }

            if (FundSource == "")
            {
                MessageBox.Show("No Source Selected");
                return;
            }
            else
            {
                if (SelectedUpdate != null)
                {
                    SelectedUpdate(this, new EventArgs());
                }
                this.DialogResult = true;
            }
          
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

