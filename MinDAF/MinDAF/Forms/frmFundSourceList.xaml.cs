﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmFundSourceList : ChildWindow
    {
        public event EventHandler Selected;
        public String DivisionId { get; set; }
        public String SelectedFundSource { get; set; }
        public String SelectedCode { get; set; }
        public String SelectedFundType { get; set; }

        public String WorkingYear { get; set; }

        private clsFundSource c_source = new clsFundSource();

        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private List<FundSourceDataList> _listFundsource = new List<FundSourceDataList>();

        public frmFundSourceList()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            this.Loaded += frmFundSourceList_Loaded;
        }

        void frmFundSourceList_Loaded(object sender, RoutedEventArgs e)
        {
            this.Fetchfundsource();    
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_source.Process)
            {
                case "Fetchfundsource":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new FundSourceDataList
                                     {
                                         Code = Convert.ToString(info.Element("code").Value),
                                         FundSource = Convert.ToString(info.Element("respo_name").Value),
                                           FundType = Convert.ToString(info.Element("fund_type").Value)
                                     };

                    _listFundsource.Clear();


                    foreach (var item in _dataLists)
                    {
                        FundSourceDataList _varDetails = new FundSourceDataList();


                        _varDetails.Code = item.Code;
                        _varDetails.FundSource = item.FundSource;
                        _varDetails.FundType = item.FundType;



                        _listFundsource.Add(_varDetails);

                    }

                    grdData.ItemsSource = null;
                    grdData.ItemsSource = _listFundsource;

                    grdData.Columns["Code"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["FundType"].Visibility = System.Windows.Visibility.Collapsed;
                    this.Cursor = Cursors.Arrow;

                    break;
            }
        }
        private void Fetchfundsource() 
        {
            c_source.Process = "Fetchfundsource";
            svc_mindaf.ExecuteSQLAsync(c_source.FetchFundsource(DivisionId,this.WorkingYear));
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            if (grdData.ActiveItem!=null)
            {
                FundSourceDataList _selected = (FundSourceDataList)grdData.ActiveItem;

                this.SelectedCode = _selected.Code;
                this.SelectedFundSource = _selected.FundSource;
                this.SelectedFundType = _selected.FundType;
                if (this.Selected!=null)
                {
                    this.Selected(this, new EventArgs());
                }
                this.DialogResult = true;
            }
           
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}

