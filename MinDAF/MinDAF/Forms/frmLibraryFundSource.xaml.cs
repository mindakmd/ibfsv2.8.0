﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmLibraryFundSource : ChildWindow
    {

        public event EventHandler Reload;
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsRespoLibrary c_respo = new clsRespoLibrary();
        private List<RespoData> ListRespo = new List<RespoData>();
        private List<RespoTypes> ListRespoTypes = new List<RespoTypes>();

        public Boolean IsDivision { get; set; }
        public String WorkingYear { get; set; }
        public frmLibraryFundSource()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        
        }


        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_respo.Process)
            {
                case "FetchRespo":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new RespoData
                                     {
                                         Id = Convert.ToString(info.Element("id").Value),
                                        Name  = Convert.ToString(info.Element("respo_name").Value),
                                        FundType  = Convert.ToString(info.Element("fund_type").Value),
                                     };

                    ListRespo.Clear();
                

                    foreach (var item in _dataLists)
                    {
                        RespoData _varDetails = new RespoData();


                        _varDetails.Id = item.Id;
                        _varDetails.Name= item.Name;
                        _varDetails.FundType = item.FundType;

                        ListRespo.Add(_varDetails);
                        
                    }


                    grdData.ItemsSource = null;
                    grdData.ItemsSource = ListRespo;

                    try
                    {
                        grdData.Columns["Id"].Visibility = System.Windows.Visibility.Collapsed;
                        grdData.Columns["FundType"].Visibility = System.Windows.Visibility.Collapsed;
                    }
                    catch (Exception)
                    {
                        
                    
                    }
                  
                    this.Cursor = Cursors.Arrow;

                   
                    break;
                case "FetchRespoType":
                    XDocument oDocKeyResultsFetchRespoType = XDocument.Parse(_results);
                    var _dataListsFetchRespoType = from info in oDocKeyResultsFetchRespoType.Descendants("Table")
                                     select new RespoTypes
                                     {
                                        Code  = Convert.ToString(info.Element("code").Value),
                                        Description  = Convert.ToString(info.Element("description").Value)
                                     };

                    ListRespoTypes.Clear();
                    cmbRespoTypes.Items.Clear();

                    foreach (var item in _dataListsFetchRespoType)
                    {
                        RespoTypes _varDetails = new RespoTypes();


                        _varDetails.Code     = item.Code;
                        _varDetails.Description = item.Description;


                        ListRespoTypes.Add(_varDetails);
                        cmbRespoTypes.Items.Add(item.Description);
                    }
                    cmbRespoTypes.SelectedIndex = 0;
                    this.Cursor = Cursors.Arrow;
                    FetchRespo();

                    break;
            }
        }
        private void FetchRespo() 
        {
            String _respo_code = "";
            c_respo.Process = "FetchRespo";
            List<RespoTypes> _selected = ListRespoTypes.Where(x => x.Description == cmbRespoTypes.SelectedItem.ToString()).ToList();

            if (_selected.Count!=0)
            {
                _respo_code = _selected[0].Code;

                switch (IsDivision)
                {
                    case true:
                        svc_mindaf.ExecuteSQLAsync(c_respo.FetchRespoLibrary(1,_respo_code,this.WorkingYear));
                        break;
                    case false:
                        svc_mindaf.ExecuteSQLAsync(c_respo.FetchRespoLibrary(0, _respo_code,this.WorkingYear));
                        break;
                }
            }

            
           
        }
        private void FetchRespoType()
        {
            c_respo.Process = "FetchRespoType";
            svc_mindaf.ExecuteSQLAsync(c_respo.FetchRespoTypes());

        }

        private void SaveRespo() 
        {
            String _respo_code = "";
            List<RespoTypes> _selected = ListRespoTypes.Where(x => x.Description == cmbRespoTypes.SelectedItem.ToString()).ToList();


            if (_selected.Count != 0)
            {
                _respo_code = _selected[0].Code;
            }

            c_respo.Process = "SaveRespo";
            c_respo.SQLOperation += c_respo_SQLOperation;

            switch (IsDivision)
            {
                case true:
                    c_respo.SaveRespo(txtRespo.Text, 1, _respo_code,this.WorkingYear);
                    break;
                case false:
                    c_respo.SaveRespo(txtRespo.Text, 0, _respo_code,this.WorkingYear);
                    break;
            }
            
        }

        private void UpdateRespo(String _id,String _name)
        {
            c_respo.Process = "UpdateRespo";
            c_respo.SQLOperation += c_respo_SQLOperation;
            c_respo.UpdateRespo(_id, _name,this.WorkingYear);
        }
        private void DeleteRespo(String _id)
        {
            c_respo.Process = "DeleteRespo";
            c_respo.SQLOperation += c_respo_SQLOperation;
            c_respo.DeleteRespo(_id,this.WorkingYear);
        }
        
        void c_respo_SQLOperation(object sender, EventArgs e)
        {
            txtRespo.Text = "";
            switch (c_respo.Process)
            {
                case "SaveRespo":
                    FetchRespo();
                    break;
                case"UpdateRespo":
                    FetchRespo();
                    break;
                case "DeleteRespo":
                    FetchRespo();
                    break;
            }
        }
       

        private void frm_respo_lib_Loaded(object sender, RoutedEventArgs e)
        {
            FetchRespoType();
           
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            SaveRespo();
        }

        private void grdData_CellExitedEditMode(object sender, Infragistics.Controls.Grids.CellExitedEditingEventArgs e)
        {
            try
            {
                String _name = e.Cell.Value.ToString();
                String _id = grdData.Rows[e.Cell.Row.Index].Cells["Id"].Value.ToString();
                UpdateRespo(_id, _name);
            }
            catch (Exception)
            {
               
              
            }
          

        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                String _id = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["Id"].Value.ToString();

                DeleteRespo(_id);
            }
            catch (Exception)
            {
                
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.Reload!=null)
            {
                Reload(this, new EventArgs());
            }
            this.Close();
        }

        private void cmbRespoTypes_DropDownClosed(object sender, EventArgs e)
        {
            FetchRespo();
        }
        frmLibRespo frm_lib = new frmLibRespo();
        frmListType frm_type = new frmListType();

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            frm_type = new frmListType();

          //  frm_lib = new frmLibRespo();
      
            frm_type.ActiveList = ListRespo;
            frm_type.SelectedUpdate += frm_type_SelectedUpdate;
            frm_type.SetSource = cmbRespoTypes.SelectedItem.ToString();
          //  frm_type.FundSource = ListRespoTypes[0].Code;
            frm_type.Show();


            //frm_lib.SelectedUpdate += frm_lib_SelectedUpdate;
            //frm_lib.ActiveList = ListRespo;
            //frm_lib.SetSource = cmbRespoTypes.SelectedItem.ToString();
            //frm_lib.Show();
        }

        void frm_type_SelectedUpdate(object sender, EventArgs e)
        {
            txtRespo.Text = frm_type.FundSource;
        }

        void frm_lib_SelectedUpdate(object sender, EventArgs e)
        {
            txtRespo.Text = frm_lib.FundSource;
        }
    }
}

