﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmRevisionStatus : ChildWindow
    {

        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsRevisionStatus c_revstat = new clsRevisionStatus();
        private List<RevisionStatus> ListRevStatus = new List<RevisionStatus>();
        public frmRevisionStatus()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            this.Loaded += frmRevisionStatus_Loaded;
        }

        void frmRevisionStatus_Loaded(object sender, RoutedEventArgs e)
        {
            LoadRevisionList();
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_revstat.Process)
            {
                case "LoadRevisionList":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new RevisionStatus
                                     {
                                         DivisionName = Convert.ToString(info.Element("divname").Value),
                                         Status = Convert.ToString(info.Element("status").Value)

                                     };

                    ListRevStatus.Clear();


                    foreach (var item in _dataLists)
                    {
                        RevisionStatus _varDetails = new RevisionStatus();

                        _varDetails.DivisionName = item.DivisionName;
                        _varDetails.Status = item.Status;

                        ListRevStatus.Add(_varDetails);

                    }

                    this.Cursor = Cursors.Arrow;

                    grdData.ItemsSource = null;
                    grdData.ItemsSource = ListRevStatus;
                    break;
            
            }
        }

        private void LoadRevisionList()
        {
            c_revstat.Process = "LoadRevisionList";
            svc_mindaf.ExecuteSQLAsync(c_revstat.FetchRevisionList());
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}

