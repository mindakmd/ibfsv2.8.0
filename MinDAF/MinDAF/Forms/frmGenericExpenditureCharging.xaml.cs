﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using MinDAF.Usercontrol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmGenericExpenditureCharging : ChildWindow
    {
        public String DivisionId { get; set; }
        public String AccountableID { get; set; }
        public String MOOE_ID { get; set; }
        public String MOOE_INDEX { get; set; }
        public String ActivityID { get; set; }
        public String _Year { get; set; }
        public String _Month { get; set; }
        public String FundSource { get; set; }

        public String FundMOOEName { get; set; }
        public event EventHandler ReloadData;
        public Boolean IsRevision { get; set; }

        private List<GridData> ListGridData = new List<GridData>();
        private List<DynamicEXPItems> ListDynamicData = new List<DynamicEXPItems>();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsDynamicExpenditures c_expen = new clsDynamicExpenditures();

        public frmGenericExpenditureCharging()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }
        private Double ComputeTotalBalance()
        {
            double _Total = 0.00;
            foreach (var item in ListGridData)
            {
                _Total += Convert.ToDouble(item.Total.ToString());
            }

            return _Total;
        }
        private BudgetRunningBalance _budget_bal { get; set; }
        private List<ProfData> _Combo = new List<ProfData>();
        private void LoadBudgetBalance()
        {
            _budget_bal = new BudgetRunningBalance(ComputeTotalBalance(), FundMOOEName, MOOE_ID,MOOE_INDEX);
            _budget_bal._DivisionID = DivisionId;
            _budget_bal._Year = this._Year;
            _budget_bal.WorkingYear = this._Year;
            _budget_bal._FundSource = this.FundSource;
            grdBR.Children.Clear();
            grdBR.Children.Add(_budget_bal);
        }
        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
             var _results = e.Result.ToString();
             switch (c_expen.Process)
             {
                 case "FetchDynamicExpenditure":
                     XDocument oDocKeyResults = XDocument.Parse(_results);
                     var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                      select new DynamicEXPItems
                                      {

                                          id = Convert.ToString(info.Element("id").Value),
                                          item_name = Convert.ToString(info.Element("item_name").Value),
                                          rate = Convert.ToString(info.Element("rate").Value)

                                      };


                     ListDynamicData.Clear();
                     
                     foreach (var item in _dataLists)
                     {
                         DynamicEXPItems _val = new DynamicEXPItems();
                         ProfData _var = new ProfData();

                         _val.id = item.id;
                         _val.item_name = item.item_name;
                         _val.rate = item.rate;
                         _var._Name = item.item_name;

                         _Combo.Add(_var);

                         ListDynamicData.Add(_val);
                        


                     }

                     FetchGridData();
                     this.Cursor = Cursors.Arrow;

                     break;
                 case "FetchGridData":
                     XDocument oDocKeyFetchGridData = XDocument.Parse(_results);
                     var _dataListsFetchGridData = from info in oDocKeyFetchGridData.Descendants("Table")
                                                   select new GridData
                                                   {
                                                       ActId = Convert.ToString(info.Element("ActId").Value),
                                                       Activity = Convert.ToString(info.Element("Activity").Value),
                                                       Assigned = Convert.ToString(info.Element("Assigned").Value),
                                                       Destination = Convert.ToString(info.Element("Destination").Value),
                                                       DateEnd = Convert.ToString(info.Element("DateEnd").Value),
                                                       Fare_Rate = Convert.ToString(info.Element("Fare_Rate").Value),
                                                       No_Staff = Convert.ToString(info.Element("No_Staff").Value),
                                                       Remarks = Convert.ToString(info.Element("Remarks").Value),
                                                       DateStart = Convert.ToString(info.Element("DateStart").Value),
                                                       Total = Convert.ToString(info.Element("Total").Value),
                                                       Travel_Allowance = Convert.ToString(info.Element("Travel_Allowance").Value),
                                                       Service_Type = Convert.ToString(info.Element("Service_Type").Value),
                                                       Breakfast = Convert.ToString(info.Element("Breakfast").Value),
                                                       AM_Snacks = Convert.ToString(info.Element("AM_Snacks").Value),
                                                       Lunch = Convert.ToString(info.Element("Lunch").Value),
                                                       PM_Snacks = Convert.ToString(info.Element("PM_Snacks").Value),
                                                       Dinner = Convert.ToString(info.Element("Dinner").Value),
                                                       No_Days = Convert.ToString(info.Element("No_Days").Value)

                                                   };

                     ListGridData.Clear();

                     foreach (var item in _dataListsFetchGridData)
                     {
                         GridData _varDetails = new GridData();

                         _varDetails.ActId = item.ActId;
                         _varDetails.Activity = item.Activity;
                         _varDetails.Assigned = item.Assigned;
                         _varDetails.Destination = item.Destination;
                         _varDetails.DateEnd = item.DateEnd;
                         _varDetails.Fare_Rate = item.Fare_Rate;
                         _varDetails.No_Staff = item.No_Staff;
                         _varDetails.Remarks = item.Remarks;
                         _varDetails.DateStart = item.DateStart;
                         _varDetails.Total = item.Total;
                         _varDetails.Travel_Allowance = item.Travel_Allowance;
                         _varDetails.Service_Type = item.Service_Type;
                         _varDetails.Breakfast = item.Breakfast;
                         _varDetails.AM_Snacks = item.AM_Snacks;
                         _varDetails.Lunch = item.Lunch;
                         _varDetails.PM_Snacks = item.PM_Snacks;
                         _varDetails.Dinner = item.Dinner;
                         _varDetails.No_Days = item.No_Days;
                         ListGridData.Add(_varDetails);

                     }
                     grdData.ItemsSource = null;
                     grdData.ItemsSource = ListGridData;
                     grdData.Columns["ActId"].Visibility = System.Windows.Visibility.Collapsed;
                     grdData.Columns["DateStart"].Visibility = System.Windows.Visibility.Collapsed;
                     grdData.Columns["DateEnd"].Visibility = System.Windows.Visibility.Collapsed;
                     grdData.Columns["Travel_Allowance"].Visibility = System.Windows.Visibility.Collapsed;
                     grdData.Columns["Destination"].Visibility = System.Windows.Visibility.Collapsed;
                     grdData.Columns["DateEnd"].Visibility = System.Windows.Visibility.Collapsed;
                     grdData.Columns["DateStart"].Visibility = System.Windows.Visibility.Collapsed;
                     grdData.Columns["Travel_Allowance"].Visibility = System.Windows.Visibility.Collapsed;
                     grdData.Columns["Service_Type"].Visibility = System.Windows.Visibility.Collapsed;
                     grdData.Columns["Breakfast"].Visibility = System.Windows.Visibility.Collapsed;
                     grdData.Columns["AM_Snacks"].Visibility = System.Windows.Visibility.Collapsed;
                     grdData.Columns["Lunch"].Visibility = System.Windows.Visibility.Collapsed;
                     grdData.Columns["Dinner"].Visibility = System.Windows.Visibility.Collapsed;
                     grdData.Columns["PM_Snacks"].Visibility = System.Windows.Visibility.Collapsed;
                     grdData.Columns["Quantity"].Visibility = System.Windows.Visibility.Collapsed;
                     grdData.Columns["Fare_Rate"].HeaderText = "Rate";
                     grdData.Columns["No_Staff"].HeaderText = "No. of Persons";
                     LoadBudgetBalance();
                     this.Cursor = Cursors.Arrow;
                     break;
             }
        }
        private void SaveData() 
        {
            String isRev = "0";
            if (IsRevision)
            {
                isRev = "1";
            }
            
            double _noRate = Convert.ToDouble(txtRate.Value);
            double _noQty = nudQuantity.Value;

            c_expen.Process = "SaveData";
            c_expen.SQLOperation += c_expen_SQLOperation;
            c_expen.SaveProjectDynamic(this.ActivityID, this.AccountableID, txtRemarks.Text,
                "0",_noRate, Convert.ToDouble(txttotal.Value), this._Month, this._Year,
                this.MOOE_INDEX, "0",txtLibrary.Text, this.FundSource,isRev);
        }

        void c_expen_SQLOperation(object sender, EventArgs e)
        {
            switch (c_expen.Process)
            {
                case "SaveData":
                    FetchGridData();
                    break;
                case "Suspend":
                    FetchGridData();
                    break;
                default:
                    break;
            }

        }
        private void FetchGridData()
        {
            c_expen.Process = "FetchGridData";
            svc_mindaf.ExecuteSQLAsync(c_expen.FetchLocalData(this.ActivityID,this._Month,this._Year,this.MOOE_INDEX,this.FundSource));
        }
        private void FetchDynamicExpenditure() 
        {
            c_expen.Process = "FetchDynamicExpenditure";
            svc_mindaf.ExecuteSQLAsync(c_expen.FetchExpenditureItems(this.MOOE_INDEX));
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void frm_b_dynamic_Loaded(object sender, RoutedEventArgs e)
        {
            FetchDynamicExpenditure();
        }

        private void cmbList_DropDownClosed(object sender, EventArgs e)
        {
           
        }
        private void ComputeTotal() 
        {
            try
            {
                double _rate = Convert.ToDouble(txtRate.Value);
                double _qty = nudQuantity.Value;

                txttotal.Value = _rate * _qty;
            }
            catch (Exception)
            {
                
            }
      

        }
        private void nudQuantity_ValueChanging(object sender, RoutedPropertyChangingEventArgs<double> e)
        {
            ComputeTotal();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {

            if (_budget_bal.BalanceOff < Convert.ToDouble(txttotal.Value))
            {
                frmNotifyBalance fBa = new frmNotifyBalance();

                fBa.Show();
            }
            else
            {
                SaveData();
            }
        }

        private void frm_b_dynamic_Closed(object sender, EventArgs e)
        {
            if (ReloadData!=null)
            {
                ReloadData(this, new EventArgs());
            }
        }

        private void nudQuantity_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }
        private void SuspendActivity()
        {
            String _id = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["ActId"].Value.ToString();
            c_expen.Process = "Suspend";
            c_expen.SQLOperation+=c_expen_SQLOperation;
            c_expen.UpdateSuspend(_id, "1");

        }
        private void btnSuspend_Click(object sender, RoutedEventArgs e)
        {
            SuspendActivity();
        }

        private void cmbList_DropDownClosed_1(object sender, EventArgs e)
        {
           
               
          
        }
        frmLibList f_form ;
        private void btnLoadLibrary_Click(object sender, RoutedEventArgs e)
        {
            f_form = new frmLibList();
            f_form.GridData = _Combo;
            f_form.Selected += f_form_Selected;
            f_form.Show();
        }

        void f_form_Selected(object sender, EventArgs e)
        {
            try
            {
                txtLibrary.Text = f_form.SelectedData;

                List<DynamicEXPItems> _x = ListDynamicData.Where(items => items.item_name == txtLibrary.Text).ToList();
                foreach (var item in _x)
                {
                    txtRate.Value = Convert.ToDouble(item.rate.ToString());
                }

                ComputeTotal();
            }
            catch (Exception)
            {
                txtLibrary.Focus();
               
            }
            
        }
    }
}

