﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmProcActivity : ChildWindow
    {
        public String SelectMode { get; set; }
        public event EventHandler SelectedData;
        MinDAFSVCClient svc_mindaf_c = new MinDAFSVCClient();
        private clsProcurementActivityLibrary c_procure = new clsProcurementActivityLibrary();
        private List<ProcurementActivityData> ListProcurementMode = new List<ProcurementActivityData>();

        public frmProcActivity()
        {
            InitializeComponent();
            svc_mindaf_c.ExecuteQueryCompleted += svc_mindaf_c_ExecuteQueryCompleted;
        }

        void svc_mindaf_c_ExecuteQueryCompleted(object sender, ExecuteQueryCompletedEventArgs e)
        {
            
        }

        //void svc_mindaf_ExecuteQueryCompleted(object sender, ExecuteQueryCompletedEventArgs e)
        //{
        //    var _results = e.Result.ToString();
        //    switch (c_procure.Process)
        //    {
        //        case "LoadData":
        //            XDocument oDocKeyResults = XDocument.Parse(_results);
        //            var _dataLists = from info in oDocKeyResults.Descendants("Table")
        //                             select new ProcurementActivityData
        //                             {
        //                                 Id = Convert.ToString(info.Element("Id").Value),
        //                                 Library = Convert.ToString(info.Element("Library").Value)
        //                             };

        //            ListProcurementMode.Clear();

        //            foreach (var item in _dataLists)
        //            {
        //                ProcurementActivityData _varDetails = new ProcurementActivityData();


        //                _varDetails.Id = item.Id;
        //                _varDetails.Library = item.Library;


        //                ListProcurementMode.Add(_varDetails);
        //            }

        //            this.Cursor = Cursors.Arrow;

        //            grdData.ItemsSource = null;
        //            grdData.ItemsSource = ListProcurementMode;

        //            grdData.Columns["Id"].Visibility = System.Windows.Visibility.Collapsed;
        //            break;
        //    }
        //}
        private void LoadData()
        {
            c_procure.Process = "LoadData";
            svc_mindaf_c.ExecuteSQLAsync(c_procure.LoadProcurementActivityLibrary());
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

       
        private void grdData_CellDoubleClicked(object sender, Infragistics.Controls.Grids.CellClickedEventArgs e)
        {
            this.SelectMode = grdData.Rows[this.grdData.ActiveCell.Row.Index].Cells[1].Value.ToString();
            if (SelectedData != null)
            {
                SelectedData(this, new EventArgs());
            }
            this.DialogResult = true;
        }

       

        private void frmprocact_Loaded(object sender, RoutedEventArgs e)
        {
            LoadData();
        }
    }
}

