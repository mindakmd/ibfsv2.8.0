﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Forms
{
    public partial class frmMonths : ChildWindow
    {
        public event EventHandler OkData;
        public String SelectedMonth { get; set; }

        private List<frmMonths_ListMonths> _Months = new List<frmMonths_ListMonths>();
        public frmMonths()
        {
            InitializeComponent();
            this.Loaded += frmMonths_Loaded;
        }

        void frmMonths_Loaded(object sender, RoutedEventArgs e)
        {
            ListMonths();
        }

        private void ListMonths() 
        {
            _Months.Clear();
            frmMonths_ListMonths _data;
            _data = new frmMonths_ListMonths();
            _data.Months = "January";
            _Months.Add(_data);
            _data = new frmMonths_ListMonths();
            _data.Months = "February";
            _Months.Add(_data);
            _data = new frmMonths_ListMonths();
            _data.Months = "March";
            _Months.Add(_data);
            _data = new frmMonths_ListMonths();
            _data.Months = "April";
            _Months.Add(_data);
            _data = new frmMonths_ListMonths();
            _data.Months = "May";
            _Months.Add(_data);
            _data = new frmMonths_ListMonths();
            _data.Months = "June";
            _Months.Add(_data);
            _data = new frmMonths_ListMonths();
            _data.Months = "July";
            _Months.Add(_data);
            _data = new frmMonths_ListMonths();
            _data.Months = "August";
            _Months.Add(_data);
            _data = new frmMonths_ListMonths();
            _data.Months = "September";
            _Months.Add(_data);
            _data = new frmMonths_ListMonths();
            _data.Months = "October";
            _Months.Add(_data);
            _data = new frmMonths_ListMonths();
            _data.Months = "November";
            _Months.Add(_data);
            _data = new frmMonths_ListMonths();
            _data.Months = "December";
            _Months.Add(_data);

            grdList.ItemsSource = _Months;

        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            frmMonths_ListMonths _selected = (frmMonths_ListMonths)grdList.ActiveItem;

            SelectedMonth = _selected.Months;
            if (this.OkData!=null)
            {
                this.OkData(this, new EventArgs());
               
            }
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
    public class frmMonths_ListMonths     
    {
        public String Months { get; set; }
    }
}

