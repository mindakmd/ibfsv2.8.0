﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Forms
{
    public partial class programeditor : ChildWindow
    {
        public string _Menu { get; set; }
        public String _ProgramCode { get; set; }
        public String _ProjectCode { get; set; }
        public String _OutputCode { get; set; }
        public String _ActivityCode { get; set; }

        public String _Data { get; set; }

        public event EventHandler ReloadData;

        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsMainData c_main_data = new clsMainData();

        public programeditor()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void UpdateProgramTable()
        {
            c_main_data.Process = "UpdateRowProgram";
            c_main_data.SQLOperation += c_main_data_SQLOperation;
            c_main_data.UpdateProgramTable(_ProgramCode, txtEdit.Text);

        }
        private void UpdateProjectTable()
        {
            c_main_data.Process = "UpdateRowProgram";
            c_main_data.SQLOperation += c_main_data_SQLOperation;
            c_main_data.UpdateProjectTable(_ProjectCode, txtEdit.Text);

        }
        private void UpdateOutPutTable()
        {
            c_main_data.Process = "UpdateRowProgram";
            c_main_data.SQLOperation += c_main_data_SQLOperation;
            c_main_data.UpdateOutputTable(_OutputCode, txtEdit.Text);

        }
        private void UpdateActivityTable()
        {
            c_main_data.Process = "UpdateRowProgram";
            c_main_data.SQLOperation += c_main_data_SQLOperation;
            c_main_data.UpdateActivityTable(_ActivityCode, txtEdit.Text);

        }

        void c_main_data_SQLOperation(object sender, EventArgs e)
        {
            if (ReloadData!=null)
            {
                this.ReloadData(this, new EventArgs());
            }
           
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            switch (this._Menu)
            {

                case "program":
                    UpdateProgramTable();
                    break;
                case "project":
                    UpdateProjectTable();
                    break;
                case "output":
                    UpdateOutPutTable();
                    break;
                case "activity":
                    UpdateActivityTable();
                    break; 
                default:
                    break;
            }
            
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void frmEditorData_Loaded(object sender, RoutedEventArgs e)
        {
            txtEdit.Text = this._Data;
        }
    }
}

