﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using MinDAF.ReportService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Forms
{
    public partial class frmReportBalances : ChildWindow
    {
        public String FundSource { get; set; }
        public frmReportBalances()
        {
            InitializeComponent();
            InitializeReport();
            this.Loaded += frmReportBalances_Loaded;
        }

        void frmReportBalances_Loaded(object sender, RoutedEventArgs e)
        {
            InitData();
        }
        private ReportReportBalances _data = new ReportReportBalances();
        private List<ReportReportBalances> _main = new List<ReportReportBalances>();

        private void InitData() 
        {
            var client = new DataServiceClient();

            client.GetOBRDetailsCompleted += (s, ea) =>
            {
                Report.ItemsSource = ea.Result;
                _main.Clear();
                foreach (var item in ea.Result)
                {
                    _data = new ReportReportBalances();
                    _data.ActualOBR = item.ActualOBR;
                    _data.Allocation = item.Allocation;
                    _data.Balances = item.Balances;
                    _data.ExpenseItem = item.ExpenseItem;
                    _data.UACS = item.UACS;


                    _main.Add(_data);
                }
                grdData.ItemsSource = _main;
                if (ea.Result.Count > 0)
                {
                    OKButton.IsEnabled = true;
                 //   PrintReportForceVector.IsEnabled = true;
                    StatusText.Content = "Data Loaded. Report can now be printed.";
                }
                else
                {
                    OKButton.IsEnabled = false;
                  //  PrintReportForceVector.IsEnabled = false;
                    StatusText.Content = "Failed to load data.";
                }
            };

            client.GetOBRDetailsAsync(FundSource);
        }
        private void InitializeReport()
        {
            Report.EndPrint += (s, e) =>
            {
                //MessageBox.Show("Report printed.");
                StatusText.Content = "Report printed.";
            };

            Report.BeginBuildReport += (s, e) =>
            {
                StatusText.Content = "Building and printing report.";
                // initialize running totals
              
                
            };

            Report.BeginBuildReportItem += (s, e) =>
            {
               
                var item = e.DataContext as ReportBalances;

             
            };
           
            Report.BeginBuildReportFooter += (s, e) =>
            {
                // set the running total as the context for the report footer
               // e.DataContext = _data;
              //  grdData.DataContext = _main;
            };

        }

       
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
           
            Report.Print();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            InitializeReport();
            InitData();
        }
    }
}

