﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmPRRequest : ChildWindow
    {

        public String DivisionID { get; set; }
        public String _Year { get; set; }

        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsPRRequest c_pr = new clsPRRequest();

        private List<_libMonthActivityDetails> ListMonthActivityDetail = new List<_libMonthActivityDetails>();
        private List<_libMonthActivity> ListMonthActivity = new List<_libMonthActivity>();
        private List<_LibMainActivity> MainData = new List<_LibMainActivity>();
        private List<_LibPRRequest> PRData = new List<_LibPRRequest>();

        public frmPRRequest()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
             var _results = e.Result.ToString();
             switch (c_pr.Process)
             {
                 case "LoadMonthActivity":
                     XDocument oDocKeyResultsLoadMonthActivity = XDocument.Parse(_results);
                     var _dataListsLoadLoadMonthActivity = from info in oDocKeyResultsLoadMonthActivity.Descendants("Table")
                                                             select new _libMonthActivity
                                                             {
                                                                 activity = Convert.ToString(info.Element("activity").Value),
                                                                 id = Convert.ToString(info.Element("act_id").Value),
                                                                
                                                             };

                     ListMonthActivity.Clear();



                     foreach (var item in _dataListsLoadLoadMonthActivity)
                     {
                         _libMonthActivity _varDetails = new _libMonthActivity();


                         _varDetails.activity = item.activity;
                         _varDetails.id = item.id;
                     
                         ListMonthActivity.Add(_varDetails);

                     }

                     LoadMonthActivityDetails();

                     break;
                 case "LoadPRRequest":
                                     XDocument oDocKeyResultsLoadPRRequest = XDocument.Parse(_results);
                                     var _dataListsLoadLoadPRRequest = from info in oDocKeyResultsLoadPRRequest.Descendants("Table")
                                                                             select new _LibPRRequest
                                                                             {
                                                                                 ActId = Convert.ToString(info.Element("act_id").Value),
                                                                                  Details= Convert.ToString(info.Element("Details").Value),
                                                                                  ExpenseItem= Convert.ToString(info.Element("ExpenseItem").Value),
                                                                                  Status= Convert.ToString(info.Element("Status").Value),
                                                                                  Total= Convert.ToString(info.Element("Total").Value),
                                                                
                                                                             };

                                     PRData.Clear();

                                     foreach (var item in _dataListsLoadLoadPRRequest)
                                     {
                                         _LibPRRequest _varDetails = new _LibPRRequest();

                                         _varDetails.ActId = item.ActId;
                                         _varDetails.Details = item.Details;
                                         _varDetails.ExpenseItem = item.ExpenseItem;
                                         _varDetails.Status = item.Status;
                                         _varDetails.Total = item.Total;
                                         PRData.Add(_varDetails);

                                     }

                                     grdSubmit.ItemsSource = null;
                                     grdSubmit.ItemsSource = PRData;
                                     grdSubmit.Columns["ActId"].Visibility = System.Windows.Visibility.Collapsed;
                                     break;

                 case "LoadMonthActivityDetails":
                     XDocument oDocKeyResultsLoadMonthActivities = XDocument.Parse(_results);
                     var _dataListsLoadLoadMonthActivities = from info in oDocKeyResultsLoadMonthActivities.Descendants("Table")
                                                    select new _libMonthActivityDetails
                                                    {
                                                        det_id = Convert.ToString(info.Element("det_id").Value),
                                                        act_id = Convert.ToString(info.Element("act_id").Value),
                                                        activity = Convert.ToString(info.Element("activity").Value),
                                                        details = Convert.ToString(info.Element("details").Value),
                                                        div_id = Convert.ToString(info.Element("div_id").Value),
                                                        month = Convert.ToString(info.Element("month").Value),
                                                        name = Convert.ToString(info.Element("name").Value),
                                                        total = Convert.ToString(info.Element("total").Value),
                                                        uacs_code = Convert.ToString(info.Element("uacs_code").Value)
                                                    };

                     ListMonthActivityDetail.Clear();

                     MainData.Clear();

                     foreach (var item in _dataListsLoadLoadMonthActivities)
                     {
                         _libMonthActivityDetails _varDetails = new _libMonthActivityDetails();

                         _varDetails.det_id = item.det_id;
                         _varDetails.act_id = item.act_id;
                         _varDetails.activity = item.activity;
                         _varDetails.details = item.details;
                         _varDetails.div_id = item.div_id;
                         _varDetails.month = item.month;
                         _varDetails.name = item.name;
                         _varDetails.total = Convert.ToDouble(item.total).ToString("#,##0.00");
                         _varDetails.uacs_code = item.uacs_code;
                      //   _varDetails._set = true    ;

                         ListMonthActivityDetail.Add(_varDetails);
                         
                     }

                     foreach (_libMonthActivity item in ListMonthActivity)
                     {
                         _LibMainActivity _data = new _LibMainActivity();

                         _data.id = item.id;
                         _data.activity = item.activity;

                         List<_libMonthActivityDetails> _details = ListMonthActivityDetail.Where(x => x.act_id == item.id).ToList();

                         _data.details = _details;

                         MainData.Add(_data);
                     }

                     grdData.ItemsSource = null;
                     grdData.ItemsSource = MainData;
                   
                      grdData.Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
                      grdData.Columns["activity"].HeaderText = "Activity";
                      foreach (var item in grdData.Rows)
                      {
                          if (item.HasChildren)
                          {
                              item.ChildBands[0].Columns["act_id"].Visibility = System.Windows.Visibility.Collapsed;
                              item.ChildBands[0].Columns["activity"].Visibility = System.Windows.Visibility.Collapsed;
                              item.ChildBands[0].Columns["div_id"].Visibility = System.Windows.Visibility.Collapsed;
                              item.ChildBands[0].Columns["month"].Visibility = System.Windows.Visibility.Collapsed;
                              item.ChildBands[0].Columns["uacs_code"].Visibility = System.Windows.Visibility.Collapsed;
                              item.ChildBands[0].Columns["det_id"].Visibility = System.Windows.Visibility.Collapsed;

                              item.ChildBands[0].Columns["name"].HeaderText = "Expense Item";
                              item.ChildBands[0].Columns["details"].HeaderText = "Specifications";
                              item.ChildBands[0].Columns["total"].HeaderText = "Total";
                           //   item.ChildBands[0].Columns["_set"].HeaderText = "Check To Select";
                              
                              item.IsExpanded = true;
                          }
                         
                      }
                     //grdData.Columns["month"].Visibility = System.Windows.Visibility.Collapsed;
                      LoadPRRequest();
                     break;
             }
        }
     
        private void GenerateMonths() 
        {
            cmbMonth.Items.Clear();
            cmbMonth.Items.Add("Jan");
            cmbMonth.Items.Add("Feb");
            cmbMonth.Items.Add("Mar");
            cmbMonth.Items.Add("Apr");
            cmbMonth.Items.Add("May");
            cmbMonth.Items.Add("Jun");
            cmbMonth.Items.Add("Jul");
            cmbMonth.Items.Add("Aug");
            cmbMonth.Items.Add("Sep");
            cmbMonth.Items.Add("Oct");
            cmbMonth.Items.Add("Nov");
            cmbMonth.Items.Add("Dec");
            Int32 _month = DateTime.Now.Month;
            String _monthname = "";
            switch (_month)
            {
                case 1:
                    _monthname = "Jan";
                    break;
                case 2:
                    _monthname = "Feb";
                    break;
                case 3:
                    _monthname = "Mar";
                    break;
                case 4:
                    _monthname = "Apr";
                    break;
                case 5:
                    _monthname = "May";
                    break;
                case 6:
                    _monthname = "Jun";
                    break;
                case 7:
                    _monthname = "Jul";
                    break;
                case 8:
                    _monthname = "Aug";
                    break;
                case 9:
                    _monthname = "Sep";
                    break;
                case 10:
                    _monthname = "Oct";
                    break;
                case 11:
                    _monthname = "Nov";
                    break;
                case 12:
                    _monthname = "Dec";
                    break;
            }

            for (int i = 0; i < cmbMonth.Items.Count; i++)
            {
                if (cmbMonth.Items[i].ToString() == _monthname)
                {
                    cmbMonth.SelectedIndex = i;
                    break;
                }
            }
        }
        private void LoadMonthActivity()
        {
            c_pr.Process = "LoadMonthActivity";
            svc_mindaf.ExecuteSQLAsync(c_pr.FetchMonthlyActivities(cmbMonth.SelectedItem.ToString(), this.DivisionID));
        }
        private void LoadPRRequest()
        {
            c_pr.Process = "LoadPRRequest";
            svc_mindaf.ExecuteSQLAsync(c_pr.LoadForPRRequest(cmbMonth.SelectedItem.ToString(), this.DivisionID));
        }
        private void LoadMonthActivityDetails() 
        {
            c_pr.Process = "LoadMonthActivityDetails";
            svc_mindaf.ExecuteSQLAsync(c_pr.FetchMonthlyActivityDetails(cmbMonth.SelectedItem.ToString(), this.DivisionID));
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void cmbMonth_DropDownClosed(object sender, EventArgs e)
        {
            LoadMonthActivity() ;
        }

        private void frmPRForm_Loaded(object sender, RoutedEventArgs e)
        {
            GenerateMonths();
        }

        private void grdData_CellClicked(object sender, Infragistics.Controls.Grids.CellClickedEventArgs e)
        {
            btnTransfer.Content = ">>";
        }

        private void SubmitSelected()
        {
    
            String _sqlString = "";
            StringBuilder sb = new StringBuilder(79);
            String SQLCommand = "";

            foreach (var item in grdData.SelectionSettings.SelectedRows)
            {
                 
                    _sqlString = "UPDATE dbo.mnda_activity_data SET status ='PR REQUEST' WHERE id = "+ item.Cells["det_id"].Value.ToString() +"";

                    SQLCommand += _sqlString;
            
            }

            c_pr.Process = "SubmitData";


            svc_mindaf.ExecuteQueryListsCompleted += svc_mindaf_ExecuteQueryListsCompleted;
            svc_mindaf.ExecuteQueryListsAsync(SQLCommand);
        }

        private void ReturnSelected()
        {

            String _sqlString = "";
            StringBuilder sb = new StringBuilder(79);
            String SQLCommand = "";

            foreach (var item in grdSubmit.SelectionSettings.SelectedRows)
            {

                _sqlString = "UPDATE dbo.mnda_activity_data SET status ='FINANCE APPROVED' WHERE id = " + item.Cells["ActId"].Value.ToString() + "; ";

                SQLCommand += _sqlString;

            }

            c_pr.Process = "ReturnData";


            svc_mindaf.ExecuteQueryListsCompleted += svc_mindaf_ExecuteQueryListsCompleted;
            svc_mindaf.ExecuteQueryListsAsync(SQLCommand);
        }

        void svc_mindaf_ExecuteQueryListsCompleted(object sender, ExecuteQueryListsCompletedEventArgs e)
        {
            switch (c_pr.Process)
            {
                case "SubmitData":
                    LoadMonthActivityDetails();
                    break;
                case "ReturnData":
                    LoadMonthActivityDetails();
                    break;
               
                default:
                    break;
            }
        }

        private void btnTransfer_Click(object sender, RoutedEventArgs e)
        {
            switch (btnTransfer.Content.ToString())
            {
                case ">>":
                    SubmitSelected();
                    break;
                case "<<":
                    ReturnSelected();
                    break;
            }
           
        }

        private void grdSubmit_CellClicked(object sender, Infragistics.Controls.Grids.CellClickedEventArgs e)
        {
            btnTransfer.Content = "<<";
        }
    }
}

