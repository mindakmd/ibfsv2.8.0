﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Forms
{
    public partial class frmActivityReportPerTotal : ChildWindow
    {
        public event EventHandler Reload;
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsActivityReport  c_activity = new clsActivityReport();
  
        private List<ActivityData_Report> ListActivityTotal = new List<ActivityData_Report>();
        private List<ActivityData_Programs> ListProgramProject = new List<ActivityData_Programs>();
        private List<ActivityData_Output> ListOutputs = new List<ActivityData_Output>();
        private List<ActivityData_Activity> ListActivity= new List<ActivityData_Activity>();
        private List<ActivityData_ExpenseItem> ListExpense = new List<ActivityData_ExpenseItem>();
        public String DivisionId { get; set; }
        public String _Year { get; set; }

        public frmActivityReportPerTotal()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_activity.Process)
            {
                case "FetchProjectPrograms":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new ActivityData_Programs
                                     {
                                         Id = Convert.ToString(info.Element("project_id").Value),
                                        Program  = Convert.ToString(info.Element("program").Value),
                                        Project  = Convert.ToString(info.Element("project").Value)
                                
                                     };

                    ListProgramProject.Clear();
                   

                    foreach (var item in _dataLists)
                    {
                        ActivityData_Programs _varDetails = new ActivityData_Programs();

                        _varDetails.Id = item.Id;
                        _varDetails.Program = item.Program;
                        _varDetails.Project = item.Project;
                  


                        ListProgramProject.Add(_varDetails);
                      
                    }



                    this.Cursor = Cursors.Arrow;

                    FetchOutputs();
                    break;
                case "FetchOutputs":
                    XDocument oDocKeyResultsFetchOutputs = XDocument.Parse(_results);
                    var _dataListsFetchOutputs = from info in oDocKeyResultsFetchOutputs.Descendants("Table")
                                     select new ActivityData_Output
                                     {
                                         Id = Convert.ToString(info.Element("program_id").Value),
                                         OutputId = Convert.ToString(info.Element("output_id").Value),
                                         Output  = Convert.ToString(info.Element("output").Value)

                                     };

                    ListOutputs.Clear();


                    foreach (var item in _dataListsFetchOutputs)
                    {
                        ActivityData_Output _varDetails = new ActivityData_Output();

                        _varDetails.Id = item.Id;
                        _varDetails.Output = item.Output;
                        _varDetails.OutputId = item.OutputId;



                        ListOutputs.Add(_varDetails);

                    }



                    this.Cursor = Cursors.Arrow;

                    FetchActivities();
                    break;
                case "FetchActivities":
                    XDocument oDocKeyResultsFetchActivities = XDocument.Parse(_results);
                    var _dataListsFetchActivities = from info in oDocKeyResultsFetchActivities.Descendants("Table")
                                                 select new ActivityData_Activity
                                                 {
                                                     Id = Convert.ToString(info.Element("act_id").Value),
                                                     Activity = Convert.ToString(info.Element("description").Value),
                                                     OutputId = Convert.ToString(info.Element("output_id").Value)
                                                 };

                    ListActivity.Clear();


                    foreach (var item in _dataListsFetchActivities)
                    {
                        ActivityData_Activity _varDetails = new ActivityData_Activity();

                        _varDetails.Id = item.Id;
                        _varDetails.Activity = item.Activity;
                        _varDetails.OutputId = item.OutputId;



                        ListActivity.Add(_varDetails);

                    }



                    this.Cursor = Cursors.Arrow;

                    FetchExpenseItems();

                    break;
                case "FetchExpenseItems":
                    XDocument oDocKeyResultsFetchExpenseItems = XDocument.Parse(_results);
                    var _dataListsFetchExpenseItems = from info in oDocKeyResultsFetchExpenseItems.Descendants("Table")
                                                    select new ActivityData_ExpenseItem
                                                    {
                                                        ActId = Convert.ToString(info.Element("act_id").Value),
                                                        Expense_Item = Convert.ToString(info.Element("expense_item").Value),
                                                        Total = Convert.ToString(info.Element("total").Value)
                                                    };

                    ListExpense.Clear();


                    foreach (var item in _dataListsFetchExpenseItems)
                    {
                        ActivityData_ExpenseItem _varDetails = new ActivityData_ExpenseItem();

                        _varDetails.ActId = item.ActId;
                        _varDetails.Expense_Item = item.Expense_Item;
                        _varDetails.Total = item.Total;



                        ListExpense.Add(_varDetails);

                    }



                    this.Cursor = Cursors.Arrow;

                    GenerateData();
                    break;

            }
        }

        private void GenerateData ()
        {
            List<ActivityData_Main> _data = new List<ActivityData_Main>();

            foreach (ActivityData_Programs item in ListProgramProject)
            {
                ActivityData_Main x_programs_space = new ActivityData_Main();
                x_programs_space.Program_Project = "PROGRAM";
                _data.Add(x_programs_space);

                ActivityData_Main x_programs = new ActivityData_Main();
                x_programs.Program_Project = item.Program;
                _data.Add(x_programs);

                ActivityData_Main x_projects_space = new ActivityData_Main();
                x_projects_space.Program_Project = "          " + "PROJECT";
                _data.Add(x_projects_space);

                ActivityData_Main x_projects = new ActivityData_Main();

                x_projects.Program_Project = "                 " + item.Project;


                _data.Add(x_projects);


                List<ActivityData_Output> x_output_data = ListOutputs.Where(x => x.Id == item.Id).ToList();

                ActivityData_Main x_output_space = new ActivityData_Main();
                x_output_space.Program_Project = "                        " + "OUTPUT";
                _data.Add(x_output_space);

                foreach (var results_output in x_output_data)
                {
                    List<ActivityData_Main> x_check = _data.Where(xd => xd.Program_Project == "\"" + results_output.Output + "\"").ToList();
                    if (x_check.Count==0)
                    {
                        ActivityData_Main x_output = new ActivityData_Main();
                        x_output.Program_Project = "                              " + "\"" + results_output.Output + "\"";
                        _data.Add(x_output);
                    }
                   

                    List<ActivityData_Activity> x_activity = ListActivity.Where(x => x.OutputId == results_output.OutputId).ToList();
                    ActivityData_Main x_activity_space = new ActivityData_Main();
                    x_activity_space.Program_Project = "                                    " + "ACTIVITIES";
                    _data.Add(x_activity_space);

                    foreach (var itemx_activity in x_activity)
                    {
                        ActivityData_Main x_activityd= new ActivityData_Main();

                        x_activityd.Program_Project = "                                               " + itemx_activity.Activity;

                        _data.Add(x_activityd);

                        ActivityData_Main x_expense_space = new ActivityData_Main();
                        x_expense_space.Program_Project = "                                                         EXPENSES";

                        _data.Add(x_expense_space);

                        List<ActivityData_ExpenseItem> x_expense = ListExpense.Where(x => x.ActId == itemx_activity.Id).ToList();
                        Int32 _counter = 0;
                        double _Totals = 0;
                        foreach (var item_x_expense in x_expense)
                        {
                           
                            _counter += 1;
                             ActivityData_Main x_exp= new ActivityData_Main();

                             x_exp.Program_Project = "                                                                " + _counter.ToString() + ".) " + item_x_expense.Expense_Item + " : " + item_x_expense.Total;

                            _data.Add(x_exp);
                            _Totals += Convert.ToDouble(item_x_expense.Total);
                        }
                        ActivityData_Main x_space_line = new ActivityData_Main();

                        x_space_line.Program_Project = "                                                                ----------------------------------------------";

                        _data.Add(x_space_line);

                        ActivityData_Main x_Total = new ActivityData_Main();

                        x_Total.Program_Project = "                                                                 Total : " + _Totals.ToString("#,##0.00");

                        _data.Add(x_Total);

                    }
                }
                ActivityData_Main x_activity_line = new ActivityData_Main();
                x_activity_line.Program_Project = "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------";
                _data.Add(x_activity_line);

                 
            }

            grdData.ItemsSource = null;
            grdData.ItemsSource = _data;
        }
        
           
        

        private void FetchProjectPrograms() 
        {
            c_activity.Process = "FetchProjectPrograms";
            svc_mindaf.ExecuteSQLAsync(c_activity.FetchPrograms(this.DivisionId, this._Year));
        }

        private void FetchActivities()
        {
            c_activity.Process = "FetchActivities";
            svc_mindaf.ExecuteSQLAsync(c_activity.FetchActivities(this.DivisionId, this._Year));
        }

        private void FetchOutputs() 
        {
            c_activity.Process = "FetchOutputs";
            svc_mindaf.ExecuteSQLAsync(c_activity.FetchOutputs(this.DivisionId, this._Year));
        }

        private void FetchExpenseItems()
        {
            c_activity.Process = "FetchExpenseItems";
            svc_mindaf.ExecuteSQLAsync(c_activity.FetchExpenseItems(this.DivisionId, this._Year));
        }
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void frmActTotal_Loaded(object sender, RoutedEventArgs e)
        {
            FetchProjectPrograms();
        }
    }
}

