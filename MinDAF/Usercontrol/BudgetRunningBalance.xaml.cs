﻿using MinDAF.Class;
using MinDAF.Forms;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol
{
    public partial class BudgetRunningBalance : UserControl
    {
        public event EventHandler LockControls;
        private Double TotalExpenditure = 0.00;
        public String _DivisionID { get; set; }
        public String _FundSource { get; set; }
        public String _Year { get; set; }
        public String _UACS { get; set; }
        public String _UACSINDEX { get; set; }
        public String WorkingYear { get; set; }

        public double BalanceOff { get; set; }
        public double BegBalance { get; set; }

        private List<ChiefData_DivisionFundSource> _ListComboFS = new List<ChiefData_DivisionFundSource>();


        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();

        private List<BugdetBalanceFields> ListBudgetRunningBalance = new List<BugdetBalanceFields>();
        private List<DivisionRafNo> ListRafNo = new List<DivisionRafNo>();
        private List<DivisionAdjustment> ListAdjustments = new List<DivisionAdjustment>();

        private clsBudgetRunning c_run = new clsBudgetRunning();
       
      private  System.Windows.Threading.DispatcherTimer myDispatcherTimer = new System.Windows.Threading.DispatcherTimer();
        public BudgetRunningBalance(Double _TotalExpenditure,String Expenditure,String UACS,String UACSIndex)
        {
            InitializeComponent();
            
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;

            TotalExpenditure = _TotalExpenditure;
            lblCaption.Content = "Allocation and Running Balance For " + Expenditure;
            _UACS = UACS;
            _UACSINDEX = UACSIndex;
            BegBalance = 0.00;
            BalanceOff = 0.00;
        }
        private void FetchDivisionFundSource()
        {

            c_run.Process = "FetchDivisionFundSource";
            svc_mindaf.ExecuteSQLAsync(c_run.FetchDivisionFundSource(_DivisionID, WorkingYear));
        }
        private void FetchBudgetRAFNo()
        {
            String _mftype = "";
            List<ChiefData_DivisionFundSource> _selected = _ListComboFS.Where(x => x.Fund_Name == _FundSource).ToList();
            if (_selected.Count != 0)
            {
                _mftype = _selected[0].FType;
            }
            c_run.Process = "FetchBudgetRAFNo";
            svc_mindaf.ExecuteSQLAsync(c_run.FetchBudgetRafNo(this._DivisionID, WorkingYear, _mftype));
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
              var _results = e.Result.ToString();
              switch (c_run.Process)
              {
                  case "FetchBudgetAdjustments":
                      XDocument oDocKeyResultsFetchBudgetAdjustments = XDocument.Parse(_results);
                      var _dataListsFetchBudgetAdjustments = from info in oDocKeyResultsFetchBudgetAdjustments.Descendants("Table")
                                                             select new DivisionAdjustment
                                                             {
                                                                 Raf = Convert.ToString(info.Element("number").Value),
                                                                 Mooe_Id = Convert.ToString(info.Element("mooe_id").Value),
                                                                 Adjustments = Convert.ToString(info.Element("adjustment").Value),
                                                                 PlusMinus = Convert.ToString(info.Element("plusminus").Value)

                                                             };

                      ListAdjustments.Clear();

                      foreach (var item in _dataListsFetchBudgetAdjustments)
                      {

                          DivisionAdjustment _varProf = new DivisionAdjustment();

                          _varProf.Adjustments = item.Adjustments;
                          _varProf.Mooe_Id = item.Mooe_Id;
                          _varProf.PlusMinus = item.PlusMinus;
                          _varProf.Raf = item.Raf;

                          ListAdjustments.Add(_varProf);

                      }


                      FetchBudgetRunningBalance();



                      this.Cursor = Cursors.Arrow;
                      break;
                  case "FetchBudgetRAFNo":
                      XDocument oDocKeyResultsFetchBudgetRAFNo = XDocument.Parse(_results);
                      var _dataListsFetchBudgetRAFNo = from info in oDocKeyResultsFetchBudgetRAFNo.Descendants("Table")
                                                       select new DivisionRafNo
                                                       {
                                                           Raf = Convert.ToString(info.Element("number").Value)

                                                       };

                      ListRafNo.Clear();

                      foreach (var item in _dataListsFetchBudgetRAFNo)
                      {

                          DivisionRafNo _varProf = new DivisionRafNo();

                          _varProf.Raf = item.Raf;


                          ListRafNo.Add(_varProf);

                      }


                      this.Cursor = Cursors.Arrow;
                      FetchBudgetAdjustments();
   

                      break;

                  case "FetchDivisionFundSource":
                      XDocument oDocFetchDivisionFundSource = XDocument.Parse(_results);
                      var _dataFetchDivisionFundSource = from info in oDocFetchDivisionFundSource.Descendants("Table")
                                                         select new ChiefData_DivisionFundSource
                                                         {
                                                             Fund_Name = Convert.ToString(info.Element("Fund_Name").Value),
                                                             FType = Convert.ToString(info.Element("Fund_Type").Value)
                                                         };



                     
                      _ListComboFS.Clear();
                      foreach (var item in _dataFetchDivisionFundSource)
                      {
                          ChiefData_DivisionFundSource varx = new ChiefData_DivisionFundSource();

                          varx.FType = item.FType;
                          varx.Fund_Name = item.Fund_Name;

                         
                          _ListComboFS.Add(varx);
                      }

                      FetchBudgetRAFNo();
                      this.Cursor = Cursors.Arrow;
                      break;
                  case "FetchBudgetRunningBalance":
                      XDocument oDocKeyResults = XDocument.Parse(_results);
                      var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                       select new BugdetBalanceFields
                                       {
                                           balance = Convert.ToString(info.Element("balance").Value),
                                          name  = Convert.ToString(info.Element("name").Value),
                                          total  = Convert.ToString(info.Element("total").Value),
                                           uacs_code = Convert.ToString(info.Element("uacs_code").Value)

                                       };


                    ListBudgetRunningBalance.Clear();
                    BugdetBalanceFields _BegBal = new BugdetBalanceFields();

                   
                    List<BugdetBalanceFields> _begBalance = _dataLists.Where(item => item.name == "Beginning Balance").ToList();
                    foreach (DivisionRafNo item in ListRafNo)
                    {
                        List<DivisionAdjustment> _filtered = ListAdjustments.Where(x => x.Raf == item.Raf).ToList();

                        foreach (BugdetBalanceFields item_f in _begBalance)
                        {
                            if (_filtered.Count!=0)
                            {
                               
                                foreach (DivisionAdjustment itemd in _filtered)
                                {
                                    double _adjustment = Convert.ToDouble(itemd.Adjustments);
                                    double _allocation = Convert.ToDouble(item_f.total);
                                    double _new_allocation = 0.00;

                                    switch (itemd.PlusMinus)
                                     {
                                         case "+":

                                             _new_allocation = _allocation + _adjustment;
                                             break;

                                         case "-":
                                             _new_allocation = _allocation - _adjustment;
                                             break;

                                     }
                                    item_f.total = _new_allocation.ToString("#,##0.00");
                                    item_f.balance = _new_allocation.ToString("#,##0.00");
                                }
                            }
                        }

                    }
                    double RunningBalance = 0.00;
                    if (_begBalance.Count!=0)
                    {
                        RunningBalance = Convert.ToDouble(_begBalance[0].balance);
                        _BegBal.balance = RunningBalance.ToString("#,##0.00");
                        _BegBal.name = _begBalance[0].uacs_code;
                        _BegBal.total = "";
                        _BegBal.uacs_code = _begBalance[0].uacs_code;

                        ListBudgetRunningBalance.Add(_BegBal);
                    }
                 

                   
                 

              

                    if (_dataLists.Count()>1)
                    {
                        foreach (var item in _dataLists)
                        {
                            if (item.name!="Beginning Balance")
                            {
                                BugdetBalanceFields _temp = new BugdetBalanceFields();


                                RunningBalance -= Convert.ToDouble(item.total);
                                _temp.balance = RunningBalance.ToString("#,##0.00");
                                _temp.name = item.uacs_code;
                                _temp.total = Convert.ToDouble(item.total).ToString("#,##0.00");
                                _temp.uacs_code = item.uacs_code;
                                ListBudgetRunningBalance.Add(_temp);
                                BalanceOff = Convert.ToDouble(_temp.balance);
                                BegBalance = RunningBalance;
                            }
                      


                        }
                    }
                    else
                    {
                        try
                        {
                            BalanceOff = Convert.ToDouble(_begBalance[0].total);
                        }
                        catch (Exception)
                        {
                            BalanceOff = 0.00;
                           
                        }
                        
                        //foreach (var item in _dataLists)
                        //{
                        //         if (Convert.ToDouble(item.total) != 0)
                        //                    {
                        //                        BalanceOff = Convert.ToDouble(item.total);
                        //                    }
                        //                    break;
                        //}
                    
                    }
                  

                    
                     
                      grdData.ItemsSource = null;
                      grdData.ItemsSource = ListBudgetRunningBalance;
              
                      try
                      {
                          grdData.Columns["uacs_code"].Visibility = System.Windows.Visibility.Collapsed;
                          grdData.Columns["name"].HeaderText = "UACS Code";
                      }
                      catch (Exception)
                      {
                       
                         
                      }
                    
                     
                      if (TotalExpenditure > Convert.ToDouble(_BegBal.balance) && Convert.ToDouble(_BegBal.balance)!=0)
                      {
                          frmNotifyBalance fBa = new frmNotifyBalance();

                          fBa.Show();
                          if (LockControls != null)
                          {
                              LockControls(this, new EventArgs());
                          }
                      }
                      else if (BalanceOff <0)
                      {
                          frmNotifyBalance fBa = new frmNotifyBalance();
                           fBa.Show();
                          if (LockControls != null)
                          {
                              LockControls(this, new EventArgs());
                          }
                      }
                      {

                      }
                      this.Cursor = Cursors.Arrow;
                      break;
              }
        }
        private void FetchBudgetAdjustments()
        {
            c_run.Process = "FetchBudgetAdjustments";
            svc_mindaf.ExecuteSQLAsync(c_run.FetchBudgetAdjustments(this._DivisionID, WorkingYear,this._UACS,this._FundSource));
        }
        private void FetchBudgetRunningBalance()
        {
            c_run.Process = "FetchBudgetRunningBalance";
            svc_mindaf.ExecuteSQLAsync(c_run.GetCurrentRunningBalance(_DivisionID, WorkingYear, this._FundSource, _UACS,_UACSINDEX));
        }

        private void frm_bud_run_Loaded(object sender, RoutedEventArgs e)
        {
            FetchDivisionFundSource();
         
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            FetchBudgetRunningBalance();
        }
    }
}
