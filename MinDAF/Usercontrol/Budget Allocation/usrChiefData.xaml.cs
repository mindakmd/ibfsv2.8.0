﻿using Infragistics.Controls.Grids;
using MinDAF.Class;
using MinDAF.Forms;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol.Budget_Allocation
{
    public partial class usrChiefData : UserControl
    {

        public String DivisionId { get; set; }
        private String SelectedYear { get; set; }
        public String OfficeId { get; set; }
        public String PAPCODE { get; set; }
        public String _User { get; set; }
        public String IsUnit { get; set; }

        public String AttachedFundId { get; set; }

        private clsOfficeDashBoard c_office_dash = new clsOfficeDashBoard();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();

        private List<BudgetExpenitureMain> ListBudgetExpense = new List<BudgetExpenitureMain>();
        private List<OfficeDetails> BalanceDetails = new List<OfficeDetails>();
        private List<BudgetBalances> BBDetails = new List<BudgetBalances>();
        private List<ExpenseItemsList> ExpenseItemData = new List<ExpenseItemsList>();

        private List<ChiefData_FundSourceMain> cd_FundSourceMain = new List<ChiefData_FundSourceMain>();
        private List<ChiefData_FundSourceMainDetailsDASH> cd_FundSourceMainDetails = new List<ChiefData_FundSourceMainDetailsDASH>();
        private List<ChiefData_FundSourceDASH> cd_FundSource = new List<ChiefData_FundSourceDASH>();
        private List<ChiefData_ListExpenditure> cd_ExpenditureLists = new List<ChiefData_ListExpenditure>();
        private List<ChiefData_DivisionFundSource> cd_DivisionFundSource = new List<ChiefData_DivisionFundSource>();

        private List<ChiefData_DivisionFundSource> _ListComboFS = new List<ChiefData_DivisionFundSource>();

        private List<DivisionRafNo> ListRafNo = new List<DivisionRafNo>();
        private List<DivisionAdjustment> ListAdjustments = new List<DivisionAdjustment>();
        private clsppmp_realign c_ppmp = new clsppmp_realign();

        public usrChiefData()
        {
            InitializeComponent();

            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            svc_mindaf.ExecuteImportDataSQLCompleted += svc_mindaf_ExecuteImportDataSQLCompleted;
        }
        private List<BudgetRevisionMain> _Main = new List<BudgetRevisionMain>();
        private List<OBR_Details> ListGridDataDetails = new List<OBR_Details>();
        void svc_mindaf_ExecuteImportDataSQLCompleted(object sender, ExecuteImportDataSQLCompletedEventArgs e)
        {
            string _result = e.Result.ToString();

            switch (c_office_dash.Process)
            {

                case "FetchOBRData":
                    XDocument oDocKeyFetchGridData = XDocument.Parse(_result);
                    var _dataListsFetchGridData = from info in oDocKeyFetchGridData.Descendants("Table")
                                                  select new OBR_Details
                                                  {
                                                      _Month = Convert.ToString(info.Element("_Month").Value),
                                                      AccountCode = Convert.ToString(info.Element("AccountCode").Value),
                                                      DateCreated = Convert.ToString(info.Element("DateCreated").Value),
                                                      Division = Convert.ToString(info.Element("Division").Value),
                                                      Total = Convert.ToString(info.Element("Total").Value)

                                                  };

                    ListGridDataDetails.Clear();

                    foreach (var item in _dataListsFetchGridData)
                    {
                        OBR_Details _varDetails = new OBR_Details();

                        switch (item._Month)
                        {
                            case "1": _varDetails._Month = "Jan"; break;
                            case "2": _varDetails._Month = "Feb"; break;
                            case "3": _varDetails._Month = "Mar"; break;
                            case "4": _varDetails._Month = "Apr"; break;
                            case "5": _varDetails._Month = "May"; break;
                            case "6": _varDetails._Month = "Jun"; break;
                            case "7": _varDetails._Month = "Jul"; break;
                            case "8": _varDetails._Month = "Aug"; break;
                            case "9": _varDetails._Month = "Sep"; break;
                            case "10": _varDetails._Month = "Oct"; break;
                            case "11": _varDetails._Month = "Nov"; break;
                            case "12": _varDetails._Month = "Dec"; break;
                        }

                        _varDetails.AccountCode = item.AccountCode;
                        _varDetails.DateCreated = item.DateCreated;
                        _varDetails.Division = item.Division;
                        _varDetails.Total = item.Total;

                        ListGridDataDetails.Add(_varDetails);

                    }
                    List<String> _months = new List<string>();

                    for (int i = 1; i < DateTime.Now.Month; i++)
                    {
                        switch (i.ToString())
                        {
                            case "1":
                                _months.Add("Jan");
                                break;
                            case "2":
                                _months.Add("Feb");
                                break;
                            case "3":
                                _months.Add("Mar");
                                break;
                            case "4":
                                _months.Add("Apr");
                                break;
                            case "5":
                                _months.Add("May");
                                break;
                            case "6":
                                _months.Add("Jun");
                                break;
                            case "7":
                                _months.Add("Jul");
                                break;
                            case "8":
                                _months.Add("Aug");
                                break;
                            case "9":
                                _months.Add("Sep");
                                break;
                            case "10":
                                _months.Add("Oct");
                                break;
                            case "11":
                                _months.Add("Nov");
                                break;
                            case "12":
                                _months.Add("Dec");
                                break;
                        }
                    }

                    foreach (var item in _Main)
                    {
                        if (item.TotalAmount != null)
                        {
                            if (item.TotalAmount.Contains("-----------------"))
                            {
                                break;
                            }
                            double _OBR = 0.00;

                            foreach (string item_months in _months)
                            {
                                double _Total = 0.00;
                                List<OBR_Details> _data = ListGridDataDetails.Where(x => x.AccountCode == item.UACS && x._Month == item_months).ToList();

                                foreach (OBR_Details item_OBR in _data)
                                {
                                    _Total += Convert.ToDouble(item_OBR.Total);

                                    _OBR += Convert.ToDouble(item_OBR.Total);
                                }
                                switch (item_months)
                                {
                                    case "Jan":
                                        if (_Total == 0)
                                        {
                                            item.Jan = "(" + Convert.ToDouble(item.Jan).ToString("#,##0.00") + ")";
                                        }
                                        else
                                        {
                                            item.Jan = _Total.ToString("#,##0.00");
                                        }
                                        break;
                                    case "Feb":
                                        if (_Total == 0)
                                        {
                                            item.Feb = "(" + Convert.ToDouble(item.Feb).ToString("#,##0.00") + ")";
                                        }
                                        else
                                        {
                                            item.Feb = _Total.ToString("#,##0.00");
                                        }
                                        break;
                                    case "Mar":
                                        if (_Total == 0)
                                        {
                                            item.Mar = "(" + Convert.ToDouble(item.Mar).ToString("#,##0.00") + ")";
                                        }
                                        else
                                        {
                                            item.Mar = _Total.ToString("#,##0.00");
                                        }
                                        break;
                                    case "Apr":
                                        if (_Total == 0)
                                        {
                                            item.Apr = "(" + Convert.ToDouble(item.Apr).ToString("#,##0.00") + ")";
                                        }
                                        else
                                        {
                                            item.Apr = _Total.ToString("#,##0.00");
                                        }
                                        break;
                                    case "May":
                                        if (_Total == 0)
                                        {
                                            item.May = "(" + Convert.ToDouble(item.May).ToString("#,##0.00") + ")";
                                        }
                                        else
                                        {
                                            item.May = _Total.ToString("#,##0.00");
                                        }
                                        break;
                                    case "Jun":
                                        if (_Total == 0)
                                        {
                                            item.Jun = "(" + Convert.ToDouble(item.Jun).ToString("#,##0.00") + ")";
                                        }
                                        else
                                        {
                                            item.Jun = _Total.ToString("#,##0.00");
                                        }
                                        break;
                                    case "Jul":
                                        if (_Total == 0)
                                        {
                                            item.Jul = "(" + Convert.ToDouble(item.Jul).ToString("#,##0.00") + ")";
                                        }
                                        else
                                        {
                                            item.Jul = _Total.ToString("#,##0.00");
                                        }
                                        break;

                                    case "Aug":
                                        if (_Total == 0)
                                        {
                                            item.Aug = "(" + Convert.ToDouble(item.Aug).ToString("#,##0.00") + ")";
                                        }
                                        else
                                        {
                                            item.Aug = _Total.ToString("#,##0.00");
                                        }
                                        break;
                                    case "Sep":
                                        if (_Total == 0)
                                        {
                                            item.Sep = "(" + Convert.ToDouble(item.Sep).ToString("#,##0.00") + ")";
                                        }
                                        else
                                        {
                                            item.Sep = _Total.ToString("#,##0.00");
                                        }
                                        break;
                                    case "Oct":
                                        if (_Total == 0)
                                        {
                                            item.Oct = "(" + Convert.ToDouble(item.Oct).ToString("#,##0.00") + ")";
                                        }
                                        else
                                        {
                                            item.Oct = _Total.ToString("#,##0.00");
                                        }
                                        break;
                                    case "Nov":
                                        if (_Total == 0)
                                        {
                                            item.Nov = "(" + Convert.ToDouble(item.Nov).ToString("#,##0.00") + ")";
                                        }
                                        else
                                        {
                                            item.Nov = _Total.ToString("#,##0.00");
                                        }
                                        break;
                                    case "Dec":
                                        if (_Total == 0)
                                        {
                                            item.Dec = "(" + Convert.ToDouble(item.Dec).ToString("#,##0.00") + ")";
                                        }
                                        else
                                        {
                                            item.Dec = _Total.ToString("#,##0.00");
                                        }
                                        break;
                                }


                            }
                            item.TotalOBR = _OBR.ToString("#,##0.00");
                            item.RemainingBalance = (Convert.ToDouble(item.Allocation) - _OBR).ToString("#,##0.00");
                        }
                    }


                    FetchFundSourceMainDetails();


                    break;

                case "FetchBalances":
                    XDocument oDocFetchBalances = XDocument.Parse(_result);
                    var _dataFetchBalances = from info in oDocFetchBalances.Descendants("Table")
                                             select new BudgetBalances
                                             {
                                                 Division = Convert.ToString(info.Element("Division").Value),
                                                 Office = Convert.ToString(info.Element("Office").Value),
                                                 Total = Convert.ToString(info.Element("Total").Value)

                                             };



                    BBDetails.Clear();

                    foreach (var item in _dataFetchBalances)
                    {
                        BudgetBalances _varData = new BudgetBalances();

                        _varData.Division = item.Division;
                        _varData.Office = item.Office;
                        _varData.Total = item.Total;

                        BBDetails.Add(_varData);

                    }
                    FetchBudgetAllocation();



                    this.Cursor = Cursors.Arrow;
                    break;
            }
        }
        List<BudgetExpenditureList> _list = new List<BudgetExpenditureList>();
        List<BudgetExpenditureList> _list_sub = new List<BudgetExpenditureList>();

        private void AssignDetails()
        {
            var results = from p in _list
                          group p by p.pap_code into g
                          select new
                          {
                              PAPCode = g.Key,
                              ExpenseName = g.Select(m => m.mo_name)
                          };

            foreach (var item in results)
            {
                BudgetExpenitureMain x_datas = new BudgetExpenitureMain();

                var x = item.ExpenseName.ToList();

                x_datas.MOOE_NAME = x[0].ToString();

                List<BudgetExpenditureList> _xList = _list_sub.Where(items => items.pap_sub == item.PAPCode).ToList();
                List<BudgetExpenditureDashBoard> _subdata = new List<BudgetExpenditureDashBoard>();

                foreach (var itemList in _xList)
                {
                    BudgetExpenditureDashBoard _xxdata = new BudgetExpenditureDashBoard();

                    _xxdata.Source = itemList.Fund_Source;
                    _xxdata.Expenditure = itemList.Name;
                    _xxdata.UACS = itemList.uacs_code;
                    _xxdata.Total = Convert.ToDouble(itemList.Amount).ToString("#,##0.00");

                    _subdata.Add(_xxdata);
                }
                x_datas.ListExpenditure = _subdata;

                ListBudgetExpense.Add(x_datas);

            }


            grdBudget.ItemsSource = null;
            grdBudget.ItemsSource = ListBudgetExpense;


            foreach (var item in grdBudget.Rows)
            {
                if (item.HasChildren)
                {
                    item.IsExpanded = true;
                }
            }
            try
            {
                grdBudget.Columns["MOOE_NAME"].HeaderText = "Mode of Expenditure";
            }
            catch (Exception)
            {


            }


            FetchFundSourceMain();

        }
        private void FetchOBRData()
        {

            c_office_dash.Process = "FetchOBRData";
            svc_mindaf.ExecuteImportDataSQLAsync(c_office_dash.FetchOBRData(this.SelectedYear, cmbFundSource.SelectedItem.ToString()));
        }
        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            string _result = e.Result.ToString();
            switch (c_office_dash.Process)
            {
                case "FetchBudgetRAFNo":
                    XDocument oDocKeyResultsFetchBudgetRAFNo = XDocument.Parse(_result);
                    var _dataListsFetchBudgetRAFNo = from info in oDocKeyResultsFetchBudgetRAFNo.Descendants("Table")
                                                     select new DivisionRafNo
                                                     {
                                                         Raf = Convert.ToString(info.Element("number").Value)

                                                     };

                    ListRafNo.Clear();

                    foreach (var item in _dataListsFetchBudgetRAFNo)
                    {

                        DivisionRafNo _varProf = new DivisionRafNo();

                        _varProf.Raf = item.Raf;


                        ListRafNo.Add(_varProf);

                    }


                    this.Cursor = Cursors.Arrow;
                    FetchBudgetAdjustments();
                    //  FetchActivityDataMonths();

                    break;
                case "FetchBudgetAdjustments":
                    XDocument oDocKeyResultsFetchBudgetAdjustments = XDocument.Parse(_result);
                    var _dataListsFetchBudgetAdjustments = from info in oDocKeyResultsFetchBudgetAdjustments.Descendants("Table")
                                                           select new DivisionAdjustment
                                                           {
                                                               Raf = Convert.ToString(info.Element("number").Value),
                                                               Mooe_Id = Convert.ToString(info.Element("mooe_id").Value),
                                                               Adjustments = Convert.ToString(info.Element("adjustment").Value),
                                                               PlusMinus = Convert.ToString(info.Element("plusminus").Value)

                                                           };

                    ListAdjustments.Clear();

                    foreach (var item in _dataListsFetchBudgetAdjustments)
                    {

                        DivisionAdjustment _varProf = new DivisionAdjustment();

                        _varProf.Adjustments = item.Adjustments;
                        _varProf.Mooe_Id = item.Mooe_Id;
                        _varProf.PlusMinus = item.PlusMinus;
                        _varProf.Raf = item.Raf;

                        ListAdjustments.Add(_varProf);

                    }






                    this.Cursor = Cursors.Arrow;
                    AssignDetails();

                    break;
                case "FetchBudgetAllocation":

                    XDocument oDocFetchBudgetAllocation = XDocument.Parse(_result);
                    var _dataFetchBudgetAllocation = from info in oDocFetchBudgetAllocation.Descendants("Table")
                                                     select new BudgetExpenditureList
                                                     {
                                                         mo_name = Convert.ToString(info.Element("mo_name").Value),
                                                         Fund_Source = Convert.ToString(info.Element("fund_name").Value),
                                                         Amount = Convert.ToString(info.Element("Amount").Value),
                                                         Name = Convert.ToString(info.Element("Name").Value),
                                                         pap_code = Convert.ToString(info.Element("pap_code").Value),
                                                         pap_sub = Convert.ToString(info.Element("pap_sub").Value),
                                                         uacs_code = Convert.ToString(info.Element("uacs_code").Value),
                                                     };


                    //ListBudgetAllocated.Clear();

                    ListBudgetExpense.Clear();

                    foreach (var item in _dataFetchBudgetAllocation)
                    {
                        BudgetExpenditureList _varData = new BudgetExpenditureList();

                        _varData.Amount = item.Amount;
                        _varData.mo_name = item.mo_name;
                        _varData.Fund_Source = item.Fund_Source;
                        _varData.Name = item.Name;
                        _varData.pap_code = item.pap_code;
                        _varData.pap_sub = item.pap_sub;
                        _varData.uacs_code = item.uacs_code;


                        _list.Add(_varData);
                        _list_sub.Add(_varData);
                    }

                    FetchBudgetRAFNo();
                    break;
                case "FetchDivisionFundSource":
                    XDocument oDocFetchDivisionFundSource = XDocument.Parse(_result);
                    var _dataFetchDivisionFundSource = from info in oDocFetchDivisionFundSource.Descendants("Table")
                                                       select new ChiefData_DivisionFundSource
                                                       {
                                                           Fund_Name = Convert.ToString(info.Element("Fund_Name").Value),
                                                           FType = Convert.ToString(info.Element("Fund_Type").Value)
                                                       };



                    cmbFundSource.Items.Clear();
                    _ListComboFS.Clear();
                    foreach (var item in _dataFetchDivisionFundSource)
                    {
                        ChiefData_DivisionFundSource varx = new ChiefData_DivisionFundSource();

                        varx.FType = item.FType;
                        varx.Fund_Name = item.Fund_Name;

                        cmbFundSource.Items.Add(item.Fund_Name);
                        _ListComboFS.Add(varx);
                    }
                    cmbFundSource.SelectedIndex = 0;
                    if (_ListComboFS.Count != 0)
                    {
                        FetchFundSourceCode();
                    }
                    else
                    {
                        grdBudget.ItemsSource = null;
                        grdDivisionData.ItemsSource = null;
                    }

                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchOfficeDetails":
                    XDocument oDocFetchOfficeDetails = XDocument.Parse(_result);
                    var _dataFetchOfficeDetails = from info in oDocFetchOfficeDetails.Descendants("Table")
                                                  select new OfficeDetails
                                                  {
                                                      fund_source_id = Convert.ToString(info.Element("fund_source_id").Value),
                                                      budget_allocation = Convert.ToString(info.Element("budget_allocation").Value),
                                                      division_id = Convert.ToString(info.Element("div_id").Value),
                                                      Fund_Source = Convert.ToString(info.Element("Fund_Name").Value),
                                                      balance = "0.00",
                                                      total_program_budget = "0.00",
                                                      pap_code = ""
                                                  };



                    BalanceDetails.Clear();

                    foreach (var item in _dataFetchOfficeDetails)
                    {
                        OfficeDetails _varData = new OfficeDetails();

                        _varData.budget_allocation = item.budget_allocation;
                        _varData.division_id = item.division_id;
                        _varData.Fund_Source = item.Fund_Source;
                        _varData.total_program_budget = item.total_program_budget;
                        _varData.pap_code = item.pap_code;
                        _varData.fund_source_id = item.fund_source_id;
                        BalanceDetails.Add(_varData);

                    }
                    //  FetchOfficeTotals();
                    FetchDivisionFundSource();
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchFundSourceCode":
                    XDocument oDocFetchFundSource = XDocument.Parse(_result);
                    var _dataFetchFundSource = from info in oDocFetchFundSource.Descendants("Table")
                                               select new ChiefData_FetchDivisionFundSourceCode
                                               {
                                                   Code = Convert.ToString(info.Element("code").Value),

                                               };





                    foreach (var item in _dataFetchFundSource)
                    {
                        this.FundSource = item.Code;
                        break;
                    }
                    FetchBudgetAllocation();
                   // FetchBalances();
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchExpenditureDetails":
                    XDocument oDocFetchExpenditureDetails = XDocument.Parse(_result);
                    var _dataFetchExpenditureDetails = from info in oDocFetchExpenditureDetails.Descendants("Table")
                                                       select new ChiefData_ListExpenditure
                                                       {
                                                           expenditure = Convert.ToString(info.Element("expenditure").Value),
                                                           fund_source_id = Convert.ToString(info.Element("fund_source_id").Value),
                                                           total = Convert.ToString(info.Element("total").Value)

                                                       };



                    cd_ExpenditureLists.Clear();

                    foreach (var item in _dataFetchExpenditureDetails)
                    {
                        ChiefData_ListExpenditure _varData = new ChiefData_ListExpenditure();

                        _varData.expenditure = item.expenditure;
                        _varData.fund_source_id = item.fund_source_id;
                        _varData.total = item.total;

                        cd_ExpenditureLists.Add(_varData);

                    }
                    FetchFundSourceMainDetails();
                   // FetchOBRData();

                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchFundMain":
                    XDocument oDocFetchFundMain = XDocument.Parse(_result);
                    var _dataFetchFundMain = from info in oDocFetchFundMain.Descendants("Table")
                                             select new ChiefData_FundSourceMain
                                             {
                                                 id = Convert.ToString(info.Element("id").Value),
                                                 fund_source = Convert.ToString(info.Element("fund_name").Value)
                                             };



                    cd_FundSourceMain.Clear();

                    foreach (var item in _dataFetchFundMain)
                    {
                        ChiefData_FundSourceMain _varData = new ChiefData_FundSourceMain();

                        _varData.id = item.id;
                        _varData.fund_source = item.fund_source;

                        cd_FundSourceMain.Add(_varData);
                    }
                    FetchExpenditureDetails();
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchFundSourceMainDetails":
                    XDocument oDocFetchFundSourceMainDetails = XDocument.Parse(_result);
                    var _dataFetchFundSourceMainDetails = from info in oDocFetchFundSourceMainDetails.Descendants("Table")
                                                          select new ChiefData_FundSourceMainDetailsDASH
                                                          {
                                                              allocation = Convert.ToString(info.Element("Allocation").Value),
                                                              fund_name = Convert.ToString(info.Element("Fund_Name").Value),
                                                              id = Convert.ToString(info.Element("id").Value),
                                                              name = Convert.ToString(info.Element("Name").Value),
                                                              mooe_id = Convert.ToString(info.Element("Mooe_Id").Value)
                                                          };



                    cd_FundSourceMainDetails.Clear();

                    foreach (var item in _dataFetchFundSourceMainDetails)
                    {
                        ChiefData_FundSourceMainDetailsDASH _varData = new ChiefData_FundSourceMainDetailsDASH();

                        _varData.allocation = Convert.ToDouble(item.allocation).ToString("#,##0.00");
                        _varData.fund_name = item.fund_name;
                        _varData.id = item.id;
                        _varData.name = item.name;
                        _varData.mooe_id = item.mooe_id;

                        cd_FundSourceMainDetails.Add(_varData);
                    }

                    foreach (DivisionRafNo item in ListRafNo)
                    {
                        List<DivisionAdjustment> _filtered = ListAdjustments.Where(x => x.Raf == item.Raf).ToList();

                        foreach (ChiefData_FundSourceMainDetailsDASH item_f in cd_FundSourceMainDetails)
                        {
                            List<DivisionAdjustment> _selection = _filtered.Where(x => x.Mooe_Id == item_f.mooe_id).ToList();
                            if (_selection.Count != 0)
                            {
                                foreach (DivisionAdjustment item_s in _selection)
                                {
                                    double _adjustment = Convert.ToDouble(item_s.Adjustments);
                                    double _allocation = Convert.ToDouble(item_f.allocation);
                                    double _new_allocation = 0.00;

                                    switch (item_s.PlusMinus)
                                    {
                                        case "+":

                                            _new_allocation = _allocation + _adjustment;
                                            break;

                                        case "-":
                                            _new_allocation = _allocation - _adjustment;
                                            break;

                                    }

                                    item_f.allocation = _new_allocation.ToString("#,##0.00");
                                }
                            }
                        }

                    }





                    cd_FundSource.Clear();



                    foreach (var item in cd_FundSourceMain)
                    {
                        //9-14-2017 List<ChiefData_FundSourceMainDetailsDASH> x_details = _dataFetchFundSourceMainDetails.Where(x => x.id == item.id).ToList();
                        List<ChiefData_FundSourceMainDetailsDASH> x_details = cd_FundSourceMainDetails.Where(x => x.id == item.id).ToList();

                        ChiefData_FundSourceDASH x_data = new ChiefData_FundSourceDASH();

                        x_data.FundName = item.fund_source;

                        foreach (var item_data in x_details)
                        {
                            List<ChiefData_ListExpenditure> x_exp = cd_ExpenditureLists.Where(x => x.fund_source_id == item_data.id && x.expenditure == item_data.name).ToList();
                            if (x_exp.Count != 0)
                            {
                                double _total_exp = 0.00;

                                foreach (var item_r in x_exp)
                                {
                                    _total_exp += Convert.ToDouble(item_r.total);
                                }

                                item_data.total_expenditures = _total_exp.ToString("#,##0.00");
                                item_data.total_balance = (Convert.ToDouble(item_data.allocation.ToString()) - _total_exp).ToString("#,##0.00");

                            }
                            item_data.allocation = Convert.ToDouble(item_data.allocation).ToString("#,##0.00");

                            List<OBR_Details> _Actual = ListGridDataDetails.Where(c => c.AccountCode == item_data.mooe_id).ToList();

                            if (_Actual.Count != 0)
                            {
                                double _totalActual = 0.00;

                                foreach (OBR_Details itemc in _Actual)
                                {
                                    _totalActual += Convert.ToDouble(itemc.Total);
                                }
                                item_data.total_obligated = _totalActual.ToString("#,##0.00");
                                item_data.actual_balance = (Convert.ToDouble(item_data.allocation.ToString()) - _totalActual).ToString("#,##0.00");

                            }
                            else
                            {
                                item_data.total_obligated = 0.00.ToString("#,##0.00");
                                item_data.actual_balance = (Convert.ToDouble(item_data.allocation.ToString()) - 0.00).ToString("#,##0.00");

                            }

                        }

                        //List<BudgetBalances> _BudgetBalance = BBDetails.Where(x_bal => x_bal.Division == item.fund_source).ToList();
                        //foreach (var item_b in x_details)
                        //{
                        //    item_b.
                        //}
                        x_data.FundAllocation = x_details;

                        cd_FundSource.Add(x_data);

                    }


                    grdDivisionData.ItemsSource = null;
                    grdDivisionData.ItemsSource = cd_FundSource;
                    try
                    {
                        grdDivisionData.Columns[""].HeaderText = "Name";
                        grdDivisionData.Columns[""].HeaderText = "NEP Allocation";
                        grdDivisionData.Columns[""].HeaderText = "Expenditures";
                        grdDivisionData.Columns[""].HeaderText = "Remaining Balance";

                    }
                    catch (Exception)
                    {

                    }
                    foreach (var item in grdDivisionData.Rows)
                    {
                        if (item.HasChildren)
                        {
                            item.IsExpanded = true;
                            foreach (var item_c in item.ChildBands)
                            {
                                item_c.Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
                                item_c.Columns["fund_name"].Visibility = System.Windows.Visibility.Collapsed;
                                item_c.Columns["total_obligated"].Visibility = System.Windows.Visibility.Collapsed;
                                item_c.Columns["actual_balance"].Visibility = System.Windows.Visibility.Collapsed;

                                item_c.Columns["name"].HeaderText = "Name";
                                item_c.Columns["allocation"].HeaderText = "NEP Allocation";
                                item_c.Columns["total_expenditures"].HeaderText = "Budget Plan";
                                item_c.Columns["total_balance"].HeaderText = "Projected Balance";
                                item_c.Columns["total_obligated"].HeaderText = "Actual OBR";
                                item_c.Columns["actual_balance"].HeaderText = "Remaining Balance";
                            }
                        }
                    }

                    //   FetchFundSourceMain();
                    firstLoad = false;
                    this.Cursor = Cursors.Arrow;
                    break;

            }

        }
        private void FetchBudgetAdjustments()
        {
            c_office_dash.Process = "FetchBudgetAdjustments";
            svc_mindaf.ExecuteSQLAsync(c_ppmp.FetchBudgetAdjustments(this.DivisionId, cmbYear.SelectedItem.ToString(), this.FundSource));
        }
        private void FetchBudgetRAFNo()
        {
            String _mftype = "";
            List<ChiefData_DivisionFundSource> _selected = _ListComboFS.Where(x => x.Fund_Name == cmbFundSource.SelectedItem.ToString()).ToList();
            if (_selected.Count != 0)
            {
                _mftype = _selected[0].FType;
            }
            c_office_dash.Process = "FetchBudgetRAFNo";
            svc_mindaf.ExecuteSQLAsync(c_ppmp.FetchBudgetRafNo(this.DivisionId, cmbYear.SelectedItem.ToString(), _mftype));
        }
        private void FetchFundSourceMainDetails()
        {
            c_office_dash.Process = "FetchFundSourceMainDetails";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchFundSourceDetails(DivisionId, cmbYear.SelectedItem.ToString(), FundSource));
        }
        private void FetchExpenditureDetails()
        {
            c_office_dash.Process = "FetchExpenditureDetails";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchDivisionExpenditureDetails(this._User, cmbYear.SelectedItem.ToString(), this.FundSource));
        }
        private void FetchFundSourceMain()
        {
            c_office_dash.Process = "FetchFundMain";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchFundSource(DivisionId, cmbYear.SelectedItem.ToString(), this.FundSource));
        }
        private void FetchFundSource()
        {
            c_office_dash.Process = "FetchFundSource";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchOfficeTotals(OfficeId, cmbYear.SelectedItem.ToString()));
        }
        private void FetchOfficeTotals()
        {
            c_office_dash.Process = "FetchOfficeTotals";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchOfficeTotals(OfficeId, cmbYear.SelectedItem.ToString()));
        }
        private void FetchBalances()
        {
            c_office_dash.Process = "FetchBalances";
            svc_mindaf.ExecuteImportDataSQLAsync(c_office_dash.FetchBalancesDivision(PAPCODE, cmbYear.SelectedItem.ToString()));
        }
        private void FetchOfficeDetails()
        {
            c_office_dash.Process = "FetchOfficeDetails";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchOfficeDetails(OfficeId, cmbYear.SelectedItem.ToString()));
        }
        private void FetchBudgetAllocation()
        {
            SelectedYear = cmbYear.SelectedItem.ToString();
            c_office_dash.Process = "FetchBudgetAllocation";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchBudgetAllocation(DivisionId, SelectedYear));
        }
        private void FetchDivisionFundSource()
        {

            c_office_dash.Process = "FetchDivisionFundSource";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchDivisionFundSource(DivisionId, SelectedYear));
        }
        private void GenerateYear()
        {
            int _year = DateTime.Now.Year;
            int _limit = _year + 20;
            _year -= 1;
            for (int i = _year; i != _limit; i++)
            {
                cmbYear.Items.Add(_year);
                _year += 1;
            }
            for (int i = 0; i < cmbYear.Items.Count; i++)
            {
                if (cmbYear.Items[i].ToString() == DateTime.Now.Year.ToString())
                {
                    cmbYear.SelectedIndex = 1;
                    break;
                }
            }

            this.SelectedYear = cmbYear.SelectedItem.ToString();
        }
        public bool firstLoad = false;

        private void ctrlDashChief_Loaded(object sender, RoutedEventArgs e)
        {//HOME

            if (firstLoad == false)
            {
                GenerateYear();
                FetchOfficeDetails();
                firstLoad = true;
            }
            else
            {
                firstLoad = false;
            }
        }

        private void btnOBR_Click(object sender, RoutedEventArgs e)
        {
            frmBudgetOBR b_obr = new frmBudgetOBR();
            b_obr._FundSource = cmbFundSource.SelectedItem.ToString();
            b_obr._Year = cmbYear.SelectedItem.ToString();
            b_obr._DivisionId = this.DivisionId;
            b_obr.Show();
        }

        private void cmbYear_DropDownClosed(object sender, EventArgs e)
        {
            SelectedYear = cmbYear.SelectedItem.ToString();
            //if (firstLoad == false)
            //{
            //    GenerateYear();
            FetchOfficeDetails();
            //    firstLoad = true;
            //}
            //else
            //{
            //    firstLoad = false;
            //}
        }
        private String FundSource { get; set; }
        private void FetchFundSourceCode()
        {
            try
            {
                c_office_dash.Process = "FetchFundSourceCode";
                svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchDivisionFundSourceCode(this.DivisionId, cmbYear.SelectedItem.ToString(), cmbFundSource.SelectedItem.ToString()));
            }
            catch (Exception)
            {

            }

        }
        private void cmbFundSource_DropDownClosed(object sender, EventArgs e)
        {
            if (cmbFundSource.Items.Count != 0)
            {
                FetchFundSourceCode();
            }
            else
            {
                MessageBox.Show("No Current Fund Source");
            }

        }

        private void btnActualBalance_Click(object sender, RoutedEventArgs e)
        {
            switch (btnActualBalance.Content.ToString())
            {
                case "View Actual Balance":
                    foreach (var item in grdDivisionData.Rows)
                    {
                        if (item.HasChildren)
                        {
                            item.IsExpanded = true;
                            foreach (var item_c in item.ChildBands)
                            {
                                item_c.Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
                                item_c.Columns["fund_name"].Visibility = System.Windows.Visibility.Collapsed;
                                item_c.Columns["total_obligated"].Visibility = System.Windows.Visibility.Visible;
                                item_c.Columns["actual_balance"].Visibility = System.Windows.Visibility.Visible;

                                item_c.Columns["total_expenditures"].Visibility = System.Windows.Visibility.Collapsed;
                                item_c.Columns["total_balance"].Visibility = System.Windows.Visibility.Collapsed;

                                item_c.Columns["name"].HeaderText = "Name";
                                item_c.Columns["allocation"].HeaderText = "NEP Allocation";
                                item_c.Columns["total_expenditures"].HeaderText = "Budget Plan";
                                item_c.Columns["total_balance"].HeaderText = "Projected Balance";
                                item_c.Columns["total_obligated"].HeaderText = "Actual OBR";
                                item_c.Columns["actual_balance"].HeaderText = "Remaining Balance";
                            }
                        }
                    }

                    btnActualBalance.Content = "View Budget Balance";
                    break;
                case "View Budget Balance":
                    foreach (var item in grdDivisionData.Rows)
                    {
                        if (item.HasChildren)
                        {
                            item.IsExpanded = true;
                            foreach (var item_c in item.ChildBands)
                            {
                                item_c.Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
                                item_c.Columns["fund_name"].Visibility = System.Windows.Visibility.Collapsed;
                                item_c.Columns["total_obligated"].Visibility = System.Windows.Visibility.Collapsed;
                                item_c.Columns["actual_balance"].Visibility = System.Windows.Visibility.Collapsed;

                                item_c.Columns["total_expenditures"].Visibility = System.Windows.Visibility.Visible;
                                item_c.Columns["total_balance"].Visibility = System.Windows.Visibility.Visible;

                                item_c.Columns["name"].HeaderText = "Name";
                                item_c.Columns["allocation"].HeaderText = "NEP Allocation";
                                item_c.Columns["total_expenditures"].HeaderText = "Budget Plan";
                                item_c.Columns["total_balance"].HeaderText = "Projected Balance";
                                item_c.Columns["total_obligated"].HeaderText = "Actual OBR";
                                item_c.Columns["actual_balance"].HeaderText = "Remaining Balance";
                            }
                        }
                    }

                    btnActualBalance.Content = "View Actual Balance";
                    break;
                default:
                    break;
            }
        }
    }
}
