﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol.Budget_Allocation
{
    public partial class ctrlPAPNewSetup : UserControl
    {
        public event EventHandler RefreshData;
        public String TypeDepartment { get; set; }
        public String DBM_Sub_Id { get; set; }
        public String PAP { get; set; }
        public String WorkingYear { get; set; }
        public String DivisionId { get; set; }
        public List<String> Excempted { get; set; }
        private clsOfficeDashBoard c_office_dash = new clsOfficeDashBoard();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private List<DivisionAssign> _Data = new List<DivisionAssign>();

        private List<DivisionData> _DivList = new List<DivisionData>();
        private List<OfficePaps> _SubPap = new List<OfficePaps>();

        public ctrlPAPNewSetup()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            this.Loaded += ctrlPAPNewSetup_Loaded;
        }

        void ctrlPAPNewSetup_Loaded(object sender, RoutedEventArgs e)
        {
            switch (this.TypeDepartment)
            {
                case "Assign":case "Division":
                    FetchDivisionData();
                    break;
                case "Unit":
                    FetchDivisionUnits();
                    break;
                case "SubPAP":
                    FetchSubPAP();
                    break;
            }
        }

        private void FetchDivisionData()
        {
            c_office_dash.Process = "FetchDivisionData";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchDivisionAsign(this.WorkingYear));
        }
        private void FetchSubPAP()
        {
            c_office_dash.Process = "FetchSubPAP";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchOfficePAPS(this.WorkingYear));
        }
        private void FetchDivisionUnits()
        {
            c_office_dash.Process = "FetchDivisionUnits";
            svc_mindaf.ExecuteSQLAsync(c_office_dash.FetchDivisionUnits(this.WorkingYear));
        }
        private List<OfficePaps> _DataUnits = new List<OfficePaps>();
        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            string _result = e.Result.ToString();

            switch (c_office_dash.Process)
            {
                case "FetchDivisionData":
                    XDocument oDocFetchBalances = XDocument.Parse(_result);
                    var _dataFetchBalances = from info in oDocFetchBalances.Descendants("Table")
                                             select new DivisionAssign
                                             {
                                                 Division_Id = Convert.ToString(info.Element("Division_Id").Value),
                                                 Division_Desc = Convert.ToString(info.Element("Division_Desc").Value),
                                                 DivisionAccro = Convert.ToString(info.Element("DivisionAccro").Value)

                                             };



                    _Data.Clear();

                    foreach (var item in _dataFetchBalances)
                    {
                        
                            DivisionAssign _varData = new DivisionAssign();

                            _varData.Division_Id = item.Division_Id;
                            _varData.Division_Desc = item.Division_Desc;
                            _varData.DivisionAccro = item.DivisionAccro;

                            _Data.Add(_varData);
                   

                    }
                    grdPaps.ItemsSource = null;
                    grdPaps.ItemsSource = _Data;
                   


                    grdPaps.Columns["Division_Id"].Visibility = System.Windows.Visibility.Collapsed;
                  //  grdPaps.Columns["Division_Desc"].Visibility = System.Windows.Visibility.Collapsed;
                    grdPaps.Columns["DivisionAccro"].Visibility = System.Windows.Visibility.Collapsed;

                   
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchDivisionUnits":
                    XDocument oDocFetchDivisionUnits = XDocument.Parse(_result);
                    var _dataFetchDivisionUnits = from info in oDocFetchDivisionUnits.Descendants("Table")
                                             select new DivisionAssign
                                             {
                                                 Division_Id = Convert.ToString(info.Element("Division_Id").Value),
                                                 Division_Desc = Convert.ToString(info.Element("Division_Desc").Value),
                                                 DivisionAccro = Convert.ToString(info.Element("DivisionAccro").Value)

                                             };



                    _Data.Clear();

                    foreach (var item in _dataFetchDivisionUnits)
                    {
                        DivisionAssign _varData = new DivisionAssign();

                        _varData.Division_Id = item.Division_Id;
                        _varData.Division_Desc = item.Division_Desc;
                        _varData.DivisionAccro = item.DivisionAccro;

                        _Data.Add(_varData);

                    }
                    grdPaps.ItemsSource = null;
                    grdPaps.ItemsSource = _Data;
                   


                    grdPaps.Columns["Division_Id"].Visibility = System.Windows.Visibility.Collapsed;
                  //  grdPaps.Columns["Division_Desc"].Visibility = System.Windows.Visibility.Collapsed;
                    grdPaps.Columns["DivisionAccro"].Visibility = System.Windows.Visibility.Collapsed;

                   
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchSubPAP":
                    XDocument oDocFetchSubPAP = XDocument.Parse(_result);
                    var _dataFetchSubPAP = from info in oDocFetchSubPAP.Descendants("Table")
                                                  select new OfficePaps
                                                  {
                                                      //Id = Convert.ToString(info.Element("Division_Id").Value),
                                                      //Code = Convert.ToString(info.Element("Division_Desc").Value),
                                                      //Office = Convert.ToString(info.Element("DivisionAccro").Value)
                                                      Id = Convert.ToString(info.Element("Id").Value),
                                                      Code = Convert.ToString(info.Element("Code").Value),
                                                      Office = Convert.ToString(info.Element("Office").Value)
                                                  };



                    _SubPap.Clear();

                    foreach (var item in _dataFetchSubPAP)
                    {
                        OfficePaps _varData = new OfficePaps();

                        _varData.Id = item.Id;
                        _varData.Code = item.Code;
                        _varData.Office = item.Office;

                        _SubPap.Add(_varData);

                    }
                    grdPaps.ItemsSource = null;
                    grdPaps.ItemsSource = _SubPap;



                    grdPaps.Columns["Id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdPaps.Columns["Code"].Visibility = System.Windows.Visibility.Collapsed;
                  //  grdPaps.Columns["Office"].Visibility = System.Windows.Visibility.Collapsed;


                    this.Cursor = Cursors.Arrow;
                    break;
            }
        }
        private void AddNewData() 
        {
            switch (this.TypeDepartment)
            {
                case "SubPapNew":case "SubPap":

                    //DivisionAssign _selected_c = (DivisionAssign)grdPaps.ActiveItem;
                    DivisionData _new_c = new DivisionData();
                    //+ _data.Division_Code + ",");
                    //sb.AppendLine(@"  '" + _data.Division_Desc + "',");
                    //sb.AppendLine(@"  '" + _data.DBM_SUP_PAP_ID + "',");
                    //sb.AppendLine(@"  '" + _data.Year_Setup 
                    _new_c.DBM_SUP_PAP_ID = this.DBM_Sub_Id;
                    _new_c.Division_Code =this.PAP;
                    _new_c.Division_Desc = txtDivision.Text;
                    _new_c.Division_Accro = txtAccro.Text;
                    _new_c.Is_Sub_Division = "0";
                    _new_c.Year_Setup = this.WorkingYear;


                    c_office_dash.Process = "SubPapNew";
                    c_office_dash.SQLOperation += c_office_dash_SQLOperation;
                    c_office_dash.AddDivisionCode("SubPapNew", _new_c);

                    break;
                case "AddNew":

                    DivisionAssign _selected_a = (DivisionAssign)grdPaps.ActiveItem;
                    DivisionData _new_a = new DivisionData();

                    _new_a.DBM_SUP_PAP_ID = this.DBM_Sub_Id;
                    _new_a.Division_Code = txtPAPCode.Text;
                    _new_a.Division_Desc = txtDivision.Text;
                    _new_a.Division_Accro = txtAccro.Text;
                    _new_a.Is_Sub_Division = "0";
                    _new_a.Year_Setup = this.WorkingYear;


                    c_office_dash.Process = "AddNew";
                    c_office_dash.SQLOperation += c_office_dash_SQLOperation;
                
                    c_office_dash.AddDivisionCode("AddNew", _new_a);

                    break;
                case "Assign": case "Division":
                    
                    DivisionAssign _selected = (DivisionAssign)grdPaps.ActiveItem;
                    DivisionData _new = new DivisionData();

                    _new.DBM_SUP_PAP_ID = this.DBM_Sub_Id;
                    _new.Division_Code = this.PAP;
                    _new.Division_Desc = _selected.Division_Desc;
                    _new.Division_Accro = _selected.DivisionAccro;
                    _new.Is_Sub_Division = "0";
                    _new.Year_Setup = this.WorkingYear;
                    _new.Division_Id = _selected.Division_Id;

                    c_office_dash.Process = "AssignNew";
                    c_office_dash.SQLOperation+=c_office_dash_SQLOperation;
                    c_office_dash.AddDivisionCode("Assign", _new);
           
                    break;
                case "Unit":

                    DivisionAssign _selected_d = (DivisionAssign)grdPaps.ActiveItem;
                    DivisionData _new_d = new DivisionData();

                    _new_d.DBM_SUP_PAP_ID = this.DBM_Sub_Id;
                    _new_d.Division_Code = this.PAP;
                    _new_d.Division_Desc = _selected_d.Division_Desc;
                    _new_d.Division_Accro = _selected_d.DivisionAccro;
                    _new_d.Is_Sub_Division = "1";
                    _new_d.Year_Setup = this.WorkingYear;
                    _new_d.Unit_Code = DivisionId;
                    _new_d.Division_Id = _selected_d.Division_Id;

                    c_office_dash.Process = "AssignNew";
                    c_office_dash.SQLOperation += c_office_dash_SQLOperation;
                    c_office_dash.AddDivisionCode("Assign", _new_d);

                    break;
            }

      
            
        }

        private Boolean isDone = false;
        void c_office_dash_SQLOperation(object sender, EventArgs e)
        {
            switch (c_office_dash.Process)
            {
                case "AssignNew":
                    if (this.RefreshData != null)
                    {
                        this.RefreshData(this, new EventArgs());
                    }
                    break;
                case "AddNew":
                    FetchDivisionData();
                    btnAdd.Content = "Assign";
                    btnAddNew.Content = "Add New";
                    grdAddDivision.Visibility = System.Windows.Visibility.Collapsed;
                    txtAccro.Text = "";
                    txtDivision.Text = "";
                    txtPAPCode.Text = "";
                    break;
                case "SubPapNew":
                    FetchSubPAP();
                    btnAdd.Content = "Assign";
                    btnAddNew.Content = "Add New";
                    grdAddDivision.Visibility = System.Windows.Visibility.Collapsed;
                    txtAccro.Text = "";
                    txtDivision.Text = "";
                    txtPAPCode.Text = "";
                    break;
                default:
                    break;
            }
        }


        //private void btnTransfer_Click(object sender, RoutedEventArgs e)
        //{
        //    var x_result = MessageBox.Show("Confirm transfer.", "Transfer Confirmation", MessageBoxButton.OKCancel);
        //    if (x_result.ToString() == "OK")
        //    {
        //        switch (this.TypeDepartment)
        //        {
        //            case "Office":
        //                UpdateOffice();
        //                break;
        //            case "Unit":
        //                UpdateDivision();
        //                break;
        //            default:
        //                break;
        //        }
        //    }
        //}

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            if (true)
            {
                
            }

            switch (btnAdd.Content.ToString())
            {
                case "Assign":
                    AddNewData();
                    break;

                case "Cancel":
                    btnAdd.Content = "Assign";
                    btnAddNew.Content = "Add New";
                    grdAddDivision.Visibility = System.Windows.Visibility.Collapsed;
                    break;
            }
         
        }
        private void SaveNewDivision() 
        {
            this.TypeDepartment = "AddNew";
            AddNewData();
            this.TypeDepartment = "Division";
        }
        private void SaveNewSubPAP()
        {
            this.TypeDepartment = "SubPapNew";
            AddNewData();
        }
        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {
            switch (btnAddNew.Content.ToString())
            {
                case "Add New":
                    btnAddNew.Content = "Save";
                    btnAdd.Content = "Cancel";
                    grdAddDivision.Visibility = System.Windows.Visibility.Visible;
                    break;
                case "Save":
                    switch (this.TypeDepartment)
	                {
                        case "SubPAP":
                            SaveNewSubPAP();
                            break;
                        case "Assign":case "Division":
                            SaveNewDivision();
                            break;
	                }
                    
                    break;
            }
        }
    }
}
