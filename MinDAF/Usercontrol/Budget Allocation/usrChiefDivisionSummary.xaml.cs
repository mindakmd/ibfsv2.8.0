﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol.Budget_Allocation
{
    public partial class usrChiefDivisionSummary : UserControl

    {
        public String DivisionID { get; set; }
        public String Years { get; set; }

        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsFinanceApproval c_finance = new clsFinanceApproval();

        private List<ChiefData_FundSourceMain> cd_FundSourceMain = new List<ChiefData_FundSourceMain>();
        private List<ChiefData_FundSourceMainDetails> cd_FundSourceMainDetails = new List<ChiefData_FundSourceMainDetails>();
        private List<ChiefData_FundSource> cd_FundSource = new List<ChiefData_FundSource>();
        private List<ChiefData_ListExpenditure> cd_ExpenditureLists = new List<ChiefData_ListExpenditure>();
        private List<ChiefData_ItemLists> ListItemDetails = new List<ChiefData_ItemLists>();
        Boolean state;
        public usrChiefDivisionSummary()
        {
            InitializeComponent();
            

            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            String _results = e.Result.ToString();
            switch (c_finance.Process)
            {
                case "FetchExpenditureDetailsItemLists":
                    XDocument oDocFetchFetchExpenditureDetailsItemLists = XDocument.Parse(_results);
                    var _dataFetchFetchExpenditureDetailsItemLists = from info in oDocFetchFetchExpenditureDetailsItemLists.Descendants("Table")
                                                                     select new ChiefData_ItemLists
                                                                     {
                                                                         id = Convert.ToString(info.Element("id").Value),
                                                                         expenditure = Convert.ToString(info.Element("expenditure").Value),
                                                                         months = Convert.ToString(info.Element("month").Value),
                                                                         total = Convert.ToDouble(info.Element("total").Value)
                                                                     };



                    ListItemDetails.Clear();

                    foreach (var item in _dataFetchFetchExpenditureDetailsItemLists)
                    {
                        ChiefData_ItemLists _varData = new ChiefData_ItemLists();

                        _varData.id = item.id;
                        _varData.expenditure = item.expenditure;
                        _varData.months = item.months;
                        _varData.total = item.total;

                        ListItemDetails.Add(_varData);
                    }
                    FetchFundSourceMain();
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchFundMain":
                    XDocument oDocFetchFundMain = XDocument.Parse(_results);
                    var _dataFetchFundMain = from info in oDocFetchFundMain.Descendants("Table")
                                             select new ChiefData_FundSourceMain
                                             {
                                                 id = Convert.ToString(info.Element("id").Value),
                                                 fund_source = Convert.ToString(info.Element("fund_name").Value)
                                             };



                    cd_FundSourceMain.Clear();

                    foreach (var item in _dataFetchFundMain)
                    {
                        ChiefData_FundSourceMain _varData = new ChiefData_FundSourceMain();

                        _varData.id = item.id;
                        _varData.fund_source = item.fund_source;

                        cd_FundSourceMain.Add(_varData);
                    }
                    FetchExpenditureDetails();
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchFundSourceMainDetails":
                    XDocument oDocFetchFundSourceMainDetails = XDocument.Parse(_results);
                    var _dataFetchFundSourceMainDetails = from info in oDocFetchFundSourceMainDetails.Descendants("Table")
                                                          select new ChiefData_FundSourceMainDetails
                                                          {
                                                              allocation = Convert.ToString(info.Element("Allocation").Value),
                                                              fund_name = Convert.ToString(info.Element("Fund_Name").Value),
                                                              id = Convert.ToString(info.Element("id").Value),
                                                              name = Convert.ToString(info.Element("Name").Value)
                                                          };



                    cd_FundSourceMainDetails.Clear();

                    foreach (var item in _dataFetchFundSourceMainDetails)
                    {
                        ChiefData_FundSourceMainDetails _varData = new ChiefData_FundSourceMainDetails();

                        _varData.allocation = Convert.ToDouble(item.allocation).ToString("#,##0.00");
                        _varData.fund_name = item.fund_name;
                        _varData.id = item.id;
                        _varData.name = item.name;

                        cd_FundSourceMainDetails.Add(_varData);
                    }


                    cd_FundSource.Clear();

                    foreach (var item in cd_FundSourceMain)
                    {
                        List<ChiefData_FundSourceMainDetails> x_details = _dataFetchFundSourceMainDetails.Where(x => x.id == item.id).ToList();
                        ChiefData_FundSource x_data = new ChiefData_FundSource();

                        x_data.FundName = item.fund_source;

                        foreach (var item_data in x_details)
                        {
                            List<ChiefData_ListExpenditure> x_exp = cd_ExpenditureLists.Where(x => x.fund_source_id == item_data.id && x.expenditure == item_data.name).ToList();
                            List<ChiefData_ItemLists> _lists = ListItemDetails.Where(x => x.expenditure == item_data.name).ToList();

                            if (x_exp.Count != 0)
                            {
                                double _total_exp = 0.00;
                                double _JanTotalDetails = 0.00;
                                double _FebTotalsDetails = 0.00;
                                double _MarTotalsDetails = 0.00;
                                double _AprTotalsDetails = 0.00;
                                double _MayTotalsDetails = 0.00;
                                double _JunTotalsDetails = 0.00;
                                double _JulTotalsDetails = 0.00;
                                double _AugTotalsDetails = 0.00;
                                double _SepTotalsDetails = 0.00;
                                double _OctTotalsDetails = 0.00;
                                double _NovTotalsDetails = 0.00;
                                double _DecTotalsDetails = 0.00;

                                if (_lists.Count != 0)
                                {


                                    foreach (var item_r in x_exp)
                                    {
                                        _total_exp += Convert.ToDouble(item_r.total);
                                    }

                                    foreach (var item_lists in _lists)
                                    {

                                        switch (item_lists.months)
                                        {
                                            case "Jan":
                                                _JanTotalDetails += Convert.ToDouble(item_lists.total);
                                                item_data.Jan = _JanTotalDetails.ToString("#,##0.00");
                                                break;
                                            case "Feb":
                                                _FebTotalsDetails += Convert.ToDouble(item_lists.total);
                                                item_data.Feb = _FebTotalsDetails.ToString("#,##0.00");
                                                break;
                                            case "Mar":
                                                _MarTotalsDetails += Convert.ToDouble(item_lists.total);
                                                item_data.Mar = _MarTotalsDetails.ToString("#,##0.00");
                                                break;
                                            case "Apr":
                                                _AprTotalsDetails += Convert.ToDouble(item_lists.total);
                                                item_data.Apr = _AprTotalsDetails.ToString("#,##0.00");
                                                break;
                                            case "May":
                                                _MayTotalsDetails += Convert.ToDouble(item_lists.total);
                                                item_data.May = _MayTotalsDetails.ToString("#,##0.00");
                                                break;
                                            case "Jun":
                                                _JunTotalsDetails += Convert.ToDouble(item_lists.total);
                                                item_data.Jun = _JunTotalsDetails.ToString("#,##0.00");
                                                break;
                                            case "Jul":
                                                _JulTotalsDetails += Convert.ToDouble(item_lists.total);
                                                item_data.Jul = _JulTotalsDetails.ToString("#,##0.00");
                                                break;
                                            case "Aug":
                                                _AugTotalsDetails += Convert.ToDouble(item_lists.total);
                                                item_data.Aug = _AugTotalsDetails.ToString("#,##0.00");
                                                break;
                                            case "Sep":
                                                _SepTotalsDetails += Convert.ToDouble(item_lists.total);
                                                item_data.Sep = _SepTotalsDetails.ToString("#,##0.00");
                                                break;
                                            case "Oct":
                                                _OctTotalsDetails += Convert.ToDouble(item_lists.total);
                                                item_data.Oct = _OctTotalsDetails.ToString("#,##0.00");
                                                break;
                                            case "Nov":
                                                _NovTotalsDetails += Convert.ToDouble(item_lists.total);
                                                item_data.Nov = _NovTotalsDetails.ToString("#,##0.00");
                                                break;
                                            case "Dec":
                                                _DecTotalsDetails += Convert.ToDouble(item_lists.total);
                                                item_data.Dec = _DecTotalsDetails.ToString("#,##0.00");
                                                break;

                                        }

                                    }

                                    item_data.total_expenditures = _total_exp.ToString("#,##0.00");

                                    item_data.total_balance = (Convert.ToDouble(item_data.allocation.ToString()) - _total_exp).ToString("#,##0.00");

                                    item_data.allocation = Convert.ToDouble(item_data.allocation).ToString("#,##0.00");
                                    item_data.total_budget = (_total_exp + Convert.ToDouble(item_data.total_balance)).ToString("#,##0.00");
                                }
                            }
                            else
                            {
                                item_data.Jan = 0.ToString("#,##0.00");
                                item_data.Feb = 0.ToString("#,##0.00");
                                item_data.Mar = 0.ToString("#,##0.00");
                                item_data.Apr = 0.ToString("#,##0.00");
                                item_data.May = 0.ToString("#,##0.00");
                                item_data.Jun = 0.ToString("#,##0.00");
                                item_data.Jul = 0.ToString("#,##0.00");
                                item_data.Aug = 0.ToString("#,##0.00");
                                item_data.Sep = 0.ToString("#,##0.00");
                                item_data.Oct = 0.ToString("#,##0.00");
                                item_data.Nov = 0.ToString("#,##0.00");
                                item_data.Dec = 0.ToString("#,##0.00");
                                item_data.total_expenditures = 0.ToString("#,##0.00");
                                item_data.total_budget = 0.ToString("#,##0.00");
                                item_data.total_balance = (Convert.ToDouble(item_data.allocation.ToString()) - 0).ToString("#,##0.00");

                                item_data.allocation = Convert.ToDouble(item_data.allocation).ToString("#,##0.00");
                            }



                        }
                        ChiefData_FundSourceMainDetails x_space = new ChiefData_FundSourceMainDetails();

                        x_space.name = "-----------------------------------------";
                        x_space.fund_name = "------------";
                        x_space.allocation = "---------------------";
                        x_space.Jan = "------------";
                        x_space.Feb = "------------";
                        x_space.Mar = "------------";
                        x_space.Apr = "------------";
                        x_space.May = "------------";
                        x_space.Jun = "------------";
                        x_space.Jul = "------------";
                        x_space.Aug = "------------";
                        x_space.Sep = "------------";
                        x_space.Oct = "------------";
                        x_space.Nov = "------------";
                        x_space.Dec = "------------";
                        x_space.total_expenditures = "---------------------";
                        x_space.total_balance = "---------------------";
                        x_space.total_budget = "---------------------";
                        x_details.Add(x_space);

                        ChiefData_FundSourceMainDetails x_totals = new ChiefData_FundSourceMainDetails();


                        double _Allocation = 0.00;

                        double _JanTotal = 0.00;
                        double _FebTotals = 0.00;
                        double _MarTotals = 0.00;
                        double _AprTotals = 0.00;
                        double _MayTotals = 0.00;
                        double _JunTotals = 0.00;
                        double _JulTotals = 0.00;
                        double _AugTotals = 0.00;
                        double _SepTotals = 0.00;
                        double _OctTotals = 0.00;
                        double _NovTotals = 0.00;
                        double _DecTotals = 0.00;


                        double _total_expenditures = 0.00;
                        double _total_balance = 0.00;
                        double _total_budgets = 0.00;
                        x_totals.name = "Grand Total";

                        foreach (var item_x in x_details)
                        {
                            try
                            {

                                _Allocation += Convert.ToDouble(item_x.allocation);
                                _JanTotal += Convert.ToDouble(item_x.Jan);
                                _FebTotals += Convert.ToDouble(item_x.Feb);
                                _MarTotals += Convert.ToDouble(item_x.Mar);
                                _AprTotals += Convert.ToDouble(item_x.Apr);
                                _MayTotals += Convert.ToDouble(item_x.May);
                                _JunTotals += Convert.ToDouble(item_x.Jun);
                                _JulTotals += Convert.ToDouble(item_x.Jul);
                                _AugTotals += Convert.ToDouble(item_x.Aug);
                                _SepTotals += Convert.ToDouble(item_x.Sep);
                                _OctTotals += Convert.ToDouble(item_x.Oct);
                                _NovTotals += Convert.ToDouble(item_x.Nov);
                                _DecTotals += Convert.ToDouble(item_x.Dec);

                                _total_expenditures += Convert.ToDouble(item_x.total_expenditures);
                                _total_balance += Convert.ToDouble(item_x.total_balance);
                                _total_budgets += Convert.ToDouble(item_x.total_budget);
                            }
                            catch (Exception)
                            {


                            }


                        }
                        x_totals.allocation = _Allocation.ToString("#,##0.00");
                        x_totals.Jan = _JanTotal.ToString("#,##0.00");
                        x_totals.Feb = _FebTotals.ToString("#,##0.00");
                        x_totals.Mar = _MarTotals.ToString("#,##0.00");
                        x_totals.Apr = _AprTotals.ToString("#,##0.00");
                        x_totals.May = _MayTotals.ToString("#,##0.00");
                        x_totals.Jun = _JunTotals.ToString("#,##0.00");
                        x_totals.Jul = _JulTotals.ToString("#,##0.00");
                        x_totals.Aug = _AugTotals.ToString("#,##0.00");
                        x_totals.Sep = _SepTotals.ToString("#,##0.00");
                        x_totals.Oct = _OctTotals.ToString("#,##0.00");
                        x_totals.Nov = _NovTotals.ToString("#,##0.00");
                        x_totals.Dec = _DecTotals.ToString("#,##0.00");
                        x_totals.total_expenditures = _total_expenditures.ToString("#,##0.00");
                        x_totals.total_balance = _total_balance.ToString("#,##0.00");
                        x_totals.total_budget = _total_budgets.ToString("#,##0.00");
                        x_details.Add(x_totals);

                        x_data.FundAllocation = x_details;

                        cd_FundSource.Add(x_data);
                    }
                    grdData.ItemsSource = null;
                    grdData.ItemsSource = cd_FundSource;

                    foreach (var item in grdData.Rows)
                    {
                        if (item.HasChildren)
                        {
                            item.IsExpanded = true;
                            foreach (var item_c in item.ChildBands)
                            {
                                item_c.Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
                                item_c.Columns["fund_name"].Visibility = System.Windows.Visibility.Collapsed;
                                item_c.Columns["total_obligated"].Visibility = System.Windows.Visibility.Collapsed;

                                item_c.Columns["name"].HeaderText = "Expense Item";
                                item_c.Columns["allocation"].HeaderText = "Allocation";
                                item_c.Columns["total_expenditures"].HeaderText = "Total Plan";
                                item_c.Columns["total_balance"].HeaderText = "Contingency";

                                item_c.Columns["total_budget"].HeaderText = "Total Budget";
                            }
                        }
                    }

                    //   FetchFundSourceMain();
                    SetAlignment();
                    if (state = state ^ true)
                    {
                        VisualStateManager.GoToState(this, "Start", true);
                    }

                    else
                    {
                        VisualStateManager.GoToState(this, "New", true);
                    }
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchExpenditureDetails":
                    XDocument oDocFetchExpenditureDetails = XDocument.Parse(_results);
                    var _dataFetchExpenditureDetails = from info in oDocFetchExpenditureDetails.Descendants("Table")
                                                       select new ChiefData_ListExpenditure
                                                       {
                                                           expenditure = Convert.ToString(info.Element("expenditure").Value),
                                                           fund_source_id = Convert.ToString(info.Element("fund_source_id").Value),
                                                           total = Convert.ToString(info.Element("total").Value)

                                                       };



                    cd_ExpenditureLists.Clear();

                    foreach (var item in _dataFetchExpenditureDetails)
                    {
                        ChiefData_ListExpenditure _varData = new ChiefData_ListExpenditure();

                        _varData.expenditure = item.expenditure;
                        _varData.fund_source_id = item.fund_source_id;
                        _varData.total = item.total;

                        cd_ExpenditureLists.Add(_varData);

                    }
                    FetchFundSourceMainDetails();
                    this.Cursor = Cursors.Arrow;
                    break;
            }
        }
        private void FetchExpenditureDetails()
        {
            
                c_finance.Process = "FetchExpenditureDetails";
                svc_mindaf.ExecuteSQLAsync(c_finance.FetchDivisionExpenditureDetailsByDiv(this.DivisionID, this.Years));

        }
        private void FetchFundSourceMain()
        {
                c_finance.Process = "FetchFundMain";
                svc_mindaf.ExecuteSQLAsync(c_finance.FetchFundSource(this.DivisionID, this.Years,"",""));
        }

        private void FetchExpenditureDetailsItems()
        {
          
                c_finance.Process = "FetchExpenditureDetailsItemLists";
                svc_mindaf.ExecuteSQLAsync(c_finance.FetchDivisionExpenditureDetailsItemByDiv(this.DivisionID, this.Years,"",""));
           
        }

       
        private void LoadDivisionUnit()
        {
            
            c_finance.Process = "LoadDivisionUnit";
            svc_mindaf.ExecuteSQLAsync(c_finance.LoadDivisionUnit(this.DivisionID,this.Years));

        }
        private void FetchFundSourceMainDetails()
        {

                c_finance.Process = "FetchFundSourceMainDetails";
                svc_mindaf.ExecuteSQLAsync(c_finance.FetchFundSourceDetails(this.DivisionID, this.Years,"",""));
  
        }

        public void CloseAnimation() 
        {
            VisualStateManager.GoToState(this, "Out", true);
        }

        private void PageTrans_CurrentStateChanged(object sender, VisualStateChangedEventArgs e)
        {

        }

        private void SetAlignment() 
        {
            grdData.Rows[0].ChildBands[0].Cells["Jan"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            grdData.Rows[0].ChildBands[0].Cells["Feb"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            grdData.Rows[0].ChildBands[0].Cells["Mar"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            grdData.Rows[0].ChildBands[0].Cells["Apr"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            grdData.Rows[0].ChildBands[0].Cells["May"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            grdData.Rows[0].ChildBands[0].Cells["Jun"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            grdData.Rows[0].ChildBands[0].Cells["Jul"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            grdData.Rows[0].ChildBands[0].Cells["Aug"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            grdData.Rows[0].ChildBands[0].Cells["Sep"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            grdData.Rows[0].ChildBands[0].Cells["Oct"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            grdData.Rows[0].ChildBands[0].Cells["Nov"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            grdData.Rows[0].ChildBands[0].Cells["Dec"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            grdData.Rows[0].ChildBands[0].Cells["allocation"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            grdData.Rows[0].ChildBands[0].Cells["total_expenditures"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            grdData.Rows[0].ChildBands[0].Cells["total_balance"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            grdData.Rows[0].ChildBands[0].Cells["total_budget"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
        }

        private void usr_DivSumm_Loaded(object sender, RoutedEventArgs e)
        {
            FetchExpenditureDetailsItems();
          
           
        }

        private void btnhideShow_Click(object sender, RoutedEventArgs e)
        {
           
            switch (btnhideShow.Content.ToString())
            {
                case "Hide Months":
                    grdData.Rows[0].ChildBands[0].Columns["Jan"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Rows[0].ChildBands[0].Columns["Feb"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Rows[0].ChildBands[0].Columns["Mar"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Rows[0].ChildBands[0].Columns["Apr"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Rows[0].ChildBands[0].Columns["May"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Rows[0].ChildBands[0].Columns["Jun"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Rows[0].ChildBands[0].Columns["Jul"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Rows[0].ChildBands[0].Columns["Aug"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Rows[0].ChildBands[0].Columns["Sep"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Rows[0].ChildBands[0].Columns["Oct"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Rows[0].ChildBands[0].Columns["Nov"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Rows[0].ChildBands[0].Columns["Dec"].Visibility = System.Windows.Visibility.Collapsed;
                    btnhideShow.Content = "Show Months";
                    break;
                case "Show Months":
                    grdData.Rows[0].ChildBands[0].Columns["Jan"].Visibility = System.Windows.Visibility.Visible;
                    grdData.Rows[0].ChildBands[0].Columns["Feb"].Visibility = System.Windows.Visibility.Visible;
                    grdData.Rows[0].ChildBands[0].Columns["Mar"].Visibility = System.Windows.Visibility.Visible;
                    grdData.Rows[0].ChildBands[0].Columns["Apr"].Visibility = System.Windows.Visibility.Visible;
                    grdData.Rows[0].ChildBands[0].Columns["May"].Visibility = System.Windows.Visibility.Visible;
                    grdData.Rows[0].ChildBands[0].Columns["Jun"].Visibility = System.Windows.Visibility.Visible;
                    grdData.Rows[0].ChildBands[0].Columns["Jul"].Visibility = System.Windows.Visibility.Visible;
                    grdData.Rows[0].ChildBands[0].Columns["Aug"].Visibility = System.Windows.Visibility.Visible;
                    grdData.Rows[0].ChildBands[0].Columns["Sep"].Visibility = System.Windows.Visibility.Visible;
                    grdData.Rows[0].ChildBands[0].Columns["Oct"].Visibility = System.Windows.Visibility.Visible;
                    grdData.Rows[0].ChildBands[0].Columns["Nov"].Visibility = System.Windows.Visibility.Visible;
                    grdData.Rows[0].ChildBands[0].Columns["Dec"].Visibility = System.Windows.Visibility.Visible;
                    btnhideShow.Content = "Hide Months";
                    break;
            }
        }
    }
}
