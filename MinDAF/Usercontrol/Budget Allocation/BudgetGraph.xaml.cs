﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml.Linq;

namespace MinDAF.Usercontrol.Budget_Allocation
{
    public partial class BudgetGraph : UserControl
    {
        public String DivisionID { get; set; }

        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private BudgetChartData c_budget_chart = new BudgetChartData();
        private List<BudgetChartList> LocalData_Budget = new List<BudgetChartList>();
        private List<BudgetPieList> LocalChartData = new List<BudgetPieList>();
        PagedCollectionView MainData = null;
        private DispatcherTimer _timer = new DispatcherTimer();



        public BudgetGraph()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            Loaded += BudgetGraph_Loaded;

            _timer.Tick += _timer_Tick;
            _timer.Interval = new TimeSpan(0, 0, 3);

           
        }


        void _timer_Tick(object sender, EventArgs e)
        {
            //throw new NotImplementedException();
            _timer.Stop();
            FetchLogs();
        }



        void BudgetGraph_Loaded(object sender, RoutedEventArgs e)
        {
            FetchProjects();

            GenerateYear();
                 
        }

        private void FetchLogs() 
        {
            c_budget_chart.Process = "FetchLogs";
          
                svc_mindaf.ExecuteSQLAsync(c_budget_chart.BudgetActiviyLog(this.DivisionID));

        }

        private void ExpandAll() 
        {
            foreach (var item in grdProjectList.Rows)
            {
                if (item.HasChildren)
                {
                    item.IsExpanded = true;

                   
                }
               
             
            }
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_budget_chart.Process)
            {
                case "FetchChartDataByYear":
                      XDocument oDocExpenditures = XDocument.Parse(_results);
                      var _dataExpenditures = from info in oDocExpenditures.Descendants("Table")
                                 select new  BudgetChartList
                                 {
                                     Project = Convert.ToString(info.Element("Project").Value),
                                     Expenditure = Convert.ToString(info.Element("Expenditure").Value),
                                     Amount  = Convert.ToDouble(info.Element("Amount").Value).ToString("#,##0.00")
                        
                                 };


                      List<BudgetChartList> ProjectExpendituresDetails = new List<BudgetChartList>();
                      LocalData_Budget.Clear();
                      foreach (var item in _dataExpenditures)
                    {
                        BudgetChartList _varDetails = new BudgetChartList();

                   
                        _varDetails.Project = item.Project;
                        _varDetails.Expenditure = item.Expenditure;
                        _varDetails.Amount = item.Amount;


                        LocalData_Budget.Add(_varDetails);
                    }

                     MainData = new PagedCollectionView(LocalData_Budget);
                     MainData.GroupDescriptions.Add(new PropertyGroupDescription("Project"));

                     if (LocalData_Budget.Count>0)
                     {
                         grdProjectList.ItemsSource = MainData;
                         foreach (var item in grdProjectList.Columns)
                         {
                             if (item.HeaderText == "Project")
                             {
                                 item.Visibility = System.Windows.Visibility.Collapsed;
                             }
                         }
                         ExpandAll();
                     }
                     else
                     {
                         try
                         {
                             grdProjectList.ItemsSource = null;
                         }
                         catch (Exception)
                         {

                         }
                                                                            
                     }

                     FetchChartData();
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchChartGraphData":
                      XDocument oDocChart = XDocument.Parse(_results);
                      var _dataChart = from info in oDocChart.Descendants("Table")
                                 select new  BudgetPieList
                                 {
                                     Project = Convert.ToString(info.Element("Project").Value),
                                     Amount  = Convert.ToDouble(info.Element("Amount").Value)
                        
                                 };


                      List<BudgetPieList> ProjectChartDetails = new List<BudgetPieList>();
                      LocalData_Budget.Clear();
                      LocalChartData.Clear();
                      foreach (var item in _dataChart)
                    {
                        BudgetPieList _varDetails = new BudgetPieList();

                   
                        _varDetails.Project = item.Project;
                       
                        _varDetails.Amount = item.Amount;


                        LocalChartData.Add(_varDetails);
                    }

                      this.DataContext = null;
                      this.DataContext = new BudgetChartViewModel(LocalChartData);
                      FetchOverAllData();
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchOverallData":

                     XDocument oDocOverAll = XDocument.Parse(_results);
                     var _dataOverAll = from info in oDocOverAll.Descendants("Table")
                                 select new  BudgetOverAllData
                                 {
                                     Title = Convert.ToString(info.Element("Title").Value),
                                     Amount  = Convert.ToDouble(info.Element("Amount").Value)
                        
                                 };


                   
                      foreach (var item in _dataOverAll)
                    {
                        BudgetPieList _varDetails = new BudgetPieList();


                        switch (item.Title)
                        {
                            case "Allocated Budget":
                                txtTotalFix.Value = item.Amount;
                                break;
                            case "Budget Proposal":
                                txtTotalProjectCost.Value = item.Amount;
                                break;
                            case "Total Projects":
                                txtNoProjects.Value = item.Amount;
                                break;
                        }
      
                    }

                      txtTotalRemaining.Value = (double)txtTotalFix.Value - (double)txtTotalProjectCost.Value;


                      _timer.Start();
                    this.Cursor = Cursors.Arrow;
                    break;

                case "FetchLogs":

                    XDocument oDocLogs = XDocument.Parse(_results);
                    var _dataLogs = from info in oDocLogs.Descendants("Table")
                                       select new Logs
                                       {
                                           Status = Convert.ToString(info.Element("Status").Value),
                                       };

                    List<Logs> _dataListLogs = new List<Logs>();

                    foreach (var item in _dataLogs)
                    {
                        Logs _varDetails = new Logs();


                
                        _varDetails.Status = item.Status;

                        _dataListLogs.Add(_varDetails);


                    }

                    grdActivities.ItemsSource = _dataListLogs;

                    _timer.Start();
                    this.Cursor = Cursors.Arrow;
                    break;
            }
        }

        private void FetchOverAllData() 
        {
            c_budget_chart.Process = "FetchOverallData";
            if (cmbYear.SelectedItem == null)
            {
                int _year = DateTime.Now.Year;
                svc_mindaf.ExecuteSQLAsync(c_budget_chart.FetchOverAllData(this.DivisionID, _year.ToString()));

            }
            else
            {
                svc_mindaf.ExecuteSQLAsync(c_budget_chart.FetchOverAllData(this.DivisionID, cmbYear.SelectedItem.ToString()));

            }
        }


        private void GenerateYear()         
        {
            int _year = DateTime.Now.Year;
            int _limit = _year + 100;

            for (int i = _year ; i != _limit; i++)
            {
                cmbYear.Items.Add(_year);
                _year +=1;
            }
            cmbYear.SelectedIndex = 0;
        }

        private void FetchProjects() 
        {
            c_budget_chart.Process = "FetchChartDataByYear";
            if (cmbYear.SelectedItem==null)
            {
                int _year = DateTime.Now.Year;
                svc_mindaf.ExecuteSQLAsync(c_budget_chart.FetchDashBoardData(this.DivisionID, _year.ToString()));

            }
            else
            {
                svc_mindaf.ExecuteSQLAsync(c_budget_chart.FetchDashBoardData(this.DivisionID, cmbYear.SelectedItem.ToString()));

            }
           
        }

        private void FetchChartData()
        {
            c_budget_chart.Process = "FetchChartGraphData";
            if (cmbYear.SelectedItem == null)
            {
                int _year = DateTime.Now.Year;
                svc_mindaf.ExecuteSQLAsync(c_budget_chart.FetchChartData(this.DivisionID, _year.ToString()));

            }
            else
            {
                svc_mindaf.ExecuteSQLAsync(c_budget_chart.FetchChartData(this.DivisionID, cmbYear.SelectedItem.ToString()));

            }
        }

        private void cmbYear_DropDownClosed(object sender, EventArgs e)
        {
            LocalData_Budget.Clear();
          
            FetchProjects();
          
        }

        private void StackPanel_SizeChanged_1(object sender, SizeChangedEventArgs e)
        {
           
        }

        private void stkChild_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            grdProjectList.Width = e.NewSize.Width;
            grdProjectList.Height = e.NewSize.Height;
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
           
        }

    }
}
