﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using Infragistics.Controls.Grids;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol.Budget_Allocation
{
    public partial class BudgetApprovals : UserControl
    {


        private List<App_FundSources> ListFundSourceType = new List<App_FundSources>();
        private List<SubPAP_Division> ListSubPap = new List<SubPAP_Division>();
        private List<OfficesPAP> ListOffice = new List<OfficesPAP>();
        private List<Divisions> ListDivision = new List<Divisions>();
        private List<Divisions> ListDivisionUnit = new List<Divisions>();

        private List<ChiefData_FundSourceMain> cd_FundSourceMain = new List<ChiefData_FundSourceMain>();
        private List<ChiefData_FundSourceMainDetails> cd_FundSourceMainDetails = new List<ChiefData_FundSourceMainDetails>();
        private List<ChiefData_FundSource> cd_FundSource = new List<ChiefData_FundSource>();
        private List<ChiefData_ListExpenditure> cd_ExpenditureLists = new List<ChiefData_ListExpenditure>();
        private List<Approved_Data> cd_Approved = new List<Approved_Data>();
        private List<ChiefData_DivisionFundSource> cd_DivisionFundSource = new List<ChiefData_DivisionFundSource>();
        private List<OfficeDetails> BalanceDetails = new List<OfficeDetails>();
        private List<ChiefData_ItemLists> ListItemDetails = new List<ChiefData_ItemLists>();
        

        public String DivisionID { get; set; }
        public String Process { get; set; }
        public String WorkingYear { get; set; }
        private String FundSource { get; set; }
        private BudgetApprovalsList c_approval = new BudgetApprovalsList();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        PagedCollectionView MainData = null;

        private List<ApprovalData> ListApprovalData = new List<ApprovalData>();
        private List<MainData> _ListMainData = new List<MainData>();
        private List<MainTotals> _ListMainTotals = new List<MainTotals>();
        private List<Main_OverAllYear> _ListOverAllYear = new List<Main_OverAllYear>();
        private List<MenuFundSource> f_source = new List<MenuFundSource>();

        public BudgetApprovals(String fSource)
        {
            InitializeComponent();

            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            this.Loaded += BudgetApprovals_Loaded;
            FundSource = fSource;
        }

        void BudgetApprovals_Loaded(object sender, RoutedEventArgs e)
        {
            FetchFundSourceMain();
        }
        private void FetchApprovedFinance()
        {
            c_approval.Process = "FetchApprovedFinance";
            svc_mindaf.ExecuteSQLAsync(c_approval.FetchApprovedFinance());
        }
        private void FetchFundSourceMainDetails()
        {
           
            String OfficeId = this.DivisionID;

          
                c_approval.Process = "FetchFundSourceMainDetails";
                svc_mindaf.ExecuteSQLAsync(c_approval.FetchFundSourceDetails(OfficeId,WorkingYear,this.FundSource));
     
        }
        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            String _results = e.Result.ToString();

            switch (c_approval.Process)
            {
             
                case "FetchExpenditureDetails":
                    XDocument oDocFetchExpenditureDetails = XDocument.Parse(_results);
                    var _dataFetchExpenditureDetails = from info in oDocFetchExpenditureDetails.Descendants("Table")
                                                       select new ChiefData_ListExpenditure
                                                       {
                                                           expenditure = Convert.ToString(info.Element("expenditure").Value),
                                                           fund_source_id = Convert.ToString(info.Element("fund_source_id").Value),
                                                           total = Convert.ToString(info.Element("total").Value)

                                                       };



                    cd_ExpenditureLists.Clear();

                    foreach (var item in _dataFetchExpenditureDetails)
                    {
                        ChiefData_ListExpenditure _varData = new ChiefData_ListExpenditure();

                        _varData.expenditure = item.expenditure;
                        _varData.fund_source_id = item.fund_source_id;
                        _varData.total = item.total;

                        cd_ExpenditureLists.Add(_varData);

                    }
                    FetchApprovedFinance();

                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchApprovedFinance":
                    XDocument oDocFetchApprovedFinance = XDocument.Parse(_results);
                    var _dataFetchApprovedFinance = from info in oDocFetchApprovedFinance.Descendants("Table")
                                                    select new Approved_Data
                                                    {
                                                        Code = Convert.ToString(info.Element("code").Value)

                                                    };



                    cd_Approved.Clear();

                    foreach (var item in _dataFetchApprovedFinance)
                    {
                        Approved_Data _varData = new Approved_Data();

                        _varData.Code = item.Code;


                        cd_Approved.Add(_varData);

                    }
                    FetchFundSourceMainDetails();
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchFundMain":
                    XDocument oDocFetchFundMain = XDocument.Parse(_results);
                    var _dataFetchFundMain = from info in oDocFetchFundMain.Descendants("Table")
                                             select new ChiefData_FundSourceMain
                                             {
                                                 id = Convert.ToString(info.Element("id").Value),
                                                 fund_source = Convert.ToString(info.Element("fund_name").Value),
                                                 fund_type = Convert.ToString(info.Element("type_fund").Value)
                                             };



                    cd_FundSourceMain.Clear();
                    cmbFundSource.Items.Clear();

                    foreach (var item in _dataFetchFundMain)
                    {
                        ChiefData_FundSourceMain _varData = new ChiefData_FundSourceMain();

                        _varData.id = item.id;
                        _varData.fund_source = item.fund_source;
                        _varData.fund_type = item.fund_type;
                        cd_FundSourceMain.Add(_varData);
                        cmbFundSource.Items.Add(item.fund_source);
                    }

                    cmbFundSource.SelectedIndex = 0;
                  //  FetchExpenditureDetails();
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchExpenditureDetailsItemLists":
                    XDocument oDocFetchFetchExpenditureDetailsItemLists = XDocument.Parse(_results);
                    var _dataFetchFetchExpenditureDetailsItemLists = from info in oDocFetchFetchExpenditureDetailsItemLists.Descendants("Table")
                                                                     select new ChiefData_ItemLists
                                                                     {
                                                                         main_id = Convert.ToString(info.Element("main_id").Value),
                                                                         mpo_id = Convert.ToString(info.Element("mpo_id").Value),
                                                                         prog_id = Convert.ToString(info.Element("prog_id").Value),
                                                                         project_id = Convert.ToString(info.Element("project_id").Value),
                                                                         id = Convert.ToString(info.Element("id").Value),
                                                                         expenditure = Convert.ToString(info.Element("expenditure").Value),
                                                                         months = Convert.ToString(info.Element("month").Value),
                                                                         total = Convert.ToDouble(info.Element("total").Value),
                                                                         is_revision = Convert.ToString(info.Element("is_revision").Value),
                                                                     };



                    ListItemDetails.Clear();

                    foreach (var item in _dataFetchFetchExpenditureDetailsItemLists)
                    {
                        ChiefData_ItemLists _varData = new ChiefData_ItemLists();

                        _varData.main_id = item.main_id;
                        _varData.mpo_id = item.mpo_id;
                        _varData.prog_id = item.prog_id;
                        _varData.project_id = item.project_id;

                        _varData.id = item.id;
                        _varData.expenditure = item.expenditure;
                        _varData.months = item.months;
                        _varData.total = item.total;
                        _varData.is_revision = item.is_revision;

                        ListItemDetails.Add(_varData);
                    }

                    FetchExpenditureDetails();
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchFundSourceMainDetails":
                    XDocument oDocFetchFundSourceMainDetails = XDocument.Parse(_results);
                    var _dataFetchFundSourceMainDetails = from info in oDocFetchFundSourceMainDetails.Descendants("Table")
                                                          select new ChiefData_FundSourceMainDetails
                                                          {
                                                              allocation = Convert.ToString(info.Element("Allocation").Value),
                                                              fund_name = Convert.ToString(info.Element("Fund_Name").Value),
                                                              id = Convert.ToString(info.Element("id").Value),
                                                              name = Convert.ToString(info.Element("Name").Value)
                                                          };



                    cd_FundSourceMainDetails.Clear();

                    foreach (var item in _dataFetchFundSourceMainDetails)
                    {
                        ChiefData_FundSourceMainDetails _varData = new ChiefData_FundSourceMainDetails();

                        _varData.allocation = Convert.ToDouble(item.allocation).ToString("#,##0.00");
                        _varData.fund_name = item.fund_name;
                        _varData.id = item.id;
                        _varData.name = item.name;

                        if (item.Jan==null){item.Jan = 0.ToString("#,##0.00");}
                        if (item.Feb == null) { item.Feb = 0.ToString("#,##0.00"); }
                        if (item.Mar == null) { item.Mar = 0.ToString("#,##0.00"); }
                        if (item.Apr == null) { item.Apr = 0.ToString("#,##0.00"); }
                        if (item.May == null) { item.May = 0.ToString("#,##0.00"); }
                        if (item.Jun == null) { item.Jun = 0.ToString("#,##0.00"); }
                        if (item.Jul == null) { item.Jul = 0.ToString("#,##0.00"); }
                        if (item.Aug == null) { item.Aug = 0.ToString("#,##0.00"); }
                        if (item.Sep == null) { item.Sep = 0.ToString("#,##0.00"); }
                        if (item.Oct == null) { item.Oct = 0.ToString("#,##0.00"); }
                        if (item.Nov == null) { item.Nov = 0.ToString("#,##0.00"); }
                        if (item.Dec == null) { item.Dec = 0.ToString("#,##0.00"); }
                  
                        cd_FundSourceMainDetails.Add(_varData);
                    }


                    cd_FundSource.Clear();

                    foreach (var item in cd_FundSourceMain)
                    {
                        if (item.fund_source == cmbFundSource.SelectedItem.ToString())
                        {


                            List<ChiefData_FundSourceMainDetails> x_details = _dataFetchFundSourceMainDetails.Where(x => x.id == item.id).ToList();
                            ChiefData_FundSource x_data = new ChiefData_FundSource();

                            x_data.FundName = item.fund_source;
                            FundSource = item.id;

                            List<Approved_Data> x_check = cd_Approved.Where(x => x.Code == FundSource).ToList();
                            bool isapprove = false;


                            foreach (var item_data in x_details)
                            {
                                List<ChiefData_ListExpenditure> x_exp = cd_ExpenditureLists.Where(x => x.fund_source_id == item_data.id && x.expenditure == item_data.name).ToList();
                                List<ChiefData_ItemLists> _lists = ListItemDetails.Where(x => x.expenditure == item_data.name).ToList();

                                if (x_exp.Count != 0)
                                {
                                    double _total_exp = 0.00;
                                    double _JanTotalDetails = 0.00;
                                    double _FebTotalsDetails = 0.00;
                                    double _MarTotalsDetails = 0.00;
                                    double _AprTotalsDetails = 0.00;
                                    double _MayTotalsDetails = 0.00;
                                    double _JunTotalsDetails = 0.00;
                                    double _JulTotalsDetails = 0.00;
                                    double _AugTotalsDetails = 0.00;
                                    double _SepTotalsDetails = 0.00;
                                    double _OctTotalsDetails = 0.00;
                                    double _NovTotalsDetails = 0.00;
                                    double _DecTotalsDetails = 0.00;

                                    if (_lists.Count != 0)
                                    {


                                        foreach (var item_r in x_exp)
                                        {
                                            _total_exp += Convert.ToDouble(item_r.total);
                                        }
                                        _total_exp = 0;
                                        foreach (var item_lists in _lists)
                                        {

                                            switch (item_lists.months)
                                            {
                                                case "Jan":
                                                    _JanTotalDetails += Convert.ToDouble(item_lists.total);
                                                    _total_exp += Convert.ToDouble(item_lists.total);
                                                    item_data.Jan = _JanTotalDetails.ToString("#,##0.00");
                                                    break;
                                                case "Feb":
                                                    _FebTotalsDetails += Convert.ToDouble(item_lists.total);
                                                    _total_exp += Convert.ToDouble(item_lists.total);
                                                    item_data.Feb = _FebTotalsDetails.ToString("#,##0.00");
                                                    break;
                                                case "Mar":
                                                    _MarTotalsDetails += Convert.ToDouble(item_lists.total);
                                                    _total_exp += Convert.ToDouble(item_lists.total);
                                                    item_data.Mar = _MarTotalsDetails.ToString("#,##0.00");
                                                    break;
                                                case "Apr":
                                                    _AprTotalsDetails += Convert.ToDouble(item_lists.total);
                                                    _total_exp += Convert.ToDouble(item_lists.total);
                                                    item_data.Apr = _AprTotalsDetails.ToString("#,##0.00");
                                                    break;
                                                case "May":
                                                    _MayTotalsDetails += Convert.ToDouble(item_lists.total);
                                                    _total_exp += Convert.ToDouble(item_lists.total);
                                                    item_data.May = _MayTotalsDetails.ToString("#,##0.00");
                                                    break;
                                                case "Jun":
                                                    _JunTotalsDetails += Convert.ToDouble(item_lists.total);
                                                    _total_exp += Convert.ToDouble(item_lists.total);
                                                    item_data.Jun = _JunTotalsDetails.ToString("#,##0.00");
                                                    break;
                                                case "Jul":
                                                    _JulTotalsDetails += Convert.ToDouble(item_lists.total);
                                                    _total_exp += Convert.ToDouble(item_lists.total);
                                                    item_data.Jul = _JulTotalsDetails.ToString("#,##0.00");
                                                    break;
                                                case "Aug":
                                                    _AugTotalsDetails += Convert.ToDouble(item_lists.total);
                                                    _total_exp += Convert.ToDouble(item_lists.total);
                                                    item_data.Aug = _AugTotalsDetails.ToString("#,##0.00");
                                                    break;
                                                case "Sep":
                                                    _SepTotalsDetails += Convert.ToDouble(item_lists.total);
                                                    _total_exp += Convert.ToDouble(item_lists.total);
                                                    item_data.Sep = _SepTotalsDetails.ToString("#,##0.00");
                                                    break;
                                                case "Oct":
                                                    _OctTotalsDetails += Convert.ToDouble(item_lists.total);
                                                    _total_exp += Convert.ToDouble(item_lists.total);
                                                    item_data.Oct = _OctTotalsDetails.ToString("#,##0.00");
                                                    break;
                                                case "Nov":
                                                    _NovTotalsDetails += Convert.ToDouble(item_lists.total);
                                                    _total_exp += Convert.ToDouble(item_lists.total);
                                                    item_data.Nov = _NovTotalsDetails.ToString("#,##0.00");
                                                    break;
                                                case "Dec":
                                                    _DecTotalsDetails += Convert.ToDouble(item_lists.total);
                                                    _total_exp += Convert.ToDouble(item_lists.total);
                                                    item_data.Dec = _DecTotalsDetails.ToString("#,##0.00");
                                                    break;

                                            }

                                        }

                                        item_data.total_expenditures = _total_exp.ToString("#,##0.00");

                                        item_data.total_balance = (Convert.ToDouble(item_data.allocation.ToString()) - _total_exp).ToString("#,##0.00");

                                        item_data.allocation = Convert.ToDouble(item_data.allocation).ToString("#,##0.00");
                                        item_data.total_budget = (_total_exp + (Convert.ToDouble(item_data.allocation.ToString()) - _total_exp)).ToString("#,##0.00");
                                    }
                                }
                                else
                                {
                                    item_data.Jan = 0.ToString("#,##0.00");
                                    item_data.Feb = 0.ToString("#,##0.00");
                                    item_data.Mar = 0.ToString("#,##0.00");
                                    item_data.Apr = 0.ToString("#,##0.00");
                                    item_data.May = 0.ToString("#,##0.00");
                                    item_data.Jun = 0.ToString("#,##0.00");
                                    item_data.Jul = 0.ToString("#,##0.00");
                                    item_data.Aug = 0.ToString("#,##0.00");
                                    item_data.Sep = 0.ToString("#,##0.00");
                                    item_data.Oct = 0.ToString("#,##0.00");
                                    item_data.Nov = 0.ToString("#,##0.00");
                                    item_data.Dec = 0.ToString("#,##0.00");
                                    item_data.total_expenditures = 0.ToString("#,##0.00");
                                    item_data.total_budget = (Convert.ToDouble(item_data.allocation.ToString()) - 0).ToString("#,##0.00");
                                    item_data.total_balance = (Convert.ToDouble(item_data.allocation.ToString()) - 0).ToString("#,##0.00");

                                    item_data.allocation = Convert.ToDouble(item_data.allocation).ToString("#,##0.00");
                                }



                            }
                            ChiefData_FundSourceMainDetails x_space = new ChiefData_FundSourceMainDetails();

                            x_space.name = "-----------------------------------------";
                            x_space.fund_name = "------------";
                            x_space.allocation = "---------------------";
                            x_space.Jan = "------------";
                            x_space.Feb = "------------";
                            x_space.Mar = "------------";
                            x_space.Apr = "------------";
                            x_space.May = "------------";
                            x_space.Jun = "------------";
                            x_space.Jul = "------------";
                            x_space.Aug = "------------";
                            x_space.Sep = "------------";
                            x_space.Oct = "------------";
                            x_space.Nov = "------------";
                            x_space.Dec = "------------";
                            x_space.total_expenditures = "---------------------";
                            x_space.total_balance = "---------------------";
                            x_space.total_budget = "---------------------";
                            x_details.Add(x_space);

                            ChiefData_FundSourceMainDetails x_totals = new ChiefData_FundSourceMainDetails();


                            double _Allocation = 0.00;

                            double _JanTotal = 0.00;
                            double _FebTotals = 0.00;
                            double _MarTotals = 0.00;
                            double _AprTotals = 0.00;
                            double _MayTotals = 0.00;
                            double _JunTotals = 0.00;
                            double _JulTotals = 0.00;
                            double _AugTotals = 0.00;
                            double _SepTotals = 0.00;
                            double _OctTotals = 0.00;
                            double _NovTotals = 0.00;
                            double _DecTotals = 0.00;


                            double _total_expenditures = 0.00;
                            double _total_balance = 0.00;
                            double _total_budgets = 0.00;
                            x_totals.name = "Grand Total";

                            foreach (var item_x in x_details)
                            {
                                try
                                {

                                    if (item_x.Jan == null) { item_x.Jan = 0.ToString("#,##0.00"); }
                                    if (item_x.Feb == null) { item_x.Feb = 0.ToString("#,##0.00"); }
                                    if (item_x.Mar == null) { item_x.Mar = 0.ToString("#,##0.00"); }
                                    if (item_x.Apr == null) { item_x.Apr = 0.ToString("#,##0.00"); }
                                    if (item_x.May == null) { item_x.May = 0.ToString("#,##0.00"); }
                                    if (item_x.Jun == null) { item_x.Jun = 0.ToString("#,##0.00"); }
                                    if (item_x.Jul == null) { item_x.Jul = 0.ToString("#,##0.00"); }
                                    if (item_x.Aug == null) { item_x.Aug = 0.ToString("#,##0.00"); }
                                    if (item_x.Sep == null) { item_x.Sep = 0.ToString("#,##0.00"); }
                                    if (item_x.Oct == null) { item_x.Oct = 0.ToString("#,##0.00"); }
                                    if (item_x.Nov == null) { item_x.Nov = 0.ToString("#,##0.00"); }
                                    if (item_x.Dec == null) { item_x.Dec = 0.ToString("#,##0.00"); }
                                    if (item_x.total_expenditures == null) { item_x.total_expenditures = 0.ToString("#,##0.00"); }
                                    if (item_x.total_budget == null) { item_x.total_budget = 0.ToString("#,##0.00"); }
                                    if (item_x.total_balance == null) { item_x.total_balance = 0.ToString("#,##0.00"); }
                                 

                                    _Allocation += Convert.ToDouble(item_x.allocation);
                                    _JanTotal += Convert.ToDouble(item_x.Jan);
                                    _FebTotals += Convert.ToDouble(item_x.Feb);
                                    _MarTotals += Convert.ToDouble(item_x.Mar);
                                    _AprTotals += Convert.ToDouble(item_x.Apr);
                                    _MayTotals += Convert.ToDouble(item_x.May);
                                    _JunTotals += Convert.ToDouble(item_x.Jun);
                                    _JulTotals += Convert.ToDouble(item_x.Jul);
                                    _AugTotals += Convert.ToDouble(item_x.Aug);
                                    _SepTotals += Convert.ToDouble(item_x.Sep);
                                    _OctTotals += Convert.ToDouble(item_x.Oct);
                                    _NovTotals += Convert.ToDouble(item_x.Nov);
                                    _DecTotals += Convert.ToDouble(item_x.Dec);

                                    _total_expenditures += Convert.ToDouble(item_x.total_expenditures);
                                    _total_balance += Convert.ToDouble(item_x.total_balance);
                                    _total_budgets += Convert.ToDouble(item_x.total_budget);
                                }
                                catch (Exception)
                                {


                                }


                            }
                            x_totals.allocation = _Allocation.ToString("#,##0.00");
                            x_totals.Jan = _JanTotal.ToString("#,##0.00");
                            x_totals.Feb = _FebTotals.ToString("#,##0.00");
                            x_totals.Mar = _MarTotals.ToString("#,##0.00");
                            x_totals.Apr = _AprTotals.ToString("#,##0.00");
                            x_totals.May = _MayTotals.ToString("#,##0.00");
                            x_totals.Jun = _JunTotals.ToString("#,##0.00");
                            x_totals.Jul = _JulTotals.ToString("#,##0.00");
                            x_totals.Aug = _AugTotals.ToString("#,##0.00");
                            x_totals.Sep = _SepTotals.ToString("#,##0.00");
                            x_totals.Oct = _OctTotals.ToString("#,##0.00");
                            x_totals.Nov = _NovTotals.ToString("#,##0.00");
                            x_totals.Dec = _DecTotals.ToString("#,##0.00");
                            x_totals.total_expenditures = _total_expenditures.ToString("#,##0.00");
                            x_totals.total_balance = _total_balance.ToString("#,##0.00");
                            x_totals.total_budget = _total_budgets.ToString("#,##0.00");
                            x_details.Add(x_totals);

                            x_data.FundAllocation = x_details;

                            cd_FundSource.Add(x_data);
                        }
                    }
                    grdProjectDetails.ItemsSource = null;
                    grdProjectDetails.ItemsSource = cd_FundSource;

                    foreach (var item in grdProjectDetails.Rows)
                    {
                        if (item.HasChildren)
                        {
                            item.IsExpanded = true;
                            foreach (var item_c in item.ChildBands)
                            {
                                item_c.Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
                                item_c.Columns["fund_name"].Visibility = System.Windows.Visibility.Collapsed;
                                item_c.Columns["total_obligated"].Visibility = System.Windows.Visibility.Collapsed;

                                item_c.Columns["name"].HeaderText = "Expense Item";
                                item_c.Columns["allocation"].HeaderText = "Allocation";
                                item_c.Columns["total_expenditures"].HeaderText = "Total Plan";
                                item_c.Columns["total_balance"].HeaderText = "Contingency";

                                item_c.Columns["total_budget"].HeaderText = "Total Budget";
                            }
                        }
                    }
                    SetAlignment();

                    //   FetchFundSourceMain();

                    this.Cursor = Cursors.Arrow;
                    break;

            }
        }
        private void FetchExpenditureDetailsItems()
        {

            String OfficeId = "";

                OfficeId =this.DivisionID;
                c_approval.Process = "FetchExpenditureDetailsItemLists";
                svc_mindaf.ExecuteSQLAsync(c_approval.FetchDivisionExpenditureDetailsItemByDiv(OfficeId, WorkingYear,this.FundType));
           

        }
        private void FetchFundSourceMain()
        {
            String OfficeId = "";

                OfficeId = this.DivisionID;
                c_approval.Process = "FetchFundMain";
                svc_mindaf.ExecuteSQLAsync(c_approval.FetchFundSource(OfficeId,WorkingYear));
         
        }
        private void FetchExpenditureDetails()
        {
      

            String OfficeId = "";

                OfficeId =DivisionID;
                c_approval.Process = "FetchExpenditureDetails";

                svc_mindaf.ExecuteSQLAsync(c_approval.FetchDivisionExpenditureDetailsByDiv(OfficeId, WorkingYear,this.FundSource));
            
        }
        private void SetAlignment()
        {
            if (grdProjectDetails.Rows.Count != 0)
            {
                grdProjectDetails.Rows[0].ChildBands[0].Cells["Jan"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdProjectDetails.Rows[0].ChildBands[0].Cells["Feb"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdProjectDetails.Rows[0].ChildBands[0].Cells["Mar"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdProjectDetails.Rows[0].ChildBands[0].Cells["Apr"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdProjectDetails.Rows[0].ChildBands[0].Cells["May"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdProjectDetails.Rows[0].ChildBands[0].Cells["Jun"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdProjectDetails.Rows[0].ChildBands[0].Cells["Jul"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdProjectDetails.Rows[0].ChildBands[0].Cells["Aug"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdProjectDetails.Rows[0].ChildBands[0].Cells["Sep"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdProjectDetails.Rows[0].ChildBands[0].Cells["Oct"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdProjectDetails.Rows[0].ChildBands[0].Cells["Nov"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdProjectDetails.Rows[0].ChildBands[0].Cells["Dec"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdProjectDetails.Rows[0].ChildBands[0].Cells["allocation"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdProjectDetails.Rows[0].ChildBands[0].Cells["total_expenditures"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdProjectDetails.Rows[0].ChildBands[0].Cells["total_balance"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
                grdProjectDetails.Rows[0].ChildBands[0].Cells["total_budget"].Column.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Right;
            }

        }

        private void grdProjectDetails_CellClicked(object sender, CellClickedEventArgs e)
        {

        }

        private void usrApprovals_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void grdActivities_CellClicked(object sender, CellClickedEventArgs e)
        {

        }
        private void SendNotify()
        {

            String _message = "Request for Approval on the Following: <br\\> " + Environment.NewLine + "<br\\>Division : " + this.DivisionID + Environment.NewLine + "<br\\>Dated : " + DateTime.Now.ToShortDateString();

            c_approval.Process = "MailUser";
            svc_mindaf.SendMailAsync("info@minda.gov.ph", "sharon.santiago@minda.gov.ph", _message, "Request Budget Approval", "Budget Approval");

        }
        private void btnApproved_Click(object sender, RoutedEventArgs e)
        {
             MessageBoxResult res = MessageBox.Show("Confirm approve budget.", "", MessageBoxButton.OKCancel);
             if (res.ToString() == "OK")
             {
                 List<ApproveList> _app = new List<ApproveList>();
                 String _Status = "";
                 foreach (ChiefData_ItemLists item in ListItemDetails)
                 {
                     ApproveList x_data = new ApproveList();

                     x_data._activity_id = item.id;
                     x_data._activity_id_main = item.main_id;
                     x_data._output_id = item.mpo_id;
                     x_data._program_id = item.prog_id;
                     x_data._project_id = item.project_id;
                     x_data._value = "1";
                     if (item.is_revision =="1")
                     {
                         _Status = "FINANCE APPROVED";
                     }
                     else
                     {
                         _Status = "DEFAULT";
                     }
                     _app.Add(x_data);
                 }
                 UpdateActivity(_app, "1",_Status);

                 grdProjectDetails.ItemsSource = null;
                 MessageBox.Show("Budget has been approved successfully.");
                 SendNotify();
             }
           
       
        }
        private void UpdateActivity(List<ApproveList> _data, String _value,String Status)
        {
            c_approval.Process = "UpdateActivity";
            c_approval.UpdateActivity(_data, _value,Status);
        }
        private String FundType = "";
        private void cmbFundSource_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           List<ChiefData_FundSourceMain>  _selected = cd_FundSourceMain.Where(x=> x.fund_source == cmbFundSource.SelectedItem.ToString()).ToList();
            if (_selected.Count !=0)
	        {
                this.FundSource = _selected[0].id;
                this.FundType = _selected[0].fund_type;
                FetchExpenditureDetailsItems();
	        }

            
        }

        private void chkAll_Unchecked(object sender, RoutedEventArgs e)
        {

        }

        private void chkAll_Checked(object sender, RoutedEventArgs e)
        {

        }



    }

}
