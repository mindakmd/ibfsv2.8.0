﻿using MinDAF.Class;
using MinDAF.Forms;
using MinDAF.Usercontrol.Budget_Allocation;
using MinDAF.Usercontrol.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Browser;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Usercontrol
{
    public partial class usr_Menu_Approvals : UserControl
    {
        public String Division { get; set; }
        public String DivisionID { get; set; }
        public String Level { get; set; }
        public String User { get; set; }

        public String PaP { get; set; }

        public String FundSource { get; set; }
        public String WorkingYear { get; set; }

        private BudgetApprovals ctrl_budget_approvals = new BudgetApprovals("");
        private BudgetPPMPApproval ctrl_budget_ppmp = new BudgetPPMPApproval();
        private division_stage_status ctrl_div_stat = new division_stage_status();
        private BudgetApprovalFinance ctrl_budget_approvals_finance = new BudgetApprovalFinance();
        private usr_monthly_budget_allocation ctrl_report_mba = new usr_monthly_budget_allocation();
        private usr_mba_summary_mooe_divisions ctrl_report_mba_summary = new usr_mba_summary_mooe_divisions();
        private ppmp_report_user ctrl_report_budget_monthly = new ppmp_report_user();

        private clsReportCache c_cache = new clsReportCache();


        public usr_Menu_Approvals(String fundid)
        {
            InitializeComponent();
            FundSource = fundid;
            ctrl_budget_approvals = new BudgetApprovals(this.FundSource);
          
        }

        private void mnuReviewLists_Click(object sender, EventArgs e)
        {

            ctrl_budget_approvals_finance = new BudgetApprovalFinance();
            ctrl_budget_approvals_finance.Width = stkChild.Width;
            ctrl_budget_approvals_finance.Height = stkChild.Height;
            ctrl_budget_approvals_finance.Level = Level;
            ctrl_budget_approvals_finance.User = this.User;

            ctrl_budget_approvals.WorkingYear = this.WorkingYear;
            ctrl_budget_approvals.DivisionID = this.DivisionID;
            ctrl_budget_approvals_finance.DivisionID = this.DivisionID;

            stkChild.Children.Clear();
            if (DivisionID == "3" && Level =="1")
            {
                frmFinanceApproval fApp = new frmFinanceApproval();
                fApp._User = this.User;
                fApp.WorkingYear = this.WorkingYear;
                fApp.Show();
              //  stkChild.Children.Add(ctrl_budget_approvals);
            }
            else
            {
                stkChild.Children.Add(ctrl_budget_approvals);
            }
         
        }
        private void SecuredLevel()
        {
            ctrl_div_stat = new division_stage_status();
            this.stkChild.Children.Clear();
            switch (this.Level)
            {
                   
                case "1":
                    mnuManagement.Visibility = System.Windows.Visibility.Visible;
                    mnuBudgetAllocation.Visibility = System.Windows.Visibility.Visible;
                    mnuBudgetReport.Visibility = System.Windows.Visibility.Visible;
                    mnuRevisionApprovals.Visibility = System.Windows.Visibility.Visible;

                    ctrl_div_stat.WorkingYear = this.WorkingYear;
                    ctrl_div_stat.Width = stkChild.Width;
                    ctrl_div_stat.Height = stkChild.Height;

                    stkChild.Children.Add(ctrl_div_stat);
                    break;
                case "2":
                    mnuManagement.Visibility = System.Windows.Visibility.Collapsed;
                    mnuBudgetAllocation.Visibility = System.Windows.Visibility.Collapsed;
                    mnuBudgetReport.Visibility = System.Windows.Visibility.Collapsed;
                    mnuRevisionApprovals.Visibility = System.Windows.Visibility.Collapsed;
                    ctrl_div_stat.WorkingYear = this.WorkingYear;
                    ctrl_div_stat.Width = stkChild.Width;
                    ctrl_div_stat.Height = stkChild.Height;

                    stkChild.Children.Add(ctrl_div_stat);
                    break;             
            }
        }
        private void ctrlMenuApprovals_Loaded(object sender, RoutedEventArgs e)
        {
            lblDivision.Content = Division;
            lblUser.Content =  User;

            ctrl_budget_approvals.Width = stkChild.Width;
            ctrl_budget_approvals.Height = stkChild.Height;
            ctrl_budget_approvals_finance.Width = stkChild.Width;
            ctrl_budget_approvals_finance.Height = stkChild.Height;
            ctrl_report_mba.Width = stkChild.Width;
            ctrl_report_mba.Height = stkChild.Height;

            SecuredLevel();

        }

        private void mnuIBSConsolidated_Click(object sender, EventArgs e)
        {
            //SilverlightPrinter.PrintPreview pdfPrt = new SilverlightPrinter.PrintPreview();
            //pdfPrt.NewPage();
            //pdfPrt.PrtTxt(1, "DFGDFGDFGDFG");

            //ChildWindow PrintPreview = new ChildWindow();
            //PrintPreview.Content = pdfPrt;
            //PrintPreview.Width = pdfPrt.Width;
            //PrintPreview.Height = pdfPrt.Height;
            //PrintPreview.Title = "Print Preview";
            //PrintPreview.Show();
        }

        private void mnuPapOffice_Click(object sender, EventArgs e)
        {
            frmPAPManagementOffices frm_pap_office = new frmPAPManagementOffices();

            frm_pap_office.Show();
        }

       private frmYear f_year = new frmYear();
       private ppmp_report_finance ctrl_report_ppmp;
        private void mnuMonthlyActualObligation_Click(object sender, EventArgs e)
        {
            f_year = new frmYear();
            f_year.ProcessReport += f_year_ProcessReport;
            f_year.Process = "Division";
            f_year.Show();
            stkChild.Children.Clear();
        }

        void f_year_ProcessReport(object sender, EventArgs e)
        {
            ctrl_report_mba = new usr_monthly_budget_allocation();
            ctrl_report_mba.Width = stkChild.Width;
            ctrl_report_mba.Height = stkChild.Height;

            ctrl_report_mba.DivisionName = f_year.SelectedDivision;
            ctrl_report_mba.DivisionId = f_year.SelectedDivisionID;
            ctrl_report_mba.SelectedYear = f_year.SelectedYear;
            ctrl_report_mba.FundSource = f_year.SelectedFundSource;
            ctrl_report_mba.DivisionPAP = f_year.SelectedDivisionPAP;
            ctrl_report_mba.PaP = this.PaP;

            stkChild.Children.Clear();
            stkChild.Children.Add(ctrl_report_mba);
        }

        private void mnuBudgetAllocationProcess_Click(object sender, EventArgs e)
        {

            frmDivisionBudget frm_div_bud = new frmDivisionBudget();
            frm_div_bud.Division = f_year.SelectedDivision;
            frm_div_bud.DivisionID = f_year.SelectedDivisionID;

            frm_div_bud.Show();

            //f_year = new frmYear();
            //f_year.ProcessBudget += f_year_ProcessBudget;
            //f_year.lblCaption.Content = "Select Year and Division";
            //f_year.cmbYear.IsEnabled = false;
            //f_year.Process = "Division";
            //f_year.Show();
            //stkChild.Children.Clear();
        }

        private void mnuBudgetAllocation_Click(object sender, EventArgs e)
        {
           
        }

        void f_year_ProcessBudget(object sender, EventArgs e)
        {
            frmDivisionBudget frm_div_bud = new frmDivisionBudget();
            frm_div_bud.Division = f_year.SelectedDivision;
            frm_div_bud.DivisionID = f_year.SelectedDivisionID;
           
            frm_div_bud.Show();
        }

        private void mnuMOOESummary_Click(object sender, EventArgs e)
        {
            f_year = new frmYear();
            f_year.ProcessSummary += f_year_ProcessSummary;
            f_year.lblCaption.Content = "Select Year";
         
            f_year.Process = "SUBPAP";
            f_year.Show();
            stkChild.Children.Clear();
        }

        void f_year_ProcessSummary(object sender, EventArgs e)
        {
            ctrl_report_mba_summary = new usr_mba_summary_mooe_divisions();
            ctrl_report_mba_summary.Width = stkChild.Width;
            ctrl_report_mba_summary.Height = stkChild.Height;


            ctrl_report_mba_summary.DivisionName = f_year.SelectedDivision;
            ctrl_report_mba_summary.DivisionId = f_year.SelectedDivisionID;
            ctrl_report_mba_summary.SelectedYear = f_year.SelectedYear;

            stkChild.Children.Clear();
            stkChild.Children.Add(ctrl_report_mba_summary);
        }

        private void mnuBudgBal_Click(object sender, EventArgs e)
        {
            ctrl_report_budget_monthly = new ppmp_report_user();
            ctrl_report_budget_monthly.Width = stkChild.Width;
            ctrl_report_budget_monthly.Height = stkChild.Height;


            //ctrl_report_budget_monthly.DivisionName = f_year.SelectedDivision;
            //        ctrl_report_mba_summary.DivisionId = f_year.SelectedDivisionID;
            //        ctrl_report_mba_summary.SelectedYear = f_year.SelectedYear;

                    stkChild.Children.Clear();
                    stkChild.Children.Add(ctrl_report_budget_monthly);
        }

        private void mnuMonthlyActualObligationSummary_Click(object sender, EventArgs e)
        {
            f_year = new frmYear();
            f_year.ProcessMAOSummary += f_year_ProcessMAOSummary;
            f_year.Process = "SUMMARY";
            f_year.cmbFundSource.IsEnabled = false;
            f_year.Show();
            stkChild.Children.Clear();
        }

        void f_year_ProcessMAOSummary(object sender, EventArgs e)
        {
            ctrl_report_mba = new usr_monthly_budget_allocation();
            ctrl_report_mba.Width = stkChild.Width;
            ctrl_report_mba.Height = stkChild.Height;

            ctrl_report_mba.DivisionName = f_year.SelectedDivision;
            ctrl_report_mba.DivisionId = f_year.SelectedDivisionID;
            ctrl_report_mba.SelectedYear = f_year.SelectedYear;
            ctrl_report_mba.FundSource = f_year.SelectedFundSource;
            ctrl_report_mba.DivisionPAP = f_year.SelectedDivisionPAP;
            ctrl_report_mba.PaP = f_year.SelectedDivisionPAP;
            ctrl_report_mba.Process = "SUMMARY";
            stkChild.Children.Clear();
            stkChild.Children.Add(ctrl_report_mba);
        }

        private void mnuReviewppmp_Click(object sender, EventArgs e)
        {
            ctrl_budget_ppmp = new BudgetPPMPApproval();
            ctrl_budget_ppmp.Width = stkChild.Width;
            ctrl_budget_ppmp.Height = stkChild.Height;

            ctrl_budget_ppmp.DivisionId = f_year.SelectedDivisionID;
            ctrl_budget_ppmp.SelectedYear = f_year.SelectedYear;
            ctrl_budget_ppmp.PAPCode = this.PaP;

            stkChild.Children.Clear();
            stkChild.Children.Add(ctrl_budget_ppmp);
        }

        private void mnuPPMP_Click(object sender, EventArgs e)
        {
            ctrl_report_ppmp = new ppmp_report_finance();
            ctrl_report_ppmp.Width = stkChild.Width;
            ctrl_report_ppmp.Height = stkChild.Height;

            stkChild.Children.Clear();
            stkChild.Children.Add(ctrl_report_ppmp);
        }

        private void mnuRepApp_Click(object sender, EventArgs e)
        {
            HtmlPage.Window.Navigate(new Uri("http://mindafinance/help/report/ReportTool.application"), "_blank");
        }

        private void mnuBudgetOveriew_Click(object sender, EventArgs e)
        {
            frmConsolidatedBudgetView f_condo = new frmConsolidatedBudgetView();

            f_condo.Show();
        }

        private void mnuDivDist_Click(object sender, EventArgs e)
        {
            c_cache.Process = "Division Distribution";
            c_cache.CacheFDReport("Division Distribution", "n-a", "2017");
            c_cache.SQLOperation += c_cache_SQLOperation;
        }

        private bool rep_isloaded = false;
        void c_cache_SQLOperation(object sender, EventArgs e)
        {
            switch (c_cache.Process)
            {
                case "Division Distribution":
                    if (rep_isloaded==false)
                    {
                        HtmlPage.Window.Navigate(new Uri("http://mindafinance/help/report/ReportTool.application"), "_blank");
                        rep_isloaded = true;
                    }
                   
                    break;
            }
        }

        private void mnuRevisionApprovals_Click(object sender, EventArgs e)
        {
            frmRevisionApproval f_condo = new frmRevisionApproval("sad");
            f_condo.WorkingYear = this.WorkingYear;
            f_condo.Show();
        }

        private void mnuRafNo_Click(object sender, EventArgs e)
        {
            frmRAFMonitoring f_monitor = new frmRAFMonitoring();
            f_monitor.WorkingYear = this.WorkingYear;
            f_monitor.Show();

        }
    }
}
