﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using Infragistics.Controls.Grids;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using System.Text;
using MinDAF.Forms;

namespace MinDAF.Usercontrol.Report
{
    public partial class usr_division_project_status : UserControl
    {
        public String DivisionName { get; set; }
        public String SelectedYear { get; set; }
        public String DivisionId { get; set; }
        public String User { get; set; }
        public Int32 Level { get; set; }

        public String ServiceType { get; set; }


        private List<ReportMBA> ReportData = new List<ReportMBA>();
        private List<ReportProjectStatus> ListHeaderTemplate = new List<ReportProjectStatus>();
        private List<ReportMBADetail> ListHeaderDetail = new List<ReportMBADetail>();
        private List<ReportProjectStatus> ListTotals = new List<ReportProjectStatus>();
        private List<ReportDataMBA> ListReportData = new List<ReportDataMBA>();
        private List<FundSourceData> ListFundSourceData = new List<FundSourceData>();

        private List<ActivityStatus> ListActivityStatus = new List<ActivityStatus>();
        private List<AllocationExpenditure> ListAllocation = new List<AllocationExpenditure>();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsBudgetProgramStatus c_rep_mba = new clsBudgetProgramStatus();
        private String _MonthSelected = "";

        public usr_division_project_status()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            ServiceType = "502";
        }

        private void FetchBudgetRAFNo()
        {
            String MFType = ListFundSourceData.Where(x => x.Fund_Name == cmbFundSource.SelectedItem.ToString()).ToList()[0].fundtype;
           
            c_rep_mba.Process = "FetchBudgetRAFNo";
            svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchBudgetRafNo(this.DivisionId, this.SelectedYear, MFType));
        }
        private void FetchBudgetAdjustments()
        {
            String FSource = ListFundSourceData.Where(x => x.Fund_Name == cmbFundSource.SelectedItem.ToString()).ToList()[0].Fund_Source_Id;
           
            c_rep_mba.Process = "FetchBudgetAdjustments";
            svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchBudgetAdjustments(this.DivisionId, this.SelectedYear, FSource));
        }
        private List<DivisionRafNo> ListRafNo = new List<DivisionRafNo>();
        private List<DivisionAdjustment> ListAdjustments = new List<DivisionAdjustment>();
        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_rep_mba.Process)
            {
                case "FetchBudgetAdjustments":
                    XDocument oDocKeyResultsFetchBudgetAdjustments = XDocument.Parse(_results);
                    var _dataListsFetchBudgetAdjustments = from info in oDocKeyResultsFetchBudgetAdjustments.Descendants("Table")
                                                           select new DivisionAdjustment
                                                           {
                                                               Raf = Convert.ToString(info.Element("number").Value),
                                                               Mooe_Id = Convert.ToString(info.Element("mooe_id").Value),
                                                               Adjustments = Convert.ToString(info.Element("adjustment").Value),
                                                               PlusMinus = Convert.ToString(info.Element("plusminus").Value)

                                                           };

                    ListAdjustments.Clear();

                    foreach (var item in _dataListsFetchBudgetAdjustments)
                    {

                        DivisionAdjustment _varProf = new DivisionAdjustment();

                        _varProf.Adjustments = item.Adjustments;
                        _varProf.Mooe_Id = item.Mooe_Id;
                        _varProf.PlusMinus = item.PlusMinus;
                        _varProf.Raf = item.Raf;

                        ListAdjustments.Add(_varProf);

                    }

                    foreach (DivisionRafNo item in ListRafNo)
                    {
                        List<DivisionAdjustment> _filtered = ListAdjustments.Where(x => x.Raf == item.Raf).ToList();

                        foreach (AllocationExpenditure item_f in ListAllocation)
                        {
                            List<DivisionAdjustment> _selection = _filtered.Where(x => x.Mooe_Id == item_f.uacs_code).ToList();
                            if (_selection.Count != 0)
                            {
                                foreach (DivisionAdjustment item_s in _selection)
                                {
                                    double _adjustment = Convert.ToDouble(item_s.Adjustments);
                                    double _allocation = Convert.ToDouble(item_f.allocated_budget);
                                    double _new_allocation = 0.00;

                                    switch (item_s.PlusMinus)
                                    {
                                        case "+":

                                            _new_allocation = _allocation + _adjustment;
                                            break;

                                        case "-":
                                            _new_allocation = _allocation - _adjustment;
                                            break;

                                    }

                                    item_f.allocated_budget = _new_allocation.ToString("#,##0.00");
                                }
                            }
                        }

                    }



                    FetchExpenditureHeader();
                    this.Cursor = Cursors.Arrow;
                    

                    break;
                case "FetchBudgetRAFNo":
                    XDocument oDocKeyResultsFetchBudgetRAFNo = XDocument.Parse(_results);
                    var _dataListsFetchBudgetRAFNo = from info in oDocKeyResultsFetchBudgetRAFNo.Descendants("Table")
                                                     select new DivisionRafNo
                                                     {
                                                         Raf = Convert.ToString(info.Element("number").Value)

                                                     };

                    ListRafNo.Clear();

                    foreach (var item in _dataListsFetchBudgetRAFNo)
                    {

                        DivisionRafNo _varProf = new DivisionRafNo();

                        _varProf.Raf = item.Raf;


                        ListRafNo.Add(_varProf);

                    }


                    this.Cursor = Cursors.Arrow;
                    FetchBudgetAdjustments();
                    //  FetchActivityDataMonths();

                    break;
                case "FetchListAllocation":
                    XDocument oDocKeyResultsFetchListAllocation = XDocument.Parse(_results);
                    var _dataListsFetchListAllocation = from info in oDocKeyResultsFetchListAllocation.Descendants("Table")
                                     select new AllocationExpenditure
                                     {
                                         uacs_code = Convert.ToString(info.Element("uacs_code").Value),
                                         name = Convert.ToString(info.Element("name").Value),
                                         allocated_budget = Convert.ToString(info.Element("amount").Value)
                                     };


                    ListAllocation.Clear();

                    foreach (var item in _dataListsFetchListAllocation)
                    {
                        AllocationExpenditure _varProf = new AllocationExpenditure();

                        _varProf.uacs_code = item.uacs_code;
                        _varProf.name = item.name;
                        _varProf.allocated_budget = item.allocated_budget;


                        ListAllocation.Add(_varProf);

                    }

                    FetchBudgetRAFNo();
                    this.Cursor = Cursors.Arrow;

                    break;
                case "FetchExpenditureTemplate":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new ReportProjectStatus
                                     {
                                         Apr = Convert.ToString(info.Element("Apr").Value),
                                         Aug = Convert.ToString(info.Element("Aug").Value),
                                         Dec = Convert.ToString(info.Element("Dec").Value),
                                         Feb = Convert.ToString(info.Element("Feb").Value),
                                         id = Convert.ToString(info.Element("id").Value),
                                         Jan = Convert.ToString(info.Element("Jan").Value),
                                         Jul = Convert.ToString(info.Element("Jul").Value),
                                         Jun = Convert.ToString(info.Element("Jun").Value),
                                         Mar = Convert.ToString(info.Element("Mar").Value),
                                         May = Convert.ToString(info.Element("May").Value),
                                         Name = Convert.ToString(info.Element("Name").Value),
                                         Nov = Convert.ToString(info.Element("Nov").Value),
                                         Oct = Convert.ToString(info.Element("Oct").Value),
                                         Sep = Convert.ToString(info.Element("Sep").Value),
                                         Total = Convert.ToString(info.Element("Total").Value)
                                     };


                    ListHeaderTemplate.Clear();

                    foreach (var item in _dataLists)
                    {
                        ReportProjectStatus _varProf = new ReportProjectStatus();

                        _varProf.Apr = item.Apr;
                        _varProf.Aug = item.Aug;
                        _varProf.Dec = item.Dec;
                        _varProf.Feb = item.Feb;
                        _varProf.id = item.id;
                        _varProf.Jan = item.Jan;
                        _varProf.Jul = item.Jul;
                        _varProf.Jun = item.Jun;
                        _varProf.Mar = item.Mar;
                        _varProf.May = item.May;
                        _varProf.Name = item.Name;
                        _varProf.Nov = item.Nov;
                        _varProf.Oct = item.Oct;
                        _varProf.Sep = item.Sep;
                        _varProf.Total = item.Total;

                        ListHeaderTemplate.Add(_varProf);

                    }

                    FetchExpenditureDetail();
                    this.Cursor = Cursors.Arrow;

                    break;
                case "FetchExpenditureDetail":
                    XDocument oDocKeyResultsFetchExpenditureDetail = XDocument.Parse(_results);
                    var _dataListsFetchExpenditureDetail = from info in oDocKeyResultsFetchExpenditureDetail.Descendants("Table")
                                                           select new ReportMBADetail
                                                           {
                                                               Alloted = Convert.ToString(info.Element("Alloted").Value),
                                                               Apr = Convert.ToString(info.Element("Apr").Value),
                                                               Aug = Convert.ToString(info.Element("Aug").Value),
                                                               Balance = Convert.ToString(info.Element("Balance").Value),
                                                               Dec = Convert.ToString(info.Element("Dec").Value),
                                                               Feb = Convert.ToString(info.Element("Feb").Value),
                                                               id = Convert.ToString(info.Element("id").Value),
                                                               Jan = Convert.ToString(info.Element("Jan").Value),
                                                               Jul = Convert.ToString(info.Element("Jul").Value),
                                                               Jun = Convert.ToString(info.Element("Jun").Value),
                                                               Mar = Convert.ToString(info.Element("Mar").Value),
                                                               May = Convert.ToString(info.Element("May").Value),
                                                               Name = Convert.ToString(info.Element("Name").Value),
                                                               Nov = Convert.ToString(info.Element("Nov").Value),
                                                               Oct = Convert.ToString(info.Element("Oct").Value),
                                                               Sep = Convert.ToString(info.Element("Sep").Value),
                                                               Total = Convert.ToString(info.Element("Total").Value),
                                                               UACS = Convert.ToString(info.Element("uacs_code").Value)
                                                           };


                    ListHeaderDetail.Clear();

                    foreach (var item in _dataListsFetchExpenditureDetail)
                    {
                        ReportMBADetail _varProf = new ReportMBADetail();

                        _varProf.Alloted = item.Alloted;
                        _varProf.Apr = item.Apr;
                        _varProf.Aug = item.Aug;
                        _varProf.Balance = item.Balance;
                        _varProf.Dec = item.Dec;
                        _varProf.Feb = item.Feb;
                        _varProf.id = item.id;
                        _varProf.Jan = item.Jan;
                        _varProf.Jul = item.Jul;
                        _varProf.Jun = item.Jun;
                        _varProf.Mar = item.Mar;
                        _varProf.May = item.May;
                        _varProf.Name = item.Name;
                        _varProf.Nov = item.Nov;
                        _varProf.Oct = item.Oct;
                        _varProf.Sep = item.Sep;
                        _varProf.Total = item.Total;
                        _varProf.UACS = item.UACS;



                        try
                        {
                            if (item.UACS.Substring(0, 3) == ServiceType || item.UACS.Substring(0, 3) == "506")
                            {

                                ListHeaderDetail.Add(_varProf);
                            }
                        }
                        catch (Exception)
                        {


                            ListHeaderDetail.Add(_varProf);
                        }




                    }

                    FetchData();

                    this.Cursor = Cursors.Arrow;

                    break;
                case "FetchFundSource":
                    XDocument oDocKeyResultsFetchFundSource = XDocument.Parse(_results);
                    var _dataListsFetchFundSource = from info in oDocKeyResultsFetchFundSource.Descendants("Table")
                                                    select new FundSourceData
                                                    {
                                                        Amount = Convert.ToString(info.Element("Amount").Value),
                                                        details = Convert.ToString(info.Element("details").Value),
                                                        division_id = Convert.ToString(info.Element("division_id").Value),
                                                        Fund_Name = Convert.ToString(info.Element("Fund_Name").Value),
                                                        Fund_Source_Id = Convert.ToString(info.Element("Fund_Source_Id").Value),
                                                         fundtype = Convert.ToString(info.Element("fund_type").Value)
                                                    };


                    ListFundSourceData.Clear();
                    cmbFundSource.Items.Clear();
                    try
                    {
                        foreach (var item in _dataListsFetchFundSource)
                        {
                            FundSourceData _varProf = new FundSourceData();

                            _varProf.Amount = item.Amount;
                            _varProf.details = item.details;
                            _varProf.division_id = item.division_id;
                            _varProf.Fund_Name = item.Fund_Name;
                            _varProf.Fund_Source_Id = item.Fund_Source_Id;
                            _varProf.fundtype = item.fundtype;
                            cmbFundSource.Items.Add(item.Fund_Name);


                            ListFundSourceData.Add(_varProf);

                        }

                        this.Cursor = Cursors.Arrow;
                    }
                    catch (Exception)
                    {

                    }
                    cmbFundSource.SelectedIndex = 0;

                    FetchListAllocationData();

                    break;
                case "FetchData":
                    XDocument oDocKeyResultsFetchData = XDocument.Parse(_results);
                    var _dataListsFetchData = from info in oDocKeyResultsFetchData.Descendants("Table")
                                              select new ReportDataMBA
                                              {
                                                  id = Convert.ToString(info.Element("id").Value),
                                                  month = Convert.ToString(info.Element("month").Value),
                                                  total = Convert.ToString(info.Element("total").Value),
                                                  year = Convert.ToString(info.Element("year").Value),
                                                  name = Convert.ToString(info.Element("name").Value),
                                                  status = Convert.ToString(info.Element("is_suspended").Value)
                                              };


                    ListReportData.Clear();

                    foreach (var item in _dataListsFetchData)
                    {
                        ReportDataMBA _varProf = new ReportDataMBA();

                        _varProf.id = item.id;
                        _varProf.month = item.month;
                        _varProf.total = item.total;
                        _varProf.year = item.year;
                        _varProf.name = item.name;
                        _varProf.status = item.status;

                        ListReportData.Add(_varProf);

                    }

                    this.Cursor = Cursors.Arrow;

                    FetchAllocationData();
                    break;
                case "FetchAllocation":
                    XDocument oDocKeyResultsFetchAllocated = XDocument.Parse(_results);
                    var _dataListsFetchAllocated = from info in oDocKeyResultsFetchAllocated.Descendants("Table")
                                                   select new ActivityAllocated
                                                   {
                                                       AllocatedAmount = Convert.ToDouble(info.Element("total").Value)

                                                   };

                    double _Total = 0.00;

                    foreach (var item in _dataListsFetchAllocated)
                    {
                        _Total += item.AllocatedAmount;
                    }
                    txtTotalAllocation.Value = _Total;

                    this.Cursor = Cursors.Arrow;
                    FetchCumulative();
                    break;
                case "FetchCumulative":
                    XDocument oDocKeyResultsFetchCumulative = XDocument.Parse(_results);
                    var _dataListsFetchCumulative = from info in oDocKeyResultsFetchCumulative.Descendants("Table")
                                                    select new CumulativeAmount
                                                    {
                                                        Amount = Convert.ToDouble(info.Element("total").Value)

                                                    };

                    foreach (var item in _dataListsFetchCumulative)
                    {
                        txtComulative.Value = item.Amount;
                        break;
                    }
                    txtComulative.Value = 0.00;
                    this.Cursor = Cursors.Arrow;
                    GenerateTemplate();
                    break;

                case "FetchActivityStatus":
                    XDocument oDocKeyResultsFetchActivityStatus = XDocument.Parse(_results);
                    var _dataListsFetchActivityStatus = from info in oDocKeyResultsFetchActivityStatus.Descendants("Table")
                                                        select new ActivityStatus
                                                        {
                                                            uacs_code = Convert.ToString(info.Element("uacs_code").Value),
                                                            accountable_id = Convert.ToString(info.Element("accountable_id").Value),
                                                            activity_id = Convert.ToString(info.Element("activity_id").Value),
                                                            destination = Convert.ToString(info.Element("destination").Value),
                                                            end = String.Format("{0:M/d/yyyy}", Convert.ToString(info.Element("end").Value)),
                                                            name = Convert.ToString(info.Element("name").Value),
                                                            no_days = Convert.ToString(info.Element("no_days").Value),
                                                            no_staff = Convert.ToString(info.Element("no_staff").Value),
                                                            quantity = Convert.ToString(info.Element("quantity").Value),
                                                            rate = Convert.ToString(info.Element("rate").Value),
                                                            remarks = Convert.ToString(info.Element("remarks").Value),
                                                            start = String.Format("{0:M/d/yyyy}", Convert.ToString(info.Element("start").Value)),
                                                            status = Convert.ToString(info.Element("status").Value),
                                                            total = Convert.ToString(info.Element("total").Value),
                                                            type_service = Convert.ToString(info.Element("type_service").Value),
                                                            suspend_activity = false,
                                                            suspend_stats = Convert.ToInt32(info.Element("suspend_stats").Value),
                                                            submit_stats = Convert.ToInt32(info.Element("submit").Value)
                                                        };


                    ListActivityStatus.Clear();
                    

                  
                    foreach (var item in _dataListsFetchActivityStatus)
                    {
                        try
                        {
                            if (item.uacs_code.Substring(0, 3) == ServiceType || item.uacs_code.Substring(0, 3) == "506")
                            {
                                ActivityStatus _varProf = new ActivityStatus();
                                DateTime _dateStart = Convert.ToDateTime(item.start);
                                DateTime _dateEnd = Convert.ToDateTime(item.end);
                                _varProf.accountable_id = item.accountable_id;
                                _varProf.activity_id = item.activity_id;

                                _varProf.destination = item.destination;
                                if (_dateStart.Year < 2016)
                                {
                                    _varProf.start = "-";
                                    _varProf.end = "-";
                                }
                                else
                                {
                                    _varProf.start = _dateStart.ToShortDateString();
                                    _varProf.end = _dateEnd.ToShortDateString();
                                }

                                _varProf.name = item.name;
                                _varProf.no_days = item.no_days;
                                _varProf.no_staff = item.no_staff;
                                _varProf.quantity = item.quantity;
                                _varProf.rate = Convert.ToDouble(item.rate).ToString("#,##0.00");
                                _varProf.remarks = item.remarks;
                                _varProf.status = item.status;
                                _varProf.total = Convert.ToDouble(item.total).ToString("#,##0.00");
                                _varProf.type_service = item.type_service;


                                if (item.suspend_stats == 1)
                                {
                                    _varProf.suspend_activity = true;
                                }
                                else
                                {
                                    _varProf.suspend_activity = false;
                                }
                                if (item.submit_stats == 1)
                                {
                                    _varProf.submit = true;
                                }
                                else
                                {
                                    _varProf.submit = false;
                                }
                               
                                   ListActivityStatus.Add(_varProf);

                               
                               
                            }
                        }
                        catch (Exception)
                        {

                        }


                    }
                    double Total = 0;
                    foreach (ActivityStatus item in ListActivityStatus)
                    {

                        Total += Convert.ToDouble(item.total);
                    }

                    ActivityStatus _varProfLast = new ActivityStatus();

                    _varProfLast.activity_id = "";
                    _varProfLast.destination = "";
                    _varProfLast.start = "----------";
                    _varProfLast.end = "----------";
                    _varProfLast.name = "----------";
                    _varProfLast.no_days = "----------";
                    _varProfLast.no_staff = "----------";
                    _varProfLast.quantity = "----------";
                    _varProfLast.rate = "TOTAL";
                    _varProfLast.remarks = "----------";
                    _varProfLast.status = "----------";
                    _varProfLast.total = Total.ToString("#,##0.00");
                    _varProfLast.type_service = "-";
                    _varProfLast.suspend_activity = false;

                    ListActivityStatus.Add(_varProfLast);


                    grdStatus.ItemsSource = null;
                    grdStatus.ItemsSource = ListActivityStatus;
                    grdStatus.Columns["activity_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdStatus.Columns["suspend_activity"].Visibility = System.Windows.Visibility.Collapsed;
                    grdStatus.Columns["accountable_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdStatus.Columns["suspend_stats"].Visibility = System.Windows.Visibility.Collapsed;
                    grdStatus.Columns["submit_stats"].Visibility = System.Windows.Visibility.Collapsed;
                    this.Cursor = Cursors.Arrow;


                    break;
                case "FetchUpdateStatus":
                    XDocument oDocKeyResultsFetchUpdateStatus = XDocument.Parse(_results);
                    var _dataListsFetchUpdateStatus = from info in oDocKeyResultsFetchUpdateStatus.Descendants("Table")
                                                      select new ActivityStatus
                                                      {
                                                          accountable_id = Convert.ToString(info.Element("accountable_id").Value),
                                                          activity_id = Convert.ToString(info.Element("activity_id").Value),
                                                          destination = Convert.ToString(info.Element("destination").Value),
                                                          end = String.Format("{0:M/d/yyyy}", Convert.ToString(info.Element("end").Value)),
                                                          name = Convert.ToString(info.Element("name").Value),
                                                          no_days = Convert.ToString(info.Element("no_days").Value),
                                                          no_staff = Convert.ToString(info.Element("no_staff").Value),
                                                          quantity = Convert.ToString(info.Element("quantity").Value),
                                                          rate = Convert.ToString(info.Element("rate").Value),
                                                          remarks = Convert.ToString(info.Element("remarks").Value),
                                                          start = String.Format("{0:M/d/yyyy}", Convert.ToString(info.Element("start").Value)),
                                                          status = Convert.ToString(info.Element("status").Value),
                                                          total = Convert.ToString(info.Element("total").Value),
                                                          type_service = Convert.ToString(info.Element("type_service").Value),
                                                          suspend_activity = false,
                                                          suspend_stats = Convert.ToInt32(info.Element("suspend_stats").Value),
                                                          submit_stats = Convert.ToInt32(info.Element("submit").Value)
                                                      };


                    ListActivityStatus.Clear();

                    foreach (var item in _dataListsFetchUpdateStatus)
                    {
                        ActivityStatus _varProf = new ActivityStatus();
                        DateTime _dateStart = Convert.ToDateTime(item.start);
                        DateTime _dateEnd = Convert.ToDateTime(item.end);
                        _varProf.accountable_id = item.accountable_id;
                        _varProf.activity_id = item.activity_id;

                        _varProf.destination = item.destination;
                        if (_dateStart.Year < 2016)
                        {
                            _varProf.start = "-";
                            _varProf.end = "-";
                        }
                        else
                        {
                            _varProf.start = _dateStart.ToShortDateString();
                            _varProf.end = _dateEnd.ToShortDateString();
                        }

                        _varProf.name = item.name;
                        _varProf.no_days = item.no_days;
                        _varProf.no_staff = item.no_staff;
                        _varProf.quantity = item.quantity;
                        _varProf.rate = item.rate;
                        _varProf.remarks = item.remarks;
                        _varProf.status = item.status;
                        _varProf.total = Convert.ToDouble(item.total).ToString("#,##0.00");
                        _varProf.type_service = item.type_service;

                        if (item.suspend_stats == 1)
                        {
                            _varProf.suspend_activity = true;
                        }
                        else
                        {
                            _varProf.suspend_activity = false;
                        }
                        if (item.submit_stats == 1)
                        {
                            _varProf.submit = true;
                        }
                        else
                        {
                            _varProf.submit = false;
                        }

                        ListActivityStatus.Add(_varProf);

                    }
                    ReportData.Clear();
                    grdStatus.ItemsSource = null;
                    grdStatus.ItemsSource = ListActivityStatus;
                    grdStatus.Columns["activity_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdStatus.Columns["accountable_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdStatus.Columns["suspend_stats"].Visibility = System.Windows.Visibility.Collapsed;
                    grdStatus.Columns["submit_stats"].Visibility = System.Windows.Visibility.Collapsed;

                    FetchExpenditureHeader();
                    this.Cursor = Cursors.Arrow;

                    break;


            }
        }

      
        private void GenerateTemplate()
        {
            double _JanTotal = 0.00;
            double _FebTotal = 0.00;
            double _MarTotal = 0.00;
            double _AprTotal = 0.00;
            double _MayTotal = 0.00;
            double _JunTotal = 0.00;
            double _JulTotal = 0.00;
            double _AugTotal = 0.00;
            double _SepTotal = 0.00;
            double _OctTotal = 0.00;
            double _NovTotal = 0.00;
            double _DecTotal = 0.00;
            double _LineTotal = 0.00;
            double _AllTotal = 0.00;
            double _AllBalance = 0.00;
            double _LineContigency = 0.00;
            double _PlanTotal = 0.00;
            ReportData.Clear();
            foreach (var item in ListHeaderDetail)
            {
                List<AllocationExpenditure> x_data = ListAllocation.Where(_filter => _filter.name == item.Name).ToList();
                if (x_data.Count != 0)
                {
                    double _all = Convert.ToDouble(x_data[0].allocated_budget);
                    item.Alloted = _all.ToString("#,##0.00");
                }
            }
            foreach (var item in ListHeaderDetail)
            {

                try
                {

                    List<ReportDataMBA> x_data;

                    if (item.UACS.Substring(0, 3) == ServiceType || item.UACS.Substring(0, 3) == "506")
                    {
                        if (item.Name == "Other Supplies and Materials Expenses")
                        {

                        }

                        x_data = ListReportData.Where(_filter => _filter.name == item.Name).ToList();

                        if (x_data.Count!=0)
                        {
                            foreach (var item_data in x_data)
                            {

                                switch (item_data.month)
                                {
                                    case "Jan":
                                        double _valJan = Convert.ToDouble(item_data.total);
                                        _JanTotal += Convert.ToDouble(item_data.total);
                                        if (item_data.status == "1")
                                        {
                                            _JanTotal -= _valJan;
                                            _valJan = 0;

                                        }
                                        else
                                        {
                                            if (Convert.ToDouble(item.Jan) < 0)
                                            {
                                                item.Jan = 0.ToString("#,##0.00");
                                            }


                                        }
                                        item.Jan = (Convert.ToDouble(item.Jan) + _valJan).ToString("#,##0.00");
                                        break;
                                    case "Feb":
                                        double _valFeb = Convert.ToDouble(item_data.total);
                                        _FebTotal += Convert.ToDouble(item_data.total);
                                        if (item_data.status == "1")
                                        {
                                            _FebTotal -= _valFeb;
                                            _valFeb = 0;
                                        }
                                        else
                                        {
                                            if (Convert.ToDouble(item.Feb) < 0)
                                            {
                                                item.Feb = 0.ToString("#,##0.00");
                                            }

                                        }
                                        item.Feb = (Convert.ToDouble(item.Feb) + _valFeb).ToString("#,##0.00");
                                        break;
                                    case "Mar":
                                        double _valMar = Convert.ToDouble(item_data.total);
                                        _MarTotal += Convert.ToDouble(item_data.total);
                                        if (item_data.status == "1")
                                        {
                                            _MarTotal -= _valMar;
                                            _valMar = 0;
                                        }
                                        else
                                        {
                                            if (Convert.ToDouble(item.Mar) < 0)
                                            {
                                                item.Mar = 0.ToString("#,##0.00");
                                            }
                                        }
                                        item.Mar = (Convert.ToDouble(item.Mar) + _valMar).ToString("#,##0.00");
                                        break;
                                    case "Apr":
                                        double _valApr = Convert.ToDouble(item_data.total);
                                        _AprTotal += Convert.ToDouble(item_data.total);
                                        if (item_data.status == "1")
                                        {
                                            _AprTotal -= _valApr;
                                            _valApr = 0;
                                        }
                                        else
                                        {
                                            if (Convert.ToDouble(item.Apr) < 0)
                                            {
                                                item.Apr = 0.ToString("#,##0.00");
                                            }
                                        }
                                        item.Apr = (Convert.ToDouble(item.Apr) + _valApr).ToString("#,##0.00");
                                        break;
                                    case "May":
                                        double _valMay = Convert.ToDouble(item_data.total);
                                        _MayTotal += Convert.ToDouble(item_data.total);
                                        if (item_data.status == "1")
                                        {
                                            _MayTotal -= _valMay;
                                            _valMay = 0;
                                        }
                                        else
                                        {
                                            if (Convert.ToDouble(item.May) < 0)
                                            {
                                                item.May = 0.ToString("#,##0.00");
                                            }
                                        }
                                        item.May = (Convert.ToDouble(item.May) + _valMay).ToString("#,##0.00");
                                        break;
                                    case "Jun":
                                        double _valJun = Convert.ToDouble(item_data.total);
                                        _JunTotal += Convert.ToDouble(item_data.total);
                                        if (item_data.status == "1")
                                        {
                                            _JunTotal -= _valJun;
                                            _valJun = 0;
                                        }
                                        else
                                        {
                                            if (Convert.ToDouble(item.Jun) < 0)
                                            {
                                                item.Jun = 0.ToString("#,##0.00");
                                            }
                                        }
                                        item.Jun = (Convert.ToDouble(item.Jun) + _valJun).ToString("#,##0.00");
                                        break;
                                    case "Jul":
                                        double _valJul = Convert.ToDouble(item_data.total);
                                        _JulTotal += Convert.ToDouble(item_data.total);
                                        if (item_data.status == "1")
                                        {
                                            _JulTotal -= _valJul;
                                            _valJul = 0;
                                        }
                                        else
                                        {
                                            if (Convert.ToDouble(item.Jul) < 0)
                                            {
                                                item.Jul = 0.ToString("#,##0.00");
                                            }
                                        }
                                        item.Jul = (Convert.ToDouble(item.Jul) + _valJul).ToString("#,##0.00");
                                        break;

                                    case "Aug":
                                        double _valAug = Convert.ToDouble(item_data.total);
                                        _AugTotal += Convert.ToDouble(item_data.total);
                                        if (item_data.status == "1")
                                        {
                                            _AugTotal -= _valAug;
                                            _valAug = 0;
                                        }
                                        else
                                        {
                                            if (Convert.ToDouble(item.Aug) < 0)
                                            {
                                                item.Aug = 0.ToString("#,##0.00");
                                            }
                                        }
                                        item.Aug = (Convert.ToDouble(item.Aug) + _valAug).ToString("#,##0.00");
                                        break;
                                    case "Sep":
                                        double _valSep = Convert.ToDouble(item_data.total);
                                        _SepTotal += Convert.ToDouble(item_data.total);
                                        if (item_data.status == "1")
                                        {
                                            _SepTotal -= _valSep;
                                            _valSep = 0;
                                        }
                                        else
                                        {
                                            if (Convert.ToDouble(item.Sep) < 0)
                                            {
                                                item.Sep = 0.ToString("#,##0.00");
                                            }
                                        }
                                        item.Sep = (Convert.ToDouble(item.Sep) + _valSep).ToString("#,##0.00");
                                        break;
                                    case "Oct":
                                        double _valOct = Convert.ToDouble(item_data.total);
                                        _OctTotal += Convert.ToDouble(item_data.total);
                                        if (item_data.status == "1")
                                        {
                                            _OctTotal -= _valOct;
                                            _valOct = 0;
                                        }
                                        else
                                        {
                                            if (Convert.ToDouble(item.Oct) < 0)
                                            {
                                                item.Oct = 0.ToString("#,##0.00");
                                            }
                                        }
                                        item.Oct = (Convert.ToDouble(item.Oct) + _valOct).ToString("#,##0.00");
                                        break;
                                    case "Nov":
                                        double _valNov = Convert.ToDouble(item_data.total);
                                        _NovTotal += Convert.ToDouble(item_data.total);
                                        if (item_data.status == "1")
                                        {
                                            _NovTotal -= _valNov;
                                            _valNov = 0;
                                        }
                                        else
                                        {
                                            if (Convert.ToDouble(item.Nov) < 0)
                                            {
                                                item.Nov = 0.ToString("#,##0.00");
                                            }
                                        }
                                        item.Nov = (Convert.ToDouble(item.Nov) + _valNov).ToString("#,##0.00");
                                        break;
                                    case "Dec":
                                        double _valDec = Convert.ToDouble(item_data.total);
                                        _DecTotal += Convert.ToDouble(item_data.total);
                                        if (item_data.status == "1")
                                        {
                                            _DecTotal -= _valDec;
                                            _valDec = 0;
                                        }
                                        else
                                        {
                                            if (Convert.ToDouble(item.Dec) < 0)
                                            {
                                                item.Dec = 0.ToString("#,##0.00");
                                            }
                                        }
                                        item.Dec = (Convert.ToDouble(item.Dec) + _valDec).ToString("#,##0.00");
                                        break;

                                }

                            }
                        }
                        else
                        {
                            item.Total = Convert.ToDouble(item.Alloted).ToString("#,##0.00");
                        }
                       
                    }
                }
                catch (Exception)
                {


                }
            }
            //Computer For Total and Balance
            foreach (var item in ListHeaderDetail)
            {
                try
                {
                    if (item.UACS.Substring(0, 3) == ServiceType || item.UACS.Substring(0, 3) == "506")
                    {
                        double _lineTotal = Convert.ToDouble(item.Jan) + Convert.ToDouble(item.Feb) + Convert.ToDouble(item.May) + Convert.ToDouble(item.Mar) + Convert.ToDouble(item.Apr) +
                                      Convert.ToDouble(item.Jun) + Convert.ToDouble(item.Jul) + Convert.ToDouble(item.Aug) + Convert.ToDouble(item.Sep) + Convert.ToDouble(item.Oct) +
                                      Convert.ToDouble(item.Nov) + Convert.ToDouble(item.Dec);
                        double _lineAllotted = Convert.ToDouble(item.Alloted);

                        double _lContigency =  _lineAllotted - _lineTotal;
                        double _lPlan = _lContigency + _lineTotal;
                        item.Total = _lineTotal.ToString("#,##0.00");
                        _LineTotal += _lineTotal;
                        _LineContigency += _lContigency;
                        _PlanTotal += _lPlan;
                       

                        if (_lineTotal > 0)
                        {
                            if (_lineAllotted > 0)
                            {
                                _AllTotal += _lineAllotted;
                            }
                        }
                    }
                }
                catch (Exception)
                {

                }



            }

            // ReportProjectStatus _Totals = new ReportProjectStatus();
            List<ReportMBA> TotalData = new List<ReportMBA>();
            ReportMBA _headdata = new ReportMBA();
            _headdata.Alloted = "";
            _headdata.Apr = _AprTotal.ToString("#,##0.00");
            _headdata.Aug = _AugTotal.ToString("#,##0.00");
            _headdata.Balance = _LineContigency.ToString("#,##0.00");
            _headdata.Dec = _DecTotal.ToString("#,##0.00");
            _headdata.Feb = _FebTotal.ToString("#,##0.00");
            _headdata.Jan = _JanTotal.ToString("#,##0.00");
            _headdata.Jul = _JulTotal.ToString("#,##0.00");
            _headdata.Jun = _JunTotal.ToString("#,##0.00");
            _headdata.Mar = _MarTotal.ToString("#,##0.00");
            _headdata.May = _MayTotal.ToString("#,##0.00");
            _headdata.Nov = _NovTotal.ToString("#,##0.00");
            _headdata.Oct = _OctTotal.ToString("#,##0.00");
            _headdata.Sep = _SepTotal.ToString("#,##0.00");
            _headdata.Total = _LineTotal.ToString("#,##0.00");
            _headdata.TotalBudget = _PlanTotal.ToString("#,##0.00");
            _headdata.ReportHeader = "Total";

            ReportMBADetail _Totals = new ReportMBADetail();
            List<ReportMBADetail> _TotalLists = new List<ReportMBADetail>();

            _Totals.Name = "Totals";
            _Totals.Jan = _JanTotal.ToString("#,##0.00");
            _Totals.Feb = _FebTotal.ToString("#,##0.00");
            _Totals.Mar = _MarTotal.ToString("#,##0.00");
            _Totals.Apr = _AprTotal.ToString("#,##0.00");
            _Totals.May = _MayTotal.ToString("#,##0.00");
            _Totals.Jun = _JunTotal.ToString("#,##0.00");
            _Totals.Jul = _JulTotal.ToString("#,##0.00");
            _Totals.Aug = _AugTotal.ToString("#,##0.00");
            _Totals.Sep = _SepTotal.ToString("#,##0.00");
            _Totals.Oct = _OctTotal.ToString("#,##0.00");
            _Totals.Nov = _NovTotal.ToString("#,##0.00");
            _Totals.Dec = _DecTotal.ToString("#,##0.00");
            _Totals.Total = _LineTotal.ToString("#,##0.00");

            _TotalLists.Add(_Totals);

           // _headdata.ReportDetail = _TotalLists;
            _headdata.ReportHeader = "Total";

            txtBudgetAmount.Value = _LineTotal;

            TotalData.Add(_headdata);
            _LineContigency = 0;
            _PlanTotal = 0;
            foreach (var item in ListHeaderTemplate)
            {
                ReportMBA _final = new ReportMBA();

                if (item.Name == "Other Supplies and Materials Expenses")
                {

                }
                List<ReportMBADetail> _data = ListHeaderDetail.Where(itemDetail => itemDetail.id == item.id).ToList();
                List<ReportMBADetail> _dataFinal = _data.Where(itemDetail => itemDetail.Total != "0.00" || itemDetail.Alloted !="0.00").ToList();

                foreach (var item_data in _dataFinal)
                {
                    if (item_data.Total != "0.00" || item_data.Alloted !="0.00")
                    {
                        if (item_data.UACS.Substring(0, 3) == ServiceType || item_data.UACS.Substring(0, 3) == "506")
                        {
                            _final.ReportHeader = item.Name;

                         
                            ReportData.Add(_final);

                            foreach (var itemFinal in _dataFinal)
                            {
                                ReportMBA _final_detail = new ReportMBA();

                                _final_detail.Alloted = itemFinal.Alloted;
                                _final_detail.Apr = itemFinal.Apr;
                                _final_detail.Aug = itemFinal.Aug;
                                _final_detail.Balance = (Convert.ToDouble(itemFinal.Alloted) - Convert.ToDouble(itemFinal.Total)).ToString("#,##0.00");
                                _LineContigency += Convert.ToDouble(_final_detail.Balance);

                                _final_detail.TotalBudget = (Convert.ToDouble(_final_detail.Balance) + Convert.ToDouble(itemFinal.Total)).ToString("#,##0.00");
                                _PlanTotal += Convert.ToDouble(_final_detail.TotalBudget );
                               
                                _final_detail.Dec = itemFinal.Dec;
                                _final_detail.Feb = itemFinal.Feb;
                                _final_detail.Jan = itemFinal.Jan;
                                _final_detail.Jul = itemFinal.Jul;
                                _final_detail.Jun = itemFinal.Jun;
                                _final_detail.Mar = itemFinal.Mar;
                                _final_detail.May = itemFinal.May;
                                _final_detail.Nov = itemFinal.Nov;
                                _final_detail.Oct = itemFinal.Oct;
                                _final_detail.ReportHeader = "      " + itemFinal.Name;
                                _final_detail.Sep = itemFinal.Sep;
                                _final_detail.Total = itemFinal.Total;
                                _final_detail.UACS = itemFinal.UACS;

                                ReportData.Add(_final_detail);

                            }
                          //  _final.ReportDetail = _dataFinal;
                            break;
                        }

                    }
                    //else
                    //{
                    //    if (item_data.Alloted !="0.00")
                    //    {
                    //        if (item_data.UACS.Substring(0, 3) == ServiceType || item_data.UACS.Substring(0, 3) == "506")
                    //            {
                    //                _final.ReportHeader = item.Name;

                         
                    //                ReportData.Add(_final);

                    //                foreach (var itemFinal in _dataFinal)
                    //                {
                    //                    ReportMBA _final_detail = new ReportMBA();

                    //                    _final_detail.Alloted = itemFinal.Alloted;
                    //                    _final_detail.Apr = itemFinal.Apr;
                    //                    _final_detail.Aug = itemFinal.Aug;
                    //                    _final_detail.Balance = (Convert.ToDouble(itemFinal.Alloted) - Convert.ToDouble(itemFinal.Total)).ToString("#,##0.00");
                    //                    _LineContigency += Convert.ToDouble(_final_detail.Balance);

                    //                    _final_detail.TotalBudget = (Convert.ToDouble(_final_detail.Balance) + Convert.ToDouble(itemFinal.Total)).ToString("#,##0.00");
                    //                    _PlanTotal += Convert.ToDouble(_final_detail.TotalBudget );
                               
                    //                    _final_detail.Dec = itemFinal.Dec;
                    //                    _final_detail.Feb = itemFinal.Feb;
                    //                    _final_detail.Jan = itemFinal.Jan;
                    //                    _final_detail.Jul = itemFinal.Jul;
                    //                    _final_detail.Jun = itemFinal.Jun;
                    //                    _final_detail.Mar = itemFinal.Mar;
                    //                    _final_detail.May = itemFinal.May;
                    //                    _final_detail.Nov = itemFinal.Nov;
                    //                    _final_detail.Oct = itemFinal.Oct;
                    //                    _final_detail.ReportHeader = "      " + itemFinal.Name;
                    //                    _final_detail.Sep = itemFinal.Sep;
                    //                    _final_detail.Total = itemFinal.Total;
                    //                    _final_detail.UACS = itemFinal.UACS;

                    //                    ReportData.Add(_final_detail);

                    //                }
                               
                    //                break;
                    //            }
                    //    }
                    //}
                }



            }

            _headdata.Balance = _LineContigency.ToString("#,##0.00");

            _headdata.TotalBudget = _PlanTotal.ToString("#,##0.00");

            ReportMBA _lines= new ReportMBA();
            _lines.Alloted = "--------------";
            _lines.Apr = "--------------";
            _lines.Aug = "--------------";
            _lines.Balance = "--------------";
            _lines.Dec = "--------------";
            _lines.Feb = "--------------";
            _lines.Jan = "--------------";
            _lines.Jul = "--------------";
            _lines.Jun = "--------------";
            _lines.Mar = "--------------";
            _lines.May = "--------------";
            _lines.Nov = "--------------";
            _lines.Oct = "--------------";
            _lines.Sep = "--------------";
            _lines.Total = "--------------";
            _lines.UACS = "--------------";
            _lines.TotalBudget = "--------------";
   
            _lines.ReportHeader = "---------------------------------------------------------";

            ReportData.Add(_lines);

            ReportData.Add(_headdata);

            grdData.ItemsSource = null;
            grdData.ItemsSource = ReportData;
            grdData.Columns["Alloted"].Visibility = System.Windows.Visibility.Collapsed;
            grdData.Columns["Balance"].HeaderText = "Contigency";

            grdTotals.Visibility = System.Windows.Visibility.Collapsed;
            grdTotals.ItemsSource = null;
            grdTotals.ItemsSource = TotalData;
            grdTotals.Columns["Alloted"].Visibility = System.Windows.Visibility.Collapsed;
           // grdTotals.Columns["Balance"].Visibility = System.Windows.Visibility.Collapsed;

            // grdTotals.Columns["activity_id"].Visibility = System.Windows.Visibility.Collapsed;

            //grdTotals.Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
            //  grdTotals.Columns["UACS"].Visibility = System.Windows.Visibility.Collapsed;
            //grdTotals.Columns["Alloted"].Visibility = System.Windows.Visibility.Collapsed;
            //grdTotals.Columns["Balance"].Visibility = System.Windows.Visibility.Collapsed;
            Column col_project_name = grdData.Columns.DataColumns["ReportHeader"];
            col_project_name.Width = new ColumnWidth(250, false);

            //Column col_project_name_t = grdTotals.Columns.DataColumns["ReportHeader"];
            //col_project_name_t.Width = new ColumnWidth(300, false);

            foreach (var item in grdData.Rows)
            {
                if (item.HasChildren)
                {
                    item.IsExpanded = true;
                    item.ChildBands[0].ColumnLayout.HeaderVisibility = Visibility.Collapsed;
                    item.ChildBands[0].Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
                    item.ChildBands[0].Columns["Alloted"].Visibility = System.Windows.Visibility.Collapsed;
                    item.ChildBands[0].Columns["Balance"].Visibility = System.Windows.Visibility.Collapsed;
                    item.ChildBands[0].Columns["TypeService"].Visibility = System.Windows.Visibility.Collapsed;
                    foreach (var child in item.ChildBands)
                    {
                        Column col_project_name_child = child.Columns.DataColumns["Name"];
                        col_project_name_child.Width = new ColumnWidth(250, false);

                    }
                }
            }

            foreach (var item in grdStatus.Rows)
            {
                if (item.HasChildren)
                {
                    item.IsExpanded = true;
                    item.ChildBands[0].ColumnLayout.HeaderVisibility = Visibility.Collapsed;
                    item.ChildBands[0].Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
                    item.ChildBands[0].Columns["Alloted"].Visibility = System.Windows.Visibility.Collapsed;
                    item.ChildBands[0].Columns["Balance"].Visibility = System.Windows.Visibility.Collapsed;
                    foreach (var child in item.ChildBands)
                    {
                        Column col_project_name_child = child.Columns.DataColumns["Name"];
                        col_project_name_child.Width = new ColumnWidth(300, false);

                    }
                }
            }

            txtRemainingBalance.Value = Convert.ToDouble(txtTotalAllocation.Value) - (Convert.ToDouble(txtBudgetAmount.Value) + Convert.ToDouble(txtComulative.Value));
            //   grdTotals.Columns["activity_id"].Visibility = System.Windows.Visibility.Collapsed;
        }


        void svc_mindaf_ExecuteImportDataSQLCompleted(object sender, ExecuteImportDataSQLCompletedEventArgs e)
        {
            throw new NotImplementedException();
        }
        private void FetchExpenditureHeader()
        {
            c_rep_mba.Process = "FetchExpenditureTemplate";
            svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchExpenditureTemplate());
        }
        private void FetchAllocationData()
        {
            var x_data = ListFundSourceData.Where(item => item.Fund_Name == cmbFundSource.SelectedItem.ToString()).ToList();

            string fund_source_id = "";

            if (x_data.Count != 0)
            {
                fund_source_id = x_data[0].Fund_Source_Id.ToString();
            }
            if (fund_source_id != "")
            {
                c_rep_mba.Process = "FetchAllocation";
                svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchAllocation(this.DivisionId, cmbYear.SelectedItem.ToString(), fund_source_id));
            }

        }
        private void FetchListAllocationData()
        {
           
            var x_data = ListFundSourceData.Where(item => item.Fund_Name == cmbFundSource.SelectedItem.ToString()).ToList();

            string fund_source_id = "";

            if (x_data.Count != 0)
            {
                fund_source_id = x_data[0].Fund_Source_Id.ToString();
            }
            if (fund_source_id != "")
            {
                c_rep_mba.Process = "FetchListAllocation";
                svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchListAllocation(this.DivisionId, cmbYear.SelectedItem.ToString(), fund_source_id));
            }

        }

        private void LoadStatus() 
        {
            cmbType.Items.Clear();
            cmbType.Items.Add("Finance Approved");
            cmbType.Items.Add("Submitted");
            cmbType.Items.Add("For Approval");
            cmbType.Items.Add("Suspended");
            cmbType.SelectedIndex = 0;
        }
        private void FetchActivityStatus(String _year, String _month, String _user, String fund_source,String Status)
        {
            c_rep_mba.Process = "FetchActivityStatus";
        
            svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchActivityStatusUser(_year, _MonthSelected, this.User, fund_source,Status));


        }
        private Boolean _isStayLine = false;

        private void FetchUpdateStatus(String _year, String _month)
        {
            c_rep_mba.Process = "FetchUpdateStatus";
            svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchActivityStatus(_year, _MonthSelected));
        }
        private void FetchAllocation()
        {
            c_rep_mba.Process = "FetchAllocation";
            svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchAllocationExpenditure(this.DivisionId, this.SelectedYear));
        }
        private void FetchFundSource()
        {
            c_rep_mba.Process = "FetchFundSource";
            svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchFundSource(this.DivisionId, this.SelectedYear));
        }
        private void FetchCumulative()
        {
            c_rep_mba.Process = "FetchCumulative";
            this.User = this.User.Replace("Welcome ", "");
            svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchCumulative(this.DivisionId, this.SelectedYear, this.User));
        }
        private void FetchExpenditureDetail()
        {
            c_rep_mba.Process = "FetchExpenditureDetail";
            svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchExpenditureDetailTemplate());
        }
        private void FetchData()
        {
            if (cmbFundSource.SelectedItem == null)
            {
                cmbFundSource.SelectedIndex = 0;
            }
            var x_data = ListFundSourceData.Where(item => item.Fund_Name == cmbFundSource.SelectedItem.ToString()).ToList();

            string fund_source_id = "";

            if (x_data.Count != 0)
            {
                fund_source_id = x_data[0].Fund_Source_Id.ToString();
            }

            c_rep_mba.Process = "FetchData";
            this.User = this.User.Replace("Welcome ", "");

            switch (cmbType.SelectedItem.ToString())
            {
                case "Finance Approved":
                    svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchDataMBA(this.SelectedYear, this.DivisionId, this.User, fund_source_id, false,"FINANCE APPROVED"));

                    break;
                case "Submitted":
                    svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchDataMBA(this.SelectedYear, this.DivisionId, this.User, fund_source_id, false,"Submitted"));
                    break;
                case "For Approval":
                    svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchDataMBA(this.SelectedYear, this.DivisionId, this.User, fund_source_id, false,"For Approval"));
                    break;
                case "Suspended":
                    svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchDataMBA(this.SelectedYear, this.DivisionId, this.User, fund_source_id, false,"SUSPENDED-REVISION"));
                    break;
                default:
                    break;
            }

            
        }
        private void GenerateYear()
        {
            int _year = DateTime.Now.Year;
            int _limit = _year + 20;
            _year -= 1;
            for (int i = _year; i != _limit; i++)
            {
                cmbYear.Items.Add(_year);
                _year += 1;
            }
            cmbYear.SelectedIndex = 0;
            this.SelectedYear = cmbYear.SelectedItem.ToString();
        }
        private void usr_b_p_status_Loaded(object sender, RoutedEventArgs e)
        {
            GenerateYear();
            FetchFundSource();
            LoadStatus();

        }

        private void cmbYear_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                grdData.ItemsSource = null;
                grdTotals.ItemsSource = null;
                ReportData.Clear();
                this.SelectedYear = cmbYear.SelectedItem.ToString();
                FetchFundSource();
               // FetchExpenditureHeader();
            }
            catch (Exception)
            {

            }

        }
        object _activeCell = null;
        private void grdData_CellClicked(object sender, CellClickedEventArgs e)
        {
            if (e.Cell.Column.HeaderText.ToString() == "Jan" ||
               e.Cell.Column.HeaderText.ToString() == "Feb" ||
               e.Cell.Column.HeaderText.ToString() == "Mar" ||
               e.Cell.Column.HeaderText.ToString() == "Apr" ||
               e.Cell.Column.HeaderText.ToString() == "May" ||
               e.Cell.Column.HeaderText.ToString() == "Jun" ||
               e.Cell.Column.HeaderText.ToString() == "Jul" ||
               e.Cell.Column.HeaderText.ToString() == "Aug" ||
               e.Cell.Column.HeaderText.ToString() == "Sep" ||
               e.Cell.Column.HeaderText.ToString() == "Oct" ||
               e.Cell.Column.HeaderText.ToString() == "Nov" ||
               e.Cell.Column.HeaderText.ToString() == "Dec"
           )
            {

                _MonthSelected = e.Cell.Column.HeaderText.ToString();

                List<FundSourceData> x = ListFundSourceData.Where(items => items.Fund_Name == cmbFundSource.SelectedItem.ToString()).ToList();
                String _fundid = "";

                if (x.Count != 0)
                {
                    _fundid = x[0].Fund_Source_Id;
                }
                switch (cmbType.SelectedItem.ToString())
                {
                    case "Finance Approved":
                        btnSubmit.IsEnabled = false;
                        btnCheckAll.IsEnabled = false;
                        FetchActivityStatus(cmbYear.SelectedItem.ToString(), _MonthSelected, this.User, _fundid, "FINANCE APPROVED");

                        break;
                    case "Submitted":
                        btnSubmit.IsEnabled = false;
                        btnCheckAll.IsEnabled = false;
                        FetchActivityStatus(cmbYear.SelectedItem.ToString(), _MonthSelected, this.User, _fundid, "Submitted");
                        break;
                    case "For Approval":
                        btnSubmit.IsEnabled = true;
                        btnCheckAll.IsEnabled = true;
                        FetchActivityStatus(cmbYear.SelectedItem.ToString(), _MonthSelected, this.User, _fundid, "For Approval");
                        break;
                    case "Suspended":
                        btnSubmit.IsEnabled = false;
                        btnCheckAll.IsEnabled = false;
                        FetchActivityStatus(cmbYear.SelectedItem.ToString(), _MonthSelected, this.User, _fundid, "SUSPENDED-REVISION");
                        break;
                    default:
                        break;
                }
            
                if (btnCheckAll.Content== "Uncheck All")
                {
                    btnCheckAll.Content = "Check All";
                }
            }
        }
       
        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            PrinterHelper.PrintElement("MOB-Data", grdData, "NO");
        }

        private void grdStatus_CellClicked(object sender, CellClickedEventArgs e)
        {
            //List<ActivityStatus> x_data = ListActivityStatus.Where(x => x.activity_id == grdStatus.Rows[grdStatus.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString()).ToList();
            //if (e.Cell.Column.HeaderText == "submit")
            //{
            //    if (x_data.Count!=0)
            //    {
            //        if (x_data[0].submit)
            //        {
            //            x_data[0].submit = false;
            //        else
            //        {
            //        }
            //            x_data[0].submit = true;
            //        }


            //        grdStatus.ItemsSource = null;
            //        grdStatus.ItemsSource = ListActivityStatus;
            //        //grdStatus.Columns["activity_id"].Visibility = System.Windows.Visibility.Collapsed;
            //        //grdStatus.Columns["accountable_id"].Visibility = System.Windows.Visibility.Collapsed;
            //        //grdStatus.Columns["suspend_stats"].Visibility = System.Windows.Visibility.Collapsed;
            //        //grdStatus.Columns["submit_stats"].Visibility = System.Windows.Visibility.Collapsed;

            //        //FetchExpenditureHeader();

            //    }

            //}


        }

        void c_rep_mba_SQLOperation(object sender, EventArgs e)
        {
            String _fundid = "";
            switch (c_rep_mba.Process)
            {
                case "SuspendActivity":

                    List<FundSourceData> x = ListFundSourceData.Where(items => items.Fund_Name == cmbFundSource.SelectedItem.ToString()).ToList();


                    if (x.Count != 0)
                    {
                        _fundid = x[0].Fund_Source_Id;
                    }
                    FetchActivityStatus(cmbYear.SelectedItem.ToString(), _MonthSelected, this.User, _fundid,"");

                    break;
                case "SubmitActivity":



                    List<FundSourceData> xy = ListFundSourceData.Where(items => items.Fund_Name == cmbFundSource.SelectedItem.ToString()).ToList();


                    if (xy.Count != 0)
                    {
                        _fundid = xy[0].Fund_Source_Id;
                    }
                    FetchActivityStatus(cmbYear.SelectedItem.ToString(), _MonthSelected, this.User, _fundid,"");
                    _isStayLine = true;
                    break;
            }
        }

        private void grdStatus_CellExitingEditMode(object sender, ExitEditingCellEventArgs e)
        {

        }

        private void grdStatus_CellDoubleClicked(object sender, CellClickedEventArgs e)
        {

        }

        private void cmbFundSource_DropDownClosed(object sender, EventArgs e)
        {
            FetchListAllocationData();
        }
        private void SubmitSelected()         
        {
            List<ActivityStatus> _data = (List<ActivityStatus>)grdStatus.ItemsSource;
            String _sqlString = "";
            StringBuilder sb = new StringBuilder(79);
            String SQLCommand = "";
            String up1 = "";
            String up2 = "";
            foreach (ActivityStatus item in _data)
            {
                if (item.activity_id !="")
                {
                    if (item.submit)
                    {
                        up1 = @"  is_submitted = 1 , status = 'Submitted',is_suspended =0";
                    }
                    else
                    {
                        up1 = @"  is_submitted = 0 ,status = ''";
                    }

                    up2 = @" WHERE id = " + item.activity_id + ";";
                    _sqlString = "-" + "UPDATE dbo.mnda_activity_data SET" + up1 + up2 + Environment.NewLine;

                    SQLCommand += _sqlString;
                }
               
            }

            c_rep_mba.Process = "SubmitData";
            is_loaded = false;
            svc_mindaf.ExecuteQueryListsCompleted += svc_mindaf_ExecuteQueryListsCompleted;
            svc_mindaf.ExecuteQueryListsAsync(SQLCommand);
        }
        private bool is_loaded;
        void svc_mindaf_ExecuteQueryListsCompleted(object sender, ExecuteQueryListsCompletedEventArgs e)
        {
            if (c_rep_mba.Process =="SubmitData")
            {
                if (is_loaded ==false)
                {
                    if (e.Result == true)
                    {
                        MessageBox.Show("Data has been submitted successfully");
                        btnCheckAll.Content = "Check All";   
                    }
                    is_loaded = true;
                }
                
            }
        }
        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            SubmitSelected();
        }

        private void grdStatus_CellEnteringEditMode(object sender, BeginEditingCellEventArgs e)
        {
            if (e.Cell.Column.HeaderText != "submit")
            {
                e.Cancel = true;
            }
        }

        private void btnCheckAll_Click(object sender, RoutedEventArgs e)
        {
            switch (btnCheckAll.Content.ToString())
            {
                case "Check All":
                    foreach (ActivityStatus item in ListActivityStatus)
                    {
                        item.submit = true;

                    }
                    btnCheckAll.Content = "Uncheck All";
                    break;
                case "Uncheck All":
                    foreach (ActivityStatus item in ListActivityStatus)
                    {
                        item.submit = false;

                    }
                    btnCheckAll.Content = "Check All";
                    break;
                default:
                    break;
            }

            grdStatus.ItemsSource = null;
            grdStatus.ItemsSource = ListActivityStatus;
            grdStatus.Columns["activity_id"].Visibility = System.Windows.Visibility.Collapsed;
            grdStatus.Columns["accountable_id"].Visibility = System.Windows.Visibility.Collapsed;
            grdStatus.Columns["suspend_stats"].Visibility = System.Windows.Visibility.Collapsed;
            grdStatus.Columns["submit_stats"].Visibility = System.Windows.Visibility.Collapsed;
            grdStatus.Columns["suspend_activity"].Visibility = System.Windows.Visibility.Collapsed;
        }

        private void btnCheckBalances_Click(object sender, RoutedEventArgs e)
        {
              String _fundid = "";

                    List<FundSourceData> x = ListFundSourceData.Where(items => items.Fund_Name == cmbFundSource.SelectedItem.ToString()).ToList();


                    if (x.Count != 0)
                    {
                        _fundid = x[0].Fund_Source_Id;
                    }
            //frmReportBalances xdata = new frmReportBalances();
            //xdata.FundSource = _fundid;
            //xdata.Show();
        }

        private void cmbType_DropDownClosed(object sender, EventArgs e)
        {
            grdStatus.ItemsSource = null;
            FetchListAllocationData();
            //List<FundSourceData> x = ListFundSourceData.Where(items => items.Fund_Name == cmbFundSource.SelectedItem.ToString()).ToList();
            //String _fundid = "";

            //if (x.Count != 0)
            //{
            //    _fundid = x[0].Fund_Source_Id;
            //}
            

            //switch (cmbType.SelectedItem.ToString())
            //{
            //    case "Finance Approved":
            //        btnSubmit.IsEnabled = false;
            //        btnCheckAll.IsEnabled = false;
            //        FetchActivityStatus(cmbYear.SelectedItem.ToString(), _MonthSelected, this.User, _fundid,"FINANCE APPROVED");
            
            //        break;
            //    case "Submitted":
            //         btnSubmit.IsEnabled = false;
            //        btnCheckAll.IsEnabled = false;
            //        FetchActivityStatus(cmbYear.SelectedItem.ToString(), _MonthSelected, this.User, _fundid, "Submitted");
            //        break;
            //    case "For Approval":
            //        btnSubmit.IsEnabled = true;
            //        btnCheckAll.IsEnabled = true;
            //        FetchActivityStatus(cmbYear.SelectedItem.ToString(), _MonthSelected, this.User, _fundid, "For Approval");
            //        break;
            //    case "Suspended":
            //   btnSubmit.IsEnabled = false;
            //        btnCheckAll.IsEnabled = false;
            //        FetchActivityStatus(cmbYear.SelectedItem.ToString(), _MonthSelected, this.User, _fundid, "SUSPENDED-REVISION");
            //        break;
            //    default:
            //        break;
            //}
        }
      
    }
}
