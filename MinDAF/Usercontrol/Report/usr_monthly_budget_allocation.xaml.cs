﻿using MinDAF.Class;
using MinDAF.MinDAFS;
using Infragistics.Controls.Grids;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Printing;
using System.Windows.Shapes;
using System.Xml.Linq;
using MinDAF.Forms;

namespace MinDAF.Usercontrol.Report
{
   
    public partial class usr_monthly_budget_allocation : UserControl
    {
        private PrintDocument xPrint = new PrintDocument();

        public String DivisionName { get; set; }
        public String SelectedYear { get; set; }
        public String DivisionId { get; set; }
        public String DivisionPAP { get; set; }
        public String FundSource { get; set; }
        public String TypeService { get; set; }
        public String PaP { get; set; }

        public String Process { get; set; }

        private List<ReportMBA> ReportData = new List<ReportMBA>();
        private List<ReportMBAHeader> ListHeaderTemplate = new List<ReportMBAHeader>();
        private List<ReportMBADetail> ListHeaderDetail = new List<ReportMBADetail>();
        private List<ReportMBAHeader> ListTotals = new List<ReportMBAHeader>();
        private List<ReportDataMBA> ListReportData = new List<ReportDataMBA>();
        private List<ReportTypeService> ListReporTypeService = new List<ReportTypeService>();
        private List<AllocationExpenditure> ListAllocation = new List<AllocationExpenditure>();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private clsBudgetReportMBA c_rep_mba = new clsBudgetReportMBA();
        private   PrintDocument document = new PrintDocument();
        public usr_monthly_budget_allocation()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;

        

        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_rep_mba.Process)
            {
                case "FetchExpenditureTemplate":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new ReportMBAHeader
                                     {
                                         UACS = Convert.ToString(info.Element("UACS").Value),
                                         Alloted = Convert.ToString(info.Element("Alloted").Value),
                                         Apr = Convert.ToString(info.Element("Apr").Value),
                                         Aug = Convert.ToString(info.Element("Aug").Value),
                                         Balance = Convert.ToString(info.Element("Balance").Value),
                                         Dec = Convert.ToString(info.Element("Dec").Value),
                                         Feb = Convert.ToString(info.Element("Feb").Value),
                                         id   = Convert.ToString(info.Element("id").Value),
                                         Jan   = Convert.ToString(info.Element("Jan").Value),
                                         Jul    = Convert.ToString(info.Element("Jul").Value),
                                         Jun   = Convert.ToString(info.Element("Jun").Value),
                                         Mar   = Convert.ToString(info.Element("Mar").Value),
                                         May   = Convert.ToString(info.Element("May").Value),
                                         Name   = Convert.ToString(info.Element("Name").Value),
                                         Nov  = Convert.ToString(info.Element("Nov").Value),
                                         Oct    = Convert.ToString(info.Element("Oct").Value),
                                         Sep   = Convert.ToString(info.Element("Sep").Value),
                                         Total   = Convert.ToString(info.Element("Total").Value)
                                     };


                    ListHeaderTemplate.Clear();

                    foreach (var item in _dataLists)
                    {
                        ReportMBAHeader _varProf = new ReportMBAHeader();

                        _varProf.Alloted = item.Alloted;
                        _varProf.Apr = item.Apr;
                        _varProf.Aug = item.Aug;
                        _varProf.Balance = item.Balance;
                        _varProf.Dec = item.Dec;
                        _varProf.Feb = item.Feb;
                        _varProf.id = item.id;
                        _varProf.Jan = item.Jan;
                        _varProf.Jul = item.Jul;
                        _varProf.Jun = item.Jun;
                        _varProf.Mar = item.Mar;
                        _varProf.May = item.May;
                        _varProf.Name = item.Name;
                        _varProf.Nov = item.Nov;
                        _varProf.Oct = item.Oct;
                        _varProf.Sep = item.Sep;
                        _varProf.Total = item.Total;
                        _varProf.UACS = item.UACS;

                        ListHeaderTemplate.Add(_varProf);

                    }

                    FetchTypeService();
                    this.Cursor = Cursors.Arrow;

                    break;
                case "FetchExpenditureDetail":
                    XDocument oDocKeyResultsFetchExpenditureDetail = XDocument.Parse(_results);
                    var _dataListsFetchExpenditureDetail = from info in oDocKeyResultsFetchExpenditureDetail.Descendants("Table")
                                     select new ReportMBADetail
                                     {
                                         UACS = Convert.ToString(info.Element("UACS").Value),
                                         Alloted = Convert.ToString(info.Element("Alloted").Value),
                                         Apr = Convert.ToString(info.Element("Apr").Value),
                                         Aug = Convert.ToString(info.Element("Aug").Value),
                                         Balance = Convert.ToString(info.Element("Balance").Value),
                                         Dec = Convert.ToString(info.Element("Dec").Value),
                                         Feb = Convert.ToString(info.Element("Feb").Value),
                                         id   = Convert.ToString(info.Element("id").Value),
                                         Jan   = Convert.ToString(info.Element("Jan").Value),
                                         Jul    = Convert.ToString(info.Element("Jul").Value),
                                         Jun   = Convert.ToString(info.Element("Jun").Value),
                                         Mar   = Convert.ToString(info.Element("Mar").Value),
                                         May   = Convert.ToString(info.Element("May").Value),
                                         Name   = Convert.ToString(info.Element("Name").Value),
                                         Nov  = Convert.ToString(info.Element("Nov").Value),
                                         Oct    = Convert.ToString(info.Element("Oct").Value),
                                         Sep   = Convert.ToString(info.Element("Sep").Value),
                                         Total   = Convert.ToString(info.Element("Total").Value)
                                     };


                    ListHeaderDetail.Clear();

                    foreach (var item in _dataListsFetchExpenditureDetail)
                    {
                        ReportMBADetail _varProf = new ReportMBADetail();

                        _varProf.Alloted = item.Alloted;
                        _varProf.Apr = item.Apr;
                        _varProf.Aug = item.Aug;
                        _varProf.Balance = item.Balance;
                        _varProf.Dec = item.Dec;
                        _varProf.Feb = item.Feb;
                        _varProf.id = item.id;
                        _varProf.Jan = item.Jan;
                        _varProf.Jul = item.Jul;
                        _varProf.Jun = item.Jun;
                        _varProf.Mar = item.Mar;
                        _varProf.May = item.May;
                        _varProf.Name = item.Name;
                        _varProf.Nov = item.Nov;
                        _varProf.Oct = item.Oct;
                        _varProf.Sep = item.Sep;
                        _varProf.Total = item.Total;
                        _varProf.UACS = item.UACS;
                        ListHeaderDetail.Add(_varProf);

                    }
                    if (this.Process =="SUMMARY")
                    {
                        FetchDataSummary();
                    }
                    else
                    {
                        FetchData();
                    }
                   
                    this.Cursor = Cursors.Arrow;
                   
                    break; 
                case "FetchData":
                    XDocument oDocKeyResultsFetchData = XDocument.Parse(_results);
                    var _dataListsFetchData = from info in oDocKeyResultsFetchData.Descendants("Table")
                                     select new ReportDataMBA
                                     {
                                          id= Convert.ToString(info.Element("id").Value),
                                          month= Convert.ToString(info.Element("month").Value),
                                          total= Convert.ToString(info.Element("total").Value),
                                          year= Convert.ToString(info.Element("year").Value),
                                          name = Convert.ToString(info.Element("name").Value),
                                          fund_source = Convert.ToString(info.Element("fund_source").Value),
                                          mooe = Convert.ToString(info.Element("mooe").Value),
                                          uacs = Convert.ToString(info.Element("UACS").Value)
                                     };


                    ListReportData.Clear();

                    foreach (var item in _dataListsFetchData)
                    {
                        ReportDataMBA _varProf = new ReportDataMBA();

                        _varProf.id = item.id;
                        _varProf.month = item.month;
                        _varProf.total = item.total;
                        _varProf.year = item.year;
                        _varProf.name = item.name;
                        _varProf.fund_source = item.fund_source;
                        _varProf.mooe = item.mooe;
                        _varProf.uacs = item.uacs;
                        ListReportData.Add(_varProf);

                    }
                    
                    this.Cursor = Cursors.Arrow;
                    FetchAllocation();
                    break; 
                case "FetchAllocation":
                    XDocument oDocKeyResultsFetchAllocation = XDocument.Parse(_results);
                    var _dataListsFetchAllocation = from info in oDocKeyResultsFetchAllocation.Descendants("Table")
                                     select new AllocationExpenditure
                                     {
                                         name = Convert.ToString(info.Element("name").Value),
                                         allocated_budget = Convert.ToString(info.Element("allocated_budget").Value)
                                         
                                     };


                    ListAllocation.Clear();

                    foreach (var item in _dataListsFetchAllocation)
                    {
                        AllocationExpenditure _varProf = new AllocationExpenditure();

                        _varProf.name = item.name;
                        _varProf.allocated_budget = item.allocated_budget;

                        ListAllocation.Add(_varProf);

                    }
                    GenerateTemplate();
                    this.Cursor = Cursors.Arrow;
                   
                    break;
                case "FetchTypeService":
                    XDocument oDocKeyResultsFetchTypeService = XDocument.Parse(_results);
                    var _dataListsFetchTypeService = from info in oDocKeyResultsFetchTypeService.Descendants("Table")
                                                     select new ReportTypeService
                                                    {
                                                        name = Convert.ToString(info.Element("name").Value),
                                                        uacs = Convert.ToString(info.Element("uacs").Value)

                                                    };


                    ListReporTypeService.Clear();

                    foreach (var item in _dataListsFetchTypeService)
                    {
                        ReportTypeService _varProf = new ReportTypeService();

                        _varProf.code = item.uacs.Substring(0,3);
                        _varProf.name = item.name;
                        _varProf.uacs = item.uacs;

                        ListReporTypeService.Add(_varProf);

                        cmbTypeService.Items.Add(item.name);

                    }
                    cmbTypeService.SelectedIndex = 1;
                   
                    TypeService = "502";
                    FetchExpenditureDetail();
                    this.Cursor = Cursors.Arrow;

                    break;
            }
        }

        private void GenerateTemplate()
        {
            double _JanTotal = 0.00;
            double _FebTotal = 0.00;
            double _MarTotal = 0.00;
            double _AprTotal = 0.00;
            double _MayTotal = 0.00;
            double _JunTotal = 0.00;
            double _JulTotal = 0.00;
            double _AugTotal = 0.00;
            double _SepTotal = 0.00;
            double _OctTotal = 0.00;
            double _NovTotal = 0.00;
            double _DecTotal = 0.00;
            double _LineTotal = 0.00;
            double _AllTotal = 0.00;
            double _AllBalance = 0.00;
            List<ReportMBADetail> NewLists = new List<ReportMBADetail>();
            ReportData = new List<ReportMBA>();
            foreach (var item in ListHeaderDetail)
            {
                List<AllocationExpenditure> x_data = ListAllocation.Where(_filter => _filter.name == item.Name).ToList();
                if (x_data.Count!=0)
                {
                    if (item.UACS.Substring(0,3)==TypeService)
                    {
                        double _all = Convert.ToDouble(x_data[0].allocated_budget);
                        item.Alloted = _all.ToString("#,##0.00");
                        NewLists.Add(item);
                    }
                   
                }
            }
            
            foreach (var item in NewLists)
            {
                List<ReportDataMBA> x_data = new List<ReportDataMBA>();
                if (Process=="SUMMARY")
                {
                     x_data = ListReportData.Where(_filter => _filter.uacs ==item.UACS).ToList();
                }
                else
                {
                     x_data = ListReportData.Where(_filter => _filter.uacs ==item.UACS && _filter.fund_source == FundSource).ToList();
                }
               
               
                      
                                item.Jan =00.ToString("#,##0.00");
                            
                                item.Feb =00.ToString("#,##0.00");
                           
                                item.Mar =00.ToString("#,##0.00");
                         
                                item.Apr =00.ToString("#,##0.00");
                         
                                item.May =00.ToString("#,##0.00");
                            
                                item.Jun =00.ToString("#,##0.00");
                              
                                item.Jul =00.ToString("#,##0.00");
                              
                                item.Aug =00.ToString("#,##0.00");
                            
                                item.Sep =00.ToString("#,##0.00");
                              
                                item.Oct =00.ToString("#,##0.00");
                             
                                item.Nov =00.ToString("#,##0.00");
                               
                                item.Dec =00.ToString("#,##0.00");

                                item.TypeService = item.UACS.Substring(0, 3);
                              
               // }
            }

            foreach (var item in NewLists)
            {

                List<ReportDataMBA> x_data = new List<ReportDataMBA>();
                if (Process == "SUMMARY")
                {
                    x_data = ListReportData.Where(_filter => _filter.uacs == item.UACS).ToList();
                }
                else
                {
                    x_data = ListReportData.Where(_filter => _filter.uacs == item.UACS && _filter.fund_source == FundSource).ToList();
                }
              //  x_data = ListReportData.Where(_filter => _filter.uacs == "5020309000" && _filter.fund_source == FundSource).ToList();

                //List<ReportDataMBA> x_data = ListReportData.Where(_filter => _filter.uacs == item.UACS && _filter.fund_source == FundSource).ToList();
                 _JanTotal = 0.00;
                 _FebTotal = 0.00;
                 _MarTotal = 0.00;
                 _AprTotal = 0.00;
                 _MayTotal = 0.00;
                 _JunTotal = 0.00;
                 _JulTotal = 0.00;
                 _AugTotal = 0.00;
                 _SepTotal = 0.00;
                 _OctTotal = 0.00;
                 _NovTotal = 0.00;
                 _DecTotal = 0.00;
                foreach (var itemRemove in x_data)
                {
                    switch (itemRemove.month)
                    {
                        case "Jan":
                            _JanTotal += Convert.ToDouble(itemRemove.total);
                            item.Jan = _JanTotal.ToString("#,##0.00");
                            break;
                        case "Feb":
                            _FebTotal += Convert.ToDouble(itemRemove.total);
                            item.Feb = _FebTotal.ToString("#,##0.00");
                            break;
                        case "Mar":
                            _MarTotal += Convert.ToDouble(itemRemove.total);
                            item.Mar = _MarTotal.ToString("#,##0.00");
                            break;
                        case "Apr":
                            _AprTotal += Convert.ToDouble(itemRemove.total);
                            item.Apr = _AprTotal.ToString("#,##0.00");
                            break;
                        case "May":
                            _MayTotal += Convert.ToDouble(itemRemove.total);
                            item.May = _MayTotal.ToString("#,##0.00");
                            break;
                        case "Jun":
                            _JunTotal += Convert.ToDouble(itemRemove.total);
                            item.Jun = _JunTotal.ToString("#,##0.00");
                            break;
                        case "Jul":
                            _JulTotal += Convert.ToDouble(itemRemove.total);
                            item.Jul = _JulTotal.ToString("#,##0.00");
                            break;
                        case "Aug":
                            _AugTotal += Convert.ToDouble(itemRemove.total);
                            item.Aug = _AugTotal.ToString("#,##0.00");
                            break;
                        case "Sep":
                            _SepTotal += Convert.ToDouble(itemRemove.total);
                            item.Sep = _SepTotal.ToString("#,##0.00");
                            break;
                        case "Oct":
                            _OctTotal += Convert.ToDouble(itemRemove.total);
                            item.Oct = _OctTotal.ToString("#,##0.00");
                            break;
                        case "Nov":
                            _NovTotal += Convert.ToDouble(itemRemove.total);
                            item.Nov = _NovTotal.ToString("#,##0.00");
                            break;
                        case "Dec":
                            _DecTotal += Convert.ToDouble(itemRemove.total);
                            item.Dec = _DecTotal.ToString("#,##0.00");
                            break;

                    }

                    ListReportData.Remove(itemRemove);
                }
            }
            var results = from p in ListReportData
                          group p by p.name into g
                          select new
                          {
                              ExpenseName = g.Key, UACS = g.Select(m => m.uacs)
                          };

            List<ReportMBADetail> _AddedData = new List<ReportMBADetail>();
            foreach (var item in results)
            {
                  
                  ReportMBADetail x_data  = new ReportMBADetail();
                 
                  x_data.Name  = item.ExpenseName;
                  foreach (var uacs in item.UACS)
                  {
                      if (uacs.Substring(0,3)==TypeService)
                      {
                          x_data.UACS = uacs.ToString();
                          x_data.TypeService = uacs.Substring(0, 3);
                          break;
                      }
                       
                  }
                
                  _AddedData.Add(x_data);  
            }

            foreach (var item in _AddedData)
            {
                if (item.UACS=="5020309000")
                {
                    
                }
                List<ReportDataMBA> x_data = new List<ReportDataMBA>();
                if (Process == "SUMMARY")
                {
                    x_data = ListReportData.Where(_filter => _filter.uacs == item.UACS).ToList();
                }
                else
                {
                    x_data =ListReportData.Where(_filter => _filter.name == item.Name && _filter.fund_source == FundSource).ToList();;
                }

                _JanTotal = 0.00;
                _FebTotal = 0.00;
                _MarTotal = 0.00;
                _AprTotal = 0.00;
                _MayTotal = 0.00;
                _JunTotal = 0.00;
                _JulTotal = 0.00;
                _AugTotal = 0.00;
                _SepTotal = 0.00;
                _OctTotal = 0.00;
                _NovTotal = 0.00;
                _DecTotal = 0.00;

                foreach (var item_data in x_data)
                {

                    switch (item_data.month)
                    {
                        case "Jan":
                            _JanTotal += Convert.ToDouble(item_data.total);
                            item.Jan = _JanTotal.ToString("#,##0.00");
                            break;
                        case "Feb":
                            _FebTotal += Convert.ToDouble(item_data.total);
                            item.Feb = _FebTotal.ToString("#,##0.00");
                            break;
                        case "Mar":
                            _MarTotal += Convert.ToDouble(item_data.total);
                            item.Mar = _MarTotal.ToString("#,##0.00");
                            break;
                        case "Apr":
                            _AprTotal += Convert.ToDouble(item_data.total);
                            item.Apr = _AprTotal.ToString("#,##0.00");
                            break;
                        case "May":
                            _MayTotal += Convert.ToDouble(item_data.total);
                            item.May = _MayTotal.ToString("#,##0.00");
                            break;
                        case "Jun":
                            _JunTotal += Convert.ToDouble(item_data.total);
                            item.Jun = _JunTotal.ToString("#,##0.00");
                            break;
                        case "Jul":
                            _JulTotal += Convert.ToDouble(item_data.total);
                            item.Jul = _JulTotal.ToString("#,##0.00");
                            break;
                        case "Aug":
                            _AugTotal += Convert.ToDouble(item_data.total);
                            item.Aug = _AugTotal.ToString("#,##0.00");
                            break;
                        case "Sep":
                            _SepTotal += Convert.ToDouble(item_data.total);
                            item.Sep = _SepTotal.ToString("#,##0.00");
                            break;
                        case "Oct":
                            _OctTotal += Convert.ToDouble(item_data.total);
                            item.Oct = _OctTotal.ToString("#,##0.00");
                            break;
                        case "Nov":
                            _NovTotal += Convert.ToDouble(item_data.total);
                            item.Nov = _NovTotal.ToString("#,##0.00");
                            break;
                        case "Dec":
                            _DecTotal += Convert.ToDouble(item_data.total);
                            item.Dec = _DecTotal.ToString("#,##0.00");
                            break;

                    }
                    item.UACS = item_data.uacs;
                    item.id = item_data.id;
                    item.Name = item_data.name;
                    item.TypeService = item_data.uacs.Substring(0, 3);
                }
            }
            foreach (var item in _AddedData)
            {
                if (item.id==null)
                {
                    List<ReportMBADetail> x = ListHeaderDetail.Where(items => items.UACS == item.UACS).ToList();
                    foreach (var itemu in x)
                    {

                        item.id = itemu.id;
                     //   item.Name = itemu.Name;
                    }
                }
                if (item.TypeService==TypeService)
                {
                    NewLists.Add(item);
                }
                  
               
            }
            _JanTotal = 0.00;
            _FebTotal = 0.00;
            _MarTotal = 0.00;
            _AprTotal = 0.00;
            _MayTotal = 0.00;
            _JunTotal = 0.00;
            _JulTotal = 0.00;
            _AugTotal = 0.00;
            _SepTotal = 0.00;
            _OctTotal = 0.00;
            _NovTotal = 0.00;
            _DecTotal = 0.00;

            foreach (var item in NewLists)
            {

                if (item.Jan!="0.00")
                {
                    
                }
                        _JanTotal += Convert.ToDouble(item.Jan);
                        _FebTotal += Convert.ToDouble(item.Feb);
                        _MarTotal += Convert.ToDouble(item.Mar);
                        _AprTotal += Convert.ToDouble(item.Apr);
                        _MayTotal += Convert.ToDouble(item.May);
                        _JunTotal += Convert.ToDouble(item.Jun);
                        _JulTotal += Convert.ToDouble(item.Jul);
                        _AugTotal += Convert.ToDouble(item.Aug);
                        _SepTotal += Convert.ToDouble(item.Sep);
                        _OctTotal += Convert.ToDouble(item.Oct);
                        _NovTotal += Convert.ToDouble(item.Nov);
                        _DecTotal += Convert.ToDouble(item.Dec);

            }

            //Computer For Total and Balance
            foreach (var item in NewLists)
            {
                double _lineTotal = Convert.ToDouble(item.Jan) + Convert.ToDouble(item.Feb) + Convert.ToDouble(item.May) + Convert.ToDouble(item.Mar) + Convert.ToDouble(item.Apr) +
                                    Convert.ToDouble(item.Jun) + Convert.ToDouble(item.Jul) + Convert.ToDouble(item.Aug) + Convert.ToDouble(item.Sep) + Convert.ToDouble(item.Oct) +
                                    Convert.ToDouble(item.Nov) + Convert.ToDouble(item.Dec);
                double _lineAllotted = Convert.ToDouble(item.Alloted);
                double _balance = _lineAllotted - _lineTotal;

                item.Total = _lineTotal.ToString("#,##0.00");
                item.Balance = _balance.ToString("#,##0.00");
                _LineTotal += _lineTotal;

                if (_lineTotal>0)
                {
                    if (_lineAllotted>0)
                    {
                        _AllTotal += _lineAllotted;
                        _AllBalance += _balance;
                    }
                    else
                    {
                        _AllBalance += _balance;
                    }
                }
                else
                {
                    _AllTotal += _lineAllotted;
                    _AllBalance += _balance;
                }
               
            }

            ReportMBAHeader _Totals = new ReportMBAHeader();
            _Totals.Name = "Totals";
            _Totals.Alloted = _AllTotal.ToString("#,##0.00");
            _Totals.Jan = _JanTotal.ToString("#,##0.00");
            _Totals.Feb = _FebTotal.ToString("#,##0.00");
            _Totals.Mar = _MarTotal.ToString("#,##0.00");
            _Totals.Apr = _AprTotal.ToString("#,##0.00");
            _Totals.May = _MayTotal.ToString("#,##0.00");
            _Totals.Jun = _JunTotal.ToString("#,##0.00");
            _Totals.Jul = _JulTotal.ToString("#,##0.00");
            _Totals.Aug = _AugTotal.ToString("#,##0.00");
            _Totals.Sep = _SepTotal.ToString("#,##0.00");
            _Totals.Oct = _OctTotal.ToString("#,##0.00");
            _Totals.Nov = _NovTotal.ToString("#,##0.00");
            _Totals.Dec = _DecTotal.ToString("#,##0.00");
            _Totals.Total = _LineTotal.ToString("#,##0.00");
            _Totals.Balance = _AllBalance.ToString("#,##0.00");
            ListTotals.Clear();
            ListTotals.Add(_Totals);
            foreach (var item in ListHeaderTemplate)
            {
                if (item.UACS == "2020100000")
                {
                    try
                    {

                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }

                ReportMBA _final = new ReportMBA();

                List<ReportMBADetail> _data = NewLists.Where(itemDetail => itemDetail.id == item.id).ToList();
                if (_data.Count != 0)
                {
                    List<ReportMBADetail> _dataFinal = _data.Where(itemDetail => itemDetail.Total != "0.00").ToList();
                    List<ReportMBADetail> _dataFinal2 = _data.Where(itemDetail => itemDetail.Alloted != "0.00").ToList();
                    if (_dataFinal.Count!=0)
                    {
                    
                        foreach (var item_data in _dataFinal)
                        {
                            if (item_data.Alloted != "0.00" || item_data.Alloted == null)
                            {
                                _final.ReportHeader = item.Name;
                                _final.UACS = "";
                                _final.ReportDetail = _dataFinal;
                     
                                ReportData.Add(_final);
                                break;
                            }
                        }
                     
                    }
                    else
                    {
                        foreach (var item_data in _dataFinal2)
                        {
                            if (item_data.Alloted != "0.00")
                            {
                                _final.ReportHeader = item.Name;
                                _final.UACS = "";
                                _final.ReportDetail = _dataFinal2;
                                ReportData.Add(_final);
                                break;
                            }
                        }
                    }
                   
                }
              
            //    List<ReportMBADetail> _dataFinal = _data.Where(itemDetail => itemDetail.Alloted != "0.00").ToList();
              
               
                
              
              

            }
            grdData.ItemsSource = null;
            grdData.ItemsSource = ReportData;
            
            grdTotals.ItemsSource = null;
            grdTotals.ItemsSource = ListTotals;

            grdTotals.Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
          //  grdTotals.Columns["TypeService"].Visibility = System.Windows.Visibility.Collapsed;

            Column col_project_name = grdData.Columns.DataColumns["ReportHeader"];
            col_project_name.Width = new ColumnWidth(298.5, false);

            Column col_project_name_t = grdTotals.Columns.DataColumns["Name"];
            col_project_name_t.Width = new ColumnWidth(325, false);

            foreach (var item in grdData.Rows)
            {
                if (item.HasChildren)
                {
                    item.IsExpanded = true;
                    item.ChildBands[0].ColumnLayout.HeaderVisibility = Visibility.Collapsed;
                    item.ChildBands[0].Columns["id"].Visibility = System.Windows.Visibility.Collapsed;
                    item.ChildBands[0].Columns["TypeService"].Visibility = System.Windows.Visibility.Collapsed;
                    foreach (var child in item.ChildBands)
                    {
                        Column col_project_name_child = child.Columns.DataColumns["Name"];
                        col_project_name_child.Width = new ColumnWidth(300, false);
                        
                    }
                }
            }
            if (ReportData.Count!=0)
            {
                try
                {
                    LoadReportDataMAO();
                }
                catch (Exception)
                {

                }
               
            }
           
        }

        private void LoadReportDataMAO()
        {
         //   c_rep_mba.Process = "SaveReportPrintOutMAO";
            c_rep_mba.SaveReportPrintOutMAO(ReportData,DivisionName , FundSource, DivisionPAP);
         
        }

        
        private void FetchExpenditureHeader() 
        {
            c_rep_mba.Process = "FetchExpenditureTemplate";
            svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchExpenditureTemplate());
        } 
        private void FetchAllocation() 
        {
            c_rep_mba.Process = "FetchAllocation";
            if (Process=="SUMMARY")
            {
                svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchAllocationExpenditureSummary(this.PaP, this.SelectedYear));
            }
            else
            {
                svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchAllocationExpenditure(this.DivisionId, this.SelectedYear, this.FundSource));
            }
           
        }
        private void FetchExpenditureDetail()
        {
            c_rep_mba.Process = "FetchExpenditureDetail";
            svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchExpenditureDetailTemplate());
        }
        private void FetchData()
        {
            c_rep_mba.Process = "FetchData";
           
            svc_mindaf.ExecuteImportDataSQLAsync(c_rep_mba.FetchDataMBA(this.SelectedYear,this.FundSource));
            svc_mindaf.ExecuteImportDataSQLCompleted += svc_mindaf_ExecuteImportDataSQLCompleted;
          //  svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchDataDB(this.SelectedYear, this.DivisionPAP,this.FundSource));
        }
        private void FetchTypeService()
        {
            c_rep_mba.Process = "FetchTypeService";

            svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchTypeService());
        }
        private void FetchDataSummary()
        {
            c_rep_mba.Process = "FetchData";
            svc_mindaf.ExecuteImportDataSQLAsync(c_rep_mba.FetchDataMBASummary(this.SelectedYear, this.PaP));
            svc_mindaf.ExecuteImportDataSQLCompleted += svc_mindaf_ExecuteImportDataSQLCompleted;
            //  svc_mindaf.ExecuteSQLAsync(c_rep_mba.FetchDataDB(this.SelectedYear, this.DivisionPAP,this.FundSource));
        }

        void svc_mindaf_ExecuteImportDataSQLCompleted(object sender, ExecuteImportDataSQLCompletedEventArgs e)
        {
             var _results = e.Result.ToString();
             switch (c_rep_mba.Process)
             {
                 case "FetchData":
                     XDocument oDocKeyResultsFetchData = XDocument.Parse(_results);
                    var _dataListsFetchData = from info in oDocKeyResultsFetchData.Descendants("Table")
                                     select new ReportDataMBA
                                     {
                                         type_service = "",
                                          month = Convert.ToString(info.Element("_month").Value),
                                          total= Convert.ToString(info.Element("Total").Value),
                                          name = Convert.ToString(info.Element("name").Value),
                                          fund_source = Convert.ToString(info.Element("FundSource").Value),
                                          uacs = Convert.ToString(info.Element("uacs").Value)
                                     };


                    ListReportData.Clear();

                    foreach (var item in _dataListsFetchData)
                    {
                        ReportDataMBA _varProf = new ReportDataMBA();

                        _varProf.id = item.id;
                   
                        switch (item.month)
                        {
                            case "1":
                                _varProf.month = "Jan";
                                break;
                            case "2":
                                _varProf.month = "Feb";
                                break;
                            case "3":
                                _varProf.month = "Mar";
                                break;
                            case "4":
                                _varProf.month = "Apr";
                                break;
                            case "5":
                                _varProf.month = "May";
                                break;
                            case "6":
                                _varProf.month = "Jun";
                                break;
                            case "7":
                                _varProf.month = "Jul";
                                break;
                            case "8":
                                _varProf.month = "Aug";
                                break;
                            case "9":
                                _varProf.month = "Sep";
                                break;
                            case "10":
                                _varProf.month = "Oct";
                                break;
                            case "11":
                                _varProf.month = "Nov";
                                break;
                            case "12":
                                _varProf.month = "Dec";
                                break;
                        }
                        _varProf.total = item.total;
                        _varProf.year = item.year;
                        _varProf.name = item.name;
                        _varProf.fund_source = item.fund_source;
                        _varProf.mooe = item.mooe;
                        _varProf.uacs = item.uacs;
                        ListReportData.Add(_varProf);

                    }
                    
                    this.Cursor = Cursors.Arrow;
                    FetchAllocation();
                     break;
             }
        }
        private void ctrl_mao_Loaded(object sender, RoutedEventArgs e)
        {
            lblOffice.Content = DivisionName;
            if (Process=="SUMMARY")
            {
                lblFundSouce.Content = "Summary";
            }
            else
            {
                lblFundSouce.Content = FundSource;
            }
           
            FetchExpenditureHeader();
        }

  
    

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            //frmPrintPreview f_view = new frmPrintPreview();
            //f_view.ReportType = "mao_urs";
            //f_view.Division = this.DivisionName;
            //f_view.FundSource = FundSource;
            //f_view.ReportData = ReportData;
          
            //f_view.PAP = this.PaP;
            //f_view.Show();
        }

        private void cmbTypeService_DropDownClosed(object sender, EventArgs e)
        {
             TypeService = ListReporTypeService.Where(items => items.name == cmbTypeService.SelectedItem.ToString()).ToList()[0].code;
         
            FetchExpenditureDetail();
        }
    }
}
