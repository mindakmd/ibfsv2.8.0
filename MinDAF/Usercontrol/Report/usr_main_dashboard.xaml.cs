﻿using MinDAF.Class;
using MinDAF.Forms;
using MinDAF.MinDAFS;
using Infragistics.Controls.Grids;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace MinDAF.Usercontrol.Report
{
    public partial class usr_main_dashboard : UserControl
    {
        public String DivisionId { get; set; }
        public String UserId { get; set; }
        public String Username { get; set; }
        private String SelectedExpenditure { get; set; }
        private String _MonthSelected { get; set; }

        public String FundSource { get; set; }
        public String FundName { get; set; }
        public String IsAdmin { get; set; }

        public Boolean IsUnit { get; set; }
        private List<BudgetRevisionMain> _Main = new List<BudgetRevisionMain>();
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private MinDAFSVCClient svc_mindaf_refresh = new MinDAFSVCClient();
        private clsMainData c_main_data = new clsMainData();
        private frmListExpenditures f_expenditures = new frmListExpenditures();

        private List<MainData> _ListMainData = new List<MainData>();
        private List<MainTotals> _ListMainTotals = new List<MainTotals>();
        private List<Main_OverAllYear> _ListOverAllYear = new List<Main_OverAllYear>();
        private List<MainDashboardActivityData> ListActivityStatus = new List<MainDashboardActivityData>();
        private List<MainDashboardActivityData> ListActivityStatusByMonth = new List<MainDashboardActivityData>();
        private List<OBR_Details> ListGridDataDetails = new List<OBR_Details>();

        private List<ListExpenseMain> ListExpenseItems = new List<ListExpenseMain>();

        private List<ActualDataMain> ListActualData = new List<ActualDataMain>();

        private Boolean IsLocked = false;

        private String _updateProgram = "";
        private String _updateProject = "";
        private String _updateProjectOutput = "";
        private String _updateOutputActivity = "";
        private String _updateModeOfProcurement = "";
       private frmEmployeeList f_employee = new frmEmployeeList();

       public String WorkingYear { get; set; }

        public usr_main_dashboard()
        {
            InitializeComponent();
            svc_mindaf.ExecuteSQLCompleted += svc_mindaf_ExecuteSQLCompleted;
            svc_mindaf_refresh.ExecuteSQLCompleted += svc_mindaf_refresh_ExecuteSQLCompleted;
            svc_mindaf.ExecuteImportDataSQLCompleted += svc_mindaf_ExecuteImportDataSQLCompleted;
            f_expenditures.SelectedExpenditure+=f_expenditures_SelectedExpenditure;
            c_main_data.SQLOperation += c_main_data_SQLOperation;

            f_expenditures = new frmListExpenditures(this.DivisionId);
        }

        void svc_mindaf_ExecuteImportDataSQLCompleted(object sender, ExecuteImportDataSQLCompletedEventArgs e)
        {
            String _results = e.Result.ToString();

            switch (c_main_data.Process)
            {
                case "FetchOBRData":
                    XDocument oDocKeyFetchGridData = XDocument.Parse(_results);
                    var _dataListsFetchGridData = from info in oDocKeyFetchGridData.Descendants("Table")
                                                  select new OBR_Details
                                                  {
                                                      _Month = Convert.ToString(info.Element("_Month").Value),
                                                      AccountCode = Convert.ToString(info.Element("AccountCode").Value),
                                                      DateCreated = Convert.ToString(info.Element("DateCreated").Value),
                                                      Division = Convert.ToString(info.Element("Division").Value),
                                                      Total = Convert.ToString(info.Element("Total").Value)

                                                  };

                    ListGridDataDetails.Clear();

                    foreach (var item in _dataListsFetchGridData)
                    {
                        OBR_Details _varDetails = new OBR_Details();

                        switch (item._Month)
                        {
                            case "1": _varDetails._Month = "Jan"; break;
                            case "2": _varDetails._Month = "Feb"; break;
                            case "3": _varDetails._Month = "Mar"; break;
                            case "4": _varDetails._Month = "Apr"; break;
                            case "5": _varDetails._Month = "May"; break;
                            case "6": _varDetails._Month = "Jun"; break;
                            case "7": _varDetails._Month = "Jul"; break;
                            case "8": _varDetails._Month = "Aug"; break;
                            case "9": _varDetails._Month = "Sep"; break;
                            case "10": _varDetails._Month = "Oct"; break;
                            case "11": _varDetails._Month = "Nov"; break;
                            case "12": _varDetails._Month = "Dec"; break;
                        }

                        _varDetails.AccountCode = item.AccountCode;
                        _varDetails.DateCreated = item.DateCreated;
                        _varDetails.Division = item.Division;
                        _varDetails.Total = item.Total;

                        ListGridDataDetails.Add(_varDetails);

                    }
                    ListActualData.Clear();
                    FetchActivityStatusByMonth(this.SelectedYear, this.SelectedMonth, this.SelectedUser);
                    break;


            }
        }
        private void FetchOBRData()
        {
            // lblTitle.Content = "Obligation Expense and Balances for Fund Source : " + _FundSource;
            String _month = grdData.ActiveCell.Column.HeaderText.ToString();
            c_main_data.Process = "FetchOBRData";
            svc_mindaf.ExecuteImportDataSQLAsync(c_main_data.FetchOBRData(WorkingYear, this.FundName, _month));
        }
        void svc_mindaf_refresh_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_main_data.Process)
            {
                case "FetchAll":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new MainData
                                     {
                                         Apr = Convert.ToString(info.Element("Apr").Value),
                                         Aug = Convert.ToString(info.Element("Aug").Value),
                                         Dec = Convert.ToString(info.Element("Dec").Value),
                                         activity = Convert.ToString(info.Element("activity").Value),
                                         mode_of_procurement = Convert.ToString(info.Element("mode_of_procurement").Value),
                                         Feb = Convert.ToString(info.Element("Feb").Value),
                                         fiscal_year = Convert.ToString(info.Element("fiscal_year").Value),
                                         Jan = Convert.ToString(info.Element("Jan").Value),
                                         Jul = Convert.ToString(info.Element("Jul").Value),
                                         Jun = Convert.ToString(info.Element("Jun").Value),
                                         Mar = Convert.ToString(info.Element("Mar").Value),
                                         May = Convert.ToString(info.Element("May").Value),
                                         Nov = Convert.ToString(info.Element("Nov").Value),
                                         Oct = Convert.ToString(info.Element("Oct").Value),
                                         output = Convert.ToString(info.Element("output").Value),
                                         program_id = Convert.ToInt32(info.Element("program_id").Value),
                                         program_name = Convert.ToString(info.Element("program_name").Value),
                                         project_name = Convert.ToString(info.Element("project_name").Value),
                                         Sep = Convert.ToString(info.Element("Sep").Value),
                                         User_Fullname = Convert.ToString(info.Element("User_Fullname").Value),
                                         activity_id = Convert.ToString(info.Element("activity_id").Value),
                                         project_id = Convert.ToString(info.Element("project_id").Value),
                                         output_id = Convert.ToString(info.Element("output_id").Value)
                                     };

                    _ListMainData.Clear();

                    foreach (var item in _dataLists)
                    {
                        MainData _varDetails = new MainData();

                        _varDetails.activity_id = item.activity_id;
                        _varDetails.Apr = item.Apr;
                        _varDetails.Aug = item.Aug;
                        _varDetails.Dec = item.Dec;
                        _varDetails.activity = item.activity;
                        _varDetails.mode_of_procurement = item.mode_of_procurement;
                        _varDetails.Feb = item.Feb;
                        _varDetails.fiscal_year = item.fiscal_year;
                        _varDetails.Jan = item.Jan;
                        _varDetails.Jul = item.Jul;
                        _varDetails.Jun = item.Jun;
                        _varDetails.Mar = item.Mar;
                        _varDetails.May = item.May;
                        _varDetails.Nov = item.Nov;
                        _varDetails.Oct = item.Oct;
                        _varDetails.output = item.output;
                        _varDetails.program_id = item.program_id;
                        _varDetails.program_name = item.program_name;
                        _varDetails.project_name = item.project_name;
                        _varDetails.Sep = item.Sep;
                        _varDetails.User_Fullname = item.User_Fullname;
                        _varDetails.project_id = item.project_id;
                        _varDetails.output_id = item.output_id;
                        _ListMainData.Add(_varDetails);
                    }
                    if (IsAdmin == "0")
                    {
                        List<MainData> _EmptyProgram = _ListMainData.Where(item => item.User_Fullname == "").ToList();
                        List<MainData> _Temp = _ListMainData.Where(item => item.User_Fullname == Username.Replace("Welcome ", "").Trim()).ToList();
                        _ListMainData = _Temp;
                        foreach (var item in _EmptyProgram)
                        {
                            _ListMainData.Add(item);
                        }

                    }





                    this.Cursor = Cursors.Arrow;

                    GetTotals();
                    break;
            }
        }

        void c_main_data_SQLOperation(object sender, EventArgs e)
        {
            switch (c_main_data.Process)
            {
                case "AddNewRow":
                    RefreshData(this.DivisionId);
                    break;
                case "RemoveOutput":
                    RefreshData(this.DivisionId);
                    break;
                case "RemoveData":
                    RefreshData(this.DivisionId);
                    break;
                case "UpdateRowYear":
                    RefreshData(this.DivisionId);
                    break;
                case "UpdateRowModeOfProcurement":
                    RefreshData(this.DivisionId);
                    break;
                case "UpdateRowProgram":
                    UpdateTableProgramID(_updateProgram);
                    break;
                case "UpdateMainProgramID":
                    RefreshData(this.DivisionId);
                    break;
                case "UpdateRowProject":
                    RefreshData(this.DivisionId);
                    break;
                case "UpdateProjectOutput":
                    RefreshData(this.DivisionId);
                    break;
                case "UpdateOutputActivity":
                    RefreshData(this.DivisionId);
                    break;
                case "UpdateAssignedEmployee":
                    RefreshData(this.DivisionId);
                    break;
                case "Suspend":
                   FetchActivityStatusRefresh(SelectedYear,SelectedMonth, SelectedUser, SelectedActId);;
                    break;
                case "TransferMonths":
                   FetchActivityStatusRefresh(SelectedYear,SelectedMonth, SelectedUser, SelectedActId);;
                    break;
            }
        }

        void svc_mindaf_ExecuteSQLCompleted(object sender, ExecuteSQLCompletedEventArgs e)
        {
            var _results = e.Result.ToString();
            switch (c_main_data.Process)
            {
                case "FetchAll":
                    XDocument oDocKeyResults = XDocument.Parse(_results);
                    var _dataLists = from info in oDocKeyResults.Descendants("Table")
                                     select new MainData
                                     {
                                         Apr = Convert.ToString(info.Element("Apr").Value),
                                         Aug = Convert.ToString(info.Element("Aug").Value),
                                         Dec = Convert.ToString(info.Element("Dec").Value),
                                         activity = Convert.ToString(info.Element("activity").Value),
                                         mode_of_procurement = Convert.ToString(info.Element("mode_of_procurement").Value),
                                         Feb = Convert.ToString(info.Element("Feb").Value),
                                         fiscal_year = Convert.ToString(info.Element("fiscal_year").Value),
                                         Jan = Convert.ToString(info.Element("Jan").Value),
                                         Jul = Convert.ToString(info.Element("Jul").Value),
                                         Jun = Convert.ToString(info.Element("Jun").Value),
                                         Mar = Convert.ToString(info.Element("Mar").Value),
                                         May = Convert.ToString(info.Element("May").Value),
                                         Nov = Convert.ToString(info.Element("Nov").Value),
                                         Oct = Convert.ToString(info.Element("Oct").Value),
                                         output = Convert.ToString(info.Element("output").Value),
                                         program_id = Convert.ToInt32(info.Element("program_id").Value),
                                         program_name = Convert.ToString(info.Element("program_name").Value),
                                         project_name = Convert.ToString(info.Element("project_name").Value),
                                         Sep = Convert.ToString(info.Element("Sep").Value),
                                         User_Fullname = Convert.ToString(info.Element("User_Fullname").Value),
                                         activity_id = Convert.ToString(info.Element("activity_id").Value),
                                         project_id = Convert.ToString(info.Element("project_id").Value),
                                         output_id = Convert.ToString(info.Element("output_id").Value),
                                         program_index = Convert.ToString(info.Element("program_index").Value)
                                     };

                    _ListMainData.Clear();
                   
                    foreach (var item in _dataLists)
                    {
                        MainData _varDetails = new MainData();

                        _varDetails.activity_id = item.activity_id;
                         _varDetails.Apr = item.Apr;
                         _varDetails.Aug = item.Aug;
                         _varDetails.Dec = item.Dec;
                         _varDetails.activity = item.activity;
                         _varDetails.mode_of_procurement = item.mode_of_procurement;
                         _varDetails.Feb = item.Feb;
                         _varDetails.fiscal_year = item.fiscal_year;
                         _varDetails.Jan = item.Jan;
                         _varDetails.Jul = item.Jul;
                         _varDetails.Jun = item.Jun;
                         _varDetails.Mar = item.Mar;
                         _varDetails.May = item.May;
                         _varDetails.Nov = item.Nov;
                         _varDetails.Oct = item.Oct;
                         _varDetails.output = item.output;
                         _varDetails.program_id = item.program_id;
                         _varDetails.program_name = item.program_name;
                         _varDetails.project_name = item.project_name;
                         _varDetails.Sep = item.Sep;
                         _varDetails.User_Fullname = item.User_Fullname;
                         _varDetails.project_id = item.project_id;
                         _varDetails.output_id = item.output_id;
                         _varDetails.program_index = item.program_index;
                        _ListMainData.Add(_varDetails);
                    }
                    if (IsAdmin=="0")
                    {
                        List<MainData> _EmptyProgram = _ListMainData.Where(item => item.User_Fullname == "").ToList();
                        List<MainData> _Temp = _ListMainData.Where(item => item.User_Fullname == Username.Replace("Welcome ", "").Trim()).ToList();
                        _ListMainData = _Temp;
                        foreach (var item in _EmptyProgram)
                        {
                            _ListMainData.Add(item);
                        }
                    
                    }
                    
                   
                  
                   
                  
                    this.Cursor = Cursors.Arrow;

                    GetTotals();
                    break;
               case "FetchTotals":
                    XDocument oDocKeyFetchTotals = XDocument.Parse(_results);
                    var _dataListsFetchTotals = from info in oDocKeyFetchTotals.Descendants("Table")
                                     select new MainTotals
                                     {
                                         activity_id = Convert.ToString(info.Element("activity_id").Value),
                                         month = Convert.ToString(info.Element("month").Value),
                                         total = Convert.ToString(info.Element("total").Value)
                                        
                                     };

                    _ListMainTotals.Clear();


                    foreach (var item in _dataListsFetchTotals)
                    {
                        MainTotals _varDetails = new MainTotals();

                        _varDetails.activity_id = item.activity_id;
                        _varDetails.month = item.month;
                        _varDetails.total = item.total;

                         _ListMainTotals.Add(_varDetails);
                    }

                    DistributeData();
                    this.Cursor = Cursors.Arrow;
                                       
                    grdData.ItemsSource = null;
                    grdData.ItemsSource = _ListMainData;

                    grdData.Columns.DataColumns["activity"].AllowToolTips = AllowToolTips.Always;
                    grdData.Columns.DataColumns["project_name"].AllowToolTips = AllowToolTips.Overflow;
                    grdData.Columns.DataColumns["program_name"].AllowToolTips = AllowToolTips.Overflow;
                    grdData.Columns.DataColumns["output"].AllowToolTips = AllowToolTips.Overflow;
                    grdData.Columns.DataColumns["mode_of_procurement"].AllowToolTips = AllowToolTips.Overflow;

                    Column col_project_name = grdData.Columns.DataColumns["project_name"];
                    Column col_program_name = grdData.Columns.DataColumns["program_name"];
                    Column col_User_Fullname = grdData.Columns.DataColumns["User_Fullname"];
                    Column col_fiscal_year = grdData.Columns.DataColumns["fiscal_year"];
                    Column col_mode_of_procurement = grdData.Columns.DataColumns["mode_of_procurement"];
                    Column col_activity = grdData.Columns.DataColumns["activity"];
                    Column col_output = grdData.Columns.DataColumns["output"];

                    //col_fiscal_year.Width = new ColumnWidth(70, false);
                    //col_program_name.Width = new ColumnWidth(250, false);
                    //col_User_Fullname.Width = new ColumnWidth(100, false);
                    //col_activity.Width = new ColumnWidth(120,false);
                    //col_output.Width = new ColumnWidth(120,false);
                    //col_project_name.Width = new ColumnWidth(150, false);
                
                    col_fiscal_year.Width = new ColumnWidth(60, false);
                    col_program_name.Width = new ColumnWidth(120, false);
                    col_User_Fullname.Width = new ColumnWidth(100, false);
                    col_mode_of_procurement.Width = new ColumnWidth(100, false);
                    col_activity.Width = new ColumnWidth(200, false);
                    col_output.Width = new ColumnWidth(100, false);
                    col_project_name.Width = new ColumnWidth(120, false);

                    //Column col_pap_total = grdData.Columns.DataColumns["pap_total"];
                    Column col_jan = grdData.Columns.DataColumns["Jan"];
                    Column col_feb = grdData.Columns.DataColumns["Feb"];
                    Column col_mar = grdData.Columns.DataColumns["Mar"];
                    Column col_apr = grdData.Columns.DataColumns["Apr"];
                    Column col_may = grdData.Columns.DataColumns["May"];
                    Column col_jun = grdData.Columns.DataColumns["Jun"];
                    Column col_jul = grdData.Columns.DataColumns["Jul"];
                    Column col_aug = grdData.Columns.DataColumns["Aug"];
                    Column col_sep = grdData.Columns.DataColumns["Sep"];
                    Column col_oct = grdData.Columns.DataColumns["Oct"];
                    Column col_nov = grdData.Columns.DataColumns["Nov"];
                    Column col_dec = grdData.Columns.DataColumns["Dec"];

                    //col_pap_total.Width = new ColumnWidth(100, false);
                    col_jan.Width = new ColumnWidth(100,false);
                    col_feb.Width = new ColumnWidth(100, false);
                    col_mar.Width = new ColumnWidth(100, false);
                    col_apr.Width = new ColumnWidth(100, false);
                    col_may.Width = new ColumnWidth(100, false);
                    col_jun.Width = new ColumnWidth(100, false);
                    col_jul.Width = new ColumnWidth(100, false);
                    col_aug.Width = new ColumnWidth(100, false);
                    col_sep.Width = new ColumnWidth(100, false);
                    col_oct.Width = new ColumnWidth(100, false);
                    col_nov.Width = new ColumnWidth(100, false);
                    col_dec.Width = new ColumnWidth(100, false);

                    grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_fiscal_year);
                    grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_program_name);
                    grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_project_name);
                    grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_output);
                    grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_activity);
                    grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_mode_of_procurement);
                    grdData.FixedColumnSettings.FixedColumnsLeft.Add(col_User_Fullname);

                    switch (IsAdmin)
	                {
                        case "1":
                            grdData.Columns["User_Fullname"].Visibility = System.Windows.Visibility.Visible;
                            break;

                        case "2":
                            grdData.Columns["User_Fullname"].Visibility = System.Windows.Visibility.Visible;
                            break;
                        case "0":
                            grdData.Columns["User_Fullname"].Visibility = System.Windows.Visibility.Collapsed;
                            break; 
	                }
                    

                    grdData.Columns["activity_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["program_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["project_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["output_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["div_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdData.Columns["program_index"].Visibility = System.Windows.Visibility.Collapsed;
                    FetchYearPrograms();
                    FetchStatusPPMPStatus();
                    break;
               case "FetchYear":
                    XDocument oDocKeyFetchYear = XDocument.Parse(_results);
                    var _dataListsFetchYear = from info in oDocKeyFetchYear.Descendants("Table")
                                                select new Main_OverAllYear
                                                {
                                                    Fiscal_Year = Convert.ToString(info.Element("Fiscal_Year").Value)

                                                };

                    _ListOverAllYear.Clear();


                    foreach (var item in _dataListsFetchYear)
                    {
                        Main_OverAllYear _varDetails = new Main_OverAllYear();

                        _varDetails.Fiscal_Year = item.Fiscal_Year;

                        _ListOverAllYear.Add(_varDetails);
                    }
                   
                    this.Cursor = Cursors.Arrow;

                    break;
               case "FetchStatusPPMPStatus":
                    XDocument oDocKeyFetchStatusPPMPStatus = XDocument.Parse(_results);
                    var _dataListsFetchStatusPPMPStatus = from info in oDocKeyFetchStatusPPMPStatus.Descendants("Table")
                                                 select new PPMPStatus
                                                {
                                                     Status = Convert.ToString(info.Element("status").Value)

                                                };


                    Boolean _locked = false;

                    foreach (var item in _dataListsFetchStatusPPMPStatus)
                    {
                        PPMPStatus _varDetails = new PPMPStatus();

                     //   lblStatus.Content = "PPMP Data has been " + item.Status;

                        switch (item.Status)
                        {
                            case "Locked":
                                _locked = true;
                                break;
                        }
                        break;
                    }
                    switch (_locked)
	                {
                        case true:
                            IsLocked = true;
                            lblStatus.Content = "PPMP Data locked.";
                            break;
                        case false:
                            IsLocked = false;
                            lblStatus.Content = "PPMP Data unlocked";
                            break;
	                }
                   
                    this.Cursor = Cursors.Arrow;

                    break;
               case "FetchProgramID":
                    XDocument oDocKeyFetchProgramID = XDocument.Parse(_results);
                    var _dataListsFetchProgramID = from info in oDocKeyFetchProgramID.Descendants("Table")
                                                select new FetchProgramId
                                                {
                                                    id = Convert.ToString(info.Element("id").Value)

                                                };

                    String _program_id = "";
                    String _id = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["program_id"].Value.ToString();

                    foreach (var item in _dataListsFetchProgramID)
                    {
                        FetchProgramId _varDetails = new FetchProgramId();

                        _program_id = item.id;
                        break;

                    }
                    UpdateMainProgramID(_program_id, _id);
                    this.Cursor = Cursors.Arrow;

                    break;  
                case "FetchProjectID":
                    XDocument oDocKeyFetchProjectID = XDocument.Parse(_results);
                    var _dataListsFetchProjectID = from info in oDocKeyFetchProjectID.Descendants("Table")
                                                select new FetchProjectId
                                                {
                                                    id = Convert.ToString(info.Element("id").Value)

                                                };

                    String _project_id = "";
                     _id = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["program_id"].Value.ToString();

                     foreach (var item in _dataListsFetchProjectID)
                    {
                        FetchProgramId _varDetails = new FetchProgramId();

                        _project_id = item.id;
                        break;

                    }
                     UpdateMainProjectID(_project_id, _id);
                    this.Cursor = Cursors.Arrow;

                    break;
                case "FetchFundSource":
                    XDocument oDocKeyResultsFetchFundSource = XDocument.Parse(_results);
                    var _dataListsFetchFundSource = from info in oDocKeyResultsFetchFundSource.Descendants("Table")
                                                    select new BudgetRevisionFundSource
                                                    {
                                                        Code = Convert.ToString(info.Element("code").Value),
                                                        Name = Convert.ToString(info.Element("respo_name").Value)
                                                    };



                    //foreach (var item in _dataListsFetchFundSource)
                    //{
                    //    if (this.FundName== item.Name)
                    //    {
                    //        FundSource = item.Code;
                    //        FundName = item.Name;
                    //        break;
                    //    }
                        


                    //}
                    FetchExpenseItems();
                    break;

                case "FetchExpenseItems":
                    XDocument oDocKeyResultsFetchExpenseItems = XDocument.Parse(_results);
                    var _dataListsFetchExpenseItems = from info in oDocKeyResultsFetchExpenseItems.Descendants("Table")
                                                      select new ListExpenseMain
                                                    {
                                                        UACS = Convert.ToString(info.Element("uacs_code").Value),
                                                        Name = Convert.ToString(info.Element("name").Value)
                                                      

                                                    };
                    ListExpenseItems.Clear();


                    foreach (var item in _dataListsFetchExpenseItems)
                    {
                        ListExpenseMain _var = new ListExpenseMain();

                        _var.UACS = item.UACS;
                        _var.Name = item.Name;

                        ListExpenseItems.Add(_var);
                    }

                    FetchOBRData();
                    break;
                case "FetchActivityStatusRefresh":
                    XDocument oDocKeyResultsFetchActivityStatusr = XDocument.Parse(_results);
                    var _dataListsFetchActivityStatusr = from info in oDocKeyResultsFetchActivityStatusr.Descendants("Table")
                                                        select new MainDashboardActivityData
                                                        {
                                                            act_id = Convert.ToString(info.Element("act_id").Value),
                                                            accountable_id = Convert.ToString(info.Element("accountable_id").Value),
                                                            activity_id = Convert.ToString(info.Element("activity_id").Value),
                                                            destination = Convert.ToString(info.Element("destination").Value),
                                                            name = Convert.ToString(info.Element("name").Value),
                                                            no_days = Convert.ToString(info.Element("no_days").Value),
                                                            no_staff = Convert.ToString(info.Element("no_staff").Value),
                                                            quantity = Convert.ToString(info.Element("quantity").Value),
                                                            remarks = Convert.ToString(info.Element("remarks").Value),
                                                            total = Convert.ToString(info.Element("total").Value),
                                                            type_service = Convert.ToString(info.Element("type_service").Value)

                                                        };


                    ListActivityStatus.Clear();

                    foreach (var item in _dataListsFetchActivityStatusr)
                    {
                        MainDashboardActivityData _varProf = new MainDashboardActivityData();

                        _varProf.act_id = item.act_id;
                        _varProf.accountable_id = item.accountable_id;
                        _varProf.activity_id = item.activity_id;
                        _varProf.destination = item.destination;
                        _varProf.name = item.name;
                        _varProf.no_days = item.no_days;
                        _varProf.no_staff = item.no_staff;
                        _varProf.quantity = item.quantity;
                        _varProf.remarks = item.remarks;
                        _varProf.total = Convert.ToDouble(item.total).ToString("#,##0.00");
                        _varProf.type_service = item.type_service;
                        ListActivityStatus.Add(_varProf);

                    }
                    grdStatus.ItemsSource = null;
                    grdStatus.ItemsSource = ListActivityStatus;

                    grdData.Columns["activity_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdStatus.Columns["act_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdStatus.Columns["activity_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdStatus.Columns["accountable_id"].Visibility = System.Windows.Visibility.Collapsed;
                    RefreshData(this.DivisionId);
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchActivityStatus":
                    XDocument oDocKeyResultsFetchActivityStatus = XDocument.Parse(_results);
                    var _dataListsFetchActivityStatus = from info in oDocKeyResultsFetchActivityStatus.Descendants("Table")
                                                        select new MainDashboardActivityData
                                                        {
                                                            act_id = Convert.ToString(info.Element("act_id").Value),
                                                            accountable_id = Convert.ToString(info.Element("accountable_id").Value),
                                                            activity_id = Convert.ToString(info.Element("activity_id").Value),
                                                            destination = Convert.ToString(info.Element("destination").Value),
                                                            name = Convert.ToString(info.Element("name").Value),
                                                            no_days = Convert.ToString(info.Element("no_days").Value),
                                                            no_staff = Convert.ToString(info.Element("no_staff").Value),
                                                            quantity = Convert.ToString(info.Element("quantity").Value),
                                                            remarks = Convert.ToString(info.Element("remarks").Value),
                                                            total = Convert.ToString(info.Element("total").Value),
                                                            type_service = Convert.ToString(info.Element("type_service").Value)

                                                        };


                    ListActivityStatus.Clear();

                    foreach (var item in _dataListsFetchActivityStatus)
                    {
                        MainDashboardActivityData _varProf = new MainDashboardActivityData();

                        _varProf.act_id = item.act_id;
                        _varProf.accountable_id = item.accountable_id;
                        _varProf.activity_id = item.activity_id;
                        _varProf.destination= item.destination;
                        _varProf.name = item.name;
                        _varProf.no_days = item.no_days;
                        _varProf.no_staff = item.no_staff;
                        _varProf.quantity = item.quantity;
                        _varProf.remarks = item.remarks;
                        _varProf.total = Convert.ToDouble(item.total).ToString("#,##0.00");
                        _varProf.type_service = item.type_service;
                        ListActivityStatus.Add(_varProf);

                    }
                    grdStatus.ItemsSource = null;
                    grdStatus.ItemsSource = ListActivityStatus;

                    grdData.Columns["activity_id"].Visibility  = System.Windows.Visibility.Collapsed;
                    grdStatus.Columns["act_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdStatus.Columns["activity_id"].Visibility = System.Windows.Visibility.Collapsed;
                    grdStatus.Columns["accountable_id"].Visibility = System.Windows.Visibility.Collapsed;
                    FetchFundSource();
                    this.Cursor = Cursors.Arrow;
                    break;
                case "FetchActivityStatusByMonth":
                    XDocument oDocKeyResultsFetchActivityStatusByMonth = XDocument.Parse(_results);
                    var _dataListsFetchActivityStatusByMonth = from info in oDocKeyResultsFetchActivityStatusByMonth.Descendants("Table")
                                                        select new MainDashboardActivityData
                                                        {
                                                            act_id = Convert.ToString(info.Element("act_id").Value),
                                                            accountable_id = Convert.ToString(info.Element("accountable_id").Value),
                                                            activity_id = Convert.ToString(info.Element("activity_id").Value),
                                                            destination = Convert.ToString(info.Element("destination").Value),
                                                            name = Convert.ToString(info.Element("name").Value),
                                                            no_days = Convert.ToString(info.Element("no_days").Value),
                                                            no_staff = Convert.ToString(info.Element("no_staff").Value),
                                                            quantity = Convert.ToString(info.Element("quantity").Value),
                                                            remarks = Convert.ToString(info.Element("remarks").Value),
                                                            total = Convert.ToString(info.Element("total").Value),
                                                            type_service = Convert.ToString(info.Element("type_service").Value)

                                                        };


                    ListActivityStatusByMonth.Clear();

                    foreach (var item in _dataListsFetchActivityStatusByMonth)
                    {
                        MainDashboardActivityData _varProf = new MainDashboardActivityData();

                        _varProf.act_id = item.act_id;
                        _varProf.accountable_id = item.accountable_id;
                        _varProf.activity_id = item.activity_id;
                        _varProf.destination = item.destination;
                        _varProf.name = item.name;
                        _varProf.no_days = item.no_days;
                        _varProf.no_staff = item.no_staff;
                        _varProf.quantity = item.quantity;
                        _varProf.remarks = item.remarks;
                        _varProf.total = Convert.ToDouble(item.total).ToString("#,##0.00");
                        _varProf.type_service = item.type_service;
                        ListActivityStatusByMonth.Add(_varProf);

                    }
                    
                    foreach (var item in ListExpenseItems)
                    {
                       
                        List<OBR_Details> _ListN = ListGridDataDetails.Where(i => i.AccountCode == item.UACS).ToList();

                        if (_ListN.Count!=0)
                        {
                            ActualDataMain _vardata = new ActualDataMain();

                            _vardata.AccountCode = item.UACS;
                            _vardata.ExpenseItem = item.Name;
                            double _total = 0.00;
                            double _budget = 0.00;
                            foreach (var itemd in _ListN)
                            {
                                _total += Convert.ToDouble(itemd.Total);
                            }
                           
                            _vardata.Actual_OBR = _total.ToString("#,##0.00");

                            List<MainDashboardActivityData> _ListExp = ListActivityStatusByMonth.Where(x => x.name == item.Name).ToList();
                            foreach (var itemd in _ListExp)
                            {
                                _budget += Convert.ToDouble(itemd.total);
                            }
                            _vardata.Planned = _budget.ToString("#,##0.00");
                            _vardata.Balance = (_budget - _total).ToString("#,##0.00");
                            ListActualData.Add(_vardata);

                        }
                     

                    }

                    grdLive.ItemsSource = null;
                    grdLive.ItemsSource = ListActualData;

                    Column col_output_AccountCode = grdLive.Columns.DataColumns["AccountCode"];
                    col_output_AccountCode.Width = new ColumnWidth(100, false);

                    Column col_output_ExpenseItem = grdLive.Columns.DataColumns["ExpenseItem"];
                    col_output_ExpenseItem.Width = new ColumnWidth(250, true);

                    Column col_output_Actual_OBR = grdLive.Columns.DataColumns["Actual_OBR"];
                    col_output_Actual_OBR.Width = new ColumnWidth(90, false);
                    col_output_Actual_OBR.HeaderText = "Actual OBR";

                    Column col_output_Planned = grdLive.Columns.DataColumns["Planned"];
                    col_output_Planned.Width = new ColumnWidth(90, false);

                    Column col_output_Balance = grdLive.Columns.DataColumns["Balance"];
                    col_output_Balance.Width = new ColumnWidth(90, false);
                    col_output_Balance.HeaderText = "Accounted";

                   
                    this.Cursor = Cursors.Arrow;
                    break;
            }
        }
        private void FetchFundSource()
        {
            c_main_data.Process = "FetchFundSource";
            svc_mindaf.ExecuteSQLAsync(c_main_data.FetchFundSource(this.DivisionId));
        }

        public void UpdateMainProgramID(string _program_id,string _id) 
        {
            c_main_data.Process = "UpdateMainProgramID";
            c_main_data.UpdateProgramID(_program_id,_id);
        }
        public void UpdateMainProjectID(string _program_id, string _id)
        {
            c_main_data.Process = "UpdateMainProjectID";
            c_main_data.UpdateProgramID(_program_id, _id);
        }
        public void AddNewData() 
        {
            c_main_data.Process = "AddNewRow";
            c_main_data.AddNewRow(this.DivisionId,this.FundName,this.WorkingYear,this.UserId);
        }
        public void RemoveData()
        {
            MessageBoxResult result = MessageBox.Show("Warning!! This will Remove the selected data."+ Environment.NewLine +"",
            "Confirm Data Removal", MessageBoxButton.OKCancel);

            if (result == MessageBoxResult.OK)
            {
                string _program_id = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["program_id"].Value.ToString();
                string _project_id = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["project_id"].Value.ToString();
                string _output_id = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["output_id"].Value.ToString();
                string _activity_id = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                c_main_data.Process = "RemoveData";

                Int32  x_output = _ListMainData.Where(x => x.output_id == _output_id && _output_id!="0").ToList().Count();
                Int32 x_project = _ListMainData.Where(x => x.project_id == _project_id && _project_id !="0").ToList().Count();
                String _selection = "";

                if (x_project >1)
                {
                    
                }
                if (x_output <=0)
                {
                    if (x_project >1)
                    {
                        _selection = "project";
                    }
                    else if (x_project <= 1)
                    {
                        _selection = "program";
                    }
                }
                else
                {
                    if (x_output > 1)
                    {
                        _selection = "activity";
                    }
                    else if (x_output == 1)
                    {
                        _selection = "output";
                    }
                }
               
               
                c_main_data.RemoveData(_program_id, _project_id, _output_id, _activity_id,_selection);
            }

          
        }
        private void FetchYearPrograms() 
        {
            //c_main_data.Process = "FetchYear";
            //svc_mindaf.ExecuteSQLAsync(c_main_data.FetchOverAllYear());
        }
        private void DistributeData() 
        {
               Decimal     _Jan =0;
                  Decimal  _Feb =0;
                  Decimal  _Mar =0;
                  Decimal  _Apr =0;
                  Decimal  _May =0;
                  Decimal  _Jun=0;
                  Decimal  _Jul =0;
                  Decimal  _Aug =0;
                  Decimal  _Sep =0;
                  Decimal  _Oct =0;
                  Decimal  _Nov =0;
                  Decimal  _Dec = 0;
                  Decimal _pap_totals = 0;
            foreach (var item in _ListMainData)
            {
                if (item.activity_id =="189")
                {

                }
                 List<MainTotals> y = _ListMainTotals.Where(items => items.activity_id == item.activity_id).ToList();
                    foreach (var item_details in y)
                    {
                        switch (item_details.month)
                        {
                            case "Jan":
                                item.Jan = (Convert.ToDecimal(item.Jan) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                                _Jan += Convert.ToDecimal(item_details.total);
                                break;
                            case "Feb":
                                item.Feb =(Convert.ToDecimal(item.Feb) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                                _Feb += Convert.ToDecimal(item_details.total);
                                break;
                            case "Mar":
                                item.Mar = (Convert.ToDecimal(item.Mar) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                                _Mar += Convert.ToDecimal(item_details.total);
                                break;
                            case "Apr":
                                item.Apr = (Convert.ToDecimal(item.Apr) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                                _Apr += Convert.ToDecimal(item_details.total);
                                break;
                            case "May":
                                item.May = (Convert.ToDecimal(item.May) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                                _May += Convert.ToDecimal(item_details.total);
                                break;
                            case "Jun":
                                item.Jun = (Convert.ToDecimal(item.Jun) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                                _Jun += Convert.ToDecimal(item_details.total);
                                break;
                            case "Jul":
                                item.Jul = (Convert.ToDecimal(item.Jul) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                                _Jul += Convert.ToDecimal(item_details.total);
                                break;
                            case "Aug":
                                item.Aug = (Convert.ToDecimal(item.Aug) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                                _Aug += Convert.ToDecimal(item_details.total);
                                break;
                            case "Sep":
                                item.Sep = (Convert.ToDecimal(item.Sep) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                                _Sep += Convert.ToDecimal(item_details.total);
                                break;
                            case "Oct":
                                item.Oct = (Convert.ToDecimal(item.Oct) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                                _Oct += Convert.ToDecimal(item_details.total);
                                break;
                            case "Nov":
                                item.Nov = (Convert.ToDecimal(item.Nov) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                                _Nov += Convert.ToDecimal(item_details.total);
                                break;
                            case "Dec":
                                item.Dec = (Convert.ToDecimal(item.Dec) + Convert.ToDecimal(item_details.total)).ToString("#,##0.00");
                                _Dec += Convert.ToDecimal(item_details.total);
                                break;
                            default:
                                break;
                        }
                        _pap_totals = _Jan + _Feb + _Mar + _Apr + _May + _Jun + _Jul + _Aug + _Sep + _Oct + _Nov + _Dec;
                    }
            }
            AddGrandTotals(_Jan, _Feb, _Mar, _Apr, _May, _Jun, _Jul, _Aug, _Sep, _Oct, _Nov, _Dec);
        }

        private void AddGrandTotals(
            Decimal _jan,Decimal _feb,Decimal _mar,
            Decimal _apr,Decimal _may,Decimal _jun,
            Decimal _jul,Decimal _aug,Decimal _sep,
            Decimal _oct,Decimal _nov,Decimal _dec
            ) 
        {
            List<MainData> _OverAll = new List<MainData>();
            MainData _varDetails;

     

             _varDetails = new MainData();

            _varDetails.output = "";
            _varDetails.program_id = 0;
            _varDetails.program_name = "";
            _varDetails.project_name = "";
            _varDetails.User_Fullname = "Grand Total :";
            _varDetails.fiscal_year = "";
            _varDetails.activity = "";
            _varDetails.mode_of_procurement = "";
            _varDetails.activity_id = "";
            _varDetails.Jan = Convert.ToDouble(_jan).ToString("#,##0.00");
            _varDetails.Feb = Convert.ToDouble(_feb).ToString("#,##0.00");
            _varDetails.Mar = Convert.ToDouble(_mar).ToString("#,##0.00");
            _varDetails.Apr = Convert.ToDouble(_apr).ToString("#,##0.00");
            _varDetails.May = Convert.ToDouble(_may).ToString("#,##0.00");
            _varDetails.Jun = Convert.ToDouble(_jun).ToString("#,##0.00");
            _varDetails.Jul = Convert.ToDouble(_jul).ToString("#,##0.00");
            _varDetails.Aug = Convert.ToDouble(_aug).ToString("#,##0.00");
            _varDetails.Sep = Convert.ToDouble(_sep).ToString("#,##0.00");
            _varDetails.Oct = Convert.ToDouble(_oct).ToString("#,##0.00");
            _varDetails.Nov = Convert.ToDouble(_nov).ToString("#,##0.00");
            _varDetails.Dec = Convert.ToDouble(_dec).ToString("#,##0.00");

            _ListMainData.Add(_varDetails);

            _OverAll.Add(_varDetails);
            grdTotals.ItemsSource = null;
            grdTotals.ItemsSource = _OverAll;
           grdTotals.HeaderVisibility = System.Windows.Visibility.Collapsed;

           switch (IsAdmin)
           {
               case "1":
                   grdTotals.Columns["User_Fullname"].Visibility = System.Windows.Visibility.Visible;
                   break;

               case "2":
                   grdTotals.Columns["User_Fullname"].Visibility = System.Windows.Visibility.Visible;
                   break;
               case "0":
                   try
                   {
                       grdTotals.Columns["User_Fullname"].Visibility = System.Windows.Visibility.Collapsed;
                   }
                   catch (Exception)
                   {

                   }
                
                   break;
           }

            Column col_project_name = grdTotals.Columns.DataColumns["project_name"];
            Column col_program_name = grdTotals.Columns.DataColumns["program_name"];
            Column col_User_Fullname = grdTotals.Columns.DataColumns["User_Fullname"];
            Column col_fiscal_year = grdTotals.Columns.DataColumns["fiscal_year"];
            Column col_activity = grdTotals.Columns.DataColumns["activity"];
            Column col_mode_of_procurement = grdTotals.Columns.DataColumns["mode_of_procurement"];
            Column col_output = grdTotals.Columns.DataColumns["output"];

            col_fiscal_year.Width = new ColumnWidth(60, false);
            col_program_name.Width = new ColumnWidth(200, false);
            col_User_Fullname.Width = new ColumnWidth(75, false);
            col_activity.Width = new ColumnWidth(150, false);
            col_output.Width = new ColumnWidth(100, false);
            col_project_name.Width = new ColumnWidth(150, false);
            col_mode_of_procurement.Width = new ColumnWidth(100, false);

            Column col_jan = grdTotals.Columns.DataColumns["Jan"];
            Column col_feb = grdTotals.Columns.DataColumns["Feb"];
            Column col_mar = grdTotals.Columns.DataColumns["Mar"];
            Column col_apr = grdTotals.Columns.DataColumns["Apr"];
            Column col_may = grdTotals.Columns.DataColumns["May"];
            Column col_jun = grdTotals.Columns.DataColumns["Jun"];
            Column col_jul = grdTotals.Columns.DataColumns["Jul"];
            Column col_aug = grdTotals.Columns.DataColumns["Aug"];
            Column col_sep = grdTotals.Columns.DataColumns["Sep"];
            Column col_oct = grdTotals.Columns.DataColumns["Oct"];
            Column col_nov = grdTotals.Columns.DataColumns["Nov"];
            Column col_dec = grdTotals.Columns.DataColumns["Dec"];

            col_jan.Width = new ColumnWidth(100, false);
            col_feb.Width = new ColumnWidth(100, false);
            col_mar.Width = new ColumnWidth(100, false);
            col_apr.Width = new ColumnWidth(100, false);
            col_may.Width = new ColumnWidth(100, false);
            col_jun.Width = new ColumnWidth(100, false);
            col_jul.Width = new ColumnWidth(100, false);
            col_aug.Width = new ColumnWidth(100, false);
            col_sep.Width = new ColumnWidth(100, false);
            col_oct.Width = new ColumnWidth(100, false);
            col_nov.Width = new ColumnWidth(100, false);
            col_dec.Width = new ColumnWidth(100, false);

            grdTotals.FixedColumnSettings.FixedColumnsLeft.Add(col_fiscal_year);
            grdTotals.FixedColumnSettings.FixedColumnsLeft.Add(col_program_name);
            grdTotals.FixedColumnSettings.FixedColumnsLeft.Add(col_project_name);
            grdTotals.FixedColumnSettings.FixedColumnsLeft.Add(col_output);
            grdTotals.FixedColumnSettings.FixedColumnsLeft.Add(col_activity);
            grdTotals.FixedColumnSettings.FixedColumnsLeft.Add(col_mode_of_procurement);
            grdTotals.FixedColumnSettings.FixedColumnsLeft.Add(col_User_Fullname);

            grdTotals.Columns["project_name"].HeaderText = "";

            grdTotals.Columns["activity_id"].Visibility = System.Windows.Visibility.Collapsed;
           grdTotals.Columns["program_id"].Visibility = System.Windows.Visibility.Visible;
            grdTotals.Columns["project_id"].Visibility = System.Windows.Visibility.Collapsed;
            grdTotals.Columns["output_id"].Visibility = System.Windows.Visibility.Collapsed;
            grdTotals.Columns["div_id"].Visibility = System.Windows.Visibility.Collapsed;
        }

      
        public void RefreshData(String Division_ID) 
        {
           // this.FundSource = f_expenditures.FundSourceId;
            c_main_data.Process = "FetchAll";
            if (this.IsAdmin=="1")
            {
                svc_mindaf.ExecuteSQLAsync(c_main_data.FetchDataPersonal(WorkingYear,DivisionId, this.UserId, Username,this.FundSource,this.FundName));
               // svc_mindaf.ExecuteSQLAsync(c_main_data.FetchData(DivisionId));
               
            }
            else
            {
                svc_mindaf.ExecuteSQLAsync(c_main_data.FetchDataPersonal(WorkingYear, DivisionId, this.UserId, Username, this.FundSource, this.FundName));
            }
           
    
        }
        private void RefreshDataUpdated(String Division_ID)
        {
           // this.FundSource = f_expenditures.FundSourceId;
            c_main_data.Process = "FetchAll";
            //if (this.IsAdmin == "1")
            //{
            //    svc_mindaf_refresh.ExecuteSQLAsync(c_main_data.FetchData(DivisionId));

            //}
            //else
           // {
                svc_mindaf_refresh.ExecuteSQLAsync(c_main_data.FetchDataPersonal(WorkingYear,DivisionId, this.UserId, Username, this.FundSource,this.FundName));
           // }


        }
        private void GetTotals()
        {
            c_main_data.Process = "FetchTotals";
            svc_mindaf.ExecuteSQLAsync(c_main_data.FetchTotals(this.FundSource));

        }
        private void FetchStatusPPMPStatus()
        {
            c_main_data.Process = "FetchStatusPPMPStatus";
            svc_mindaf.ExecuteSQLAsync(c_main_data.FetchPPMPStatus(this.FundName));

        }
        private void ctrl_main_dashboard_Loaded(object sender, RoutedEventArgs e)
        {
            RefreshData(this.DivisionId);
        }

      

        private void grdData_CellDoubleClicked(object sender, CellClickedEventArgs e)
        {
          //  this.FundSource = f_expenditures.FundSourceId;
            String ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
            if (grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString() == "Grand Total :")
            {
                return;
            }
            if (e.Cell.Column.HeaderText.ToString() == "Jan" ||
               e.Cell.Column.HeaderText.ToString() == "Feb" ||
               e.Cell.Column.HeaderText.ToString() == "Mar" ||
               e.Cell.Column.HeaderText.ToString() == "Apr" ||
               e.Cell.Column.HeaderText.ToString() == "May" ||
               e.Cell.Column.HeaderText.ToString() == "Jun" ||
               e.Cell.Column.HeaderText.ToString() == "Jul" ||
               e.Cell.Column.HeaderText.ToString() == "Aug" ||
               e.Cell.Column.HeaderText.ToString() == "Sep" ||
               e.Cell.Column.HeaderText.ToString() == "Oct" ||
               e.Cell.Column.HeaderText.ToString() == "Nov" ||
               e.Cell.Column.HeaderText.ToString() == "Dec"
           )
            {
                if (grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString()=="Grand Total :")
                {
                    return;
                }
                if (ActivityId != "0")
                {
                      //if (IsLocked)
                      //{
                      //      MessageBox.Show("PPMP has already been locked and recorded." + Environment.NewLine + "Cannot modify locked PPMP");
                      //      return;
                      //}
                      //else
                      //{

                          f_expenditures = new frmListExpenditures();
                          f_expenditures._Month = e.Cell.Column.HeaderText.ToString();
                          f_expenditures.DivisionId = this.DivisionId;
                          f_expenditures.FundName = this.FundName;
                          f_expenditures.FundSource = this.FundSource;
                          f_expenditures.WorkingYear = this.WorkingYear;
                          f_expenditures.cmbFundSource.IsEnabled = false;
                          f_expenditures.SelectedExpenditure += f_expenditures_SelectedExpenditure;
                          f_expenditures.Show();
                      //}

                }
                else
                {
                        MessageBox.Show("Select a program that has Activities and Assigned Person");

                }
                   

           }
            else if (e.Cell.Column.HeaderText.ToString() == "User_Fullname")
                {
                    if (IsAdmin == "1" || IsAdmin == "2")
                    {
                        f_employee.DivisionId = this.DivisionId;
                        f_employee.AssignedUpdate += f_employee_AssignedUpdate;
                        f_employee.Show();
                    }

                    
                }
            else if (e.Cell.Column.HeaderText.ToString() == "program_name")
                {
                    if (IsAdmin == "1" || IsAdmin == "2")
                    {
                        String _Program = grdData.Rows[e.Cell.Row.Index].Cells["program_name"].Value.ToString().Replace("'", "`");
                        String _ProgramCode = grdData.Rows[e.Cell.Row.Index].Cells["program_index"].Value.ToString();
                        programeditor fEdit = new programeditor();
                        fEdit._Data = _Program;
                        fEdit._Menu = "program";
                        fEdit._ProgramCode = _ProgramCode;
                        fEdit.ReloadData += fEdit_ReloadData;
                        fEdit.Show();
                    }

                    
                }
            else if (e.Cell.Column.HeaderText.ToString() == "project_name")
            {
                if (IsAdmin == "1" || IsAdmin == "2")
                {
                    String _Project = grdData.Rows[e.Cell.Row.Index].Cells["project_name"].Value.ToString().Replace("'", "`");
                    String _ProjectCode = grdData.Rows[e.Cell.Row.Index].Cells["project_id"].Value.ToString();
                    programeditor fEdit = new programeditor();
                    fEdit._Data = _Project;
                    fEdit._Menu = "project";
                    fEdit._ProjectCode = _ProjectCode;
                    fEdit.ReloadData += fEdit_ReloadData;
                    fEdit.Show();
                }


            }
            else if (e.Cell.Column.HeaderText.ToString() == "output")
                {

                    f_verify = new frmYESNO();
                    f_verify.ResultData+=f_verify_ResultData;
                    f_verify.Show();
                    
                  
                }
            else if (e.Cell.Column.HeaderText.ToString() == "activity")
            {
                f_verify_act = new frmYESNO();
                f_verify_act.ResultData += f_verify_act_ResultData;
                f_verify_act.Show();
               
            }

            else if (e.Cell.Column.HeaderText.ToString() == "mode_of_procurement")
            {
                if (IsAdmin == "1" || IsAdmin == "2")
                {
                    String _ModeOfProcurement = grdData.Rows[e.Cell.Row.Index].Cells["mode_of_procurement"].Value.ToString().Replace("'", "`");
                    String _ActivityCode = grdData.Rows[e.Cell.Row.Index].Cells["activity_id"].Value.ToString();
                    programeditor fEdit = new programeditor();
                    fEdit._Data = _ModeOfProcurement;
                    fEdit._Menu = "mode_of_procurement";
                    fEdit._ActivityCode = _ActivityCode;
                    fEdit.ReloadData += fEdit_ReloadData;
                    fEdit.Show();
                }


            }
        

        }

        void f_verify_act_ResultData(object sender, EventArgs e)
        {
            if (f_verify_act.IsModify)
            {
                String _activity = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity"].Value.ToString().Replace("'", "`");
                String _activity_code = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                programeditor fEdit = new programeditor();
                fEdit._Data = _activity;
                fEdit._Menu = "activity";
                fEdit._ActivityCode = _activity_code;
                fEdit.ReloadData += fEdit_ReloadData;
                fEdit.Show();
            }
            else
            {
                if (IsAdmin == "1" || IsAdmin == "2")
                {
                    String _Activity = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["output_id"].Value.ToString().Replace("'", "`");
                    AddOutputActivity(grdData.Rows[grdData.ActiveCell.Row.Index].Cells["output_id"].Value.ToString(), "");

                }
            }
        }
        frmYESNO f_verify;
        frmYESNO f_verify_act;
        void f_verify_ResultData(object sender, EventArgs e)
        {
            if (f_verify.IsModify)
            {
                String _output = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["output"].Value.ToString().Replace("'", "`");
                String _output_code = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["output_id"].Value.ToString();
                programeditor fEdit = new programeditor();
                fEdit._Data = _output;
                fEdit._Menu = "output";
                fEdit._OutputCode = _output_code;
                fEdit.ReloadData += fEdit_ReloadData;
                fEdit.Show();
            }
            else
            {
                if (IsAdmin == "1" || IsAdmin == "2")
                {
                    String _Activity = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["output_id"].Value.ToString().Replace("'", "`");

                    UpdateProjectOutput(_Activity, grdData.Rows[grdData.ActiveCell.Row.Index].Cells["project_id"].Value.ToString(), "");

                }
            }
           
        }

        void fEdit_ReloadData(object sender, EventArgs e)
        {
            RefreshDataUpdated(this.DivisionId);
        }


        
        void f_employee_AssignedUpdate(object sender, EventArgs e)
        {
            String ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
            String OutputId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["output_id"].Value.ToString();
            UpdateAssignedEmployee(f_employee.AssignedId, ActivityId,OutputId);
        }

        void f_expenditures_SelectedExpenditure(object sender, EventArgs e)
        {
            switch (f_expenditures.SelectedCode)
            {
                case "Local Travel":
                    frmBudgetCreation frm_local_travel = new frmBudgetCreation();
                    String ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    String Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    string _Month = f_expenditures._Month;
                    string _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_local_travel.ActivityID = ActivityId;
                    frm_local_travel.AccountableID = Assigned;
                    frm_local_travel._Month = _Month;
                    frm_local_travel._Year = WorkingYear;
                    frm_local_travel.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_local_travel.MOOE_Index = f_expenditures.MOOE_Index;
                    frm_local_travel.ReloadData += frm_local_travel_ReloadData;
                    frm_local_travel.TravelType = f_expenditures.SelectedCode;
                    frm_local_travel.FundSource = this.f_expenditures.FundSourceId;
                    frm_local_travel.DivisionId = this.DivisionId;
                    frm_local_travel.IsRevision = f_expenditures.IsRevision;
                    frm_local_travel.Show();
                    break;
                case "International Travel":
                    frmBudgetCreation frm_foreign_travel1 = new frmBudgetCreation();
                     ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                     Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                     _Month = f_expenditures._Month;
                     _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                     frm_foreign_travel1.ActivityID = ActivityId;
                     frm_foreign_travel1.AccountableID = Assigned;
                     frm_foreign_travel1._Month = _Month;
                     frm_foreign_travel1._Year = WorkingYear;
                     frm_foreign_travel1.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_foreign_travel1.MOOE_Index =  f_expenditures.MOOE_Index;
                     frm_foreign_travel1.ReloadData += frm_foreign_travel_ReloadData;
                     frm_foreign_travel1.TravelType = f_expenditures.SelectedCode;
                     frm_foreign_travel1.FundSource = f_expenditures.FundSourceId;
                     frm_foreign_travel1.DivisionId = this.DivisionId;
                     frm_foreign_travel1.IsRevision = f_expenditures.IsRevision;
                     frm_foreign_travel1.Show();
                     break;
                case "Foreign Travel":
                     frmBudgetCreation frm_foreign_travel = new frmBudgetCreation();
                     ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                     Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                     _Month = f_expenditures._Month;
                     _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                     frm_foreign_travel.ActivityID = ActivityId;
                     frm_foreign_travel.AccountableID = Assigned;
                     frm_foreign_travel._Month = _Month;
                     frm_foreign_travel._Year = WorkingYear;
                     frm_foreign_travel.MOOE_ID = f_expenditures.MOOE_ID;
                     frm_foreign_travel.MOOE_Index = f_expenditures.MOOE_Index;
                     frm_foreign_travel.ReloadData += frm_foreign_travel_ReloadData;
                     frm_foreign_travel.TravelType = f_expenditures.SelectedCode;
                     frm_foreign_travel.FundSource = f_expenditures.FundSourceId;
                     frm_foreign_travel.DivisionId = this.DivisionId;
                     frm_foreign_travel.IsRevision = f_expenditures.IsRevision;
                     frm_foreign_travel.Show();
                     break;
                case "Gasoline , Oil and Lubricants Expenses":
                     frmBudgetGasoline frm_gasoline1 = new frmBudgetGasoline();
                     ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                     Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                     _Month = f_expenditures._Month;
                     _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                     frm_gasoline1.ActivityID = ActivityId;
                     frm_gasoline1.AccountableID = Assigned;
                     frm_gasoline1._Month = _Month;
                     frm_gasoline1._Year = WorkingYear;
                     frm_gasoline1.MOOE_ID = f_expenditures.MOOE_ID;
                     frm_gasoline1.MOOE_INDEX = f_expenditures.MOOE_Index;
                     frm_gasoline1.ReloadData += frm_gasoline_ReloadData;
                     frm_gasoline1.TravelType = f_expenditures.SelectedCode;
                     frm_gasoline1.FundSource = f_expenditures.FundSourceId;
                     frm_gasoline1.DivisionId = this.DivisionId;
                     frm_gasoline1.IsRevision = f_expenditures.IsRevision;
                     frm_gasoline1.Show();
                    break;
                case "Fuel , Oil and Lubricants Expenses":
                     frmBudgetGasoline frm_gasoline = new frmBudgetGasoline();
                     ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                     Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                     _Month = f_expenditures._Month;
                     _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                     frm_gasoline.ActivityID = ActivityId;
                     frm_gasoline.AccountableID = Assigned;
                     frm_gasoline._Month = _Month;
                     frm_gasoline._Year = WorkingYear;
                     frm_gasoline.MOOE_ID = f_expenditures.MOOE_ID;
                     frm_gasoline.MOOE_INDEX = f_expenditures.MOOE_Index;
                     frm_gasoline.ReloadData += frm_gasoline_ReloadData;
                     frm_gasoline.TravelType = f_expenditures.SelectedCode;
                     frm_gasoline.FundSource = f_expenditures.FundSourceId;
                     frm_gasoline.DivisionId = this.DivisionId;
                     frm_gasoline.IsRevision = f_expenditures.IsRevision;
                     frm_gasoline.Show();
                    break;
                case "Supplies":
                    frmBudgetSupplies frm_budget_supplies1 = new frmBudgetSupplies();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_budget_supplies1.ActivityID = ActivityId;
                    frm_budget_supplies1.AccountableID = Assigned;
                    frm_budget_supplies1._Month = _Month;
                    frm_budget_supplies1._Year = WorkingYear;
                    frm_budget_supplies1.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_budget_supplies1.MOOE_INDEX = f_expenditures.MOOE_Index;
                    frm_budget_supplies1.ReloadData += frm_budget_supplies_ReloadData;
                    frm_budget_supplies1.FundSource = f_expenditures.FundSourceId;
                    frm_budget_supplies1.DivisionId = this.DivisionId;
                    frm_budget_supplies1.IsRevision = f_expenditures.IsRevision;
                    frm_budget_supplies1.Show();
                    break;
                case "Office Supplies Expenses":
                     frmBudgetSupplies frm_budget_supplies2 = new frmBudgetSupplies();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_budget_supplies2.ActivityID = ActivityId;
                    frm_budget_supplies2.AccountableID = Assigned;
                    frm_budget_supplies2._Month = _Month;
                    frm_budget_supplies2._Year = WorkingYear;
                    frm_budget_supplies2.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_budget_supplies2.MOOE_INDEX = f_expenditures.MOOE_Index;
                    frm_budget_supplies2.ReloadData += frm_budget_supplies_ReloadData;
                    frm_budget_supplies2.FundSource = f_expenditures.FundSourceId;
                    frm_budget_supplies2.ExpenditureType = "Office Supplies Expenses";
                    frm_budget_supplies2.DivisionId = this.DivisionId;
                    frm_budget_supplies2.IsRevision = f_expenditures.IsRevision;
                    frm_budget_supplies2.Show();
                    break;
                case "Non-PS DBM Supplies":
                    frmNonPSDBMSupplies frm_nonps_dbm = new frmNonPSDBMSupplies();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_nonps_dbm.ActivityID = ActivityId;
                    frm_nonps_dbm.AccountableID = Assigned;
                    frm_nonps_dbm._Month = _Month;
                    frm_nonps_dbm._Year = WorkingYear;
                    frm_nonps_dbm.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_nonps_dbm.MOOE_INDEX = f_expenditures.MOOE_Index;
                    frm_nonps_dbm.ReloadData += frm_budget_supplies_ReloadData;
                    frm_nonps_dbm.FundSource = f_expenditures.FundSourceId;
                    frm_nonps_dbm.ExpenditureType = "Non-PS DBM Supplies";
                    frm_nonps_dbm.DivisionId = this.DivisionId;
                    frm_nonps_dbm.IsRevision = f_expenditures.IsRevision;
                    frm_nonps_dbm.Show();
                    break;
                case "ICT Office Supplies":
                    frmBudgetSupplies frm_budget_supplies = new frmBudgetSupplies();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_budget_supplies.ActivityID = ActivityId;
                    frm_budget_supplies.AccountableID = Assigned;
                    frm_budget_supplies._Month = _Month;
                    frm_budget_supplies._Year = WorkingYear;
                    frm_budget_supplies.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_budget_supplies.MOOE_INDEX = f_expenditures.MOOE_Index;
                    frm_budget_supplies.ReloadData += frm_budget_supplies_ReloadData;
                    frm_budget_supplies.FundSource = f_expenditures.FundSourceId;
                    frm_budget_supplies.DivisionId = this.DivisionId;
                    frm_budget_supplies.ExpenditureType = "ICT Office Supplies";
                    frm_budget_supplies.IsRevision = f_expenditures.IsRevision;
                    frm_budget_supplies.Show();
                    break;
                case "ICT Office Equipment":
                    frmBudgetSupplies frm_budget_icteq = new frmBudgetSupplies();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_budget_icteq.ActivityID = ActivityId;
                    frm_budget_icteq.AccountableID = Assigned;
                    frm_budget_icteq._Month = _Month;
                    frm_budget_icteq._Year = WorkingYear;
                    frm_budget_icteq.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_budget_icteq.MOOE_INDEX = f_expenditures.MOOE_Index;
                    frm_budget_icteq.ReloadData += frm_budget_supplies_ReloadData;
                    frm_budget_icteq.FundSource = f_expenditures.FundSourceId;
                    frm_budget_icteq.DivisionId = this.DivisionId;
                    frm_budget_icteq.ExpenditureType = "ICT Office Equipment";
                    frm_budget_icteq.IsRevision = f_expenditures.IsRevision;
                    frm_budget_icteq.Show();
                    break;
                case "Other Supplies and Materials Expenses":
                    frmBudgetSupplies frm_budget_supplies_osme = new frmBudgetSupplies();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_budget_supplies_osme.ActivityID = ActivityId;
                    frm_budget_supplies_osme.AccountableID = Assigned;
                    frm_budget_supplies_osme._Month = _Month;
                    frm_budget_supplies_osme._Year = WorkingYear;
                    frm_budget_supplies_osme.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_budget_supplies_osme.MOOE_INDEX = f_expenditures.MOOE_Index;
                    frm_budget_supplies_osme.ReloadData += frm_budget_supplies_ReloadData;
                    frm_budget_supplies_osme.FundSource = f_expenditures.FundSourceId;
                    frm_budget_supplies_osme.DivisionId = this.DivisionId;
                    frm_budget_supplies_osme.ExpenditureType = "Other Supplies and Materials Expenses";
                    frm_budget_supplies_osme.IsRevision = f_expenditures.IsRevision;
                    frm_budget_supplies_osme.Show();
                    break;
                case "Representation":
                    frmBudgetRepresentation frm_budget_representation = new frmBudgetRepresentation();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_budget_representation.ActivityID = ActivityId;
                    frm_budget_representation.AccountableID = Assigned;
                    frm_budget_representation._Month = _Month;
                    frm_budget_representation._Year = WorkingYear;
                    frm_budget_representation.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_budget_representation.MOOE_INDEX = f_expenditures.MOOE_Index;
                    frm_budget_representation.ReloadData += frm_budget_representation_ReloadData;
                    frm_budget_representation.FundSource = f_expenditures.FundSourceId;
                    frm_budget_representation.DivisionId = this.DivisionId;
                    frm_budget_representation.IsRevision = f_expenditures.IsRevision;
                    frm_budget_representation.Show();
                    break;
                case "Representation Expenses":
                    frmBudgetRepresentation frm_budget_representation_expense = new frmBudgetRepresentation();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_budget_representation_expense.ActivityID = ActivityId;
                    frm_budget_representation_expense.AccountableID = Assigned;
                    frm_budget_representation_expense._Month = _Month;
                    frm_budget_representation_expense._Year = WorkingYear;
                    frm_budget_representation_expense.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_budget_representation_expense.MOOE_INDEX = f_expenditures.MOOE_Index;
                    frm_budget_representation_expense.ReloadData += frm_budget_representation_ReloadData;
                    frm_budget_representation_expense.FundSource = f_expenditures.FundSourceId;
                    frm_budget_representation_expense.DivisionId = this.DivisionId;
                    frm_budget_representation_expense.IsRevision = f_expenditures.IsRevision;
                    frm_budget_representation_expense.Show();
                    break;
                case "Professional Fee":
                    frmBudgetProfessionalFees frm_prof = new frmBudgetProfessionalFees();
                     ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                     Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                     _Month = f_expenditures._Month;
                     _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                     frm_prof.ActivityID = ActivityId;
                     frm_prof.AccountableID = Assigned;
                     frm_prof._Month = _Month;
                     frm_prof._Year = WorkingYear;
                     frm_prof.MOOE_ID = f_expenditures.MOOE_ID;
                     frm_prof.MOOE_INDEX = f_expenditures.MOOE_Index;
                     frm_prof.FundSource = f_expenditures.FundSourceId;
                     frm_prof.DivisionId = this.DivisionId;
                     frm_prof.IsRevision = f_expenditures.IsRevision;
                     frm_prof.ReloadData += frm_prof_ReloadData;

                     frm_prof.Show();
                    break;
                case "Printing and Binding":
                    frmBudgetPrintingBinding frm_printbind = new frmBudgetPrintingBinding();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_printbind.ActivityID = ActivityId;
                    frm_printbind.AccountableID = Assigned;
                    frm_printbind._Month = _Month;
                    frm_printbind._Year = WorkingYear;
                    frm_printbind.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_printbind.MOOE_INDEX = f_expenditures.MOOE_Index;
                    frm_printbind.FundSource = f_expenditures.FundSourceId;
                    frm_printbind.DivisionId = this.DivisionId;
                    frm_printbind.IsRevision = f_expenditures.IsRevision;
                    frm_printbind.ReloadData += frm_printbind_ReloadData;

                    frm_printbind.Show();
                    break;
                case "Dues":
                    frmBudgetDues frm_due = new frmBudgetDues();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_due.ActivityID = ActivityId;
                    frm_due.AccountableID = Assigned;
                    frm_due._Month = _Month;
                    frm_due._Year = WorkingYear;
                    frm_due.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_due.MOOE_INDEX = f_expenditures.MOOE_Index;
                    frm_due.FundSource = f_expenditures.FundSourceId;
                    frm_due.ReloadData += frm_due_ReloadData;
                    frm_due.DivisionId = this.DivisionId;
                    frm_due.IsRevision = f_expenditures.IsRevision;
                    frm_due.Show();
                    break;
                case "Subscription":
                    frmBudgetSubscription frm_subs = new frmBudgetSubscription();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_subs.ActivityID = ActivityId;
                    frm_subs.AccountableID = Assigned;
                    frm_subs._Month = _Month;
                    frm_subs._Year = WorkingYear;
                    frm_subs.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_subs.MOOE_INDEX = f_expenditures.MOOE_Index;
                    frm_subs.DivisionId = this.DivisionId;
                    frm_subs.FundSource = f_expenditures.FundSourceId;
                    frm_subs.IsRevision = f_expenditures.IsRevision;
                    frm_subs.ReloadData += frm_subs_ReloadData;

                    frm_subs.Show();
                    break;
                case "Advertising":
                    frmBudgetAdvertising frm_ads1 = new frmBudgetAdvertising();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_ads1.ActivityID = ActivityId;
                    frm_ads1.AccountableID = Assigned;
                    frm_ads1._Month = _Month;
                    frm_ads1._Year = WorkingYear;
                    frm_ads1.DivisionId = this.DivisionId;
                    frm_ads1.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_ads1.MOOE_INDEX = f_expenditures.MOOE_Index;
                    frm_ads1.FundSource = f_expenditures.FundSourceId;
                    frm_ads1.ReloadData += frm_ads_ReloadData;
                    frm_ads1.IsRevision = f_expenditures.IsRevision;

                    frm_ads1.Show();
                    break;
                case "Advertising Expenses":
                    frmBudgetAdvertising frm_ads = new frmBudgetAdvertising();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_ads.ActivityID = ActivityId;
                    frm_ads.AccountableID = Assigned;
                    frm_ads._Month = _Month;
                    frm_ads._Year = WorkingYear;
                    frm_ads.DivisionId = this.DivisionId;
                    frm_ads.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_ads.MOOE_INDEX = f_expenditures.MOOE_Index;
                    frm_ads.FundSource = f_expenditures.FundSourceId;
                    frm_ads.ReloadData += frm_ads_ReloadData;
                    frm_ads.IsRevision = f_expenditures.IsRevision;
                    frm_ads.Show();
                    break;
                case "Training Expenses":
                    frmBudgetTraining frm_bud = new frmBudgetTraining();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_bud.ActivityID = ActivityId;
                    frm_bud.AccountableID = Assigned;
                    frm_bud._Month = _Month;
                    frm_bud._Year = WorkingYear;
                    frm_bud.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_bud.MOOE_INDEX = f_expenditures.MOOE_Index;
                    frm_bud.FundSource = f_expenditures.FundSourceId;
                    frm_bud.DivisionId = this.DivisionId;
                    frm_bud.ReloadData += frm_ads_ReloadData;
                    frm_bud.IsRevision = f_expenditures.IsRevision;
                    frm_bud.Show();
                    break;
                case "Rental Fee":
                    frmBudgetRental frm_rent = new frmBudgetRental();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_rent.ActivityID = ActivityId;
                    frm_rent.AccountableID = Assigned;
                    frm_rent._Month = _Month;
                    frm_rent._Year = WorkingYear;
                    frm_rent.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_rent.MOOE_INDEX = f_expenditures.MOOE_Index;
                    frm_rent.FundSource = f_expenditures.FundSourceId;
                    frm_rent.DivisionId = this.DivisionId;
                    frm_rent.ReloadData += frm_rent_ReloadData;
                    frm_rent.IsRevision = f_expenditures.IsRevision;
                    frm_rent.Show();
                    break;
                case "Labor and Wages":
                    frmBudgetLaborAndWages frm_law = new frmBudgetLaborAndWages();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_law.ActivityID = ActivityId;
                    frm_law.AccountableID = Assigned;
                    frm_law._Month = _Month;
                    frm_law._Year = WorkingYear;
                    frm_law.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_law.MOOE_INDEX = f_expenditures.MOOE_Index;
                    frm_law.DivisionId = this.DivisionId;
                    frm_law.FundSource = f_expenditures.FundSourceId;
                    frm_law.ReloadData += frm_law_ReloadData;
                    frm_law.IsRevision = f_expenditures.IsRevision;
                    frm_law.Show();
                    break;
                case "Other MOOE":
                    frmBudgetOtherMooe frm_other = new frmBudgetOtherMooe();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_other.ActivityID = ActivityId;
                    frm_other.AccountableID = Assigned;
                    frm_other._Month = _Month;
                    frm_other._Year = WorkingYear;
                    frm_other.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_other.MOOE_INDEX = f_expenditures.MOOE_Index;
                    frm_other.FundSource = f_expenditures.FundSourceId;
                    frm_other.DivisionId = this.DivisionId;
                    frm_other.ReloadData += frm_other_ReloadData;
                    frm_other.IsRevision = f_expenditures.IsRevision;
                    frm_other.Show();
                    break;
                case "Other Maintenance &  Expenses":
                    frmBudgetOtherMooe frm_other_me1 = new frmBudgetOtherMooe();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_other_me1.ActivityID = ActivityId;
                    frm_other_me1.AccountableID = Assigned;
                    frm_other_me1._Month = _Month;
                    frm_other_me1._Year = WorkingYear;
                    frm_other_me1.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_other_me1.MOOE_INDEX = f_expenditures.MOOE_Index;
                    frm_other_me1.FundSource = f_expenditures.FundSourceId;
                    frm_other_me1.DivisionId = this.DivisionId;
                    frm_other_me1.ReloadData+=frm_other_me_ReloadData;
                    frm_other_me1.IsRevision = f_expenditures.IsRevision;
                    frm_other_me1.Show();
                    break;
                case "Other Maintenance and Operating Expenses":
                    frmBudgetOtherMooe frm_other_me = new frmBudgetOtherMooe();
                    ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                    Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                    _Month = f_expenditures._Month;
                    _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                    frm_other_me.ActivityID = ActivityId;
                    frm_other_me.AccountableID = Assigned;
                    frm_other_me._Month = _Month;
                    frm_other_me._Year = WorkingYear;
                    frm_other_me.MOOE_ID = f_expenditures.MOOE_ID;
                    frm_other_me.MOOE_INDEX = f_expenditures.MOOE_Index;
                    frm_other_me.FundSource = f_expenditures.FundSourceId;
                    frm_other_me.DivisionId = this.DivisionId;
                    frm_other_me.IsRevision = f_expenditures.IsRevision;
                    frm_other_me.ReloadData+=frm_other_me_ReloadData;

                    frm_other_me.Show();
                    break;
                default:
                    if (f_expenditures.IsDynamic)
                    {
                        frmGenericExpenditureCharging f_dynamic = new frmGenericExpenditureCharging();
                        ActivityId = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["activity_id"].Value.ToString();
                        Assigned = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString();
                        _Month = f_expenditures._Month;
                        _Year = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["fiscal_year"].Value.ToString();

                        f_dynamic.ActivityID = ActivityId;
                        f_dynamic.AccountableID = Assigned;
                        f_dynamic._Month = _Month;
                        f_dynamic._Year = WorkingYear;
                        f_dynamic.MOOE_ID = f_expenditures.MOOE_ID;
                        f_dynamic.MOOE_INDEX =f_expenditures.MOOE_Index;
                        f_dynamic.FundMOOEName = f_expenditures.SelectedName;
                        f_dynamic.FundSource = f_expenditures.FundSourceId;
                        f_dynamic.DivisionId = this.DivisionId;
                        f_dynamic.ReloadData += f_dynamic_ReloadData;
                        f_dynamic.IsRevision = f_expenditures.IsRevision;
                     
                        f_dynamic.Show();
                    }
                    break;
            }
         
        }

        void frm_other_me_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void f_dynamic_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void frm_other_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void frm_law_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void frm_rent_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void frm_ads_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void frm_subs_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void frm_due_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void frm_printbind_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void frm_prof_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void frm_budget_representation_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void frm_budget_supplies_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }
        private void UpdateYear(string _year,string _id)
        {
            c_main_data.Process = "UpdateRowYear";
            c_main_data.UpdateYear(_year,_id);
        }

        //private void UpdateModeOfProcurement(string _mode_of_procurement, string _id)
        //{
        //    c_main_data.Process = "UpdateRowModeofProcurement";
        //    c_main_data.UpdateModeOfProcurement(_mode_of_procurement, _id);
        //}

        private void UpdateModeOfProcurement(string _activity_id, String _mode_of_procurement)
        {
            c_main_data.Process = "UpdateRowModeofProcurement";
            _updateModeOfProcurement = _mode_of_procurement;
            if (IsAdmin == "1")
            {
                c_main_data.UpdateModeOfProcurement(_activity_id, _mode_of_procurement);
            }
            else
            {
                c_main_data.UpdateModeOfProcurement(_activity_id, _mode_of_procurement);
            }


        }

        private void UpdateProgram(string _id, string program)
        {
            c_main_data.Process = "UpdateRowProgram";
            _updateProgram = program;
            c_main_data.UpdateProgram(_id, program);

        }
      
        private void UpdateProject(string _id, string project,string _year)
        {
            c_main_data.Process = "UpdateRowProject";
            _updateProject = project;
            c_main_data.UpdateProject(_id, project,_year,this.DivisionId);

        }
        private void UpdateProjectOutput(string _output_id, string _id, string project_output)
        {
            c_main_data.Process = "UpdateProjectOutput";
            _updateProjectOutput = project_output;
            if (IsAdmin=="1")
            {
                c_main_data.UpdateProjectOutput(_output_id, _id, project_output,"");

            }
            else
            {
                c_main_data.UpdateProjectOutput(_output_id, _id, project_output,this.UserId);
            }
         

        } private void UpdateDeleteProjectOutput(string _output_id, string _id, string project_output)
        {
            c_main_data.Process = "UpdateProjectOutput";
            _updateProjectOutput = project_output;
            if (IsAdmin=="1")
            {
                c_main_data.UpdateDeleteProjectOutput(_output_id, _id, project_output,"");
            }
            else
            {
                c_main_data.UpdateDeleteProjectOutput(_output_id, _id, project_output,UserId);
            }
       

        }
        private void UpdateOutputActivity(String _activity_id, string _ouput_id, string _activity)
        {
            c_main_data.Process = "UpdateOutputActivity";
            _updateOutputActivity = _activity;
            if (IsAdmin=="1")
            {
                c_main_data.UpdateOutputActivity(_activity_id, _ouput_id, _activity,"-");
            }
            else
            {
                c_main_data.UpdateOutputActivity(_activity_id, _ouput_id, _activity,this.UserId);
            }
        

        }
        private void FetchExpenseItems()
        {
            c_main_data.Process = "FetchExpenseItems";
    
            svc_mindaf.ExecuteSQLAsync(c_main_data.FetchExpenseItems());

        }
        private void AddOutputActivity(string _ouput_id, string _activity)
        {
            c_main_data.Process = "UpdateOutputActivity";
            _updateOutputActivity = _activity;
            String _prog_id = grdData.Rows[grdData.ActiveCell.Row.Index].Cells["program_id"].Value.ToString();

            if (IsAdmin=="1")
            {
                c_main_data.UpdateAddOutputActivity(_ouput_id, _activity,"-",_prog_id);

            }
            else
            {
                c_main_data.UpdateAddOutputActivity(_ouput_id, _activity, this.UserId, _prog_id);

            }
           
        }
        private void UpdateAssignedEmployee(string _user_id, string _activity_id,string _output_id)
        {
            c_main_data.Process = "UpdateAssignedEmployee";

            c_main_data.UpdateAssignedEmployee(_user_id, _activity_id, _output_id);

        }
        private void UpdateTableProgramID(string _program) 
        {
            c_main_data.Process = "FetchProgramID";
            svc_mindaf.ExecuteSQLAsync(c_main_data.FetchProgramId(_program));
        }
       

        private void UpdateTableProjectID(string _project)
        {
            c_main_data.Process = "FetchProjectID";
            svc_mindaf.ExecuteSQLAsync(c_main_data.FetchProjectId(_project));
        }
        void frm_gasoline_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void frm_foreign_travel_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }

        void frm_local_travel_ReloadData(object sender, EventArgs e)
        {
            RefreshData(this.DivisionId);
        }
        private void AddNewProgram() 
        {

        }
        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {

        }

        private void grdData_ColumnSorting(object sender, SortingCancellableEventArgs e)
        {
            e.Cancel = true;
        }

       
        private Boolean isUpdated = false;
    //    private string FundName;
        private void grdData_CellExitedEditMode(object sender, CellExitedEditingEventArgs e)
        {
            String _UpData = e.Cell.Value.ToString();
            if (isUpdated)
            {
                if (grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString() == "Grand Total:")
                {
                   
                    return;
                }
                if (e.Cell.Value.ToString()!="")
                {
                    if (_UpData.Contains("'"))
                    {
                        _UpData = e.Cell.Value.ToString().Replace("'", "`");
                    }
                    switch (e.Cell.Column.HeaderText)
                    {
                        case "fiscal_year":
                            UpdateYear(_UpData, grdData.Rows[e.Cell.Row.Index].Cells["program_id"].Value.ToString());
                            break;
                        case "mode_of_procurement":
                            UpdateModeOfProcurement(grdData.Rows[e.Cell.Row.Index].Cells["activity_id"].Value.ToString(), _UpData);
                            break;
                        case "project_name":
                            UpdateProject(grdData.Rows[e.Cell.Row.Index].Cells["program_id"].Value.ToString(), _UpData, grdData.Rows[e.Cell.Row.Index].Cells["fiscal_year"].Value.ToString());
                            break;
                        case "output":
                            UpdateDeleteProjectOutput(grdData.Rows[e.Cell.Row.Index].Cells["output_id"].Value.ToString(), grdData.Rows[e.Cell.Row.Index].Cells["project_id"].Value.ToString(), _UpData);
                            break;
                        case "activity":

                            UpdateOutputActivity(grdData.Rows[e.Cell.Row.Index].Cells["activity_id"].Value.ToString(), grdData.Rows[e.Cell.Row.Index].Cells["output_id"].Value.ToString(), _UpData);
                            break;
                    }
                }
               
            }
           
           
        }

        private void grdData_CellEnteringEditMode(object sender, BeginEditingCellEventArgs e)
        {
            if (e.Cell.Column.HeaderText.ToString() == "Jan" ||
              e.Cell.Column.HeaderText.ToString() == "Feb" ||
              e.Cell.Column.HeaderText.ToString() == "Mar" ||
              e.Cell.Column.HeaderText.ToString() == "Apr" ||
              e.Cell.Column.HeaderText.ToString() == "May" ||
              e.Cell.Column.HeaderText.ToString() == "Jun" ||
              e.Cell.Column.HeaderText.ToString() == "Jul" ||
              e.Cell.Column.HeaderText.ToString() == "Aug" ||
              e.Cell.Column.HeaderText.ToString() == "Sep" ||
              e.Cell.Column.HeaderText.ToString() == "Oct" ||
              e.Cell.Column.HeaderText.ToString() == "Nov" ||
              e.Cell.Column.HeaderText.ToString() == "Dec" ||
              e.Cell.Column.HeaderText.ToString() == "User_Fullname"
          )
            {
                e.Cancel = true;

            }
            else
            {
                if (grdData.Rows[grdData.ActiveCell.Row.Index].Cells["User_Fullname"].Value.ToString()=="Grand Total :")
                {
                    e.Cancel = true;
                }
                if (e.Cell.Value == "")
                {
                    grdData.EditingSettings.AllowEditing = EditingType.Cell;
                }
                else
                {
                    if (e.Cell.Column.HeaderText.ToString() == "fiscal_year" ||
                      e.Cell.Column.HeaderText.ToString() == "program_name" ||
                      e.Cell.Column.HeaderText.ToString() == "project_name")
                    {
                        //if (IsAdmin == "1")
                        //{
                        //    grdData.EditingSettings.AllowEditing = EditingType.Cell;
                        //    isUpdated = true;
                        //}
                        //else
                        //{
                            e.Cancel = true;
                        //}
                       
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                  
                }
                
                
                
            }
           
        }

        private void grdData_TextInputUpdate(object sender, TextCompositionEventArgs e)
        {

        }

        private void grdData_CellEnteredEditMode(object sender, EditingCellEventArgs e)
        {
            if (e.Cell.Value.ToString() =="")
            {
                isUpdated = true;
            }
            else
            {
                if (e.Cell.Column.HeaderText.ToString() == "fiscal_year" ||
                      e.Cell.Column.HeaderText.ToString() == "program_name" ||
                      e.Cell.Column.HeaderText.ToString() == "project_name")
                {
               
                    isUpdated = true;
                }
                else
                {
                    isUpdated = false;
                }
               

            }
        }
        private void FetchActivityStatusRefresh(String _year, String _month, String _user, String act_id)
        {
            c_main_data.Process = "FetchActivityStatusRefresh";
            svc_mindaf.ExecuteSQLAsync(c_main_data.FetchActivityStatus(_year, _month, _user, act_id));
        }
        
        private void grdData_CellClicked(object sender, CellClickedEventArgs e)
        {

            if (e.Cell.Column.HeaderText.ToString() == "Jan" ||
               e.Cell.Column.HeaderText.ToString() == "Feb" ||
               e.Cell.Column.HeaderText.ToString() == "Mar" ||
               e.Cell.Column.HeaderText.ToString() == "Apr" ||
               e.Cell.Column.HeaderText.ToString() == "May" ||
               e.Cell.Column.HeaderText.ToString() == "Jun" ||
               e.Cell.Column.HeaderText.ToString() == "Jul" ||
               e.Cell.Column.HeaderText.ToString() == "Aug" ||
               e.Cell.Column.HeaderText.ToString() == "Sep" ||
               e.Cell.Column.HeaderText.ToString() == "Oct" ||
               e.Cell.Column.HeaderText.ToString() == "Nov" ||
               e.Cell.Column.HeaderText.ToString() == "Dec"
           )
            {
                String _year = grdData.Rows[e.Cell.Row.Index].Cells["fiscal_year"].Value.ToString();
                String _user = grdData.Rows[e.Cell.Row.Index].Cells["User_Fullname"].Value.ToString();
                String _act_id = grdData.Rows[e.Cell.Row.Index].Cells["activity_id"].Value.ToString();
                if (_year != "" && _user != "")
                {
                    SelectedMonth = e.Cell.Column.HeaderText.ToString();
                    SelectedYear = _year;
                    SelectedUser = _user;
                    SelectedActId = _act_id;
                    FetchActivityStatus(_year, e.Cell.Column.HeaderText.ToString(), _user, _act_id);
                }
                else
                {
                    grdStatus.ItemsSource = null;
                }


            }
        }
        private void FetchActivityStatus(String _year, String _month,String _user,String act_id)
        {
            c_main_data.Process = "FetchActivityStatus";
            svc_mindaf.ExecuteSQLAsync(c_main_data.FetchActivityStatus(_year, _month,_user,act_id));
        }

        private void FetchActivityStatusByMonth(String _year, String _month, String _user)
        {
            c_main_data.Process = "FetchActivityStatusByMonth";
            svc_mindaf.ExecuteSQLAsync(c_main_data.FetchActivityStatusByMonth(_year, _month, _user));
        }

        private string SelectedMonth = "";
        private string SelectedYear = "";
        private string SelectedUser = "";
        private string SelectedActId = "";
       
      
        private void btnSuspend_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult res = MessageBox.Show("Confirm suspend of expense item.", "", MessageBoxButton.OKCancel);
            if (res.ToString()== "OK")
            {
                MainDashboardActivityData _selected =(MainDashboardActivityData)grdStatus.ActiveItem;
                if (_selected == null)
                {
                    MessageBox.Show("Please select an expense item to suspend. ", "", MessageBoxButton.OK);
                }
                else
                {

                    List<ActualDataMain> _find = ListActualData.Where(x => x.ExpenseItem == _selected.name).ToList();
                    if (_find.Count != 0)
                    {
                        MessageBox.Show("Cannot suspend of expense item. because there is an Actual OBR charged to the expense item.", "", MessageBoxButton.OK);
                    }
                    else
                    {
                        c_main_data.Process = "Suspend";
                        c_main_data.SQLOperation += c_main_data_SQLOperation;
                        c_main_data.UpdateSuspend(_selected.act_id, "1");
                    }

                 
                }

               
            }
           
        }
        private frmMonths _frmMonths = new frmMonths();
        private void btnTransfer_Click(object sender, RoutedEventArgs e)
        {
            
             MainDashboardActivityData _selected = (MainDashboardActivityData)grdStatus.ActiveItem;
             if (_selected ==null )
             {
                 MessageBox.Show("Please select an expense item to transfer. ", "", MessageBoxButton.OK);
             }
             else
             {
                 List<ActualDataMain> _find = ListActualData.Where(x => x.ExpenseItem == _selected.name).ToList();
                 if (_find.Count != 0)
                 {
                     MessageBox.Show("Cannot transfer of expense item. because there is an Actual OBR charged to the expense item.", "", MessageBoxButton.OK);
                 }
                 else
                 {
                     _frmMonths = new frmMonths();
                     _frmMonths.OkData += _frmMonths_Ok;
                     _frmMonths.Show();
                 }
             }
           
            
        }

        private void TransferExpenseItem()         
        {
            MessageBoxResult res = MessageBox.Show("Confirm transfer of expense item.", "", MessageBoxButton.OKCancel);
            if (res.ToString() == "OK")
            {
                MainDashboardActivityData _selected = (MainDashboardActivityData)grdStatus.ActiveItem;
               
                    String _Months = "";
                    switch (_frmMonths.SelectedMonth)
                    {
                        case "January": _Months = "Jan"; break;
                        case "February": _Months = "Feb"; break;
                        case "March": _Months = "Mar"; break;
                        case "April": _Months = "Apr"; break;
                        case "May": _Months = "May"; break;
                        case "June": _Months = "Jun"; break;
                        case "July": _Months = "Jul"; break;
                        case "August": _Months = "Aug"; break;
                        case "September": _Months = "Sep"; break;
                        case "October": _Months = "Oct"; break;
                        case "November": _Months = "Nov"; break;
                        case "December": _Months = "Dec"; break;

                    }
                    c_main_data.Process = "TransferMonths";
                    c_main_data.SQLOperation += c_main_data_SQLOperation;
                    c_main_data.TransferMonths(_selected.act_id, _Months);

            }
        }
        void _frmMonths_Ok(object sender, EventArgs e)
        {
            TransferExpenseItem();
        }
        private frmTitles frm_title;

        private void grdData_KeyDown(object sender, KeyEventArgs e)
        {
            frm_title = new frmTitles();
            frm_title.SelectedData += frm_title_SelectedData;
            if (e.Key == Key.F2)
            {
                switch (grdData.ActiveCell.Column.HeaderText.ToString())
                {
                    case "program_name":
                        frm_title.SelectType = "Programs";
                        break;
                    case "project_name":
                        frm_title.SelectType = "Projects";
                        break;
                    case "output":
                        frm_title.SelectType = "Outputs";
                        break;
                    case "activity":
                        frm_title.SelectType = "Activity";
                        break;
                    case "mode_of_procurement":
                        frm_title.SelectType = "Mode of Procurement";
                        break;
                }
                frm_title.DivisionId = this.DivisionId;
                frm_title.Show();
            }
           
        }

        void frm_title_SelectedData(object sender, EventArgs e)
        {
            MainData _selected = (MainData)grdData.ActiveItem;
            switch (grdData.ActiveCell.Column.HeaderText.ToString())
            {
                case "program_name":
                    
                    UpdateProgram(_selected.program_id.ToString(), frm_title.DataSelected);
                    
                    break;
                case "project_name":
                    UpdateProject(_selected.program_id.ToString(),frm_title.DataSelected,this.WorkingYear);
                    break;
                case "output":
                    UpdateDeleteProjectOutput(_selected.output_id.ToString(), _selected.project_id.ToString(), frm_title.DataSelected);                   
                    break;
                case "activity":
                    UpdateOutputActivity(_selected.activity_id, _selected.output_id, frm_title.DataSelected);
                    break;
                case "mode_of_procurement":
                    UpdateModeOfProcurement(_selected.activity_id, frm_title.DataSelected);
                    break;
            }
        }

        

      


    }
    public class ListExpenseMain
    {
        public String UACS { get; set; }
        public String Name { get; set; }

    }
    public class ActualDataMain 
    {

  
       public String AccountCode {get;set;}
       public String ExpenseItem {get;set;}
       public String Actual_OBR { get; set; }
       public String Planned { get; set; }
       public String Balance { get; set; }
    }


    public class StringToIntVC : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Int32.Parse((string)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value.ToString();
        }
    }

}
