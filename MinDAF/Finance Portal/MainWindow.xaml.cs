﻿using Finance_Portal.Usercontrol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Finance_Portal
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ctrlPAP ctrl_pap = new ctrlPAP();
 
        public MainWindow()
        {
            InitializeComponent();
        }

        private void mnuPapMan_Click(object sender, RoutedEventArgs e)
        {
            

        }

        private void btnPAP_Click(object sender, RoutedEventArgs e)
        {
            ctrl_pap.Width = stkChild.Width;
            ctrl_pap.Height = stkChild.Height;

            stkChild.Children.Clear();
            stkChild.Children.Add(ctrl_pap);
        }

     
    }
}
