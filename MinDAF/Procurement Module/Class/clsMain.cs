﻿using ReportTool.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Procurement_Module.Class
{
   public class clsMain
    {

        clsData c_data = new clsData();

        public DataSet GetExpenseItems()
        {
            var sb = new System.Text.StringBuilder(132);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Division_Id,");
            sb.AppendLine(@"  DBM_Sub_Pap_Id,");
            sb.AppendLine(@"  Division_Code,");
            sb.AppendLine(@"  Division_Desc,");
            sb.AppendLine(@"  DivisionAccro,");
            sb.AppendLine(@"  UnitCode");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.Division;");

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }
       public DataSet GetActivityData(String _month,String _div_id)
        {
            var sb = new System.Text.StringBuilder(581);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"ma.id as ActId,");
            sb.AppendLine(@"mad.id as expense_id,");
            sb.AppendLine(@"mpe.acountable_division_code as div_id,");
            sb.AppendLine(@"mooe.uacs_code,");
            sb.AppendLine(@"ma.description as activity,");
            sb.AppendLine(@"mooe.name,");
            sb.AppendLine(@"ISNULL(mpi.item_specifications,'Expense Item') as details,");
            sb.AppendLine(@"mad.month,");
            sb.AppendLine(@"mad.total,");
            sb.AppendLine(@"mad.status");
            sb.AppendLine(@"FROM mnda_activity_data mad ");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.id = mad.activity_id");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.id = ma.output_id");
            sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.id  = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_procurement_items mpi on mpi.id = mad.procurement_id");
            sb.AppendLine(@"WHERE mad.month ='"+ _month +"' AND mpe.acountable_division_code ="+ _div_id +"");


            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }
       public DataSet GetActivityList(String _month, String _div_id)
       {
           var sb = new System.Text.StringBuilder(611);
           sb.AppendLine(@"SELECT ");
           sb.AppendLine(@"ma.id as ActId,");
           sb.AppendLine(@"div.Division_Desc,");
           sb.AppendLine(@"ma.description as activity");
           sb.AppendLine(@"FROM mnda_activity_data mad ");
           sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.id = mad.activity_id");
           sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.id = ma.output_id");
           sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
           sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.id  = mad.mooe_sub_expenditure_id");
           sb.AppendLine(@"INNER JOIN Division div on div.Division_Id = mpe.acountable_division_code");
           sb.AppendLine(@"LEFT JOIN mnda_procurement_items mpi on mpi.id = mad.procurement_id");
           sb.AppendLine(@"WHERE mad.month ='"+ _month +"' AND div.Division_Id ="+ _div_id +"");
           sb.AppendLine(@"GROUP BY ma.id,div.Division_Id,div.Division_Desc,ma.description");
           sb.AppendLine(@"ORDER BY div.Division_Id ASC");

           DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

           return dsData;
       }
    }

   
}
