﻿using Procurement_Module.Class;
using Procurement_Module.Forms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Procurement_Module
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private clsMain c_main = new clsMain();
        private DataTable dtDivision;
        private DataTable dtActivity;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void LoadDivisionList() 
        {
            dtDivision = new DataTable();
            dtDivision = c_main.GetExpenseItems().Tables[0].Copy();
            cmbDivision.Items.Clear();

            foreach (DataRow item in dtDivision.Rows)
            {
                cmbDivision.Items.Add(item["Division_Desc"].ToString());
            }
        }


        private void btnGenerateAPP_Click(object sender, RoutedEventArgs e)
        {
            frmAPP f_app = new frmAPP();
            f_app.ShowDialog();
        }

        private void btnPPMPManager_Click(object sender, RoutedEventArgs e)
        {
            frmPPMP f_app = new frmPPMP();
            f_app.ShowDialog();
        }

        private void btnConsolidatedPPMP_Click(object sender, RoutedEventArgs e)
        {
             frmConsolidatedPPMP f_app = new frmConsolidatedPPMP();
            f_app.ShowDialog();
        }

        private void GenerateMonths()        
        {
            cmbMonths.Items.Clear();
            cmbMonths.Items.Add("Jan");
            cmbMonths.Items.Add("Feb");
            cmbMonths.Items.Add("Mar");
            cmbMonths.Items.Add("Apr");
            cmbMonths.Items.Add("May");
            cmbMonths.Items.Add("Jun");
            cmbMonths.Items.Add("Jul");
            cmbMonths.Items.Add("Aug");
            cmbMonths.Items.Add("Sep");
            cmbMonths.Items.Add("Oct");
            cmbMonths.Items.Add("Nov");
            cmbMonths.Items.Add("Dec");

            Int32 _month = DateTime.Now.Month;
            String _MonthName = "";

            switch (_month)
            {
                case 1:
                    _MonthName = "Jan";
                    break;
                case 2:
                    _MonthName = "Feb";
                    break;
                case 3:
                    _MonthName = "Mar";
                    break;
                case 4:
                    _MonthName = "Apr";
                    break;
                case 5:
                    _MonthName = "May";
                    break;
                case 6:
                    _MonthName = "Jun";
                    break;
                case 7:
                    _MonthName = "Jul";
                    break;
                case 8:
                    _MonthName = "Aug";
                    break;
                case 9:
                    _MonthName = "Sep";
                    break;
                case 10:
                    _MonthName = "Oct";
                    break;
                case 11:
                    _MonthName = "Nov";
                    break;
                case 12:
                    _MonthName = "Dec";
                    break;
            }

            for (int i = 0; i < cmbMonths.Items.Count; i++)
            {
                if (cmbMonths.Items[i].ToString() ==_MonthName)
                {
                    cmbMonths.SelectedIndex = i;
                }
            }


            

        }

        private void LoadActivities()
        {
            grdData.DataSource = null;
            dtDivision.DefaultView.RowFilter = "Division_Desc='" + cmbDivision.SelectedItem.ToString() + "'";
            String _Code = dtDivision.DefaultView[0][0].ToString();

            DataTable dtAct = c_main.GetActivityData(cmbMonths.SelectedItem.ToString(),_Code).Tables[0].Copy();
            DataTable dtActList = c_main.GetActivityList(cmbMonths.SelectedItem.ToString(), _Code).Tables[0].Copy();

           

            DataTable dtHeader = new DataTable();
            DataTable dtDetails = new DataTable();

            dtHeader.Columns.Add("ActId");
            dtHeader.Columns.Add("Activity");
            dtHeader.Columns.Add("Status");


            dtDetails.Columns.Add("ActId");
            dtDetails.Columns.Add("ExpenseItem");
            dtDetails.Columns.Add("Details");
            dtDetails.Columns.Add("Totals");

            foreach (DataRow item in dtActList.Rows)
            {

                 dtAct.DefaultView.RowFilter = "ActId ='"+ item["ActId"].ToString() +"' AND status ='PR REQUEST'";
                 DataTable dt = dtAct.DefaultView.ToTable().Copy();

                 Int32 _countPR = dt.Rows.Count;

                DataRow dr = dtHeader.NewRow();

                dr["ActId"] = item["ActId"].ToString();
               dr["Activity"] = item["activity"].ToString();
               if (_countPR!=0)
               {
                    dr["Status"] = "PR Request Sumbitted";
               }
               else
               {
                   dr["Status"] = "No PR Sumbission";
               }

               dtAct.DefaultView.RowFilter = "";
               dtHeader.Rows.Add(dr);

            }

            foreach (DataRow item in dtAct.Rows)
            {
                DataRow dr = dtDetails.NewRow();

                dr["ActId"] = item["ActId"].ToString(); ;
                dr["ExpenseItem"] = item["name"].ToString(); ;
                  dr["Details"] =item["details"].ToString();
                  dr["Totals"] = Convert.ToDouble(item["total"]).ToString("#,##0.00");


                  dtDetails.Rows.Add(dr);

            }
            DataSet dsMain = new DataSet();

            dtHeader.TableName = "Header";
            dtDetails.TableName = "Details";
            dsMain.Tables.Add(dtHeader);
            dsMain.Tables.Add(dtDetails);
            DataRelation relation = new DataRelation("relation", dtHeader.Columns["ActId"], dtDetails.Columns["ActId"]);

            

            dsMain.Relations.Add(relation);
            grdData.DataSource = dsMain.Tables[0].DefaultView;
            grdData.FieldLayouts[0].Fields[0].Visibility = System.Windows.Visibility.Collapsed;
            grdData.FieldLayouts[1].Fields[0].Visibility = System.Windows.Visibility.Collapsed;
            grdData.FieldLayoutSettings.LabelLocation = Infragistics.Windows.DataPresenter.LabelLocation.Hidden;

            grdData.FieldLayouts[0].Fields[1].Width = new Infragistics.Windows.DataPresenter.FieldLength(350);
            grdData.FieldLayouts[1].Fields[1].Width = new Infragistics.Windows.DataPresenter.FieldLength(150);
            grdData.FieldLayouts[1].Fields[2].Width = new Infragistics.Windows.DataPresenter.FieldLength(300);
        }

        private void frmMain_Loaded(object sender, RoutedEventArgs e)
        {
            GenerateMonths();
            LoadDivisionList();

            
        }

        private void cmbDivision_DropDownClosed(object sender, EventArgs e)
        {
            LoadActivities();
        }

        private void cmbMonths_DropDownClosed(object sender, EventArgs e)
        {
            LoadActivities();
        }
        
    }
}
