﻿using ReportTool.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ReportTool
{
    /// <summary>
    /// Interaction logic for frmPPMPLoad.xaml
    /// </summary>
    public partial class frmPPMPLoad : Window
    {

        public String WorkingYear { get; set; }
        public String DivId { get; set; }
        public String FundType { get; set; }
        public Boolean IsRevised { get; set; }
        public Boolean IsCONT { get; set; }
        private  clsData c_data = new clsData();
        public String  FundSourceId { get; set; }
        private List<frmPPMPLoad_RespoList> ListFundsource = new List<frmPPMPLoad_RespoList>();

      
        public frmPPMPLoad()
        {
            InitializeComponent();
            this.Loaded += frmPPMPLoad_Loaded;
        }

        void frmPPMPLoad_Loaded(object sender, RoutedEventArgs e)
        {
            LoadYear();
            LoadPPMPList();
            LoadFundSource();
          
        }

        private void LoadYear() 
        {
            lblYear.Content = WorkingYear;

        }
        private void LoadPPMPList() 
        {
            var sb = new System.Text.StringBuilder(199);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"'Main' as MainPPMP");
            sb.AppendLine(@"UNION");
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"'Revision' as MainPPMP");
            sb.AppendLine(@"FROM mnda_alignment_record mar");
            sb.AppendLine(@"WHERE mar.division_pap = "+ DivId +" and mar.division_year = "+WorkingYear +"");
            sb.AppendLine(@"and mar.is_active = 1 and mar.is_approved = 1 ");

            DataTable dtData = c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Copy();

            grdData.DataSource = dtData.DefaultView;

            
        }
        private void LoadFundSource(String DivId)
        {
            var sb = new System.Text.StringBuilder(84);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mrl.respo_name,");
            sb.AppendLine(@"mrl.code,");
            sb.AppendLine(@"mrl.fund_type");
            sb.AppendLine(@"FROM mnda_respo_library mrl");
            sb.AppendLine(@"WHERE mrl.div_id = "+ DivId +" and mrl.working_year = '"+ WorkingYear +"'");


            DataTable dtData = c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Copy();

            grdData.DataSource = dtData.DefaultView;


        }
        public void LoadFundSource()
        {
            ListFundsource.Clear();
            cmbFundsource.Items.Clear();

            var sb = new System.Text.StringBuilder(84);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mrl.respo_name,");
            sb.AppendLine(@"mrl.code,");
            sb.AppendLine(@"mrl.fund_type");
            sb.AppendLine(@"FROM mnda_respo_library mrl");
            sb.AppendLine(@"WHERE mrl.div_id = " + DivId + " and mrl.working_year = '"+ WorkingYear +"'");

           
            foreach (DataRow item in c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Rows)
            {
                frmPPMPLoad_RespoList _var = new frmPPMPLoad_RespoList();

                _var.code = item["code"].ToString();
                _var.respo_name = item["respo_name"].ToString();
                _var.fund_type = item["fund_type"].ToString();
                ListFundsource.Add(_var);
                cmbFundsource.Items.Add(_var.respo_name);
            }

     
        }

        private void btnSelect_Click(object sender, RoutedEventArgs e)
        {
            if (grdData.ActiveCell.Value.ToString() =="Main")
            {
                IsRevised = false;
            }
            else
            {
                IsRevised = true;
            }
            this.DialogResult = true;
        }

        private void cmbFundsource_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                btnSelect.IsEnabled = true;
                this.FundSourceId = ListFundsource.Where(x => x.respo_name == cmbFundsource.SelectedItem.ToString()).ToList()[0].code.ToString();
                this.FundType = ListFundsource.Where(x => x.respo_name == cmbFundsource.SelectedItem.ToString()).ToList()[0].fund_type.ToString();
                if (cmbFundsource.SelectedItem.ToString().Contains("CONT"))
                {
                    this.IsCONT = true;
                }
                else
                {
                    this.IsCONT = false;
                }
            }
            catch (Exception)
            {
                
                
            }
           
        }
    }
    public class frmPPMPLoad_RespoList 
    {
        public String respo_name { get; set; }
        public String code { get; set; }
        public String fund_type { get; set; }
    }
}
