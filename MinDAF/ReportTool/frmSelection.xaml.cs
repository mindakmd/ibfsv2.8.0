﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ReportTool
{
    /// <summary>
    /// Interaction logic for frmSelection.xaml
    /// </summary>
    public partial class frmSelection : Window
    {

        public string Selected { get; set; }
        public frmSelection()
        {
            InitializeComponent();
        }

        private void btnFinanceReports_Click(object sender, RoutedEventArgs e)
        {
            this.Selected = "Finance Reports";
            this.DialogResult = true;
        }

        private void btnPPMPReports_Click(object sender, RoutedEventArgs e)
        {
            this.Selected = "PPMP Reports";
            this.DialogResult = true;
        }
    }
}
