﻿using System;
using System.IO;
using Microsoft.Win32;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ReportTool
{
    /// <summary>
    /// Interaction logic for frmExport.xaml
    /// </summary>
    public partial class frmExport : Window
    {
        public String FileName { get; set; }
        public String FilePath { get; set; }
        public frmExport()
        {
            InitializeComponent();
            
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {

            if (string.IsNullOrWhiteSpace(txtFileName.Text) && string.IsNullOrWhiteSpace(txtFilePath.Text))
            {
                MessageBox.Show("Please complete information.");
            }
            else
            {
                FilePath = txtFilePath.Text;
                FileName = txtFileName.Text;
                this.DialogResult = false;
            }
            

            //this.DialogResult = true;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.ValidateNames = false;
            dlg.CheckFileExists = false;
            dlg.CheckPathExists = true;
            dlg.FileName = "Folder Selection";
            bool? UserClickedOK = dlg.ShowDialog();
            if (UserClickedOK == true)
            {
                string folderPath = System.IO.Path.GetDirectoryName(dlg.FileName);
                txtFilePath.Text = folderPath;
            }
        }
    }
}
