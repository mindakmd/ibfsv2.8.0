﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ReportTool
{
    /// <summary>
    /// Interaction logic for frmYearSelect.xaml
    /// </summary>
    public partial class frmYearSelect : Window
    {
        public String WorkingYear { get; set; }
        public frmYearSelect()
        {
            InitializeComponent();
            this.Loaded += frmYearSelect_Loaded;
        }

        void frmYearSelect_Loaded(object sender, RoutedEventArgs e)
        {
            GenerateMonths();
        }

        private void GenerateMonths() 
        {
            int _year = DateTime.Now.Year;
            int _index = 20;
            _year -= 1;
            cmbYear.Items.Clear();

            for (int i = 0; i <= _index; i++)
            {
                cmbYear.Items.Add(_year.ToString());
                _year += 1;
            }

            cmbYear.SelectedIndex = 1;
        }
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            WorkingYear = cmbYear.SelectedItem.ToString();

            this.DialogResult = true;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
