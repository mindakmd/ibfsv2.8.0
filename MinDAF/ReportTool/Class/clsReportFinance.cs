﻿using MinDAF;
using ReportTool.MinDAF;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;

namespace ReportTool.Class
{
    class clsReportFinance
    {
        private MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();
        private MinDAFSVCClient svc_service = new MinDAFSVCClient();
        public List<DivisionList> DivisionData = new List<DivisionList>();
        public UserDetail UserInfo = new UserDetail();


        public Boolean ExecuteQuery(string _SQL)
        {
            SqlConnection conn = new SqlConnection();
            string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=mindafinance;User Id=sa;Password=minda1234";
            // string ConnectionString = @"Server=LOCALHOST\SQLEXPRESS;Database=mindafinance_dirty;User Id=sa;Password=minda1234;";
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionString;
            con.Open();
            SqlCommand comm = new SqlCommand(_SQL, con);

            int result = comm.ExecuteNonQuery();
            con.Close();
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public DataSet ExecuteSQLQuery(String _sql)
        {
            String _ReturnData = "";

            SqlConnection conn = new SqlConnection();
            string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance;User Id=sa;Password=minda1234";
            //string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=mindafinance_dirty;User Id=sa;Password=minda1234;"; 
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionString;
            SqlDataAdapter da = new SqlDataAdapter(_sql, con);
            DataSet ds = new DataSet();


            da.Fill(ds);




            return ds;
        }

        public DataSet FetchFDCache()
        {
            DataSet _dsData = new DataSet();
  
            var sb = new System.Text.StringBuilder(97);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  report_command,");
            sb.AppendLine(@"  division_id,");
            sb.AppendLine(@"  requested_year");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_fd_report;");


            _dsData = ExecuteSQLQuery(sb.ToString());

            ExecuteQuery("DELETE FROM dbo.mnda_fd_report;");

            return _dsData;
        }
        public DataSet ProcessBedsReport(String WorkingYear) 
        {
            DataSet ds = new DataSet();
            DataTable dtPAP = new DataTable();
            DataTable dtSUBPAP = new DataTable();
            DataTable dtDIVUNITS = new DataTable();
            DataTable dtDIVOFFICE = new DataTable();
            DataTable dtFundSource = new DataTable();

            List<lib_PAP> _PAP = new List<lib_PAP>();
            List<lib_SUB_PAP> _SUBPAP = new List<lib_SUB_PAP>();
            List<DivisionBudgetAllocationFields> _DIVUNITS = new List<DivisionBudgetAllocationFields>();
            List<DivisionBudgetAllocationFields> _DIVOFFICE = new List<DivisionBudgetAllocationFields>();

            var sb = new System.Text.StringBuilder(109);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  DBM_Sub_Pap_id,");
            sb.AppendLine(@"  DBM_Pap_Id,");
            sb.AppendLine(@"  DBM_Sub_Pap_Code,");
            sb.AppendLine(@"  DBM_Sub_Pap_Desc");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.DBM_Sub_Pap ");
            sb.AppendLine(@"  WHERE WorkingYear ='" + WorkingYear + "';");

            dtPAP = ExecuteSQLQuery(sb.ToString()).Tables[0].Copy();

            foreach (DataRow item in dtPAP.Rows)
            {
                lib_PAP _pap = new lib_PAP();

                _pap.DBM_Pap_Id = item["DBM_Pap_Id"].ToString();
                _pap.DBM_Sub_Pap_Code = item["DBM_Sub_Pap_Code"].ToString();
                _pap.DBM_Sub_Pap_Desc = item["DBM_Sub_Pap_Desc"].ToString();
                _pap.DBM_Sub_Pap_id = item["DBM_Sub_Pap_id"].ToString();

                _PAP.Add(_pap);

            }

            sb.Clear();
          

            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Sub_Id,");
            sb.AppendLine(@"  DBM_Sub_Id,");
            sb.AppendLine(@"  Description,");
            sb.AppendLine(@"  PAP");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.DBM_Sub_Office ");
            sb.AppendLine(@"  WHERE WorkingYear ='" + WorkingYear + "';");

            dtSUBPAP = ExecuteSQLQuery(sb.ToString()).Tables[0].Copy();

            foreach (DataRow item in dtSUBPAP.Rows)
            {
                lib_SUB_PAP _pap = new lib_SUB_PAP();
                _pap.Sub_Id = item["Sub_Id"].ToString();
                _pap.DBM_Sub_Id = item["DBM_Sub_Id"].ToString();
                _pap.Description = item["Description"].ToString();
                _pap.PAP = item["PAP"].ToString();
               

                _SUBPAP.Add(_pap);

            }
            sb.Clear();
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Division_Id,");
            sb.AppendLine(@"  DBM_Sub_Pap_Id,");
            sb.AppendLine(@"  Division_Code,");
            sb.AppendLine(@"  Division_Desc,UnitCode");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.Division WHERE is_sub_division = 1 and year_setup ='" + WorkingYear + "'");

            dtDIVUNITS = ExecuteSQLQuery(sb.ToString()).Tables[0].Copy();

            foreach (DataRow item in dtDIVUNITS.Rows)
            {
                DivisionBudgetAllocationFields _pap = new DivisionBudgetAllocationFields();

                _pap.DBM_Sub_Pap_Id = item["DBM_Sub_Pap_Id"].ToString();
                _pap.Division_Code = item["Division_Code"].ToString();
                _pap.Division_Desc = item["Division_Desc"].ToString();
                _pap.Division_Id = item["Division_Id"].ToString();
                _pap.UnitCode = item["UnitCode"].ToString();

                _DIVUNITS.Add(_pap);

            }

            sb.Clear();
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Division_Id,");
            sb.AppendLine(@"  DBM_Sub_Pap_Id,");
            sb.AppendLine(@"  Division_Code,");
            sb.AppendLine(@"  Division_Desc,UnitCode");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.Division  WHERE is_sub_division = 0 and year_setup ='" + WorkingYear + "'");

            dtDIVOFFICE = ExecuteSQLQuery(sb.ToString()).Tables[0].Copy();

            foreach (DataRow item in dtDIVOFFICE.Rows)
            {
                DivisionBudgetAllocationFields _pap = new DivisionBudgetAllocationFields();

                _pap.DBM_Sub_Pap_Id = item["DBM_Sub_Pap_Id"].ToString();
                _pap.Division_Code = item["Division_Code"].ToString();
                _pap.Division_Desc = item["Division_Desc"].ToString();
                _pap.Division_Id = item["Division_Id"].ToString();
                _pap.UnitCode = item["UnitCode"].ToString();

                _DIVOFFICE.Add(_pap);

            }
            StringBuilder varname1 = new StringBuilder();
            varname1.Append("SELECT \n");
            varname1.Append("mrl.div_id, \n");
            varname1.Append("mrl.code \n");
            varname1.Append("FROM mnda_respo_library mrl \n");
            varname1.Append("WHERE mrl.working_year = "+ WorkingYear +" and mrl.fund_type ='mf-1'");

            dtFundSource = ExecuteSQLQuery(varname1.ToString()).Tables[0].Copy();

              List<lib_MainBudgetAllowance> ListBudgetMain = new List<lib_MainBudgetAllowance>();
              List<lib_SUB_PAP> ListSubPap = new List<lib_SUB_PAP>();
              List<DivisionBudgetAllocationFields> ListDivision = new List<DivisionBudgetAllocationFields>();
              List<DivisionBudgetAllocationFields> ListDivisionUnits = new List<DivisionBudgetAllocationFields>();


            foreach (lib_PAP item in _PAP)
            {
                lib_MainBudgetAllowance _xdata = new lib_MainBudgetAllowance();
                _xdata.Id = item.DBM_Sub_Pap_id;
                _xdata.Pap = item.DBM_Sub_Pap_Code;
                _xdata.Description = item.DBM_Sub_Pap_Desc;
                _xdata.IsDiv = false;
                _xdata.IsUnit = false;
                _xdata.IsSubPAP = false;
                ListBudgetMain.Add(_xdata);

                List<lib_SUB_PAP> subpap = _SUBPAP.Where(x => x.DBM_Sub_Id == item.DBM_Sub_Pap_id).ToList();

                foreach (lib_SUB_PAP itemsubpap in subpap)
                {
                    _xdata = new lib_MainBudgetAllowance();
                    _xdata.Id = itemsubpap.Sub_Id;
                    _xdata.Pap = itemsubpap.PAP;
                    _xdata.Description = "       " + itemsubpap.Description;
                    _xdata.IsDiv = false;
                    _xdata.IsUnit = false;
                    _xdata.IsSubPAP = true;
                    ListBudgetMain.Add(_xdata);


                    List<DivisionBudgetAllocationFields> div = _DIVOFFICE.Where(x => x.Division_Code == itemsubpap.PAP).ToList();
                    foreach (DivisionBudgetAllocationFields itemdiv in div)
                    {
                        
                        String FundCode = "";
                        dtFundSource.DefaultView.RowFilter = "div_id = '" + itemdiv.Division_Id + "'";
                        if (dtFundSource.DefaultView.Count!=0)
                        {
                            FundCode = dtFundSource.DefaultView[0]["code"].ToString();
                        }
                        
                        _xdata = new lib_MainBudgetAllowance();
                        _xdata.Id = itemdiv.Division_Id;
                        _xdata.Pap = itemdiv.Division_Code;
                        _xdata.Description = "       " + itemdiv.Division_Code + " - " + itemdiv.Division_Desc;
                        _xdata.IsDiv = true;
                        _xdata.IsUnit = false;
                        _xdata.IsSubPAP = false;
                        _xdata.FundSource = FundCode;

                        double _JanTotal = 0.00;
                        double _FebTotal = 0.00;
                        double _MarTotal = 0.00;
                        double _AprTotal = 0.00;
                        double _MayTotal = 0.00;
                        double _JunTotal = 0.00;
                        double _JulTotal = 0.00;
                        double _AugTotal = 0.00;
                        double _SepTotal = 0.00;
                        double _OctTotal = 0.00;
                        double _NovTotal = 0.00;
                        double _DecTotal = 0.00;
                        double _LineTax = 0.00;
                        double _SubTotal = 0.00;


                        List<lib_ActivityTotals> _total = FetchTotals(FundCode, WorkingYear);

                        foreach (lib_ActivityTotals item_t in _total)
                        {
                           
                            switch (item_t.Months)
                            {
                                case "Jan":_JanTotal += item_t.Total;break;
                                case "Feb":_FebTotal += item_t.Total;break;
                                case "Mar":_MarTotal += item_t.Total;break;
                                case "Apr":_AprTotal+= item_t.Total;break;
                                case "May":_MayTotal += item_t.Total;break;
                                case "Jun":_JunTotal += item_t.Total;break;
                                case "Jul":_JulTotal += item_t.Total;break;
                                case "Aug":_AugTotal += item_t.Total;break;
                                case "Sep":_SepTotal += item_t.Total;break;
                                case "Oct":_OctTotal += item_t.Total;break;
                                case "Nov":_NovTotal += item_t.Total;break;
                                case "Dec":_DecTotal += item_t.Total;break;
                            }
                        }
                        double _Budget = FetchBudgetAllocation(_xdata.Id, FundCode, WorkingYear);
                       
                        double _TotalPlan = _JanTotal + _FebTotal + _MarTotal + _AprTotal + _MayTotal + _JunTotal + _JulTotal + _AugTotal + _SepTotal + _OctTotal + _NovTotal + _DecTotal;

                        double _AllocationPerMonth = Math.Round((_Budget - _TotalPlan) / 12, 2, MidpointRounding.AwayFromZero);

                        _JanTotal += _AllocationPerMonth;
                        _FebTotal += _AllocationPerMonth;
                        _MarTotal += _AllocationPerMonth;
                        _AprTotal += _AllocationPerMonth;
                        _MayTotal += _AllocationPerMonth;
                        _JunTotal += _AllocationPerMonth;
                        _JulTotal += _AllocationPerMonth;
                        _AugTotal += _AllocationPerMonth;
                        _SepTotal += _AllocationPerMonth;
                        _OctTotal += _AllocationPerMonth;
                        _NovTotal += _AllocationPerMonth;
                        _DecTotal += _AllocationPerMonth;

                        _LineTax = (_JanTotal * 0.05) +
                                    (_FebTotal * 0.05) +
                                    (_MarTotal * 0.05) +
                                    (_AprTotal * 0.05) +
                                    (_MayTotal * 0.05) +
                                    (_JunTotal * 0.05) +
                                    (_JulTotal * 0.05) +
                                    (_AugTotal * 0.05) +
                                    (_SepTotal * 0.05) +
                                    (_OctTotal * 0.05) +
                                    (_NovTotal * 0.05) +
                                    (_DecTotal * 0.05);
                       
                        _JanTotal = Math.Round( _JanTotal- (_JanTotal * 0.05),2, MidpointRounding.AwayFromZero);
                        _FebTotal = Math.Round(  _FebTotal -(_FebTotal * 0.05),2, MidpointRounding.AwayFromZero);
                        _MarTotal =  Math.Round( _MarTotal -(_MarTotal * 0.05),2, MidpointRounding.AwayFromZero);
                        _AprTotal =  Math.Round( _AprTotal -(_AprTotal * 0.05),2, MidpointRounding.AwayFromZero);
                        _MayTotal =  Math.Round( _MayTotal -(_MayTotal * 0.05),2, MidpointRounding.AwayFromZero);
                        _JunTotal =  Math.Round( _JunTotal -(_JunTotal * 0.05),2, MidpointRounding.AwayFromZero);
                        _JulTotal =  Math.Round( _JulTotal -(_JulTotal * 0.05),2, MidpointRounding.AwayFromZero);
                        _AugTotal =  Math.Round( _AugTotal -(_AugTotal * 0.05),2, MidpointRounding.AwayFromZero);
                        _SepTotal =  Math.Round( _SepTotal -(_SepTotal * 0.05),2, MidpointRounding.AwayFromZero);
                        _OctTotal =  Math.Round( _OctTotal -(_OctTotal * 0.05),2, MidpointRounding.AwayFromZero);
                        _NovTotal =  Math.Round( _NovTotal -(_NovTotal * 0.05),2, MidpointRounding.AwayFromZero);
                        _DecTotal = Math.Round(_DecTotal - (_DecTotal * 0.05), 2, MidpointRounding.AwayFromZero);



                        _SubTotal = (_JanTotal) +
                         (_FebTotal) +
                         (_MarTotal) +
                         (_AprTotal) +
                         (_MayTotal) +
                         (_JunTotal) +
                         (_JulTotal) +
                         (_AugTotal) +
                         (_SepTotal) +
                         (_OctTotal) +
                         (_NovTotal) +
                         (_DecTotal);


                        _xdata.Jan = (_JanTotal).ToString();
                        _xdata.Feb = (_FebTotal).ToString();
                        _xdata.Mar = (_MarTotal).ToString();
                        _xdata.Apr = (_AprTotal).ToString();
                        _xdata.May = (_MayTotal).ToString();
                        _xdata.Jun = (_JunTotal).ToString();
                        _xdata.Jul = (_JulTotal).ToString();
                        _xdata.Aug = (_AugTotal).ToString();
                        _xdata.Sep = (_SepTotal).ToString();
                        _xdata.Oct = (_OctTotal).ToString();
                        _xdata.Nov = (_NovTotal).ToString();
                        _xdata.Dec = (_DecTotal).ToString();

                  

                        _xdata.Q1 = (Convert.ToDouble(_xdata.Jan) + Convert.ToDouble(_xdata.Feb) + Convert.ToDouble(_xdata.Mar)).ToString();
                        _xdata.Q2 = (Convert.ToDouble(_xdata.Apr) + Convert.ToDouble(_xdata.May) + Convert.ToDouble(_xdata.Jun)).ToString();
                        _xdata.Q3 = (Convert.ToDouble(_xdata.Jul) + Convert.ToDouble(_xdata.Aug) + Convert.ToDouble(_xdata.Sep)).ToString();
                        _xdata.Q4 = (Convert.ToDouble(_xdata.Oct) + Convert.ToDouble(_xdata.Nov) + Convert.ToDouble(_xdata.Dec)).ToString();

                        //double _Totals = Convert.ToDouble( _xdata.Q1 ) + Convert.ToDouble( _xdata.Q2) + Convert.ToDouble(_xdata.Q3 ) + Convert.ToDouble(_xdata.Q4);
                        double _Totals =Math.Round(_SubTotal + _LineTax,1, MidpointRounding.AwayFromZero);
                        _xdata.SubTotal =Math.Round(_SubTotal,1, MidpointRounding.AwayFromZero).ToString();
                        _xdata.TotalTax = Math.Round(_LineTax, 1, MidpointRounding.AwayFromZero).ToString();
                        _xdata.Total =_Totals.ToString();
                        

                        ListBudgetMain.Add(_xdata);


                        //List<DivisionBudgetAllocationFields> div_unit = _DIVUNITS.Where(x => x.UnitCode == itemdiv.Division_Id).ToList();
                        //foreach (DivisionBudgetAllocationFields item_unit in div_unit)
                        //{
                        //    _xdata = new lib_MainBudgetAllowance();
                        //    _xdata.Id = item_unit.Division_Id;
                        //    _xdata.Pap = item_unit.Division_Code;
                        //    _xdata.Description = "                          " + item_unit.Division_Desc;
                        //    _xdata.IsDiv = false;
                        //    _xdata.IsUnit = true;
                        //    _xdata.IsSubPAP = false;
                        //    ListBudgetMain.Add(_xdata);
                        //}
                    }
                }
            }
            List<lib_MainBudgetAllowance> _FinalData = ListBudgetMain;
            foreach (lib_MainBudgetAllowance item in ListBudgetMain)
            {
                if (item.IsSubPAP == true && item.IsDiv ==false&& item.IsUnit == false)
                {
                    double _JanTotal = 0.00;
                    double _FebTotal = 0.00;
                    double _MarTotal = 0.00;
                    double _AprTotal = 0.00;
                    double _MayTotal = 0.00;
                    double _JunTotal = 0.00;
                    double _JulTotal = 0.00;
                    double _AugTotal = 0.00;
                    double _SepTotal = 0.00;
                    double _OctTotal = 0.00;
                    double _NovTotal = 0.00;
                    double _DecTotal = 0.00;
                    double _SubTotal = 0.00;
                    double _TaxTotal = 0.00;

                    List<lib_MainBudgetAllowance> _result = _FinalData.Where(x => x.Pap == item.Pap && x.IsDiv == true).ToList();
                    foreach (lib_MainBudgetAllowance item_r in _result)
                    {
                         _JanTotal += Convert.ToDouble(item_r.Jan);
                         _FebTotal += Convert.ToDouble(item_r.Feb);
                         _MarTotal += Convert.ToDouble(item_r.Mar);
                         _AprTotal += Convert.ToDouble(item_r.Apr);
                         _MayTotal += Convert.ToDouble(item_r.May);
                         _JunTotal += Convert.ToDouble(item_r.Jun);
                         _JulTotal += Convert.ToDouble(item_r.Jul);
                         _AugTotal += Convert.ToDouble(item_r.Aug);
                         _SepTotal += Convert.ToDouble(item_r.Sep);
                         _OctTotal += Convert.ToDouble(item_r.Oct);
                         _NovTotal += Convert.ToDouble(item_r.Nov);
                         _DecTotal += Convert.ToDouble(item_r.Dec);
                         _SubTotal += Convert.ToDouble(item_r.SubTotal);
                         _TaxTotal += Convert.ToDouble(item_r.TotalTax);

                    }
                    item.Jan = _JanTotal.ToString();
                    item.Feb = _FebTotal.ToString();
                    item.Mar = _MarTotal.ToString();
                    item.Apr = _AprTotal.ToString();
                    item.May = _MayTotal.ToString();
                    item.Jun = _JunTotal.ToString();
                    item.Jul = _JulTotal.ToString();
                    item.Aug = _AugTotal.ToString();
                    item.Sep = _SepTotal.ToString();
                    item.Oct = _OctTotal.ToString();
                    item.Nov = _NovTotal.ToString();
                    item.Dec = _DecTotal.ToString();

                    item.Q1 = (_JanTotal + _FebTotal + _MarTotal).ToString();
                    item.Q2 = (_AprTotal + _MayTotal + _JunTotal).ToString(); 
                    item.Q3 = (_JulTotal + _AugTotal + _SepTotal).ToString(); 
                    item.Q4 = (_OctTotal + _NovTotal + _DecTotal).ToString();

                 //   double _Totals = Convert.ToDouble(item.Q1) + Convert.ToDouble(item.Q2) + Convert.ToDouble(item.Q3) + Convert.ToDouble(item.Q4);

                    double _Totals_F = _SubTotal + _TaxTotal;
                    item.SubTotal = _SubTotal.ToString();
                    item.TotalTax = _TaxTotal.ToString();
                    item.Total = _Totals_F.ToString();

                  //  item.Total = _Totals.ToString();
                       

                }   
            }
                double _JanTotal_F = 0.00;
                double _FebTotal_F = 0.00;
                double _MarTotal_F = 0.00;
                double _AprTotal_F = 0.00;
                double _MayTotal_F = 0.00;
                double _JunTotal_F = 0.00;
                double _JulTotal_F = 0.00;
                double _AugTotal_F = 0.00;
                double _SepTotal_F = 0.00;
                double _OctTotal_F = 0.00;
                double _NovTotal_F = 0.00;
                double _DecTotal_F = 0.00;
                double _SubTotal_F = 0.00;
                double _TotalTax_F = 0.00;
                double _Total_F = 0.00;

            foreach (lib_MainBudgetAllowance item in ListBudgetMain)
            {
               

                if (item.IsSubPAP == false && item.IsDiv == true && item.IsUnit == false)
                {
                   // double _test = Convert.ToDouble(item.Jan) - (Convert.ToDouble(item.Jan) * 0.05);
                    _JanTotal_F +=Convert.ToDouble(item.Jan) ;
                    _FebTotal_F += Convert.ToDouble(item.Feb);
                    _MarTotal_F += Convert.ToDouble(item.Mar);
                    _AprTotal_F += Convert.ToDouble(item.Apr);
                    _MayTotal_F += Convert.ToDouble(item.May);
                    _JunTotal_F += Convert.ToDouble(item.Jun);
                    _JulTotal_F += Convert.ToDouble(item.Jul);
                    _AugTotal_F += Convert.ToDouble(item.Aug);
                    _SepTotal_F += Convert.ToDouble(item.Sep) ;
                    _OctTotal_F += Convert.ToDouble(item.Oct);
                    _NovTotal_F += Convert.ToDouble(item.Nov);
                    _DecTotal_F += Convert.ToDouble(item.Dec);
                    _SubTotal_F += Convert.ToDouble(item.SubTotal);
                    _TotalTax_F += Convert.ToDouble(item.TotalTax);
                    _Total_F += Convert.ToDouble(item.Total);
                }
            }


            lib_MainBudgetAllowance _lastLine = new lib_MainBudgetAllowance();
            _lastLine.Description = "TOTAL :";
            _lastLine.Jan = _JanTotal_F.ToString();
            _lastLine.Feb = _FebTotal_F.ToString();
            _lastLine.Mar = _MarTotal_F.ToString();
            _lastLine.Apr = _AprTotal_F.ToString();
            _lastLine.May = _MayTotal_F.ToString();
            _lastLine.Jun = _JunTotal_F.ToString();
            _lastLine.Jul = _JulTotal_F.ToString();
            _lastLine.Aug = _AugTotal_F.ToString();
            _lastLine.Sep = _SepTotal_F.ToString();
            _lastLine.Oct = _OctTotal_F.ToString();
            _lastLine.Nov = _NovTotal_F.ToString();
            _lastLine.Dec = _DecTotal_F.ToString();

            _lastLine.Q1 = (_JanTotal_F + _FebTotal_F + _MarTotal_F).ToString();
            _lastLine.Q2 = (_AprTotal_F + _MayTotal_F + _JunTotal_F).ToString();
            _lastLine.Q3 = (_JulTotal_F + _AugTotal_F + _SepTotal_F).ToString();
            _lastLine.Q4 = (_OctTotal_F + _NovTotal_F + _DecTotal_F).ToString();

           // double _Totals_F = Convert.ToDouble(_lastLine.Q1) + Convert.ToDouble(_lastLine.Q2) + Convert.ToDouble(_lastLine.Q3) + Convert.ToDouble(_lastLine.Q4);

            double _Totals_F_ = _SubTotal_F + _TotalTax_F;
            _lastLine.SubTotal = _TotalTax_F.ToString();
            _lastLine.TotalTax = _SubTotal_F.ToString();
            _lastLine.Total = _Totals_F_.ToString();


            _lastLine.Total = _Totals_F_.ToString();
            
            ListBudgetMain.Add(_lastLine); 

            

            DataTable dtFinal = new DataTable();
            dtFinal.Columns.Add("isTop");
            dtFinal.Columns.Add("Description");
            dtFinal.Columns.Add("Jan");
            dtFinal.Columns.Add("Feb");
            dtFinal.Columns.Add("Mar");
            dtFinal.Columns.Add("Q1");
            dtFinal.Columns.Add("Apr");
            dtFinal.Columns.Add("May");
            dtFinal.Columns.Add("Jun");
            dtFinal.Columns.Add("Q2");
            dtFinal.Columns.Add("Jul");
            dtFinal.Columns.Add("Aug");
            dtFinal.Columns.Add("Sep");
            dtFinal.Columns.Add("Q3");
            dtFinal.Columns.Add("Oct");
            dtFinal.Columns.Add("Nov");
            dtFinal.Columns.Add("Dec");
            dtFinal.Columns.Add("Q4");
            dtFinal.Columns.Add("SubTotal");
            dtFinal.Columns.Add("TaxTotal");
            dtFinal.Columns.Add("Total");

            foreach (lib_MainBudgetAllowance item in ListBudgetMain)
            {
                DataRow dr = dtFinal.NewRow();
                if (item.IsSubPAP == true && item.IsDiv ==false&& item.IsUnit == false)
                {
                     dr["isTop"] ="1";
                }
                else if (item.IsSubPAP == false && item.IsDiv ==true&& item.IsUnit == false)
                {
                    dr["isTop"] ="2";
                }
                else
                {
                    dr["isTop"] = "0";
                }
               

                dr["Description"] = item.Description;
                dr["Jan"] =item.Jan;
                dr["Feb"] = item.Feb;
                dr["Mar"] = item.Mar;
                dr["Q1"] = item.Q1;
                dr["Apr"] = item.Apr;
                dr["May"] = item.May;
                dr["Jun"] = item.Jun;
                dr["Q2"] = item.Q2;
                dr["Jul"] = item.Jul;
                dr["Aug"] = item.Aug;
                dr["Sep"] = item.Sep;
                dr["Q3"] = item.Q3;
                dr["Oct"] = item.Oct;
                dr["Nov"] = item.Nov;
                dr["Dec"] = item.Dec;
                dr["Q4"] = item.Q4;
                dr["TaxTotal"] = item.TotalTax;
                dr["SubTotal"] = item.SubTotal;

                dr["Total"] = item.Total;

                dtFinal.Rows.Add(dr);
            }
            dtFinal.TableName = "BEDSDATA";
            ds.Tables.Add(dtFinal);
            ds.DataSetName = "BEDS";
            return ds;
        }
        FormatTitleFormat _h_c;
        public DataSet ProcessDivisionDistribution(Boolean IsCONT,String Year) 
        {
            List<FormatTitleFormat> _Header = new List<FormatTitleFormat>();

            _h_c = new FormatTitleFormat(); _h_c.isMain = "1"; _h_c.Code = "50201000"; _h_c.Header = "Traveling Expenses"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5020101000"; _h_c.Header = "     Traveling Expenses - Local"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5020102000"; _h_c.Header = "     Traveling Expenses - Foreign"; _Header.Add(_h_c);

            _h_c = new FormatTitleFormat(); _h_c.isMain = "1"; _h_c.Code = "50202000"; _h_c.Header = "Training and Scholarship Expenses"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5020201002"; _h_c.Header = "     Training Expenses"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5020201001"; _h_c.Header = "     ICT Training Expenses"; _Header.Add(_h_c);

            _h_c = new FormatTitleFormat(); _h_c.isMain = "1"; _h_c.Code = "50203000"; _h_c.Header = "Supplies and Materials Expenses"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5020301000"; _h_c.Header = "     Office Supplies Expenses"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5020302000"; _h_c.Header = "     Accountable Forms Expenses"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5020309000"; _h_c.Header = "     Fuel, Oil and Lubricants Expenses"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "50203011002"; _h_c.Header = "     ICT Office Supplies"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5060405003"; _h_c.Header = "     ICT Office Equipment"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5020399000"; _h_c.Header = "     Other Supplies and Materials Expenses"; _Header.Add(_h_c);

            _h_c = new FormatTitleFormat(); _h_c.isMain = "1"; _h_c.Code = "50204000"; _h_c.Header = "Utility Expenses"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5020401000"; _h_c.Header = "     Water Expenses"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5020402000"; _h_c.Header = "     Electricity Expenses"; _Header.Add(_h_c);

            _h_c = new FormatTitleFormat(); _h_c.isMain = "1"; _h_c.Code = "50205000"; _h_c.Header = "Communication Expenses"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5020501000"; _h_c.Header = "     Postage and Courier"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5020502001"; _h_c.Header = "     Mobile"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5020502002"; _h_c.Header = "     Landline"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5020503000"; _h_c.Header = "     Internet Subscription Expenses"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5020504000"; _h_c.Header = "     Cable, Satellite Expenses"; _Header.Add(_h_c);

            _h_c = new FormatTitleFormat(); _h_c.isMain = "1"; _h_c.Code = "50210030"; _h_c.Header = "Extraordinary and Miscellaneous Expenses (EME)"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5021003000"; _h_c.Header = "     Extraordinary and Miscellaneous Expenses (EME)"; _Header.Add(_h_c);

            _h_c = new FormatTitleFormat(); _h_c.isMain = "1"; _h_c.Code = "50211000"; _h_c.Header = "Professional Services"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5021102000"; _h_c.Header = "     Auditing Services"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5021103000"; _h_c.Header = "     Consultancy Services"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5021199000"; _h_c.Header = "     Other Professional Services"; _Header.Add(_h_c);

            _h_c = new FormatTitleFormat(); _h_c.isMain = "1"; _h_c.Code = "50212000"; _h_c.Header = "General Services"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5021202000"; _h_c.Header = "     Janitorial Services"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5021203000"; _h_c.Header = "     Security Services"; _Header.Add(_h_c);

            _h_c = new FormatTitleFormat(); _h_c.isMain = "1"; _h_c.Code = "50213000"; _h_c.Header = "Repairs and Maintenance"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5021399099"; _h_c.Header = "     Other Property, Plant and Equipment"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5021307000"; _h_c.Header = "     Furniture and Fixtures"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5021305003"; _h_c.Header = "     ICT Machinery and Equipment"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5021306001"; _h_c.Header = "     Motor Vehicles"; _Header.Add(_h_c);

            _h_c = new FormatTitleFormat(); _h_c.isMain = "1"; _h_c.Code = "50215000"; _h_c.Header = "Taxes, Insurances, Premiums and Other Fees"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5021501001"; _h_c.Header = "     Taxes, Duties and Licenses"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5021502000"; _h_c.Header = "     Fidelity Bond Premiums"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5021503000"; _h_c.Header = "     Insurance Expenses"; _Header.Add(_h_c);

            _h_c = new FormatTitleFormat(); _h_c.isMain = "1"; _h_c.Code = "50216000"; _h_c.Header = "Labor and Wages"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5021601000"; _h_c.Header = "     Labor and Wages (Job Order)"; _Header.Add(_h_c);

            _h_c = new FormatTitleFormat(); _h_c.isMain = "1"; _h_c.Code = "50299000"; _h_c.Header = "Other Maintenance & Operating Expenses"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5029901000"; _h_c.Header = "     Advertising Expenses"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5029902000"; _h_c.Header = "     Printing and Publication Expenses"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5029903000"; _h_c.Header = "     Representation Expenses"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5029999099"; _h_c.Header = "     Other Maintenance & Operating Expenses"; _Header.Add(_h_c);

            _h_c = new FormatTitleFormat(); _h_c.isMain = "1"; _h_c.Code = "50299050"; _h_c.Header = "Rent Expenses"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5029905001"; _h_c.Header = "     Buildings and Structures"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5029905003"; _h_c.Header = "     Motor Vehicles"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5029905004"; _h_c.Header = "     Equipment"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5029905008"; _h_c.Header = "     ICT Equipment"; _Header.Add(_h_c);

            _h_c = new FormatTitleFormat(); _h_c.isMain = "1"; _h_c.Code = "50299060"; _h_c.Header = "Membership Dues & Cont'ns to Organizations"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5029906000"; _h_c.Header = "     Membership Dues & Cont'ns to Organizations"; _Header.Add(_h_c);

            _h_c = new FormatTitleFormat(); _h_c.isMain = "1"; _h_c.Code = "50299070"; _h_c.Header = "Subscriptions Expenses"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5029907000"; _h_c.Header = "     Other Subscription"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5029907001"; _h_c.Header = "     Library and other reading materials"; _Header.Add(_h_c);
            _h_c = new FormatTitleFormat(); _h_c.isMain = "0"; _h_c.Code = "5029907002"; _h_c.Header = "     ICT Software Subscriptions"; _Header.Add(_h_c);

            DataSet _dsData = new DataSet();
            var sbD = new System.Text.StringBuilder(113);


            DataTable dtHeader = new DataTable();

            var sb = new System.Text.StringBuilder(711);

            if (Year =="2017")
            {
                sb.AppendLine(@"SELECT");
                sb.AppendLine(@"msub.uacs_code as code,");
                sb.AppendLine(@"mooe.name,");
                sb.AppendLine(@"msub.name as _expenditure,");
                sb.AppendLine(@"0.00 as _oc,");
                sb.AppendLine(@"0.00 as _oed,");
                sb.AppendLine(@"0.00 as _oed_iso,");
                sb.AppendLine(@"0.00 as _ad,");
                sb.AppendLine(@"0.00 as _ad_office_transfer,");
                sb.AppendLine(@"0.00 as _kmd_issp,");
                sb.AppendLine(@"0.00 as _fd,");
                sb.AppendLine(@"0.00 as _gms,");
                sb.AppendLine(@"0.00 as _prd,");
                sb.AppendLine(@"0.00 as _mdc,");
                sb.AppendLine(@"0.00 as _kmd,");
                sb.AppendLine(@"0.00 as _kmd_thematic_map,");
                sb.AppendLine(@"0.00 as _dpk,");
                sb.AppendLine(@"0.00 as _pfd,");
                sb.AppendLine(@"0.00 as _drpa,");
                sb.AppendLine(@"0.00 as _pdd,");
                sb.AppendLine(@"0.00 as _pdd_now,");
                sb.AppendLine(@"0.00 as _pdd_ppp,");
                sb.AppendLine(@"0.00 as _pdrg,");
                sb.AppendLine(@"0.00 as _amo_wm,");
                sb.AppendLine(@"0.00 as _amo_nm,");
                sb.AppendLine(@"0.00 as _amo_nem,");
                sb.AppendLine(@"0.00 as _amo_cm,");
                sb.AppendLine(@"0.00 as _amo,");
                sb.AppendLine(@"0.00 as _mirpp,");
                sb.AppendLine(@"0.00 as _ipd,");
                sb.AppendLine(@"0.00 as _ipd_cacao,");
                sb.AppendLine(@"0.00 as _purd,");
                sb.AppendLine(@"0.00 as _ipd_mpmc,");
                sb.AppendLine(@"0.00 as _ippr,");
                sb.AppendLine(@"0.00 as _ird,");
                sb.AppendLine(@"0.00 as _ird_eaga,");
                sb.AppendLine(@"0.00 as _ird_mct,");
                sb.AppendLine(@"0.00 as _mebeaga");
                sb.AppendLine(@"FROM mnda_mooe_expenditures mooe");
                sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures msub on msub.mooe_id = mooe.code");
                sb.AppendLine(@"WHERE msub.is_active = 1 ORDER BY mooe.id ASC");

                _dsData = ExecuteSQLQuery(sb.ToString());
            }
            else if (Year =="2018")
            {
                DataTable _data = new DataTable();

                _dsData = new DataSet();

                _data.Columns.Add("_code");
                _data.Columns.Add("_expenditure");
                _data.Columns.Add("_gms");
                _data.Columns.Add("_performance_management");
                _data.Columns.Add("_tsp");
                _data.Columns.Add("_legal_services");
                _data.Columns.Add("_pppdo");
                _data.Columns.Add("_pdrg");
                _data.Columns.Add("_is");
                _data.Columns.Add("_ip");
                _data.Columns.Add("_bimp_eaga");
                _data.Columns.Add("_total");

                foreach (FormatTitleFormat item in _Header)
                {
                    DataRow dr = _data.NewRow();
                    if (item.isMain=="1")
                    {
                        dr["_code"] = item.Code;
                        dr["_expenditure"] = item.Header;
                        dr["_gms"] = "";
                        dr["_performance_management"] = "";
                        dr["_tsp"] = "";
                        dr["_legal_services"] = "";
                        dr["_pppdo"] = "";
                        dr["_pdrg"] = "";
                        dr["_is"] = "";
                        dr["_ip"] = "";
                        dr["_bimp_eaga"] = "";
                        dr["_total"] = "";
                    }
                    else
                    {
                        dr["_code"] = item.Code;
                        dr["_expenditure"] = item.Header;
                        dr["_gms"] = "0.00";
                        dr["_performance_management"] = "0.00";
                        dr["_tsp"] = "0.00";
                        dr["_legal_services"] = "0.00";
                        dr["_pppdo"] = "0.00";
                        dr["_pdrg"] = "0.00";
                        dr["_is"] = "0.00";
                        dr["_ip"] = "0.00";
                        dr["_bimp_eaga"] = "0.00";
                        dr["_total"] = "0.00";
                    }
                   
                    _data.Rows.Add(dr);
                }
                _dsData.Tables.Add(_data);
                
            }
      

            if (IsCONT == false)
            {
                sbD.AppendLine(@"SELECT ");
                sbD.AppendLine(@"  division_id,");
                sbD.AppendLine(@"  mooe_id,");
                sbD.AppendLine(@"  Amount,");
                sbD.AppendLine(@"  Year");
                sbD.AppendLine(@"FROM ");
                sbD.AppendLine(@"  dbo.mnda_fund_source_line_item WHERE Year ="+Year +";");
            }
            else
            {
                sbD.AppendLine(@"SELECT ");
                sbD.AppendLine(@"mfs.Fund_Name,");
                sbD.AppendLine(@" mfsl.division_id,");
                sbD.AppendLine(@"  mfsl.mooe_id,");
                sbD.AppendLine(@"  mfsl.Amount,");
                sbD.AppendLine(@"  mfsl.Year ");
                sbD.AppendLine(@"FROM dbo.mnda_fund_source_line_item mfsl");
                sbD.AppendLine(@"INNER JOIN dbo.mnda_fund_source mfs on mfs.code = mfsl.Fund_Source_Id");
                sbD.AppendLine(@"WHERE mfsl.Year =2017 AND mfs.Fund_Name LIKE '%CONT%';");
            }


            switch (Year)
            {
                case "2017":
                             DataSet _dsFundD = new DataSet();
                            _dsFundD = ExecuteSQLQuery(sbD.ToString());

                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =1";
                            DataTable dtData_OC = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_OC.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_OC.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_oc"] = _total;
                            }

                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =2";
                            DataTable dtData_OED = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_OED.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_OED.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_oed"] = _total;
                            }

                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =16";
                            DataTable dtData_OED_ISO = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_OED_ISO.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_OED_ISO.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_oed_iso"] = _total;
                            }

                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =4";
                            DataTable dtData_AD = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_AD.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_AD.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_ad"] = _total;
                            }
                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =18";
                            DataTable dtData_AD_OFFICE = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_AD_OFFICE.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_AD_OFFICE.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_ad_office_transfer"] = _total;
                            }

                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =19";
                            DataTable dtData_KMD_ISSP = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_KMD_ISSP.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_KMD_ISSP.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_kmd_issp"] = _total;
                            }

                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =3";
                            DataTable dtData_FD = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_FD.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_FD.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_fd"] = _total;
                            }



                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {

                                double _total = 0.00;

                                _total += Convert.ToDouble(item["_oc"].ToString()) + Convert.ToDouble(item["_oed"].ToString()) +
                                Convert.ToDouble(item["_oed_iso"].ToString()) + Convert.ToDouble(item["_ad"].ToString()) +
                                Convert.ToDouble(item["_ad_office_transfer"].ToString()) + Convert.ToDouble(item["_kmd_issp"].ToString()) +
                                Convert.ToDouble(item["_fd"].ToString());

                                item["_gms"] = _total;


                            }
                            //  '----------------------------------------'

                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =5";
                            DataTable dtData_PRD = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_PRD.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_PRD.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_prd"] = _total;
                            }

                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =21";
                            DataTable dtData_MDC = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_MDC.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_MDC.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_mdc"] = _total;
                            }

                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =6";
                            DataTable dtData_KMD = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_KMD.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_KMD.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_kmd"] = _total;
                            }

                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =22";
                            DataTable dtData_Thematic = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_Thematic.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_Thematic.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_kmd_thematic_map"] = _total;
                            }


                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {

                                double _total = 0.00;

                                _total += Convert.ToDouble(item["_prd"].ToString()) + Convert.ToDouble(item["_mdc"].ToString()) +
                                Convert.ToDouble(item["_kmd"].ToString()) + Convert.ToDouble(item["_kmd_thematic_map"].ToString());

                                item["_dpk"] = _total;


                            }

                            //  '----------------------------------------'
                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =7";
                            DataTable dtData_PFD = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_PFD.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_PFD.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_pfd"] = _total;
                            }


                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {

                                double _total = 0.00;

                                _total += Convert.ToDouble(item["_pfd"].ToString());

                                item["_drpa"] = _total;


                            }

                            //  '----------------------------------------'


                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =8";
                            DataTable dtData_PDD = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_PDD.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_PDD.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_pdd"] = _total;
                            }

                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =23";
                            DataTable dtData_PDD_NOW = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_PDD_NOW.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_PDD_NOW.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_pdd_now"] = _total;
                            }

                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =24";
                            DataTable dtData_PDD_PPP = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_PDD_PPP.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_PDD_PPP.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_pdd_ppp"] = _total;
                            }



                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_PDD.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_PDD.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_pdrg"] = _total;
                            }


                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {

                                double _total = 0.00;

                                _total += Convert.ToDouble(item["_pdd"].ToString()) + Convert.ToDouble(item["_pdd_now"].ToString()) +
                                Convert.ToDouble(item["_pdd_ppp"].ToString());

                                item["_pdrg"] = _total;


                            }

                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =9";
                            DataTable dtData_AMO_WM = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_AMO_WM.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_AMO_WM.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_amo_wm"] = _total;
                            }



                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {

                                double _total = 0.00;

                                _total += Convert.ToDouble(item["_amo_wm"].ToString());

                                item["_amo_wm"] = _total;


                            }

                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =10";
                            DataTable dtData_AMO_NM = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_AMO_NM.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_AMO_NM.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_amo_nm"] = _total;
                            }



                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {

                                double _total = 0.00;

                                _total += Convert.ToDouble(item["_amo_nm"].ToString());

                                item["_amo_nm"] = _total;


                            }

                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =11";
                            DataTable dtData_AMO_NEM = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_AMO_NEM.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_AMO_NEM.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_amo_nem"] = _total;
                            }



                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {

                                double _total = 0.00;

                                _total += Convert.ToDouble(item["_amo_nem"].ToString());

                                item["_amo_nem"] = _total;


                            }
                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =12";
                            DataTable dtData_AMO_CM = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_AMO_CM.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_AMO_CM.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_amo_cm"] = _total;
                            }



                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {

                                double _total = 0.00;

                                _total += Convert.ToDouble(item["_amo_cm"].ToString());

                                item["_amo_cm"] = _total;


                            }



                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {

                                double _total = 0.00;

                                _total += Convert.ToDouble(item["_amo_wm"].ToString()) + Convert.ToDouble(item["_amo_nm"].ToString()) + Convert.ToDouble(item["_amo_nem"].ToString()) + Convert.ToDouble(item["_amo_cm"].ToString());

                                item["_mirpp"] = _total;


                            }



                            //--------------------------------
                            // sb.AppendLine(@"0.00 as _ipd,");
                            ////  sb.AppendLine(@"0.00 as _ipd_cacao,");
                            // sb.AppendLine(@"0.00 as _purd,");
                            //  sb.AppendLine(@"0.00 as _ipd_mpmc,");
                            //  sb.AppendLine(@"0.00 as _ippr,");

                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =13";
                            DataTable dtData_IPD = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_IPD.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_IPD.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_ipd"] = _total;
                            }

                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =31";
                            DataTable dtData_IPD_CACAO = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_IPD_CACAO.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_IPD_CACAO.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_ipd_cacao"] = _total;
                            }

                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =14";
                            DataTable dtData_PURD = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_PURD.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_PURD.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_purd"] = _total;
                            }

                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =27";
                            DataTable dtData_IPD_MPMC = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_IPD_MPMC.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_IPD_MPMC.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_ipd_mpmc"] = _total;
                            }
                            // sb.AppendLine(@"0.00 as _ipd,");
                            ////  sb.AppendLine(@"0.00 as _ipd_cacao,");
                            // sb.AppendLine(@"0.00 as _purd,");
                            //  sb.AppendLine(@"0.00 as _ipd_mpmc,");
                            //  sb.AppendLine(@"0.00 as _ippr,");

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {

                                double _total = 0.00;

                                _total += Convert.ToDouble(item["_ipd"].ToString()) + Convert.ToDouble(item["_ipd_cacao"].ToString()) + Convert.ToDouble(item["_purd"].ToString()) + Convert.ToDouble(item["_ipd_mpmc"].ToString());

                                item["_ippr"] = _total;


                            }
                            //--------------------------------
                            // sb.AppendLine(@"0.00 as _ird,");
                            // sb.AppendLine(@"0.00 as _ird_eaga,");
                            // sb.AppendLine(@"0.00 as _ird_mct,");
                           // sb.AppendLine(@"0.00 as _mebeaga");

                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =15";
                            DataTable dtData_IRD = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_IRD.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_IRD.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_ird"] = _total;
                            }

                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =28";
                            DataTable dtData_IRD_EAGA = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_IRD_EAGA.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_IRD_EAGA.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_ird_eaga"] = _total;
                            }
                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =29";
                            DataTable dtData_IRD_MCT = _dsFundD.Tables[0].DefaultView.ToTable();

                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_IRD_MCT.DefaultView.RowFilter = "mooe_id = '" + item["code"].ToString() + "'";
                                DataTable _items = dtData_IRD_MCT.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_ird_mct"] = _total;
                            }


                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {

                                double _total = 0.00;

                                _total += Convert.ToDouble(item["_ird"].ToString()) + Convert.ToDouble(item["_ird_eaga"].ToString()) + Convert.ToDouble(item["_ird_mct"].ToString());

                                item["_mebeaga"] = _total;


                            }
                    break;
                case "2018":
                            _dsFundD = new DataSet();
                            _dsFundD = ExecuteSQLQuery(sbD.ToString());

                            _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =1 OR division_id=2 OR division_id=3 OR division_id=4";
                             dtData_OC = _dsFundD.Tables[0].DefaultView.ToTable();
                             double GMSTotal = 0.00;
                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_OC.DefaultView.RowFilter = "mooe_id = '" + item["_code"].ToString() + "'";
                                DataTable _items = dtData_OC.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_gms"] = _total;
                            }

                            _dsFundD.Tables[0].DefaultView.RowFilter = "";
                             _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =101 OR division_id=102 ";
                             dtData_OC = _dsFundD.Tables[0].DefaultView.ToTable();
                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_OC.DefaultView.RowFilter = "mooe_id = '" + item["_code"].ToString() + "'";
                                DataTable _items = dtData_OC.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_performance_management"] = _total;
                            }

                             _dsFundD.Tables[0].DefaultView.RowFilter = "";
                             _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =101 OR division_id=102 ";
                             dtData_OC = _dsFundD.Tables[0].DefaultView.ToTable();
                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_OC.DefaultView.RowFilter = "mooe_id = '" + item["_code"].ToString() + "'";
                                DataTable _items = dtData_OC.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_performance_management"] = _total;
                            }

                             _dsFundD.Tables[0].DefaultView.RowFilter = "";
                             _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =6 OR division_id=103 OR division_id=14";
                             dtData_OC = _dsFundD.Tables[0].DefaultView.ToTable();
                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_OC.DefaultView.RowFilter = "mooe_id = '" + item["_code"].ToString() + "'";
                                DataTable _items = dtData_OC.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_tsp"] = _total;
                            }

                            _dsFundD.Tables[0].DefaultView.RowFilter = "";
                             _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =104";
                             dtData_OC = _dsFundD.Tables[0].DefaultView.ToTable();
                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_OC.DefaultView.RowFilter = "mooe_id = '" + item["_code"].ToString() + "'";
                                DataTable _items = dtData_OC.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_legal_services"] = _total;
                            }
                             _dsFundD.Tables[0].DefaultView.RowFilter = "";
                             _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =5 OR division_id=105 OR division_id=7";
                             dtData_OC = _dsFundD.Tables[0].DefaultView.ToTable();
                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_OC.DefaultView.RowFilter = "mooe_id = '" + item["_code"].ToString() + "'";
                                DataTable _items = dtData_OC.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_pppdo"] = _total;
                            }

                             _dsFundD.Tables[0].DefaultView.RowFilter = "";
                             _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =8 OR division_id=106";
                             dtData_OC = _dsFundD.Tables[0].DefaultView.ToTable();
                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_OC.DefaultView.RowFilter = "mooe_id = '" + item["_code"].ToString() + "'";
                                DataTable _items = dtData_OC.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_pdrg"] = _total;
                            }
                             _dsFundD.Tables[0].DefaultView.RowFilter = "";
                             _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =9 OR division_id=10 OR division_id=11 OR division_id=12";
                             dtData_OC = _dsFundD.Tables[0].DefaultView.ToTable();
                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_OC.DefaultView.RowFilter = "mooe_id = '" + item["_code"].ToString() + "'";
                                DataTable _items = dtData_OC.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_is"] = _total;
                            }

                            _dsFundD.Tables[0].DefaultView.RowFilter = "";
                             _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =13 OR division_id=107 OR division_id=108";
                             dtData_OC = _dsFundD.Tables[0].DefaultView.ToTable();
                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_OC.DefaultView.RowFilter = "mooe_id = '" + item["_code"].ToString() + "'";
                                DataTable _items = dtData_OC.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_ip"] = _total;
                            }
                             _dsFundD.Tables[0].DefaultView.RowFilter = "";
                             _dsFundD.Tables[0].DefaultView.RowFilter = "division_id =15";
                             dtData_OC = _dsFundD.Tables[0].DefaultView.ToTable();
                            foreach (DataRow item in _dsData.Tables[0].Rows)
                            {
                                dtData_OC.DefaultView.RowFilter = "mooe_id = '" + item["_code"].ToString() + "'";
                                DataTable _items = dtData_OC.DefaultView.ToTable();
                                double _total = 0.00;
                                foreach (DataRow itemd in _items.Rows)
                                {
                                    _total += Convert.ToDouble(itemd["Amount"].ToString());
                                }
                                item["_bimp_eaga"] = _total;
                            }
                            break;
                default:
                    break;
            }

            foreach (DataRow item in _dsData.Tables[0].Rows)
            {
                double total = 0.00;

                for (int i = 2; i < _dsData.Tables[0].Columns.Count -1; i++)
                {
                    total += Convert.ToDouble(item[i].ToString());
                }
                item["_total"] = total.ToString();
               
            }



            return _dsData;
        }

       
        public DataSet FetchDataPPMP(string PAP, string _year,String _div)
        {
            DataSet _dsData = new DataSet();
            DataTable dtHeader = new DataTable();

            var sb = new System.Text.StringBuilder(300);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  ISNULL(Uacs,'') as Uacs ,");
            sb.AppendLine(@"  ISNULL(Description,'') as Description,");
            sb.AppendLine(@"  ISNULL(Quantity,'') as Quantity,");
            sb.AppendLine(@"  ISNULL(EstimateBudget,'0') as EstimatedBudget,");
            sb.AppendLine(@"  ISNULL(ModeOfProcurement,'') as ModeOfProcurement,");
            sb.AppendLine(@"  ISNULL(Jan,'') as Jan,");
            sb.AppendLine(@"  ISNULL(Feb,'') as Feb,");
            sb.AppendLine(@"  ISNULL(Mar,'') as Mar,");
            sb.AppendLine(@"  ISNULL(Apr,'') as Apr,");
            sb.AppendLine(@"  ISNULL(May,'') as May,");
            sb.AppendLine(@"  ISNULL(Jun,'') as Jun,");
            sb.AppendLine(@"  ISNULL(Jul,'') as Jul,");
            sb.AppendLine(@"  ISNULL(Aug,'') as Aug,");
            sb.AppendLine(@"  ISNULL(Sep,'') as Sep,");
            sb.AppendLine(@"  ISNULL(Octs,'') as Octs,");
            sb.AppendLine(@"  ISNULL(Nov,'') as Nov,");
            sb.AppendLine(@"  ISNULL(Dec,'') as Dec,");
            sb.AppendLine(@"  ISNULL(Total,'') as Total,");
            sb.AppendLine(@"  ISNULL(Division,'') as Division,");
            sb.AppendLine(@"  ISNULL(Yearssss,'') as Yearssss,");
            sb.AppendLine(@"  ISNULL(PAP,'') as PAP,");
            sb.AppendLine(@"  ISNULL(approved,'') as approved,");
            sb.AppendLine(@"  ISNULL(header,'') as header,");
            sb.AppendLine(@"  ISNULL(revision,'') as revision,");
            sb.AppendLine(@"  ISNULL(overall,'') as revision");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_report_data_ppmp WHERE PAP = '" + PAP + "' AND Yearssss =" + _year + " AND Division ='"+ _div +"';");

            _dsData = new DataSet();
            _dsData = ExecuteSQLQuery(sb.ToString());

            dtHeader.TableName = "Header";


            return _dsData;
        }
        public DataSet FetchDataMAO()
        {
            DataSet _dsData = new DataSet();
            DataTable dtHeader = new DataTable();

            var sb = new System.Text.StringBuilder(250);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  ReportHeader,");
            sb.AppendLine(@"  UACS,");
            sb.AppendLine(@"  Allocation,");
            sb.AppendLine(@"  Jan,");
            sb.AppendLine(@"  Feb,");
            sb.AppendLine(@"  Mar,");
            sb.AppendLine(@"  Apr,");
            sb.AppendLine(@"  May,");
            sb.AppendLine(@"  Jun,");
            sb.AppendLine(@"  Jul,");
            sb.AppendLine(@"  Aug,");
            sb.AppendLine(@"  Sep,");
            sb.AppendLine(@"  Octs,");
            sb.AppendLine(@"  Nov,");
            sb.AppendLine(@"  Dec,");
            sb.AppendLine(@"  Total,");
            sb.AppendLine(@"  Balance,");
            sb.AppendLine(@"  Divisions,");
            sb.AppendLine(@"  FundSource,");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  PAP");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_report_data_mao_urs ORDER By id ASC;");

            _dsData = new DataSet();
            _dsData = svc_mindaf.ExecuteSQLReportQuery(sb.ToString());

            dtHeader.TableName = "Header";


            return _dsData;
        }

        public DataSet FetchDataAPP(string _year)
        {
            DataSet _dsData = new DataSet();
            DataTable dtHeader = new DataTable();

            var sb = new System.Text.StringBuilder(362);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@" app.code,");
            sb.AppendLine(@" app.procurement_project,");
            sb.AppendLine(@" app.pmo_end_user,");
            sb.AppendLine(@" app.mode_of_procurement,");
            sb.AppendLine(@" app.ads_post_rei,");
            sb.AppendLine(@" app.sub_open_bids,");
            sb.AppendLine(@" app.notice_award,");
            sb.AppendLine(@" app.contract_signing,");
            sb.AppendLine(@" app.source_fund,");
            sb.AppendLine(@" app.eb_total,");
            sb.AppendLine(@" app.eb_mooe,");
            sb.AppendLine(@" app.eb_co,");
            sb.AppendLine(@" app.remarks,");
            sb.AppendLine(@" app.pap_code,");
            sb.AppendLine(@" app.yearsss");
            sb.AppendLine(@"FROM mnda_report_data_app app");
            sb.AppendLine(@"WHERE app.yearsss ='"+ _year +"';");


            _dsData = new DataSet();
            _dsData = svc_mindaf.ExecuteSQLReportQuery(sb.ToString());

            dtHeader.TableName = "Header";


            return _dsData;
        }
        clsData c_data = new clsData();
        public Boolean LoginUser(String Password) 
        {
            DataTable dtData = new DataTable();
            var sb = new System.Text.StringBuilder(228);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  CONCAT(dso.PAP ,' - ',dso.Description) as pap,");
            sb.AppendLine(@"  div.Division_Id,");
            sb.AppendLine(@"  div.Division_Code,");
            sb.AppendLine(@"  div.Division_Desc,");
            sb.AppendLine(@"  ua.Username,");
            sb.AppendLine(@"  ua.is_admin as UserLevel");
            sb.AppendLine(@"FROM mnda_user_accounts ua");
            sb.AppendLine(@"INNER JOIN Division div on div.Division_Id = ua.Division_Id");
            sb.AppendLine(@"INNER JOIN DBM_Sub_Office dso on dso.PAP = div.Division_Code");
            sb.AppendLine(@" WHERE Password='"+ Password +"'");


            dtData = c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Copy();
            if (dtData.Rows.Count!=0)
            {
                foreach (DataRow item in dtData.Rows)
                {
                    UserInfo = new UserDetail();

                    UserInfo.DivisionCode = item["pap"].ToString();
                    UserInfo.DivisionId = item["Division_Id"].ToString();
                    UserInfo.DivisionName = item["Division_Desc"].ToString();
                    UserInfo.UserLevel = item["UserLevel"].ToString();
                    UserInfo.Username = item["Username"].ToString();
                    break;
                }
                return true;
            }else
	        {
                return false;
	        }
        }

        public DataTable FetchDivisions() 
        {
            DataTable dtData = new DataTable();
            //var sb = new System.Text.StringBuilder(29);
            //sb.AppendLine(@"SELECT ");
            //sb.AppendLine(@"*");
            //sb.AppendLine(@"FROM Division div");

            //dtData = svc_mindaf.(sb.ToString()).Tables[0].Copy();
            //DivisionData.Clear();

            //foreach (DataRow item in dtData.Rows)
            //{
            //    DivisionList _vData = new DivisionList();
            //    _vData.DivisionId = item["Division_Id"].ToString();
            //    _vData.DivisionCode = item["Division_Code"].ToString();
            //    _vData.DivisionName = item["Division_Desc"].ToString();
                
            //    DivisionData.Add(_vData);
            //}
            return dtData;
        }

        private List<lib_ActivityTotals> FetchTotals(String FundCode,String WorkingYear) 
        {
            List<lib_ActivityTotals> _data = new List<lib_ActivityTotals>();

            StringBuilder varname1 = new StringBuilder();
            varname1.Append("SELECT \n");
            varname1.Append("mad.month, \n");
            varname1.Append("mad.total \n");
            varname1.Append("FROM mnda_activity_data mad \n");
            varname1.Append("WHERE mad.fund_source_id = '"+ FundCode +"' \n");
            varname1.Append("and mad.year = " + WorkingYear + " and mad.status ='FINANCE APPROVED' AND mad.is_approved = 1 AND mad.is_suspended = 0");

            foreach (DataRow item in ExecuteSQLQuery(varname1.ToString()).Tables[0].Rows)
            {
                lib_ActivityTotals _var = new lib_ActivityTotals();


                _var.Months = item["month"].ToString();
                _var.Total = Convert.ToDouble(item["total"].ToString());


                _data.Add(_var);
            }

            return _data;
        }
        private double FetchBudgetAllocation(String div_id,String FundCode,String _Year)
        {
            List<lib_ActivityTotals> _data = new List<lib_ActivityTotals>();
            Double _Total = 0;
            StringBuilder varname1 = new StringBuilder();
            varname1.Append("SELECT \n");
            varname1.Append("SUM(mfsl.Amount) TotalBudget \n");
            varname1.Append("FROM mnda_fund_source_line_item mfsl \n");
            varname1.Append("WHERE mfsl.Year = "+ _Year +" \n");
            varname1.Append("AND mfsl.Fund_Source_Id ='" + FundCode + "' AND mfsl.division_id ='"+ div_id +"';");

            foreach (DataRow item in ExecuteSQLQuery(varname1.ToString()).Tables[0].Rows)
            {
         
                _Total = Convert.ToDouble(item["TotalBudget"].ToString());


                break;
            }

            return _Total;
        }
       
    }


    /// <summary>
    /// BEDS
    /// </summary>
    /// 
    //BED3

    public class lib_ActivityTotals 
    {
        public String Months { get; set; }
        public Double Total { get; set; }
    }
    public class lib_MainBudgetAllowance
    {
        public String Id { get; set; }
        public String Pap { get; set; }
        public String FundSource { get; set; }
        public String Description { get; set; }
        public Boolean IsDiv { get; set; }
        public Boolean IsUnit { get; set; }
        public Boolean IsSubPAP { get; set; }


        public string Jan { get; set; }
        public string Feb { get; set; }
        public string Mar { get; set; }
        public string Q1 { get; set; }
        public string Apr { get; set; }
        public string May { get; set; }
        public string Jun { get; set; }
        public string Q2 { get; set; }
        public string Jul { get; set; }
        public string Aug { get; set; }
        public string Sep { get; set; }
        public string Q3 { get; set; }
        public string Oct { get; set; }
        public string Nov { get; set; }
        public string Dec { get; set; }
        public string Q4{ get; set; }
        public string SubTotal { get; set; }
        public string TotalTax { get; set; }
        public string Total { get; set; }

    }
    public class lib_MainBudget
    {
        public String Id { get; set; }
        public String Pap { get; set; }
        public String Description { get; set; }
        public Boolean IsDiv { get; set; }
        public Boolean IsUnit { get; set; }
        public Boolean IsSubPAP { get; set; }
    }
    public class lib_PAP
    {
        public String DBM_Sub_Pap_id { get; set; }
        public String DBM_Pap_Id { get; set; }
        public String DBM_Sub_Pap_Code { get; set; }
        public String DBM_Sub_Pap_Desc { get; set; }
    }
    public class lib_SUB_PAP
    {
        public String Sub_Id { get; set; }
        public String DBM_Sub_Id { get; set; }
        public String Description { get; set; }
        public String PAP { get; set; }
    }

    public class DivisionBudgetAllocationFields
    {
        public String Division_Id { get; set; }
        public String DBM_Sub_Pap_Id { get; set; }
        public String Division_Code { get; set; }
        public String Division_Desc { get; set; }
        public String UnitCode { get; set; }
    }
    /// <summary>
    /// Others
    /// </summary>
    
    public class FormatTitleFormat
    {
        public String isMain { get; set; }
        public String Code { get; set; }
        public String Header { get; set; }

    }
    public class DivisionList 
    {
        public String DivisionId { get; set; }
        public String DivisionCode { get; set; }
        public String DivisionName { get; set; }
        
    }

    public class UserDetail 
    {
        public String DivisionId { get; set; }
        public String DivisionCode { get; set; }
        public String DivisionName { get; set; }
        public String Username { get; set; }
        public String UserLevel { get; set; }
    }
}
