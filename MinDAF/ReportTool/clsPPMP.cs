﻿using ReportTool.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Procurement_Module.Class
{
    class clsPPMP
    {

        clsData c_data = new clsData();

        public DataSet GetExpenseItems() 
        {
            var sb = new System.Text.StringBuilder(141);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mooe.id,");
            sb.AppendLine(@"mooe.mooe_id,");
            sb.AppendLine(@"mooe.uacs_code,");
            sb.AppendLine(@"mooe.name");
            sb.AppendLine(@"FROM mnda_mooe_sub_expenditures mooe");
            sb.AppendLine(@"WHERE mooe.is_active = 1");
            sb.AppendLine(@"ORDER BY mooe.id");
            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }
     
        public DataSet GetMOOE()
        {
            var sb = new System.Text.StringBuilder(69);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  code,");
            sb.AppendLine(@"  name");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_mooe_expenditures;");

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;

        }
        public DataSet GetDivisionList(String _UACS,String DivId)
        {
            var sb = new System.Text.StringBuilder(170);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"div.DivisionAccro as _accro");
            sb.AppendLine(@"FROM mnda_fund_source_line_item mfsl");
            sb.AppendLine(@"INNER JOIN Division div on div.Division_Id = mfsl.division_id");
            sb.AppendLine(@"WHERE mfsl.mooe_id  ='"+_UACS +"' and div.DivisionId ="+ DivId +"");


            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;

        }
        public DataSet GetProcurementList()
        {
            var sb = new System.Text.StringBuilder(182);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  item_specifications,");
            sb.AppendLine(@"  general_category,");
            sb.AppendLine(@"  sub_category,");
            sb.AppendLine(@"  minda_inventory,");
            sb.AppendLine(@"  unit_of_measure,");
            sb.AppendLine(@"  price,");
            sb.AppendLine(@"  expenditure_id");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_procurement_items;");


            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;

        }
        public DataSet GetLocalAllowance()
        {
            var sb = new System.Text.StringBuilder(97);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mel.id,");
            sb.AppendLine(@"mel.item_name,");
            sb.AppendLine(@"mel.rate");
            sb.AppendLine(@"FROM mnda_expenditures_library mel");
            sb.AppendLine(@"WHERE mel.sub_expenditure_code = '108'");

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;

        }
        public DataSet GetForeignAllowance()
        {
            var sb = new System.Text.StringBuilder(97);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mel.id,");
            sb.AppendLine(@"mel.item_name,");
            sb.AppendLine(@"mel.rate");
            sb.AppendLine(@"FROM mnda_expenditures_library mel");
            sb.AppendLine(@"WHERE mel.sub_expenditure_code = '109'");

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;

        }
        public DataSet GetForeignPlaneRate()
        {
            var sb = new System.Text.StringBuilder(209);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mel.item_name as _search_code,");
            sb.AppendLine(@"CONCAT(mel.library_type,' - ',mel.item_name) as ticket");
            sb.AppendLine(@"FROM mnda_expenditures_library mel");
            sb.AppendLine(@"WHERE mel.sub_expenditure_code = '2' ");

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;

        }
        public DataSet GetLocalPlaneRate()
        {
            var sb = new System.Text.StringBuilder(209);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mel.item_name as _search_code,");
            sb.AppendLine(@"CONCAT(mel.library_type,' - ',mel.item_name) as ticket");
            sb.AppendLine(@"FROM mnda_expenditures_library mel");
            sb.AppendLine(@"WHERE mel.sub_expenditure_code = '1' ");

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;

        }
        public DataSet GetExpenseQuantity(String _ID)
        {
            var sb = new System.Text.StringBuilder(319);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mad.procurement_id,");
            sb.AppendLine(@"mad.month,");
            sb.AppendLine(@"mad.quantity,");
            sb.AppendLine(@"mad.destination,");
            sb.AppendLine(@"mad.travel_allowance,");
            sb.AppendLine(@"mad.rate,");
            sb.AppendLine(@"mad.total");
            sb.AppendLine(@"FROM mnda_activity_data mad ");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.id = mad.activity_id");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.id = ma.output_id");
            sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
            sb.AppendLine(@"WHERE mad.status = 'FINANCE APPROVED' AND mad.mooe_sub_expenditure_id = '"+ _ID +"'");


            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

           
            return dsData;

        }

        public DataSet GetDivisionProjects(string _div_id, string _year, Boolean isrevised, String _fundsource)
        {
            var sb = new System.Text.StringBuilder(319);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@" DISTINCT ISNULL(mpe.project_name,'') as project_name");
            //sb.AppendLine(@" ,div.Division_Code");
            sb.AppendLine(@" FROM mnda_main_program_encoded mnd");
            sb.AppendLine(@" LEFT JOIN mnda_programs mp on mp.id = mnd.program_code");
            sb.AppendLine(@" LEFT JOIN mnda_program_encoded mpe on mpe.main_program_id = mnd.id");
            sb.AppendLine(@" LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@" LEFT JOiN mnda_project_output  mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@" LEFT JOIN mnda_activity ma on ma.output_id = mpo.id");
            sb.AppendLine(@" LEFT JOIN mnda_activity_data mad on mad.activity_id = ma.id");
            sb.AppendLine(@" LEFT JOIN mnda_user_accounts mua on mua.User_Id = ma.accountable_member_id");
            sb.AppendLine(@" LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@" LEFT JOIN mnda_fund_source mfs on mfs.code = mad.fund_source_id");
            sb.AppendLine(@" WHERE mnd.accountable_division ="+_div_id+" AND mnd.fiscal_year ="+_year+"");
            sb.AppendLine(@" and mfs.service_type = 1");
            sb.AppendLine(@" and mad.total !=0");
            sb.AppendLine(@" and mad.status ='FINANCE APPROVED'");
            sb.AppendLine(@" AND mad.fund_source_id ='"+_fundsource+"'");
            sb.AppendLine(@" AND mad.is_approved = 1");


            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());


            return dsData;

        }

        public DataSet FetchDivisionActivities(string _div_id, string _year, string _PAPCode)
        {
            var sb = new System.Text.StringBuilder(319);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@" ISNULL(mnd.id,'') as program_id,ISNULL(mpe.id,'') as project_id, ISNULL(mp.id,'') as program_index,ISNULL(mpo.id,'') as output_id,");
            sb.AppendLine(@" ISNULL(mnd.fiscal_year,'') as fiscal_year,");
            sb.AppendLine(@" ISNULL(mp.name,'') as program_name,");
            sb.AppendLine(@" ISNULL(mpo.name,'') as output,");
            sb.AppendLine(@" ISNULL(mpe.project_name,'') as project_name, div.Division_Code,");
            sb.AppendLine(@" ISNULL(ma.description,'') as activity,");
            sb.AppendLine(@" ISNULL(ma.id,'') as activity_id,");
            sb.AppendLine(@" ISNULL(ma.mode_of_procurement,'') as mode_of_procurement,");
            sb.AppendLine(@" ISNULL(mua.User_Fullname,'') as User_Fullname");
            sb.AppendLine(@" FROM mnda_main_program_encoded mnd");
            sb.AppendLine(@" LEFT JOIN mnda_programs mp on mp.id = mnd.program_code");
            sb.AppendLine(@" LEFT JOIN mnda_program_encoded mpe on mpe.main_program_id = mnd.id");
            sb.AppendLine(@" LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@" LEFT JOiN mnda_project_output  mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@" LEFT JOIN mnda_activity ma on ma.output_id = mpo.id");
            sb.AppendLine(@" LEFT JOIN mnda_activity_data mad on mad.activity_id = ma.id");
            sb.AppendLine(@" LEFT JOIN mnda_user_accounts mua on mua.User_Id = ma.accountable_member_id");
            sb.AppendLine(@" WHERE mnd.accountable_division ="+_div_id+" AND mnd.fiscal_year ="+_year+"");
            sb.AppendLine(@" AND div.Division_Code ='" + _PAPCode + "'");
            sb.AppendLine(@" GROUP BY mnd.id,mpe.id,mp.id,mpo.id, mnd.fiscal_year,mp.name,mpo.name,mpe.project_name,div.Division_Code,ma.description,ma.mode_of_procurement,ma.id,mua.User_Fullname, mad.fund_source_id");
            sb.AppendLine(@" ORDER BY  ma.id,mnd.fiscal_year, activity ASC");

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;

        }

        public DataSet FetchActivityTotals(String DivId, String ActID, String WorkingYear, String FundSourceId, String DivisionCode)
        {
            var sb = new System.Text.StringBuilder(319);
            sb.AppendLine(@" SELECT");
            sb.AppendLine(@" ISNULL(mnd.id,'') as program_id,ISNULL(mpe.id,'') as project_id, ISNULL(mp.id,'') as program_index,ISNULL(mpo.id,'') as output_id,");
            sb.AppendLine(@" ISNULL(mnd.fiscal_year,'') as fiscal_year,");
            sb.AppendLine(@" ISNULL(mp.name,'') as program_name,");
            sb.AppendLine(@" ISNULL(mpo.name,'') as output,");
            sb.AppendLine(@" ISNULL(mpe.project_name,'') as project_name,ISNULL(mua.User_Fullname,'') as User_Fullname, div.Division_Code,");
            sb.AppendLine(@" ISNULL(ma.description,'') as activity,");
            sb.AppendLine(@" ISNULL(ma.id,'') as activity_id,");
            sb.AppendLine(@" ISNULL((SELECT sum(mad.total) from mnda_activity_data mad JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id WHERE mad.month = 'Jan' AND mad.activity_id = '" + ActID + "' and mad.is_suspended = 0 and mad.fund_source_id='" + FundSourceId + "' AND mooe.is_for_ppmp = 1),0) as Jan,");
            sb.AppendLine(@" ISNULL((SELECT sum(mad.total) from mnda_activity_data mad JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id WHERE mad.month = 'Feb' AND mad.activity_id = '" + ActID + "' and mad.is_suspended = 0 and mad.fund_source_id='" + FundSourceId + "' AND mooe.is_for_ppmp = 1),0) as Feb,");
            sb.AppendLine(@" ISNULL((SELECT sum(mad.total) from mnda_activity_data mad JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id WHERE mad.month = 'Mar' AND mad.activity_id = '" + ActID + "' and mad.is_suspended = 0 and mad.fund_source_id='" + FundSourceId + "' AND mooe.is_for_ppmp = 1),0) as Mar,");
            sb.AppendLine(@" ISNULL((SELECT sum(mad.total) from mnda_activity_data mad JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id WHERE mad.month = 'Apr' AND mad.activity_id = '" + ActID + "' and mad.is_suspended = 0 and mad.fund_source_id='" + FundSourceId + "' AND mooe.is_for_ppmp = 1),0) as Apr,");
            sb.AppendLine(@" ISNULL((SELECT sum(mad.total) from mnda_activity_data mad JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id WHERE mad.month = 'May' AND mad.activity_id = '" + ActID + "' and mad.is_suspended = 0 and mad.fund_source_id='" + FundSourceId + "' AND mooe.is_for_ppmp = 1),0) as May,");
            sb.AppendLine(@" ISNULL((SELECT sum(mad.total) from mnda_activity_data mad JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id WHERE mad.month = 'Jun' AND mad.activity_id = '" + ActID + "' and mad.is_suspended = 0 and mad.fund_source_id='" + FundSourceId + "' AND mooe.is_for_ppmp = 1),0) as Jun,");
            sb.AppendLine(@" ISNULL((SELECT sum(mad.total) from mnda_activity_data mad JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id WHERE mad.month = 'Jul' AND mad.activity_id = '" + ActID + "' and mad.is_suspended = 0 and mad.fund_source_id='" + FundSourceId + "' AND mooe.is_for_ppmp = 1),0) as Jul,");
            sb.AppendLine(@" ISNULL((SELECT sum(mad.total) from mnda_activity_data mad JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id WHERE mad.month = 'Aug' AND mad.activity_id = '" + ActID + "' and mad.is_suspended = 0 and mad.fund_source_id='" + FundSourceId + "' AND mooe.is_for_ppmp = 1),0) as Aug,");
            sb.AppendLine(@" ISNULL((SELECT sum(mad.total) from mnda_activity_data mad JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id WHERE mad.month = 'Sep' AND mad.activity_id = '" + ActID + "' and mad.is_suspended = 0 and mad.fund_source_id='" + FundSourceId + "' AND mooe.is_for_ppmp = 1),0) as Sep,");
            sb.AppendLine(@" ISNULL((SELECT sum(mad.total) from mnda_activity_data mad JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id WHERE mad.month = 'Oct' AND mad.activity_id = '" + ActID + "' and mad.is_suspended = 0 and mad.fund_source_id='" + FundSourceId + "' AND mooe.is_for_ppmp = 1),0) as Oct,");
            sb.AppendLine(@" ISNULL((SELECT sum(mad.total) from mnda_activity_data mad JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id WHERE mad.month = 'Nov' AND mad.activity_id = '" + ActID + "' and mad.is_suspended = 0 and mad.fund_source_id='" + FundSourceId + "' AND mooe.is_for_ppmp = 1),0) as Nov,");
            sb.AppendLine(@" ISNULL((SELECT sum(mad.total) from mnda_activity_data mad JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id WHERE mad.month = 'Dec' AND mad.activity_id = '" + ActID + "' and mad.is_suspended = 0 and mad.fund_source_id='" + FundSourceId + "' AND mooe.is_for_ppmp = 1),0) as Dec");
            sb.AppendLine(@" FROM mnda_main_program_encoded mnd");
            sb.AppendLine(@" LEFT JOIN mnda_programs mp on mp.id = mnd.program_code");
            sb.AppendLine(@" LEFT JOIN mnda_program_encoded mpe on mpe.main_program_id = mnd.id");
            sb.AppendLine(@" LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@" LEFT JOiN mnda_project_output  mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@" LEFT JOIN mnda_activity ma on ma.output_id = mpo.id");
            sb.AppendLine(@" LEFT JOIN mnda_activity_data mad on mad.activity_id = ma.id");
            sb.AppendLine(@" LEFT JOIN mnda_user_accounts mua on mua.User_Id = ma.accountable_member_id");
            sb.AppendLine(@" WHERE mnd.accountable_division = "+DivId+" AND mnd.fiscal_year = "+WorkingYear+"  and div.Division_Code = '"+DivisionCode+"' and activity_id = '" + ActID + "'");
            sb.AppendLine(@" GROUP BY mnd.id,mpe.id,mp.id,mpo.id, mnd.fiscal_year,mp.name,mpo.name,mpe.project_name,ma.description,ma.id,mua.User_Fullname, div.Division_Code, mad.fund_source_id");
            sb.AppendLine(@" ORDER BY  ma.id ASC");

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;

        }

        public DataSet GetDivisionListDetails(String div_id,String WorkingYear)
        {

            var sb = new System.Text.StringBuilder(85);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"div.Division_Id as div_id,");
            sb.AppendLine(@"div.Division_Desc as div_name");
            sb.AppendLine(@"FROM Division div WHERE div.Division_Id =" + div_id + "");
            sb.AppendLine(@"AND  div.year_setup = '"+ WorkingYear +"'");


            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }

        public DataSet FetchDataPPMP(string PAP, string _year)
        {
            DataSet _dsData = new DataSet();
            DataTable dtHeader = new DataTable();

            var sb = new System.Text.StringBuilder(300);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  ISNULL(Uacs,'') as Uacs ,");
            sb.AppendLine(@"  ISNULL(Description,'') as Description,");
            sb.AppendLine(@"  ISNULL(Quantity,'') as Quantity,");
            sb.AppendLine(@"  ISNULL(EstimateBudget,'0') as EstimatedBudget,");
            sb.AppendLine(@"  ISNULL(ModeOfProcurement,'') as ModeOfProcurement,");
            sb.AppendLine(@"  ISNULL(Jan,'') as Jan,");
            sb.AppendLine(@"  ISNULL(Feb,'') as Feb,");
            sb.AppendLine(@"  ISNULL(Mar,'') as Mar,");
            sb.AppendLine(@"  ISNULL(Apr,'') as Apr,");
            sb.AppendLine(@"  ISNULL(May,'') as May,");
            sb.AppendLine(@"  ISNULL(Jun,'') as Jun,");
            sb.AppendLine(@"  ISNULL(Jul,'') as Jul,");
            sb.AppendLine(@"  ISNULL(Aug,'') as Aug,");
            sb.AppendLine(@"  ISNULL(Sep,'') as Sep,");
            sb.AppendLine(@"  ISNULL(Octs,'') as Octs,");
            sb.AppendLine(@"  ISNULL(Nov,'') as Nov,");
            sb.AppendLine(@"  ISNULL(Dec,'') as Dec,");
            sb.AppendLine(@"  ISNULL(Total,'') as Total,");
            sb.AppendLine(@"  ISNULL(Division,'') as Division,");
            sb.AppendLine(@"  ISNULL(Yearssss,'') as Yearssss,");
            sb.AppendLine(@"  ISNULL(PAP,'') as PAP,");
            sb.AppendLine(@"  ISNULL(approved,'') as approved,");
            sb.AppendLine(@"  ISNULL(header,'') as header,");
            sb.AppendLine(@"  ISNULL(revision,'') as revision,");
            sb.AppendLine(@"  ISNULL(overall,'') as revision");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_report_data_ppmp WHERE PAP = '" + PAP + "' AND Yearssss =" + _year + " AND Division ='Thematic Map (KMD)';");

            _dsData = new DataSet();
            _dsData = c_data.ExecuteSQLQuery(sb.ToString()); 

            dtHeader.TableName = "Header";


            return _dsData;
        }

        public DataSet FetchGeneralCategory()
        {
            var sb = new System.Text.StringBuilder(149);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"DISTINCT general_category");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_procurement_items_new");
            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }

        public DataSet FetchSubCategory()
        {
            var sb = new System.Text.StringBuilder(149);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"DISTINCT general_category, sub_category");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_procurement_items_new");
            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }

        public DataSet FetchDistinctSupplies(String year, String fundsource)
        {
            var sb = new System.Text.StringBuilder(149);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@" DISTINCT mad.type_service,");
            sb.AppendLine(@" mooe.name,");
            sb.AppendLine(@" mooe.uacs_code,");
            sb.AppendLine(@" mpin.general_category, mpin.sub_category, mpin.unit_of_measure, mpin.price");
            sb.AppendLine(@" FROM mnda_activity_data mad");
            sb.AppendLine(@" LEFT JOIN mnda_procurement_items_new mpin on mpin.item_specifications = mad.type_service");
            sb.AppendLine(@" LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@" LEFT JOIN mnda_fund_source mfs on mfs.code = mad.fund_source_id");
            sb.AppendLine(@" WHERE mad.year = "+year+" and mfs.service_type = 1 and mad.total !=0 and mad.status ='FINANCE APPROVED'");
            sb.AppendLine(@" AND mad.fund_source_id ='"+fundsource+"'  AND mad.is_approved = 1");
            sb.AppendLine(@" GROUP BY");
            sb.AppendLine(@" mfs.Fund_Name,");
            sb.AppendLine(@" mooe.uacs_code,");
            sb.AppendLine(@" mpin.general_category,");
            sb.AppendLine(@" mpin.sub_category,mpin.unit_of_measure, mpin.price,");
            sb.AppendLine(@" mooe.name,");
            sb.AppendLine(@" mad.total,");
            sb.AppendLine(@" mad.quantity,");
            sb.AppendLine(@" mad.type_service,");
            sb.AppendLine(@" mad.id,");
            sb.AppendLine(@" mad.month,");
            sb.AppendLine(@" mad.year ,mad.entry_date,");
            sb.AppendLine(@" mad.start,");
            sb.AppendLine(@" mad.[end],mad.status");
            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }

        public DataSet FetchSuppliesCount(String year, String fundsource, String type_service)
        {
            var sb = new System.Text.StringBuilder(149);
            sb.AppendLine(@"SELECT DISTINCT mad.type_service,");
            sb.AppendLine(@" ISNULL((SELECT sum(CAST(mad.quantity as INT)) FROM mnda_activity_data mad WHERE mad.year = " + year + " and mad.total !=0 and mad.status ='FINANCE APPROVED' AND mad.fund_source_id ='" + fundsource + "'  AND mad.is_approved = 1 and mad.type_service = '" + type_service + "' and mad.month='Jan' GROUP BY mad.type_service, mad.quantity),0) AS Jan,");
            sb.AppendLine(@" ISNULL((SELECT sum(CAST(mad.quantity as INT)) FROM mnda_activity_data mad WHERE mad.year = " + year + " and mad.total !=0 and mad.status ='FINANCE APPROVED' AND mad.fund_source_id ='" + fundsource + "'  AND mad.is_approved = 1 and mad.type_service = '" + type_service + "' and mad.month='Feb' GROUP BY mad.type_service, mad.quantity),0) AS Feb,");
            sb.AppendLine(@" ISNULL((SELECT sum(CAST(mad.quantity as INT)) FROM mnda_activity_data mad WHERE mad.year = " + year + " and mad.total !=0 and mad.status ='FINANCE APPROVED' AND mad.fund_source_id ='" + fundsource + "'  AND mad.is_approved = 1 and mad.type_service = '" + type_service + "' and mad.month='Mar' GROUP BY mad.type_service, mad.quantity),0) AS Mar,");
            sb.AppendLine(@" ISNULL((SELECT sum(CAST(mad.quantity as INT)) FROM mnda_activity_data mad WHERE mad.year = " + year + " and mad.total !=0 and mad.status ='FINANCE APPROVED' AND mad.fund_source_id ='" + fundsource + "'  AND mad.is_approved = 1 and mad.type_service = '" + type_service + "' and mad.month='Apr' GROUP BY mad.type_service, mad.quantity),0) AS Apr,");
            sb.AppendLine(@" ISNULL((SELECT sum(CAST(mad.quantity as INT)) FROM mnda_activity_data mad WHERE mad.year = " + year + " and mad.total !=0 and mad.status ='FINANCE APPROVED' AND mad.fund_source_id ='" + fundsource + "'  AND mad.is_approved = 1 and mad.type_service = '" + type_service + "' and mad.month='May' GROUP BY mad.type_service, mad.quantity),0) AS May,");
            sb.AppendLine(@" ISNULL((SELECT sum(CAST(mad.quantity as INT)) FROM mnda_activity_data mad WHERE mad.year = " + year + " and mad.total !=0 and mad.status ='FINANCE APPROVED' AND mad.fund_source_id ='" + fundsource + "'  AND mad.is_approved = 1 and mad.type_service = '" + type_service + "' and mad.month='Jun' GROUP BY mad.type_service, mad.quantity),0) AS Jun,");
            sb.AppendLine(@" ISNULL((SELECT sum(CAST(mad.quantity as INT)) FROM mnda_activity_data mad WHERE mad.year = " + year + " and mad.total !=0 and mad.status ='FINANCE APPROVED' AND mad.fund_source_id ='" + fundsource + "'  AND mad.is_approved = 1 and mad.type_service = '" + type_service + "' and mad.month='Jul' GROUP BY mad.type_service, mad.quantity),0) AS Jul,");
            sb.AppendLine(@" ISNULL((SELECT sum(CAST(mad.quantity as INT)) FROM mnda_activity_data mad WHERE mad.year = " + year + " and mad.total !=0 and mad.status ='FINANCE APPROVED' AND mad.fund_source_id ='" + fundsource + "'  AND mad.is_approved = 1 and mad.type_service = '" + type_service + "' and mad.month='Aug' GROUP BY mad.type_service, mad.quantity),0) AS Aug,");
            sb.AppendLine(@" ISNULL((SELECT sum(CAST(mad.quantity as INT)) FROM mnda_activity_data mad WHERE mad.year = " + year + " and mad.total !=0 and mad.status ='FINANCE APPROVED' AND mad.fund_source_id ='" + fundsource + "'  AND mad.is_approved = 1 and mad.type_service = '" + type_service + "' and mad.month='Sep' GROUP BY mad.type_service, mad.quantity),0) AS Sep,");
            sb.AppendLine(@" ISNULL((SELECT sum(CAST(mad.quantity as INT)) FROM mnda_activity_data mad WHERE mad.year = " + year + " and mad.total !=0 and mad.status ='FINANCE APPROVED' AND mad.fund_source_id ='" + fundsource + "'  AND mad.is_approved = 1 and mad.type_service = '" + type_service + "' and mad.month='Oct' GROUP BY mad.type_service, mad.quantity),0) AS Oct,");
            sb.AppendLine(@" ISNULL((SELECT sum(CAST(mad.quantity as INT)) FROM mnda_activity_data mad WHERE mad.year = " + year + " and mad.total !=0 and mad.status ='FINANCE APPROVED' AND mad.fund_source_id ='" + fundsource + "'  AND mad.is_approved = 1 and mad.type_service = '" + type_service + "' and mad.month='Nov' GROUP BY mad.type_service, mad.quantity),0) AS Nov,");
            sb.AppendLine(@" ISNULL((SELECT sum(CAST(mad.quantity as INT)) FROM mnda_activity_data mad WHERE mad.year = " + year + " and mad.total !=0 and mad.status ='FINANCE APPROVED' AND mad.fund_source_id ='" + fundsource + "'  AND mad.is_approved = 1 and mad.type_service = '" + type_service + "' and mad.month='Dec' GROUP BY mad.type_service, mad.quantity),0) AS Dec");
            sb.AppendLine(@" from mnda_activity_data mad WHERE mad.type_service = '" + type_service + "'");
            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }

        public DataSet LoadFundSource(string _div_id, string _year , Boolean isRevised,String FundCode,String FundType)
        {
            var sb = new System.Text.StringBuilder(282);
           
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"div.Division_Code pap, ");
            sb.AppendLine(@"mooe.name, ");
            sb.AppendLine(@"  mfsl.Fund_Source_Id,");
            sb.AppendLine(@"  mfsl.division_id,");
            sb.AppendLine(@"  mfsl.mooe_id,mrl.respo_name,");
            sb.AppendLine(@"mfsl.amount as Amount,");        
            sb.AppendLine(@"  mfsl.Year,");
            sb.AppendLine(@"0 as adjustment,");
            sb.AppendLine(@" ISNULL(mfsl.is_revision,0) as is_revision,");
            sb.AppendLine(@" ISNULL(mfsl.plusminus,0) as plusminus");
            sb.AppendLine(@"FROM   dbo.mnda_fund_source_line_item mfsl");
            sb.AppendLine(@"INNER JOIN Division div on div.Division_Id = mfsl.division_id");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.uacs_code = mfsl.mooe_id");
            sb.AppendLine(@"INNER JOIN mnda_respo_library mrl on mrl.code = mfsl.Fund_Source_Id");
            if (isRevised)
            {
                sb.AppendLine(@"WHERE mfsl.division_id = " + _div_id + " AND mfsl.Year = " + _year + " AND Fund_Source_Id = '" + FundCode + "' AND div.year_setup = " + _year + "");
            }
            else
            {
                sb.AppendLine(@"WHERE mfsl.division_id = " + _div_id + " AND mfsl.Year = " + _year + "  AND Fund_Source_Id = '" + FundCode + "' AND div.year_setup = " + _year + "");
            }
            //AND is_revision = 0
            sb.AppendLine(@"GROUP BY div.Division_Code,mooe.name,mrl.respo_name,mfsl.Fund_Source_Id,mfsl.division_id,mfsl.mooe_id,mfsl.Amount,mfsl.Year, mfsl.plusminus, mfsl.is_revision,mfsl.plusminus;");

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            DataTable dtRAF = LoadRafNo(_div_id, _year,FundType);

            foreach (DataRow item in dtRAF.Rows)
            {
                StringBuilder varname1 = new StringBuilder();
                varname1.Append("SELECT \n");
                varname1.Append("mfsa.mooe_id, \n");
                varname1.Append("mfsa.adjustment, \n");
                varname1.Append("mfsa.plusminus \n");
                varname1.Append("FROM mnda_fund_source_adjustment mfsa \n");
                varname1.Append("WHERE mfsa.raf_no = '"+item[0].ToString() +"' and mfsa.fund_code = '"+ FundCode +"'");

                DataSet dsRAFTB = c_data.ExecuteSQLQuery(varname1.ToString());

                foreach (DataRow itemRaf in dsRAFTB.Tables[0].Rows)
                {
                    dsData.Tables[0].DefaultView.RowFilter = "mooe_id  ='" + itemRaf["mooe_id"].ToString() + "'";

                    double  _amount = Convert.ToDouble(dsData.Tables[0].DefaultView[0]["Amount"].ToString());

                    switch (itemRaf["plusminus"].ToString())
                    {
                        case "+":
                            _amount += Convert.ToDouble(itemRaf["adjustment"].ToString());
                            break;
                        case "-":
                            _amount -= Convert.ToDouble(itemRaf["adjustment"].ToString());
                            break;
                        default:
                            break;
                    }

                    dsData.Tables[0].DefaultView[0]["Amount"] = _amount.ToString();
                    dsData.AcceptChanges();
                }

            }

            return dsData;
        }

        public DataTable LoadRafNo(string div_id,string _year,String FundType)
        {
            StringBuilder varname1 = new StringBuilder();
            varname1.Append("SELECT \n");
            varname1.Append("raf.number as no \n");
            varname1.Append("FROM mnda_raf_no raf \n");
            varname1.Append("WHERE raf.div_id = '"+ div_id +"' and raf.year ="+ _year +" \n");
            varname1.Append("AND raf.status = 'Served' AND raf.mf_type ='"+FundType +"'");

            DataSet dsData = c_data.ExecuteSQLQuery(varname1.ToString());


            return dsData.Tables[0];

        }

        public DataTable FetchSuppliesPerDivision(string _div_id, string _year, Boolean isrevised, String _fundsource)
        {
            StringBuilder varname1 = new StringBuilder();
            varname1.Append("SELECT");
            varname1.Append(" DISTINCT mad.type_service, mooe.name, mooe.uacs_code");
            varname1.Append(" FROM mnda_activity_data mad");
            varname1.Append(" LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            varname1.Append(" LEFT JOIN mnda_fund_source mfs on mfs.code = mad.fund_source_id");
            varname1.Append(" WHERE mad.year = " + _year + " and mfs.service_type = 1 and mad.total !=0 and mad.status ='FINANCE APPROVED'");
            varname1.Append(" AND mad.fund_source_id ='" + _fundsource + "'  AND mad.is_approved = 1");
            varname1.Append(" GROUP BY ");
            varname1.Append(" mfs.Fund_Name,");
            varname1.Append(" mooe.uacs_code,");
            varname1.Append(" mooe.name,");
            varname1.Append(" mad.total,");
            varname1.Append(" mad.quantity,");
            varname1.Append(" mad.type_service,");
            varname1.Append(" mad.id,");
            varname1.Append(" mad.month,");
            varname1.Append(" mad.year ,mad.entry_date,");
            varname1.Append(" mad.start,");
            varname1.Append(" mad.[end],mad.status");
            DataSet dsData = c_data.ExecuteSQLQuery(varname1.ToString());

            return dsData.Tables[0];
        }

        public DataSet FetchData(string _div_id, string _year,Boolean isrevised,String _fundsource, String _PAPCode)
        {
            var sb = new System.Text.StringBuilder(873);
            //sb.AppendLine(@"SELECT");
            //sb.AppendLine(@"  mad.id,");
            //sb.AppendLine(@"  '' as acountable_division_code,");
            //sb.AppendLine(@"  mfs.Fund_Name,");
            //sb.AppendLine(@"  '' Division_Desc,");
            //sb.AppendLine(@"  '' Division_Code,");
            //sb.AppendLine(@"  mooe.uacs_code,");
            //sb.AppendLine(@"  mooe.name,");
            //sb.AppendLine(@"  mad.total as rate,");
            //sb.AppendLine(@"  mad.quantity,mad.type_service,ISNULL(mad.id,0) as act_id,");
            //sb.AppendLine(@"  mad.month,");
            //sb.AppendLine(@"  mad.year,");
            //sb.AppendLine(@"  ISNULL(mad.entry_date,'') as entry_date,");
            //sb.AppendLine(@"  ISNULL(mad.start,'') as s_date,");
            //sb.AppendLine(@"  ISNULL(mad.[end],'') as e_date,");
            //sb.AppendLine(@"  mad.status");
            //sb.AppendLine(@"FROM mnda_activity_data mad");
            //sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            //sb.AppendLine(@"LEFT JOIN mnda_fund_source mfs on mfs.code = mad.fund_source_id");
            //sb.AppendLine(@"WHERE mad.year = "+ _year +" and mfs.service_type = 1 and mad.total !=0 and mad.status ='FINANCE APPROVED'");
            //sb.AppendLine(@"AND mad.fund_source_id ='" + _fundsource + "'  AND mad.is_approved = 1");
            //sb.AppendLine(@"GROUP BY ");
            //sb.AppendLine(@"  mfs.Fund_Name,");
            //sb.AppendLine(@"  mooe.uacs_code,");
            //sb.AppendLine(@"  mooe.name,");
            //sb.AppendLine(@"  mad.total,");
            //sb.AppendLine(@"  mad.quantity,");
            //sb.AppendLine(@"  mad.type_service,");
            //sb.AppendLine(@"  mad.id,");
            //sb.AppendLine(@"  mad.month,");
            //sb.AppendLine(@"  mad.year ,mad.entry_date,");
            //sb.AppendLine(@"  mad.start,");
            //sb.AppendLine(@"  mad.[end],mad.status");

            sb.AppendLine(@"SELECT");
            sb.AppendLine(@" ISNULL(mnd.id,'') as program_id,");
            sb.AppendLine(@" ISNULL(mpe.id,'') as project_id,");
            sb.AppendLine(@" ISNULL(mp.id,'') as program_index,");
            sb.AppendLine(@" ISNULL(mpo.id,'') as output_id,");
            sb.AppendLine(@" ISNULL(mnd.fiscal_year,'') as fiscal_year,");
            sb.AppendLine(@" ISNULL(mp.name,'') as program_name,");
            sb.AppendLine(@" ISNULL(mpo.name,'') as output,");
            sb.AppendLine(@" ISNULL(mpe.project_name,'') as project_name,");
            sb.AppendLine(@" ISNULL(ma.description,'') as activity, mooe.name,");
            sb.AppendLine(@" ISNULL(ma.id,'') as activity_id,");
            sb.AppendLine(@" ISNULL(mua.User_Fullname,'') as User_Fullname,");
            sb.AppendLine(@" mad.id, '' as acountable_division_code,");
            sb.AppendLine(@" mfs.Fund_Name, div.Division_Desc, div.Division_Code,");
            sb.AppendLine(@" mooe.uacs_code, mad.total as rate,");
            sb.AppendLine(@" mad.quantity,mad.type_service,");
            sb.AppendLine(@" ISNULL(mad.id,0) as act_id,");
            sb.AppendLine(@" mad.month, mad.year,");
            sb.AppendLine(@" ISNULL(mad.entry_date,'') as entry_date,");
            sb.AppendLine(@" ISNULL(mad.start,'') as s_date,");
            sb.AppendLine(@" ISNULL(mad.[end],'') as e_date, mad.status,mad.procure_item");
            sb.AppendLine(@" FROM mnda_main_program_encoded mnd");
            sb.AppendLine(@" LEFT JOIN mnda_programs mp on mp.id = mnd.program_code");
            sb.AppendLine(@" LEFT JOIN mnda_program_encoded mpe on mpe.main_program_id = mnd.id");
            sb.AppendLine(@" LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@" LEFT JOiN mnda_project_output  mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@" LEFT JOIN mnda_activity ma on ma.output_id = mpo.id");
            sb.AppendLine(@" LEFT JOIN mnda_activity_data mad on mad.activity_id = ma.id");
            sb.AppendLine(@" LEFT JOIN mnda_user_accounts mua on mua.User_Id = ma.accountable_member_id");
            sb.AppendLine(@" LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@" LEFT JOIN mnda_fund_source mfs on mfs.code = mad.fund_source_id");
            sb.AppendLine(@" WHERE mad.year = " + _year + " AND mfs.service_type = 1");
            sb.AppendLine(@" and mad.total !=0");
            sb.AppendLine(@" and mad.status ='FINANCE APPROVED'");
            sb.AppendLine(@" AND mad.fund_source_id ='" + _fundsource + "'");
            sb.AppendLine(@" AND mad.is_approved = 1");
            sb.AppendLine(@" AND div.Division_Code = '"+_PAPCode+"'");
            //sb.AppendLine(@" AND mad.procure_item = 1");
            sb.AppendLine(@" GROUP BY");
            sb.AppendLine(@" mnd.id, mpe.id,");
            sb.AppendLine(@" mp.id,mpo.id, mnd.fiscal_year,");
            sb.AppendLine(@" mp.name,mpo.name,");
            sb.AppendLine(@" mpe.project_name,ma.description,");
            sb.AppendLine(@" ma.id,mua.User_Fullname,");
            sb.AppendLine(@" mfs.Fund_Name, div.Division_Desc, div.Division_Code, mooe.uacs_code,");
            sb.AppendLine(@" mooe.name, mad.total,");
            sb.AppendLine(@" mad.quantity, mad.type_service,");
            sb.AppendLine(@" mad.id, mad.month,");
            sb.AppendLine(@" mad.year ,mad.entry_date,");
            sb.AppendLine(@" mad.start, mad.[end],mad.status,mad.procure_item ORDER BY activity ASC");

          
            //sb.AppendLine(@"SELECT");
            //sb.AppendLine(@"  mpe.acountable_division_code,");
            //sb.AppendLine(@"  mfs.Fund_Name,");
            //sb.AppendLine(@"  div.Division_Desc,");
            //sb.AppendLine(@"  div.Division_Code,");
            //sb.AppendLine(@"  mooe.uacs_code,");
            //sb.AppendLine(@"  mooe.name,");
            //sb.AppendLine(@"  mad.total as rate,");
            //sb.AppendLine(@"  mad.quantity,mad.type_service,ISNULL(mad.id,0) as act_id,");
            //sb.AppendLine(@"  mad.month,");
            //sb.AppendLine(@"  mad.year,");
            //sb.AppendLine(@"  ISNULL(mad.entry_date,'') as entry_date,");
            //sb.AppendLine(@"  ISNULL(mad.start,'') as s_date,");
            //sb.AppendLine(@"  ISNULL(mad.[end],'') as e_date,");
            //sb.AppendLine(@"  mad.status");
            //sb.AppendLine(@"FROM mnda_approved_projects_division madp");
            //sb.AppendLine(@"LEFT JOIN mnda_activity_data mad on mad.id = madp.activity_id");
            //sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            //sb.AppendLine(@"LEFT JOIN mnda_activity ma on ma.id = madp.main_activity_id");
            //sb.AppendLine(@"LEFT JOIN mnda_project_output mpo on mpo.id = ma.output_id	");
            //sb.AppendLine(@"LEFT JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
            //sb.AppendLine(@"LEFT JOIN mnda_fund_source mfs on mfs.code = mad.fund_source_id");
            //sb.AppendLine(@"LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            //sb.AppendLine(@"WHERE mpe.acountable_division_code = " + _div_id + " AND mad.year = " + _year + " and mfs.service_type = 1 and mad.total !=0 and mad.status ='FINANCE APPROVED'");
            //sb.AppendLine(@"GROUP BY ");
            //sb.AppendLine(@"  mpe.acountable_division_code,");
            //sb.AppendLine(@"  mfs.Fund_Name,");
            //sb.AppendLine(@"  div.Division_Desc,");
            //sb.AppendLine(@"  div.Division_Code,");
            //sb.AppendLine(@"  mooe.uacs_code,");
            //sb.AppendLine(@"  mooe.name,");
            //sb.AppendLine(@"  mad.total,");
            //sb.AppendLine(@"  mad.quantity,");
            //sb.AppendLine(@"  mad.type_service,");
            //sb.AppendLine(@"  mad.id,");
            //sb.AppendLine(@"  mad.month,");
            //sb.AppendLine(@"  mad.year ,mad.entry_date,");
            //sb.AppendLine(@"  mad.start,");
            //sb.AppendLine(@"  mad.[end],mad.status");

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());
        
            return dsData;
        }
        public DataSet FetchDataAnnexA(string _div_id, string _year)
        {
            var sb = new System.Text.StringBuilder(535);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@" '' as mfo_pap,");
            sb.AppendLine(@" CONCAT(mo_from.uacs_code, ' ', mo_from.name) as from_obect_expenditure,");
            sb.AppendLine(@" mar.total_alignment as from_total,");
            sb.AppendLine(@" CONCAT(mo_to.uacs_code, ' ', mo_to.name) as to_obect_expenditure,");
            sb.AppendLine(@" mar.total_alignment as to_total");
            sb.AppendLine(@"FROM mnda_alignment_record mar");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mo_from on mo_from.uacs_code = mar.from_uacs");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mo_to on mo_to.uacs_code = mar.to_uacs");
            sb.AppendLine(@"WHERE mar.division_pap = "+ _div_id +" and mar.division_year = "+ _year +"");
            sb.AppendLine(@"and mar.is_active = 1 and mar.is_approved = 1");


            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());
            return dsData;
        }
        public DataSet FetchDataAnnexBDefficient(string _div_id, string _year)
        {
            var sb = new System.Text.StringBuilder(486);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@" CONCAT(mo_to.uacs_code, ' ', mo_to.name) as from_obect_expenditure,");
            sb.AppendLine(@"SUM(CAST(mar.total_alignment as Numeric(18,2))) as from_total");
            sb.AppendLine(@"FROM mnda_alignment_record mar");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mo_from on mo_from.uacs_code = mar.from_uacs");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mo_to on mo_to.uacs_code = mar.to_uacs");
            sb.AppendLine(@"WHERE mar.division_pap = "+ _div_id +" and mar.division_year = "+ _year +"");
            sb.AppendLine(@"and mar.is_active = 1 and mar.is_approved = 1");
            sb.AppendLine(@"GROUP BY mo_to.uacs_code, mo_to.name");



            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());
            return dsData;
        }

        public DataSet FetchDataAnnexBSource(string _div_id, string _year)
        {
            var sb = new System.Text.StringBuilder(486);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@" CONCAT(mo_from.uacs_code, ' ', mo_from.name) as from_obect_expenditure,");
            sb.AppendLine(@"SUM(CAST(mar.total_alignment as Numeric(18,2))) as from_total");
            sb.AppendLine(@"FROM mnda_alignment_record mar");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mo_from on mo_from.uacs_code = mar.from_uacs");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mo_to on mo_to.uacs_code = mar.to_uacs");
            sb.AppendLine(@"WHERE mar.division_pap = " + _div_id + " and mar.division_year = " + _year + "");
            sb.AppendLine(@"and mar.is_active = 1 and mar.is_approved = 1");
            sb.AppendLine(@"GROUP BY mo_from.uacs_code, mo_from.name");



            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());
            return dsData;
        }
        public DataSet FetchDataAnnexATitle(string _div_id, string _year)
        {
            var sb = new System.Text.StringBuilder(436);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@" '' as mfo_pap,");
            sb.AppendLine(@" CONCAT(mo_from.uacs_code, ' ', mo_from.name) as from_obect_expenditure");
            sb.AppendLine(@"FROM mnda_alignment_record mar");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mo_from on mo_from.uacs_code = mar.from_uacs");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mo_to on mo_to.uacs_code = mar.to_uacs");
            sb.AppendLine(@"WHERE mar.division_pap = "+ _div_id +" and mar.division_year = "+ _year+"");
            sb.AppendLine(@"and mar.is_active = 1 and mar.is_approved = 1");
            sb.AppendLine(@"GROUP BY mo_from.uacs_code,mo_from.name");



            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());
            return dsData;
        }
        public Boolean ExecuteData(String _sql)
        {

            Boolean _res = c_data.ExecuteQuery(_sql);

            return _res;
        }
    }
}
