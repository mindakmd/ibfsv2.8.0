﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ReportTool.MinDAF {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="OfficeData", Namespace="http://schemas.datacontract.org/2004/07/MinDAF.Web.Service")]
    [System.SerializableAttribute()]
    public partial class OfficeData : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string IdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string OfficeNameField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Id {
            get {
                return this.IdField;
            }
            set {
                if ((object.ReferenceEquals(this.IdField, value) != true)) {
                    this.IdField = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string OfficeName {
            get {
                return this.OfficeNameField;
            }
            set {
                if ((object.ReferenceEquals(this.OfficeNameField, value) != true)) {
                    this.OfficeNameField = value;
                    this.RaisePropertyChanged("OfficeName");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="", ConfigurationName="MinDAF.MinDAFSVC")]
    public interface MinDAFSVC {
        
        [System.ServiceModel.OperationContractAttribute(Action="urn:MinDAFSVC/DoWork", ReplyAction="urn:MinDAFSVC/DoWorkResponse")]
        void DoWork();
        
        [System.ServiceModel.OperationContractAttribute(Action="urn:MinDAFSVC/VerifyUser", ReplyAction="urn:MinDAFSVC/VerifyUserResponse")]
        bool VerifyUser(string _username, string _password);
        
        [System.ServiceModel.OperationContractAttribute(Action="urn:MinDAFSVC/FetchOffice", ReplyAction="urn:MinDAFSVC/FetchOfficeResponse")]
        ReportTool.MinDAF.OfficeData FetchOffice();
        
        [System.ServiceModel.OperationContractAttribute(Action="urn:MinDAFSVC/ExecuteImportDataSQL", ReplyAction="urn:MinDAFSVC/ExecuteImportDataSQLResponse")]
        string ExecuteImportDataSQL(string _sql);
        
        [System.ServiceModel.OperationContractAttribute(Action="urn:MinDAFSVC/SendMail", ReplyAction="urn:MinDAFSVC/SendMailResponse")]
        string SendMail(string _FromEmail, string _ToEmail, string Message, string Subject, string _Type);
        
        [System.ServiceModel.OperationContractAttribute(Action="urn:MinDAFSVC/ExecuteSQL", ReplyAction="urn:MinDAFSVC/ExecuteSQLResponse")]
        string ExecuteSQL(string _sql);
        
        [System.ServiceModel.OperationContractAttribute(Action="urn:MinDAFSVC/ExecuteSQLReportQuery", ReplyAction="urn:MinDAFSVC/ExecuteSQLReportQueryResponse")]
        System.Data.DataSet ExecuteSQLReportQuery(string _sql);
        
        [System.ServiceModel.OperationContractAttribute(Action="urn:MinDAFSVC/ExecuteSQLReportQueryProcurement", ReplyAction="urn:MinDAFSVC/ExecuteSQLReportQueryProcurementResponse")]
        System.Data.DataSet ExecuteSQLReportQueryProcurement(string _sql);
        
        [System.ServiceModel.OperationContractAttribute(Action="urn:MinDAFSVC/ExecuteSQLReportQueryHRIS", ReplyAction="urn:MinDAFSVC/ExecuteSQLReportQueryHRISResponse")]
        System.Data.DataSet ExecuteSQLReportQueryHRIS(string _sql);
        
        [System.ServiceModel.OperationContractAttribute(Action="urn:MinDAFSVC/ExecuteQuery", ReplyAction="urn:MinDAFSVC/ExecuteQueryResponse")]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(ReportTool.MinDAF.OfficeData))]
        bool ExecuteQuery(object _SQL);
        
        [System.ServiceModel.OperationContractAttribute(Action="urn:MinDAFSVC/ApproveToken", ReplyAction="urn:MinDAFSVC/ApproveTokenResponse")]
        bool ApproveToken();
        
        [System.ServiceModel.OperationContractAttribute(Action="urn:MinDAFSVC/ExecuteQueryProcurement", ReplyAction="urn:MinDAFSVC/ExecuteQueryProcurementResponse")]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(ReportTool.MinDAF.OfficeData))]
        bool ExecuteQueryProcurement(object _SQL);
        
        [System.ServiceModel.OperationContractAttribute(Action="urn:MinDAFSVC/ExecuteQueryLists", ReplyAction="urn:MinDAFSVC/ExecuteQueryListsResponse")]
        bool ExecuteQueryLists(string _SQL);
        
        [System.ServiceModel.OperationContractAttribute(Action="urn:MinDAFSVC/ExecuteQueryReturnID", ReplyAction="urn:MinDAFSVC/ExecuteQueryReturnIDResponse")]
        string ExecuteQueryReturnID(string _SQL);
        
        [System.ServiceModel.OperationContractAttribute(Action="urn:MinDAFSVC/ExecuteListQuery", ReplyAction="urn:MinDAFSVC/ExecuteListQueryResponse")]
        [System.ServiceModel.ServiceKnownTypeAttribute(typeof(ReportTool.MinDAF.OfficeData))]
        bool ExecuteListQuery(object _data);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface MinDAFSVCChannel : ReportTool.MinDAF.MinDAFSVC, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class MinDAFSVCClient : System.ServiceModel.ClientBase<ReportTool.MinDAF.MinDAFSVC>, ReportTool.MinDAF.MinDAFSVC {
        
        public MinDAFSVCClient() {
        }
        
        public MinDAFSVCClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public MinDAFSVCClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MinDAFSVCClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MinDAFSVCClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public void DoWork() {
            base.Channel.DoWork();
        }
        
        public bool VerifyUser(string _username, string _password) {
            return base.Channel.VerifyUser(_username, _password);
        }
        
        public ReportTool.MinDAF.OfficeData FetchOffice() {
            return base.Channel.FetchOffice();
        }
        
        public string ExecuteImportDataSQL(string _sql) {
            return base.Channel.ExecuteImportDataSQL(_sql);
        }
        
        public string SendMail(string _FromEmail, string _ToEmail, string Message, string Subject, string _Type) {
            return base.Channel.SendMail(_FromEmail, _ToEmail, Message, Subject, _Type);
        }
        
        public string ExecuteSQL(string _sql) {
            return base.Channel.ExecuteSQL(_sql);
        }
        
        public System.Data.DataSet ExecuteSQLReportQuery(string _sql) {
            return base.Channel.ExecuteSQLReportQuery(_sql);
        }
        
        public System.Data.DataSet ExecuteSQLReportQueryProcurement(string _sql) {
            return base.Channel.ExecuteSQLReportQueryProcurement(_sql);
        }
        
        public System.Data.DataSet ExecuteSQLReportQueryHRIS(string _sql) {
            return base.Channel.ExecuteSQLReportQueryHRIS(_sql);
        }
        
        public bool ExecuteQuery(object _SQL) {
            return base.Channel.ExecuteQuery(_SQL);
        }
        
        public bool ApproveToken() {
            return base.Channel.ApproveToken();
        }
        
        public bool ExecuteQueryProcurement(object _SQL) {
            return base.Channel.ExecuteQueryProcurement(_SQL);
        }
        
        public bool ExecuteQueryLists(string _SQL) {
            return base.Channel.ExecuteQueryLists(_SQL);
        }
        
        public string ExecuteQueryReturnID(string _SQL) {
            return base.Channel.ExecuteQueryReturnID(_SQL);
        }
        
        public bool ExecuteListQuery(object _data) {
            return base.Channel.ExecuteListQuery(_data);
        }
    }
}
