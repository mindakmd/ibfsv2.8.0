﻿using Infragistics.Windows.Reporting;
using Procurement_Module.Forms;
using ReportTool.Class;
using ReportTool.MinDAF;
using ReportTool.ReportDesigners;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ReportTool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
     
        private clsReportFinance c_dues = new clsReportFinance();
        UserDetail c_user = new UserDetail();

        public MainWindow()
        {
           
                InitializeComponent();

            _report.Owner = Window.GetWindow(this);

        }

        private void FetchReportsCache() 
        {
            DataSet ds = c_dues.FetchFDCache().Copy();
            String ReportType = "";
            String Years = "";
            String DivisionId = "";
            if (ds.Tables[0].Rows.Count !=0)
            {
                ReportType = ds.Tables[0].Rows[0]["report_command"].ToString();
                Years = ds.Tables[0].Rows[0]["requested_year"].ToString();
                DivisionId = ds.Tables[0].Rows[0]["division_id"].ToString();

            }

            switch (ReportType)
            {
                case "Division Distribution":
                    LoadReportDivisionDistribution(false);
                    break;
                case "Division Distribution CONT":
                    LoadReportDivisionDistribution(true);
                    break;
                case "APP REPORT":
                    LoadReportAPP(Years);
                    break;
            }
        }

        private void LoadReportTypes()        
        {
            cmbReportType.Items.Clear();
            if (c_user.DivisionId == "4" && c_user.UserLevel == "1")
            {
                cmbReportType.Items.Add("MOOE Summary");
                cmbReportType.Items.Add("PPMP Viewer");
                cmbReportType.Items.Add("Generate APP");
            
            }
            else if (c_user.DivisionId == "3" && c_user.UserLevel == "1")     
            {
            //    cmbReportType.Items.Add("Monthly Actual Obligation");
           //     cmbReportType.Items.Add("Monthly Actual Obligation Summary");
                cmbReportType.Items.Add("Division Distribution");
                cmbReportType.Items.Add("BEDs Report");
                cmbReportType.Items.Add("Project Summary");
           //     cmbReportType.Items.Add("Division Distribution CONT");
           //     cmbReportType.Items.Add("MOOE Summary");
           //     cmbReportType.Items.Add("PPMP Viewer");
            //    cmbReportType.Items.Add("PPMP Printout");
                cmbYear.Text = DateTime.Now.Year.ToString();
                cmbDivision.IsEnabled = false;
            }
           
        }

        private void LoadReportMembers()
        {
       
            frmPPMP ppmp = new frmPPMP();

            ppmp.DivID = c_user.DivisionId;
            ppmp.PAPCode = c_user.DivisionCode;
            ppmp.Closed += ppmp_Closed;
            ppmp.Show();
            this.Visibility = System.Windows.Visibility.Hidden;
        }

        void ppmp_Closed(object sender, EventArgs e)
        {
            cmbReportType.IsEnabled = false;
            cmbDivision.IsEnabled = false;
            cmbYear.IsEnabled = false;
            btnLoad.IsEnabled = false;
            this.Visibility = System.Windows.Visibility.Visible;
            txtPass.Clear();
            txtPass.Focus();
        }
        


        private void LoadReportDivisionDistribution(Boolean isCont) 
        {
            try
            {
                rpt_NepReport _rptData = new rpt_NepReport();
                rptNEPSummary2018 _rptDataNEP = new rptNEPSummary2018();

                String _path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"ReportSchemas\";

                DataSet ds = c_dues.ProcessDivisionDistribution(isCont,cmbYear.SelectedItem.ToString()).Copy();

                switch (cmbYear.SelectedItem.ToString())
                {
                    case "2017":
                        //   ds.WriteXmlSchema(_path + "div_dis.xml");
                        _rptData.SetDataSource(ds);

                        _report.ViewerCore.Zoom(70);
                        _report.ViewerCore.ReportSource = _rptData;
                        break;
                    case "2018":
                        //  ds.WriteXmlSchema(_path + "div_dis_nep_2018.xml");
                        _rptDataNEP.SetDataSource(ds);

                        _report.ViewerCore.Zoom(70);
                        _report.ViewerCore.ReportSource = _rptDataNEP;
                        break;
                }
                    
              
               
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + Environment.NewLine + ex.InnerException.ToString());
            }
           

        }

        private void LoadReportBeds(String WorkingYear)
        {
            try
            {
                rptBED3 _rptData = new rptBED3();
              

                String _path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"ReportSchemas\";

                DataSet ds = c_dues.ProcessBedsReport(cmbYear.SelectedItem.ToString()).Copy();


             // ds.WriteXmlSchema(_path + "beds.xml");
                _rptData.SetDataSource(ds);

                _report.ViewerCore.Zoom(70);
                _report.ViewerCore.ReportSource = _rptData;
                    


            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + Environment.NewLine + ex.InnerException.ToString());
            }


        }
        private void LoadReportAPP(String Years)
        {
            rptAPP _rptData = new rptAPP();

            String _path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"ReportSchemas\";
            String DivisionPap = "";

            //var x_data = c_dues.DivisionData.Where(items => items.DivisionName == cmbDivision.SelectedItem.ToString()).ToList();

            //if (x_data.Count!=0)
            //{
            //    DivisionPap = x_data[0].DivisionCode;
            //}

            DataSet ds = c_dues.FetchDataAPP(Years).Copy();
          //    ds.WriteXmlSchema(_path + "div_app.xml");
            _rptData.SetDataSource(ds);

            _report.ViewerCore.Zoom(70);
            _report.ViewerCore.ReportSource = _rptData;

        }
        private void LoadReportPPMP()
        {
            rptPPMPUser _rptData = new rptPPMPUser();

            String _path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"ReportSchemas\";
            DataSet ds = c_dues.FetchDataPPMP(c_user.DivisionCode,cmbYear.SelectedItem.ToString(),c_user.DivisionName).Copy();
        //   ds.WriteXmlSchema(_path + "div_ppmp.xml");
            _rptData.SetDataSource(ds);

            _report.ViewerCore.Zoom(70);
            _report.ViewerCore.ReportSource = _rptData;

        }
        private void LoadReportMAO()
        {
            rptMAO _rptData = new rptMAO();

            String _path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"ReportSchemas\";
            DataSet ds = c_dues.FetchDataMAO().Copy();
           //  ds.WriteXmlSchema(_path + "div_mao.xml");
            _rptData.SetDataSource(ds);

            _report.ViewerCore.Zoom(70);
            _report.ViewerCore.ReportSource = _rptData;

        }
        private void GenerateYear()
        {
            int _year = DateTime.Now.Year;
            int _limit = _year + 100;

            for (int i = _year; i != _limit; i++)
            {
                cmbYear.Items.Add(_year);
                _year += 1;
            }

        }

        private void btnX_Click(object sender, RoutedEventArgs e)
        {

            Environment.Exit(0);
        }
        frmSecurity f_sec = new frmSecurity();
        private void frmMain_Loaded(object sender, RoutedEventArgs e)
        {
           // FetchReportsCache();
            LoadReportTypes();
            GenerateYear();
            GenerateDivisions();

            //f_sec = new frmSecurity();
            //f_sec.LoginHandler += f_sec_LoginHandler;
            //f_sec.Visibility = System.Windows.Visibility.Visible;
            //f_sec.ShowDialog();
            
        }

        void f_sec_LoginHandler(object sender, EventArgs e)
        {
           
        }
        private void GenerateDivisions() 
        {
            cmbDivision.Items.Clear();
            c_dues.FetchDivisions();
            foreach (var item in c_dues.DivisionData)
            {
                cmbDivision.Items.Add(item.DivisionName);
            }

        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                

                switch (cmbReportType.SelectedItem.ToString())
                {
                    case "BEDs Report":
                        LoadReportBeds(cmbYear.SelectedItem.ToString());
                        break;
                    case "Division Distribution":
                        LoadReportDivisionDistribution(false);
                        break;
                    case "Division Distribution CONT":
                        LoadReportDivisionDistribution(true);
                        break;
                    case "Monthly Actual Obligation":
                        LoadReportMAO();
                        break;
                    case "PPMP Printout":
                      //  LoadReportPPMP();
                        LoadReportMembers();
                        break;
                    case "Generate APP":
                        LoadReportAPP("");
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message + Environment.NewLine + ex.InnerException, "ERROR", MessageBoxButton.OK);
            }
           
        }

        private void cmbReportType_DropDownClosed(object sender, EventArgs e)
        {
            if (cmbReportType.SelectedItem==null)
            {
                return;
            }
            switch (cmbReportType.SelectedItem.ToString())
            {
                case "Division Distribution":
                    cmbDivision.IsEnabled = false;
                    break;
                case "Division Distribution CONT":
                    cmbDivision.IsEnabled = false;
                    break;
                case  "PPMP Printout":
                    if (c_user.DivisionId == "3" && c_user.UserLevel == "1")     
                    {
                        for (int i = 0; i < cmbDivision.Items.Count; i++)
                        {
                            if (cmbDivision.Items[i].ToString() == "Finance Division (FD)")
                            {
                                cmbDivision.SelectedIndex = i;
                                break;
                            }
                        }
                       
                    }
                    break;
                default:
                    break;
            }
        }
        clsReportFinance c_rf = new clsReportFinance();
        private UserDetail c_user_details = new UserDetail();

        private void txtPass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (c_rf.LoginUser(txtPass.Password))
                {

                    c_user = c_rf.UserInfo;

                    switch (c_user.UserLevel)
                    {
                        case "1":

                            frmSelection f_select = new frmSelection();
                            f_select.ShowDialog();

                            if (f_select.DialogResult.Value.ToString().ToLower()=="true")
                            {
                                switch (f_select.Selected)
                                {
                                    case "Finance Reports":
                                        LoadReportTypes();
                                        grdLogin.Visibility = System.Windows.Visibility.Collapsed;
                                       
                                          cmbReportType.IsEnabled = true;
                                            cmbDivision.IsEnabled = true;
                                            cmbYear.IsEnabled = true;
                                            btnLoad.IsEnabled = true;
                                        break;
                                    case "PPMP Reports":
                                          LoadReportTypes();
                                          
                                            LoadReportMembers();
                                        break;
                                    default:
                                        break;
                                }
                            }
                          
                            break;
                        case "0":
                            LoadReportMembers();
                            break;
                        case "2":
                            LoadReportMembers();
                            break;
                    }

                }
                else
                {
                    MessageBox.Show("Invalid Credentials, Please try again.");
                    txtPass.SelectAll();
                    txtPass.Focus();
                }
            }
        }
    }
}
