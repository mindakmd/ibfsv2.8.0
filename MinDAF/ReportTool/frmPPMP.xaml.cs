﻿using Procurement_Module.Class;

using ReportTool;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using Infragistics.Windows.DataPresenter;
using Infragistics.Windows.DataPresenter.ExcelExporter;
using Infragistics.Documents.Excel;

namespace Procurement_Module.Forms
{
    /// <summary>
    /// Interaction logic for frmPPMP.xaml
    /// </summary>
    public partial class frmPPMP : Window
    {
        private string WorkingYear = "";
        private DataTable dtDivisions = new DataTable();
        private clsPPMP c_ppmp = new clsPPMP();
        DataPresenterExcelExporter exporter = new DataPresenterExcelExporter();
        public String DivID { get; set; }
        public String PAPCode { get; set; }
        public String PAPId { get; set; }

        public String Pap_Code;
        public frmPPMP()
        {
            InitializeComponent();
            frmppmp.Loaded += frmppmp_Loaded;
        }

        void frmppmp_Loaded(object sender, RoutedEventArgs e)
        {

            frmYearSelect f_year = new frmYearSelect();
            f_year.ShowDialog();
            if (f_year.DialogResult.ToString().ToLower() == "true")
            {
                WorkingYear = f_year.WorkingYear;
                LoadDivision(DivID);
            }
            totalAmount.Visibility = System.Windows.Visibility.Collapsed;
            totalAmountLbl.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void LoadDivision(String DivId)
        {
            dtDivisions = c_ppmp.GetDivisionListDetails(DivID, WorkingYear).Tables[0].Copy();
            cmbDivisions.Items.Clear();
            foreach (DataRow item in dtDivisions.Rows)
            {
                cmbDivisions.Items.Add(item[1].ToString());
            }
            cmbDivisions.SelectedIndex = 0;
            cmbDivisions.IsEnabled = false;
        }
        private Boolean isRevised = false;
        private String PAP = "";
        frmPPMPLoad p_data = new frmPPMPLoad();

        private void RefreshData()
        {
            grdData.DataSource = null;

            dtDivisions.DefaultView.RowFilter = "div_name ='" + cmbDivisions.SelectedItem.ToString() + "'";
            String DivId = dtDivisions.DefaultView[0][0].ToString();
            String FundSource = "";
            dtDivisions.DefaultView.RowFilter = "";


            PAP = "";
            DataTable dtFundSource = c_ppmp.LoadFundSource(DivId, WorkingYear, isRevised, p_data.FundSourceId, p_data.FundType).Tables[0].Copy();
            if (dtFundSource.Rows.Count != 0)
            {
                foreach (DataRow item in dtFundSource.Rows)
                {
                    if (item["respo_name"].ToString().Contains("CONT"))
                    {
                        continue;
                    }
                    else
                    {
                        FundSource = item[2].ToString();
                        break;
                    }
                }

            }
            if (FundSource == "")
            {
                FundSource = p_data.FundSourceId;
            }

            Pap_Code = Regex.Replace(PAPCode, @"[^\d]", "");
            //DataTable dtPPMPData = c_ppmp.FetchData(DivId, WorkingYear, isRevised, FundSource, Pap_Code).Tables[0].Copy();
            DataTable dtPPMPData = c_ppmp.FetchData(DivId, WorkingYear, isRevised, FundSource, Pap_Code).Tables[0].Copy();

            if (chkCSE.IsChecked == true)
            {
                dtPPMPData.DefaultView.RowFilter = "uacs_code ='5020301000' or uacs_code='5020399000' or uacs_code='5023990001' or uacs_code ='50203011002'";
                dtFundSource.DefaultView.RowFilter = "mooe_id ='5020301000' or mooe_id='5020399000' or mooe_id = '5023990001' or mooe_id ='50203011002'";
                dtFundSource = dtFundSource.DefaultView.ToTable().Copy();

                dtPPMPData = dtPPMPData.DefaultView.ToTable().Copy();
                PAP = dtFundSource.Rows[0]["pap"].ToString();
            }
            else
            {
                PAP = dtFundSource.Rows[0]["pap"].ToString();
                dtPPMPData.DefaultView.RowFilter = "NOT uacs_code ='5020301000'";
                dtPPMPData = dtPPMPData.DefaultView.ToTable().Copy();
                dtPPMPData.DefaultView.RowFilter = "NOT uacs_code ='5020399000'";
                dtPPMPData = dtPPMPData.DefaultView.ToTable().Copy();
                dtPPMPData.DefaultView.RowFilter = "NOT uacs_code ='5023990001'";
                dtPPMPData = dtPPMPData.DefaultView.ToTable().Copy();
                dtPPMPData.DefaultView.RowFilter = "NOT uacs_code ='50203011002'";
                dtPPMPData = dtPPMPData.DefaultView.ToTable().Copy();

                dtFundSource.DefaultView.RowFilter = "NOT mooe_id ='5020301000'";
                dtFundSource = dtFundSource.DefaultView.ToTable().Copy();
                dtFundSource.DefaultView.RowFilter = "NOT mooe_id ='5020399000'";
                dtFundSource = dtFundSource.DefaultView.ToTable().Copy();
                dtFundSource.DefaultView.RowFilter = "NOT mooe_id ='5023990001'";
                dtFundSource = dtFundSource.DefaultView.ToTable().Copy();
                dtFundSource.DefaultView.RowFilter = "NOT mooe_id ='50203011002'";
                dtFundSource = dtFundSource.DefaultView.ToTable().Copy();
            }
            //dtPPMPData = dtPPMPData.DefaultView.ToTable().Copy();
            DataTable dtData = new DataTable();
            DataTable dtRevisions = new DataTable();
            if (p_data.IsCONT == false)
            {
                dtFundSource.DefaultView.RowFilter = "NOT respo_name LIKE '*CONT*'";
                dtData = dtFundSource.DefaultView.ToTable().Copy();
                dtData.DefaultView.RowFilter = "is_revision=1";

                dtRevisions = dtData.DefaultView.ToTable().Copy();
                dtData.DefaultView.RowFilter = "is_revision=0";
                dtData = dtData.DefaultView.ToTable().Copy();
            }
            else
            {

                dtFundSource.DefaultView.RowFilter = "respo_name LIKE '*CONT*'";
                dtData = dtFundSource.DefaultView.ToTable().Copy();
                dtData.DefaultView.RowFilter = "is_revision=1";

                dtRevisions = dtData.DefaultView.ToTable().Copy();
                dtData.DefaultView.RowFilter = "is_revision=0";
                dtData = dtData.DefaultView.ToTable().Copy();
            }


            foreach (DataRow item in dtData.Rows)
            {
                double _total = 0.00;
                dtRevisions.DefaultView.RowFilter = "mooe_id ='" + item["mooe_id"] + "'";
                if (dtRevisions.DefaultView.ToTable().Rows.Count != 0)
                {
                    double _rowAmount = 0.00;
                    double _origAmount = 0.00;

                    foreach (DataRow itemRow in dtRevisions.DefaultView.ToTable().Rows)
                    {
                        _origAmount = Convert.ToDouble(item["Amount"].ToString());
                        _rowAmount = Convert.ToDouble(itemRow["Amount"].ToString());
                        if (itemRow["plusminus"].ToString() == "+")
                        {


                            item["Amount"] = (_rowAmount + _origAmount).ToString();
                        }
                        else
                        {
                            item["Amount"] = (_rowAmount - _origAmount).ToString();
                        }

                    }

                }


            }
            foreach (DataRow itemRev in dtRevisions.Rows)
            {
                //dtData.DefaultView.RowFilter = "mooe_id = '" + itemRev["mooe_id"].ToString() + "'";
                if (dtData.DefaultView.ToTable().Rows.Count == 0)
                {
                    DataRow dr = dtData.NewRow();
                    dr[0] = itemRev.ItemArray[0].ToString();
                    dr[1] = itemRev.ItemArray[1].ToString();
                    dr[2] = itemRev.ItemArray[2].ToString();
                    dr[3] = itemRev.ItemArray[3].ToString();
                    dr[4] = itemRev.ItemArray[4].ToString();
                    dr[5] = itemRev.ItemArray[5].ToString();
                    dr[6] = itemRev.ItemArray[6].ToString();
                    dr[7] = itemRev.ItemArray[7].ToString();
                    dr[8] = itemRev.ItemArray[8].ToString();
                    dr[9] = itemRev.ItemArray[9].ToString();
                    dr[10] = itemRev.ItemArray[10].ToString();
                    dtData.Rows.Add(dr);
                    continue;
                }
            }
            this.IsCONT = p_data.IsCONT;
            GenerateData(dtData, dtPPMPData, p_data.IsCONT);
        }
        private void LoadFundSource(String clicked)
        {
            dtDivisions.DefaultView.RowFilter = "div_name ='" + cmbDivisions.SelectedItem.ToString() + "'";
            String DivId = dtDivisions.DefaultView[0][0].ToString();
            String FundSource = "";
            dtDivisions.DefaultView.RowFilter = "";
            p_data = new frmPPMPLoad();
            isRevised = false;
            p_data.DivId = DivId;
            p_data.WorkingYear = this.WorkingYear;
            p_data.ShowDialog();
            if (p_data.DialogResult == true)
            {
                isRevised = p_data.IsRevised;
            }
            else
            {
                isRevised = p_data.IsRevised;
            }

            PAP = "";
            DataTable dtFundSource = c_ppmp.LoadFundSource(DivId, WorkingYear, isRevised, p_data.FundSourceId, p_data.FundType).Tables[0].Copy();
            if (dtFundSource.Rows.Count != 0)
            {
                foreach (DataRow item in dtFundSource.Rows)
                {
                    if (item["respo_name"].ToString().Contains("CONT"))
                    {
                        continue;
                    }
                    else
                    {
                        FundSource = item[2].ToString();
                        break;
                    }
                }

            }
            if (FundSource == "")
            {
                FundSource = p_data.FundSourceId;
            }
            //DataTable dtPPMPData = c_ppmp.FetchData(DivId, WorkingYear, isRevised, FundSource).Tables[0].Copy();
            Pap_Code = Regex.Replace(PAPCode, @"[^\d]", "");
            DataTable dtPPMPData = c_ppmp.FetchData(DivId, WorkingYear, isRevised, FundSource, Pap_Code).Tables[0].Copy();

            DataTable dtDivProjectsData = c_ppmp.GetDivisionProjects(DivId, WorkingYear, isRevised, FundSource).Tables[0].Copy();

            DataTable dtDivActivitiesData = c_ppmp.FetchDivisionActivities(DivId, WorkingYear, Pap_Code).Tables[0].Copy();

            DataTable dtGeneralCategory = c_ppmp.FetchGeneralCategory().Tables[0].Copy();

            DataTable dtSubCategory = c_ppmp.FetchSubCategory().Tables[0].Copy();

            DataTable dtDistinctSupplies = c_ppmp.FetchDistinctSupplies(WorkingYear, FundSource).Tables[0].Copy();

            if (chkCSE.IsChecked == true)
            {
                dtPPMPData.DefaultView.RowFilter = "uacs_code ='50203011003' or uacs_code ='5020301000' or uacs_code='5020399000' or uacs_code='5023990001' or uacs_code ='50203011002'";
                dtFundSource.DefaultView.RowFilter = "mooe_id ='50203011003' or mooe_id ='5020301000' or mooe_id='5020399000' or mooe_id = '5023990001' or mooe_id ='50203011002'";
                dtFundSource = dtFundSource.DefaultView.ToTable().Copy();

                dtPPMPData = dtPPMPData.DefaultView.ToTable().Copy();
                PAP = dtFundSource.Rows[0]["pap"].ToString();
            }
            else
            {
                PAP = dtFundSource.Rows[0]["pap"].ToString();
                dtPPMPData.DefaultView.RowFilter = "NOT uacs_code ='5020301000'";
                dtPPMPData = dtPPMPData.DefaultView.ToTable().Copy();
                dtPPMPData.DefaultView.RowFilter = "NOT uacs_code ='5020399000'";
                dtPPMPData = dtPPMPData.DefaultView.ToTable().Copy();
                dtPPMPData.DefaultView.RowFilter = "NOT uacs_code ='5023990001'";
                dtPPMPData = dtPPMPData.DefaultView.ToTable().Copy();
                dtPPMPData.DefaultView.RowFilter = "NOT uacs_code ='50203011002'";
                dtPPMPData = dtPPMPData.DefaultView.ToTable().Copy();
                dtPPMPData.DefaultView.RowFilter = "NOT uacs_code ='50203011003'";
                dtPPMPData = dtPPMPData.DefaultView.ToTable().Copy();

                dtFundSource.DefaultView.RowFilter = "NOT mooe_id ='50203011003'";
                dtFundSource = dtFundSource.DefaultView.ToTable().Copy();
                dtFundSource.DefaultView.RowFilter = "NOT mooe_id ='5020301000'";
                dtFundSource = dtFundSource.DefaultView.ToTable().Copy();
                dtFundSource.DefaultView.RowFilter = "NOT mooe_id ='5020399000'";
                dtFundSource = dtFundSource.DefaultView.ToTable().Copy();
                dtFundSource.DefaultView.RowFilter = "NOT mooe_id ='5023990001'";
                dtFundSource = dtFundSource.DefaultView.ToTable().Copy();
                dtFundSource.DefaultView.RowFilter = "NOT mooe_id ='50203011002'";
                dtFundSource = dtFundSource.DefaultView.ToTable().Copy();
            }
            //dtPPMPData = dtPPMPData.DefaultView.ToTable().Copy();
            DataTable dtData = new DataTable();
            DataTable dtRevisions = new DataTable();
            if (p_data.IsCONT == false)
            {
                dtFundSource.DefaultView.RowFilter = "NOT respo_name LIKE '*CONT*'";
                dtData = dtFundSource.DefaultView.ToTable().Copy();
                dtData.DefaultView.RowFilter = "is_revision=1";

                dtRevisions = dtData.DefaultView.ToTable().Copy();
                dtData.DefaultView.RowFilter = "is_revision=0";
                dtData = dtData.DefaultView.ToTable().Copy();
            }
            else
            {

                dtFundSource.DefaultView.RowFilter = "respo_name LIKE '*CONT*'";
                dtData = dtFundSource.DefaultView.ToTable().Copy();
                dtData.DefaultView.RowFilter = "is_revision=1";

                dtRevisions = dtData.DefaultView.ToTable().Copy();
                dtData.DefaultView.RowFilter = "is_revision=0";
                dtData = dtData.DefaultView.ToTable().Copy();
            }


            foreach (DataRow item in dtData.Rows)
            {
                double _total = 0.00;
                dtRevisions.DefaultView.RowFilter = "mooe_id ='" + item["mooe_id"] + "'";
                if (dtRevisions.DefaultView.ToTable().Rows.Count != 0)
                {
                    double _rowAmount = 0.00;
                    double _origAmount = 0.00;

                    foreach (DataRow itemRow in dtRevisions.DefaultView.ToTable().Rows)
                    {
                        _origAmount = Convert.ToDouble(item["Amount"].ToString());
                        _rowAmount = Convert.ToDouble(itemRow["Amount"].ToString());
                        if (itemRow["plusminus"].ToString() == "+")
                        {


                            item["Amount"] = (_rowAmount + _origAmount).ToString();
                        }
                        else
                        {
                            item["Amount"] = (_rowAmount - _origAmount).ToString();
                        }

                    }

                }


            }
            foreach (DataRow itemRev in dtRevisions.Rows)
            {
                dtData.DefaultView.RowFilter = "mooe_id = '" + itemRev["mooe_id"].ToString() + "'";
                if (dtData.DefaultView.ToTable().Rows.Count == 0)
                {
                    DataRow dr = dtData.NewRow();
                    dr[0] = itemRev.ItemArray[0].ToString();
                    dr[1] = itemRev.ItemArray[1].ToString();
                    dr[2] = itemRev.ItemArray[2].ToString();
                    dr[3] = itemRev.ItemArray[3].ToString();
                    dr[4] = itemRev.ItemArray[4].ToString();
                    dr[5] = itemRev.ItemArray[5].ToString();
                    dr[6] = itemRev.ItemArray[6].ToString();
                    dr[7] = itemRev.ItemArray[7].ToString();
                    dr[8] = itemRev.ItemArray[8].ToString();
                    dr[9] = itemRev.ItemArray[9].ToString();
                    dr[10] = itemRev.ItemArray[10].ToString();
                    dtData.Rows.Add(dr);
                    continue;
                }
            }
            this.IsCONT = p_data.IsCONT;

            if (clicked == "btnPPMPPrintout")
            {
                GenerateData(dtData, dtPPMPData, p_data.IsCONT);
            }
            else if (clicked == "btnPPMPCSE")
            {
                GenerateDataForPPMPCSE(dtData, dtPPMPData, p_data.IsCONT, dtGeneralCategory, dtSubCategory, dtDistinctSupplies);
            }
            else if (clicked == "btnPPMPPrintoutPerExpenseItem")
            {
                GenerateDataForPPMPPerExpenseItem(dtData, dtPPMPData, p_data.IsCONT);
            }
            else { GenerateDataForPPMP(dtDivProjectsData, dtPPMPData, p_data.IsCONT, dtDivActivitiesData, DivId); }
        }
        private Boolean IsCONT = false;
        private DataTable dtFinal = new DataTable();
        private DataTable dtFinal2 = new DataTable();
        private DataTable dtFinal3 = new DataTable();
        private DataTable dtReport = new DataTable();

        //old PPMP
        private void GenerateData(DataTable _FundSource, DataTable _PPMPData, Boolean IsCONT)
        {

            double _Totals = 0.00;
            double _OverAll = 0.00;

            dtFinal = new DataTable();

            //if (chkCSE.IsChecked == true)
            //{
            //    dtFinal.Columns.Add("ACTID");
            //    dtFinal.Columns.Add("UACS");
            //    dtFinal.Columns.Add("Description");
            //    dtFinal.Columns.Add("Quantity_Size");
            //    dtFinal.Columns.Add("EstimatedBudget");
            //    dtFinal.Columns.Add("ModeOfProcurement");

            //    dtFinal.Columns.Add("_Pap");
            //    dtFinal.Columns.Add("Jan");
            //    dtFinal.Columns.Add("Feb");
            //    dtFinal.Columns.Add("Mar");
            //    dtFinal.Columns.Add("Q1");
            //    dtFinal.Columns.Add("Apr");
            //    dtFinal.Columns.Add("May");
            //    dtFinal.Columns.Add("Jun");
            //    dtFinal.Columns.Add("Q2");
            //    dtFinal.Columns.Add("Jul");
            //    dtFinal.Columns.Add("Aug");
            //    dtFinal.Columns.Add("Sep");
            //    dtFinal.Columns.Add("Q3");
            //    dtFinal.Columns.Add("Oct");
            //    dtFinal.Columns.Add("Nov");
            //    dtFinal.Columns.Add("Dec");
            //    dtFinal.Columns.Add("Q4");
            //    dtFinal.Columns.Add("Total Quantity");
            //    dtFinal.Columns.Add("Price Catalogue");
            //    dtFinal.Columns.Add("Total Amount");

            //    dtFinal.Columns.Add("_Total");
            //    dtFinal.Columns.Add("_Year");
            //    dtFinal.Columns.Add("_Division");
            //    dtFinal.TableName = "PPMP";
            //}
            //else
            //{
            dtFinal.Columns.Add("ACTID");
            dtFinal.Columns.Add("UACS");
            dtFinal.Columns.Add("Description");
            dtFinal.Columns.Add("Quantity_Size");
            dtFinal.Columns.Add("EstimatedBudget");
            dtFinal.Columns.Add("ModeOfProcurement");

            dtFinal.Columns.Add("_Pap");
            dtFinal.Columns.Add("Jan");
            dtFinal.Columns.Add("Feb");
            dtFinal.Columns.Add("Mar");
            dtFinal.Columns.Add("Apr");
            dtFinal.Columns.Add("May");
            dtFinal.Columns.Add("Jun");
            dtFinal.Columns.Add("Jul");
            dtFinal.Columns.Add("Aug");
            dtFinal.Columns.Add("Sep");
            dtFinal.Columns.Add("Oct");
            dtFinal.Columns.Add("Nov");
            dtFinal.Columns.Add("Dec");

            dtFinal.Columns.Add("_Total");
            dtFinal.Columns.Add("_Year");
            dtFinal.Columns.Add("_Division");
            dtFinal.TableName = "PPMP";
            //}
            String _PAPCode = "";
            String _ActId = "";
            foreach (DataRow item in _FundSource.Rows)
            {
                double _Allocation = Convert.ToDouble(item["Amount"].ToString());
                DataRow dr = dtFinal.NewRow();

                _PPMPData.DefaultView.RowFilter = "uacs_code ='" + item["mooe_id"].ToString() + "'";
                try
                {
                    _PAPCode = _PPMPData.DefaultView.ToTable().Rows[0]["Division_Code"].ToString();

                }
                catch (Exception)
                {

                }

                dr["ACTID"] = "";
                dr["UACS"] = item["mooe_id"].ToString();
                dr["Description"] = item["name"].ToString();
                dr["ModeOfProcurement"] = "";
                dr["Quantity_Size"] = "";
                dr["_Year"] = WorkingYear;

                dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                dr["_Pap"] = _PAPCode;
                dr["Jan"] = "";
                dr["Feb"] = "";
                dr["Mar"] = "";
                dr["Apr"] = "";
                dr["May"] = "";
                dr["Jun"] = "";
                dr["Jul"] = "";
                dr["Aug"] = "";
                dr["Sep"] = "";
                dr["Oct"] = "";
                dr["Nov"] = "";
                dr["Dec"] = "";
                dr["EstimatedBudget"] = "";
                dr["_Total"] = "0.00";

                dtFinal.Rows.Add(dr);
                _Totals = 0.00;
                double totals = 0.00;
                double total_for_contigency = 0.00;


                if (_PPMPData.DefaultView.ToTable().Rows.Count != 0)
                {


                    DataTable x_data = _PPMPData.DefaultView.ToTable().Copy();
                    foreach (DataRow itemRows in x_data.Rows)
                    {

                        _PAPCode = itemRows["Division_Code"].ToString();
                        dr = dtFinal.NewRow();

                        dr["ACTID"] = itemRows["id"].ToString();
                        dr["UACS"] = "";
                        dr["Description"] = itemRows["type_service"].ToString();
                        dr["ModeOfProcurement"] = "";
                        dr["Quantity_Size"] = itemRows["quantity"].ToString();
                        dr["_Year"] = WorkingYear;
                        dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                        dr["_Pap"] = _PAPCode;


                        switch (itemRows["month"].ToString())
                        {
                            case "Jan":
                                dr["Jan"] = "X";
                                _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                                break;
                            case "Feb":
                                dr["Feb"] = "X";
                                _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                                break;
                            case "Mar":
                                dr["Mar"] = "X";
                                _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                                break;
                            case "Apr":
                                dr["Apr"] = "X";
                                _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                                break;
                            case "May":
                                dr["May"] = "X";
                                _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                                break;
                            case "Jun":
                                dr["Jun"] = "X";
                                _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                                break;
                            case "Jul":
                                dr["Jul"] = "X";
                                _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                                break;

                            case "Aug":
                                dr["Aug"] = "X";
                                _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                                break;
                            case "Sep":
                                dr["Sep"] = "X";
                                _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                                break;
                            case "Oct":
                                dr["Oct"] = "X";
                                _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                                break;
                            case "Nov":
                                dr["Nov"] = "X";
                                _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                                break;
                            case "Dec":
                                dr["Dec"] = "X";
                                _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                                break;
                        }



                        dr["EstimatedBudget"] = Convert.ToDouble(itemRows["rate"].ToString()).ToString("#,##0.00");
                        dr["_Total"] = 0.ToString("#,##0.00");
                        total_for_contigency += Convert.ToDouble(itemRows["rate"].ToString());

                        dtFinal.Rows.Add(dr);
                    }



                    dr = dtFinal.NewRow();

                    dr["ACTID"] = "";
                    dr["UACS"] = "";
                    dr["Description"] = "Contingency";
                    dr["ModeOfProcurement"] = "";
                    dr["Quantity_Size"] = "";
                    dr["_Year"] = WorkingYear;
                    dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                    dr["_Pap"] = _PAPCode;
                    dr["Jan"] = "";
                    dr["Feb"] = "";
                    dr["Mar"] = "";
                    dr["Apr"] = "";
                    dr["May"] = "";
                    dr["Jun"] = "";
                    dr["Jul"] = "";
                    dr["Aug"] = "";
                    dr["Sep"] = "";
                    dr["Oct"] = "";
                    dr["Nov"] = "";
                    dr["Dec"] = "";
                    dr["EstimatedBudget"] = (_Allocation - total_for_contigency).ToString("#,##0.00");
                    dr["_Total"] = "0.00";

                    dtFinal.Rows.Add(dr);
                    dr = dtFinal.NewRow();

                    dr["ACTID"] = "";
                    dr["UACS"] = "";
                    dr["Description"] = "Total :" + (total_for_contigency + (_Allocation - total_for_contigency)).ToString("#,##0.00"); ;
                    dr["ModeOfProcurement"] = "";
                    dr["Quantity_Size"] = "";
                    dr["_Year"] = WorkingYear;
                    dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                    dr["_Pap"] = _PAPCode;
                    dr["Jan"] = "";
                    dr["Feb"] = "";
                    dr["Mar"] = "";
                    dr["Apr"] = "";
                    dr["May"] = "";
                    dr["Jun"] = "";
                    dr["Jul"] = "";
                    dr["Aug"] = "";
                    dr["Sep"] = "";
                    dr["Oct"] = "";
                    dr["Nov"] = "";
                    dr["Dec"] = "";
                    dr["EstimatedBudget"] = "";
                    dr["_Total"] = "0.00";

                    dtFinal.Rows.Add(dr);




                }
                else
                {
                    dr = dtFinal.NewRow();

                    dr["ACTID"] = "";
                    dr["UACS"] = "";
                    dr["Description"] = "Contingency";
                    dr["ModeOfProcurement"] = "";
                    dr["Quantity_Size"] = "";
                    dr["_Year"] = WorkingYear;
                    dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                    dr["_Pap"] = _PAPCode;
                    dr["Jan"] = "";
                    dr["Feb"] = "";
                    dr["Mar"] = "";
                    dr["Apr"] = "";
                    dr["May"] = "";
                    dr["Jun"] = "";
                    dr["Jul"] = "";
                    dr["Aug"] = "";
                    dr["Sep"] = "";
                    dr["Oct"] = "";
                    dr["Nov"] = "";
                    dr["Dec"] = "";
                    dr["EstimatedBudget"] = (_Allocation).ToString("#,##0.00");
                    dr["_Total"] = (_Allocation).ToString("#,##0.00");

                    dtFinal.Rows.Add(dr);
                    dr = dtFinal.NewRow();

                    dr["ACTID"] = "";
                    dr["UACS"] = "";
                    dr["Description"] = "Total :" + (_Allocation).ToString("#,##0.00"); ;
                    dr["ModeOfProcurement"] = "";
                    dr["Quantity_Size"] = "";
                    dr["_Year"] = WorkingYear;
                    dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                    dr["_Pap"] = _PAPCode;
                    dr["Jan"] = "";
                    dr["Feb"] = "";
                    dr["Mar"] = "";
                    dr["Apr"] = "";
                    dr["May"] = "";
                    dr["Jun"] = "";
                    dr["Jul"] = "";
                    dr["Aug"] = "";
                    dr["Sep"] = "";
                    dr["Oct"] = "";
                    dr["Nov"] = "";
                    dr["Dec"] = "";
                    dr["EstimatedBudget"] = "";
                    dr["_Total"] = "0.00";

                    dtFinal.Rows.Add(dr);
                }



            }
            Boolean NotPassed = false;
            for (int i = 0; i < dtFinal.Rows.Count; i++)
            {
                String _checkval = dtFinal.Rows[i]["EstimatedBudget"].ToString();
                if (_checkval == "")
                {
                    _checkval = "0";
                }

                double _val = Convert.ToDouble(_checkval);
                if (_val < 0)
                {
                    NotPassed = true;
                    break;
                }
            }
            if (NotPassed)
            {
                MessageBox.Show("Please settle first your PPMP Data." + Environment.NewLine + "You have a negative amount on your activities");
                btnPrintPreview.IsEnabled = false;
            }
            else
            {
                btnPrintPreview.IsEnabled = true;
            }

            grdData.DataSource = null;
            grdData.DataSource = dtFinal.DefaultView;

            grdData.FieldLayouts[0].Fields["ACTID"].Visibility = Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["_Year"].Visibility = Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["_Pap"].Visibility = Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["_Division"].Visibility = Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["_Total"].Visibility = Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["ModeOfProcurement"].Visibility = Visibility.Visible;


            grdData.FieldLayouts[0].Fields["UACS"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData.FieldLayouts[0].Fields["Description"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData.FieldLayouts[0].Fields["Quantity_Size"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData.FieldLayouts[0].Fields["EstimatedBudget"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData.FieldLayouts[0].Fields["ModeOfProcurement"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;

            grdData.FieldLayouts[0].Fields["UACS"].Label = "UACS Code";
            grdData.FieldLayouts[0].Fields["Description"].Label = "Description";
            grdData.FieldLayouts[0].Fields["Quantity_Size"].Label = "Quantity";
            grdData.FieldLayouts[0].Fields["EstimatedBudget"].Label = "Estimated Budget";
            grdData.FieldLayouts[0].Fields["ModeOfProcurement"].Label = "Mode of Procurement";

            grdData.FieldLayouts[0].Fields["UACS"].Width = new FieldLength(200);
            grdData.FieldLayouts[0].Fields["Description"].Width = new FieldLength(600);
            grdData.FieldLayouts[0].Fields["Quantity_Size"].Width = new FieldLength(175);
            grdData.FieldLayouts[0].Fields["EstimatedBudget"].Width = new FieldLength(250);

            //grdData.FieldLayouts[0].Fields["UACS"].PerformAutoSize();
            //grdData.FieldLayouts[0].Fields["Description"].PerformAutoSize();
            //grdData.FieldLayouts[0].Fields["Quantity_Size"].PerformAutoSize();
            //grdData.FieldLayouts[0].Fields["EstimatedBudget"].PerformAutoSize();
            //grdData.FieldLayouts[0].Fields["ModeOfProcurement"].PerformAutoSize();
        }

        private void GenerateDataForPPMPPerExpenseItem(DataTable _FundSource, DataTable _PPMPData, Boolean IsCONT)
        {

            double _Totals = 0.00;
            double _OverAll = 0.00;

            dtFinal = new DataTable();

            //if (chkCSE.IsChecked == true)
            //{
            //    dtFinal.Columns.Add("ACTID");
            //    dtFinal.Columns.Add("UACS");
            //    dtFinal.Columns.Add("Description");
            //    dtFinal.Columns.Add("Quantity_Size");
            //    dtFinal.Columns.Add("EstimatedBudget");
            //    dtFinal.Columns.Add("ModeOfProcurement");

            //    dtFinal.Columns.Add("_Pap");
            //    dtFinal.Columns.Add("Jan");
            //    dtFinal.Columns.Add("Feb");
            //    dtFinal.Columns.Add("Mar");
            //    dtFinal.Columns.Add("Q1");
            //    dtFinal.Columns.Add("Apr");
            //    dtFinal.Columns.Add("May");
            //    dtFinal.Columns.Add("Jun");
            //    dtFinal.Columns.Add("Q2");
            //    dtFinal.Columns.Add("Jul");
            //    dtFinal.Columns.Add("Aug");
            //    dtFinal.Columns.Add("Sep");
            //    dtFinal.Columns.Add("Q3");
            //    dtFinal.Columns.Add("Oct");
            //    dtFinal.Columns.Add("Nov");
            //    dtFinal.Columns.Add("Dec");
            //    dtFinal.Columns.Add("Q4");
            //    dtFinal.Columns.Add("Total Quantity");
            //    dtFinal.Columns.Add("Price Catalogue");
            //    dtFinal.Columns.Add("Total Amount");

            //    dtFinal.Columns.Add("_Total");
            //    dtFinal.Columns.Add("_Year");
            //    dtFinal.Columns.Add("_Division");
            //    dtFinal.TableName = "PPMP";
            //}
            //else
            //{
            dtFinal.Columns.Add("ACTID");
            dtFinal.Columns.Add("UACS");
            dtFinal.Columns.Add("Description");
            dtFinal.Columns.Add("Quantity_Size");
            dtFinal.Columns.Add("EstimatedBudget");
            dtFinal.Columns.Add("ModeOfProcurement");

            dtFinal.Columns.Add("_Pap");
            dtFinal.Columns.Add("Jan");
            dtFinal.Columns.Add("Feb");
            dtFinal.Columns.Add("Mar");
            dtFinal.Columns.Add("Apr");
            dtFinal.Columns.Add("May");
            dtFinal.Columns.Add("Jun");
            dtFinal.Columns.Add("Jul");
            dtFinal.Columns.Add("Aug");
            dtFinal.Columns.Add("Sep");
            dtFinal.Columns.Add("Oct");
            dtFinal.Columns.Add("Nov");
            dtFinal.Columns.Add("Dec");

            dtFinal.Columns.Add("_Total");
            dtFinal.Columns.Add("_Year");
            dtFinal.Columns.Add("_Division");
            dtFinal.TableName = "PPMP";
            //}
            String _PAPCode = "";
            String _ActId = "";
            foreach (DataRow item in _FundSource.Rows)
            {
                double _Allocation = Convert.ToDouble(item["Amount"].ToString());
                DataRow dr = dtFinal.NewRow();

                _PPMPData.DefaultView.RowFilter = "uacs_code ='" + item["mooe_id"].ToString() + "'";
                try
                {
                    _PAPCode = _PPMPData.DefaultView.ToTable().Rows[0]["Division_Code"].ToString();

                }
                catch (Exception)
                {

                }

                dr["ACTID"] = "";
                dr["UACS"] = item["mooe_id"].ToString();
                dr["Description"] = item["name"].ToString();
                dr["ModeOfProcurement"] = "";
                dr["Quantity_Size"] = "";
                dr["_Year"] = WorkingYear;

                dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                dr["_Pap"] = _PAPCode;

                //dtFinal.Rows.Add(dr);
                _Totals = 0.00;
                double totals = 0.00;
                double total_for_contigency = 0.00;
                double Jan = 0.00;
                double Feb = 0.00;
                double Mar = 0.00;
                double Apr = 0.00;
                double May = 0.00;
                double Jun = 0.00;
                double Jul = 0.00;
                double Aug = 0.00;
                double Sep = 0.00;
                double Oct = 0.00;
                double Nov = 0.00;
                double Dec = 0.00;
                int quantity = 0;


                if (_PPMPData.DefaultView.ToTable().Rows.Count != 0)
                {
                    
                    //dr = dtFinal.NewRow();

                    DataTable x_data = _PPMPData.DefaultView.ToTable().Copy();
                    foreach (DataRow itemRows in x_data.Rows)
                    {

                        _PAPCode = itemRows["Division_Code"].ToString();

                        //dr["ACTID"] = itemRows["id"].ToString();
                        //dr["UACS"] = "";
                        //dr["Description"] = itemRows["type_service"].ToString();
                        //dr["ModeOfProcurement"] = "";
                        //dr["Quantity_Size"] = itemRows["quantity"].ToString();
                        quantity += Convert.ToInt32(itemRows["quantity"].ToString());
                        totals += Convert.ToDouble(itemRows["rate"].ToString());
                        //dr["_Year"] = WorkingYear;
                        //dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                        //dr["_Pap"] = _PAPCode;

                        switch (itemRows["month"].ToString())
                        {
                            case "Jan":
                                Jan += Convert.ToDouble(itemRows["rate"].ToString());
                                break;
                            case "Feb":
                                Feb += Convert.ToDouble(itemRows["rate"].ToString());
                                break;
                            case "Mar":
                                Mar += Convert.ToDouble(itemRows["rate"].ToString());
                                break;
                            case "Apr":
                                Apr += Convert.ToDouble(itemRows["rate"].ToString());
                                break;
                            case "May":
                                May += Convert.ToDouble(itemRows["rate"].ToString());
                                break;
                            case "Jun":
                                Jun += Convert.ToDouble(itemRows["rate"].ToString());
                                break;
                            case "Jul":
                                Jul += Convert.ToDouble(itemRows["rate"].ToString());
                                break;
                            case "Aug":
                                Aug += Convert.ToDouble(itemRows["rate"].ToString());
                                break;
                            case "Sep":
                                Sep += Convert.ToDouble(itemRows["rate"].ToString());
                                break;
                            case "Oct":
                                Oct += Convert.ToDouble(itemRows["rate"].ToString());
                                break;
                            case "Nov":
                                Nov += Convert.ToDouble(itemRows["rate"].ToString());
                                break;
                            case "Dec":
                                Dec += Convert.ToDouble(itemRows["rate"].ToString());
                                break;
                        }

                        dr["Quantity_Size"] = quantity;
                        dr["Jan"] = (Jan == 0) ? " " : Jan.ToString("#,##0.00");
                        dr["Feb"] = (Feb == 0) ? " " : Feb.ToString("#,##0.00");
                        dr["Mar"] = (Mar == 0) ? " " : Mar.ToString("#,##0.00");
                        dr["Apr"] = (Apr == 0) ? " " : Apr.ToString("#,##0.00");
                        dr["May"] = (May == 0) ? " " : May.ToString("#,##0.00");
                        dr["Jun"] = (Jun == 0) ? " " : Jun.ToString("#,##0.00");
                        dr["Jul"] = (Jul == 0) ? " " : Jul.ToString("#,##0.00");
                        dr["Aug"] = (Aug == 0) ? " " : Aug.ToString("#,##0.00");
                        dr["Sep"] = (Sep == 0) ? " " : Sep.ToString("#,##0.00");
                        dr["Oct"] = (Oct == 0) ? " " : Oct.ToString("#,##0.00");
                        dr["Nov"] = (Nov == 0) ? " " : Nov.ToString("#,##0.00");
                        dr["Dec"] = (Dec == 0) ? " " : Dec.ToString("#,##0.00");
                        //switch (itemRows["month"].ToString())
                        //{
                        //    case "Jan":
                        //        dr["Jan"] = "X";
                        //        _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                        //        break;
                        //    case "Feb":
                        //        dr["Feb"] = "X";
                        //        _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                        //        break;
                        //    case "Mar":
                        //        dr["Mar"] = "X";
                        //        _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                        //        break;
                        //    case "Apr":
                        //        dr["Apr"] = "X";
                        //        _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                        //        break;
                        //    case "May":
                        //        dr["May"] = "X";
                        //        _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                        //        break;
                        //    case "Jun":
                        //        dr["Jun"] = "X";
                        //        _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                        //        break;
                        //    case "Jul":
                        //        dr["Jul"] = "X";
                        //        _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                        //        break;

                        //    case "Aug":
                        //        dr["Aug"] = "X";
                        //        _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                        //        break;
                        //    case "Sep":
                        //        dr["Sep"] = "X";
                        //        _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                        //        break;
                        //    case "Oct":
                        //        dr["Oct"] = "X";
                        //        _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                        //        break;
                        //    case "Nov":
                        //        dr["Nov"] = "X";
                        //        _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                        //        break;
                        //    case "Dec":
                        //        dr["Dec"] = "X";
                        //        _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                        //        break;
                        //}



                        dr["EstimatedBudget"] = totals.ToString("#,##0.00");
                        dr["_Total"] = 0.ToString("#,##0.00");
                        total_for_contigency += Convert.ToDouble(itemRows["rate"].ToString());

                        
                    }

                    dtFinal.Rows.Add(dr);

                    //dr = dtFinal.NewRow();

                    //dr["ACTID"] = "";
                    //dr["UACS"] = "";
                    //dr["Description"] = "Contingency";
                    //dr["ModeOfProcurement"] = "";
                    //dr["Quantity_Size"] = "";
                    //dr["_Year"] = WorkingYear;
                    //dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                    //dr["_Pap"] = _PAPCode;
                    //dr["Jan"] = "";
                    //dr["Feb"] = "";
                    //dr["Mar"] = "";
                    //dr["Apr"] = "";
                    //dr["May"] = "";
                    //dr["Jun"] = "";
                    //dr["Jul"] = "";
                    //dr["Aug"] = "";
                    //dr["Sep"] = "";
                    //dr["Oct"] = "";
                    //dr["Nov"] = "";
                    //dr["Dec"] = "";
                    //dr["EstimatedBudget"] = (_Allocation - total_for_contigency).ToString("#,##0.00");
                    //dr["_Total"] = "0.00";

                    //dtFinal.Rows.Add(dr);
                    //dr = dtFinal.NewRow();

                    //dr["ACTID"] = "";
                    //dr["UACS"] = "";
                    //dr["Description"] = "Total :" + (total_for_contigency + (_Allocation - total_for_contigency)).ToString("#,##0.00"); ;
                    //dr["ModeOfProcurement"] = "";
                    //dr["Quantity_Size"] = "";
                    //dr["_Year"] = WorkingYear;
                    //dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                    //dr["_Pap"] = _PAPCode;
                    //dr["Jan"] = "";
                    //dr["Feb"] = "";
                    //dr["Mar"] = "";
                    //dr["Apr"] = "";
                    //dr["May"] = "";
                    //dr["Jun"] = "";
                    //dr["Jul"] = "";
                    //dr["Aug"] = "";
                    //dr["Sep"] = "";
                    //dr["Oct"] = "";
                    //dr["Nov"] = "";
                    //dr["Dec"] = "";
                    //dr["EstimatedBudget"] = "";
                    //dr["_Total"] = "0.00";

                    //dtFinal.Rows.Add(dr);




                }
                else
                {
                    //dr = dtFinal.NewRow();

                    //dr["ACTID"] = "";
                    //dr["UACS"] = "";
                    //dr["Description"] = "Contingency";
                    //dr["ModeOfProcurement"] = "";
                    //dr["Quantity_Size"] = "";
                    //dr["_Year"] = WorkingYear;
                    //dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                    //dr["_Pap"] = _PAPCode;
                    //dr["Jan"] = "";
                    //dr["Feb"] = "";
                    //dr["Mar"] = "";
                    //dr["Apr"] = "";
                    //dr["May"] = "";
                    //dr["Jun"] = "";
                    //dr["Jul"] = "";
                    //dr["Aug"] = "";
                    //dr["Sep"] = "";
                    //dr["Oct"] = "";
                    //dr["Nov"] = "";
                    //dr["Dec"] = "";
                    //dr["EstimatedBudget"] = (_Allocation).ToString("#,##0.00");
                    //dr["_Total"] = (_Allocation).ToString("#,##0.00");

                    //dtFinal.Rows.Add(dr);
                    //dr = dtFinal.NewRow();

                    //dr["ACTID"] = "";
                    //dr["UACS"] = "";
                    //dr["Description"] = "Total :" + (_Allocation).ToString("#,##0.00"); ;
                    //dr["ModeOfProcurement"] = "";
                    //dr["Quantity_Size"] = "";
                    //dr["_Year"] = WorkingYear;
                    //dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                    //dr["_Pap"] = _PAPCode;
                    //dr["Jan"] = "";
                    //dr["Feb"] = "";
                    //dr["Mar"] = "";
                    //dr["Apr"] = "";
                    //dr["May"] = "";
                    //dr["Jun"] = "";
                    //dr["Jul"] = "";
                    //dr["Aug"] = "";
                    //dr["Sep"] = "";
                    //dr["Oct"] = "";
                    //dr["Nov"] = "";
                    //dr["Dec"] = "";
                    //dr["EstimatedBudget"] = "";
                    //dr["_Total"] = "0.00";

                    //dtFinal.Rows.Add(dr);
                }



            }
            Boolean NotPassed = false;
            //for (int i = 0; i < dtFinal.Rows.Count; i++)
            //{
            //    String _checkval = dtFinal.Rows[i]["EstimatedBudget"].ToString();
            //    if (_checkval == "")
            //    {
            //        _checkval = "0";
            //    }

            //    double _val = Convert.ToDouble(_checkval);
            //    if (_val < 0)
            //    {
            //        NotPassed = true;
            //        break;
            //    }
            //}
            //if (NotPassed)
            //{
            //    MessageBox.Show("Please settle first your PPMP Data." + Environment.NewLine + "You have a negative amount on your activities");
            //    btnPrintPreview.IsEnabled = false;
            //}
            //else
            //{
            //    btnPrintPreview.IsEnabled = true;
            //}

            grdData.DataSource = null;
            grdData.DataSource = dtFinal.DefaultView;

            grdData.FieldLayouts[0].Fields["ACTID"].Visibility = Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["_Year"].Visibility = Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["_Pap"].Visibility = Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["_Division"].Visibility = Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["_Total"].Visibility = Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["ModeOfProcurement"].Visibility = Visibility.Visible;


            grdData.FieldLayouts[0].Fields["UACS"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData.FieldLayouts[0].Fields["Description"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData.FieldLayouts[0].Fields["Quantity_Size"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData.FieldLayouts[0].Fields["EstimatedBudget"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData.FieldLayouts[0].Fields["ModeOfProcurement"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;

            grdData.FieldLayouts[0].Fields["UACS"].Label = "UACS Code";
            grdData.FieldLayouts[0].Fields["Description"].Label = "Description";
            grdData.FieldLayouts[0].Fields["Quantity_Size"].Label = "Quantity";
            grdData.FieldLayouts[0].Fields["EstimatedBudget"].Label = "Estimated Budget";
            grdData.FieldLayouts[0].Fields["ModeOfProcurement"].Label = "Mode of Procurement";

            grdData.FieldLayouts[0].Fields["UACS"].Width = new FieldLength(200);
            grdData.FieldLayouts[0].Fields["Description"].Width = new FieldLength(600);
            grdData.FieldLayouts[0].Fields["Quantity_Size"].Width = new FieldLength(175);
            grdData.FieldLayouts[0].Fields["EstimatedBudget"].Width = new FieldLength(250);

            //grdData.FieldLayouts[0].Fields["UACS"].PerformAutoSize();
            //grdData.FieldLayouts[0].Fields["Description"].PerformAutoSize();
            //grdData.FieldLayouts[0].Fields["Quantity_Size"].PerformAutoSize();
            //grdData.FieldLayouts[0].Fields["EstimatedBudget"].PerformAutoSize();
            //grdData.FieldLayouts[0].Fields["ModeOfProcurement"].PerformAutoSize();
        }

        //new PPMP
        private void GenerateDataForPPMP(DataTable _DivProjectsData, DataTable _PPMPData, Boolean IsCONT, DataTable _DivActivitiesData, String DivId)
        {

            double _Totals = 0.00;
            double _OverAll = 0.00;

            dtFinal = new DataTable();
            DataRow drow = dtFinal.NewRow();


            dtFinal.Columns.Add("ACTID");
            dtFinal.Columns.Add("UACS");
            dtFinal.Columns.Add("Description");
            dtFinal.Columns.Add("Quantity_Size");
            dtFinal.Columns.Add("EstimatedBudget");
            dtFinal.Columns.Add("ModeOfProcurement");

            dtFinal.Columns.Add("_Pap");
            dtFinal.Columns.Add("Jan");
            dtFinal.Columns.Add("Feb");
            dtFinal.Columns.Add("Mar");
            dtFinal.Columns.Add("Apr");
            dtFinal.Columns.Add("May");
            dtFinal.Columns.Add("Jun");
            dtFinal.Columns.Add("Jul");
            dtFinal.Columns.Add("Aug");
            dtFinal.Columns.Add("Sep");
            dtFinal.Columns.Add("Oct");
            dtFinal.Columns.Add("Nov");
            dtFinal.Columns.Add("Dec");

            dtFinal.Columns.Add("_Total");
            dtFinal.Columns.Add("_Year");
            dtFinal.Columns.Add("_Division");
            dtFinal.TableName = "PPMP";
            String _PAPCode = "";
            String _ActId = "";
            int count = 0;
            double grand_total = 0.00;
            //foreach (DataRow item in _FundSource.Rows)
            foreach (DataRow item in _DivProjectsData.Rows)
            {
                count++;
                //double _Allocation = Convert.ToDouble(item["Amount"].ToString());
                DataRow dr = dtFinal.NewRow();
                //Pap_Code = Regex.Replace(PAPCode, @"[^\d]", "");
                //_PPMPData.DefaultView.RowFilter = "project_name ='" + item["project_name"].ToString() + "' AND Division_Code = '" + Pap_Code + "'";
                _DivActivitiesData.DefaultView.RowFilter = "project_name ='" + item["project_name"].ToString() + "'";
                try
                {
                    //_PAPCode = _PPMPData.DefaultView.ToTable().Rows[0]["Division_Code"].ToString();

                }
                catch (Exception)
                {

                }

                dr["ACTID"] = "";
                //dr["UACS"] = item["Division_Code"].ToString();
                dr["UACS"] = "";
                dr["Description"] = "PAP " + count + ": " + item["project_name"].ToString();
                dr["ModeOfProcurement"] = "";
                dr["Quantity_Size"] = "";
                dr["_Year"] = WorkingYear;

                dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                dr["_Pap"] = _PAPCode;
                dr["Jan"] = "";
                dr["Feb"] = "";
                dr["Mar"] = "";
                dr["Apr"] = "";
                dr["May"] = "";
                dr["Jun"] = "";
                dr["Jul"] = "";
                dr["Aug"] = "";
                dr["Sep"] = "";
                dr["Oct"] = "";
                dr["Nov"] = "";
                dr["Dec"] = "";
                dr["EstimatedBudget"] = "";
                dr["_Total"] = "0.00";

                dtFinal.Rows.Add(dr);
                _Totals = 0.00;
                double pap_totals = 0.00;

                if (_DivActivitiesData.DefaultView.ToTable().Rows.Count != 0)
                {


                    DataTable x_data = _DivActivitiesData.DefaultView.ToTable().Copy();

                    //x_data.DefaultView.RowFilter = "project_name ='" + item["project_name"].ToString() + "' AND Division_Code = '" + Pap_Code + "'";

                    foreach (DataRow itemRows in x_data.Rows)
                    {

                        _PAPCode = itemRows["Division_Code"].ToString();
                        dr = dtFinal.NewRow();

                        //dr["ACTID"] = itemRows["id"].ToString();
                        dr["UACS"] = itemRows["Division_Code"].ToString();

                        //old
                        //dr["Description"] = itemRows["type_service"].ToString();

                        //new
                        dr["Description"] = itemRows["activity"].ToString();

                        dr["ModeOfProcurement"] = itemRows["mode_of_procurement"].ToString();
                        //dr["Quantity_Size"] = itemRows["quantity"].ToString();
                        dr["_Year"] = WorkingYear;
                        dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                        dr["_Pap"] = _PAPCode;

                        DataTable dtFetchActivityTotals = c_ppmp.FetchActivityTotals(DivId, itemRows["activity_id"].ToString(), WorkingYear, p_data.FundSourceId, itemRows["Division_Code"].ToString()).Tables[0].Copy();
                        foreach (DataRow item_Rows in dtFetchActivityTotals.Rows)
                        {
                            int quantity = 0;
                            if (Convert.ToDouble(item_Rows["Jan"].ToString()) == 0)
                            {
                                dr["Jan"] = "";
                            }
                            else
                            {
                                dr["Jan"] = String.Format("{0:#,##0.00}", item_Rows["Jan"]); quantity++;
                            }

                            if (Convert.ToDouble(item_Rows["Feb"].ToString()) == 0)
                            {
                                dr["Feb"] = "";
                            }
                            else
                            {
                                dr["Feb"] = String.Format("{0:#,##0.00}", item_Rows["Feb"]); quantity++;
                            }

                            if (Convert.ToDouble(item_Rows["Mar"].ToString()) == 0)
                            {
                                dr["Mar"] = "";
                            }
                            else
                            {
                                dr["Mar"] = String.Format("{0:#,##0.00}", item_Rows["Mar"]); quantity++;
                            }

                            if (Convert.ToDouble(item_Rows["Apr"].ToString()) == 0)
                            {
                                dr["Apr"] = "";
                            }
                            else
                            {
                                dr["Apr"] = String.Format("{0:#,##0.00}", item_Rows["Apr"]); quantity++;
                            }

                            if (Convert.ToDouble(item_Rows["May"].ToString()) == 0)
                            {
                                dr["May"] = "";
                            }
                            else
                            {
                                dr["May"] = String.Format("{0:#,##0.00}", item_Rows["May"]); quantity++;
                            }

                            if (Convert.ToDouble(item_Rows["Jun"].ToString()) == 0)
                            {
                                dr["Jun"] = "";
                            }
                            else
                            {
                                dr["Jun"] = String.Format("{0:#,##0.00}", item_Rows["Jun"]); quantity++;
                            }

                            if (Convert.ToDouble(item_Rows["Jul"].ToString()) == 0)
                            {
                                dr["Jul"] = "";
                            }
                            else
                            {
                                dr["Jul"] = String.Format("{0:#,##0.00}", item_Rows["Jul"]); quantity++;
                            }

                            if (Convert.ToDouble(item_Rows["Aug"].ToString()) == 0)
                            {
                                dr["Aug"] = "";
                            }
                            else
                            {
                                dr["Aug"] = String.Format("{0:#,##0.00}", item_Rows["Aug"]); quantity++;
                            }

                            if (Convert.ToDouble(item_Rows["Sep"].ToString()) == 0)
                            {
                                dr["Sep"] = "";
                            }
                            else
                            {
                                dr["Sep"] = String.Format("{0:#,##0.00}", item_Rows["Sep"]); quantity++;
                            }

                            if (Convert.ToDouble(item_Rows["Oct"].ToString()) == 0)
                            {
                                dr["Oct"] = "";
                            }
                            else
                            {
                                dr["Oct"] = String.Format("{0:#,##0.00}", item_Rows["Oct"]); quantity++;
                            }

                            if (Convert.ToDouble(item_Rows["Nov"].ToString()) == 0)
                            {
                                dr["Nov"] = "";
                            }
                            else
                            {
                                dr["Nov"] = String.Format("{0:#,##0.00}", item_Rows["Nov"]); quantity++;
                            }

                            if (Convert.ToDouble(item_Rows["Dec"].ToString()) == 0)
                            {
                                dr["Dec"] = "";
                            }
                            else
                            {
                                dr["Dec"] = String.Format("{0:#,##0.00}", item_Rows["Dec"]); quantity++;
                            }
                            //dr["Jan"] = Convert.ToDouble(item_Rows["Jan"].ToString()) == 0 ? "" : item_Rows["Jan"].ToString();
                            //dr["Feb"] = Convert.ToDouble(item_Rows["Feb"].ToString()) == 0 ? "" : item_Rows["Feb"].ToString(); 
                            //dr["Mar"] = Convert.ToDouble(item_Rows["Mar"].ToString()) == 0 ? "" : item_Rows["Mar"].ToString(); 
                            //dr["Apr"] = Convert.ToDouble(item_Rows["Apr"].ToString()) == 0 ? "" : item_Rows["Apr"].ToString(); 
                            //dr["May"] = Convert.ToDouble(item_Rows["May"].ToString()) == 0 ? "" : item_Rows["May"].ToString(); 
                            //dr["Jun"] = Convert.ToDouble(item_Rows["Jun"].ToString()) == 0 ? "" : item_Rows["Jun"].ToString(); 
                            //dr["Jul"] = Convert.ToDouble(item_Rows["Jul"].ToString()) == 0 ? "" : item_Rows["Jul"].ToString(); 
                            //dr["Aug"] = Convert.ToDouble(item_Rows["Aug"].ToString()) == 0 ? "" : item_Rows["Aug"].ToString(); 
                            //dr["Sep"] = Convert.ToDouble(item_Rows["Sep"].ToString()) == 0 ? "" : item_Rows["Sep"].ToString(); 
                            //dr["Oct"] = Convert.ToDouble(item_Rows["Oct"].ToString()) == 0 ? "" : item_Rows["Oct"].ToString(); 
                            //dr["Nov"] = Convert.ToDouble(item_Rows["Nov"].ToString()) == 0 ? "" : item_Rows["Nov"].ToString(); 
                            //dr["Dec"] = Convert.ToDouble(item_Rows["Dec"].ToString()) == 0 ? "" : item_Rows["Dec"].ToString(); 
                            _Totals = Convert.ToDouble(item_Rows["Jan"].ToString()) + Convert.ToDouble(item_Rows["Feb"].ToString()) + Convert.ToDouble(item_Rows["Mar"].ToString()) + Convert.ToDouble(item_Rows["Apr"].ToString())
                                + Convert.ToDouble(item_Rows["May"].ToString()) + Convert.ToDouble(item_Rows["Jun"].ToString()) + Convert.ToDouble(item_Rows["Jul"].ToString()) + Convert.ToDouble(item_Rows["Aug"].ToString())
                                + Convert.ToDouble(item_Rows["Sep"].ToString()) + Convert.ToDouble(item_Rows["Oct"].ToString()) + Convert.ToDouble(item_Rows["Nov"].ToString()) + Convert.ToDouble(item_Rows["Dec"].ToString());
                            dr["EstimatedBudget"] = _Totals.ToString("#,##0.00");
                            dr["Quantity_Size"] = quantity + " lot";
                            pap_totals += _Totals;
                            //switch (itemRows["month"].ToString())
                            //{
                            //    //old PPMP
                            //    //case "Jan":
                            //    //    dr["Jan"] = "X";
                            //    //    _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                            //    //    break;
                            //    //case "Feb":
                            //    //    dr["Feb"] = "X";
                            //    //    _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                            //    //    break;
                            //    //case "Mar":
                            //    //    dr["Mar"] = "X";
                            //    //    _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                            //    //    break;
                            //    //case "Apr":
                            //    //    dr["Apr"] = "X";
                            //    //    _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                            //    //    break;
                            //    //case "May":
                            //    //    dr["May"] = "X";
                            //    //    _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                            //    //    break;
                            //    //case "Jun":
                            //    //    dr["Jun"] = "X";
                            //    //    _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                            //    //    break;
                            //    //case "Jul":
                            //    //    dr["Jul"] = "X";
                            //    //    _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                            //    //    break;

                            //    //case "Aug":
                            //    //    dr["Aug"] = "X";
                            //    //    _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                            //    //    break;
                            //    //case "Sep":
                            //    //    dr["Sep"] = "X";
                            //    //    _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                            //    //    break;
                            //    //case "Oct":
                            //    //    dr["Oct"] = "X";
                            //    //    _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                            //    //    break;
                            //    //case "Nov":
                            //    //    dr["Nov"] = "X";
                            //    //    _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                            //    //    break;
                            //    //case "Dec":
                            //    //    dr["Dec"] = "X";
                            //    //    _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                            //    //    break;

                            //    //NEW PPMP
                            //    case "Jan":
                            //        dr["Jan"] = String.Format("{0:n}", itemRows["rate"].ToString());
                            //        _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                            //        break;
                            //    case "Feb":
                            //        dr["Feb"] = String.Format("{0:n}", itemRows["rate"].ToString());
                            //        _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                            //        break;
                            //    case "Mar":
                            //        dr["Mar"] = String.Format("{0:n}", itemRows["rate"].ToString());
                            //        _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                            //        break;
                            //    case "Apr":
                            //        dr["Apr"] = String.Format("{0:n}", itemRows["rate"].ToString());
                            //        _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                            //        break;
                            //    case "May":
                            //        dr["May"] = String.Format("{0:n}", itemRows["rate"].ToString());
                            //        _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                            //        break;
                            //    case "Jun":
                            //        dr["Jun"] = String.Format("{0:n}", itemRows["rate"].ToString());
                            //        _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                            //        break;
                            //    case "Jul":
                            //        dr["Jul"] = String.Format("{0:n}", itemRows["rate"].ToString());
                            //        _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                            //        break;

                            //    case "Aug":
                            //        dr["Aug"] = String.Format("{0:n}", itemRows["rate"].ToString());
                            //        _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                            //        break;
                            //    case "Sep":
                            //        dr["Sep"] = String.Format("{0:n}", itemRows["rate"].ToString());
                            //        _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                            //        break;
                            //    case "Oct":
                            //        dr["Oct"] = String.Format("{0:n}", itemRows["rate"].ToString());
                            //        _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                            //        break;
                            //    case "Nov":
                            //        dr["Nov"] = String.Format("{0:n}", itemRows["rate"].ToString());
                            //        _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                            //        break;
                            //    case "Dec":
                            //        dr["Dec"] = String.Format("{0:n}", itemRows["rate"].ToString());
                            //        _Totals += Convert.ToDouble(itemRows["rate"].ToString());
                            //        break;

                            //}



                            //dr["EstimatedBudget"] = Convert.ToDouble(itemRows["rate"].ToString()).ToString("#,##0.00");
                            //dr["_Total"] = 0.ToString("#,##0.00");
                            //total_for_contigency += Convert.ToDouble(itemRows["rate"].ToString());
                            dtFinal.Rows.Add(dr);
                        }
                        //dtFinal.Rows.Add(dr);
                    }



                    //dr = dtFinal.NewRow();

                    //dr["ACTID"] = "";
                    //dr["UACS"] = "";
                    //dr["Description"] = "Contingency";
                    //dr["ModeOfProcurement"] = "";
                    //dr["Quantity_Size"] = "";
                    //dr["_Year"] = WorkingYear;
                    //dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                    //dr["_Pap"] = _PAPCode;
                    //dr["Jan"] = "";
                    //dr["Feb"] = "";
                    //dr["Mar"] = "";
                    //dr["Apr"] = "";
                    //dr["May"] = "";
                    //dr["Jun"] = "";
                    //dr["Jul"] = "";
                    //dr["Aug"] = "";
                    //dr["Sep"] = "";
                    //dr["Oct"] = "";
                    //dr["Nov"] = "";
                    //dr["Dec"] = "";
                    ////dr["EstimatedBudget"] = (_Allocation - total_for_contigency).ToString("#,##0.00");
                    //dr["_Total"] = "0.00";

                    //dtFinal.Rows.Add(dr);
                    dr = dtFinal.NewRow();

                    dr["ACTID"] = "";
                    dr["UACS"] = "";
                    dr["Description"] = "Total:";// +(total_for_contigency + (_Allocation - total_for_contigency)).ToString("#,##0.00"); ;
                    dr["ModeOfProcurement"] = "";
                    dr["Quantity_Size"] = "";
                    dr["_Year"] = WorkingYear;
                    dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                    dr["_Pap"] = _PAPCode;
                    dr["Jan"] = "";
                    dr["Feb"] = "";
                    dr["Mar"] = "";
                    dr["Apr"] = "";
                    dr["May"] = "";
                    dr["Jun"] = "";
                    dr["Jul"] = "";
                    dr["Aug"] = "";
                    dr["Sep"] = "";
                    dr["Oct"] = "";
                    dr["Nov"] = "";
                    dr["Dec"] = "";
                    dr["EstimatedBudget"] = pap_totals.ToString("#,##0.00");
                    dr["_Total"] = "0.00";

                    dtFinal.Rows.Add(dr);

                    dr = dtFinal.NewRow();

                    dr["ACTID"] = "";
                    dr["UACS"] = "";
                    dr["Description"] = "";
                    dr["ModeOfProcurement"] = "";
                    dr["Quantity_Size"] = "";
                    dr["_Year"] = WorkingYear;
                    dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                    dr["_Pap"] = _PAPCode;
                    dr["Jan"] = "";
                    dr["Feb"] = "";
                    dr["Mar"] = "";
                    dr["Apr"] = "";
                    dr["May"] = "";
                    dr["Jun"] = "";
                    dr["Jul"] = "";
                    dr["Aug"] = "";
                    dr["Sep"] = "";
                    dr["Oct"] = "";
                    dr["Nov"] = "";
                    dr["Dec"] = "";
                    dr["EstimatedBudget"] = "";
                    dr["_Total"] = "0.00";

                    dtFinal.Rows.Add(dr);
                    grand_total += pap_totals;
                }
                else
                {
                    dr = dtFinal.NewRow();

                    dr["ACTID"] = "";
                    dr["UACS"] = "";
                    dr["Description"] = "Contingency";
                    dr["ModeOfProcurement"] = "";
                    dr["Quantity_Size"] = "";
                    dr["_Year"] = WorkingYear;
                    dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                    dr["_Pap"] = _PAPCode;
                    dr["Jan"] = "";
                    dr["Feb"] = "";
                    dr["Mar"] = "";
                    dr["Apr"] = "";
                    dr["May"] = "";
                    dr["Jun"] = "";
                    dr["Jul"] = "";
                    dr["Aug"] = "";
                    dr["Sep"] = "";
                    dr["Oct"] = "";
                    dr["Nov"] = "";
                    dr["Dec"] = "";
                    //dr["EstimatedBudget"] = (_Allocation).ToString("#,##0.00");
                    //dr["_Total"] = (_Allocation).ToString("#,##0.00");

                    dtFinal.Rows.Add(dr);
                    dr = dtFinal.NewRow();

                    dr["ACTID"] = "";
                    dr["UACS"] = "";
                    //dr["Description"] = "Total :" + (_Allocation).ToString("#,##0.00"); ;
                    dr["ModeOfProcurement"] = "";
                    dr["Quantity_Size"] = "";
                    dr["_Year"] = WorkingYear;
                    dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                    dr["_Pap"] = _PAPCode;
                    dr["Jan"] = "";
                    dr["Feb"] = "";
                    dr["Mar"] = "";
                    dr["Apr"] = "";
                    dr["May"] = "";
                    dr["Jun"] = "";
                    dr["Jul"] = "";
                    dr["Aug"] = "";
                    dr["Sep"] = "";
                    dr["Oct"] = "";
                    dr["Nov"] = "";
                    dr["Dec"] = "";
                    dr["EstimatedBudget"] = "";
                    dr["_Total"] = "0.00";

                    dtFinal.Rows.Add(dr);
                }
            }
            //drow = dtFinal.NewRow();
            //drow["ACTID"] = "";
            //drow["UACS"] = "------------------";
            //drow["Description"] = "------------------------------------------------------";// +(_Allocation).ToString("#,##0.00"); ;
            //drow["ModeOfProcurement"] = "------------------";
            //drow["Quantity_Size"] = "------------------";
            //drow["_Year"] = WorkingYear;
            //drow["_Division"] = cmbDivisions.SelectedItem.ToString();
            //drow["_Pap"] = _PAPCode;
            //drow["Jan"] = "------------------";
            //drow["Feb"] = "------------------";
            //drow["Mar"] = "------------------";
            //drow["Apr"] = "------------------";
            //drow["May"] = "------------------";
            //drow["Jun"] = "------------------";
            //drow["Jul"] = "------------------";
            //drow["Aug"] = "------------------";
            //drow["Sep"] = "------------------";
            //drow["Oct"] = "------------------";
            //drow["Nov"] = "------------------";
            //drow["Dec"] = "------------------";
            //drow["EstimatedBudget"] = "------------------";
            //drow["_Total"] = "0.00";

            //dtFinal.Rows.Add(drow);

            //drow = dtFinal.NewRow();
            //drow["ACTID"] = "";
            //drow["UACS"] = "";
            //drow["Description"] = "Total Budget:";// +(_Allocation).ToString("#,##0.00"); ;
            //drow["ModeOfProcurement"] = "";
            //drow["Quantity_Size"] = "";
            //drow["_Year"] = WorkingYear;
            //drow["_Division"] = cmbDivisions.SelectedItem.ToString();
            //drow["_Pap"] = _PAPCode;
            //drow["Jan"] = "";
            //drow["Feb"] = "";
            //drow["Mar"] = "";
            //drow["Apr"] = "";
            //drow["May"] = "";
            //drow["Jun"] = "";
            //drow["Jul"] = "";
            //drow["Aug"] = "";
            //drow["Sep"] = "";
            //drow["Oct"] = "";
            //drow["Nov"] = "";
            //drow["Dec"] = "";
            //drow["EstimatedBudget"] = grand_total.ToString("#,##0.00");
            //drow["_Total"] = "0.00";

            //dtFinal.Rows.Add(drow);

            totalAmount.Visibility = System.Windows.Visibility.Visible;
            totalAmountLbl.Visibility = System.Windows.Visibility.Visible;
            totalAmount.Content = grand_total.ToString("#,##0.00");

            Boolean NotPassed = false;
            for (int i = 0; i < dtFinal.Rows.Count; i++)
            {
                String _checkval = dtFinal.Rows[i]["EstimatedBudget"].ToString();
                if (_checkval == "")
                {
                    _checkval = "0";
                }

                double _val = Convert.ToDouble(_checkval);
                if (_val < 0)
                {
                    NotPassed = true;
                    break;
                }
            }
            if (NotPassed)
            {
                MessageBox.Show("Please settle first your PPMP Data" + Environment.NewLine + "You have a negative amount on your activities");
                btnPrintPreview.IsEnabled = true;
            }
            else
            {
                btnPrintPreview.IsEnabled = true;
            }

            grdData.DataSource = null;
            grdData.DataSource = dtFinal.DefaultView;

            grdData.FieldLayouts[0].Fields["ACTID"].Visibility = Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["_Year"].Visibility = Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["_Pap"].Visibility = Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["_Division"].Visibility = Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["_Total"].Visibility = Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["ModeOfProcurement"].Visibility = Visibility.Visible;


            grdData.FieldLayouts[0].Fields["UACS"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData.FieldLayouts[0].Fields["Description"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData.FieldLayouts[0].Fields["Quantity_Size"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData.FieldLayouts[0].Fields["EstimatedBudget"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData.FieldLayouts[0].Fields["ModeOfProcurement"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;

            grdData.FieldLayouts[0].Fields["UACS"].Label = "Code";
            grdData.FieldLayouts[0].Fields["Description"].Label = "Description";
            grdData.FieldLayouts[0].Fields["Quantity_Size"].Label = "Quantity";
            grdData.FieldLayouts[0].Fields["EstimatedBudget"].Label = "Estimated Budget";
            grdData.FieldLayouts[0].Fields["ModeOfProcurement"].Label = "Mode of " + Environment.NewLine + "Procurement";

            grdData.FieldLayouts[0].Fields["UACS"].Width = new FieldLength(200);
            grdData.FieldLayouts[0].Fields["Description"].Width = new FieldLength(600);
            grdData.FieldLayouts[0].Fields["Quantity_Size"].Width = new FieldLength(175);
            grdData.FieldLayouts[0].Fields["EstimatedBudget"].Width = new FieldLength(250);
            grdData.FieldLayouts[0].Fields["ModeOfProcurement"].Width = new FieldLength(240);

            //grdData.FieldLayouts[0].Fields["UACS"].PerformAutoSize();
            //grdData.FieldLayouts[0].Fields["Description"].PerformAutoSize();
            //grdData.FieldLayouts[0].Fields["Quantity_Size"].PerformAutoSize();
            //grdData.FieldLayouts[0].Fields["EstimatedBudget"].PerformAutoSize();
            //grdData.FieldLayouts[0].Fields["ModeOfProcurement"].PerformAutoSize();
        }

        private void GenerateDataForPPMPCSE(DataTable _FundSource, DataTable _PPMPData, Boolean IsCONT, DataTable dtGenCat, DataTable dtSubCat, DataTable dtDistinctSupplies)
        {

            double _Totals = 0.00;
            double _OverAll = 0.00;
            double total_ps_dbm = 0.00;
            double total_nonps_dbm = 0.00;
            double total_amount = 0.00;
            double supply_total_amount = 0.00;
            double contingency = 0.00;
            double grand_total_amount = 0.00;
            double totals_q1_psdbm = 0.00;
            double totals_q1_nonpsdbm = 0.00;
            double totals_q2_psdbm = 0.00;
            double totals_q2_nonpsdbm = 0.00;
            double totals_q3_psdbm = 0.00;
            double totals_q3_nonpsdbm = 0.00;
            double totals_q4_psdbm = 0.00;
            double totals_q4_nonpsdbm = 0.00;
            double grand_total_q1 = 0.00;
            double grand_total_q2 = 0.00;
            double grand_total_q3 = 0.00;
            double grand_total_q4 = 0.00;
            double grand_total_monthly_cash = 0.00;
            double row_totals_q1 = 0.00;
            double row_totals_q2 = 0.00;
            double row_totals_q3 = 0.00;
            double row_totals_q4 = 0.00;
            int q1, q2, q3, q4, total_quantity;

            dtFinal = new DataTable();
            dtFinal.Columns.Add("ACTID");
            dtFinal.Columns.Add("UACS");
            dtFinal.Columns.Add("Description");
            dtFinal.Columns.Add("UnitOfMeasure");

            dtFinal.Columns.Add("_Pap");
            dtFinal.Columns.Add("Jan");
            dtFinal.Columns.Add("Feb");
            dtFinal.Columns.Add("Mar");
            dtFinal.Columns.Add("Q1");
            //dtFinal.Columns.Add("Q1_Total");
            dtFinal.Columns.Add("Apr");
            dtFinal.Columns.Add("May");
            dtFinal.Columns.Add("Jun");
            dtFinal.Columns.Add("Q2");
            //dtFinal.Columns.Add("Q2_Total");
            dtFinal.Columns.Add("Jul");
            dtFinal.Columns.Add("Aug");
            dtFinal.Columns.Add("Sep");
            dtFinal.Columns.Add("Q3");
            //dtFinal.Columns.Add("Q3_Total");
            dtFinal.Columns.Add("Oct");
            dtFinal.Columns.Add("Nov");
            dtFinal.Columns.Add("Dec");
            dtFinal.Columns.Add("Q4");
            //dtFinal.Columns.Add("Q4_Total");
            dtFinal.Columns.Add("total_quantity");
            dtFinal.Columns.Add("price_catalogue");
            dtFinal.Columns.Add("total_amount");

            dtFinal.Columns.Add("_Total");
            dtFinal.Columns.Add("_Year");
            dtFinal.Columns.Add("_Division");
            dtFinal.TableName = "PPMP";

            dtFinal2 = new DataTable();
            dtFinal2.Columns.Add("col1");
            dtFinal2.Columns.Add("col2");
            dtFinal2.Columns.Add("col3");

            dtFinal3 = new DataTable();
            dtFinal3.Columns.Add("col1");
            dtFinal3.Columns.Add("col2");
            dtFinal3.Columns.Add("col3");
            dtFinal3.Columns.Add("col4");
            dtFinal3.Columns.Add("col5");
            dtFinal3.Columns.Add("col6");
            dtFinal3.Columns.Add("col7");
            dtFinal3.Columns.Add("col8");
            dtFinal3.Columns.Add("col9");
            dtFinal3.Columns.Add("col10");
            dtFinal3.Columns.Add("col11");

            String _PAPCode = "";
            String _ActId = "";
            foreach (DataRow item in dtGenCat.Rows)
            {
                DataRow dr = dtFinal.NewRow();

                dtSubCat.DefaultView.RowFilter = "general_category ='" + item["general_category"].ToString() + "'";
                try
                {
                    //_PAPCode = _PPMPData.DefaultView.ToTable().Rows[0]["Division_Code"].ToString();

                }
                catch (Exception)
                {

                }

                
                dr["ACTID"] = "";
                dr["UACS"] = "";
                dr["Description"] = item["general_category"].ToString();
                dr["UnitOfMeasure"] = "";

                dr["_Pap"] = _PAPCode;
                dr["Jan"] = "";
                dr["Feb"] = "";
                dr["Mar"] = "";
                dr["Q1"] = "";
                dr["Apr"] = "";
                dr["May"] = "";
                dr["Jun"] = "";
                dr["Q2"] = "";
                dr["Jul"] = "";
                dr["Aug"] = "";
                dr["Sep"] = "";
                dr["Q3"] = "";
                dr["Oct"] = "";
                dr["Nov"] = "";
                dr["Dec"] = "";
                dr["Q4"] = "";
                dr["total_quantity"] = "";
                dr["price_catalogue"] = "";
                dr["total_amount"] = "";

                dtFinal.Rows.Add(dr);
                _Totals = 0.00;
                double totals = 0.00;
                double total_for_contigency = 0.00;
                
                
                if (dtSubCat.DefaultView.ToTable().Rows.Count != 0)
                {


                    DataTable x_data = dtSubCat.DefaultView.ToTable().Copy();
                    foreach (DataRow itemRows in x_data.Rows)
                    {
                        dtDistinctSupplies.DefaultView.RowFilter = "sub_category ='" + itemRows["sub_category"].ToString() + "' AND general_category='" + item["general_category"].ToString() + "'";
                        //_PAPCode = itemRows["Division_Code"].ToString();
                        dr = dtFinal.NewRow();

                        dr["ACTID"] = "";
                        dr["UACS"] = ""; 
                        dr["Description"] = itemRows["sub_category"].ToString();
                        dr["UnitOfMeasure"] = "";
                        dr["_Division"] = "";
                        dr["_Pap"] = "";
                        dr["_Total"] = "";

                        dtFinal.Rows.Add(dr);
                        int count = 0;
                        DataTable y_data = dtDistinctSupplies.DefaultView.ToTable().Copy();
                        
                        foreach (DataRow item_Rows in y_data.Rows)
                        {
                            DataTable dtFetchSuppliesCount = c_ppmp.FetchSuppliesCount(WorkingYear, p_data.FundSourceId, item_Rows["type_service"].ToString()).Tables[0].Copy();
                            count++;
                            dr = dtFinal.NewRow();

                            dr["ACTID"] = "";
                            dr["UACS"] = count.ToString();
                            dr["Description"] = " * " + item_Rows["type_service"].ToString();
                            dr["UnitOfMeasure"] = item_Rows["unit_of_measure"].ToString();
                            dr["_Pap"] = _PAPCode;
                            foreach (DataRow _itemRows in dtFetchSuppliesCount.Rows)
                            {
                                dr["Jan"] = (Convert.ToInt32(_itemRows["Jan"].ToString()) == 0) ? "" : _itemRows["Jan"].ToString();
                                dr["Feb"] = (Convert.ToInt32(_itemRows["Feb"].ToString()) == 0) ? "" : _itemRows["Feb"].ToString();
                                dr["Mar"] = (Convert.ToInt32(_itemRows["Mar"].ToString()) == 0) ? "" : _itemRows["Mar"].ToString();
                                q1 = Convert.ToInt32(_itemRows["Jan"].ToString()) + Convert.ToInt32(_itemRows["Feb"].ToString()) + Convert.ToInt32(_itemRows["Mar"].ToString());
                                dr["Q1"] = q1;
                                //dr["Q1_Total"] = q1 * Convert.ToDouble(item_Rows["price"].ToString());
                                row_totals_q1 = q1 * Convert.ToDouble(item_Rows["price"].ToString());
                                dr["Apr"] = (Convert.ToInt32(_itemRows["Apr"].ToString()) == 0) ? "" : _itemRows["Apr"].ToString();
                                dr["May"] = (Convert.ToInt32(_itemRows["May"].ToString()) == 0) ? "" : _itemRows["May"].ToString();
                                dr["Jun"] = (Convert.ToInt32(_itemRows["Jun"].ToString()) == 0) ? "" : _itemRows["Jun"].ToString();
                                q2 = Convert.ToInt32(_itemRows["Apr"].ToString()) + Convert.ToInt32(_itemRows["May"].ToString()) + Convert.ToInt32(_itemRows["Jun"].ToString());
                                dr["Q2"] = q2;
                                //dr["Q2_Total"] = q2 * Convert.ToDouble(item_Rows["price"].ToString());
                                row_totals_q2 = q2 * Convert.ToDouble(item_Rows["price"].ToString());
                                dr["Jul"] = (Convert.ToInt32(_itemRows["Jul"].ToString()) == 0) ? "" : _itemRows["Jul"].ToString();
                                dr["Aug"] = (Convert.ToInt32(_itemRows["Aug"].ToString()) == 0) ? "" : _itemRows["Aug"].ToString();
                                dr["Sep"] = (Convert.ToInt32(_itemRows["Sep"].ToString()) == 0) ? "" : _itemRows["Sep"].ToString();
                                q3 = Convert.ToInt32(_itemRows["Jul"].ToString()) + Convert.ToInt32(_itemRows["Aug"].ToString()) + Convert.ToInt32(_itemRows["Sep"].ToString());
                                dr["Q3"] = q3;
                                //dr["Q3_Total"] = q3 * Convert.ToDouble(item_Rows["price"].ToString());
                                row_totals_q3 = q3 * Convert.ToDouble(item_Rows["price"].ToString());
                                dr["Oct"] = (Convert.ToInt32(_itemRows["Oct"].ToString()) == 0) ? "" : _itemRows["Oct"].ToString();
                                dr["Nov"] = (Convert.ToInt32(_itemRows["Nov"].ToString()) == 0) ? "" : _itemRows["Nov"].ToString();
                                dr["Dec"] = (Convert.ToInt32(_itemRows["Dec"].ToString()) == 0) ? "" : _itemRows["Dec"].ToString();
                                q4 = Convert.ToInt32(_itemRows["Oct"].ToString()) + Convert.ToInt32(_itemRows["Nov"].ToString()) + Convert.ToInt32(_itemRows["Dec"].ToString());
                                dr["Q4"] = q4;
                                //dr["Q4_Total"] = q4 * Convert.ToDouble(item_Rows["price"].ToString());
                                row_totals_q4 = q4 * Convert.ToDouble(item_Rows["price"].ToString());
                                total_quantity = q1 + q2 + q3 + q4;
                                dr["total_quantity"] = total_quantity;
                                supply_total_amount = Convert.ToDouble(item_Rows["price"].ToString()) * Convert.ToDouble(total_quantity);
                                dr["total_amount"] = supply_total_amount;
                                total_amount += supply_total_amount;

                                if (item["general_category"].ToString() == "AVAILABLE AT PROCUREMENT SERVICE STORES")
                                {
                                    //total_ps_dbm += supply_total_amount;
                                    totals_q1_psdbm += row_totals_q1;
                                    totals_q2_psdbm += row_totals_q2;
                                    totals_q3_psdbm += row_totals_q3;
                                    totals_q4_psdbm += row_totals_q4;
                                }
                                else
                                {
                                    //total_nonps_dbm += supply_total_amount;
                                    totals_q1_nonpsdbm += row_totals_q1;
                                    totals_q2_nonpsdbm += row_totals_q2;
                                    totals_q3_nonpsdbm += row_totals_q3;
                                    totals_q4_nonpsdbm += row_totals_q4;
                                }
                            }
                            dr["price_catalogue"] = item_Rows["price"].ToString();
                            dtFinal.Rows.Add(dr);

                            
                        }
                    }
                    dr = dtFinal.NewRow();

                    dr["ACTID"] = "";
                    dr["UACS"] = "";
                    dr["Description"] = "";
                    dr["_Division"] = cmbDivisions.SelectedItem.ToString();
                    dr["_Pap"] = _PAPCode;
                    dr["Jan"] = "";
                    dr["Feb"] = "";
                    dr["Mar"] = "";
                    dr["Apr"] = "";
                    dr["May"] = "";
                    dr["Jun"] = "";
                    dr["Jul"] = "";
                    dr["Aug"] = "";
                    dr["Sep"] = "";
                    dr["Oct"] = "";
                    dr["Nov"] = "";
                    dr["Dec"] = "";

                    dtFinal.Rows.Add(dr);
                }

                dr = dtFinal.NewRow();

                dr["ACTID"] = "";
                dr["UACS"] = "";
                dr["Description"] = "";
                dr["_Year"] = "";
                dr["_Pap"] = "";
                //dr["total_amount"] = grand_total_amount;
                

                dtFinal.Rows.Add(dr);
            }
            contingency = total_amount * 0.1;
            grand_total_amount = total_amount + contingency;
            DataRow dRow = dtFinal2.NewRow();
            DataRow dRows = dtFinal3.NewRow();

            dRow = dtFinal2.NewRow();
            dRow["col1"] = "C. TOTAL (A + B)";
            dRow["col2"] = "";
            dRow["col3"] = total_amount.ToString("#,##0.00");
            dtFinal2.Rows.Add(dRow);

            dRow = dtFinal2.NewRow();
            dRow["col1"] = "D. ADDITIONAL PROVISION FOR INFLATION (10% of TOTAL)";
            dRow["col2"] = "";
            dRow["col3"] = contingency.ToString("#,##0.00");
            dtFinal2.Rows.Add(dRow);

            dRow = dtFinal2.NewRow();
            dRow["col1"] = "E. GRAND TOTAL (C + D)";
            dRow["col2"] = "";

            dRow["col3"] = grand_total_amount.ToString("#,##0.00");
            dtFinal2.Rows.Add(dRow);

            dRow = dtFinal2.NewRow();
            dRow["col1"] = "F. APPROVED BUDGET BY THE AGENCY HEAD In Figures and Words:";
            dRow["col2"] = "";
            dRow["col3"] = "";
            dtFinal2.Rows.Add(dRow);

            dRow = dtFinal2.NewRow();
            dRow["col1"] = "G. MONTHLY CASH REQUIREMENTS";
            dRow["col2"] = "";
            dRow["col3"] = "";
            dtFinal2.Rows.Add(dRow);

            dRows = dtFinal3.NewRow();
            dRows["col1"] = "G.1 Available at Procurement Service Stores";
            dRows["col2"] = "";
            dRows["col3"] = totals_q1_psdbm;
            dRows["col4"] = "";
            dRows["col5"] = totals_q2_psdbm;
            dRows["col6"] = "";
            dRows["col7"] = totals_q3_psdbm;
            dRows["col8"] = "";
            dRows["col9"] = totals_q4_psdbm;
            dRows["col10"] = "";
            total_ps_dbm = totals_q1_psdbm + totals_q2_psdbm + totals_q3_psdbm + totals_q4_psdbm;
            dRows["col11"] = total_ps_dbm.ToString("#,##0.00");
            dtFinal3.Rows.Add(dRows);

            dRows = dtFinal3.NewRow();
            dRows["col1"] = "G.2 Other Items not available at PS but regulary purchased from other sources";
            dRows["col2"] = "";
            dRows["col3"] = totals_q1_nonpsdbm;
            dRows["col4"] = "";
            dRows["col5"] = totals_q2_nonpsdbm;
            dRows["col6"] = "";
            dRows["col7"] = totals_q3_nonpsdbm;
            dRows["col8"] = "";
            dRows["col9"] = totals_q4_nonpsdbm;
            dRows["col10"] = "";
            total_nonps_dbm = totals_q1_nonpsdbm + totals_q2_nonpsdbm + totals_q3_nonpsdbm + totals_q4_nonpsdbm;
            dRows["col11"] = total_nonps_dbm.ToString("#,##0.00");
            dtFinal3.Rows.Add(dRows);

            dRows = dtFinal3.NewRow();
            dRows["col1"] = "TOTAL MONTHLY CASH REQUIREMENTS";
            dRows["col2"] = "";
            grand_total_q1 = totals_q1_psdbm + totals_q1_nonpsdbm;
            dRows["col3"] = grand_total_q1.ToString("#,##0.00");
            dRows["col4"] = "";
            grand_total_q2 = totals_q2_psdbm + totals_q2_nonpsdbm;
            dRows["col5"] = grand_total_q2.ToString("#,##0.00");
            dRows["col6"] = "";
            grand_total_q3 = totals_q3_psdbm + totals_q3_nonpsdbm;
            dRows["col7"] = grand_total_q3.ToString("#,##0.00");
            dRows["col8"] = "";
            grand_total_q4 = totals_q4_psdbm + totals_q4_nonpsdbm;
            dRows["col9"] = grand_total_q4.ToString();
            dRows["col10"] = "";
            grand_total_monthly_cash = grand_total_q1 + grand_total_q2 + grand_total_q3 + grand_total_q4;
            dRows["col11"] = grand_total_monthly_cash.ToString("#,##0.00");
            dtFinal3.Rows.Add(dRows);

            //Boolean NotPassed = false;
            //for (int i = 0; i < dtFinal.Rows.Count; i++)
            //{
            //    String _checkval = dtFinal.Rows[i]["EstimatedBudget"].ToString();
            //    if (_checkval == "")
            //    {
            //        _checkval = "0";
            //    }

            //    double _val = Convert.ToDouble(_checkval);
            //    if (_val < 0)
            //    {
            //        NotPassed = true;
            //        break;
            //    }
            //}
            //if (NotPassed)
            //{
            //    MessageBox.Show("Please settle first your PPMP Data." + Environment.NewLine + "You have a negative amount on your activities");
            //    btnPrintPreview.IsEnabled = false;
            //}
            //else
            //{
            //    btnPrintPreview.IsEnabled = true;
            //}

            grdData.DataSource = null;
            grdData.DataSource = dtFinal.DefaultView;

            grdData.FieldLayouts[0].Fields["ACTID"].Visibility = Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["_Year"].Visibility = Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["_Pap"].Visibility = Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["_Division"].Visibility = Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["_Total"].Visibility = Visibility.Collapsed;


            grdData.FieldLayouts[0].Fields["UACS"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData.FieldLayouts[0].Fields["Description"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData.FieldLayouts[0].Fields["UnitOfMeasure"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;

            grdData.FieldLayouts[0].Fields["UACS"].Label = "No.";
            grdData.FieldLayouts[0].Fields["Description"].Label = "Item Specifications";
            grdData.FieldLayouts[0].Fields["UnitOfMeasure"].Label = "Unit "+Environment.NewLine+"of Measure";

            grdData.FieldLayouts[0].Fields["UACS"].Width = new FieldLength(80);
            grdData.FieldLayouts[0].Fields["Description"].Width = new FieldLength(700);
            grdData.FieldLayouts[0].Fields["UnitOfMeasure"].Width = new FieldLength(150);

            // GRDDATA_TOTAL
            grdData_Total.DataSource = null;
            grdData_Total.DataSource = dtFinal2.DefaultView;

            grdData_Total.FieldLayouts[0].Fields["col1"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData_Total.FieldLayouts[0].Fields["col2"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData_Total.FieldLayouts[0].Fields["col3"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;

            grdData_Total.FieldLayouts[0].Fields["col1"].Label = "";
            grdData_Total.FieldLayouts[0].Fields["col2"].Label = "";
            grdData_Total.FieldLayouts[0].Fields["col3"].Label = "";

            grdData_Total.FieldLayouts[0].Fields["col1"].Width = new FieldLength(300);
            grdData_Total.FieldLayouts[0].Fields["col2"].Width = new FieldLength(750);
            grdData_Total.FieldLayouts[0].Fields["col3"].Width = new FieldLength(250);

            // GRDDATA_TOTAL_QUARTER
            grdData_Total_Quarter.DataSource = null;
            grdData_Total_Quarter.DataSource = dtFinal3.DefaultView;

            grdData_Total_Quarter.FieldLayouts[0].Fields["col1"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData_Total_Quarter.FieldLayouts[0].Fields["col2"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData_Total_Quarter.FieldLayouts[0].Fields["col3"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData_Total_Quarter.FieldLayouts[0].Fields["col4"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData_Total_Quarter.FieldLayouts[0].Fields["col5"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData_Total_Quarter.FieldLayouts[0].Fields["col6"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData_Total_Quarter.FieldLayouts[0].Fields["col7"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData_Total_Quarter.FieldLayouts[0].Fields["col8"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData_Total_Quarter.FieldLayouts[0].Fields["col9"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData_Total_Quarter.FieldLayouts[0].Fields["col10"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;
            grdData_Total_Quarter.FieldLayouts[0].Fields["col11"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;

            grdData_Total_Quarter.FieldLayouts[0].Fields["col1"].Label = "";
            grdData_Total_Quarter.FieldLayouts[0].Fields["col2"].Label = "";
            grdData_Total_Quarter.FieldLayouts[0].Fields["col3"].Label = "";
            grdData_Total_Quarter.FieldLayouts[0].Fields["col4"].Label = "";
            grdData_Total_Quarter.FieldLayouts[0].Fields["col5"].Label = "";
            grdData_Total_Quarter.FieldLayouts[0].Fields["col6"].Label = "";
            grdData_Total_Quarter.FieldLayouts[0].Fields["col7"].Label = "";
            grdData_Total_Quarter.FieldLayouts[0].Fields["col8"].Label = "";
            grdData_Total_Quarter.FieldLayouts[0].Fields["col9"].Label = "";
            grdData_Total_Quarter.FieldLayouts[0].Fields["col10"].Label = "";
            grdData_Total_Quarter.FieldLayouts[0].Fields["col11"].Label = "";

            grdData_Total_Quarter.FieldLayouts[0].Fields["col1"].Width = new FieldLength(300);
            grdData_Total_Quarter.FieldLayouts[0].Fields["col2"].Width = new FieldLength(100);
            grdData_Total_Quarter.FieldLayouts[0].Fields["col3"].Width = new FieldLength(100);
            grdData_Total_Quarter.FieldLayouts[0].Fields["col4"].Width = new FieldLength(100);
            grdData_Total_Quarter.FieldLayouts[0].Fields["col5"].Width = new FieldLength(100);
            grdData_Total_Quarter.FieldLayouts[0].Fields["col6"].Width = new FieldLength(100);
            grdData_Total_Quarter.FieldLayouts[0].Fields["col7"].Width = new FieldLength(100);
            grdData_Total_Quarter.FieldLayouts[0].Fields["col8"].Width = new FieldLength(100);
            grdData_Total_Quarter.FieldLayouts[0].Fields["col9"].Width = new FieldLength(100);
            grdData_Total_Quarter.FieldLayouts[0].Fields["col10"].Width = new FieldLength(100);
            grdData_Total_Quarter.FieldLayouts[0].Fields["col11"].Width = new FieldLength(250);

            grdData.Height = 439;
        }

        private void LoadReportPPMP(Boolean IsCONT)
        {



            dtReport = new DataTable();

            dtReport.Columns.Add("UACS");
            dtReport.Columns.Add("Description");
            dtReport.Columns.Add("Quantity");
            dtReport.Columns.Add("EstimateBudget");
            dtReport.Columns.Add("ModeOfProcurement");
            dtReport.Columns.Add("Jan");
            dtReport.Columns.Add("Feb");
            dtReport.Columns.Add("Mar");
            dtReport.Columns.Add("Apr");
            dtReport.Columns.Add("May");
            dtReport.Columns.Add("Jun");
            dtReport.Columns.Add("Jul");
            dtReport.Columns.Add("Aug");
            dtReport.Columns.Add("Sep");
            dtReport.Columns.Add("Octs");
            dtReport.Columns.Add("Nov");
            dtReport.Columns.Add("Dec");
            dtReport.Columns.Add("Total");
            dtReport.Columns.Add("Division");
            dtReport.Columns.Add("Yearssss");
            dtReport.Columns.Add("PAP");
            dtReport.Columns.Add("approved");
            dtReport.Columns.Add("header");
            dtReport.Columns.Add("revision");
            dtReport.Columns.Add("overall");
            double Total = 0.00;
            double Contigency = 0.00;
            foreach (DataRow item in dtFinal.Rows)
            {

                string _debug = item["Description"].ToString();
                if (item["Description"].ToString() != "Contingency" && item["UACS"].ToString() == Pap_Code)
                {
                    try
                    {
                        Total += Convert.ToDouble(item["EstimatedBudget"].ToString());
                    }
                    catch (Exception)
                    {

                    }

                }
                else
                {
                    try
                    {
                        Contigency += Convert.ToDouble(item["EstimatedBudget"].ToString());
                    }
                    catch (Exception)
                    {

                    }

                }


            }
            foreach (DataRow item in dtFinal.Rows)
            {
                DataRow dr = dtReport.NewRow();

                dr["Uacs"] = item["UACS"].ToString();
                dr["Description"] = item["Description"].ToString();
                dr["Quantity"] = item["Quantity_Size"].ToString();
                dr["EstimateBudget"] = item["EstimatedBudget"].ToString();
                dr["ModeOfProcurement"] = item["ModeOfProcurement"].ToString();
                dr["Jan"] = item["Jan"].ToString();
                dr["Feb"] = item["Feb"].ToString();
                dr["Mar"] = item["Mar"].ToString();
                dr["Apr"] = item["Apr"].ToString();
                dr["May"] = item["May"].ToString();
                dr["Jun"] = item["Jun"].ToString();
                dr["Jul"] = item["Jul"].ToString();
                dr["Aug"] = item["Aug"].ToString();
                dr["Sep"] = item["Sep"].ToString();
                dr["Octs"] = item["Oct"].ToString();
                dr["Nov"] = item["Nov"].ToString();
                dr["Dec"] = item["Dec"].ToString();
                dr["Total"] = Total.ToString();
                dr["Division"] = item["_Division"].ToString();
                dr["Yearssss"] = item["_Year"].ToString();
                dr["PAP"] = PAP;// item["_Pap"].ToString();
                dr["approved"] = "0";
                if (chkCSE.IsChecked == true)
                {
                    if (isRevised)
                    {
                        if (IsCONT)
                        {
                            dr["header"] = "PROJECT PROCUREMENT MANAGEMENT PLAN(PPMP-CONT-CSE) - Revised";
                        }
                        else
                        {
                            dr["header"] = "PROJECT PROCUREMENT MANAGEMENT PLAN(PPMP-CSE) - Revised";
                        }

                    }
                    else
                    {
                        if (IsCONT)
                        {
                            dr["header"] = "PROJECT PROCUREMENT MANAGEMENT PLAN(PPMP-CONT-CSE)";
                        }
                        else
                        {
                            dr["header"] = "PROJECT PROCUREMENT MANAGEMENT PLAN(PPMP-CSE)";
                        }

                    }

                }
                else
                {
                    if (isRevised)
                    {
                        if (IsCONT)
                        {
                            dr["header"] = "PROJECT PROCUREMENT MANAGEMENT PLAN(PPMP-CONT) - Revised";
                        }
                        else
                        {
                            dr["header"] = "PROJECT PROCUREMENT MANAGEMENT PLAN(PPMP) - Revised";
                        }

                    }
                    else
                    {
                        if (IsCONT)
                        {
                            dr["header"] = "PROJECT PROCUREMENT MANAGEMENT PLAN(PPMP-CONT)";
                        }
                        else
                        {
                            dr["header"] = "PROJECT PROCUREMENT MANAGEMENT PLAN(PPMP)";
                        }

                    }
                }

                dr["revision"] = "0";
                dr["overall"] = "0";

                dtReport.Rows.Add(dr);
            }


            DataSet ds = new DataSet();
            ds.Tables.Add(dtReport);


            frmGenericReport f_preview = new frmGenericReport(ds, "PPMP");


            f_preview.Show();


        }

        private void LoadReportAnnexA()
        {

            DataTable dtAnnexA = c_ppmp.FetchDataAnnexA(DivID, WorkingYear).Tables[0].Copy();
            DataTable dtAnnexATitle = c_ppmp.FetchDataAnnexATitle(DivID, WorkingYear).Tables[0].Copy();
            dtReport = new DataTable();

            dtReport.Columns.Add("mfo_pap");
            dtReport.Columns.Add("from_obect_expenditure");
            dtReport.Columns.Add("from_total");
            dtReport.Columns.Add("to_obect_expenditure");
            dtReport.Columns.Add("to_total");

            double Total = 0.00;
            double Contigency = 0.00;

            for (int i = 0; i < dtAnnexATitle.Rows.Count; i++)
            {

                DataRow dr = dtReport.NewRow();
                dtAnnexA.DefaultView.RowFilter = "";
                dtAnnexA.DefaultView.RowFilter = "from_obect_expenditure ='" + dtAnnexATitle.Rows[i][1].ToString() + "'";
                DataTable dt = dtAnnexA.DefaultView.ToTable().Copy();
                if (dt.Rows.Count > 1)
                {

                    dr["mfo_pap"] = "Operations " + Environment.NewLine + Environment.NewLine + "MFO 1: Integrated Policies and Programs for Mindanao " + Environment.NewLine + Environment.NewLine + Environment.NewLine + "      " + PAPCode;
                    dr["from_obect_expenditure"] = dtAnnexATitle.Rows[i][1].ToString();
                    double _total = 0.00;
                    for (int c = 0; c < dt.Rows.Count; c++)
                    {
                        _total += Convert.ToDouble(dt.Rows[c][4].ToString());
                    }

                    dr["from_total"] = _total;
                    dr["to_obect_expenditure"] = dt.Rows[0][3].ToString();
                    dr["to_total"] = dt.Rows[0][4].ToString();
                    dtReport.Rows.Add(dr);
                    for (int x = 1; x < dt.Rows.Count; x++)
                    {
                        dr = dtReport.NewRow();
                        dr["mfo_pap"] = "Operations " + Environment.NewLine + Environment.NewLine + "MFO 1: Integrated Policies and Programs for Mindanao " + Environment.NewLine + Environment.NewLine + Environment.NewLine + "      " + PAPCode;
                        dr["from_obect_expenditure"] = "";
                        dr["from_total"] = "0";
                        dr["to_obect_expenditure"] = dt.Rows[x][3].ToString();
                        dr["to_total"] = dt.Rows[x][4].ToString();
                        dtReport.Rows.Add(dr);
                    }
                }
                else
                {

                    dr["mfo_pap"] = "Operations " + Environment.NewLine + Environment.NewLine + "MFO 1: Integrated Policies and Programs for Mindanao " + Environment.NewLine + Environment.NewLine + Environment.NewLine + "      " + PAPCode;
                    dr["from_obect_expenditure"] = dtAnnexATitle.Rows[i][1].ToString();
                    dr["from_total"] = dt.Rows[0][2].ToString();
                    dr["to_obect_expenditure"] = dt.Rows[0][3].ToString();
                    dr["to_total"] = dt.Rows[0][4].ToString();
                    dtReport.Rows.Add(dr);
                }


            }


            DataSet ds = new DataSet();
            ds.Tables.Add(dtReport);


            frmGenericReport f_preview = new frmGenericReport(ds, "AnnexA");


            f_preview.Show();


        }

        private void LoadReportAnnexB()
        {

            DataTable dtAnnexB = c_ppmp.FetchDataAnnexBDefficient(DivID, WorkingYear).Tables[0].Copy();

            dtReport = new DataTable();

            dtReport.Columns.Add("group");
            dtReport.Columns.Add("programs_activity");
            dtReport.Columns.Add("responsibility_center");
            dtReport.Columns.Add("allotmentclass");
            dtReport.Columns.Add("obect_expenditure");
            dtReport.Columns.Add("amount");

            double Total = 0.00;
            double Contigency = 0.00;

            for (int i = 0; i < dtAnnexB.Rows.Count; i++)
            {
                DataRow dr = dtReport.NewRow();

                dr["group"] = "DEFICIENT ITEMS (TO):";
                dr["programs_activity"] = PAPCode.Split('-')[0].Trim();
                dr["responsibility_center"] = PAPCode.Replace(PAPCode.Split('-')[0].Trim(), "").Replace(" -", "").Trim() + " - " + cmbDivisions.SelectedItem.ToString(); ;
                dr["allotmentclass"] = "MOOE";
                dr["obect_expenditure"] = dtAnnexB.Rows[i][0].ToString();
                dr["amount"] = dtAnnexB.Rows[i][1].ToString();
                dtReport.Rows.Add(dr);

            }
            dtAnnexB = c_ppmp.FetchDataAnnexBSource(DivID, WorkingYear).Tables[0].Copy();

            for (int i = 0; i < dtAnnexB.Rows.Count; i++)
            {
                DataRow dr = dtReport.NewRow();

                dr["group"] = "SOURCE ITEMS (FROM):";
                dr["programs_activity"] = PAPCode.Split('-')[0].Trim();
                dr["responsibility_center"] = PAPCode.Replace(PAPCode.Split('-')[0].Trim(), "").Replace(" -", "").Trim() + " - " + cmbDivisions.SelectedItem.ToString(); ;
                dr["allotmentclass"] = "MOOE";
                dr["obect_expenditure"] = dtAnnexB.Rows[i][0].ToString();
                dr["amount"] = "-" + dtAnnexB.Rows[i][1].ToString();
                dtReport.Rows.Add(dr);

            }

            DataSet ds = new DataSet();
            ds.Tables.Add(dtReport);


            frmGenericReport f_preview = new frmGenericReport(ds, "AnnexB");


            f_preview.Show();


        }
        private void btnPPMPPrintout_Click(object sender, RoutedEventArgs e)
        {
            String click = "btnPPMPPrintout";
            LoadFundSource(click);
        }

        private void btnPrintPreview_Click(object sender, RoutedEventArgs e)
        {
            LoadReportPPMP(this.IsCONT);


        }

        private void btnAnnexA_Click(object sender, RoutedEventArgs e)
        {
            LoadReportAnnexA();
        }

        private void btnAnnexB_Click(object sender, RoutedEventArgs e)
        {
            LoadReportAnnexB();
        }


        private void SuspendData(List<String> _id)
        {
            String _sql = "";

            StringBuilder sb = new StringBuilder(79);
            foreach (String item in _id)
            {
                sb.AppendLine(@"UPDATE  dbo.mnda_activity_data SET is_suspended =1,status ='SUSPENDED-REVISION' WHERE id = " + item + ";");

            }
            _sql = sb.ToString();
            if (c_ppmp.ExecuteData(_sql))
            {
                MessageBox.Show("Expense Item has been suspended.");
                RefreshData();
            }
        }

        private void grdData_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {

                List<String> _selected_data = new List<string>();

                Infragistics.Windows.DataPresenter.DataPresenterBase.SelectedItemHolder src = grdData.SelectedItems;
                foreach (Cell record in src)
                {
                    DataRecord _data = record.Record;
                    _selected_data.Add(_data.Cells[0].Value.ToString());
                }


                MessageBoxResult _res = MessageBox.Show("Confirm Suspend of Expense Item", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Information);
                if (_res.ToString().ToLower() == "yes")
                {
                    SuspendData(_selected_data);
                }
            }
        }

        private void btnNewPPMPPrintout_Click(object sender, RoutedEventArgs e)
        {
            String click = "btnNewPPMPPrintout";
            LoadFundSource(click);
        }

        private void btnPPMPCSE_Click_1(object sender, RoutedEventArgs e)
        {
            String click = "btnPPMPCSE";
            LoadFundSource(click);
        }

        //Prints PPMP per Expense Item
        private void btnPPMPPrintoutPerExpenseItem_Click(object sender, RoutedEventArgs e)
        {
            String click = "btnPPMPPrintoutPerExpenseItem";
            LoadFundSource(click);
        }

        //Exports the PPMP/APP-CSE data to Excel File
        private void btnExportToExcel_Click(object sender, RoutedEventArgs e)
        {
            //exporter.Export(this.grdData, @"C:\Users\Minda\Desktop\export1.xlsx", WorkbookFormat.Excel2007);
            frmExport f_exp = new frmExport();
            f_exp.ShowDialog();
            //MessageBox.Show(f_exp.FilePath + @"\" + f_exp.FileName);
            exporter.Export(this.grdData, f_exp.FilePath+ @"\" +f_exp.FileName+ ".xlsx", WorkbookFormat.Excel2007);
        }
    }
}
