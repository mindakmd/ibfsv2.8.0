﻿
using ReportTool.ReportDesigners;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Procurement_Module.Forms
{
    /// <summary>
    /// Interaction logic for frmGenericReport.xaml
    /// </summary>
    public partial class frmGenericReport : Window
    {
        private DataSet dsReport = new DataSet();
        private String ReportTypes= "";

        public frmGenericReport(DataSet ds, String ReportType)
        {
            InitializeComponent();
            dsReport = ds;
            ReportTypes = ReportType;
            this.Loaded += frmGenericReport_Loaded;
        }

        private void LoadReportPPMP()
        {
            rptReportPPMPFinal _rptData = new rptReportPPMPFinal();

            String _path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"ReportSchemas\";
            DataSet ds = dsReport.Copy() ;
            
            //ds.WriteXmlSchema(_path + "div_ppmp.xml");
            _rptData.SetDataSource(ds);

            _report.ViewerCore.Zoom(70);
            _report.ViewerCore.ReportSource = _rptData;
        }
        private void LoadReportPPMPCONSO()
        {
            rptPPMPConso _rptData = new rptPPMPConso();

            String _path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"ReportSchemas\";
            DataSet ds = dsReport.Copy();

          // ds.WriteXmlSchema(_path + "div_ppmp_conso.xml");
            _rptData.SetDataSource(ds);

            _report.ViewerCore.Zoom(70);
            _report.ViewerCore.ReportSource = _rptData;
        }
        private void LoadReportAnnexA()
        {
            realignment_annex_a _rptData = new realignment_annex_a();

            String _path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"ReportSchemas\";
            DataSet ds = dsReport.Copy();

            //ds.WriteXmlSchema(_path + "div_AnnexA.xml");
            _rptData.SetDataSource(ds);

            _report.ViewerCore.Zoom(70);
            _report.ViewerCore.ReportSource = _rptData;
        }
        private void LoadReportAnnexB()
        {
            realignment_annex_b _rptData = new realignment_annex_b();

            String _path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"ReportSchemas\";
            DataSet ds = dsReport.Copy();

           // ds.WriteXmlSchema(_path + "div_AnnexB.xml");
            _rptData.SetDataSource(ds);

            _report.ViewerCore.Zoom(70);
            _report.ViewerCore.ReportSource = _rptData;
        }
        void frmGenericReport_Loaded(object sender, RoutedEventArgs e)
        {
            switch (ReportTypes)
            {
                case "PPMP":
                    LoadReportPPMP();
                    break;
                case "PPMP-CONSO":
                    LoadReportPPMPCONSO();
                    break;
                case "AnnexA":
                    LoadReportAnnexA();
                    break;
                case "AnnexB":
                    LoadReportAnnexB();
                    break;

            }
        }


    }
}
