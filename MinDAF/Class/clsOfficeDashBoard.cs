﻿
using MinDAF.MinDAFS;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsOfficeDashBoard
    {
        public event EventHandler ReceiveDivision;
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();
        MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();

        public String FetchOffice(String _office_id)
        {
            StringBuilder sb = new StringBuilder(147);
            sb.AppendLine(@"");
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"{fn CONCAT(dsp.DBM_Sub_Pap_Desc, {fn CONCAT(' - ', dsp.DBM_Sub_Pap_Code)}) } as office,dsp.DBM_Sub_Pap_Code as pap");
            sb.AppendLine(@"FROM DBM_Sub_Pap dsp");
            sb.AppendLine(@"WHERE dsp.DBM_Sub_Pap_id= " + _office_id + "");
            sb.AppendLine(@";");


            return sb.ToString();
        } 
        public String FetchOfficePAPS(String WorkingYear)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT \n");
            sb.Append(" dspap.Sub_Id as Id, \n");
            sb.Append(" dspap.PAP as Code, \n");
            sb.Append(" dspap.Description as Office \n");
            sb.Append("FROM DBM_Sub_Office dspap ");
            sb.Append("WHERE dspap.WorkingYear ='"+ WorkingYear +"' ");

            return sb.ToString();
        }
        public String FetchDivisionData()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT \n");
            sb.Append(" dspap.Division_Id as Id, \n");
            sb.Append(" dspap.Division_Id as Code, \n");
            sb.Append(" dspap.Division_Desc as Office \n");
            sb.Append("FROM Division dspap WHERE is_sub_division ='0'");


            return sb.ToString();
        }
        public String FetchDivisionAsign(String _working_year)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT \n");
            sb.Append(" dspap.Division_Id, \n");
            sb.Append(" dspap.Division_Desc, \n");
            sb.Append(" dspap.DivisionAccro \n");
            sb.Append("FROM Division dspap WHERE is_sub_division ='0'");
          //  sb.Append("AND year_setup ='" + _working_year + "'");

            return sb.ToString();
        }
        public String FetchDivisionPAPS()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT \n");
            sb.Append(" dspap.Division_Id as Id, \n");
            sb.Append(" dspap.Division_Id as Code, \n");
            sb.Append(" dspap.Division_Desc as Office \n");
            sb.Append("FROM Division dspap WHERE is_sub_division ='0'");


            return sb.ToString();
        }

        public String FetchDivisionUnits(String _working_year)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("SELECT \n");
            sb.Append(" dspap.Division_Id, \n");
            sb.Append(" dspap.Division_Desc, \n");
            sb.Append(" dspap.DivisionAccro \n");
            sb.Append("FROM Division dspap WHERE is_sub_division ='1'");
            sb.Append("AND dspap.Division_Id NOT IN (SELECT Division_Id FROM Division WHERE year_setup ='" + _working_year + "')");


            return sb.ToString();
        }
        public void AddDivisionCode(String _type,DivisionData _data)
        {
            StringBuilder sb = new StringBuilder();
            switch (_type)
            {
                case "SubPapNew":

                    sb.AppendLine(@"INSERT INTO ");
                    sb.AppendLine(@"  dbo.DBM_Sub_Office");
                    sb.AppendLine(@"(");
                    sb.AppendLine(@"  DBM_Sub_Id,");
                    sb.AppendLine(@"  Description,");
                    sb.AppendLine(@"  PAP,");
                    sb.AppendLine(@"  WorkingYear");                 
                    sb.AppendLine(@") ");
                    sb.AppendLine(@"VALUES (");
                    sb.AppendLine(@"  " + _data.DBM_SUP_PAP_ID + ",");
                    sb.AppendLine(@"  '" + _data.Division_Desc + "',");
                    sb.AppendLine(@"  '" + _data.Division_Code + "',");
                    sb.AppendLine(@"  '" + _data.Year_Setup + "'");
                    sb.AppendLine(@");");
                    break;
                case "Assign":
 
                    sb.AppendLine(@"INSERT INTO ");
                    sb.AppendLine(@"  dbo.Division");
                    sb.AppendLine(@"(");
                    sb.AppendLine(@"  Division_Id,");
                    sb.AppendLine(@"  DBM_Sub_Pap_Id,");
                    sb.AppendLine(@"  Division_Code,");
                    sb.AppendLine(@"  Division_Desc,");
                    sb.AppendLine(@"  DivisionAccro,");
                    sb.AppendLine(@"  UnitCode,");
                    sb.AppendLine(@"  is_sub_division,");
                    sb.AppendLine(@"  year_setup");
                    sb.AppendLine(@") ");
                    sb.AppendLine(@"VALUES (");
                    sb.AppendLine(@"  "+ _data.Division_Id +",");
                    sb.AppendLine(@"  '"+ _data.DBM_SUP_PAP_ID +"',");
                    sb.AppendLine(@"  '"+ _data.Division_Code +"',");
                    sb.AppendLine(@"  '"+ _data.Division_Desc +"',");
                    sb.AppendLine(@"  '"+ _data.Division_Accro +"',");
                    sb.AppendLine(@"  '"+ _data.Unit_Code +"',");
                    sb.AppendLine(@"  '"+ _data.Is_Sub_Division +"',");
                    sb.AppendLine(@"  '"+ _data.Year_Setup +"'");
                    sb.AppendLine(@");");


                    //sb.Append("UPDATE Division SET \n");
                    //sb.Append("Division_Code ='"+ _data.Division_Code +"' \n");
                    //sb.Append("WHERE Division_Id = "+_data.Division_Id +"");
                    break;
                case "AddNew":
                    sb.AppendLine(@"INSERT INTO ");
                    sb.AppendLine(@"  dbo.Division");
                    sb.AppendLine(@"(");
                    sb.AppendLine(@"  Division_Id,");
                    sb.AppendLine(@"  DBM_Sub_Pap_Id,");
                    sb.AppendLine(@"  Division_Code,");
                    sb.AppendLine(@"  Division_Desc,");
                    sb.AppendLine(@"  DivisionAccro,");
                    sb.AppendLine(@"  UnitCode,");
                    sb.AppendLine(@"  is_sub_division,");
                    sb.AppendLine(@"  year_setup");
                    sb.AppendLine(@") ");
                    sb.AppendLine(@"VALUES (");
                    sb.AppendLine(@"  (SELECT ((SELECT TOP 1 Division_Id FROM Division ORDER BY Division_id DESC) + 1)),");
                    sb.AppendLine(@"  '" + _data.DBM_SUP_PAP_ID + "',");
                    sb.AppendLine(@"  '-',");
                    sb.AppendLine(@"  '"+ _data.Division_Desc +"',");
                    sb.AppendLine(@"  '"+ _data.Division_Accro +"',");
                    sb.AppendLine(@"  '-',");
                    sb.AppendLine(@"  '" + _data.Is_Sub_Division + "',");
                    sb.AppendLine(@"  '"+ _data.Year_Setup +"'");
                    sb.AppendLine(@");");
                    break;

            }

            String _sqlString = "";


            _sqlString = sb.ToString();

            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }
        public void UpdateDivisionCode(String _type, String div_id, String _code) 
        {
            StringBuilder sb = new StringBuilder();
            switch (_type)
            {
                case "Office":                
                    sb.Append("UPDATE \n");
                    sb.Append("  dbo.Division \n");
                    sb.Append("SET \n");
                    sb.Append("  Division_Code ='"+ _code +"' \n");
                    sb.Append("WHERE Division_Id ="+ div_id +";");
                    break;
                case "Unit":
                    sb.Append("UPDATE \n");
                    sb.Append("  dbo.Division \n");
                    sb.Append("SET \n");
                    sb.Append("  UnitCode ='"+ _code +"' \n");
                    sb.Append("WHERE Division_Id ="+ div_id +";");
                    break;
              
            }

            String _sqlString = "";


            _sqlString = sb.ToString();

            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }
        private Boolean isDone = false;
        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "Update":
                    if (isDone)
                    {
                        isDone = false;
                        break;
                    }
                    if (this.SQLOperation!=null)
	                {
                        isDone = true;
                        this.SQLOperation(this, new EventArgs());
	                }  
                    break;
                case "AssignNew":
                    if (isDone)
                    {
                        isDone = false;
                        break;
                    }
                    if (this.SQLOperation != null)
                    {
                        isDone = true;
                        this.SQLOperation(this, new EventArgs());
                    }
                    break;
                case "AddNew":case "SubPapNew":
                    if (isDone)
                    {
                        isDone = false;
                        break;
                    }
                    if (this.SQLOperation != null)
                    {
                        isDone = true;
                        this.SQLOperation(this, new EventArgs());
                    }
                    break;
                default:
                    break;
            }
        }
        public String FetchDivision(String _office_id)
        {
            StringBuilder sb = new StringBuilder(140);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"div.Division_Id as id, {fn CONCAT(div.Division_Desc, {fn CONCAT(' - ', div.Division_Code)}) } as Division");
            sb.AppendLine(@"FROM Division div");
            sb.AppendLine(@"WHERE div.DBM_Sub_Pap_Id= "+ _office_id +"");
            sb.AppendLine(@";");
            
            return sb.ToString();
        }

        public String FetchFundSource(String DivisionId, String _year,String FundSource)
        {
            var sb = new System.Text.StringBuilder(134);
            sb.AppendLine(@"SELECT");
           sb.AppendLine(@"mfs.code as id,");
          //  sb.AppendLine(@"mfs.Fund_Source_Id as id,");
            sb.AppendLine(@"mfs.Fund_Name as fund_name");
            sb.AppendLine(@"FROM mnda_fund_source mfs");
            sb.AppendLine(@"WHERE mfs.division_id = " + DivisionId + " and mfs.Year =" + _year + " AND mfs.code ='"+ FundSource +"'");

            return sb.ToString();
        }
        public String FetchOBRData(String _year, String _fundsource)
        {
            String _NowMonth = DateTime.Now.Month.ToString();

            var sb = new System.Text.StringBuilder(629);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"lib_a.AccountCode,");
            sb.AppendLine(@"lib_res.Division,");
            sb.AppendLine(@"MONTH(obr.DateCreated) as _Month,");
            sb.AppendLine(@"obr.DateCreated,");
            sb.AppendLine(@"SUM(obr_d.Amount) as Total");
            sb.AppendLine(@"FROM or_ObligationRequest obr");
            sb.AppendLine(@"INNER JOIN or_ObligationRequestParticular obr_d on obr.ID = obr_d.ObligationRequestID");
            sb.AppendLine(@"INNER JOIN  lib_ResponsibilityCenter lib_res on lib_res.ID = obr_d.ResponsibilityCenterID");
            sb.AppendLine(@"INNER JOIN lib_Account lib_a on lib_a.ID =obr_d.AcctID");
            sb.AppendLine(@"WHERE YEAR(obr.DateCreated)= " + _year + " and lib_res.Division ='" + _fundsource + "' and CHARINDEX('502', lib_a.AccountCode)>0 and NOT obr.ObligationRequestNo ='NO RECORD'");
            sb.AppendLine(@"AND MONTH(obr.DateCreated) <" + _NowMonth + "");
            sb.AppendLine(@"Group By obr_d.AcctID,lib_res.Division,MONTH(obr.DateCreated),obr.DateCreated,lib_a.AccountCode");
            sb.AppendLine(@"ORDER BY obr.DateCreated");

            return sb.ToString();
        }
        public String FetchFundSourceDetails(String DivisionId, String _year,String _FSource)
        {
            var sb = new System.Text.StringBuilder(417);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"  ");
            sb.AppendLine(@"   ISNULL(mfsl.Fund_Source_Id,'') as id,");
            sb.AppendLine(@"   ISNULL(mfs.Fund_Name,'') as Fund_Name,");
            sb.AppendLine(@"   ISNULL(mooe.name,'')  as Name,");
            sb.AppendLine(@"   ISNULL(mfsl.mooe_id,'')  as Mooe_Id,");
            sb.AppendLine(@"  SUM(ISNULL(mfsl.Amount,0)) as Allocation");
            sb.AppendLine(@"FROM mnda_fund_source mfs");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source_line_item mfsl on mfsl.Fund_Source_Id = mfs.code");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.uacs_code = mfsl.mooe_id");
            sb.AppendLine(@"WHERE mfs.division_id = "+ DivisionId +" and mfs.Year = "+ _year +" and mfs.code ='"+ _FSource +"'");
            sb.AppendLine(@"GROUP BY mfsl.Fund_Source_Id, mfs.Fund_Name,mooe.name,mfsl.mooe_id,mfsl.plusminus ");


            return sb.ToString();
        }
        public String FetchDivisionExpenditureDetails(String _Assigned, String _year,String FundSource)
        {
  
            //var sb = new System.Text.StringBuilder(480);
            //sb.AppendLine(@"SELECT");
            //sb.AppendLine(@"mad.fund_source_id,");
            //sb.AppendLine(@"mosub.name as expenditure,");
            //sb.AppendLine(@"SUM(mad.total) as total");
            //sb.AppendLine(@"FROM mnda_activity_data mad");
            //sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.id = mad.activity_id");
            //sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.id = ma.output_id");
            //sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mosub on mosub.id = mad.mooe_sub_expenditure_id");
            //sb.AppendLine(@"WHERE   mad.accountable_id ='"+ _Assigned  +"'");
            //sb.AppendLine(@"and mad.year = "+ _year +" and mad.is_suspended = 0");
            //sb.AppendLine(@"GROUP BY mad.fund_source_id,mosub.name");

            var sb = new System.Text.StringBuilder(480);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mad.fund_source_id,");
            sb.AppendLine(@"mosub.name as expenditure,");
            sb.AppendLine(@"SUM(mad.total) as total");
            sb.AppendLine(@"FROM mnda_activity_data mad");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.id = mad.activity_id");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.id = ma.output_id");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mosub on mosub.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"WHERE   mad.fund_source_id ='" + FundSource + "'");
            sb.AppendLine(@"and mad.year = " + _year + " and mad.is_suspended = 0");
            sb.AppendLine(@"GROUP BY mad.fund_source_id,mosub.name");

            return sb.ToString();
        }
        public String FetchOfficeTotals(String _office_id,String _year)
        {
            StringBuilder sb = new StringBuilder(471);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"div.Division_Id as id,");
            sb.AppendLine(@"div.Division_Desc as division,div.Division_Code as pap_code,");
            sb.AppendLine(@"mba.budget_allocation,");
            sb.AppendLine(@"0.00 as total_program_budget,");
            sb.AppendLine(@"0.00 as balance");
            sb.AppendLine(@"FROM   Division div ");
            sb.AppendLine(@"INNER JOIN DBM_Sub_Pap dsp on dsp.DBM_Sub_Pap_id = div.DBM_Sub_Pap_Id");
            sb.AppendLine(@"INNER JOIN mnda_budget_allocation mba on mba.division_id  = div.Division_Id");
            sb.AppendLine(@"WHERE dsp.DBM_Pap_Id ="+ _office_id +" and mba.entry_year = "+ _year +"");
            sb.AppendLine(@"GROUP BY div.Division_Id,div.Division_Code,div.Division_Desc,mba.budget_allocation");

            return sb.ToString();
        }

        public String FetchBudgetAllocation(String division_id, String _year)
        {
         //   var sb = new System.Text.StringBuilder(431);
         //   sb.AppendLine(@"SELECT ");
         //   sb.AppendLine(@"ISNULL(mooe.code,'') as pap_code,");
         //   sb.AppendLine(@"ISNULL(msub.uacs_code,'') as uacs_code,");
         //   sb.AppendLine(@"ISNULL(mfs.Fund_Name,'') as fund_name,");
         //   sb.AppendLine(@"ISNULL(mooe.name,'') as mo_name,");
         //   sb.AppendLine(@"ISNULL(msub.mooe_id,'') as pap_sub,");
         //   sb.AppendLine(@"ISNULL(msub.name,'') as Name,");
         //   sb.AppendLine(@"ISNULL(mfsl.Amount - ISNULL(mfsl.adjustment,0) ,'0') as Amount");
         //   sb.AppendLine(@"FROM mnda_fund_source mfs");
         //   sb.AppendLine(@"LEFT JOIN Division div on div.Division_Id = mfs.division_id");
         //   sb.AppendLine(@"LEFT JOIN mnda_fund_source_line_item mfsl on mfsl.Fund_Source_Id = mfs.code");
         ////   sb.AppendLine(@"LEFT JOIN mnda_fund_source mf on mf.Fund_Source_Id = mfsl.Fund_Source_Id");
         //   sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures msub on msub.uacs_code = mfsl.mooe_id");
         //   sb.AppendLine(@"INNER JOIN mnda_mooe_expenditures mooe on mooe.code = msub.mooe_id");
         //   sb.AppendLine(@"WHERE div.Division_Id ="+ division_id +" AND mfs.Year ='"+ _year +"'");
            var sb = new System.Text.StringBuilder(745);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"ISNULL(mooe.code,'') as pap_code,");
            sb.AppendLine(@"ISNULL(msub.uacs_code,'') as uacs_code,");
            sb.AppendLine(@"ISNULL(mfs.Fund_Name,'') as fund_name,");
            sb.AppendLine(@"ISNULL(mooe.name,'') as mo_name,");
            sb.AppendLine(@"ISNULL(msub.mooe_id,'') as pap_sub,");
            sb.AppendLine(@"ISNULL(msub.name,'') as Name,");
            sb.AppendLine(@"mfsl.Amount");
            sb.AppendLine(@"FROM mnda_fund_source mfs");
            sb.AppendLine(@"LEFT JOIN Division div on div.Division_Id = mfs.division_id");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source_line_item mfsl on mfsl.Fund_Source_Id = mfs.code");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures msub on msub.uacs_code = mfsl.mooe_id");
            sb.AppendLine(@"INNER JOIN mnda_mooe_expenditures mooe on mooe.code = msub.mooe_id");
            sb.AppendLine(@"WHERE div.Division_Id =" + division_id + " AND mfs.Year ='" + _year + "'");
            sb.AppendLine(@"GROUP BY ");
            sb.AppendLine(@"mooe.code,");
            sb.AppendLine(@"msub.uacs_code,");
            sb.AppendLine(@"mfs.Fund_Name,");
            sb.AppendLine(@"mooe.name,");
            sb.AppendLine(@"msub.mooe_id,");
            sb.AppendLine(@"msub.name,");
            sb.AppendLine(@"mfsl.plusminus,");
            sb.AppendLine(@"mfsl.adjustment,");
            sb.AppendLine(@"mfsl.Amount");

        return sb.ToString();
        }

        public String FetchDivisionFundSource(String division_id, String _year)
        {
            var sb = new System.Text.StringBuilder(95);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mrl.respo_name as Fund_Name,");
            sb.AppendLine(@"mrl.fund_type as Fund_Type");
            sb.AppendLine(@"FROM mnda_respo_library mrl");
            sb.AppendLine(@"WHERE mrl.div_id = '" + division_id + "' and mrl.working_year = '" + _year + "'");


            return sb.ToString();
        }
        public String FetchDivisionFundSourceCode(String division_id, String _year,String FundName)
        {
            var sb = new System.Text.StringBuilder(95);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mfs.code");
            sb.AppendLine(@"FROM mnda_fund_source mfs");
            sb.AppendLine(@"WHERE mfs.division_id = " + division_id + " and mfs.Year = " + _year + " and mfs.Fund_Name ='"+ FundName +"'");


            return sb.ToString();
        }

        public String FetchBalances(String _pap, String _year)
        {
            StringBuilder sb = new StringBuilder(424);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"lrc.Division,");
            sb.AppendLine(@"lppa.PPACode as Office,");
            sb.AppendLine(@"SUM(oorp.Amount) as Total");
            sb.AppendLine(@"FROM or_ObligationRequestParticular oorp");
            sb.AppendLine(@"LEFT  JOIN or_ObligationRequest obr on obr.id = oorp.ObligationRequestID");
            sb.AppendLine(@"LEFT  JOIN lib_ResponsibilityCenter lrc on lrc.ID = oorp.ResponsibilityCenterID");
            sb.AppendLine(@"LEFT  JOIN lib_PPA lppa ON lppa.ID = oorp.PPAID");
            sb.AppendLine(@"WHERE lppa.PPACode ='"+ _pap +"'");
            sb.AppendLine(@"AND YEAR(obr.DateCreated) = "+ _year +"");
            sb.AppendLine(@"GROUP BY lrc.Division ,lppa.PPACode");


            return sb.ToString();
        }
        public String FetchBalancesDivision(String _pap, String _year)
        {
            StringBuilder sb = new StringBuilder(424);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"lrc.Division,");
            sb.AppendLine(@"lppa.PPACode as Office,");
            sb.AppendLine(@"SUM(oorp.Amount) as Total");
            sb.AppendLine(@"FROM or_ObligationRequestParticular oorp");
            sb.AppendLine(@"LEFT  JOIN or_ObligationRequest obr on obr.id = oorp.ObligationRequestID");
            sb.AppendLine(@"LEFT  JOIN lib_ResponsibilityCenter lrc on lrc.ID = oorp.ResponsibilityCenterID");
            sb.AppendLine(@"LEFT  JOIN lib_PPA lppa ON lppa.ID = oorp.PPAID");
            sb.AppendLine(@"WHERE lppa.PPACode ='" + _pap + "'");
            sb.AppendLine(@"AND YEAR(obr.DateCreated) = " + _year + "");
            sb.AppendLine(@"GROUP BY lrc.Division ,lppa.PPACode");


            return sb.ToString();
        }
        public String FetchBalancesDB(String _pap, String _year)
        {
            StringBuilder sb = new StringBuilder(424);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  UACS,");
            sb.AppendLine(@"  PAP,");
            sb.AppendLine(@"  Jan,");
            sb.AppendLine(@"  Feb,");
            sb.AppendLine(@"  Mar,");
            sb.AppendLine(@"  Apr,");
            sb.AppendLine(@"  May,");
            sb.AppendLine(@"  June,");
            sb.AppendLine(@"  July,");
            sb.AppendLine(@"  Aug,");
            sb.AppendLine(@"  Sep,");
            sb.AppendLine(@"  Oct,");
            sb.AppendLine(@"  Nov,");
            sb.AppendLine(@"  Dec,");
            sb.AppendLine(@"  Years");
            sb.AppendLine(@"FROM dvDB dv");
            sb.AppendLine(@"WHERE dv.PAP = '"+ _pap +"' AND dv.Years = '"+ _year +"';");

            return sb.ToString();
        }
        public String FetchOfficeDetails(String _office_id, String _year)
        {
            StringBuilder sb = new StringBuilder(294);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mfs.Fund_Source_Id as fund_source_id,mfs.division_id as div_id,");
           //    sb.AppendLine(@"mfs.code as fund_source_id,mfs.division_id as div_id,");
            sb.AppendLine(@"mfs.Fund_Name,");
            sb.AppendLine(@"mfs.Amount as budget_allocation,");
            sb.AppendLine(@"0.00 as total_program_budget,");
            sb.AppendLine(@"0.00 as balance");
            sb.AppendLine(@"FROM  Division div ");
            sb.AppendLine(@"INNER JOIN DBM_Sub_Pap dsp on dsp.DBM_Sub_Pap_id = div.DBM_Sub_Pap_Id");
            sb.AppendLine(@"INNER JOIN mnda_fund_source mfs on mfs.division_id = div.Division_Id");
           // sb.AppendLine(@"WHERE dsp.DBM_Pap_Id = " + _office_id + " and mfs.Year = "+ _year +"");
            sb.AppendLine(@"WHERE div.DBM_Sub_Pap_Id = " + _office_id + " and mfs.Year = " + _year + "");
            sb.AppendLine(@"ORDER BY mfs.Fund_Name");


            return sb.ToString();
        }
        
        
        public String FetchOfficeExpenditures(String _office_id, String _year)
        {
            StringBuilder sb = new StringBuilder(742);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mad.fund_source_id,mpe.acountable_division_code division_id,");
            sb.AppendLine(@"div.Division_Desc as division,");
            sb.AppendLine(@"mp.id as program_id,");
            sb.AppendLine(@"mp.name as program,");
            sb.AppendLine(@"mooe.name as expenditure_name,");
            sb.AppendLine(@"mooe.code as expen_code,");
            sb.AppendLine(@"moe.mooe_id,");
            sb.AppendLine(@"moe.name as expense_name,");
            sb.AppendLine(@"moe.uacs_code,mad.month,mad.is_suspended as status,");
            sb.AppendLine(@"SUM(mad.total) as total");
            sb.AppendLine(@"FROM DBM_Sub_Pap sub");
            sb.AppendLine(@"INNER JOIN  Division div on div.DBM_Sub_Pap_Id = sub.DBM_Sub_Pap_id");
            sb.AppendLine(@"INNER JOIN mnda_program_encoded  mpe on mpe.acountable_division_code = div.Division_Id");
            sb.AppendLine(@"INNER JOIN mnda_programs mp  on mp.id = mpe.main_program_id");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.output_id = mpo.id");
            sb.AppendLine(@"INNER JOIN mnda_activity_data mad on mad.activity_id =  CONVERT(varchar(max),ma.id)");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures moe on moe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"INNER JOIN mnda_mooe_expenditures mooe on mooe.code = moe.mooe_id");
            sb.AppendLine(@"WHERE mad.year ="+ _year +" AND sub.DBM_Pap_Id = "+ _office_id +"  AND mad.id IN (");
            sb.AppendLine(@"SELECT mapd.activity_id FROM mnda_approved_projects_division mapd");
            sb.AppendLine(@")");
            sb.AppendLine(@"GROUP BY mad.fund_source_id, mpe.acountable_division_code ,");
            sb.AppendLine(@"div.Division_Desc,mp.id,mp.name,");
            sb.AppendLine(@"mooe.code,");
            sb.AppendLine(@"moe.mooe_id,");
            sb.AppendLine(@"mooe.name,");
            sb.AppendLine(@"moe.name,");
            sb.AppendLine(@"moe.uacs_code,mad.month,mad.is_suspended ");



            return sb.ToString();
        }
    }

    public class DivisionAssign 
    {
        public String Division_Id { get; set; }
        public String Division_Desc { get; set; }
        public String DivisionAccro { get; set; }
    }

    public class DivisionData 
    {
        public String Division_Id { get; set; }
        public String DBM_SUP_PAP_ID { get; set; }
        public String Division_Code { get; set; }
        public String Division_Desc { get; set; }
        public String Division_Accro { get; set; }
        public String Unit_Code { get; set; }
        public String Is_Sub_Division { get; set; }
        public String Year_Setup { get; set; }
    }

    public class OfficePaps 
    {
        public String Id { get; set; }
        public String Code { get; set; }
        public String Office { get; set; }
    }
    public class ChiefData_FetchDivisionFundSourceCode
    {
        public string Code { get; set; }
   
      

    }
    public class ChiefData_DivisionFundSource
    {
        public string Fund_Name { get; set; }

        public string FType { get; set; }
    }
    public class ChiefData_ListExpenditure 
    {
          public string fund_source_id { get; set; }
          public string expenditure { get; set; }
          public string total { get; set; }
            
    }
    public class ChiefData_FundSource 
    {
        public string FundName { get; set; }
        public List<ChiefData_FundSourceMainDetails> FundAllocation { get; set; }
    }

    public class ChiefData_FundSourceDASH
    {
        public string FundName { get; set; }
        public List<ChiefData_FundSourceMainDetailsDASH> FundAllocation { get; set; }
    }

    public class ChiefData_FundSourceMainDetailsDASH
    {
        public string id { get; set; }
        public string mooe_id { get; set; }
        public string fund_name { get; set; }
        public string name { get; set; }
        public string allocation { get; set; }

    

        public string total_expenditures { get; set; }
        public string total_obligated { get; set; }
        public string total_balance { get; set; }
        public string actual_balance { get; set; }


    }

    public class ChiefData_ItemLists 
    {
        public string  main_id{ get; set; }
        public string mpo_id{ get; set; }
        public string  prog_id{ get; set; }
        public string  project_id{ get; set; }
        public string id { get; set; }
        public string expenditure { get; set; }
        public double total { get; set; }
        public string months { get; set; }
        public string is_revision { get; set; }
    }
    public class Approved_Data
    {
        public string Code { get; set; }
    }
    public class ChiefData_FundSourceMain 
    {
        public string id { get; set; }
        public string fund_source { get; set; }
        public string fund_type { get; set; }
    }
    public class ChiefData_FundSourceMainDetails
    {
        public string id { get; set; }
        public string fund_name { get; set; }
        public string name { get; set; }
        public string allocation { get; set; }

        public string Jan { get; set; }
        public string Feb { get; set; }
        public string Mar { get; set; }
        public string Apr { get; set; }
        public string May { get; set; }
        public string Jun { get; set; }
        public string Jul { get; set; }
        public string Aug { get; set; }
        public string Sep { get; set; }
        public string Oct { get; set; }
        public string Nov { get; set; }
        public string Dec { get; set; }

        public string total_expenditures { get; set; }
        public string total_obligated { get; set; }
        public string total_balance { get; set; }
        public string total_budget { get; set; }


    }
    public class BudgetExpenditureList 
    {
        public String uacs_code { get; set; }
         public String pap_code {get;set;}
         public String  mo_name {get;set;}
         public String Fund_Source { get; set; }
         public String pap_sub {get;set;}
         public String Name {get;set;}
         public String Amount { get; set; }
    }
    public class BudgetExpenitureMain 
    {
        public String MOOE_NAME { get; set; }
       
        public List<BudgetExpenditureDashBoard> ListExpenditure{get;set;}
    }

    public class BudgetExpenditureDashBoard     
    {
        public String UACS { get; set; }
        public String Source { get; set; }
        public String Expenditure { get; set; }
        public String Total { get; set; }
    }
    public class BudgetBalances 
    {
      public String Division {get;set;}
        public String  Office {get;set;}
        public String Total { get; set; }
    }
    public class OfficeTotals 
    {
             public String id {get;set;}
             public String  division  {get;set;}
             public String pap_code { get; set; }
             public String budget_allocation {get;set;}
             public String total_program_planned { get; set; }
             public String  total_program_budget {get;set;}
             public String balance { get; set; }
             public List<OfficeDetails> ListDetails { get; set; }
    }
    public class OfficeDetails
    {
        public String fund_source_id { get; set; }
        public String division_id { get; set; }
        public String Fund_Source { get; set; }
        public String pap_code { get; set; }
        public String budget_allocation { get; set; }
        public String total_program_planned { get; set; }
        public String total_program_budget { get; set; }
        public String balance { get; set; }
      
    }
    public class OfficeLevel 
    {
        public String Office { get; set; }
        public String Pap { get; set; }
        public List<DivisionLevel> DivisionList { get; set; }
        public String UACS { get; set; }
      //  public string Alloted { get; set; }
        public string Jan { get; set; }
        public string Feb { get; set; }
        public string Mar { get; set; }
        public string Apr { get; set; }
        public string May { get; set; }
        public string Jun { get; set; }
        public string Jul { get; set; }
        public string Aug { get; set; }
        public string Sep { get; set; }
        public string Oct { get; set; }
        public string Nov { get; set; }
        public string Dec { get; set; }
        public string Total { get; set; }
   //     public string Balance { get; set; }

    }

    public class DivisionLevel
    {
        public String id { get; set; }
        public String Division { get; set; }
        public List<ProgramLevel> Programs { get; set; }
        public String UACS { get; set; }
     //   public string Alloted { get; set; }
        public string Jan { get; set; }
        public string Feb { get; set; }
        public string Mar { get; set; }
        public string Apr { get; set; }
        public string May { get; set; }
        public string Jun { get; set; }
        public string Jul { get; set; }
        public string Aug { get; set; }
        public string Sep { get; set; }
        public string Oct { get; set; }
        public string Nov { get; set; }
        public string Dec { get; set; }
        public string Total { get; set; }
      //  public string Balance { get; set; }
   
    }
    public class ProgramLevel
    {
        public String id { get; set; }
        public String Program { get; set; }
        public List<ExpenditureTitles> Expenditures { get; set; }
        public String UACS { get; set; }
     //   public string Alloted { get; set; }
        public string Jan { get; set; }
        public string Feb { get; set; }
        public string Mar { get; set; }
        public string Apr { get; set; }
        public string May { get; set; }
        public string Jun { get; set; }
        public string Jul { get; set; }
        public string Aug { get; set; }
        public string Sep { get; set; }
        public string Oct { get; set; }
        public string Nov { get; set; }
        public string Dec { get; set; }
        public string Total { get; set; }
      //  public string Balance { get; set; }

    }
    public class ExpenditureTitles 
    {
        public String Code { get; set; }
        public String Expenditures { get; set; }
        public List<ExpenseItems> ExpenseList {get;set;}
        public String UACS { get; set; }
     //   public string Alloted { get; set; }
        public string Jan { get; set; }
        public string Feb { get; set; }
        public string Mar { get; set; }
        public string Apr { get; set; }
        public string May { get; set; }
        public string Jun { get; set; }
        public string Jul { get; set; }
        public string Aug { get; set; }
        public string Sep { get; set; }
        public string Oct { get; set; }
        public string Nov { get; set; }
        public string Dec { get; set; }
        public string Total { get; set; }
      //  public string Balance { get; set; }
    }
    public class ExpenseItemsList
    {
        public String fund_source_id { get; set; }
          public String  program_id { get; set; }
          public String program { get; set; }   
        public String month { get; set; }
            public String status { get; set; }
            public String  expen_code { get; set; }
            public String  mooe_id { get; set; }
            public String  division_id { get; set; }
            public String  division { get; set; }
            public String  expenditure_name { get; set; }
            public String  expense_name { get; set; }
            public String  uacs_code { get; set; }
            public String  total { get; set; }
    }
    public class ExpenseItems
    {
        public String id { get; set; }
        public String ExpenseType { get; set; }
        public String UACS { get; set; }
    //    public string Alloted { get; set; }
        public string Jan { get; set; }
        public string Feb { get; set; }
        public string Mar { get; set; }
        public string Apr { get; set; }
        public string May { get; set; }
        public string Jun { get; set; }
        public string Jul { get; set; }
        public string Aug { get; set; }
        public string Sep { get; set; }
        public string Oct { get; set; }
        public string Nov { get; set; }
        public string Dec { get; set; }
        public string Total { get; set; }
      //  public string Balance { get; set; }
    }

    public class BalancesDB
    {

        public String UACS { get; set; }
        public string PAP { get; set; }
        public string Jan { get; set; }
        public string Feb { get; set; }
        public string Mar { get; set; }
        public string Apr { get; set; }
        public string May { get; set; }
        public string Jun { get; set; }
        public string Jul { get; set; }
        public string Aug { get; set; }
        public string Sep { get; set; }
        public string Oct { get; set; }
        public string Nov { get; set; }
        public string Dec { get; set; }
        public string Years { get; set; }
    }
}
