﻿using System;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsDivisionUnit
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();

        public String GetDivisionUnits(string _main)
        {
            var sb = new System.Text.StringBuilder(132);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  Division_Id as div_id,");
            sb.AppendLine(@"  Division_Code as unit_code,");
            sb.AppendLine(@"  Division_Desc as unit_name");         
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.Division WHERE UnitCode = '" + _main + "';");

            
            return sb.ToString();
        }
      
     


        static string GetNewCode(int length,string _name)
        {
            StringBuilder code = new StringBuilder();
            StringBuilder _new = new StringBuilder();
            code.AppendLine(_name);

            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] rnd = new byte[1];
            int n = 0;
            while (n < code.Length)
            {
                rng.GetBytes(rnd);
                char c = (char)rnd[0];
                if ((Char.IsDigit(c) || Char.IsLetter(c)) && rnd[0] < 127)
                {
                    ++n;
                    _new.Append(rnd[0]);
                    if (n>=length)
                    {
                        break;
                    }
                }
            }
            return _new.ToString();
        }
     
        public void SaveDivisionUnit(string _main_division,string _name)
        {
            String _sqlString = "";
            String _Code = _name+"-" + GetNewCode(4,_name);
           
            var sb = new System.Text.StringBuilder(257);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.Division");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  DBM_Sub_Pap_Id,");
            sb.AppendLine(@"  Division_Code,");
            sb.AppendLine(@"  Division_Desc,");
            sb.AppendLine(@"  DivisionAccro,");
            sb.AppendLine(@"  UnitCode");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '-',");
            sb.AppendLine(@"  '"+ _Code +"',");
            sb.AppendLine(@"  '"+ _name +"',");
            sb.AppendLine(@"  '-',");
            sb.AppendLine(@"  '"+ _main_division +"'");
            sb.AppendLine(@");");

            _sqlString = sb.ToString();


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void RemoveDivisionUnit(string _code)
        {
            String _sqlString = "";
            var sb = new System.Text.StringBuilder(48);
            sb.AppendLine(@"DELETE FROM ");
            sb.AppendLine(@"  dbo.Division");
            sb.AppendLine(@"WHERE Division_Code ='" + _code + "';");

            _sqlString = sb.ToString();

            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void CreateBudget(String DivID,String FundCode,String WorkingYear,String DivName)
        {
            String _sqlString = "";
            StringBuilder varname1 = new StringBuilder();
            varname1.Append("INSERT INTO \n");
            varname1.Append("  dbo.mnda_respo_library \n");
            varname1.Append("( \n");
            varname1.Append("  respo_name, \n");
            varname1.Append("  code, \n");
            varname1.Append("  is_division, \n");
            varname1.Append("  is_used, \n");
            varname1.Append("  div_id, \n");
            varname1.Append("  fund_type, \n");
            varname1.Append("  working_year \n");
            varname1.Append(") \n");
            varname1.Append("VALUES ( \n");
            varname1.Append("  '" + DivName + "', \n");
            varname1.Append("   '"+ FundCode +"', \n");
            varname1.Append("  0, \n");
            varname1.Append("  0, \n");
            varname1.Append("  '"+ DivID +"', \n");
            varname1.Append("  'mf-1', \n");
            varname1.Append("  '"+ WorkingYear +"' \n");
            varname1.Append(");");


            _sqlString = varname1.ToString();

            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }

        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "CreateBudget":
                    if (this.SQLOperation!=null)
                    {
                        this.SQLOperation(this, new EventArgs());
                    }
                    break;
                case "SaveUnit":
                    if (this.SQLOperation!=null)
                    {
                        this.SQLOperation(this, new EventArgs());
                    }
                    break;
                case "RemoveUnit":
                    if (this.SQLOperation != null)
                    {
                        this.SQLOperation(this, new EventArgs());
                    }
                    break;
            }
        }


    }

    public class DivisionUnit
    {
        public String DivId { get; set; }
        public String Code { get; set; }
        public String Name { get; set; }


    }
}
