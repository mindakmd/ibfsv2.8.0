﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsPAP
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();


        public void SaveNewPap(String PAPCode, String PAPName, String Accro, String WorkingYear)
        {
            String _sqlString = "";
         

                StringBuilder varname1 = new StringBuilder();
                varname1.Append("INSERT INTO DBM_Sub_Pap(DBM_Pap_Id, DBM_Sub_Pap_Code, DBM_Sub_Pap_Desc, Acrro, WorkingYear) \n");
                varname1.Append("VALUES('0', '"+PAPCode +"', '"+ PAPName +"', '"+ Accro +"', '"+ WorkingYear +"');");

                _sqlString = varname1.ToString();
          


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        private Boolean isDone = false;
        void c_ops_DataReturn(object sender, EventArgs e)
        {
            ReturnCode = c_ops.ReturnCode;
            switch (Process)
            {
                case "SaveNewPap":
                    if (isDone)
                    {
                        isDone = false;
                        break;
                    }
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                        isDone = true;
                    }
                    break;
            }
        }

    }
}
