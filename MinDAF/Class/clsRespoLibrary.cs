﻿using System;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsRespoLibrary
    {

        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();

        public String FetchRespoLibrary(Int32 isDivision,String _respoCode,String _working_year)
        {
            var sb = new System.Text.StringBuilder(62);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  respo_name,fund_type");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_respo_library WHERE is_division = " + isDivision + " AND fund_type ='" + _respoCode + "' AND working_year ='"+ _working_year +"';");


            return sb.ToString();
        }
        public String FetchRespoTypes()
        {
            var sb = new System.Text.StringBuilder(70);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mfst.code,");
            sb.AppendLine(@"mfst.description");
            sb.AppendLine(@"FROM mnda_fund_source_types mfst");


            return sb.ToString();
        }
        public static string getHashSha256(string text)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }
        public void SaveRespo(String _name,Int32 isDivision,String _respoCode,String _working_year)
        {
            String _sqlString = "";
            String FundCode = "FS{" + getHashSha256(Guid.NewGuid().ToString()) + "}";
            var sb = new System.Text.StringBuilder(88);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_respo_library");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  respo_name,code,is_division,fund_type,working_year");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '"+ _name +"','"+ FundCode +"',"+ isDivision +",'"+ _respoCode +"','"+ _working_year +"'");
            sb.AppendLine(@");");



            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void UpdateRespo(String _id,String _name,String _working_year)
        {
            String _sqlString = "";

            var sb = new System.Text.StringBuilder(91);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_respo_library  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  respo_name ='"+ _name +"'");
            sb.AppendLine(@"WHERE ");
            sb.AppendLine(@"  id = " + _id + " AND working_year ='" + _working_year + "'");



            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void DeleteRespo(String _id,String _working_year)
        {
            String _sqlString = "";

            var sb = new System.Text.StringBuilder(48);
            sb.AppendLine(@"DELETE FROM ");
            sb.AppendLine(@"  dbo.mnda_respo_library ");
            sb.AppendLine(@"WHERE id =" + _id + " AND working_year ='" + _working_year + "' ;");


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "SaveRespo":
                    if (SQLOperation!=null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateRespo":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "DeleteRespo":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
            }
        }

    }

    public class RespoTypes
    {
        public String Code { get; set; }
        public String Description { get; set; }
    }
    public class RespoData 
    {
        public String Id { get; set; }
        public String Name { get; set; }
        public String FundType { get; set; }
    }
}
