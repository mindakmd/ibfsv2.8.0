﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsRevisionStatus
    {

        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();

        public String FetchRevisionList(String WorkingYear,String TypeList) 
        {
            //var sb = new System.Text.StringBuilder(309);
            //sb.AppendLine(@"SELECT");
            //sb.AppendLine(@"");
            //sb.AppendLine(@"div.Division_Desc as divname,");
            //sb.AppendLine(@"CASE WHEN mar.is_approved=1  THEN 'APPROVED - For REVISION PPMP'");
            //sb.AppendLine(@" ELSE  'FOR APPROVAL'  END as status");
            //sb.AppendLine(@"");
            //sb.AppendLine(@"FROM mnda_alignment_record mar");
            //sb.AppendLine(@"INNER JOIN Division div on div.Division_Id = mar.division_pap");
            //sb.AppendLine(@"WHERE mar.is_active = 1 and ");
            //sb.AppendLine(@"GROUP BY div.Division_Desc,mar.is_approved");
            StringBuilder varname1 = new StringBuilder();
            varname1.Append("SELECT \n");
            varname1.Append("raf.number as RafNo, \n");
            varname1.Append("div.Division_Desc as divname, \n");
            varname1.Append("raf.status \n");
            varname1.Append("FROM mnda_raf_no raf \n");
            varname1.Append("INNER JOIN Division div on div.Division_Id = raf.div_id \n");
            varname1.Append("WHERE raf.year = "+WorkingYear+" and div.year_setup = "+WorkingYear+" \n");
            varname1.Append("and raf.mf_type = '"+TypeList+"' \n");
            varname1.Append("ORDER BY raf.id ASC");


            return varname1.ToString();
        }

    }

    public class RevisionStatus 
    {
        public String RafNo { get; set; }
        public String DivisionName { get; set; }
        public String Status { get; set; }
    }

}
