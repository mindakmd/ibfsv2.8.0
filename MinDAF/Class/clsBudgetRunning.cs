﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsBudgetRunning
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();


        public String FetchBudgetRafNo(String div_id, String _year, String mfType)
        {
            var sb = new System.Text.StringBuilder(105);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"raf.number");
            sb.AppendLine(@"FROM mnda_raf_no raf");
            sb.AppendLine(@"WHERE raf.div_id = " + div_id + "  AND raf.year = " + _year + "  AND raf.cancelled =0 AND raf.mf_type ='" + mfType + "' and raf.status ='Served'");

            return sb.ToString();
        }
        public String FetchDivisionFundSource(String division_id, String _year)
        {
            var sb = new System.Text.StringBuilder(95);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mrl.code as Fund_Name,");
            sb.AppendLine(@"mrl.fund_type as Fund_Type");
            sb.AppendLine(@"FROM mnda_respo_library mrl");
            sb.AppendLine(@"WHERE mrl.div_id = '" + division_id + "' and mrl.working_year = '" + _year + "'");


            return sb.ToString();
        }
        public String FetchBudgetAdjustments(String _divid, String _year,String Uacs,String FSource)
        {
            //var sb = new System.Text.StringBuilder(229);
            //sb.AppendLine(@"SELECT");
            //sb.AppendLine(@"raf.number,");
            //sb.AppendLine(@"mfsa.mooe_id,");
            //sb.AppendLine(@"mfsa.adjustment,");
            //sb.AppendLine(@"mfsa.plusminus");
            //sb.AppendLine(@"FROM mnda_raf_no raf");
            //sb.AppendLine(@"INNER JOIN mnda_fund_source_adjustment mfsa on mfsa.raf_no = raf.number");
            //sb.AppendLine(@"WHERE raf.div_id = " + _divid + "  AND raf.year = " + _year + " AND raf.cancelled =0 AND mfsa.mooe_id ='"+ Uacs +"'");
            StringBuilder varname1 = new StringBuilder();
            varname1.Append("SELECT \n");
            varname1.Append("raf.number, \n");
            varname1.Append("mfsa.mooe_id, \n");
            varname1.Append("mfsa.adjustment, \n");
            varname1.Append("mfsa.plusminus \n");
            varname1.Append("FROM mnda_raf_no raf \n");
            varname1.Append("INNER JOIN mnda_fund_source_adjustment mfsa on mfsa.raf_no = raf.number \n");
            varname1.Append("WHERE raf.div_id = "+_divid +"  AND raf.year = "+ _year +" AND raf.cancelled =0 \n");
            varname1.Append("AND mfsa.fund_code ='" + FSource + "' AND mfsa.mooe_id = '"+ Uacs +"' \n");
            varname1.Append("GROUP BY \n");
            varname1.Append("raf.number, \n");
            varname1.Append("mfsa.mooe_id, \n");
            varname1.Append("mfsa.adjustment, \n");
            varname1.Append("mfsa.plusminus");

            return varname1.ToString();
        }
        public String GetCurrentRunningBalance(String _div_id , String _year,String _fundsource,string uacs,String uacs_index)
        {
            //StringBuilder sb = new StringBuilder(868);
           
            //sb.AppendLine(@"SELECT _data.uacs_code,_data.name,SUM(_data.total) as total,_data.balance  FROM (SELECT ");
            //sb.AppendLine(@"'' as uacs_code,");
            //sb.AppendLine(@"'Beginning Balance' as name,");
            //sb.AppendLine(@"ISNULL(SUM(mfsl.Amount),0) as total,");
            //sb.AppendLine(@"ISNULL(SUM(mfsl.Amount),0) as balance");
            //sb.AppendLine(@"FROM mnda_fund_source mfs");
            //sb.AppendLine(@"INNER JOIN mnda_fund_source_line_item mfsl on mfsl.Fund_Source_Id = mfs.Fund_Source_Id");
            //sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.uacs_code = mfsl.mooe_id");
            //sb.AppendLine(@"WHERE mfs.division_id = " + _div_id + " and mfs.Year = " + _year + " and mfs.Fund_Source_Id = " + _fundsource + " and mooe.id=" + uacs + "");
            //sb.AppendLine(@"UNION");
            //sb.AppendLine(@"SELECT");
            //sb.AppendLine(@"mooe.uacs_code,");
            //sb.AppendLine(@"mooe.name,");
            //sb.AppendLine(@"SUM(mad.total) as total,");
            //sb.AppendLine(@"0.00 as balance");
            //sb.AppendLine(@"FROM mnda_activity_data mad");
            //sb.AppendLine(@"LEFT JOIN mnda_activity ma on ma.id= mad.activity_id");
            //sb.AppendLine(@"LEFT JOIN mnda_project_output mpo on mpo.id = ma.output_id");
            //sb.AppendLine(@"LEFT JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
            //sb.AppendLine(@"LEFT JOIN mnda_main_program_encoded mmpe on mmpe.program_code = mpe.main_program_id");
            //sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            //sb.AppendLine(@"WHERE   mooe.id='" + uacs + "' AND mad.is_suspended =0 AND mmpe.accountable_division = " + _div_id + " and mmpe.fiscal_year = " + _year + " and mad.fund_source_id = " + _fundsource + " GROUP BY mooe.uacs_code,mooe.name,mad.total ) _data");
            //sb.AppendLine(@"GROUP BY _data.uacs_code, _data.name,_data.balance");

            //var sb = new System.Text.StringBuilder(1271);
            //sb.AppendLine(@"SELECT _databeg.uacs_code,_databeg.name,_databeg.total,_databeg.balance  FROM (SELECT ");
            //sb.AppendLine(@"'' as uacs_code,");
            //sb.AppendLine(@"'Beginning Balance' as name,");
            //sb.AppendLine(@"ISNULL(SUM(mfsl.Amount),0) as total,");
            //sb.AppendLine(@"ISNULL(SUM(mfsl.Amount),0) as balance");
            //sb.AppendLine(@"FROM mnda_fund_source mfs");
            //sb.AppendLine(@"INNER JOIN mnda_fund_source_line_item mfsl on mfsl.Fund_Source_Id = mfs.Fund_Source_Id");
            //sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.uacs_code = mfsl.mooe_id");
            //sb.AppendLine(@"WHERE mfs.division_id = " + _div_id + " and mfs.Year = " + _year + " and mfs.Fund_Source_Id = " + _fundsource + " and mooe.id=" + uacs + " ) _databeg");
            //sb.AppendLine(@"UNION");
            //sb.AppendLine(@"SELECT _data.uacs_code, _data.name,SUM(_data.total) as total,_data.balance FROM (SELECT DISTINCT");
            //sb.AppendLine(@"mad.id,");
            //sb.AppendLine(@"mooe.uacs_code,");
            //sb.AppendLine(@" mooe.name,");
            //sb.AppendLine(@"mad.total,");
            //sb.AppendLine(@"0.00 as balance");
            //sb.AppendLine(@"FROM mnda_activity_data mad");
            //sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.id= mad.activity_id");
            //sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.id = ma.output_id");
            //sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
            //sb.AppendLine(@"INNER JOIN mnda_main_program_encoded mmpe on mmpe.program_code = mpe.main_program_id");
            //sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            //sb.AppendLine(@"WHERE   mooe.id='" + uacs + "' AND mad.is_suspended =0 AND mmpe.accountable_division = " + _div_id + " ");
            //sb.AppendLine(@"and mmpe.fiscal_year = " + _year + " and mad.fund_source_id =" + _fundsource + " ) _data");
            //sb.AppendLine(@"GROUP BY _data.uacs_code, _data.name,_data.total,_data.balance");

            var sb = new System.Text.StringBuilder(1077);
            sb.AppendLine(@"SELECT _data.uacs_code,_data.name,SUM(_data.total) as total,_data.balance FROM(SELECT ");
            sb.AppendLine(@"'' as uacs_code,");
            sb.AppendLine(@"'Beginning Balance' as name,");
            sb.AppendLine(@"mfsl.Amount as total,");
            sb.AppendLine(@"mfsl.Amount as balance");
            sb.AppendLine(@"FROM mnda_fund_source mfs");
            sb.AppendLine(@"INNER JOIN mnda_fund_source_line_item mfsl on mfsl.Fund_Source_Id = mfs.code");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.uacs_code = mfsl.mooe_id");
            sb.AppendLine(@"WHERE mfs.division_id = " + _div_id + " and mfs.Year = " + _year + " and mfs.code = '" + _fundsource + "' and mooe.uacs_code='" + uacs + "'");
            sb.AppendLine(@"GROUP BY mfsl.plusminus,mfsl.Amount) _data");
            sb.AppendLine(@"GROUP BY _data.uacs_code,_data.name,_data.balance");
            sb.AppendLine(@"UNION");
            sb.AppendLine(@"SELECT _data.uacs_code,_data.name,SUM(_data.total) as total,_data.balance FROM ( SELECT ");
            sb.AppendLine(@"mooe.uacs_code,");
            sb.AppendLine(@"mooe.name,");
            sb.AppendLine(@"SUM(mad.total) as total,");
            sb.AppendLine(@"0.00 as balance");
            sb.AppendLine(@"FROM mnda_activity_data mad");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.id= mad.activity_id");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"WHERE   mad.mooe_sub_expenditure_id='" + uacs_index + "' AND mad.is_suspended =0 and mad.fund_source_id ='" + _fundsource + "' ");
            sb.AppendLine(@"GROUP BY mooe.uacs_code,mooe.name ) _data");
            sb.AppendLine(@"GROUP BY _data.uacs_code,_data.name,_data.balance");


            return sb.ToString();
        }

    }

    public class BugdetBalanceFields
    {
        public String uacs_code {get;set;}
        public String name {get;set;}
        public String total {get;set;}
        public String balance { get; set; }
    }
}
