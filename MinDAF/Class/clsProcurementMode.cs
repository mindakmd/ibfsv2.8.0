﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsProcurementMode
    {


        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();


        public String LoadProcurementLibrary()
        {
            StringBuilder sb = new StringBuilder(237);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"id as Id,");
            sb.AppendLine(@"mode_procurement as Procurement");
            sb.AppendLine(@"FROM  dbo.mnda_mode_procurement_lib;");


            return sb.ToString();
        }
        public String LoadFundSource(string _div_id, string _year)
        {
            var sb = new System.Text.StringBuilder(282);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mooe.name, ");
            sb.AppendLine(@"  mfsl.Fund_Source_Id,");
            sb.AppendLine(@"  mfsl.division_id,");
            sb.AppendLine(@"  mfsl.mooe_id,");
            sb.AppendLine(@"  mfsl.Amount,");
            sb.AppendLine(@"  mfsl.Year");
            sb.AppendLine(@"FROM   dbo.mnda_fund_source_line_item mfsl");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.uacs_code = mfsl.mooe_id");
            sb.AppendLine(@"WHERE mfsl.division_id = "+ _div_id +" AND mfsl.Year = "+ _year +";");



            return sb.ToString();
        }
        public void UpdateRow(String _id,String _procurement)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(285);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_mode_procurement_lib");
            sb.AppendLine(@"SET  ");
            sb.AppendLine(@"mode_procurement = '"+ _procurement +"'");
            sb.AppendLine(@"WHERE id = "+ _id +";");


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
  
        public String FetchAlignmentData(string _div_id, string _year)
        {
            var sb = new System.Text.StringBuilder(297);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  mar.months,");
            sb.AppendLine(@"  mfs.Fund_Name as fundsource,");
            sb.AppendLine(@"  mar.from_uacs,");
            sb.AppendLine(@"  mar.from_total,");
            sb.AppendLine(@"  mmoe.name as mooe,");
            sb.AppendLine(@"  mmo.name,");
            sb.AppendLine(@"  mar.to_uacs,");
            sb.AppendLine(@"  mar.total_alignment");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_alignment_record mar");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mmo on mmo.uacs_code = mar.to_uacs");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_expenditures mmoe on mmoe.code = mmo.mooe_id");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source mfs on mfs.code = mar.fundsource");
            sb.AppendLine(@"  WHERE mar.division_pap =" + _div_id + " AND mar.division_year =" + _year + ";");


            return sb.ToString();
        }
        public String FetchAlignmentDataAPP( string _year)
        {
            var sb = new System.Text.StringBuilder(297);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  mar.months,");
            sb.AppendLine(@"  mfs.Fund_Name as fundsource,");
            sb.AppendLine(@"  mar.from_uacs,");
            sb.AppendLine(@"  mar.from_total,");
            sb.AppendLine(@"  mmoe.name as mooe,");
            sb.AppendLine(@"  mmo.name,");
            sb.AppendLine(@"  mar.to_uacs,");
            sb.AppendLine(@"  mar.total_alignment");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_alignment_record mar");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mmo on mmo.uacs_code = mar.to_uacs");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_expenditures mmoe on mmoe.code = mmo.mooe_id");
            sb.AppendLine(@"LEFT JOIN mnda_fund_source mfs on mfs.code = mar.fundsource");
            sb.AppendLine(@"  WHERE  mar.division_year =" + _year + ";");


            return sb.ToString();
        }
        public void AddNewRow()
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(285);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_mode_procurement_lib");
            sb.AppendLine(@" ( mode_procurement)");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '  '");
            sb.AppendLine(@");");


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }

        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "AddNewRow":
                    if (SQLOperation!=null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateRow":
                    if (SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
            }
        }


    }
 
    public class ProcurementData 
    {
        public String Id { get; set; }
        public String Procurement { get; set; }
    }

    public class PPMPFundSource
    {
              public String  name { get; set; }
               public String Fund_Source_Id { get; set; }
               public String division_id { get; set; }
               public String mooe_id { get; set; }
               public String Amount { get; set; }
               public String Year { get; set; }
    }
}
