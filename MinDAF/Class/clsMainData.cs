﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsMainData
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();
        public String FetchFundSource(String _division_id)
        {
            var sb = new System.Text.StringBuilder(94);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"respo.respo_name,");
            sb.AppendLine(@"respo.code");
            sb.AppendLine(@"FROM mnda_respo_library respo ");
            sb.AppendLine(@"WHERE respo.div_id = " + _division_id + " ");


            return sb.ToString();
        }
        public String FetchOBRData(String _year, String _fundsource,String _month)
        {
            var sb = new System.Text.StringBuilder(629);
            switch (_month)
            {
                case "Jan": _month = "1"; break;
                case "Feb": _month = "2"; break;
                case "Mar": _month = "3"; break;
                case "Apr": _month = "4"; break;
                case "May": _month = "5"; break;
                case "Jun": _month = "6"; break;
                case "Jul": _month = "7"; break;
                case "Aug": _month = "8"; break;
                case "Sep": _month = "9"; break;
                case "Oct": _month = "10"; break;
                case "Nov": _month = "11"; break;
                case "Dec": _month = "12"; break;
            }
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"lib_a.AccountCode,");
            sb.AppendLine(@"lib_res.Division,");
            sb.AppendLine(@"MONTH(obr.DateCreated) as _Month,");
            sb.AppendLine(@"obr.DateCreated,");
            sb.AppendLine(@"SUM(obr_d.Amount) as Total");
            sb.AppendLine(@"FROM or_ObligationRequest obr");
            sb.AppendLine(@"INNER JOIN or_ObligationRequestParticular obr_d on obr.ID = obr_d.ObligationRequestID");
            sb.AppendLine(@"INNER JOIN  lib_ResponsibilityCenter lib_res on lib_res.ID = obr_d.ResponsibilityCenterID");
            sb.AppendLine(@"INNER JOIN lib_Account lib_a on lib_a.ID =obr_d.AcctID");
            sb.AppendLine(@"WHERE MONTH(obr.DateCreated) = " + _month + " AND YEAR(obr.DateCreated)= " + _year + " and lib_res.Division ='" + _fundsource + "' and CHARINDEX('502', lib_a.AccountCode)>0 and NOT obr.ObligationRequestNo ='NO RECORD'");
            sb.AppendLine(@"Group By obr_d.AcctID,lib_res.Division,MONTH(obr.DateCreated),obr.DateCreated,lib_a.AccountCode");
            sb.AppendLine(@"ORDER BY obr.DateCreated");

            return sb.ToString();
        }
        public void AddNewRow(String Division_ID,String FundName,String WorkingYear,String User)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(256);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_main_program_encoded");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  fiscal_year,");
            sb.AppendLine(@"  major_final_output_code,");
            sb.AppendLine(@"  performance_indicator_code,");
            sb.AppendLine(@"  program_code,");
            sb.AppendLine(@"  category_code,");
            sb.AppendLine(@"  accountable_office_code,accountable_division,fund_source,accountable_person");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '"+ WorkingYear +"',");
            sb.AppendLine(@"  NULL,");
            sb.AppendLine(@"  NULL,");
            sb.AppendLine(@"  NULL,");
            sb.AppendLine(@"  NULL,");
            sb.AppendLine(@"  NULL,'"+ Division_ID +"',");
            sb.AppendLine(@"  '"+ FundName  +"',");
            sb.AppendLine(@"  '" + User + "'");
            sb.AppendLine(@");");



            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void RemoveData(String prog_id,String proj_id,String _output_id,String _activity_id,String _selection)
        {
            String _sqlString = "";
            var sb = new System.Text.StringBuilder(737);
            switch (_selection)
            {
                case "activity":
                    sb.AppendLine(@"DELETE FROM  dbo.mnda_activity");        
                    sb.AppendLine(@"WHERE id = " + _activity_id + ";");
                    sb.AppendLine(@"");
                    sb.AppendLine(@"DELETE FROM  dbo.mnda_activity_data");
                    sb.AppendLine(@"WHERE id = " + _activity_id + ";");
                    break;
                case "output":
                    //sb.AppendLine(@"DELETE FROM dbo.mnda_main_program_encoded");
                    //sb.AppendLine(@"WHERE id = " + prog_id + ";");
                    //sb.AppendLine(@"");
                    //sb.AppendLine(@"DELETE FROM  dbo.mnda_program_encoded");           
                    //sb.AppendLine(@"WHERE id = " + proj_id + ";");
                    //sb.AppendLine(@"");         
                    sb.AppendLine(@"DELETE FROM  dbo.mnda_project_output");
                    sb.AppendLine(@"WHERE id = " + _output_id + ";");
                    sb.AppendLine(@"");
                    sb.AppendLine(@"DELETE FROM  dbo.mnda_activity");        
                    sb.AppendLine(@"WHERE id = " + _activity_id + ";");
                    sb.AppendLine(@"");
                    sb.AppendLine(@"DELETE FROM  dbo.mnda_activity_data");
                    sb.AppendLine(@"WHERE id = " + _activity_id + ";");
                    break;
                case "project":
                    sb.AppendLine(@"DELETE FROM  dbo.mnda_program_encoded");           
                    sb.AppendLine(@"WHERE id = " + proj_id + ";");
                    sb.AppendLine(@"");         
                    break;
                case "program":
                    sb.AppendLine(@"DELETE FROM dbo.mnda_main_program_encoded");
                    sb.AppendLine(@"WHERE id = " + prog_id + ";");
                    sb.AppendLine(@"");
                    break;
                default:
                    break;
            }
            //var sb = new System.Text.StringBuilder(737);
            //sb.AppendLine(@"UPDATE dbo.mnda_main_program_encoded ");
            //sb.AppendLine(@"SET  ");
            //sb.AppendLine(@"	fiscal_year = fiscal_year + '-',");
            //sb.AppendLine(@"    program_code = program_code + '-',");
            //sb.AppendLine(@"    accountable_division =accountable_division + '-'");
            //sb.AppendLine(@"WHERE id = "+ prog_id +";");
            //sb.AppendLine(@"");
            //sb.AppendLine(@"UPDATE dbo.mnda_program_encoded");
            //sb.AppendLine(@"SET  ");
            //sb.AppendLine(@"	main_program_id = main_program_id + '-',");
            //sb.AppendLine(@"    acountable_division_code =acountable_division_code + '-'");
            //sb.AppendLine(@"WHERE id = "+ proj_id +";");
            //sb.AppendLine(@"");
            //sb.AppendLine(@"UPDATE dbo.mnda_project_output");
            //sb.AppendLine(@"SET  ");
            //sb.AppendLine(@"	program_code = program_code + '-',");
            //sb.AppendLine(@"    name = name + '-'");
            //sb.AppendLine(@"WHERE id = "+ _output_id +";");
            //sb.AppendLine(@"");
            //sb.AppendLine(@"UPDATE dbo.mnda_activity");
            //sb.AppendLine(@"SET  ");
            //sb.AppendLine(@"	output_id = output_id + '-',");
            //sb.AppendLine(@"    accountable_member_id = accountable_member_id + '-'");
            //sb.AppendLine(@"WHERE id = "+ _activity_id +";");
            //sb.AppendLine(@"");
            //sb.AppendLine(@"UPDATE dbo.mnda_activity_data");
            //sb.AppendLine(@"SET  ");
            //sb.AppendLine(@"	activity_id = activity_id + '-',");
            //sb.AppendLine(@"    accountable_id = accountable_id + '-'");
            //sb.AppendLine(@"WHERE id = " + _activity_id + ";");

           

            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        public void UpdateProgramID(String _program_id, String _id)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(81);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_main_program_encoded  ");
            sb.AppendLine(@"SET   ");
            sb.AppendLine(@"  program_code ='" + _program_id + "' ");
            sb.AppendLine(@"WHERE id =" + _id + ";");

            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }
        public String FetchExpenseItems()
        {
            String _sqlString = "";

            var sb = new System.Text.StringBuilder(118);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"msub.uacs_code,");
            sb.AppendLine(@"msub.name");
            sb.AppendLine(@"FROM mnda_mooe_sub_expenditures msub");
            sb.AppendLine(@"WHERE msub.is_active = 1 and msub.is_lib = 0");


            _sqlString += sb.ToString();

            return _sqlString;
        }
        public void UpdateProjectID(String _project_id, String _id)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(81);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_main_program_encoded  ");
            sb.AppendLine(@"SET   ");
            sb.AppendLine(@"  program_code ='" + _project_id + "' ");
            sb.AppendLine(@"WHERE id =" + _id + ";");

            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }
        public void UpdateProgramTable(String _id, String _program)
        {
            String _sqlString = "";

            var sb = new System.Text.StringBuilder(68);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_programs  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  name = '"+ _program +"'");
            sb.AppendLine(@"WHERE   id = "+ _id +";");






            _sqlString = sb.ToString();


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }
        public void UpdateProjectTable(String _id, String _project)
        {
            String _sqlString = "";

            var sb = new System.Text.StringBuilder(77);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_program_encoded  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  project_name = '"+ _project +"'");
            sb.AppendLine(@"WHERE  id ="+ _id +" ;");



            _sqlString = sb.ToString();


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }
        public void UpdateOutputTable(String _id, String _output)
        {
            String _sqlString = "";

            var sb = new System.Text.StringBuilder(70);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_project_output  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  name ='" + _output +"'");
            sb.AppendLine(@"WHERE   id ="+ _id +";");




            _sqlString = sb.ToString();


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }
        public void UpdateActivityTable(String _id, String _activity)
        {
            String _sqlString = "";
            var sb = new System.Text.StringBuilder(68);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_activity  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  description = '"+ _activity +"'");
            sb.AppendLine(@"WHERE id = "+ _id +";");




            _sqlString = sb.ToString();


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }

        public void UpdateModeOfProcurementTable(String _id, String _mode_of_procurement)
        {
            String _sqlString = "";
            var sb = new System.Text.StringBuilder(68);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_activity  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  mode_of_procurement = '" + _mode_of_procurement + "'");
            sb.AppendLine(@"WHERE id = " + _id + ";");




            _sqlString = sb.ToString();


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }

        public void UpdateProgram(String _id, String _program) 
        {
            String _sqlString = "";
            var sb = new System.Text.StringBuilder(68);
           
       
                sb.AppendLine(@"INSERT INTO ");
                sb.AppendLine(@"  mnda_programs");
                sb.AppendLine(@"(");
                sb.AppendLine(@"  name");
                sb.AppendLine(@") ");
                sb.AppendLine(@"VALUES (");
                sb.AppendLine(@" '" + _program + "'");
                sb.AppendLine(@");");
          


            


            _sqlString = sb.ToString();


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }
        public void UpdateProjectOutput(String _output_id, String _program_id, String _project_output,String _assigned)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(98);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_project_output");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  program_code,");
            sb.AppendLine(@"  name,assigned_user");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '"+ _program_id +"',");
            sb.AppendLine(@"  '" + _project_output + "','" + _assigned + "'");
            sb.AppendLine(@");");



            _sqlString = sb.ToString();


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }
        public void UpdateDeleteProjectOutput(String _output_id, String _program_id, String _project_output,string Assigned_ID ="Null")
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(98);
            sb.AppendLine(@"DELETE FROM mnda_project_output WHERE id ="+ _output_id +"; INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_project_output");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  program_code,");
            sb.AppendLine(@"  name,assigned_user");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '" + _program_id + "',");
            sb.AppendLine(@"  '" + _project_output + "','"+ Assigned_ID +"'");
            sb.AppendLine(@");");



            _sqlString = sb.ToString();


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }
        public void UpdateAddOutputActivity(String output_id, String _activity,String _accountable_id="0",String _program_id= "")
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(286);
            //sb.AppendLine(@"INSERT INTO ");
            //sb.AppendLine(@"  dbo.mnda_project_output");
            //sb.AppendLine(@"(");
            //sb.AppendLine(@"  program_code,");
            //sb.AppendLine(@"  name,assigned_user");
            //sb.AppendLine(@") ");
            //sb.AppendLine(@"VALUES (");
            //sb.AppendLine(@"  '" + _program_id + "',");
            //sb.AppendLine(@"  '" + output_id + "','" + _accountable_id + "'");
            //sb.AppendLine(@");");

            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_activity");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  output_id,");
            sb.AppendLine(@"  description,");
            sb.AppendLine(@"  accountable_member_id");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '" + output_id + "',");
            sb.AppendLine(@"  '" + _activity + "',");
            sb.AppendLine(@"  '" + _accountable_id + "'");
            sb.AppendLine(@");");



            _sqlString = sb.ToString();


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }
        public void UpdateOutputActivity(String _activity_id,String output_id, String _activity,String _accountable_id ="-")
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(286);
            sb.AppendLine(@"DELETE FROM mnda_activity WHERE id = "+ _activity_id + ";  INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_activity");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  output_id,");
            sb.AppendLine(@"  description,");
            sb.AppendLine(@"  accountable_member_id");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '"+ output_id +"',");
            sb.AppendLine(@"  '"  + _activity  + "',");
            sb.AppendLine(@"  '" + _accountable_id + "'");
            sb.AppendLine(@");");



            _sqlString = sb.ToString();


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }
        public void UpdateProject(String _program_id, String _project,String _year,String _division)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(284);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.mnda_program_encoded");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  main_program_id,");
            sb.AppendLine(@"  key_result_code,");
            sb.AppendLine(@"  mindanao_2020_code,");
            sb.AppendLine(@"  scope_code,");
            sb.AppendLine(@"  fund_source_code,");
            sb.AppendLine(@"  acountable_division_code,");
            sb.AppendLine(@"  project_name,");
            sb.AppendLine(@"  project_year");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '" + _program_id + "',");
            sb.AppendLine(@" Null,");
            sb.AppendLine(@"  Null,");
            sb.AppendLine(@"  Null,");
            sb.AppendLine(@"  Null,");
            sb.AppendLine(@"  '"+ _division +"',");
            sb.AppendLine(@"  '"+ _project +"',");
            sb.AppendLine(@"  "+ _year +"");
            sb.AppendLine(@");");


            _sqlString = sb.ToString();


            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }
        public void UpdateAssignedEmployee(String _user_id, String _activity_id,String _output_id)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(81);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_activity");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  accountable_member_id = '"+ _user_id +"'");
            sb.AppendLine(@"WHERE id ="+ _activity_id +";");
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_project_output");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  assigned_user = '" + _user_id + "'");
            sb.AppendLine(@"WHERE id =" + _output_id + ";");

            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }
        public void UpdateYear(String _year , String _id) 
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(81);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_main_program_encoded  ");
            sb.AppendLine(@"SET   ");
            sb.AppendLine(@"  fiscal_year ="+ _year +" ");
            sb.AppendLine(@"WHERE id ="+ _id +";");

            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }

        public void UpdateModeOfProcurement(String _activity_id, String _mode_of_procurement)
        {
            String _sqlString = "";

            StringBuilder sb = new StringBuilder(81);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_activity  ");
            sb.AppendLine(@"SET   ");
            sb.AppendLine(@"  mode_of_procurement ='" + _mode_of_procurement + "' ");
            sb.AppendLine(@"WHERE id =" + _activity_id + ";");

            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }

        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "TransferMonths":
                    if (this.SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "Suspend":
                    if (this.SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "AddNewRow":
                    if (this.SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "RemoveOutput":
                    if (this.SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateRowYear":
                    if (this.SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateRowProgram":
                    if (this.SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateMainProgramID":
                    if (this.SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateRowProject":
                    if (this.SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateProjectOutput":
                    if (this.SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateOutputActivity":
                    if (this.SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "UpdateAssignedEmployee":
                    if (this.SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                case "RemoveData":
                    if (this.SQLOperation != null)
                    {
                        SQLOperation(this, new EventArgs());
                    }
                    break;
                default:
                    break;
            }
        }


        public String FetchOverAllYear() 
        {
            StringBuilder sb = new StringBuilder(86);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mnd.fiscal_year as Fiscal_Year");
            sb.AppendLine(@"FROM mnda_main_program_encoded mnd");
            sb.AppendLine(@"GROUP BY mnd.fiscal_year");

            return sb.ToString();
        }

        public String FetchProgramId(String _program)
        {
            StringBuilder sb = new StringBuilder(57);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id");
            sb.AppendLine(@"FROM  mnda_programs mp");
            sb.AppendLine(@"WHERE mp.name ='" + _program + "';");


            return sb.ToString();
        }
        public void UpdateSuspend(String _id, String _val)
        {


            String _sqlString = "";

            StringBuilder sb = new StringBuilder(79);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_activity_data  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  is_suspended =" + _val + ",status ='SUSPENDED-REVISION'");
            sb.AppendLine(@"WHERE id = " + _id + ";");


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn+=c_ops_DataReturn;
        }

        public void TransferMonths(String _id, String _val)
        {


            String _sqlString = "";

            StringBuilder sb = new StringBuilder(79);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_activity_data  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  month ='" + _val + "'");
            sb.AppendLine(@"WHERE id = " + _id + ";");


            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;
        }
        public String FetchActivityStatus(string _year, string _month,String _user,String _act_id)
        {
            StringBuilder sb = new StringBuilder(475);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mad.id as act_id,");
            sb.AppendLine(@"mad.activity_id,");
            sb.AppendLine(@"mad.accountable_id,");
            sb.AppendLine(@"mad.remarks,");
            sb.AppendLine(@"mooe.name,");
            sb.AppendLine(@"mad.type_service,");
            sb.AppendLine(@"mad.destination,");
            sb.AppendLine(@"mad.no_days,");
            sb.AppendLine(@"mad.no_staff,");
            sb.AppendLine(@"mad.quantity,");
            sb.AppendLine(@"mad.total");
            sb.AppendLine(@"FROM mnda_activity_data mad ");
            sb.AppendLine(@"LEFT JOIN mnda_activity mda on mda.id = mad.activity_id");
           // sb.AppendLine(@"LEFT JOIN mnda_project_output mpo on mpo.id = mda.output_id");
           // sb.AppendLine(@"left JOIN mnda_main_program_encoded mpe on mpe.program_code = mpo.program_code");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"WHERE mad.year = " + _year + " and mad.month = '" + _month + "' AND  mad.accountable_id ='" + _user + "' AND mad.activity_id = '"+ _act_id+"' and mad.is_suspended = 0");


            return sb.ToString();
        }
        public String FetchActivityStatusByMonth(string _year, string _month, String _user)
        {
            StringBuilder sb = new StringBuilder(475);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mad.id as act_id,");
            sb.AppendLine(@"mad.activity_id,");
            sb.AppendLine(@"mad.accountable_id,");
            sb.AppendLine(@"mad.remarks,");
            sb.AppendLine(@"mooe.name,");
            sb.AppendLine(@"mad.type_service,");
            sb.AppendLine(@"mad.destination,");
            sb.AppendLine(@"mad.no_days,");
            sb.AppendLine(@"mad.no_staff,");
            sb.AppendLine(@"mad.quantity,");
            sb.AppendLine(@"mad.total");
            sb.AppendLine(@"FROM mnda_activity_data mad ");
            sb.AppendLine(@"LEFT JOIN mnda_activity mda on mda.id = mad.activity_id");
            // sb.AppendLine(@"LEFT JOIN mnda_project_output mpo on mpo.id = mda.output_id");
            // sb.AppendLine(@"left JOIN mnda_main_program_encoded mpe on mpe.program_code = mpo.program_code");
            sb.AppendLine(@"LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"WHERE mad.year = " + _year + " and mad.month = '" + _month + "' AND  mad.accountable_id ='" + _user + "'  and mad.is_suspended = 0");


            return sb.ToString();
        }
        public String FetchProjectId(String _project)
        {
            StringBuilder sb = new StringBuilder(57);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id");
            sb.AppendLine(@"FROM  mnda_programs mp");
            sb.AppendLine(@"WHERE mp.name ='" + _project + "';");


            return sb.ToString();
        }

        public String FetchYearPrograms() 
                {
                    StringBuilder sb = new StringBuilder(167);
                    sb.AppendLine(@"SELECT");
                    sb.AppendLine(@"mnd.fiscal_year,");
                    sb.AppendLine(@"mnd.id, ");
                    sb.AppendLine(@"mp.name");
                    sb.AppendLine(@"FROM mnda_main_program_encoded mnd");
                    sb.AppendLine(@"INNER JOIN mnda_programs mp  ON mp.id = mnd.program_code");
                    sb.AppendLine(@"ORDER BY mnd.fiscal_year ASC");

                    return sb.ToString();
                }
      public String FetchYearProjects() 
                    {
                        StringBuilder sb = new StringBuilder(308);
                        sb.AppendLine(@"SELECT");
                        sb.AppendLine(@"mnd.id,");
                        sb.AppendLine(@"mpe.project_year,");
                        sb.AppendLine(@"mpe.project_name,");
                        sb.AppendLine(@"ISNULL(div.Division_Desc,'N/A') as Division");
                        sb.AppendLine(@"FROM mnda_main_program_encoded mnd");
                        sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.main_program_id = mnd.id");
                        sb.AppendLine(@"LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
                        sb.AppendLine(@"ORDER BY mpe.project_year ASC");

                        return sb.ToString();
                    } 
        public String FetchYearActivity() 
                    {
                        StringBuilder sb = new StringBuilder(627);
                        sb.AppendLine(@"SELECT");
                        sb.AppendLine(@"ma.id as activity_id,");
                        sb.AppendLine(@"mnd.id as program_id,");
                        sb.AppendLine(@"mpe.id as project_id,");
                        sb.AppendLine(@"mpe.project_year,");
                        sb.AppendLine(@"mpe.project_name,");
                        sb.AppendLine(@"ISNULL(div.Division_Desc,'N/A') as Division,");
                        sb.AppendLine(@"ma.description as activity,");
                        sb.AppendLine(@"mua.User_Fullname as assigned");
                        sb.AppendLine(@"FROM mnda_main_program_encoded mnd");
                        sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.main_program_id = mnd.id");
                        sb.AppendLine(@"INNER JOIN Division div on div.Division_Id = mpe.acountable_division_code");
                        sb.AppendLine(@"INNER JOiN mnda_project_output  mpo on mpo.program_code = mpe.id");
                        sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.output_id = mpo.id");
                        sb.AppendLine(@"INNER JOIN mnda_user_accounts mua on mua.User_Id = ma.accountable_member_id");
                        sb.AppendLine(@"ORDER BY mpe.project_year ASC");


                        return sb.ToString();
                    }

        public String FetchData(String Division_Id) 
        {
            StringBuilder sb = new StringBuilder(1608);
            sb.AppendLine(@"   SELECT ");
            sb.AppendLine(@"                        ISNULL(mnd.id,'') as program_id,ISNULL(mpe.id,'') as project_id,ISNULL(mpo.id,'') as output_id,");
            sb.AppendLine(@"                        ISNULL(mnd.fiscal_year,'') as fiscal_year,");
            sb.AppendLine(@"                        ISNULL(mp.name,'') as program_name,");
            sb.AppendLine(@"                        ISNULL(mpo.name,'') as output,");
            sb.AppendLine(@"                        ISNULL(mpe.project_name,'') as project_name,");
            sb.AppendLine(@"                        ISNULL(ma.description,'') as activity,");
            sb.AppendLine(@"                        ISNULL(ma.id,'') as activity_id,");
            sb.AppendLine(@"                        ISNULL(mua.User_Fullname,'') as User_Fullname,");
            sb.AppendLine(@"                        0.00 as Jan,");
            sb.AppendLine(@"                        0.00 as Feb,");
            sb.AppendLine(@"                        0.00 as Mar,");
            sb.AppendLine(@"                        0.00 as Apr,");
            sb.AppendLine(@"                        0.00 as May,");
            sb.AppendLine(@"                        0.00 as Jun,");
            sb.AppendLine(@"                        0.00 as Jul,");
            sb.AppendLine(@"                        0.00 as Aug,");
            sb.AppendLine(@"                        0.00 as Sep,");
            sb.AppendLine(@"                        0.00 as Oct,");
            sb.AppendLine(@"                        0.00 as Nov,");
            sb.AppendLine(@"                        0.00 as Dec                                           ");
            sb.AppendLine(@"                        FROM mnda_main_program_encoded mnd");
            sb.AppendLine(@"                        LEFT JOIN mnda_programs mp on mp.id = mnd.program_code");
            sb.AppendLine(@"                        LEFT JOIN mnda_program_encoded mpe on mpe.main_program_id = mnd.id");
            sb.AppendLine(@"                        LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@"                        LEFT JOiN mnda_project_output  mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@"                        LEFT JOIN mnda_activity ma on ma.output_id = mpo.id");
            sb.AppendLine(@"                        LEFT JOIN mnda_activity_data mad on mad.activity_id = ma.id");
            sb.AppendLine(@"                        LEFT JOIN mnda_user_accounts mua on mua.User_Id = ma.accountable_member_id");
            sb.AppendLine(@"                        WHERE mnd.accountable_division = "+ Division_Id +" AND mad.is_suspended = 0");
            sb.AppendLine(@"                        GROUP BY mnd.id,mpe.id,mpo.id, mnd.fiscal_year,mp.name,mpo.name,mpe.project_name,ma.description,ma.id,mua.User_Fullname"); 
            sb.AppendLine(@"                        ORDER BY  mnd.fiscal_year ASC");


            return sb.ToString();
        }
        public String FetchDataPersonal(String WorkingYear,String Division_Id,String Accountable_Id,String AssignedName , String FundSource,String FundName)
        {
            StringBuilder sb = new StringBuilder(1608);
            sb.AppendLine(@"   SELECT ");
            sb.AppendLine(@"                        ISNULL(mnd.id,'') as program_id,ISNULL(mpe.id,'') as project_id, ISNULL(mp.id,'') as program_index,ISNULL(mpo.id,'') as output_id,");
            sb.AppendLine(@"                        ISNULL(mnd.fiscal_year,'') as fiscal_year,");
            sb.AppendLine(@"                        ISNULL(mp.name,'') as program_name,");
            sb.AppendLine(@"                        ISNULL(mpo.name,'') as output,");
            sb.AppendLine(@"                        ISNULL(mpe.project_name,'') as project_name,");
            sb.AppendLine(@"                        ISNULL(ma.description,'') as activity, ISNULL(ma.mode_of_procurement,'') as mode_of_procurement, ");
            //sb.AppendLine(@"                        ISNULL(ma.description,'') as activity, '' as mode_of_procurement, ");
            sb.AppendLine(@"                        ISNULL(ma.id,'') as activity_id,");
            sb.AppendLine(@"                        ISNULL(mua.User_Fullname,'') as User_Fullname,");
            sb.AppendLine(@"                        0.00 as Jan,");
            sb.AppendLine(@"                        0.00 as Feb,");
            sb.AppendLine(@"                        0.00 as Mar,");
            sb.AppendLine(@"                        0.00 as Apr,");
            sb.AppendLine(@"                        0.00 as May,");
            sb.AppendLine(@"                        0.00 as Jun,");
            sb.AppendLine(@"                        0.00 as Jul,");
            sb.AppendLine(@"                        0.00 as Aug,");
            sb.AppendLine(@"                        0.00 as Sep,");
            sb.AppendLine(@"                        0.00 as Oct,");
            sb.AppendLine(@"                        0.00 as Nov,");
            sb.AppendLine(@"                        0.00 as Dec                                           ");
            sb.AppendLine(@"                        FROM mnda_main_program_encoded mnd");
            sb.AppendLine(@"                        LEFT JOIN mnda_programs mp on mp.id = mnd.program_code");
            sb.AppendLine(@"                        LEFT JOIN mnda_program_encoded mpe on mpe.main_program_id = mnd.id");
            sb.AppendLine(@"                        LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code");
            sb.AppendLine(@"                        LEFT JOiN mnda_project_output  mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@"                        LEFT JOIN mnda_activity ma on ma.output_id = mpo.id");
            sb.AppendLine(@"                        LEFT JOIN mnda_activity_data mad on mad.activity_id = ma.id");
            sb.AppendLine(@"                        LEFT JOIN mnda_user_accounts mua on mua.User_Id = ma.accountable_member_id");
            sb.AppendLine(@"                        WHERE mnd.accountable_division =" + Division_Id + " AND mnd.fund_source ='" + FundName + "' AND mnd.fiscal_year ='"+ WorkingYear +"' ");       
            sb.AppendLine(@"                        GROUP BY mnd.id,mpe.id,mp.id,mpo.id, mnd.fiscal_year,mp.name,mpo.name,mpe.project_name,ma.description,ma.mode_of_procurement,ma.id,mua.User_Fullname");
            sb.AppendLine(@"                        ORDER BY  ma.id,mnd.fiscal_year ASC");

          //  sb.AppendLine(@"                        and mnd.accountable_person = '" + Accountable_Id + "' ");
            //sb.AppendLine(@"                        WHERE mnd.accountable_division =" + Division_Id + " AND mpo.assigned_user =" + Accountable_Id + " ");
           
            //sb.AppendLine(@"                        ORDER BY  mnd.fiscal_year ASC");


            return sb.ToString();
        }
        public String FetchTotals(String F_source) 
        {
            StringBuilder sb = new StringBuilder(302);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mdad.activity_id ,");
            sb.AppendLine(@"mdad.month ,");
            sb.AppendLine(@"mdad.total");
            sb.AppendLine(@"FROM mnda_activity_data mdad");
            sb.AppendLine(@"INNER JOIN mnda_activity mda on mda.id = mdad.activity_id");
            sb.AppendLine(@"WHERE mdad.is_suspended = 0 AND mdad.fund_source_id='"+ F_source +"'");
            //sb.AppendLine(@"UNION");
            //sb.AppendLine(@"SELECT");
            //sb.AppendLine(@"madp.activity_id,");
            //sb.AppendLine(@"madp.month,");
            //sb.AppendLine(@"madp.total");
            //sb.AppendLine(@"FROM mnda_activity_data_procurement madp");
            //sb.AppendLine(@"INNER JOIN mnda_activity mda on mda.id = madp.activity_id");


            return sb.ToString();
        }
        public String FetchPPMPStatus(String F_source)
        {
            var sb = new System.Text.StringBuilder(86);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mppmp.status");
            sb.AppendLine(@"FROM mnda_ppmp_list mppmp");
            sb.AppendLine(@"WHERE mppmp.fund_name = '" + F_source + "'");

            return sb.ToString();
        }

        public String FetchDivision() 
        {
            var sb = new System.Text.StringBuilder(104);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"  div.Division_Id,");
            sb.AppendLine(@"  div.DivisionAccro as code");
            sb.AppendLine(@"FROM Division div");
            sb.AppendLine(@"WHERE div.DBM_Sub_Pap_Id !=0");

            return sb.ToString();
        }
    }

    public class PPMPStatus 
    {
        public String Status { get; set; }
    }

    public class MainDashboardActivityData 
    {
              public String act_id { get; set; }
             public String activity_id { get; set; }
             public String accountable_id { get; set; }
             public String remarks { get; set; }
             public String name { get; set; }
             public String type_service { get; set; }
             public String destination { get; set; }
             public String no_days { get; set; }
             public String no_staff { get; set; }
             public String quantity { get; set; }
             public String total { get; set; }
    }
    public class FetchProjectId
    {
        public String id { get; set; }
    }
    public class FetchProgramId 
    {
        public String id { get; set; }
    }
    public class MainTotals
    {
        public String activity_id {get;set;}
        public String month {get;set;}
        public String total {get;set;}
    }

    public class MainData 
    {
                 public String div_id { get; set; }
                 public String project_id { get; set; }
                 public String output_id { get; set; }
                 public Int32 		program_id {get;set;}
                 public String      fiscal_year {get;set;}
                 public String      program_name {get;set;}
                 public String      program_index { get; set; }
                 public String      output {get;set;}
                 public String      project_name {get;set;}
                 public String      activity {get;set;}
                 public String mode_of_procurement { get; set; }
                 public String      activity_id { get; set; }
                 public String      User_Fullname {get;set;}
                 public String     Jan {get;set;}
                 public String Feb { get; set; }
                 public String Mar { get; set; }
                 public String Apr { get; set; }
                 public String May { get; set; }
                 public String Jun { get; set; }
                 public String Jul { get; set; }
                 public String Aug { get; set; }
                 public String Sep { get; set; }
                 public String Oct { get; set; }
                 public String Nov { get; set; }
                 public String Dec { get; set; }
                 public String Total { get; set; }
    }
    public class MainDataFinance
    {
        public String div_id { get; set; }
        public String project_id { get; set; }
        public String output_id { get; set; }
        public Int32 program_id { get; set; }
        public String Division { get; set; }
        public String fiscal_year { get; set; }
        public String program_name { get; set; }
        public String output { get; set; }
        public String project_name { get; set; }
        public String activity { get; set; }
        public String activity_id { get; set; }
        public String User_Fullname { get; set; }
        public String Jan { get; set; }
        public String Feb { get; set; }
        public String Mar { get; set; }
        public String Apr { get; set; }
        public String May { get; set; }
        public String Jun { get; set; }
        public String Jul { get; set; }
        public String Aug { get; set; }
        public String Sep { get; set; }
        public String Oct { get; set; }
        public String Nov { get; set; }
        public String Dec { get; set; }
    }

    public class Main_OverAllYear 
    {
        public String Fiscal_Year { get; set; }
    }
    public class Main_YearPrograms 
    {
        public String fiscal_year { get; set; }
        public String id { get; set; }
        public String name { get; set; }
    }
    public class Main_YearProjects 
    {
        public String id {get;set;}
        public String project_year {get;set;}
        public String project_name {get;set;}
        public String Division { get; set; }
    }
    public class Main_YearActivities 
    {
        public String activity_id {get;set;}
        public String program_id {get;set;}
        public String project_id {get;set;}
        public String project_year {get;set;}
        public String project_name {get;set;}
        public String Division {get;set;}
        public String activity {get;set;}
        public String assigned {get;set;}
    }
}
