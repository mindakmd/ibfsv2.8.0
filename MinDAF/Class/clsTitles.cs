﻿using MinDAF.MinDAFS;
using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsTitles
    {
        public String Process { get; set; }

        MinDAFSVCClient svc_mindaf = new MinDAFSVCClient();

        public String FetchProgramLists(String _divid,String Type)
        {
            StringBuilder varname1 = new StringBuilder();

            switch (Type.ToString())
            {
                case "Programs":
                        varname1.Append("SELECT \n");
                        varname1.Append("                                    ISNULL(mp.name,'') as name \n");
                        varname1.Append("                                    FROM mnda_main_program_encoded mnd \n");
                        varname1.Append("                                    LEFT JOIN mnda_programs mp on mp.id = mnd.program_code \n");
                        varname1.Append("                                    LEFT JOIN mnda_program_encoded mpe on mpe.main_program_id = mnd.id \n");
                        varname1.Append("                                    LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code \n");
                        varname1.Append("                                    LEFT JOiN mnda_project_output  mpo on mpo.program_code = mpe.id \n");
                        varname1.Append("                                    LEFT JOIN mnda_activity ma on ma.output_id = mpo.id \n");
                        varname1.Append("                                    LEFT JOIN mnda_activity_data mad on mad.activity_id = ma.id \n");
                        varname1.Append("                                    LEFT JOIN mnda_user_accounts mua on mua.User_Id = ma.accountable_member_id \n");
                        varname1.Append("                                    WHERE mnd.accountable_division ="+ _divid +" and mpo.name <>'' \n");
                        varname1.Append("                                    GROUP BY mp.name");
                    break;
                case "Projects":
                        varname1.Append("SELECT \n");
                        varname1.Append("                                    ISNULL(mpe.project_name,'')  as name \n");
                        varname1.Append("                                    FROM mnda_main_program_encoded mnd \n");
                        varname1.Append("                                    LEFT JOIN mnda_programs mp on mp.id = mnd.program_code \n");
                        varname1.Append("                                    LEFT JOIN mnda_program_encoded mpe on mpe.main_program_id = mnd.id \n");
                        varname1.Append("                                    LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code \n");
                        varname1.Append("                                    LEFT JOiN mnda_project_output  mpo on mpo.program_code = mpe.id \n");
                        varname1.Append("                                    LEFT JOIN mnda_activity ma on ma.output_id = mpo.id \n");
                        varname1.Append("                                    LEFT JOIN mnda_activity_data mad on mad.activity_id = ma.id \n");
                        varname1.Append("                                    LEFT JOIN mnda_user_accounts mua on mua.User_Id = ma.accountable_member_id \n");
                        varname1.Append("                                    WHERE mnd.accountable_division ="+ _divid +" and mpo.name <>'' \n");
                        varname1.Append("                                    GROUP BY mpe.project_name");
                    break;
                case "Outputs":
                        varname1.Append("SELECT \n");
                        varname1.Append("                                    ISNULL(mpo.name,'') as name \n");
                        varname1.Append("                                    FROM mnda_main_program_encoded mnd \n");
                        varname1.Append("                                    LEFT JOIN mnda_programs mp on mp.id = mnd.program_code \n");
                        varname1.Append("                                    LEFT JOIN mnda_program_encoded mpe on mpe.main_program_id = mnd.id \n");
                        varname1.Append("                                    LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code \n");
                        varname1.Append("                                    LEFT JOiN mnda_project_output  mpo on mpo.program_code = mpe.id \n");
                        varname1.Append("                                    LEFT JOIN mnda_activity ma on ma.output_id = mpo.id \n");
                        varname1.Append("                                    LEFT JOIN mnda_activity_data mad on mad.activity_id = ma.id \n");
                        varname1.Append("                                    LEFT JOIN mnda_user_accounts mua on mua.User_Id = ma.accountable_member_id \n");
                        varname1.Append("                                    WHERE mnd.accountable_division ="+ _divid +" and mpo.name <>'' \n");
                        varname1.Append("                                    GROUP BY mpo.name");
                    break;
                case "Activity":
                        varname1.Append("SELECT \n");
                        varname1.Append("                                    ISNULL(ma.description,'') as name \n");
                        varname1.Append("                                    FROM mnda_main_program_encoded mnd \n");
                        varname1.Append("                                    LEFT JOIN mnda_programs mp on mp.id = mnd.program_code \n");
                        varname1.Append("                                    LEFT JOIN mnda_program_encoded mpe on mpe.main_program_id = mnd.id \n");
                        varname1.Append("                                    LEFT JOIN Division div on div.Division_Id = mpe.acountable_division_code \n");
                        varname1.Append("                                    LEFT JOiN mnda_project_output  mpo on mpo.program_code = mpe.id \n");
                        varname1.Append("                                    LEFT JOIN mnda_activity ma on ma.output_id = mpo.id \n");
                        varname1.Append("                                    LEFT JOIN mnda_activity_data mad on mad.activity_id = ma.id \n");
                        varname1.Append("                                    LEFT JOIN mnda_user_accounts mua on mua.User_Id = ma.accountable_member_id \n");
                        varname1.Append("                                    WHERE mnd.accountable_division ="+ _divid +" and mpo.name <>'' \n");
                        varname1.Append("                                    GROUP BY ma.description");
                    break;
                default:
                    break;
            }

           

            return varname1.ToString();
        }
    }

    public class TitleNames 
    {
        public String List { get; set; }
    }
}
