﻿using System;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace MinDAF.Class
{
    public class clsNotifications
    {
        public event EventHandler SQLOperation;
        public String Process { get; set; }
        public String ReturnCode { get; set; }
        private clsServiceOperation c_ops = new clsServiceOperation();


        public String FetchNotifications()
        {

            var sb = new System.Text.StringBuilder(267);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"(CASE WHEN mfn.is_read = 0 THEN 'New' ELSE '' END) as Status,");
            sb.AppendLine(@"div.Division_Desc as FromDiv,");
            sb.AppendLine(@"CONCAT(mfn.message, ' ' , div.Division_Desc) as subject,");
            sb.AppendLine(@"mfn.date_time ");
            sb.AppendLine(@"FROM mnda_finance_notification mfn");
            sb.AppendLine(@"INNER JOIN Division div on div.Division_Id = mfn.div_id");
            sb.AppendLine(@"ORDER BY mfn.id DESC");

            return sb.ToString();
        }
        public void UpdateNotifications()
        {
            string _sqlString = "";
            var sb = new System.Text.StringBuilder(82);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.mnda_finance_notification  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  is_read =1");
            sb.AppendLine(@"WHERE is_read = 0;");



            _sqlString += sb.ToString();




            c_ops.InstantiateService();
            c_ops.ExecuteSQL(_sqlString);
            c_ops.DataReturn += c_ops_DataReturn;


        }
        private bool isprocessed = false;
        void c_ops_DataReturn(object sender, EventArgs e)
        {
            switch (this.Process)
            {
                case "UpdateNotifications":
                    if (this.SQLOperation!=null)
                    {
                        if (isprocessed)
                        {
                            isprocessed = true;
                            return;
                        }
                        this.SQLOperation(this, new EventArgs());
                        isprocessed = true;
                    }
                    break;
            }
        }
        public String FetchNew()
        {

            var sb = new System.Text.StringBuilder(92);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"COUNT(mfn.id) as counter");
            sb.AppendLine(@"FROM mnda_finance_notification mfn ");
            sb.AppendLine(@"WHERE mfn.is_read = 0");


            return sb.ToString();
        }
    }

    public class ListNotifications 
    {
        public String Status { get; set; }
        public String Division { get; set; }
        public String Subject { get; set; }
        public String DateSubmitted { get; set; }
    }
    public class ListCountNew
    {
        public String Count { get; set; }
 
    }
    public class ListNotificationData
    {
        public String Status { get; set; }
        public String Subject { get; set; }
        public String DateSubmitted { get; set; }
    }
}
