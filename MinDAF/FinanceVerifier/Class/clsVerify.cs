﻿using ReportTool.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinanceVerifier.Class
{
    class clsVerify
    {
        clsData c_data = new clsData();
        public DataSet FetchExpenseItems() 
        {
            var sb = new System.Text.StringBuilder(155);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  id,");
            sb.AppendLine(@"  mooe_id,");
            sb.AppendLine(@"  uacs_code,");
            sb.AppendLine(@"  name");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_mooe_sub_expenditures ");
            sb.AppendLine(@"  WHERE is_lib	 = 0 and expense_type = 'MOOE'");
            sb.AppendLine(@"ORDER BY id ASC;");

      

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }
        public DataSet FetchActivityItems() 
        {
            var sb = new System.Text.StringBuilder(374);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mad.mooe_sub_expenditure_id as mooe_id,");
            sb.AppendLine(@"div.DivisionAccro as accro,");
            sb.AppendLine(@"mad.total");
            sb.AppendLine(@"FROM mnda_activity_data mad");
            sb.AppendLine(@"INNER JOIN mnda_activity ma  on ma.id= mad.activity_id");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.id = ma.output_id");
            sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
            sb.AppendLine(@"INNER JOIN Division div on div.Division_Id = mpe.acountable_division_code");

      

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }

        public DataSet FetchPAP()
        {
            var sb = new System.Text.StringBuilder(109);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  DBM_Sub_Pap_id,");
            sb.AppendLine(@"  DBM_Pap_Id,");
            sb.AppendLine(@"  DBM_Sub_Pap_Code,");
            sb.AppendLine(@"  DBM_Sub_Pap_Desc,");
            sb.AppendLine(@"  Acrro");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.DBM_Sub_Pap;");

            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }

        public DataSet FetchDivisionList()
        {
            var sb = new System.Text.StringBuilder(157);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"dso.DBM_Sub_Id,");
            sb.AppendLine(@"dso.PAP,");
            sb.AppendLine(@"div.Division_Desc as div_name,");
            sb.AppendLine(@"div.DivisionAccro as accro");
            sb.AppendLine(@"FROM DBM_Sub_Office dso");
            sb.AppendLine(@"INNER JOIN Division div on div.Division_Code = dso.PAP");


            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }
        public DataSet FetchExpenseMOOE()
        {
            var sb = new System.Text.StringBuilder(96);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  code,");
            sb.AppendLine(@"  name,");
            sb.AppendLine(@"  type_service");
            sb.AppendLine(@"FROM ");
            sb.AppendLine(@"  dbo.mnda_mooe_expenditures");
            sb.AppendLine(@"ORDER BY id ASC;");



            DataSet dsData = c_data.ExecuteSQLQuery(sb.ToString());

            return dsData;
        }

    }
}
