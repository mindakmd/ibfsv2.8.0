﻿#pragma checksum "C:\Users\Minda\Desktop\MinDAF\MinDAF\MinDAF\MinDAF\Forms\frmExpenditureLibrary.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "52B9EA5C21A16234EEAB95827AB74BDC"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace MinDAF.Forms {
    
    
    public partial class frmExpenditureLibrary : System.Windows.Controls.ChildWindow {
        
        internal System.Windows.Controls.ChildWindow frmexpenditure;
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.Button CancelButton;
        
        internal System.Windows.Controls.Button btnAdd;
        
        internal System.Windows.Controls.ComboBox cmbExpenditure;
        
        internal Infragistics.Controls.Grids.XamGrid grdData;
        
        internal System.Windows.Controls.TextBox txtItemCode;
        
        internal System.Windows.Controls.TextBox txtItemName;
        
        internal System.Windows.Controls.ComboBox cmbDay;
        
        internal System.Windows.Controls.ComboBox cmbYear;
        
        internal Infragistics.Controls.Editors.XamNumericInput txtRate;
        
        internal System.Windows.Controls.TextBox txtLibraryType;
        
        internal System.Windows.Controls.Button btnEnabled;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/MinDAF;component/Forms/frmExpenditureLibrary.xaml", System.UriKind.Relative));
            this.frmexpenditure = ((System.Windows.Controls.ChildWindow)(this.FindName("frmexpenditure")));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.CancelButton = ((System.Windows.Controls.Button)(this.FindName("CancelButton")));
            this.btnAdd = ((System.Windows.Controls.Button)(this.FindName("btnAdd")));
            this.cmbExpenditure = ((System.Windows.Controls.ComboBox)(this.FindName("cmbExpenditure")));
            this.grdData = ((Infragistics.Controls.Grids.XamGrid)(this.FindName("grdData")));
            this.txtItemCode = ((System.Windows.Controls.TextBox)(this.FindName("txtItemCode")));
            this.txtItemName = ((System.Windows.Controls.TextBox)(this.FindName("txtItemName")));
            this.cmbDay = ((System.Windows.Controls.ComboBox)(this.FindName("cmbDay")));
            this.cmbYear = ((System.Windows.Controls.ComboBox)(this.FindName("cmbYear")));
            this.txtRate = ((Infragistics.Controls.Editors.XamNumericInput)(this.FindName("txtRate")));
            this.txtLibraryType = ((System.Windows.Controls.TextBox)(this.FindName("txtLibraryType")));
            this.btnEnabled = ((System.Windows.Controls.Button)(this.FindName("btnEnabled")));
        }
    }
}

