﻿#pragma checksum "D:\Desktop\MinDAF\MinDAF\MinDAF\Usercontrol\Report\usr_monthly_budget_allocation.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "C42FE651F79C7830DD12EB47E753A6F6"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace MinDAF.Usercontrol.Report {
    
    
    public partial class usr_monthly_budget_allocation : System.Windows.Controls.UserControl {
        
        internal System.Windows.Controls.UserControl ctrl_mao;
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal System.Windows.Controls.Grid grdPrintArea;
        
        internal XamGrid grdData;
        
        internal XamGrid grdTotals;
        
        internal System.Windows.Controls.Label lblFundSouce;
        
        internal System.Windows.Controls.Label lblOffice;
        
        internal System.Windows.Controls.Button btnPrint;
        
        internal System.Windows.Controls.ComboBox cmbTypeService;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/MinDAF;component/Usercontrol/Report/usr_monthly_budget_allocation.xaml", System.UriKind.Relative));
            this.ctrl_mao = ((System.Windows.Controls.UserControl)(this.FindName("ctrl_mao")));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.grdPrintArea = ((System.Windows.Controls.Grid)(this.FindName("grdPrintArea")));
            this.grdData = ((XamGrid)(this.FindName("grdData")));
            this.grdTotals = ((XamGrid)(this.FindName("grdTotals")));
            this.lblFundSouce = ((System.Windows.Controls.Label)(this.FindName("lblFundSouce")));
            this.lblOffice = ((System.Windows.Controls.Label)(this.FindName("lblOffice")));
            this.btnPrint = ((System.Windows.Controls.Button)(this.FindName("btnPrint")));
            this.cmbTypeService = ((System.Windows.Controls.ComboBox)(this.FindName("cmbTypeService")));
        }
    }
}

