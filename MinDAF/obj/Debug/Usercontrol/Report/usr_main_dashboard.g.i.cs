﻿#pragma checksum "C:\Users\Minda\Desktop\sys from pc\MinDAF\MinDAF\MinDAF\MinDAF\Usercontrol\Report\usr_main_dashboard.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "9F123EC3593AEFAE1A41BF00C57AA7C5"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.Windows.Shapes;
using System.Windows.Threading;


namespace MinDAF.Usercontrol.Report {
    
    
    public partial class usr_main_dashboard : System.Windows.Controls.UserControl {
        
        internal System.Windows.Controls.UserControl ctrl_main_dashboard;
        
        internal System.Windows.Controls.Grid LayoutRoot;
        
        internal Infragistics.Controls.Grids.XamGrid grdData;
        
        internal Infragistics.Controls.Grids.XamGrid grdTotals;
        
        internal Infragistics.Controls.Grids.XamGrid grdStatus;
        
        internal Infragistics.Controls.Grids.XamGrid grdLive;
        
        internal System.Windows.Controls.Label lblStatus;
        
        internal System.Windows.Controls.Button btnSuspend;
        
        internal System.Windows.Controls.Button btnTransfer;
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Windows.Application.LoadComponent(this, new System.Uri("/MinDAF;component/Usercontrol/Report/usr_main_dashboard.xaml", System.UriKind.Relative));
            this.ctrl_main_dashboard = ((System.Windows.Controls.UserControl)(this.FindName("ctrl_main_dashboard")));
            this.LayoutRoot = ((System.Windows.Controls.Grid)(this.FindName("LayoutRoot")));
            this.grdData = ((Infragistics.Controls.Grids.XamGrid)(this.FindName("grdData")));
            this.grdTotals = ((Infragistics.Controls.Grids.XamGrid)(this.FindName("grdTotals")));
            this.grdStatus = ((Infragistics.Controls.Grids.XamGrid)(this.FindName("grdStatus")));
            this.grdLive = ((Infragistics.Controls.Grids.XamGrid)(this.FindName("grdLive")));
            this.lblStatus = ((System.Windows.Controls.Label)(this.FindName("lblStatus")));
            this.btnSuspend = ((System.Windows.Controls.Button)(this.FindName("btnSuspend")));
            this.btnTransfer = ((System.Windows.Controls.Button)(this.FindName("btnTransfer")));
        }
    }
}

