﻿using Procurement_Module.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FinanceReport
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private clsPPMP c_ppmp = new clsPPMP();
        public String DivID { get; set; }
        public String PAPCode { get; set; }
        public String PAPId { get; set; }
        public String DivisionName { get; set; }
        public String RAFID { get; set; }
        public String RAFNO { get; set; }
        public String RAFDESCRIPTION { get; set; }
        public String IsCancelled { get; set; }
        private DataSet dsReport = new DataSet();

        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += MainWindow_Loaded;
        }
        frmChooseDivision _divChoose;
        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
           _divChoose = new frmChooseDivision();
            _divChoose.SelectDone += _divChoose_SelectDone;

            _divChoose.ShowDialog();
        }

        void _divChoose_SelectDone(object sender, EventArgs e)
        {
            this.DivID = _divChoose.DivisionId;
            this.PAPCode = _divChoose.DivisionPap;
            this.DivisionName = _divChoose.DivisionName;
            this.RAFID = _divChoose.RAFID;
            this.RAFNO = _divChoose.RAFNO;
            this.RAFDESCRIPTION = _divChoose.RAFDESCRIPTION;
            this.IsCancelled = _divChoose.RAFSTATUS;
        }
        private DataTable dtReport = new DataTable();

        private void LoadReportAnnexA()
        {

            DataTable dtAnnexA = c_ppmp.FetchDataAnnexA(DivID, "2017",this.RAFDESCRIPTION).Tables[0].Copy();
            DataTable dtAnnexATitle = c_ppmp.FetchDataAnnexATitle(DivID, "2017", this.RAFDESCRIPTION).Tables[0].Copy();
            dtReport = new DataTable();

            dtReport.Columns.Add("mfo_pap");
            dtReport.Columns.Add("from_obect_expenditure");
            dtReport.Columns.Add("from_total");
            dtReport.Columns.Add("to_obect_expenditure");
            dtReport.Columns.Add("to_total");
            dtReport.Columns.Add("status");

            double Total = 0.00;
            double Contigency = 0.00;

            for (int i = 0; i < dtAnnexATitle.Rows.Count; i++)
            {
               
                DataRow dr = dtReport.NewRow();
                dtAnnexA.DefaultView.RowFilter = "";
                dtAnnexA.DefaultView.RowFilter = "from_obect_expenditure ='" + dtAnnexATitle.Rows[i][1].ToString() + "'";
                DataTable dt = dtAnnexA.DefaultView.ToTable().Copy();
                if (dt.Rows.Count > 1)
                {

                    dr["mfo_pap"] = "Operations " + Environment.NewLine + Environment.NewLine + "MFO 1: Integrated Policies and Programs for Mindanao " + Environment.NewLine + Environment.NewLine + Environment.NewLine + "      " + PAPCode;
                    dr["from_obect_expenditure"] = dtAnnexATitle.Rows[i][1].ToString();
                    double _total = 0.00;
                    for (int c = 0; c < dt.Rows.Count; c++)
                    {
                        _total += Convert.ToDouble(dt.Rows[c][4].ToString());
                    }

                    dr["from_total"] = _total;
                    dr["to_obect_expenditure"] = dt.Rows[0][3].ToString();
                    dr["to_total"] = dt.Rows[0][4].ToString();

                    if (this.IsCancelled=="1")
	                {
                         dr["status"] = "--- C A N C E L L E D ---";
                    }
                    else
                    {
                        dr["status"] = "";
                    }

                    dtReport.Rows.Add(dr);
                    for (int x = 1; x < dt.Rows.Count; x++)
                    {
                        dr = dtReport.NewRow();
                        dr["mfo_pap"] = "Operations " + Environment.NewLine + Environment.NewLine + "MFO 1: Integrated Policies and Programs for Mindanao " + Environment.NewLine + Environment.NewLine + Environment.NewLine + "      " + PAPCode;
                        dr["from_obect_expenditure"] = "";
                        dr["from_total"] = "0";
                        dr["to_obect_expenditure"] = dt.Rows[x][3].ToString();
                        dr["to_total"] = dt.Rows[x][4].ToString();

                        if (this.IsCancelled == "1")
                        {
                            dr["status"] = "--- C A N C E L L E D ---";
                        }
                        else
                        {
                            dr["status"] = "";
                        }
                        dtReport.Rows.Add(dr);
                    }
                }
                else
                {

                    dr["mfo_pap"] = "Operations " + Environment.NewLine + Environment.NewLine + "MFO 1: Integrated Policies and Programs for Mindanao " + Environment.NewLine + Environment.NewLine + Environment.NewLine + "      " + PAPCode;
                    dr["from_obect_expenditure"] = dtAnnexATitle.Rows[i][1].ToString();
                    dr["from_total"] = dt.Rows[0][2].ToString();
                    dr["to_obect_expenditure"] = dt.Rows[0][3].ToString();
                    dr["to_total"] = dt.Rows[0][4].ToString();

                    if (this.IsCancelled == "1")
                    {
                        dr["status"] = "--- C A N C E L L E D ---";
                    }
                    else
                    {
                        dr["status"] = "";
                    }
                    dtReport.Rows.Add(dr);
                }


            }


            DataSet ds = new DataSet();
            ds.Tables.Add(dtReport);

            LoadReportAnnexAReport(ds);
          


        }
        private void LoadReportAnnexAReport(DataSet dsRepo)
        {
            realignment_annex_a _rptData = new realignment_annex_a();

            String _path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"ReportSchemas\";
            DataSet ds = dsRepo.Copy();
            
           // ds.WriteXmlSchema(_path + "div_AnnexA.xml");
            _rptData.SetDataSource(ds);

            _report.ViewerCore.Zoom(70);
            _report.ViewerCore.ReportSource = _rptData;
        }
        private void LoadReportAnnexBReport(DataSet dsRepo)
        {
            realignment_annex_b _rptData = new realignment_annex_b();

            String _path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"ReportSchemas\";
            DataSet ds = dsRepo.Copy();

            //ds.WriteXmlSchema(_path + "div_AnnexB.xml");
            _rptData.SetDataSource(ds);

            _report.ViewerCore.Zoom(70);
            _report.ViewerCore.ReportSource = _rptData;
        }
        private void LoadReportAnnexB()
        {
            try
            {

            

            DataTable dtAnnexB = c_ppmp.FetchDataAnnexBDefficient(DivID, "2017",this.RAFDESCRIPTION).Tables[0].Copy();
            DataTable dtRafNo = c_ppmp.FetchRAFNo(DivID,this.RAFDESCRIPTION).Tables[0].Copy();
            dtReport = new DataTable();

            String _rafNo = dtRafNo.Rows[0]["raf_no"].ToString();
            String _rafDate = Convert.ToDateTime(dtRafNo.Rows[0]["entry_date"].ToString()).ToShortDateString();
            dtReport.Columns.Add("raf_no");
            dtReport.Columns.Add("group");
            dtReport.Columns.Add("programs_activity");
            dtReport.Columns.Add("responsibility_center");
            dtReport.Columns.Add("allotmentclass");
            dtReport.Columns.Add("obect_expenditure");
            dtReport.Columns.Add("amount");
            dtReport.Columns.Add("entry_date");
            dtReport.Columns.Add("status");

            double Total = 0.00;
            double Contigency = 0.00;

            for (int i = 0; i < dtAnnexB.Rows.Count; i++)
            {
                DataRow dr = dtReport.NewRow();

                dr["raf_no"] = _rafNo;
                dr["group"] = "DEFICIENT ITEMS (TO):";
                dr["programs_activity"] = PAPCode.Split('-')[0].Trim();
                dr["responsibility_center"] = PAPCode.Replace(PAPCode.Split('-')[0].Trim(), "").Replace(" -", "").Trim() + " - " + this.DivisionName; ;
               
                if (this.DivisionName.Contains("- CO"))
                {
                    dr["allotmentclass"] = "CO";
                }
                else
                {
                    dr["allotmentclass"] = "MOOE";
                }
                dr["obect_expenditure"] = dtAnnexB.Rows[i][0].ToString();
                dr["amount"] = dtAnnexB.Rows[i][1].ToString();
                dr["entry_date"] = _rafDate;

                if (this.IsCancelled == "1")
                {
                    dr["status"] = "--- C A N C E L L E D ---";
                }
                else
                {
                    dr["status"] = "";
                }
                dtReport.Rows.Add(dr);

            }
            dtAnnexB = c_ppmp.FetchDataAnnexBSource(DivID, "2017",this.RAFDESCRIPTION).Tables[0].Copy();

            for (int i = 0; i < dtAnnexB.Rows.Count; i++)
            {
                DataRow dr = dtReport.NewRow();
                dr["raf_no"] = _rafNo;
                dr["group"] = "SOURCE ITEMS (FROM):";
                dr["programs_activity"] = PAPCode.Split('-')[0].Trim();
                dr["responsibility_center"] = PAPCode.Replace(PAPCode.Split('-')[0].Trim(), "").Replace(" -", "").Trim() + " - " + this.DivisionName; ;
             
                if (this.DivisionName.Contains("- CO"))
                {
                    dr["allotmentclass"] = "CO";
                }
                else
                {
                    dr["allotmentclass"] = "MOOE";
                }
                dr["obect_expenditure"] = dtAnnexB.Rows[i][0].ToString();
                dr["amount"] = "-" + dtAnnexB.Rows[i][1].ToString();
                dr["entry_date"] = _rafDate;

                if (this.IsCancelled == "1")
                {
                    dr["status"] = "--- C A N C E L L E D ---";
                }
                else
                {
                    dr["status"] = "";
                }
                dtReport.Rows.Add(dr);

            }
            

            DataSet ds = new DataSet();
            ds.Tables.Add(dtReport);

            LoadReportAnnexBReport(ds);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + Environment.NewLine + "On Line No : " + GetLineNumber(ex).ToString());
                
            }
        }

        public int GetLineNumber(Exception ex)
        {
            var lineNumber = 0;
            const string lineSearch = ":line ";
            var index = ex.StackTrace.LastIndexOf(lineSearch);
            if (index != -1)
            {
                var lineNumberText = ex.StackTrace.Substring(index + lineSearch.Length);
                if (int.TryParse(lineNumberText, out lineNumber))
                {
                }
            }
            return lineNumber;
        }
        private void btnAnnexA_Click(object sender, RoutedEventArgs e)
        {
            this.LoadReportAnnexA();
        }

        private void btnAnnexB_Click(object sender, RoutedEventArgs e)
        {
            this.LoadReportAnnexB();
        }
    }
}
