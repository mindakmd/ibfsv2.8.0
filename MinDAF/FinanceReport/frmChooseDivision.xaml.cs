﻿using ReportTool.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FinanceReport
{
    /// <summary>
    /// Interaction logic for frmChooseDivision.xaml
    /// </summary>
    public partial class frmChooseDivision : Window
    {
        public event EventHandler SelectDone;


        public String DivisionId { get; set; }
        public String DivisionPap { get; set; }
        public String DivisionName { get; set; }

        public String RAFNO { get; set; }
        public String RAFID { get; set; }
        public String RAFDESCRIPTION { get; set; }
        public String RAFSTATUS { get; set; }

        private List<RafData> _RafData = new List<RafData>();

        clsData c_data = new clsData();
        DataTable dtData;
        public frmChooseDivision()
        {
            InitializeComponent();
            this.Loaded += frmChooseDivision_Loaded;
        }

        void frmChooseDivision_Loaded(object sender, RoutedEventArgs e)
        {
            LoadDivision();
        }

        private void LoadRafNo(String _Type) 
        {
            _RafData.Clear();

            dtData.DefaultView.RowFilter = "Division_Desc ='" + cmbDivision.SelectedItem + "'";
            DataTable dt = dtData.DefaultView.ToTable().Copy();
            String _divId = dt.Rows[0]["Division_Id"].ToString();

            var sb = new System.Text.StringBuilder(175);
          
                sb.AppendLine(@"SELECT ");
                sb.AppendLine(@"	raf.id,CONCAT(raf.year,'-',FORMAT(raf.month,'00'),'-',FORMAT(raf.number,'0000')) as raf_no,");
                sb.AppendLine(@"    raf.alignment_id as description,raf.cancelled");
                sb.AppendLine(@"FROM mnda_raf_no raf");
                sb.AppendLine(@"WHERE raf.div_id = " + _divId + " AND raf.mf_type = '"+ _Type +"'");
          

            foreach (DataRow item in c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Rows)
            {
                RafData _var = new RafData();

                _var.Id = item["id"].ToString();
                _var.RAFNO = item["raf_no"].ToString();
                _var.Description = item["description"].ToString();
                _var.Status = item["cancelled"].ToString();
                _RafData.Add(_var);
            }



            grdData.DataSource = null;
            grdData.DataSource = _RafData;

            grdData.FieldLayouts[0].Fields["Id"].Visibility = System.Windows.Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["Status"].Visibility = System.Windows.Visibility.Collapsed;
        }
        private void LoadDivision() 
        {
            var sb = new System.Text.StringBuilder(118);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"div.Division_Id,");
            sb.AppendLine(@"CONCAT(div.Division_Code,' - ',dso.Description) as Division_Code,");
            sb.AppendLine(@"div.Division_Desc");
            sb.AppendLine(@"FROM Division div");
            sb.AppendLine(@"INNER JOIN DBM_Sub_Office dso on dso.PAP =div.Division_Code");
            sb.AppendLine(@"WHERE NOT div.is_sub_division = -1");

             dtData = c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Copy();

            cmbDivision.Items.Clear();

            foreach (DataRow item in dtData.Rows)
            {
                cmbDivision.Items.Add(item["Division_Desc"].ToString());
            }
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (SelectDone!=null)
            {
                dtData.DefaultView.RowFilter ="Division_Desc ='"+ cmbDivision.SelectedItem +"'";
                DataTable dt = dtData.DefaultView.ToTable().Copy();

                if (SelectDone!=null)
	            {
                    this.DivisionId = dt.Rows[0][0].ToString();
                    this.DivisionPap = dt.Rows[0][1].ToString();
                    this.DivisionName = dt.Rows[0][2].ToString();
                    RafData _selected = (RafData)grdData.ActiveDataItem;

                    this.RAFID = _selected.Id;
                    this.RAFNO = _selected.RAFNO;
                    this.RAFDESCRIPTION = _selected.Description;
                    this.RAFSTATUS = _selected.Status;
                    this.SelectDone(this, new EventArgs());
                    this.DialogResult = true;
	            }
               
            }
        }
        private void LoadFundType()
        {
            cmbFundType.Items.Clear();
            cmbFundType.Items.Add("Current Appropriation");
            cmbFundType.Items.Add("Continuing Appropriation");
        }

        private void cmbDivision_DropDownClosed(object sender, EventArgs e)
        {
            LoadFundType();
            cmbFundType.Focus();
            cmbFundType.IsDropDownOpen = true;
        }

        private void cmbFundType_DropDownClosed(object sender, EventArgs e)
        {
            if (cmbFundType.SelectedItem ==null)
            {
                return;
            }
            String _ftype = "";
            if (cmbFundType.SelectedItem.ToString() == "Current Appropriation")
            {
                _ftype = "mf-1";
            }
            else if (cmbFundType.SelectedItem.ToString() =="Continuing Appropriation")
            {
                _ftype = "mf-2";
            }
            LoadRafNo(_ftype);
        }

    }
    public class RafData 
    {
        public String Id { get; set; }
        public String RAFNO { get; set; }
        public String Description { get; set; }
        public String Status { get; set; }
    }
   
}
