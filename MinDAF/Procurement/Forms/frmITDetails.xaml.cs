﻿using Procurement.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Procurement.Forms
{
    /// <summary>
    /// Interaction logic for frmITDetails.xaml
    /// </summary>
    public partial class frmITDetails : Window
    {

        public event EventHandler RefreshData;
        public String DivId { get; set; }
        public String UserId { get; set; }

        public String TOCode { get; set; }
        public String TODescription { get; set; }
        public String TOPersonnel { get; set; }
        public String TOPurpose { get; set; }
        List<ITDetails> MainDetails = new List<ITDetails>();
        private clsIT c_it = new clsIT();

        public frmITDetails()
        {
            InitializeComponent();
            this.Loaded += frmITDetails_Loaded;
        }

        void frmITDetails_Loaded(object sender, RoutedEventArgs e)
        {
            Initialize();
        }

        private void InitAMPM() 
        {
           

            txtName.Focus();
        }


        private void Initialize() 
        {
            MainDetails.Clear();
            InitAMPM();
            grdDetails.DataSource = MainDetails;


            InitColumns();

            if (this.TOCode!="")
            {
                txtName.Text = this.TOPersonnel;
                btnTO.Content = this.TOCode + " - " + this.TODescription;
                txtPurpose.Text = this.TOPurpose;
                dteDate.SelectedDate = DateTime.Now;
                txtName.IsReadOnly = true;
                txtPurpose.IsReadOnly = true;
                dteDate.IsEnabled = false;

                dteDateStart.Focus();
                

            }

        }        
        private void AddDetails() 
        {
          ITDetails _details = new ITDetails();
          _details.Dates = dteDateStart.SelectedDate.Value.ToShortDateString();
          _details.Hotel_Allowance = txtRateHotel.Value.ToString();
          _details.Incidental = txtRateIncidental.Value.ToString();
          _details.Meal_Allowance = txtRateMeal.Value.ToString();
          _details.Means_Transportation = txtMeansTranspo.Text;
          _details.Places = txtPlaces.Text;
          _details.Time_Arrive = tmeControlArr.TimeSelected;
          _details.Time_Depart = tmeControlDep.TimeSelected;
          _details.Transpo_Expenses = txtTransportExpense.Value.ToString();

            double _hotel = Convert.ToDouble(txtRateHotel.Value);
            double _meal = Convert.ToDouble(txtRateMeal.Value);
            double _incidental = Convert.ToDouble(txtRateIncidental.Value);
            double _transpo_expense = Convert.ToDouble(txtTransportExpense.Value);

            _details.Total_Amount = (_hotel + _meal + _incidental + _transpo_expense).ToString();

            MainDetails.Add(_details); 
            ClearEntries();
            grdDetails.DataSource = null;
            grdDetails.DataSource = MainDetails;
            InitColumns();
            
        }
        private void InitColumns()
        {
            grdDetails.FieldLayouts[0].Fields[0].Label = "Date";
            grdDetails.FieldLayouts[0].Fields[0].Width = new Infragistics.Windows.DataPresenter.FieldLength(30);

            grdDetails.FieldLayouts[0].Fields[1].Label = "Places to be visited";
            grdDetails.FieldLayouts[0].Fields[1].Width = new Infragistics.Windows.DataPresenter.FieldLength(80);

            grdDetails.FieldLayouts[0].Fields[2].Label = "Departure";
            grdDetails.FieldLayouts[0].Fields[2].Width = new Infragistics.Windows.DataPresenter.FieldLength(35);
            
            grdDetails.FieldLayouts[0].Fields[3].Label = "Arrival";
            grdDetails.FieldLayouts[0].Fields[3].Width = new Infragistics.Windows.DataPresenter.FieldLength(35);

            grdDetails.FieldLayouts[0].Fields[4].Label = "Means of Transportation";
            grdDetails.FieldLayouts[0].Fields[4].Width = new Infragistics.Windows.DataPresenter.FieldLength(45);

            grdDetails.FieldLayouts[0].Fields[5].Label = "Transportation Expense";
            grdDetails.FieldLayouts[0].Fields[5].Width = new Infragistics.Windows.DataPresenter.FieldLength(45);

            grdDetails.FieldLayouts[0].Fields[6].Label = "Hotel";
            grdDetails.FieldLayouts[0].Fields[6].Width = new Infragistics.Windows.DataPresenter.FieldLength(25);

            grdDetails.FieldLayouts[0].Fields[7].Label = "Meals";
            grdDetails.FieldLayouts[0].Fields[7].Width = new Infragistics.Windows.DataPresenter.FieldLength(25);

            grdDetails.FieldLayouts[0].Fields[8].Label = "Incidental";
            grdDetails.FieldLayouts[0].Fields[8].Width = new Infragistics.Windows.DataPresenter.FieldLength(25);

            grdDetails.FieldLayouts[0].Fields[9].Label = "Total Amount";
            grdDetails.FieldLayouts[0].Fields[9].Width = new Infragistics.Windows.DataPresenter.FieldLength(30);
            grdDetails.FieldLayouts[0].Fields[9].Visibility = System.Windows.Visibility.Collapsed;

        }

        private void ClearEntries() 
        {
            txtMeansTranspo.Clear();
           
            txtPlaces.Clear();
            txtRateHotel.Value = 0.00;
            txtRateIncidental.Value = 0.00;
            txtRateMeal.Value = 0.00;
          
            txtTransportExpense.Value =0.00;


            dteDate.Focus();
           

        }

        private void btnClearDetails_Click(object sender, RoutedEventArgs e)
        {
            ClearEntries();
        }

        private void btnAddDetails_Click(object sender, RoutedEventArgs e)
        {
            AddDetails();
        }

       
        private void SaveData() 
        {
            ITMain _Main = new ITMain();
            _Main.Code = "";
            _Main.Dates = dteDateStart.SelectedDate.Value.ToShortDateString();
            _Main.Details = txtPurpose.Text;
            _Main.UserCreated = this.UserId;

            if (c_it.SaveITData(_Main, MainDetails, this.DivId, txtName.Text, this.UserId))
            {
                MessageBox.Show("Entry has been successfully saved!!", "Itinerary Save", MessageBoxButton.OK);
                txtName.Clear();
                txtPurpose.Clear();
                ClearEntries();
                if (this.RefreshData!=null)
                {
                    RefreshData(this, new EventArgs());
                }
                this.DialogResult = true;
            } ;

        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult _res = MessageBox.Show("Confirm Save Entry?", "Save Data", MessageBoxButton.YesNo);
            if (_res.ToString() =="Yes")
            {
                SaveData();
                
            }
        }
        private frmRISTravel f_ris_travel = new frmRISTravel();
        private void btnCreateRIS_Click(object sender, RoutedEventArgs e)
        {
            f_ris_travel = new frmRISTravel();
            f_ris_travel.ShowDialog();
        }
    }
}
