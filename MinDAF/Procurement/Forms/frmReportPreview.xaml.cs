﻿using Procurement.Report;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Procurement.Forms
{
    /// <summary>
    /// Interaction logic for frmReportPreview.xaml
    /// </summary>
    public partial class frmReportPreview : Window
    {
        private String ReportType = "";
        private DataSet dsReport;
        public frmReportPreview(String _rep,DataSet _dsData)
        {
            InitializeComponent();
            this.Loaded += frmReportPreview_Loaded;
            ReportType = _rep;
            dsReport = _dsData;
        }

        void frmReportPreview_Loaded(object sender, RoutedEventArgs e)
        {
            switch (ReportType)
            {
                case "Travel Order":
                    LoadReportTO(dsReport);
                    break;
                case "PPMP_CONSO":
                    LoadReportPPMPCONSO(dsReport);

                    break;
            }
        }

        private void LoadReportTO(DataSet dsData)
        {
            crtTO _rptData = new crtTO();

            String _path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"ReportSchemas\";
            DataSet ds = dsData.Copy();

         // ds.WriteXmlSchema(_path + "TO.xml");
            _rptData.SetDataSource(ds);

            _report.ViewerCore.Zoom(70);
            _report.ViewerCore.ReportSource = _rptData;
        }

        private void LoadReportPPMPCONSO(DataSet dsData)
        {
            crtPPMPConso _rptData = new crtPPMPConso();

            String _path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"ReportSchemas\";
            DataSet ds = dsData.Copy();

         //   ds.WriteXmlSchema(_path + "PPMP_CONSO.xml");
            _rptData.SetDataSource(ds);

            _report.ViewerCore.Zoom(70);
            _report.ViewerCore.ReportSource = _rptData;
        }
    }
}
