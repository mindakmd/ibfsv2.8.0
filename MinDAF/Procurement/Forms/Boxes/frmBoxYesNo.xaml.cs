﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Procurement.Forms.Boxes
{
    /// <summary>
    /// Interaction logic for frmBox.xaml
    /// </summary>
    public partial class frmBox : Window
    {
        private String Message ="";
        public frmBox(String _message)
        {
            InitializeComponent();
            Message = _message;
            this.Loaded += frmBox_Loaded;
        }

        void frmBox_Loaded(object sender, RoutedEventArgs e)
        {
            SetMessage();
        }

        private void SetMessage() 
        {
            lblMessage.Content = Message;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
