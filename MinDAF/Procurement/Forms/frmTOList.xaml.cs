﻿using Procurement.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Procurement.Forms
{
    /// <summary>
    /// Interaction logic for frmTOList.xaml
    /// </summary>
    public partial class frmTOList : Window
    {
        private clsTO _cto = new clsTO();

        public String DivisionId { get; set; }
        public String SelectedCode { get; set; }
        public String SelectedDescription { get; set; }
        public String SelectedPersonnel { get; set; }
        public String SelectedPurpose { get; set; }

        private List<TORefference> _ListTO = new List<TORefference>();

        public frmTOList()
        {
            InitializeComponent();
            this.Loaded += frmTOList_Loaded;
        }

        void frmTOList_Loaded(object sender, RoutedEventArgs e)
        {
            LoadAvailableTO();
        }

        private void LoadAvailableTO()         
        {
            _ListTO = new List<TORefference>();
           _ListTO= _cto.FetchTOList(this.DivisionId);

           grdData.DataSource = null;
           grdData.DataSource = _ListTO;

           grdData.FieldLayouts[0].Fields["Code"].Visibility = System.Windows.Visibility.Collapsed;
           grdData.FieldLayouts[0].Fields["Purpose"].Visibility = System.Windows.Visibility.Collapsed;
        }

       

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (grdData.ActiveDataItem!=null)
            {
                TORefference _selected = (TORefference)grdData.ActiveDataItem;

                this.SelectedCode = _selected.Code;
                this.SelectedDescription = _selected.Subject;
                this.SelectedPersonnel = _selected.Personnel;
                this.SelectedPurpose = _selected.Purpose;
                this.DialogResult = true;
            }
        }

       
    }

    
}
