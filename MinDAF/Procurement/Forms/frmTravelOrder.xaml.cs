﻿using Procurement.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Procurement.Forms
{
    /// <summary>
    /// Interaction logic for frmTravelOrder.xaml
    /// </summary>
    public partial class frmTravelOrder : Window
    {
        public List<TO_PersonTravel> _listpersonnel = new List<TO_PersonTravel>();

        public Boolean IsEdit { get; set; }

        public String TOCode { get; set; }
        public String DivId { get; set; }
        public String User { get; set; }

        private List<TO_FundSource> _fsource = new List<TO_FundSource>();

        private clsTO c_to = new clsTO();
        public frmTravelOrder()
        {
            InitializeComponent();
            this.Loaded += frmTravelOrder_Loaded;
        }

        void frmTravelOrder_Loaded(object sender, RoutedEventArgs e)
        {
            FetchFundSource();
            grdData.DataSource = null;
            grdData.DataSource = _listpersonnel;
            if (IsEdit)
            {
                btnAdd.Content = "Update";
            }
            else
            {
                btnAdd.Content = "Save";
            }
        }

        private void AddTOPersonnel() 
        {
            TO_PersonTravel _pdata = new TO_PersonTravel();
            _pdata.PERSONNEL = txtName.Text;
            _pdata.POSITION = txtPosition.Text;

            _listpersonnel.Add(_pdata);

            grdData.DataSource = null;
            grdData.DataSource = _listpersonnel;
        }
    
        private void FetchFundSource() 
        {
            cmbMindaFund.Items.Clear();

            _fsource = c_to.FetchFundSource(DateTime.Now.Year.ToString());
            foreach (var item in _fsource)
            {
                cmbMindaFund.Items.Add(item.FundSourceData);
            }
        }

        private void SaveData() 
        {
            DateTime _date = dteDate.SelectedDate.Value;

            c_to.SaveTravelOrder(_listpersonnel, txtTo.Text, txtSubject.Text, txtDetails_1.Text, txtDetails_2.Text, cmbMindaFund.SelectedItem.ToString(), DivId, User, _date.ToString());
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            SaveData();
            this.DialogResult = true;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            AddTOPersonnel();
            txtName.Clear();
            txtPosition.Clear();
            txtName.Focus();
            txtName.SelectAll();
        }
    }
}
