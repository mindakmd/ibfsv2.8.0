﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Procurement.Forms
{
    /// <summary>
    /// Interaction logic for frmTravelSchedule.xaml
    /// </summary>
    public partial class frmTravelSchedule : Window
    {
        public TravelItenirary TravelInfo { get; set; }
        public String _Months { get; set; }

        public Int32 _DayLimit { get; set; }
        public frmTravelSchedule()
        {
            InitializeComponent();
            this.Loaded += frmTravelSchedule_Loaded;
        }

        private void SetSelectedMonth() 
        {
            Int32  _month = 0;
            switch (this._Months)
	        {
                    case "Jan":_month = 1;
                    break;
                    case "Feb":_month = 2;
                    break;
                    case "Mar":_month = 3;
                    break;
                    case "Apr":_month = 4;
                    break;
                     case "May":_month = 5;
                    break;
                     case "Jun":_month = 6;
                    break;
                     case "Jul":_month = 7;
                    break;
                     case "Aug":_month = 8;
                    break;
                     case "Sep":_month = 9;
                    break;
                     case "Oct":_month = 10;
                    break;
                     case "Nov":_month = 11;
                    break;
                     case "Dec":_month = 12;
                    break;
		     
	        }
            DateTime _from = new DateTime(DateTime.Now.Year, _month, 1);
            Int32 _days = DateTime.DaysInMonth(_from.Year, _from.Month);
            DateTime _to = new DateTime(_from.Year, _from.Month, _days);

            dteFrom.SelectedDate = _from;
            dteTo.SelectedDate = _from;
            dteFrom.DisplayDateStart = _from;
            dteFrom.DisplayDateEnd = _to;
            dteTo.DisplayDateStart = _from;
            dteTo.DisplayDateEnd = _to;
           
        }

        void frmTravelSchedule_Loaded(object sender, RoutedEventArgs e)
        {
            SetSelectedMonth();


        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            DateTime _from = (DateTime)dteFrom.SelectedDate;
            DateTime _to = (DateTime)dteTo.SelectedDate;
            TimeSpan _days = (DateTime)dteFrom.SelectedDate - (DateTime)dteTo.SelectedDate;
            if ((_to - _from).TotalDays != _DayLimit)
            {
                MessageBox.Show("Number of day travel must be equal on the budget planned");
            }           
            else
            {
                TravelInfo = new TravelItenirary();
                TravelInfo.Destination = txtDestination.Text;
                TravelInfo.Personnel = txtPersonel.Text;
                TravelInfo.TravelDate = _Months + " " + Convert.ToDateTime(dteFrom.SelectedDate).Day.ToString() + " - " + Convert.ToDateTime(dteTo.SelectedDate).Day.ToString();
                this.DialogResult = true;
            }

          
        }
    }
    public class TravelItenirary 
    {
        public String Personnel { get; set; }
        public String Destination { get; set; }
        public String TravelDate { get; set; }
    }
}
