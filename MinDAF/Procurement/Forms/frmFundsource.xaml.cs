﻿using Procurement.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Procurement.Forms
{
    /// <summary>
    /// Interaction logic for frmFundsource.xaml
    /// </summary>
    public partial class frmFundsource : Window
    {
        public String FundSource { get; set; }
        public String FundCode { get; set; }
        public String FundType { get; set; }

        public String DivId { get; set; }
        public String _Year { get; set; }

        private List<FundSourceList> f_list = new List<FundSourceList>();

        private clsRIS c_ris = new clsRIS();

        public frmFundsource()
        {
            InitializeComponent();
            this.Loaded += frmFundsource_Loaded;
        }

        void frmFundsource_Loaded(object sender, RoutedEventArgs e)
        {
            GenerateYear();
        }

        private void FetchFundsource() 
        {
            _Year = cmbYear.SelectedItem.ToString();
            f_list = c_ris.FetchFundSource(DivId, _Year);
            grdData.DataSource = null;
            grdData.DataSource = f_list;
            grdData.FieldLayouts[0].Fields["FundSource"].Label = "SELECT FUNDSOURCE";
            grdData.FieldLayouts[0].Fields["FundCode"].Visibility = System.Windows.Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["FundType"].Visibility = System.Windows.Visibility.Collapsed;
        }
        private void GenerateYear()
        {

            Int32 _start_year = DateTime.Now.Year;
            cmbYear.Items.Clear();


            for (int i = 0; i < 2050; i++)
            {
                if (_start_year > 2050)
                {
                    break;
                }
                else
                {
                    cmbYear.Items.Add(_start_year);
                    _start_year += 1;
                }

            }

        }
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            if (grdData.ActiveDataItem==null)
            {
                MessageBox.Show("Select Fundsource first");
            }
            else
            {
                FundSourceList _selected = (FundSourceList)grdData.ActiveDataItem;

                FundSource = _selected.FundSource;
                FundCode = _selected.FundCode;
                FundType = _selected.FundType;
                this.DialogResult = true;

            }
        }

        private void cmbYear_DropDownClosed(object sender, EventArgs e)
        {
            FetchFundsource();
        }
    }
}
