﻿#pragma checksum "..\..\..\..\Usercontrol\User\ctrlPR.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "5F4D8D8DCBB107F2B1271C5C2871557A"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Infragistics.Windows.DataPresenter;
using Infragistics.Windows.DataPresenter.Calculations;
using RootLibrary.WPF.Localization;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Procurement.Usercontrol.User {
    
    
    /// <summary>
    /// ctrlPR
    /// </summary>
    public partial class ctrlPR : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 33 "..\..\..\..\Usercontrol\User\ctrlPR.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtOffice;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\..\Usercontrol\User\ctrlPR.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtRespoCenter;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\..\Usercontrol\User\ctrlPR.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtPRNo;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\..\..\Usercontrol\User\ctrlPR.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtDate;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\..\Usercontrol\User\ctrlPR.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Infragistics.Windows.DataPresenter.XamDataGrid grdData;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\..\..\Usercontrol\User\ctrlPR.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtPurpose;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\..\..\Usercontrol\User\ctrlPR.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtPapCode;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\..\Usercontrol\User\ctrlPR.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbActivity;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\..\..\Usercontrol\User\ctrlPR.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbYear;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\..\..\Usercontrol\User\ctrlPR.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbProject;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\..\..\Usercontrol\User\ctrlPR.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbProgram;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\..\..\Usercontrol\User\ctrlPR.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbOutput;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\..\..\Usercontrol\User\ctrlPR.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbType;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\..\..\Usercontrol\User\ctrlPR.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbMonths;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\..\..\Usercontrol\User\ctrlPR.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtExpenseItem;
        
        #line default
        #line hidden
        
        
        #line 81 "..\..\..\..\Usercontrol\User\ctrlPR.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSubmit;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Procurement;component/usercontrol/user/ctrlpr.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Usercontrol\User\ctrlPR.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.txtOffice = ((System.Windows.Controls.TextBox)(target));
            return;
            case 2:
            this.txtRespoCenter = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.txtPRNo = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.txtDate = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.grdData = ((Infragistics.Windows.DataPresenter.XamDataGrid)(target));
            return;
            case 6:
            this.txtPurpose = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.txtPapCode = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.cmbActivity = ((System.Windows.Controls.ComboBox)(target));
            
            #line 69 "..\..\..\..\Usercontrol\User\ctrlPR.xaml"
            this.cmbActivity.DropDownClosed += new System.EventHandler(this.cmbActivity_DropDownClosed);
            
            #line default
            #line hidden
            return;
            case 9:
            this.cmbYear = ((System.Windows.Controls.ComboBox)(target));
            
            #line 71 "..\..\..\..\Usercontrol\User\ctrlPR.xaml"
            this.cmbYear.DropDownClosed += new System.EventHandler(this.cmbYear_DropDownClosed);
            
            #line default
            #line hidden
            return;
            case 10:
            this.cmbProject = ((System.Windows.Controls.ComboBox)(target));
            
            #line 72 "..\..\..\..\Usercontrol\User\ctrlPR.xaml"
            this.cmbProject.DropDownClosed += new System.EventHandler(this.cmbProject_DropDownClosed);
            
            #line default
            #line hidden
            return;
            case 11:
            this.cmbProgram = ((System.Windows.Controls.ComboBox)(target));
            
            #line 73 "..\..\..\..\Usercontrol\User\ctrlPR.xaml"
            this.cmbProgram.DropDownClosed += new System.EventHandler(this.cmbProgram_DropDownClosed);
            
            #line default
            #line hidden
            return;
            case 12:
            this.cmbOutput = ((System.Windows.Controls.ComboBox)(target));
            
            #line 74 "..\..\..\..\Usercontrol\User\ctrlPR.xaml"
            this.cmbOutput.DropDownClosed += new System.EventHandler(this.cmbOutput_DropDownClosed);
            
            #line default
            #line hidden
            return;
            case 13:
            this.cmbType = ((System.Windows.Controls.ComboBox)(target));
            
            #line 76 "..\..\..\..\Usercontrol\User\ctrlPR.xaml"
            this.cmbType.DropDownClosed += new System.EventHandler(this.cmbType_DropDownClosed);
            
            #line default
            #line hidden
            return;
            case 14:
            this.cmbMonths = ((System.Windows.Controls.ComboBox)(target));
            
            #line 78 "..\..\..\..\Usercontrol\User\ctrlPR.xaml"
            this.cmbMonths.DropDownClosed += new System.EventHandler(this.cmbMonths_DropDownClosed);
            
            #line default
            #line hidden
            return;
            case 15:
            this.txtExpenseItem = ((System.Windows.Controls.TextBox)(target));
            return;
            case 16:
            this.btnSubmit = ((System.Windows.Controls.Button)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

