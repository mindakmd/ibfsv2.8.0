﻿using Procurement.Usercontrol;
using Procurement_Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Procurement
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ctrlLogin _login = new ctrlLogin();
        private ctrlAdmin _admin = new ctrlAdmin();
        private ctrl_Users _user = new ctrl_Users();
        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += MainWindow_Loaded;
            _login.LoginSuccessfull += _login_LoginSuccessfull;
            _login.LoginFailed += _login_LoginFailed;
        }

        void _login_LoginFailed(object sender, EventArgs e)
        {
            MessageBox.Show("Invalid Username / Password");
        }

        void _login_LoginSuccessfull(object sender, EventArgs e)
        {
            grdChild.Children.Clear();

            switch (_login.IsAdmin)
            {
                case "1":
                    _admin.Width = grdChild.Width;
                    _admin.Height = grdChild.Height;
                    grdChild.Children.Add(_admin);
                    break;
                case "2":
                    _user.Width = grdChild.Width;
                    _user.Height = grdChild.Height;
                    _user._Division = _login.Division;
                    _user._Office = _login.Office;
                    _user._DivId = _login.DivId;
                    _user._PapCode = _login.PapCode;
                    _user._User = _login.User;
                    _user.IsChief = true;
                    grdChild.Children.Add(_user);
                    break; 
                default:
                    _user.Width = grdChild.Width;
                    _user.Height = grdChild.Height;
                    _user._Division = _login.Division;
                    _user._Office = _login.Office;
                    _user._DivId = _login.DivId;
                    _user._PapCode = _login.PapCode;
                    _user._User = _login.User;
                    grdChild.Children.Add(_user);
                    break;
            }
        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            ShowLoginForm();
        }

        private void ShowLoginForm() 
        {
            grdChild.Children.Clear();

            _login.Width = grdChild.Width;
            _login.Height = grdChild.Height;

            grdChild.Children.Add(_login);

        }
    }
}
