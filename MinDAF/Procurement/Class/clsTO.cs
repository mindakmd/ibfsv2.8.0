﻿using ReportTool.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Procurement.Class
{
    class clsTO
    {
        private clsData c_data = new clsData();

        
        public List<TO_TravelOrderList> FetchTravelOrders(String DivId)
        {
            List<TO_TravelOrderList> _data = new List<TO_TravelOrderList>();

            var sb = new System.Text.StringBuilder(156);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"pto.date,");
            sb.AppendLine(@"pto.code,");
            sb.AppendLine(@"CONCAT(pto.subject,' -	',pto.details_1) as _details ,");
            sb.AppendLine(@"pto.user_created ");
            sb.AppendLine(@"FROM prc_travel_order pto WHERE status ='added'");
            sb.AppendLine(@" AND pto.division_id = "+ DivId +" ORDER BY pto.id DESC");

            foreach (DataRow item in c_data.ExecuteSQLQueryProcurement(sb.ToString()).Tables[0].Rows)
            {
                TO_TravelOrderList _var = new TO_TravelOrderList();

                _var._details = item["_details"].ToString();
                _var.code = item["code"].ToString();           
                _var.date = item["date"].ToString();
                _var.user_created = item["user_created"].ToString();     
                _data.Add(_var);
            }

            return _data;
        }
           public List<TORefference> FetchTOList(String DivId)
         {
             List<TORefference> _data = new List<TORefference>();

            var sb = new System.Text.StringBuilder(120);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"p_to.code,");
            sb.AppendLine(@"p_to.subject,");
            sb.AppendLine(@"ptp.name,");
            sb.AppendLine(@"p_to.details_1");
            sb.AppendLine(@"FROM prc_travel_order p_to");
            sb.AppendLine(@"INNER JOIN prc_travel_personnel ptp on ptp.to_code = p_to.code");
            sb.AppendLine(@"WHERE p_to.refference_status = 0");
            sb.AppendLine(@"AND p_to.status ='added'");
            sb.AppendLine(@"AND p_to.division_id ='"+ DivId +"'");



            foreach (DataRow item in c_data.ExecuteSQLQueryProcurement(sb.ToString()).Tables[0].Rows)
             {

                 TORefference _cdata = new TORefference();

                 _cdata.Code = item["code"].ToString();
                  _cdata.Subject = item["subject"].ToString();
                  _cdata.Personnel = item["name"].ToString();
                  _cdata.Purpose = item["details_1"].ToString();
                 _data.Add(_cdata);
             }

             return _data;
         }


         public List<TO_FundSource> FetchFundSource(String _year)
        {
             List<TO_FundSource> _data = new  List<TO_FundSource>();

            var sb = new System.Text.StringBuilder(175);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"CONCAT(div.Division_Code,'_',mfs.Fund_Name) as f_source");
            sb.AppendLine(@"FROM mnda_fund_source mfs");
            sb.AppendLine(@"INNER JOIN Division div on div.Division_Id = mfs.division_id");
            sb.AppendLine(@"WHERE mfs.Year = 2017");


           

            foreach (DataRow item in c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Rows)
            {
               
               TO_FundSource _cdata = new TO_FundSource();

               _cdata.FundSourceData = item["f_source"].ToString();

               _data.Add(_cdata);
            }

            return _data;
        }

         public List<TO_FundSource> FetchDivisionEmployee(String DivId)
         {
             List<TO_FundSource> _data = new List<TO_FundSource>();

             var sb = new System.Text.StringBuilder(94);
             sb.AppendLine(@"SELECT");
             sb.AppendLine(@"mua.User_Fullname as _fullname");
             sb.AppendLine(@"FROM mnda_user_accounts mua");
             sb.AppendLine(@"WHERE mua.Division_Id = 6");


             foreach (DataRow item in c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Rows)
             {

                 TO_FundSource _cdata = new TO_FundSource();

                 _cdata.FundSourceData = item["f_source"].ToString();

                 _data.Add(_cdata);
             }

             return _data;
         }
 

        public TO_TravelOrderDetails FetchTravelOrdersDetails(String to_code)
        {
            TO_TravelOrderDetails _data = new TO_TravelOrderDetails();

            var sb = new System.Text.StringBuilder(57);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"*");
            sb.AppendLine(@"FROM prc_travel_order pto");
            sb.AppendLine(@"WHERE pto.code = '"+ to_code +"'");

           
            DataTable _dtPersonnel = c_data.ExecuteSQLQueryProcurement("SELECT * FROM prc_travel_personnel WHERE to_code ='" + to_code + "'").Tables[0].Copy();

            foreach (DataRow item in c_data.ExecuteSQLQueryProcurement(sb.ToString()).Tables[0].Rows)
            {
                _data.To = item["to"].ToString();
                _data.Subject = "Official Travel To " + item["subject"].ToString();
                _data.Detail_1  = item["details_1"].ToString();
                _data.Detail_2 = item["details_2"].ToString();
                _data.TravelDate = item["date"].ToString();
                _data.MindaFund = item["minda_fund"].ToString(); ;

                List<TO_PersonTravel> _cperson = new List<TO_PersonTravel>();


                foreach (DataRow item_p in _dtPersonnel.Rows)
                {
                    TO_PersonTravel _xdata = new TO_PersonTravel();
                    _xdata.PERSONNEL = item_p["name"].ToString();
                    _xdata.POSITION =  item_p["position"].ToString() ;
                    _cperson.Add(_xdata);
                }

                _data.Person = _cperson;

                break;
            }

            return _data;
        }

        public Boolean SaveTravelOrder(List<TO_PersonTravel> _dataPerson ,String _to,String _subject,String _detail_1,String _detail_2,String minda_fund,String DivId,String _user,String _date) 
        {
            String _code = FetchTOCode();
            var sb = new System.Text.StringBuilder(354);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.prc_travel_order");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  code,");
            sb.AppendLine(@"  [to],");
            sb.AppendLine(@"  subject,");
            sb.AppendLine(@"  details_1,");
            sb.AppendLine(@"  details_2,");
            sb.AppendLine(@"  minda_fund,");
            sb.AppendLine(@"  date,");
            sb.AppendLine(@"  division_id,");
            sb.AppendLine(@"  user_created,");
            sb.AppendLine(@"  status");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@" '" + _code + "',");
            sb.AppendLine(@"  '"+ _to +"',");
            sb.AppendLine(@"  '"+ _subject +"',");
            sb.AppendLine(@"  '"+ _detail_1 +"',");
            sb.AppendLine(@"  '"+ _detail_2 +"',");
            sb.AppendLine(@"  '"+ minda_fund +"',");
            sb.AppendLine(@"  '" + _date + "',");
            sb.AppendLine(@"  '" + DivId + "',");
            sb.AppendLine(@"  '" + _user + "',");
            sb.AppendLine(@"  'added'");
            sb.AppendLine(@");");

            foreach (var item in _dataPerson)
            {

                sb.AppendLine(@"INSERT INTO  dbo.prc_travel_personnel ( to_code, name, position) ");
                sb.AppendLine(@"VALUES ('"+ _code +"','"+ item.PERSONNEL +"','"+ item.POSITION +"');");

            }

            bool _state = c_data.ExecuteQueryProcurement(sb.ToString());

            return _state;

        }

        public Boolean DeleteTO(String _to_id)
        {
            var sb = new System.Text.StringBuilder(79);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.prc_travel_order  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  status = 'deleted'");
            sb.AppendLine(@"WHERE code = '"+ _to_id +"';");

          
            bool _state = c_data.ExecuteQueryProcurement(sb.ToString());

            return _state;

        }

        private string FetchTOCode() 
        {
            var sb = new System.Text.StringBuilder(87);
            sb.AppendLine(@"");
            String _Code = c_data.ExecuteSQLQueryProcurement("SELECT  CONCAT('TO-',YEAR(GETDATE()),'-00',IDENT_CURRENT('prc_travel_order')+1) as code").Tables[0].Rows[0].ItemArray[0].ToString();

            return _Code;
        }

        private string GetUniqueKey()
            {
            int maxSize  = 8 ;
            int minSize = 5 ;
            char[] chars = new char[62];
            string a;
            a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            chars = a.ToCharArray();
            int size  = maxSize ;
            byte[] data = new byte[1];
            RNGCryptoServiceProvider  crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data) ;
            size =  maxSize ;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(size) ;
            foreach(byte b in data )
            { result.Append(b % (chars.Length )); }
             return result.ToString();
            }
    }
    public class TORefference 
    {
        public String Code { get; set; }
        public String Personnel { get; set; }
        public String Subject { get; set; }
        public String Purpose { get; set; }
        
    }
    public class TO_Person 
    {

    }
    public class TO_FundSource 
    {
        public String FundSourceData { get; set; }
    }
    public class TO_TravelOrderDetails 
    {
        public String To { get; set; }
        public String Subject { get; set; }
        public String Detail_1 { get; set; }
        public String Detail_2 { get; set; }

        public String MindaFund { get; set; }
        public String TravelDate { get; set; }

        public List<TO_PersonTravel> Person { get; set; }
    }
    public class TO_PersonTravel 
    {
        public String PERSONNEL { get; set; }
        public String POSITION { get; set; }
    }
    public class TO_TravelOrderList 
    {
        public String date{ get; set; }
        public String code{ get; set; }
        public String _details { get; set; }
        public String user_created { get; set; }
    }
}
