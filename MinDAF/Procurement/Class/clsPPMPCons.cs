﻿using ReportTool.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Procurement.Class
{
    class clsPPMPCons
    {

        private clsData c_data = new clsData();

        public DataSet ConvertReport(List<ppmpCONS_PPMPConsolidated> _data) 
        {
            DataSet _ds = new DataSet();
            DataTable _dt = new DataTable();


            _dt.Columns.Add("MOOE");
            _dt.Columns.Add("TypeExpense");
            _dt.Columns.Add("UACS");
            _dt.Columns.Add("Description");
            _dt.Columns.Add("QTY");
            _dt.Columns.Add("EstimatedBudget");
            _dt.Columns.Add("ModeProcure");
            _dt.Columns.Add("Jan");
            _dt.Columns.Add("Feb");
            _dt.Columns.Add("Mar");
            _dt.Columns.Add("Q1");
            _dt.Columns.Add("Apr");
            _dt.Columns.Add("May");
            _dt.Columns.Add("Jun");
            _dt.Columns.Add("Q2");
            _dt.Columns.Add("Jul");
            _dt.Columns.Add("Aug");
            _dt.Columns.Add("Sep");
            _dt.Columns.Add("Q3");
            _dt.Columns.Add("Oct");
            _dt.Columns.Add("Nov");
            _dt.Columns.Add("Dec");
            _dt.Columns.Add("Q4");
            _dt.Columns.Add("Total");

            _dt.TableName = "DataReport";

            foreach (ppmpCONS_PPMPConsolidated item in _data)
            {
                DataRow dr = _dt.NewRow();

                dr["MOOE"] = item.MOOE;
                dr["TypeExpense"] = item.TypeExpense ;
                dr["UACS"] = item.UACS ;
                dr["Description"] = item.Description;
                dr["QTY"] = item.QTY ;
                dr["EstimatedBudget"] = item.EstimatedBudget ;
                dr["ModeProcure"] = item.ModeProcure ;
                dr["Jan"] = item.Jan ;
                dr["Feb"] = item.Feb ;
                dr["Mar"] = item.Mar;
                dr["Q1"] = item.Q1 ;
                dr["Apr"] = item.Apr;
                dr["May"] = item.May;
                dr["Jun"] = item.Jun;
                dr["Q2"] = item.Q2;
                dr["Jul"] = item.Jul;
                dr["Aug"] =item.Aug ;
                dr["Sep"] = item.Sep;
                dr["Q3"] = item.Q3 ;
                dr["Oct"] = item.Oct;
                dr["Nov"] = item.Nov;
                dr["Dec"] = item.Dec ;
                dr["Q4"] = item.Q4 ;
                dr["Total"] = item.Total;


                _dt.Rows.Add(dr);


            }


            _ds.Tables.Add(_dt);



            return _ds;

        }

        public List<ppmpCONS_ExpenditureItems> FetchItemExpenditures()         
        {
            List<ppmpCONS_ExpenditureItems> _data = new List<ppmpCONS_ExpenditureItems>();

            var sb = new System.Text.StringBuilder(135);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"	mooe.id,");
            sb.AppendLine(@"	mooe.name");
            sb.AppendLine(@"FROM mnda_mooe_sub_expenditures mooe");
            sb.AppendLine(@"WHERE mooe.is_active = 1 AND mooe.is_lib = 0 AND expense_type='MOOE'");
            sb.AppendLine(@"ORDER BY mooe.id ASC");


            foreach (DataRow item in c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Rows)
            {
                ppmpCONS_ExpenditureItems _var = new ppmpCONS_ExpenditureItems();

                _var.Id= item["id"].ToString();
                _var.ExpenseItem = item["name"].ToString();
                _data.Add(_var);
            }

            return _data;
        }

      
        public List<ppmpCONS_InventoryItems> FetchItemInventory()
        {
            List<ppmpCONS_InventoryItems> _data = new List<ppmpCONS_InventoryItems>();

            var sb = new System.Text.StringBuilder(233);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mpi.item_specifications");
            sb.AppendLine(@"FROM mnda_activity_data mad");
            sb.AppendLine(@"INNER JOIN mnda_procurement_items mpi on mpi.item_specifications=mad.type_service");
            sb.AppendLine(@"WHERE NOT mad.type_service ='' and mad.is_approved = 1");
            sb.AppendLine(@"GROUP BY mpi.item_specifications");


            foreach (DataRow item in c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Rows)
            {
                ppmpCONS_InventoryItems _var = new ppmpCONS_InventoryItems();

                _var.item_specifications = item["item_specifications"].ToString();
                _data.Add(_var);
            }

            return _data;
        }

        public List<ppmpCONS_DivisionExpense> FetchExpenseItemsCSE(String ExpId)
        {
            List<ppmpCONS_DivisionExpense> _data = new List<ppmpCONS_DivisionExpense>();

            var sb = new System.Text.StringBuilder(495);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mpe.acountable_division_code,");
            sb.AppendLine(@"msub.id,");
            sb.AppendLine(@"msub.name,");
            sb.AppendLine(@"mad.month,");
            sb.AppendLine(@"mad.rate,");
            sb.AppendLine(@"mad.quantity,");
            sb.AppendLine(@"mad.total,");
            sb.AppendLine(@"mad.no_days,");
            sb.AppendLine(@"mad.no_staff,");
            sb.AppendLine(@"mad.is_revision,");
            sb.AppendLine(@"mad.travel_allowance,");
            sb.AppendLine(@"mad.destination,");
            sb.AppendLine(@"mad.type_service");
            sb.AppendLine(@"FROM mnda_activity_data mad");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.id = mad.activity_id");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.id = ma.output_id");
            sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures msub on msub.id= mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"WHERE mad.is_approved = 1 AND msub.id=" + ExpId + "");
            sb.AppendLine(@"AND mad.status = 'FINANCE APPROVED'");


            foreach (DataRow item in c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Rows)
            {
                ppmpCONS_DivisionExpense _var = new ppmpCONS_DivisionExpense();

                _var.DivId = item["acountable_division_code"].ToString();
                _var.id = item["id"].ToString();
                _var.name = item["name"].ToString();
                _var.month = item["month"].ToString();
                _var.rate = item["rate"].ToString();
                _var.quantity = item["quantity"].ToString();
                _var.total = item["total"].ToString();
                _var.is_revision = item["is_revision"].ToString();
                _var.no_days = item["no_days"].ToString();
                _var.no_staff = item["no_staff"].ToString();
                _var.allowance = item["travel_allowance"].ToString();
                _var.destination = item["destination"].ToString();
                _var.destination = item["type_service"].ToString();
                if (_var.destination.Contains("Plotter"))
                {

                }
                _data.Add(_var);
            }

            return _data;
        }

        public List<ppmpCONS_DivisionExpense> FetchExpenseItems(String DivId,String ExpId) 
        {
            List<ppmpCONS_DivisionExpense> _data = new List<ppmpCONS_DivisionExpense>();

            var sb = new System.Text.StringBuilder(495);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"msub.id,");
            sb.AppendLine(@"msub.name,");
            sb.AppendLine(@"mad.month,");
            sb.AppendLine(@"mad.rate,");
            sb.AppendLine(@"mad.quantity,");
            sb.AppendLine(@"mad.total,");
            sb.AppendLine(@"mad.no_days,");
            sb.AppendLine(@"mad.no_staff,");
            sb.AppendLine(@"mad.is_revision,");
            sb.AppendLine(@"mad.travel_allowance,");
            sb.AppendLine(@"mad.destination,");
            sb.AppendLine(@"mad.type_service");
            sb.AppendLine(@"FROM mnda_activity_data mad");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.id = mad.activity_id");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.id = ma.output_id");
            sb.AppendLine(@"INNER JOIN mnda_program_encoded mpe on mpe.id = mpo.program_code");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures msub on msub.id= mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"WHERE  mpe.acountable_division_code = "+ DivId +" AND mad.is_approved = 1 AND msub.id="+ ExpId +"");
            sb.AppendLine(@"AND mad.status = 'FINANCE APPROVED'");


            foreach (DataRow item in c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Rows)
            {
                ppmpCONS_DivisionExpense _var = new ppmpCONS_DivisionExpense();

                _var.id = item["id"].ToString();
                _var.name = item["name"].ToString();
                _var.month = item["month"].ToString();
                _var.rate = item["rate"].ToString();
                _var.quantity = item["quantity"].ToString();
                _var.total = item["total"].ToString();
                _var.is_revision = item["is_revision"].ToString();
                _var.no_days = item["no_days"].ToString();
                _var.no_staff = item["no_staff"].ToString();
                _var.allowance = item["travel_allowance"].ToString();
                _var.destination = item["destination"].ToString();
                _var.destination = item["type_service"].ToString();
                if (_var.destination.Contains("Plotter"))
                {
                    
                }
                _data.Add(_var);
            }

            return _data;
        }
        public List<ppmpCONS_Division> FetchDivisions() 
        {
            List<ppmpCONS_Division> _data = new List<ppmpCONS_Division>();

            var sb = new System.Text.StringBuilder(137);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"div.Division_Id as Id,");
            sb.AppendLine(@"div.DivisionAccro as DivName");
            sb.AppendLine(@"FROM Division div");
            sb.AppendLine(@"WHERE div.is_sub_division = 0 OR div.is_sub_division = 1 ");
            //

            foreach (DataRow item in c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Rows)
            {
                ppmpCONS_Division _var = new ppmpCONS_Division();

                _var.Id = item["Id"].ToString();
                _var.DivName = item["DivName"].ToString();
    
                _data.Add(_var);
            }

            return _data;
        }

    }
    public class ppmpCONS_InventoryItems
    {
        public String item_specifications { get; set; }

    }
    public class ppmpCONS_Division
    {
        public String Id { get; set; }
        public String DivName { get; set; }

    }

    public class ppmpCONS_DivisionExpense 
    {
             public String DivId { get; set; }
             public String id { get; set; }
             public String name { get; set; }
             public String month { get; set; }
             public String rate { get; set; }
             public String quantity { get; set; }
             public String total { get; set; }
             public String is_revision { get; set; }
             public String no_days { get; set; }
             public String no_staff { get; set; }
             public String allowance { get; set; }
             public String destination { get; set; }
             public String type_service { get; set; }
    }

    public class ppmpCONS_ExpenditureItems
    {
        public String Id { get; set; }
        public String ExpenseItem { get; set; }
    }
    public class ppmpCONS_PPMPConsolidated 
    {
        public String MOOE { get; set; }
        public String TypeExpense { get; set; }
        public String UACS { get; set; }
        public String Description { get; set; }
        public String QTY { get; set; }
        public String EstimatedBudget { get; set; }
        public String ModeProcure { get; set; }

        public String Jan { get; set; }
        public String Feb { get; set; }
        public String Mar { get; set; }
        public String Q1 { get; set; }
        public String Apr { get; set; }
        public String May { get; set; }
        public String Jun { get; set; }
        public String Q2 { get; set; }
        public String Jul { get; set; }
        public String Aug { get; set; }
        public String Sep { get; set; }
        public String Q3 { get; set; }
        public String Oct { get; set; }
        public String Nov { get; set; }
        public String Dec { get; set; }
        public String Q4 { get; set; }
        public String Total { get; set; }
    }

   
}
