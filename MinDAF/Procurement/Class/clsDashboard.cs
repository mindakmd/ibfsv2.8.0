﻿using ReportTool.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Procurement.Class
{
    class clsDashboard
    {
        private clsData c_data = new clsData();
        public List<DashBoard> FetchDashboard(String DivId,String FundSource,String _Year,String FundType)
        {
            List<DashBoard> _data = new List<DashBoard>();

            StringBuilder varname1 = new StringBuilder();
            varname1.Append("SELECT \n");
            varname1.Append("mfs.Fund_Source_Id, \n");
            varname1.Append("mooe.uacs_code as UACS, \n");
            varname1.Append("mooe.name as ExpenseItem, \n");
            varname1.Append("0 as Quantity, \n");
            varname1.Append("0 as Served \n");
            varname1.Append("FROM mnda_fund_source_line_item mfs \n");
            varname1.Append("LEFT JOIN mnda_mooe_sub_expenditures mooe on mooe.uacs_code = mfs.mooe_id \n");
            varname1.Append("INNER JOIN mnda_respo_library mrl on mrl.code = mfs.Fund_Source_Id \n");
            varname1.Append("WHERE mrl.code = 'FS{bba8d05ce1ceae2f7f498eeb7298d50d2fb7e25bd9aced9cbc827daa0aaca91b}' AND \n");
            varname1.Append(" mrl.working_year = 2017 AND mrl.fund_type ='mf-1' \n");
            varname1.Append("ORDER BY mfs.id ASC");

            foreach (DataRow item in c_data.ExecuteSQLQuery(varname1.ToString()).Tables[0].Rows)
            {
                DashBoard _var = new DashBoard();

                _var.FundSourceID = item["Fund_Source_Id"].ToString();
                _var.UACS = item["UACS"].ToString();
                _var.ExpenseItem = item["ExpenseItem"].ToString();

                _data.Add(_var);
            }

            return _data;
        }
        public List<FundSourceData> FetchFundSources(String DivId)
        {
            List<FundSourceData> _data = new List<FundSourceData>();

            StringBuilder varname1 = new StringBuilder();
            varname1.Append("SELECT \n");
            varname1.Append("mrl.code as FundCode, \n");
            varname1.Append("mrl.respo_name as FundSource, \n");
            varname1.Append("mrl.fund_type as FundType \n");
            varname1.Append("FROM mnda_respo_library mrl \n");
            varname1.Append("WHERE mrl.div_id ='"+ DivId +"'  and mrl.working_year = "+ DateTime.Now.Year.ToString() +"");

            foreach (DataRow item in c_data.ExecuteSQLQuery(varname1.ToString()).Tables[0].Rows)
            {
                FundSourceData _var = new FundSourceData();

                _var.FundCode = item["FundCode"].ToString();
                _var.FundName = item["FundSource"].ToString();
                _var.FundType = item["FundType"].ToString();

                _data.Add(_var);
            }

            return _data;
        }
        public List<ExpenseDetails> FetchExpenseDetail(String FundCode,String _Year, String UACS)
        {

            List<String> _months = new List<String>();

            _months.Add("Jan");
            _months.Add("Feb");
            _months.Add("Mar");
            _months.Add("Apr");
            _months.Add("May");
            _months.Add("Jun");
            _months.Add("Jul");
            _months.Add("Aug");
            _months.Add("Sep");
            _months.Add("Oct");
            _months.Add("Nov");
            _months.Add("Dec");


            List<ExpenseDetails> _data = new List<ExpenseDetails>();
            List<ExpenseDetails> _sorted = new List<ExpenseDetails>();

            StringBuilder varname1 = new StringBuilder();
            varname1.Append("SELECT \n");
            varname1.Append("CONCAT(mad.remarks,CONCAT(' - ',mad.type_service)) as Information, \n");
            varname1.Append("mooe.name as ItemDetail, \n");
            varname1.Append("mad.month as Months, \n");
            varname1.Append("mad.quantity as Qty \n");
            varname1.Append("FROM mnda_activity_data mad \n");
            varname1.Append("INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id \n");
            varname1.Append("WHERE mad.fund_source_id = '"+  FundCode +"' \n");
            varname1.Append("AND mad.status = 'FINANCE APPROVED' AND mad.year ="+ _Year +" AND mooe.uacs_code = '"+UACS +"'\n");



            foreach (DataRow item in c_data.ExecuteSQLQuery(varname1.ToString()).Tables[0].Rows)
            {
                ExpenseDetails _var = new ExpenseDetails();

                _var.Months = item["Months"].ToString();
                _var.Information = item["Information"].ToString();
                _var.ItemDetail = item["ItemDetail"].ToString();
                _var.Qty = item["Qty"].ToString();
                _var.PR_Refference = "-";
                _var.RIS_Refference = "-";
                _var.Status = "No Transaction";
                _data.Add(_var);
            }

            foreach (String item in _months)
            {
                List<ExpenseDetails> _selected = _data.Where(x => x.Months == item.ToString()).ToList();

                if (_selected.Count!=0)
                {
                    foreach (ExpenseDetails itemd in _selected)
                    {
                        _sorted.Add(itemd);
                    }
                }

            }
            _data = _sorted;
          
            return _data;
        }
    }

    public class MonthList 
    {
        public String Months { get; set; }
    }

    public class DashBoard 
    {
        public String FundSourceID { get; set; }
        public String UACS { get; set; }
        public String ExpenseItem { get; set; }

    }
    public class FundSourceData 
    {
        public String FundCode { get; set; }
        public String FundName { get; set; }
        public String FundType { get; set; }
    }

    public class ExpenseDetails     
    {
        public String Months { get; set; }
        public String Information { get; set; }
        public String ItemDetail { get; set; }
        public String Qty { get; set; }
        public String RIS_Refference { get; set; }
        public String PR_Refference { get; set; }
        public String Status { get; set; }
    }
}
