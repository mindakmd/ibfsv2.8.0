﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ReportTool.Class
{
    class clsData
    {


        public Boolean ExecuteQueryProcurement(string _SQL)
        {
            SqlConnection conn = new SqlConnection();
            string ConnectionString = @"Server=LOCALHOST\SQLEXPRESS;Database=minda_procurement;User Id=sa;Password=minda1234";
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionString;
            con.Open();
            SqlCommand comm = new SqlCommand(_SQL, con);

            int result = comm.ExecuteNonQuery();
            con.Close();
            if (result == 1 || result == 2 || result == 3)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public DataSet ExecuteSQLQueryProcurement(String _sql)
        {
            String _ReturnData = "";

            SqlConnection conn = new SqlConnection();
            string ConnectionString = @"Server=LOCALHOST\SQLEXPRESS;Database=minda_procurement;User Id=sa;Password=minda1234";
            //string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance_dirty;User Id=sa;Password=12341234;"; 
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionString;
            SqlDataAdapter da = new SqlDataAdapter(_sql, con);
            DataSet ds = new DataSet();


            da.Fill(ds);




            return ds;
        }

        public Boolean ExecuteQuery(string _SQL)
        {
            SqlConnection conn = new SqlConnection();
            string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance;User Id=sa;Password=minda1234";
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionString;
            con.Open();
            SqlCommand comm = new SqlCommand(_SQL, con);

            int result = comm.ExecuteNonQuery();
            con.Close();
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public String GetNextCode(String _format, String _tablename)
        {
            String FinalCode = "";

            StringBuilder sb = new StringBuilder();

            sb.Append("SELECT SUM (row_count)\r\n");
            sb.Append("FROM sys.dm_db_partition_stats\r\n");
            sb.Append("WHERE object_id=OBJECT_ID('" + _tablename + "')   \r\n");
            sb.Append("AND (index_id=0 or index_id=1);\r\n");

            DataSet _ds = ExecuteSQLQueryProcurement(sb.ToString());

            if (_ds.Tables.Count != 0)
            {
                DataTable dt = _ds.Tables[0].Copy();
                if (dt.Rows.Count != 0)
                {
                    Int32 _nextno = Convert.ToInt32(dt.Rows[0][0].ToString());
                    _nextno += 1;
                    FinalCode = _format + "-" + DateTime.Now.Day.ToString() + DateTime.Now.Year.ToString() + "-00" + _nextno.ToString();
                }
                else
                {
                    FinalCode = "Unknown Code";
                }
            }
            return FinalCode;
        }
        public DataSet ExecuteSQLQuery(String _sql)
        {
            String _ReturnData = "";

            SqlConnection conn = new SqlConnection();
            string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance;User Id=sa;Password=minda1234";
            //string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance_dirty;User Id=sa;Password=12341234;"; 
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionString;
            SqlDataAdapter da = new SqlDataAdapter(_sql, con);
            DataSet ds = new DataSet();


            da.Fill(ds);




            return ds;
        }
        public DataSet ExecuteSQLQueryImport(String _sql)
        {
            String _ReturnData = "";

            SqlConnection conn = new SqlConnection();
            string ConnectionString = @"Server=EIBANEZ;Database=dvDb;User Id=mindafinance;Password=minda1234";
            //string ConnectionString = @"Server=MINDAFINANCE\SQLEXPRESS;Database=minda_finance_dirty;User Id=sa;Password=12341234;"; 
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConnectionString;
            SqlDataAdapter da = new SqlDataAdapter(_sql, con);
            DataSet ds = new DataSet();


            da.Fill(ds);




            return ds;
        }

        public String GetUniqueCode(String _module) 
        {
             return  _module + "-" + Guid.NewGuid().ToString("N");
        }
    }

}
