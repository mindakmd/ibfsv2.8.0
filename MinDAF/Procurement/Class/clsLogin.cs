﻿using ReportTool.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Procurement.Class
{
    class clsLogin
    {
        public String DivId { get; set; }
        public String DivCode { get; set; }
        public String Division { get; set; }
        public String FullName { get; set; }
        public String IsAdmin { get; set; }
        public String Office { get; set; }
        public String User { get; set; }
       

        public String PapCode { get; set; }
        private clsData c_data = new clsData();

        public Boolean VerifyUser(String _Username, String _Password) 
        {
            var sb = new System.Text.StringBuilder(281);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"div.Division_Id as DivId,");
            sb.AppendLine(@"div.Division_Code as DivCode,");
            sb.AppendLine(@"div.Division_Desc as Division,");
            sb.AppendLine(@"mua.User_Fullname,");
            sb.AppendLine(@"mua.is_admin,");
            sb.AppendLine(@"dso.PAP,");
            sb.AppendLine(@"dso.Description as _office,");
            sb.AppendLine(@"mua.User_Id as _user");
            sb.AppendLine(@"FROM mnda_user_accounts mua ");
            sb.AppendLine(@"INNER JOIN Division div on div.Division_Id = mua.Division_Id");
            sb.AppendLine(@"INNER JOIN DBM_Sub_Office dso on dso.PAP = div.Division_Code");
            sb.AppendLine(@"WHERE mua.Username ='"+ _Username +"' AND mua.Password ='"+ _Password +"'");

            DataTable dtData = c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Copy();

            if (dtData.Rows.Count!=0)
            {
                
                foreach (DataRow item in dtData.Rows)
                {
                    this.DivId = item["DivId"].ToString();
                    this.DivCode = item["DivCode"].ToString();
                    this.Division = item["Division"].ToString();
                    this.FullName = item["User_Fullname"].ToString();
                    this.IsAdmin = item["is_admin"].ToString();
                    this.Office = item["_office"].ToString();
                    this.PapCode = item["PAP"].ToString();
                    this.User = item["_user"].ToString();
                    break;
                    
                }
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    
}
