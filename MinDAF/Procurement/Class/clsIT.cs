﻿using ReportTool.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Procurement.Class
{
    class clsIT
    {
        private clsData c_data = new clsData();
        public List<ITMain> FetchItinerary(String DivId)
        {
            List<ITMain> _data = new List<ITMain>();

            var sb = new System.Text.StringBuilder(156);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"pim.date,");
            sb.AppendLine(@"pim.code,");
            sb.AppendLine(@"CONCAT(pim.name,' -	',pim.purpose_travel) as _details ,");
            sb.AppendLine(@"pim.user_created ");
            sb.AppendLine(@"FROM prc_it_main pim WHERE status ='added'");
            sb.AppendLine(@" AND pim.division_id = " + DivId + " ORDER BY pim.id DESC");

            foreach (DataRow item in c_data.ExecuteSQLQueryProcurement(sb.ToString()).Tables[0].Rows)
            {
                ITMain _var = new ITMain();

                _var.Code = item["code"].ToString();
                _var.Dates = Convert.ToDateTime(item["date"].ToString()).ToShortDateString();
                _var.Details = item["_details"].ToString();
                _var.UserCreated = item["user_created"].ToString();
                _data.Add(_var);
            }

            return _data;
        }
        public List<ITDetails> FetchItineraryDetails(String Code)
        {
            List<ITDetails> _data = new List<ITDetails>();

            var sb = new System.Text.StringBuilder(191);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"  code,");
            sb.AppendLine(@"  date,");
            sb.AppendLine(@"  places,");
            sb.AppendLine(@"  departure,");
            sb.AppendLine(@"  arrival,");
            sb.AppendLine(@"  means_transpo,");
            sb.AppendLine(@"  transpo_expense,");
            sb.AppendLine(@"  rate_hotel,");
            sb.AppendLine(@"  rate_meal,");
            sb.AppendLine(@"  rate_incidental");
            sb.AppendLine(@"FROM dbo.prc_it_details");
            sb.AppendLine(@"WHERE code = '"+ Code +"';");


            foreach (DataRow item in c_data.ExecuteSQLQueryProcurement(sb.ToString()).Tables[0].Rows)
            {
                ITDetails _var = new ITDetails();
                double _hotel = Convert.ToDouble(item["rate_hotel"].ToString());
                double _meal = Convert.ToDouble(item["rate_meal"].ToString());
                double _incidental = Convert.ToDouble(item["rate_incidental"].ToString());
                double _transpo_expense = Convert.ToDouble(item["transpo_expense"].ToString());

                _var.Dates = Convert.ToDateTime(item["date"].ToString()).ToShortDateString();
                _var.Hotel_Allowance = item["rate_hotel"].ToString();
                _var.Incidental = item["rate_incidental"].ToString();
                _var.Meal_Allowance = item["rate_meal"].ToString();
                _var.Means_Transportation = item["means_transpo"].ToString();
                _var.Places = item["places"].ToString();
                _var.Time_Arrive = item["arrival"].ToString();
                _var.Time_Depart = item["departure"].ToString();
                _var.Total_Amount = (_hotel + _meal + _incidental + _transpo_expense).ToString();
                _var.Transpo_Expenses = item["transpo_expense"].ToString();
                _data.Add(_var);
            }

            return _data;
        }

        private String FetchUsername(String _userid) 
        {
            var sb = new System.Text.StringBuilder(87);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mua.User_Fullname as _name");
            sb.AppendLine(@"FROM mnda_user_accounts mua");
            sb.AppendLine(@"WHERE mua.User_Id = "+ _userid +"");
            DataSet ds = c_data.ExecuteSQLQuery(sb.ToString());

            if (ds.Tables[0].Rows.Count!=0)
            {
                return ds.Tables[0].Rows[0]["_name"].ToString();
            }
            else
            {
                return "";
            }

        }

        public bool SaveITData(ITMain _Main ,List<ITDetails> _Details,String DivId,String Name,String UserId)
        {
            String _code = c_data.GetUniqueCode("IT");
            _Main.Code = _code;
            String username = FetchUsername(UserId);

            var sb = new System.Text.StringBuilder(161);
            sb.AppendLine(@"INSERT INTO   dbo.prc_it_main (code,division_id,name,purpose_travel,date,status,user_created) ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '" + _Main.Code + "',");
            sb.AppendLine(@"  '" + DivId + "',");
            sb.AppendLine(@"  '"+ Name +"',");
            sb.AppendLine(@"  '"+ _Main.Details +"',");
            sb.AppendLine(@"  '"+ Convert.ToDateTime(_Main.Dates) +"',");
            sb.AppendLine(@"  'added',");
            sb.AppendLine(@"  '" + username + "'");
            sb.AppendLine(@");");

            String _sql_details = "";
            foreach (ITDetails item in _Details)
            {
              _sql_details+= "INSERT INTO dbo.prc_it_details (code,date,places,departure,arrival,means_transpo,transpo_expense,rate_hotel,rate_meal,rate_incidental) " + Environment.NewLine +
                "VALUES (" + Environment.NewLine +
                "  '"+ _code +"'," + Environment.NewLine +
                "  '" + Convert.ToDateTime(item.Dates) + "'," + Environment.NewLine +
                "  '"+ item.Places +"'," + Environment.NewLine +
                "  '"+ item.Time_Depart +"'," + Environment.NewLine +
                "  '"+ item.Time_Arrive +"'," + Environment.NewLine +
                "  '"+ item.Means_Transportation +"'," + Environment.NewLine +
                "  '"+ item.Transpo_Expenses +"'," + Environment.NewLine +
                "  '"+ item.Hotel_Allowance +"'," + Environment.NewLine +
                "  '"+ item.Meal_Allowance +"'," + Environment.NewLine +
                "  '"+ item.Incidental +"'" + Environment.NewLine +
                ");";
            }
            string _sql = sb.ToString() + _sql_details;

            bool _state = c_data.ExecuteQueryProcurement(_sql);

            return _state;

        }
        public bool DeleteITData(String _Code)
        {
            var sb = new System.Text.StringBuilder(74);
            sb.AppendLine(@"UPDATE ");
            sb.AppendLine(@"  dbo.prc_it_main  ");
            sb.AppendLine(@"SET ");
            sb.AppendLine(@"  status = 'deleted'");
            sb.AppendLine(@"WHERE code = '"+ _Code +"';");


            bool _state = c_data.ExecuteQueryProcurement(sb.ToString());

            return _state;

        }
    }

    public class ITMain 
    {
        public String Dates { get; set; }
        public String Code { get; set; }
        public String Details { get; set; }
        public String UserCreated { get; set; }
    }
   public class ITDetails
   {
       public String Dates { get; set; }
       public String Places { get; set;  }
       public String Time_Depart { get; set; }
       public String Time_Arrive { get; set; }
       public String Means_Transportation { get; set; }
       public String Transpo_Expenses { get; set; }
       public String Hotel_Allowance { get; set; }
       public String Meal_Allowance { get; set; }
       public String Incidental { get; set; }
       public String Total_Amount { get; set; }
   }
}
