﻿using ReportTool.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Procurement.Class
{
    class clsPR
    {
        private clsData c_data = new clsData();
        public List<ProgramList> FetchPrograms(String DivId, String _Year)
        {
            List<ProgramList> _data = new List<ProgramList>();

            var sb = new System.Text.StringBuilder(207);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mmpe.id,");
            sb.AppendLine(@"mp.name");
            sb.AppendLine(@"FROM mnda_main_program_encoded mmpe");
            sb.AppendLine(@"INNER JOIN mnda_programs mp on mp.id = mmpe.program_code");
            sb.AppendLine(@"WHERE mmpe.accountable_division =" + DivId + "  AND mmpe.fiscal_year = " + _Year + "");
            sb.AppendLine(@"GROUP BY mmpe.id,mp.name");

            foreach (DataRow item in c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Rows)
            {
                ProgramList _var = new ProgramList();

                _var.Id = item["id"].ToString();
                _var.Name = item["name"].ToString();
                _data.Add(_var);
            }

            return _data;
        }
        public List<ProjectList> FetchProjects(String DivId, String _Year, String _Id)
        {
            List<ProjectList> _data = new List<ProjectList>();

            var sb = new System.Text.StringBuilder(160);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mpe.id,");
            sb.AppendLine(@"mpe.project_name");
            sb.AppendLine(@"FROM mnda_program_encoded mpe ");
            sb.AppendLine(@"WHERE mpe.main_program_id ='" + _Id + "' AND mpe.acountable_division_code ='" + DivId + "' ");
            sb.AppendLine(@"AND mpe.project_year ='" + _Year + "'");


            foreach (DataRow item in c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Rows)
            {
                ProjectList _var = new ProjectList();

                _var.Id = item["id"].ToString();
                _var.Name = item["project_name"].ToString();
                _data.Add(_var);
            }

            return _data;
        }
        public List<OutputList> FetchOutput(String DivId, String _Year, String _Id)
        {
            List<OutputList> _data = new List<OutputList>();

            var sb = new System.Text.StringBuilder(224);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mpo.id,");
            sb.AppendLine(@"mpo.name as output");
            sb.AppendLine(@"FROM mnda_program_encoded mpe");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@"WHERE mpo.program_code ='" + _Id + "' AND mpe.acountable_division_code = '" + DivId + "'");
            sb.AppendLine(@"and mpe.project_year = '" + _Year + "'");



            foreach (DataRow item in c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Rows)
            {
                OutputList _var = new OutputList();

                _var.Id = item["id"].ToString();
                _var.Name = item["output"].ToString();
                _data.Add(_var);
            }

            return _data;
        }
        public List<ActivityList> FetchActivity(String DivId, String _Year, String _Id)
        {
            List<ActivityList> _data = new List<ActivityList>();

            var sb = new System.Text.StringBuilder(273);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"ma.id,");
            sb.AppendLine(@"ma.description");
            sb.AppendLine(@"FROM mnda_program_encoded mpe");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.output_id = mpo.id");
            sb.AppendLine(@"WHERE ma.output_id ='" + _Id + "' AND mpe.acountable_division_code = '" + DivId + "'");
            sb.AppendLine(@"and mpe.project_year = '" + _Year + "'");


            foreach (DataRow item in c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Rows)
            {
                ActivityList _var = new ActivityList();

                _var.Id = item["id"].ToString();
                _var.Name = item["description"].ToString();
                _data.Add(_var);
            }

            return _data;
        }public List<ExpenseItems> FetchExpenseItems(String DivId, String _Year, String _Id)
        {
            List<ExpenseItems> _data = new List<ExpenseItems>();

            var sb = new System.Text.StringBuilder(643);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mad.id,");
            sb.AppendLine(@"mad.month,");
            sb.AppendLine(@"mad.type_service,");
            sb.AppendLine(@"mooe.name,");
            sb.AppendLine(@"mad.quantity,");
            sb.AppendLine(@"mad.total,");
            sb.AppendLine(@"mad.rate");
            sb.AppendLine(@",(mad.travel_allowance * CAST(mad.no_days as int)) as allowance,");
            sb.AppendLine(@"mad.no_days");
            sb.AppendLine(@"FROM mnda_program_encoded mpe");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.output_id = mpo.id");
            sb.AppendLine(@"INNER JOIN mnda_activity_data  mad on mad.activity_id = ma.id");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"WHERE mad.activity_id ='" + _Id + "' AND mpe.acountable_division_code = '" + DivId + "'");
            sb.AppendLine(@"and mpe.project_year = '" + _Year + "' AND NOT mooe.id = 4 AND NOT mooe.id = 82 AND NOT mooe.id=81");
            sb.AppendLine(@"AND mad.status ='FINANCE APPROVED'");
            sb.AppendLine(@"GROUP BY ");
            sb.AppendLine(@"mad.id,mad.month,");
            sb.AppendLine(@"mad.type_service,mooe.name,");
            sb.AppendLine(@"mad.quantity,mad.total,mad.rate,mad.travel_allowance ,mad.no_days");



            foreach (DataRow item in c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Rows)
            {
                ExpenseItems _var = new ExpenseItems();

                if (item["type_service"].ToString().Trim() == "")
                {
                    _var.Description = item["name"].ToString();
                }
                else
                {
                    _var.Description = item["type_service"].ToString();
                }
                _var.Expense_Type = item["name"].ToString();
                _var.Months = item["month"].ToString();
                _var.Quantity = item["quantity"].ToString();
                _var.UnitCost = item["rate"].ToString();
                _var.NoDays = item["no_days"].ToString();
                _data.Add(_var);
            }

            return _data;
        }
        public List<ExpenseTypes> FetchExpenseTypes(String DivId, String _Year, String _Id)
        {
            List<ExpenseTypes> _data = new List<ExpenseTypes>();

            var sb = new System.Text.StringBuilder(551);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mooe.name as expense_type");
            sb.AppendLine(@"FROM mnda_program_encoded mpe");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.output_id = mpo.id");
            sb.AppendLine(@"INNER JOIN mnda_activity_data  mad on mad.activity_id = ma.id");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"WHERE mad.activity_id ='"+ _Id +"' AND mpe.acountable_division_code = '"+ DivId +"'");
            sb.AppendLine(@"and mpe.project_year = '"+ _Year +"' AND NOT mooe.id = 4 AND NOT mooe.id = 82 AND NOT mooe.id=81");
            sb.AppendLine(@"AND mad.status ='FINANCE APPROVED'");
            sb.AppendLine(@"GROUP BY ");
            sb.AppendLine(@"mooe.name");



            foreach (DataRow item in c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Rows)
            {
                ExpenseTypes _var = new ExpenseTypes();



                _var.Name = item["expense_type"].ToString();
                _data.Add(_var);
            }

            return _data;
        }
        public List<ExpenseMonths> FetchExpenseMonths(String DivId, String _Year, String _Id)
        {
            List<ExpenseMonths> _data = new List<ExpenseMonths>();

            var sb = new System.Text.StringBuilder(551);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mad.month");
            sb.AppendLine(@"FROM mnda_program_encoded mpe");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.output_id = mpo.id");
            sb.AppendLine(@"INNER JOIN mnda_activity_data  mad on mad.activity_id = ma.id");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"WHERE mad.activity_id ='" + _Id + "' AND mpe.acountable_division_code = '" + DivId + "'");
            sb.AppendLine(@"and mpe.project_year = '" + _Year + "' AND NOT mooe.id = 4 AND NOT mooe.id = 82 AND NOT mooe.id=81");
            sb.AppendLine(@"AND mad.status ='FINANCE APPROVED'");



            foreach (DataRow item in c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Rows)
            {
                ExpenseMonths _var = new ExpenseMonths();



                _var.Name = item["month"].ToString();
                _data.Add(_var);
            }

            return _data;
        }
    }
}
