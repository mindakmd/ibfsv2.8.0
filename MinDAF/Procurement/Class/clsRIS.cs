﻿using ReportTool.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Procurement.Class
{
    class clsRIS
    {
        private clsData c_data = new clsData();
        public List<ProgramList> FetchPrograms(String DivId,String _Year)
        {
            List<ProgramList> _data = new List<ProgramList>();

            var sb = new System.Text.StringBuilder(207);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mmpe.id,");
            sb.AppendLine(@"mp.name");
            sb.AppendLine(@"FROM mnda_main_program_encoded mmpe");
            sb.AppendLine(@"INNER JOIN mnda_programs mp on mp.id = mmpe.program_code");
            sb.AppendLine(@"WHERE mmpe.accountable_division =" + DivId + "  AND mmpe.fiscal_year = " + _Year + "");
            sb.AppendLine(@"GROUP BY mmpe.id,mp.name");

            foreach (DataRow item in c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Rows)
            {
                ProgramList _var = new ProgramList();

                _var.Id = item["id"].ToString();
                _var.Name = item["name"].ToString();
                _data.Add(_var);
            }

            return _data;
        }


        public List<ProjectList> FetchProjects(String DivId, String _Year,String _Id)
        {
            List<ProjectList> _data = new List<ProjectList>();

            var sb = new System.Text.StringBuilder(160);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mpe.id,");
            sb.AppendLine(@"mpe.project_name");
            sb.AppendLine(@"FROM mnda_program_encoded mpe ");
            sb.AppendLine(@"WHERE mpe.main_program_id ='" + _Id + "' AND mpe.acountable_division_code ='" + DivId + "' ");
            sb.AppendLine(@"AND mpe.project_year ='" + _Year + "'");
            sb.AppendLine(@"GROUP BY mpe.id,mpe.project_name");

            foreach (DataRow item in c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Rows)
            {
                ProjectList _var = new ProjectList();

                _var.Id = item["id"].ToString();
                _var.Name = item["project_name"].ToString();
                _data.Add(_var);
            }

            return _data;
        }
        public List<OutputList> FetchOutput(String DivId, String _Year, String _Id)
        {
            List<OutputList> _data = new List<OutputList>();

            var sb = new System.Text.StringBuilder(224);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mpo.id,");
            sb.AppendLine(@"mpo.name as output");
            sb.AppendLine(@"FROM mnda_program_encoded mpe");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@"WHERE mpo.program_code ='" + _Id + "' AND mpe.acountable_division_code = '" + DivId + "'");
            sb.AppendLine(@"and mpe.project_year = '" + _Year + "'");
            sb.AppendLine(@";");


            foreach (DataRow item in c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Rows)
            {
                OutputList _var = new OutputList();

                _var.Id = item["id"].ToString();
                _var.Name = item["output"].ToString();
                _data.Add(_var);
            }

            return _data;
        }
        public List<ActivityList> FetchActivity(String DivId, String _Year, String _Id)
        {
            List<ActivityList> _data = new List<ActivityList>();

            var sb = new System.Text.StringBuilder(273);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"ma.id,");
            sb.AppendLine(@"ma.description");
            sb.AppendLine(@"FROM mnda_program_encoded mpe");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.output_id = mpo.id");
            sb.AppendLine(@"WHERE ma.output_id ='" + _Id + "' AND mpe.acountable_division_code = '" + DivId + "'");
            sb.AppendLine(@"and mpe.project_year = '"+ _Year +"'");


            foreach (DataRow item in c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Rows)
            {
                ActivityList _var = new ActivityList();

                _var.Id = item["id"].ToString();
                _var.Name = item["description"].ToString();
                _data.Add(_var);
            }

            return _data;
        }
        public bool SaveRISData(String _ref_no,String _stock_no,String _unit,String _description,String _quantity,String _status,String _months,String _purpose,String _division,String _user)
        {
            //if (_description.Contains(","))
            //{
            //    _description = _description.Replace(",", "\',' ESCAPE");
            //}
            String Code = c_data.GetNextCode("PRS", "prc_request_ris");
            var sb = new System.Text.StringBuilder(310);
            sb.AppendLine(@"INSERT INTO ");
            sb.AppendLine(@"  dbo.prc_request_ris");
            sb.AppendLine(@"(");
            sb.AppendLine(@"  code,");
            sb.AppendLine(@"  ref_no,");
            sb.AppendLine(@"  stock_no,");
            sb.AppendLine(@"  unit,");
            sb.AppendLine(@"  description,");
            sb.AppendLine(@"  quantity,");
            sb.AppendLine(@"  status,");
            sb.AppendLine(@"  months,");
            sb.AppendLine(@"  purpose,");
            sb.AppendLine(@"  remarks,");
            sb.AppendLine(@"  division_id,");
            sb.AppendLine(@"  _user");
            sb.AppendLine(@") ");
            sb.AppendLine(@"VALUES (");
            sb.AppendLine(@"  '" + Code + "',");
            sb.AppendLine(@"  '" + _ref_no + "',");
            sb.AppendLine(@"  '" + _stock_no + "',");
            sb.AppendLine(@"  '"+ _unit +"',");
            sb.AppendLine(@"  '"+ _description +"',");
            sb.AppendLine(@"  '"+ _quantity +"',");
            sb.AppendLine(@"  'Submitted',");
            sb.AppendLine(@"  '" + _months + "',");
            sb.AppendLine(@"  '"+ _purpose +"',");
            sb.AppendLine(@"  '-',");
            sb.AppendLine(@"  '"+ _division +"',");
            sb.AppendLine(@"  '" + _user+ "'");
            sb.AppendLine(@");");

            bool _state = c_data.ExecuteQueryProcurement(sb.ToString());

            StringBuilder varname1 = new StringBuilder();
            varname1.Append("INSERT INTO \n");
            varname1.Append("  dbo.prc_approvals \n");
            varname1.Append("( \n");
            varname1.Append("  refference_code, \n");
            varname1.Append("  approval_type, \n");
            varname1.Append("  approver_one_status, \n");
            varname1.Append("  approver_two_status, \n");
            varname1.Append("  div_id \n");
            varname1.Append(") \n");
            varname1.Append("VALUES ( \n");
            varname1.Append("  '"+ Code +"', \n");
            varname1.Append("  'RIS', \n");
            varname1.Append("  0, \n");
            varname1.Append("  0, \n");
            varname1.Append("  '" + _division + "' \n");
            varname1.Append(");");

            c_data.ExecuteQueryProcurement(varname1.ToString());


            return _state;

        }
        public List<ActualOBR> FetchOBRData(String _year, String _fundsource, String _month, String UACS)
        {
            List<ActualOBR> _data = new List<ActualOBR>();

            var sb = new System.Text.StringBuilder(629);
            string _backmonth = _month;
            switch (_month)
            {
                case "Jan": _month = "1"; break;
                case "Feb": _month = "2"; break;
                case "Mar": _month = "3"; break;
                case "Apr": _month = "4"; break;
                case "May": _month = "5"; break;
                case "Jun": _month = "6"; break;
                case "Jul": _month = "7"; break;
                case "Aug": _month = "8"; break;
                case "Sep": _month = "9"; break;
                case "Oct": _month = "10"; break;
                case "Nov": _month = "11"; break;
                case "Dec": _month = "12"; break;
            }
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"lib_a.AccountCode,");
            sb.AppendLine(@"lib_res.Division,");
            sb.AppendLine(@"MONTH(obr.DateCreated) as _Month,");
            sb.AppendLine(@"obr.DateCreated,");
            sb.AppendLine(@"SUM(obr_d.Amount) as Total");
            sb.AppendLine(@"FROM or_ObligationRequest obr");
            sb.AppendLine(@"INNER JOIN or_ObligationRequestParticular obr_d on obr.ID = obr_d.ObligationRequestID");
            sb.AppendLine(@"INNER JOIN  lib_ResponsibilityCenter lib_res on lib_res.ID = obr_d.ResponsibilityCenterID");
            sb.AppendLine(@"INNER JOIN lib_Account lib_a on lib_a.ID =obr_d.AcctID");
            sb.AppendLine(@"WHERE  YEAR(obr.DateCreated)= " + _year + " and lib_res.Division ='" + _fundsource + "' and CHARINDEX('502', lib_a.AccountCode)>0");
            sb.AppendLine(@"Group By obr_d.AcctID,lib_res.Division,MONTH(obr.DateCreated),obr.DateCreated,lib_a.AccountCode");
            sb.AppendLine(@"ORDER BY obr.DateCreated");

            foreach (DataRow item in c_data.ExecuteSQLQueryImport(sb.ToString()).Tables[0].Rows)
            {
                ActualOBR f_data = new ActualOBR();

                if ( item["AccountCode"].ToString() ==UACS )
                {

                    f_data._Month = Convert.ToInt32(item["_Month"].ToString());
                    f_data.AccountCode = item["AccountCode"].ToString();
                    f_data.DateCreated = item["DateCreated"].ToString();
                    f_data.Division = item["Division"].ToString();
                    f_data.Total = item["Total"].ToString();

                    _data.Add(f_data);
                }

            }


            return _data;
        }

     

        public List<FundSourceList> FetchFundSource(String DivId,String _Year)
        {
            List<FundSourceList> _data = new List<FundSourceList>();

            var sb = new System.Text.StringBuilder(85);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"mfs.Fund_Name as fund,");
            sb.AppendLine(@"mfs.code as code,");
            sb.AppendLine(@"mrl.fund_type");
            sb.AppendLine(@"FROM mnda_fund_source mfs");
            sb.AppendLine(@"INNER JOIN mnda_respo_library mrl on mrl.code = mfs.code");
            sb.AppendLine(@"WHERE mfs.division_id = '" + DivId + "' AND mfs.Year =" + _Year + "");



            foreach (DataRow item in c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Rows)
            {
                FundSourceList f_data = new FundSourceList();


                f_data.FundSource = item["fund"].ToString();
                f_data.FundCode = item["code"].ToString();
                f_data.FundType = item["fund_type"].ToString();
                _data.Add(f_data);
            }

            return _data;
        }

        public List<SubmittedRIS> FetchSubmittedRIS(String DivId)
        {
            List<SubmittedRIS> _data = new List<SubmittedRIS>();

            var sb = new System.Text.StringBuilder(100);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"ris.ref_no");
            sb.AppendLine(@"FROM prc_request_ris ris");
            sb.AppendLine(@"WHERE ris.division_id = '"+ DivId +"' AND ris.status ='Submitted'");



            foreach (DataRow item in c_data.ExecuteSQLQueryProcurement(sb.ToString()).Tables[0].Rows)
            {
                SubmittedRIS f_data = new SubmittedRIS();


                f_data.RefNo = item["ref_no"].ToString();

                _data.Add(f_data);
            }

            return _data;
        }
        public List<ExpenseMonths> FetchActualMonths(String FundCode,String UACS)         
                {

                    List<ExpenseMonths> _data = new List<ExpenseMonths>();

                    StringBuilder varname1 = new StringBuilder();
                    varname1.Append("SELECT \n");
                    varname1.Append("mad.month \n");
                    varname1.Append("FROM mnda_activity_data mad \n");
                    varname1.Append("INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id \n");
                    varname1.Append("WHERE mad.fund_source_id ='"+ FundCode +"' \n");
                    varname1.Append("AND mooe.uacs_code = '" + UACS + "' AND mad.status ='FINANCE APPROVED' \n");
                    varname1.Append("GROUP BY mad.month");

                    foreach (DataRow item in c_data.ExecuteSQLQuery(varname1.ToString()).Tables[0].Rows)
                    {
                        ExpenseMonths f_data = new ExpenseMonths();

                        f_data.Num = ReturnMonthValue(item["month"].ToString());
                        f_data.Name = item["month"].ToString();


                        _data.Add(f_data);
                    }

                    return _data;

                }
        public Int32 ReturnMonthValue(String _month) 
        {
            Int32 _monthval = 0;

            switch (_month)
	        {
                case "Jan": _monthval =1 ; break;
                case "Feb": _monthval = 2; break;
                case "Mar": _monthval = 3; break;
                case "Apr": _monthval =4 ; break;
                case "May": _monthval =5 ; break;
                case "Jun": _monthval =6 ; break;
                case "Jul": _monthval =7 ; break;
                case "Aug": _monthval =8 ; break;
                case "Sep": _monthval =9 ; break;
                case "Oct": _monthval =10 ; break;
                case "Nov": _monthval =11 ; break;
                case "Dec": _monthval =12 ; break;
	        }

            return _monthval;
        }

        public List<RISDataList> FetchRIS(String DivId)
        {

            List<RISDataList> _data = new List<RISDataList>();

            StringBuilder varname1 = new StringBuilder();
            varname1.Append("SELECT \n");
            varname1.Append("ris._user as code, \n");
            varname1.Append("ris.division_id as div_code, \n");
            varname1.Append("CONVERT(VARCHAR(10), ris.entry_date, 110) AS EntryDate, \n");
            varname1.Append("ris.months as Qtr, \n");
            varname1.Append("ris.purpose Purpose, \n");
            varname1.Append("ris.status as Status, \n");
            varname1.Append("ris._user as UserCode \n");
            varname1.Append("FROM prc_request_ris ris \n");
            varname1.Append("WHERE ris.division_id = '"+  DivId+"' AND ris.status = 'Submitted' \n");
            varname1.Append("GROUP BY ris._user, \n");
            varname1.Append("ris.division_id,CONVERT(VARCHAR(10), ris.entry_date, 110),ris.months,ris.purpose, \n");
            varname1.Append("ris.status,ris._user "); 


            foreach (DataRow item in c_data.ExecuteSQLQueryProcurement(varname1.ToString()).Tables[0].Rows)
            {
                RISDataList f_data = new RISDataList();

                f_data.Code = item["code"].ToString();
                f_data.DivCode = item["div_code"].ToString();
                f_data.EntryDate = Convert.ToDateTime(item["EntryDate"].ToString()).ToShortDateString();
                f_data.Qtr = item["Qtr"].ToString();
                f_data.Purpose = item["Purpose"].ToString();
                f_data.Status = item["Status"].ToString();
                f_data.UserCode = item["UserCode"].ToString();

                _data.Add(f_data);
            }

            return _data;

        }

        public List<RISDataListDetails> FetchRISDetails(String DivId,String Purpose)
        {

            List<RISDataListDetails> _data = new List<RISDataListDetails>();

            StringBuilder varname1 = new StringBuilder();
            varname1.Append("SELECT \n");
            varname1.Append("ris.description as Description, \n");
            varname1.Append("ris.quantity as Quantity, \n");
            varname1.Append("ris.stock_no as StockNo, \n");
            varname1.Append("ris.unit as Unit, \n");
            varname1.Append("ris.status as Status, \n");
            varname1.Append("ris._user as UserCode \n");
            varname1.Append("FROM prc_request_ris ris \n");
            varname1.Append("WHERE ris.division_id = '" + DivId + "' AND ris.status = 'Submitted' AND ris.purpose = '" + Purpose + "' \n");

            foreach (DataRow item in c_data.ExecuteSQLQueryProcurement(varname1.ToString()).Tables[0].Rows)
            {
                RISDataListDetails f_data = new RISDataListDetails();

                f_data.Description = item["Description"].ToString();
                f_data.Quantity = item["Quantity"].ToString();
                f_data.StockNo = item["StockNo"].ToString();
                f_data.Unit = item["Unit"].ToString();
     

                _data.Add(f_data);
            }

            return _data;

        }


        public List<DivisionSignatory> FetchDivisionSignatory(String DivId)
        {

            List<DivisionSignatory> _data = new List<DivisionSignatory>();

            StringBuilder varname1 = new StringBuilder();
            varname1.Append("SELECT \n");
            varname1.Append("mua.signatory_name, \n");
            varname1.Append("mua.signatory_position \n");
            varname1.Append("FROM mnda_user_accounts mua \n");
            varname1.Append("WHERE mua.Division_Id = "+ DivId +" AND mua.is_admin = 2");

            foreach (DataRow item in c_data.ExecuteSQLQuery(varname1.ToString()).Tables[0].Rows)
            {
                DivisionSignatory f_data = new DivisionSignatory();

                f_data.Name = item["signatory_name"].ToString();
                f_data.Position = item["signatory_position"].ToString();

                _data.Add(f_data);
            }

            return _data;

        }

        public List<PreparedSignatory> FetchPreparedSignatory(String DivId,String UserId)
        {

            List<PreparedSignatory> _data = new List<PreparedSignatory>();

            StringBuilder varname1 = new StringBuilder();
            varname1.Append("SELECT \n");
            varname1.Append("mua.signatory_name, \n");
            varname1.Append("mua.signatory_position \n");
            varname1.Append("FROM mnda_user_accounts mua \n");
            varname1.Append("WHERE mua.Division_Id = " + DivId + " AND mua.is_admin = 2 AND mua.User_Id ="+ UserId +"");

            foreach (DataRow item in c_data.ExecuteSQLQuery(varname1.ToString()).Tables[0].Rows)
            {
                PreparedSignatory f_data = new PreparedSignatory();

                f_data.Name = item["signatory_name"].ToString();
                f_data.Position = item["signatory_position"].ToString();

                _data.Add(f_data);
            }

            return _data;

        }

        public List<DivisionData> FetchOfficeData(String DivId)
        {

            List<DivisionData> _data = new List<DivisionData>();

            StringBuilder varname1 = new StringBuilder();
            varname1.Append("SELECT \n");
            varname1.Append("dpap.DBM_Sub_Pap_Desc as Office, \n");
            varname1.Append("div.Division_Desc as DivisionName \n");
            varname1.Append("FROM Division div \n");
            varname1.Append("INNER JOIN DBM_Sub_Office dsub on dsub.PAP = div.Division_Code \n");
            varname1.Append("INNER JOIN DBM_Sub_Pap dpap on dpap.DBM_Sub_Pap_id = dsub.DBM_Sub_Id \n");
            varname1.Append("WHERE div.Division_Id = "+ DivId +"");

            foreach (DataRow item in c_data.ExecuteSQLQuery(varname1.ToString()).Tables[0].Rows)
            {
                DivisionData f_data = new DivisionData();

                f_data.Office = item["Office"].ToString();
                f_data.Division = item["DivisionName"].ToString();

                _data.Add(f_data);
            }

            return _data;

        }

        public List<RISList> FetchSubRISData(String DivId)         
        {
           
            List<RISList> _data = new List<RISList>();

            var sb = new System.Text.StringBuilder(158);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"ris.entry_date,");
            sb.AppendLine(@"ris.months,");
            sb.AppendLine(@"ris.stock_no,");
            sb.AppendLine(@"ris.unit,");
            sb.AppendLine(@"ris.description,");
            sb.AppendLine(@"ris.purpose,");
            sb.AppendLine(@"ris.remarks,");
            sb.AppendLine(@"ris.status");
            sb.AppendLine(@"FROM prc_request_ris ris");
            sb.AppendLine(@"WHERE ris.division_id = '"+ DivId +"'");


            foreach (DataRow item in c_data.ExecuteSQLQueryProcurement(sb.ToString()).Tables[0].Rows)
            {
                RISList f_data = new RISList();

                f_data.date_submitted = Convert.ToDateTime(item["entry_date"].ToString()).ToShortDateString();
                f_data.months = item["months"].ToString();
                f_data.stock_no = item["stock_no"].ToString();
                f_data.unit = item["unit"].ToString();
                f_data.description = item["description"].ToString();
                f_data.purpose = item["purpose"].ToString();
                f_data.remarks = item["remarks"].ToString();
                f_data.status = item["status"].ToString();

                _data.Add(f_data);
            }

            return _data;

        }

        public double FetchAllocation(String DivId, String MOOE,String _Year,String FundCode,String FundType)
        {
          //  List<ActivityList> _data = new List<ActivityList>();

            var sb = new System.Text.StringBuilder(110);
            sb.AppendLine(@"SELECT ");
            sb.AppendLine(@"mfsl.Amount");
            sb.AppendLine(@"FROM mnda_fund_source_line_item mfsl");
            sb.AppendLine(@"WHERE mfsl.division_id ='"+ DivId +"' AND mfsl.mooe_id = '"+ MOOE +"' ANd mfsl.Fund_Source_Id ='"+ FundCode +"' AND Year ="+ _Year +"");

            double _amount = 0.00;

            foreach (DataRow item in c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Rows)
            {

                _amount = Convert.ToDouble(item["Amount"].ToString());
                break;
            }

            StringBuilder varname1 = new StringBuilder();
            varname1.Append("SELECT \n");
            varname1.Append("mrn.number \n");
            varname1.Append("FROM mnda_raf_no mrn \n");
            varname1.Append("WHERE mrn.div_id = '" + DivId + "' AND mrn.status ='Served' AND mrn.year =" + _Year + " AND mrn.mf_type ='"+ FundType +"'\n");
            varname1.Append("ORDER BY mrn.number ASC");
          
            DataTable dtData = c_data.ExecuteSQLQuery(varname1.ToString()).Tables[0].Copy();

            foreach (DataRow item in dtData.Rows)
            {
                StringBuilder  varname = new StringBuilder();
                varname.Append("SELECT \n");
                varname.Append("mfa.mooe_id, \n");
                varname.Append("mfa.plusminus, \n");
                varname.Append("mfa.adjustment \n");
                varname.Append("FROM mnda_fund_source_adjustment mfa \n");
                varname.Append("WHERE mfa.raf_no ='" + item["number"].ToString() + "' AND mfa.mooe_id ='"+ MOOE +"';");

                DataTable dtResults = c_data.ExecuteSQLQuery(varname.ToString()).Tables[0].Copy();

                foreach (DataRow item_r in dtResults.Rows)
                {
                    switch (item_r["plusminus"].ToString())
                    {
                        case "+":
                            _amount += Convert.ToDouble(item_r["adjustment"].ToString());
                            break;
                        case "-":
                            _amount -= Convert.ToDouble(item_r["adjustment"].ToString());
                            break;
                      
                    }   
                }
            }

          


            return _amount;
        }
        public List<RISFundLibrary> FetchFundLibrary(String FundSource,String _Year)
                {
                    List<RISFundLibrary> _data = new List<RISFundLibrary>();

                    StringBuilder varname1 = new StringBuilder();
                    varname1.Append("SELECT \n");
                    varname1.Append("mfsl.mooe_id, \n");
                    varname1.Append("mooe.name \n");
                    varname1.Append("FROM mnda_fund_source_line_item mfsl \n");
                    varname1.Append("INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.uacs_code = mfsl.mooe_id \n");
                    varname1.Append("WHERE mfsl.Fund_Source_Id = '"+ FundSource +"' \n");
                    varname1.Append("AND mfsl.Year = " + _Year + "  AND mfsl.is_revision =0 \n");
                    varname1.Append("UNION \n");
                    varname1.Append("SELECT \n");
                    varname1.Append("mfsl.mooe_id, \n");
                    varname1.Append("mooe.name \n");
                    varname1.Append("FROM mnda_fund_source_line_item mfsl \n");
                    varname1.Append("INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.uacs_code = mfsl.mooe_id \n");
                    varname1.Append("WHERE mfsl.Fund_Source_Id = '" + FundSource + "' \n");
                    varname1.Append("AND mfsl.Year = " + _Year + "  AND mfsl.is_revision =1 \n");
                    varname1.Append("GROUP BY mfsl.mooe_id, \n");
                    varname1.Append("mooe.name");


                    foreach (DataRow item in c_data.ExecuteSQLQuery(varname1.ToString()).Tables[0].Rows)
                    {
                        RISFundLibrary _var = new RISFundLibrary();

                        if (item["mooe_id"].ToString() == "5020301000")
                        {
                            _var.MOOE_ID = item["mooe_id"].ToString();
                            _var.ExpenseItem = item["name"].ToString();
                            _data.Add(_var);
                        }
                        else if (item["mooe_id"].ToString() == "50203011002")
                        {
                            _var.MOOE_ID = item["mooe_id"].ToString();
                            _var.ExpenseItem = item["name"].ToString();
                            _data.Add(_var);
                        }
                        else if (item["mooe_id"].ToString() == "5020399000")
                        {
                            _var.MOOE_ID = item["mooe_id"].ToString();
                            _var.ExpenseItem = item["name"].ToString();
                            _data.Add(_var);
                        }

                     
                      
                    }

                    return _data;
                }
        public List<ExpenseItems> FetchExpenseItems(String DivId, String _Year, String _Id)
        {
            List<ExpenseItems> _data = new List<ExpenseItems>();

            var sb = new System.Text.StringBuilder(643);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"ISNULL(mpi.expenditure_id,'') as expenditure_id,");
            sb.AppendLine(@"mad.id,");
            sb.AppendLine(@"mad.month,");
            sb.AppendLine(@"mad.type_service,");
            sb.AppendLine(@"mooe.name,");
            sb.AppendLine(@"mad.quantity,");
            sb.AppendLine(@"mad.total");
            sb.AppendLine(@"FROM mnda_program_encoded mpe");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.output_id = mpo.id");
            sb.AppendLine(@"INNER JOIN mnda_activity_data  mad on mad.activity_id = ma.id");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_procurement_items mpi on mpi.item_specifications = mad.type_service");
            sb.AppendLine(@"WHERE mad.activity_id ='"+ _Id +"' AND mpe.acountable_division_code = '"+ _Id +"'");
            sb.AppendLine(@"and mpe.project_year = '" + _Year + "' AND mooe.id = 4 OR mooe.id = 82 OR mooe.id=81 OR mooe.id=1 OR mooe.id=2");
            sb.AppendLine(@"AND mad.status ='FINANCE APPROVED'");
            sb.AppendLine(@"GROUP BY ");
            sb.AppendLine(@"mpi.expenditure_id,mad.id,mad.month,");
            sb.AppendLine(@"mad.type_service,mooe.name,");
            sb.AppendLine(@"mad.quantity,mad.total");



            foreach (DataRow item in c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Rows)
            {
                ExpenseItems _var = new ExpenseItems();

                if (item["type_service"].ToString().Trim()=="")
                {
                    _var.Description = item["name"].ToString();
                }
                else if (item["name"].ToString()=="Local Travel")
                {
                    _var.Description = item["type_service"].ToString();
                }
                else
                {
                    _var.Description = item["type_service"].ToString();
                }
                if (item["name"].ToString()=="Local Travel")
                {
                    _var.Expense_Type = "Local Travel";
                }
                else
                {
                    _var.Expense_Type = item["expenditure_id"].ToString();
                }
              
                _var.Months = item["month"].ToString();
                _var.Quantity = item["quantity"].ToString();
                _data.Add(_var);
            }

            return _data;
        }

        public Double FetchBudgetedAmount(String _fundsource, String _mooe_id, String _year) 
        {
            double _Amount = 0.00;

            StringBuilder varname1 = new StringBuilder();
            varname1.Append("SELECT \n");
            varname1.Append("SUM(ISNULL(mad.total,0)) as Total \n");
            varname1.Append("FROM mnda_activity_data mad \n");
            varname1.Append("INNER JOIN mnda_mooe_sub_expenditures msub on msub.id  = mad.mooe_sub_expenditure_id \n");
            varname1.Append("WHERE msub.uacs_code = '" + _mooe_id + "' AND \n");
            varname1.Append("mad.fund_source_id ='"+ _fundsource +"' \n");
            varname1.Append("AND mad.year = " + _year + " AND mad.status ='FINANCE APPROVED'");

            DataSet ds = c_data.ExecuteSQLQuery(varname1.ToString());

            if (ds.Tables.Count!=0)
            {
                if (ds.Tables[0].Rows.Count!=0)
                {
                    _Amount = Convert.ToDouble(ds.Tables[0].Rows[0]["Total"].ToString());
                }
            }

            return _Amount;
        }
        public List<ExpenseItems> FetchExpenseItemsRIS(String DivId, String _Year, String _Id,String MOOEID)
        {
            List<ExpenseItems> _data = new List<ExpenseItems>();

            var sb = new System.Text.StringBuilder(643);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"ISNULL(mpi.expenditure_id,'') as expenditure_id,");
            sb.AppendLine(@"mad.id,");
            sb.AppendLine(@"mad.month,");
            sb.AppendLine(@"mad.type_service,");
            sb.AppendLine(@"mooe.name,");
            sb.AppendLine(@"mad.quantity,");
            sb.AppendLine(@"mad.rate as total");
            sb.AppendLine(@"FROM mnda_program_encoded mpe");
            sb.AppendLine(@"INNER JOIN mnda_project_output mpo on mpo.program_code = mpe.id");
            sb.AppendLine(@"INNER JOIN mnda_activity ma on ma.output_id = mpo.id");
            sb.AppendLine(@"INNER JOIN mnda_activity_data  mad on mad.activity_id = ma.id");
            sb.AppendLine(@"INNER JOIN mnda_mooe_sub_expenditures mooe on mooe.id = mad.mooe_sub_expenditure_id");
            sb.AppendLine(@"LEFT JOIN mnda_procurement_items mpi on mpi.item_specifications = mad.type_service");
            sb.AppendLine(@"WHERE mad.fund_source_id ='" + _Id + "' AND mpe.acountable_division_code = '" + DivId + "'");
            sb.AppendLine(@"and mpe.project_year = '" + _Year + "' AND mooe.name ='" + MOOEID + "'  ANd mad.rate !=0 ");
            sb.AppendLine(@"AND mad.status ='FINANCE APPROVED'");
            sb.AppendLine(@"GROUP BY ");
            sb.AppendLine(@"mpi.expenditure_id,mad.id,mad.month,");
            sb.AppendLine(@"mad.type_service,mooe.name,");
            sb.AppendLine(@"mad.quantity,mad.rate");



            foreach (DataRow item in c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Rows)
            {
                ExpenseItems _var = new ExpenseItems();

                _var.Id = item["id"].ToString();
                if (item["type_service"].ToString().Trim() == "")
                {
                    _var.Description = item["name"].ToString();
                }
                else if (item["name"].ToString() == "Local Travel")
                {
                    _var.Description = item["type_service"].ToString();
                }
                else
                {
                    _var.Description = item["type_service"].ToString();
                }
                if (item["name"].ToString() == "Local Travel")
                {
                    _var.Expense_Type = "Local Travel";
                }
                else
                {
                    _var.Expense_Type = item["expenditure_id"].ToString();
                }
                _var.MonthNo = ReturnMonthValue(item["month"].ToString());
                _var.Months = item["month"].ToString();
                _var.Quantity = item["quantity"].ToString();
                _var.UnitCost = Convert.ToDouble(item["total"]).ToString("#,##0.00");
                _data.Add(_var);
            }

            return _data;
        }
    }

    public class DivisionData
    {
        public String Office { get; set; }
        public String Division { get; set; }
    }
    public class PreparedSignatory
    {
        public String Name { get; set; }
        public String Position { get; set; }
    }
    public class DivisionSignatory 
    {
        public String Name { get; set; }
        public String Position { get; set; }
    }

    public class RISDataListDetails
    {
        public String StockNo { get; set; }
        public String Unit { get; set; }
        public String Description { get; set; }
        public String Quantity { get; set; }
 
    }

    public class RISDataList
    {
        public String Code { get; set; }
        public String UserCode { get; set; }
        public String DivCode { get; set; }
        public String EntryDate { get; set; }
        public String Qtr { get; set; }
        public String Purpose { get; set; }
        public String Status { get; set; }
    }
    public class RISFundLibrary 
    {
        public String MOOE_ID { get; set; }
        public String ExpenseItem { get; set; }
  
    }
    public class RISList 
    {
          public string date_submitted { get; set; }
          public String months { get; set; }
          public String stock_no { get; set; }
          public String unit { get; set; }
          public String description { get; set; }
          public String purpose { get; set; }
          public String remarks { get; set; }
          public String status { get; set; }
    }
    public class SubmittedRIS 
    {
        public String RefNo { get; set; }
    }

    public class RISData 
    {
         public String mad_id { get; set; }
         public String stock_no { get; set; }
         public String unit { get; set; }
         public String description { get; set; }
         public String quantity { get; set; }

    }
    public class QuarterOBR 
    {
        public String Quarter { get; set; }
        public String Amount { get; set; }
    }
    public class ActualOBR
    {
           public String  AccountCode { get; set; }
            public String Division { get; set; }
            public Int32 _Month { get; set; }
           public String DateCreated { get; set; }
           public String Total { get; set; }
    }

    public class FundSourceList 
    {
        public String FundSource { get; set; }
        public String FundCode { get; set; }
        public String FundType { get; set; }
    }
    public class RISItems
    {
        public String stock_no { get; set; }
        public String unit { get; set; }
        public String description { get; set; }
        public String quantity { get; set; }

    }
    public class ExpenseMonths
    {
        public Int32 Num { get; set; }
        public String Name { get; set; }


    }
    public class ExpenseTypes
    {
        public String Name { get; set; }
  

    }
    public class ExpenseItems 
    {
        public String Id { get; set; }
        public String Expense_Type { get; set; }
        public Int32 MonthNo { get; set; }
        public String Months { get; set; }
        public String Description { get; set; }
        public String Quantity { get; set; }
        public String UnitCost { get; set; }
        public String NoDays { get; set; }
     
    }

    public class ActivityList 
    {
        public String Name { get; set; }
        public String Id { get; set; }

    }
    public class OutputList
    {
        public String Name { get; set; }
        public String Id { get; set; }

    }
    public class ProgramList 
    {
        public String Name { get; set; }
        public String Id { get; set; }
        
    }
    public class ProjectList 
    {
        public String Name { get; set; }
        public String Id { get; set; }
    }
   
}
