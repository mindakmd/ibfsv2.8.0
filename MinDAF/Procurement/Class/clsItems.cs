﻿using ReportTool.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Procurement.Class
{
    class clsItems
    {
        private clsData c_data = new clsData();

        public DataTable LoadItems() 
        {
            var sb = new System.Text.StringBuilder(271);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"met.code,");
            sb.AppendLine(@"met.expenditure,");
            sb.AppendLine(@"mpi.id,");
            sb.AppendLine(@"mpi.item_specifications,");
            sb.AppendLine(@"mpi.general_category,");
            sb.AppendLine(@"mpi.sub_category,");
            sb.AppendLine(@"mpi.minda_inventory,");
            sb.AppendLine(@"mpi.unit_of_measure,");
            sb.AppendLine(@"mpi.price");
            sb.AppendLine(@"FROM mnda_procurement_items mpi");
            sb.AppendLine(@"INNER JOIN mnda_expenditure_type met on met.code = mpi.expenditure_id");

            DataTable dtData = c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Copy();

            return dtData;

        }
        public DataTable LoadItemTypes()
        {
            var sb = new System.Text.StringBuilder(55);
            sb.AppendLine(@"SELECT");
            sb.AppendLine(@"met.expenditure");
            sb.AppendLine(@"FROM mnda_expenditure_type met");


            DataTable dtData = c_data.ExecuteSQLQuery(sb.ToString()).Tables[0].Copy();

            return dtData;

        }
    }
}
