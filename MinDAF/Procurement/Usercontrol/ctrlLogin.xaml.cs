﻿using Procurement.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Procurement_Module
{
    /// <summary>
    /// Interaction logic for ctrlLogin.xaml
    /// </summary>
    public partial class ctrlLogin : UserControl
    {
        public String IsAdmin { get; set; }
        public String DivId { get; set; }
        public String Division { get; set; }
        public String Office { get; set; }
        public String PapCode { get; set; }
        public String User { get; set; }
        public String UserCode { get; set; }
        public event EventHandler LoginSuccessfull;
        public event EventHandler LoginFailed;
        private clsLogin c_log;
        
        public ctrlLogin()
        {
            InitializeComponent();
       
        }

        private void VerifyUser() 
        {
            c_log = new clsLogin();
            if (c_log.VerifyUser(this.txtUsername.Text, this.txtPassword.Password.ToString())) 
            {
                this.IsAdmin = c_log.IsAdmin;
                this.Division = c_log.Division;
                this.Office = c_log.Office;
                this.DivId = c_log.DivId;
                this.PapCode = c_log.PapCode;
                this.User = c_log.User;
                if (LoginSuccessfull!=null)
                {
                    this.LoginSuccessfull(this, new EventArgs());
                }

            }
            else 
            {
                if (LoginFailed!=null)
                {
                    this.LoginFailed(this, new EventArgs());
                    txtPassword.Password = "";
                    txtUsername.Text = "";
                    txtUsername.Focus();
                }
            }
           
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            VerifyUser();
        }
    }
}
