﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Procurement.Usercontrol.Tools
{
    /// <summary>
    /// Interaction logic for _timeControl.xaml
    /// </summary>
    public partial class _timeControl : UserControl
    {
        public String TimeSelected { get; set; }
        public _timeControl()
        {
            InitializeComponent();
            CreateAMPM();
        }

        private void CreateAMPM() 
        {
            cmbAmPm.Items.Add("AM");
            cmbAmPm.Items.Add("PM");
            cmbAmPm.SelectedIndex = 0;
        }
        private void SetTime() 
        {
            try
            {
                 TimeSelected = txtNo1.Text + " : " + txtNo2.Text + " " + cmbAmPm.SelectedItem.ToString();
            }
            catch (Exception)
            {

            }
          
        }
        private void txtNo1_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetTime();
        }

        private void txtNo2_TextChanged(object sender, TextChangedEventArgs e)
        {
            SetTime();
        }

        private void cmbAmPm_DropDownClosed(object sender, EventArgs e)
        {
            SetTime();
        }

        private void txtNo1_MouseDown(object sender, MouseButtonEventArgs e)
        {
            txtNo1.SelectAll();
            txtNo1.Focus();
        }

        private void txtNo2_MouseDown(object sender, MouseButtonEventArgs e)
        {
            txtNo2.SelectAll();
            txtNo2.Focus();
        }

        private void txtNo1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                txtNo2.SelectAll();
                txtNo2.Focus();
            }
        }

        private void txtNo2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                cmbAmPm.Focus();
            }
        }
    }
}
