﻿using Infragistics.Windows.Controls;
using Infragistics.Windows.DataPresenter;
using Procurement.Class;
using Procurement.Report;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Procurement.Usercontrol.User
{
    /// <summary>
    /// Interaction logic for ctrlRISLIstxaml.xaml
    /// </summary>
    public partial class ctrlRISLIstxaml : UserControl
    {
        public string DivId { get; set; }
        private List<RISDataList> ris_list = new List<RISDataList>();

        private clsRIS c_ris = new clsRIS();

        public ctrlRISLIstxaml()
        {
            InitializeComponent();
            this.Loaded += ctrlRISLIstxaml_Loaded;
        }

        private void LoadRISData() 
        {
            ris_list = c_ris.FetchRIS(DivId);
            grdData.DataSource = null;
            grdData.DataSource = ris_list;

            grdData.FieldLayouts[0].Fields["Code"].Visibility = System.Windows.Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["DivCode"].Visibility = System.Windows.Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["UserCode"].Visibility = System.Windows.Visibility.Collapsed;
        }

        void ctrlRISLIstxaml_Loaded(object sender, RoutedEventArgs e)
        {
            LoadRISData();
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtSearch.Text == "")
            {
                grdData.DefaultFieldLayout.RecordFilters.Clear();
            }
            else
            {
                RecordFilter filter = new RecordFilter();
                filter.FieldName = "description";
                filter.Conditions.Add(new ComparisonCondition(ComparisonOperator.StartsWith, txtSearch.Text));
                grdData.DefaultFieldLayout.RecordFilters.Add(filter);
            }
        }

        private void LoadReportRIS()
        {
            RISDataList _selected = (RISDataList)grdData.ActiveDataItem;
            List<DivisionSignatory> _dataDivSig = new List<DivisionSignatory>();
            List<PreparedSignatory> _dataPrepSig = new List<PreparedSignatory>();
            List<DivisionData> _dataOffice = new List<DivisionData>();
            List<RISDataListDetails> _dataDetails = new List<RISDataListDetails>();

            _dataDivSig = c_ris.FetchDivisionSignatory(this.DivId);
            _dataPrepSig = c_ris.FetchPreparedSignatory(this.DivId, _selected.UserCode);
            _dataOffice = c_ris.FetchOfficeData(this.DivId);
            _dataDetails = c_ris.FetchRISDetails(this.DivId,_selected.Purpose);

            DataSet dsMain = new DataSet();
            DataTable dtHeader = new DataTable();
            DataTable dtDetail = new DataTable();

            dtHeader.Columns.Add("_division");
            dtHeader.Columns.Add("_office");
            dtHeader.Columns.Add("_resonsibility_center_code");
            dtHeader.Columns.Add("_ris_no");
            dtHeader.Columns.Add("_purpose");
            dtHeader.Columns.Add("_requested_by");
            dtHeader.Columns.Add("_approved_by");
            dtHeader.Columns.Add("_issued_by");
            dtHeader.TableName = "RISHeader";

            dtDetail.Columns.Add("_stock_no");
            dtDetail.Columns.Add("_unit");
            dtDetail.Columns.Add("_description");
            dtDetail.Columns.Add("_qty");
            dtDetail.TableName = "RISDetails";

            dsMain.DataSetName = "RISReport";

            DataRow dr = dtHeader.NewRow();

            dr["_division"]= _dataOffice[0].Division;
            dr["_office"] = _dataOffice[0].Office;
            dr["_resonsibility_center_code"]= "";
            dr["_ris_no"]= "";
            dr["_purpose"]= _selected.Purpose;
            dr["_requested_by"]= _dataPrepSig[0].Name;
            dr["_approved_by"]= _dataDivSig[0].Name;
            dr["_issued_by"] = "Maricel Caagoy";

            dtHeader.Rows.Add(dr);

            foreach (RISDataListDetails item in _dataDetails)
            {
                dr = dtDetail.NewRow();

                dr["_stock_no"] = item.StockNo;
                dr["_unit"] = item.Unit;
                dr["_description"] = item.Description;
                dr["_qty"] = item.Quantity;

                dtDetail.Rows.Add(dr);
            }

            dsMain.Tables.Add(dtHeader);
            dsMain.Tables.Add(dtDetail);


            crtRIS _rptData = new crtRIS();

            String _path = AppDomain.CurrentDomain.BaseDirectory.ToString() + @"ReportSchemas\";
            DataSet ds = dsMain.Copy();

         //   ds.WriteXmlSchema(_path + "divRIS.xml");
            _rptData.SetDataSource(ds);

            _report.ViewerCore.Zoom(45);
            _report.ViewerCore.ReportSource = _rptData;
        }

        private void grdData_SelectedItemsChanged(object sender, Infragistics.Windows.DataPresenter.Events.SelectedItemsChangedEventArgs e)
        {
            RISDataList _selected = (RISDataList)grdData.ActiveDataItem;

            switch (_selected.Status)
            {
                case "Submitted":
                    LoadReportRIS();
                    lblStatus.Content = "Submitted";
                    grdLock.Visibility = System.Windows.Visibility.Visible;
                    btnPrint.IsEnabled = false;
                   
                    break;
                default:
                    break;
            }
        }
    }
}
