﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Procurement.Usercontrol.User
{
    /// <summary>
    /// Interaction logic for ctrlApproval.xaml
    /// </summary>
    public partial class ctrlApproval : UserControl
    {
        ctrlRISApproval c_app_ris = new ctrlRISApproval();

        public ctrlApproval()
        {
            InitializeComponent();
        }

        private void LoadRISApprovalControl() 
        {
            c_app_ris = new ctrlRISApproval();
            c_app_ris.Width = grdChild.Width;
            c_app_ris.Height = grdChild.Height;

            grdChild.Children.Clear();
            grdChild.Children.Add(c_app_ris);
        }

        private void btnRISApprovals_Click(object sender, RoutedEventArgs e)
        {
            LoadRISApprovalControl();
        }
    }
}
