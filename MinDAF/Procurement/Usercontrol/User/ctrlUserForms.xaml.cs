﻿using Procurement.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Procurement.Usercontrol.User
{
    /// <summary>
    /// Interaction logic for ctrlUserForms.xaml
    /// </summary>
    public partial class ctrlUserForms : UserControl
    {
        public String _DivId { get; set; }
        public String _Division { get; set; }
        public String _Office { get; set; }
        public String _PapCode { get; set; }
        public String _User { get; set; }

        private ctrlRIS c_ris = new ctrlRIS();
        private ctrlPR c_pr = new ctrlPR();
        private ctrlRISLIstxaml c_ris_list = new ctrlRISLIstxaml();
        private ctrlTO c_to = new ctrlTO();
        private ctrlIT c_it = new ctrlIT();
        public ctrlUserForms()
        {
            InitializeComponent();

            c_ris.Division = _Division;
            c_ris.Office = _Office;
            c_ris.DivId = this._DivId;
        }

        private void btnList_Click(object sender, RoutedEventArgs e)
        {
            grdStack.Children.Clear();
            c_ris_list.Height = grdStack.Height;
            c_ris_list.Width = grdStack.Width;
            c_ris_list.DivId = this._DivId;
            grdStack.Children.Add(c_ris_list);
        }

        private void btnGenerate_Click(object sender, RoutedEventArgs e)
        {
            grdStack.Children.Clear();
            c_ris.Height = grdStack.Height;
            c_ris.Width = grdStack.Width;
            c_ris.Division = this._Division;
            c_ris.Office = this._Office;
            c_ris.DivId = this._DivId;
            c_ris.User = this._User;
            grdStack.Children.Add(c_ris);
        }

        private void btnPRGenerate_Click(object sender, RoutedEventArgs e)
        {
            grdStack.Children.Clear();
            c_pr.Height = grdStack.Height;
            c_pr.Width = grdStack.Width;
            c_pr.Division = this._Division;
            c_pr.Office = this._Office;
            c_pr.DivId = this._DivId;
            c_pr.PapCode = this._PapCode;
            grdStack.Children.Add(c_pr);
        }

        private void btnListPR_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnTOGenerate_Click(object sender, RoutedEventArgs e)
        {
            grdStack.Children.Clear();
            c_to.Height = grdStack.Height;
            c_to.Width = grdStack.Width;
            c_to.DivId = this._DivId;
            c_to.User = this._User;
            grdStack.Children.Add(c_to);
        }
        private frmTOList f_TO = new frmTOList();

        private void btnITGenerate_Click(object sender, RoutedEventArgs e)
        {
           
          
                grdStack.Children.Clear();

                c_it.Height = grdStack.Height;
                c_it.Width = grdStack.Width;
                c_it.DivId = this._DivId;
                c_it.User = this._User;
                

                grdStack.Children.Add(c_it);

        }
    }
}
