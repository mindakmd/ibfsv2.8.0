﻿using Procurement.Class;
using Procurement.Forms;
using Procurement.Forms.Boxes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Procurement.Usercontrol.User
{
    /// <summary>
    /// Interaction logic for ctrlTO.xaml
    /// </summary>
    public partial class ctrlTO : UserControl
    {

        private frmTravelOrder ft_order = new frmTravelOrder();
        private frmReportPreview frmPreview = new frmReportPreview("",null);
        public String DivId { get; set; }
        public String User { get; set; }
        private clsTO c_to = new clsTO();

        private List<TO_TravelOrderList> c_to_list = new List<TO_TravelOrderList>();

        public ctrlTO()
        {
            InitializeComponent();

            this.Loaded += ctrlTO_Loaded;

        }

        void ctrlTO_Loaded(object sender, RoutedEventArgs e)
        {
            Initialize();

        }

        private void DeleteTO() 
        {
            if (grdData.ActiveDataItem!=null)
            {
                frmBox f_box = new frmBox("Confirm Delete of item");

                f_box.ShowDialog();
                if (f_box.DialogResult == true)
                {
                    TO_TravelOrderList _xdata = (TO_TravelOrderList)grdData.ActiveDataItem;

                    Boolean _isdeleted = c_to.DeleteTO(_xdata.code);

                    if (_isdeleted)
                    {
                        MessageBox.Show("Item has been marked as deleted", "Item Removed");
                        Initialize();
                    }
                    else
                    {
                        MessageBox.Show("Item failed to be deleted", "Failed");
                    }
                }

               
            }
           

        }

        private void EditTO()
        {
            
        }
        private void Initialize() 
        {
            c_to_list = c_to.FetchTravelOrders(DivId);

            grdData.DataSource = null;
            grdData.DataSource = c_to_list;

            grdData.FieldLayouts[0].Fields["date"].Label = "Entry Date";
            grdData.FieldLayouts[0].Fields["code"].Label = "TO Code";
            grdData.FieldLayouts[0].Fields["_details"].Label = "Description";
            grdData.FieldLayouts[0].Fields["user_created"].Label = "Created By";

            grdData.FieldLayouts[0].Fields["date"].Width = new Infragistics.Windows.DataPresenter.FieldLength(50);
            grdData.FieldLayouts[0].Fields["code"].Width = new Infragistics.Windows.DataPresenter.FieldLength(50);
            grdData.FieldLayouts[0].Fields["_details"].Width = new Infragistics.Windows.DataPresenter.FieldLength(150);
            grdData.FieldLayouts[0].Fields["user_created"].Width = new Infragistics.Windows.DataPresenter.FieldLength(80);

           
        }

        private void ClearDetails() 
        {
            grdPerson.DataSource = null;
            txtSubject.Text = "";
            txtTo.Text = "";
            lblDetail_1.Text = "";
            lblDetail_2.Text = "";
            lblFund.Content = "";
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            ft_order = new frmTravelOrder();
            ft_order.DivId = this.DivId;
            ft_order.User = this.User;
            ft_order.ShowDialog();
            if (ft_order.DialogResult == true)
            {
                Initialize();
            }
        }

        private void grdData_SelectedItemsChanged(object sender, Infragistics.Windows.DataPresenter.Events.SelectedItemsChangedEventArgs e)
        {
            TO_TravelOrderList c_data = (TO_TravelOrderList)grdData.ActiveDataItem;

            TO_TravelOrderDetails _data = c_to.FetchTravelOrdersDetails(c_data.code);
            txtTo.Text = _data.To;
            txtSubject.Text = _data.Subject;

            lblDetail_1.Text = _data.Detail_1;
            lblDetail_2.Text = _data.Detail_2;
            lblFund.Content = "MinDA Fund:    PAP CODE: " + _data.MindaFund;
            grdPerson.DataSource = null;
            grdPerson.DataSource = _data.Person;

            grdDetails.Visibility = System.Windows.Visibility.Visible;
           
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            DeleteTO();
            ClearDetails();
        }

        private void grdData_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ft_order = new frmTravelOrder();
            ft_order.IsEdit = true;

            TO_TravelOrderList c_data = (TO_TravelOrderList)grdData.ActiveDataItem;
               TO_TravelOrderDetails _data = c_to.FetchTravelOrdersDetails(c_data.code);

               ft_order.txtTo.Text = _data.To;
               ft_order.txtSubject.Text = _data.Subject;
               ft_order.txtDetails_1.Text = _data.Detail_1;
               ft_order.txtDetails_2.Text = _data.Detail_2;
               ft_order._listpersonnel = _data.Person;
               ft_order.TOCode = c_data.code;  

               ft_order.ShowDialog();
        }

        private void PrintPreview() 
        {
            TO_TravelOrderList c_data = (TO_TravelOrderList)grdData.ActiveDataItem;
            TO_TravelOrderDetails _data = c_to.FetchTravelOrdersDetails(c_data.code);


            DataSet dsMain = new DataSet();
            DataTable dtDetails = new DataTable();
            DataTable dtPersons = new DataTable();

            dtDetails.Columns.Add("_to");
            dtDetails.Columns.Add("_subject");
            dtDetails.Columns.Add("_details_1");
            dtDetails.Columns.Add("_details_2");
            dtDetails.Columns.Add("_details_3");
            dtDetails.Columns.Add("_fundsource");
            dtDetails.Columns.Add("_signee");
            dtDetails.Columns.Add("_travel_date");

            dtPersons.Columns.Add("_peson");
            dtPersons.Columns.Add("_position");

            dtDetails.TableName = "Details";
            dtPersons.TableName = "Persons";

            DataRow dr = dtDetails.NewRow();
            dr["_to"] = _data.To;
            dr["_subject"] = _data.Subject;
            dr["_details_1"] = "1.  THE FOLLOWING PERSONNEL OF THE AUTHORITY ARE HEREBY AUTHORIZED TO PROCEED TO PLACE/S STATED ABOVE ON OFFICIAL BUSINESS FROM/ON " + _data.Detail_1;
            dr["_details_2"] = "2.  " + _data.Detail_2;
            dr["_details_3"] = "3. 	UPON COMPLETION OF TRAVEL, THE USUAL CERTIFICATE OF APPEARANCE AND CERTIFICATE OF TRAVEL COMPLETED SHALL BE SUBMITTED TO THE ADMINISTRATIVE STAFF AND A REPORT OF THE ACTIVITY TRAVEL SHALL BE SUBMITTED TO THE OFFICER WITHIN THREE (3) DAYS AFTER COMPLETION OF TRAVEL.";
            dr["_fundsource"] = "MinDA Fund:    PAP CODE:     " + _data.MindaFund;
            dr["_signee"] = "SEC. DATU HJ. ABUL KHAYR D. ALONTO";
            dr["_travel_date"] = Convert.ToDateTime(_data.TravelDate).ToShortDateString();

            dtDetails.Rows.Add(dr);

         
            foreach (var item in _data.Person)
            {
                DataRow drP = dtPersons.NewRow();
                drP["_peson"] = item.PERSONNEL;
                drP["_position"] = item.POSITION;
                dtPersons.Rows.Add(drP);

            }
            dsMain.Tables.Add(dtDetails);
            dsMain.Tables.Add(dtPersons);


       //     frmPreview.dsReport = dsMain;
            frmPreview = new frmReportPreview("Travel Order", dsMain);

            frmPreview.ShowDialog();

        }

        private void btnPrintPreview_Click(object sender, RoutedEventArgs e)
        {


            PrintPreview();
        }
    }
}
