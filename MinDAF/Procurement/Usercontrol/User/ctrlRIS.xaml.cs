﻿using Infragistics.Windows.Controls;
using Infragistics.Windows.DataPresenter;
using Procurement.Class;
using Procurement.Forms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Procurement.Usercontrol.User
{
    /// <summary>
    /// Interaction logic for ctrlRIS.xaml
    /// </summary>
    public partial class ctrlRIS : UserControl
    {

        private List<ProgramList> _Programs = new List<ProgramList>();
        private List<ProjectList> _Projects = new List<ProjectList>();
        private List<OutputList> _Output = new List<OutputList>();
        private List<ActivityList> _Activity = new List<ActivityList>();
        private List<ExpenseItems> _ExpenseItems = new List<ExpenseItems>();
        private List<SubmittedRIS> _SubmittedRIS = new List<SubmittedRIS>();

        private List<RISData> _RISData = new List<RISData>();

        private clsRIS c_ris = new clsRIS();

        public String DivId { get; set; }
        public String Division { get; set; }
        public String Office { get; set; }
        public String User { get; set; }


        private DataTable dtItems;
        private String FundSource;
        private String FundCode;
        private String FundYear;
        private String FundType;

        public ctrlRIS()
        {
            InitializeComponent();
            this.Loaded += ctrlRIS_Loaded;
        }

        void ctrlRIS_Loaded(object sender, RoutedEventArgs e)
        {
            Initialize();
        }
        private void GenerateYear() 
        {          

           

            frmFundsource f_source = new frmFundsource();
            f_source.DivId = this.DivId;
            f_source.ShowDialog();

            if (f_source.DialogResult == true)
            {
                FundSource = f_source.FundSource;
                FundCode = f_source.FundCode;
                FundYear = f_source._Year;
                FundType = f_source.FundType;
                FetchSubmittedRIS();
            }
        }

        private void FetchSubmittedRIS ()
        {
            _SubmittedRIS = c_ris.FetchSubmittedRIS(this.DivId);

            List<RISFundLibrary> _list_data = c_ris.FetchFundLibrary(FundCode,this.FundYear);

            grdPPMP.DataSource = null;
            grdPPMP.DataSource = _list_data;
            grdPPMP.FieldLayouts[0].Fields["MOOE_ID"].Visibility = System.Windows.Visibility.Collapsed;
            
        }

        private void FetchPrograms() 
        {
            //if ( cmbYear.SelectedItem==null)
            //{
            //    return;
            //}
            //cmbProgram.Items.Clear();
            //cmbProject.Items.Clear();
            //cmbOutput.Items.Clear();
            //cmbActivity.Items.Clear();
           

            //_Programs.Clear();
            //_Projects.Clear();
            //_Output.Clear();
            //_Activity.Clear();
            //_ExpenseItems.Clear();
            //grdExpense.DataSource = _ExpenseItems;

            //_Programs = c_ris.FetchPrograms(DivId, cmbYear.SelectedItem.ToString());
          

            //foreach (ProgramList item in _Programs)
            //{
            //    cmbProgram.Items.Add(item.Name);
            //}
            //cmbProgram.ItemsSource = cmbProgram.DataContext;
         //   cmbProgram.SelectedIndex = -1;
        }
        private void FetchProjects()        
        {
            //string _Id = "";
            //List<ProgramList> _xdata = _Programs.Where(x => x.Name == cmbProgram.SelectedItem).ToList();
            //if (_xdata.Count!=0)
            //{
            //    _Id = _xdata[0].Id.ToString();
            //}
            //cmbProject.Items.Clear();
            //cmbOutput.Items.Clear();
            //_Projects.Clear();

            //_Projects = c_ris.FetchProjects(DivId, cmbYear.SelectedItem.ToString(),_Id);


            //foreach (ProjectList item in _Projects)
            //{
            //    cmbProject.Items.Add(item.Name);
            //}

           // cmbProgram.SelectedIndex = -1;
            //cmbProgram.ItemsSource = cmbProgram.DataContext;
        }

        private void FetchOutput()
        {
            //string _Id = "";
            //List<ProjectList> _xdata = _Projects.Where(y => y.Name == cmbProject.SelectedItem).ToList();
            //if (_xdata.Count != 0)
            //{
            //    _Id = _xdata[0].Id.ToString();
            //}
            //cmbOutput.Items.Clear();
            //cmbActivity.Items.Clear();
            //_Output.Clear();
            //_Output = c_ris.FetchOutput(DivId, cmbYear.SelectedItem.ToString(), _Id);


            //foreach (OutputList item in _Output)
            //{
            //    cmbOutput.Items.Add(item.Name);
            //}

            // cmbProgram.SelectedIndex = -1;
            //cmbProgram.ItemsSource = cmbProgram.DataContext;
        }
        private void FetchActivity()
        {
            //string _Id = "";
            //List<OutputList> _xdata = _Output.Where(y => y.Name == cmbOutput.SelectedItem).ToList();
            //if (_xdata.Count != 0)
            //{
            //    _Id = _xdata[0].Id.ToString();
            //}
            //cmbActivity.Items.Clear();
            //_Activity.Clear();
            //_Activity = c_ris.FetchActivity(DivId, cmbYear.SelectedItem.ToString(), _Id);


            //foreach (ActivityList item in _Activity)
            //{
            //    cmbActivity.Items.Add(item.Name);
            //}

            // cmbProgram.SelectedIndex = -1;
            //cmbProgram.ItemsSource = cmbProgram.DataContext;
        }
        private void FetchExpenseItems(String _month_range)
        {
            RISFundLibrary _selected = (RISFundLibrary)grdPPMP.ActiveDataItem;

            _ExpenseItems.Clear();
            _ExpenseItems = c_ris.FetchExpenseItemsRIS(DivId, FundYear,FundCode, _selected.ExpenseItem);

            foreach (var item in _SubmittedRIS)
            {
                List<ExpenseItems> _data = _ExpenseItems.Where(x => x.Id == item.RefNo).ToList();
                if (_data.Count != 0)
                {
                    ExpenseItems _remove = _data[0];
                    _ExpenseItems.Remove(_remove);
                }
            }
           
            switch (_month_range)
            {
                case "1st Qtr. [Jan-Mar]":
                    List<ExpenseItems> _FirstQ = _ExpenseItems.Where(x => x.MonthNo >= 1 && x.MonthNo <= 3).ToList();
                    _ExpenseItems = _FirstQ;
                    break;
                case "2nd Qtr. [Apr-Jun]":
                    List<ExpenseItems> _SecondQ = _ExpenseItems.Where(x => x.MonthNo >= 4 && x.MonthNo <= 6).ToList();
                    _ExpenseItems = _SecondQ;
                    break;
                case "3rd Qtr. [Jul-Sep]":
                    List<ExpenseItems> _ThirdQ = _ExpenseItems.Where(x => x.MonthNo >= 7 && x.MonthNo <= 9).ToList();
                    _ExpenseItems = _ThirdQ;
                    break;
                case "4th Qtr. [Oct-Dec]":
                    List<ExpenseItems> _FourthQ = _ExpenseItems.Where(x => x.MonthNo >= 10 && x.MonthNo <= 12).ToList();
                    _ExpenseItems = _FourthQ;
                    break;
                
            }
          
        }
        private void Initialize() 
        {
            _RISData = new List<RISData>();
            _ExpenseItems = new List<ExpenseItems>();
         
            grdData.DataSource = _RISData;
            grdExpense.DataSource = _ExpenseItems;


            grdData.FieldLayouts[0].Fields["stock_no"].Width = new Infragistics.Windows.DataPresenter.FieldLength(50);
            grdData.FieldLayouts[0].Fields["unit"].Width = new Infragistics.Windows.DataPresenter.FieldLength(50);
            grdData.FieldLayouts[0].Fields["description"].Width = new Infragistics.Windows.DataPresenter.FieldLength(250);
            grdData.FieldLayouts[0].Fields["quantity"].Width = new Infragistics.Windows.DataPresenter.FieldLength(40);

            grdData.FieldLayouts[0].Fields["stock_no"].Label = "STOCK NO";
            grdData.FieldLayouts[0].Fields["unit"].Label = "UNIT";
            grdData.FieldLayouts[0].Fields["description"].Label = "DESCRIPTION";
            grdData.FieldLayouts[0].Fields["quantity"].Label = "QTY";
            grdData.FieldLayouts[0].Fields["mad_id"].Visibility = System.Windows.Visibility.Collapsed;

            try
            {
                grdExpense.FieldLayouts[0].Fields["Description"].Width = new Infragistics.Windows.DataPresenter.FieldLength(200);
                grdExpense.FieldLayouts[0].Fields["Quantity"].Width = new Infragistics.Windows.DataPresenter.FieldLength(100);
                grdExpense.FieldLayouts[0].Fields["Months"].Visibility = System.Windows.Visibility.Collapsed;
                grdExpense.FieldLayouts[0].Fields["Expense_Type"].Visibility = System.Windows.Visibility.Collapsed;
                grdExpense.FieldLayouts[0].Fields["Id"].Visibility = System.Windows.Visibility.Collapsed;
                grdExpense.FieldLayouts[0].Fields["MonthNo"].Visibility = System.Windows.Visibility.Collapsed;
            }
            catch (Exception)
            {

            }
            

            txtDivision.Text = this.Division;
            txtOffice.Text = this.Office;
            GenerateYear();
           

        }

        private void cmbYear_DropDownClosed(object sender, EventArgs e)
        {
            FetchPrograms();
        }

        private void cmbProgram_DropDownClosed(object sender, EventArgs e)
        {
            FetchProjects();
        }

        private void cmbProject_DropDownClosed(object sender, EventArgs e)
        {
            FetchOutput();
        }

        private void cmbOutput_DropDownClosed(object sender, EventArgs e)
        {
            FetchActivity();
        }

        private void cmbActivity_DropDownClosed(object sender, EventArgs e)
        {
            //if (cmbActivity.SelectedItem!=null)
            //{
            //    txtPurpose.Text = cmbActivity.SelectedItem.ToString();
            //}
          
           

            //GenerateMonths();
        }

        private void GenerateMonths()
        {
            //cmbMonths.Items.Clear();

            //cmbMonths.Items.Add("Jan");
            //cmbMonths.Items.Add("Feb");
            //cmbMonths.Items.Add("Mar");
            //cmbMonths.Items.Add("Apr");
            //cmbMonths.Items.Add("May");
            //cmbMonths.Items.Add("Jun");
            //cmbMonths.Items.Add("Jul");
            //cmbMonths.Items.Add("Aug");
            //cmbMonths.Items.Add("Sep");
            //cmbMonths.Items.Add("Oct");
            //cmbMonths.Items.Add("Nov");
            //cmbMonths.Items.Add("Dec");

            grdExpLock.IsEnabled = true;
        }
        private List<ExpenseItems> _exp_data;
        private void cmbMonths_DropDownClosed(object sender, EventArgs e)
        {
            RISFundLibrary _selected = (RISFundLibrary)grdPPMP.ActiveDataItem;

            List<ExpenseMonths> _list_months = c_ris.FetchActualMonths(this.FundCode, _selected.MOOE_ID);

            if ( grdPPMP.ActiveDataItem == null || cmbMonths.SelectedItem == null)
            {
                return;
            }

            FetchExpenseItems(cmbMonths.SelectedItem.ToString());

            

            string _extype = "";
            switch (_selected.ExpenseItem.ToString())
            {
                case "Other Supplies and Materials Expenses":
                    _extype = "EXP-OTS";
                    break;
                case "ICT Office Supplies":
                    _extype = "EXP-ICT";
                    break;
                case "Office Supplies Expenses":
                    _extype = "EXP-OS";
                    break;
                case "Local Travel":
                    _extype = "Local Travel";
                    break;
            }
            _exp_data = new List<ExpenseItems>();
            _exp_data = _ExpenseItems;

            grdExpense.DataSource = _exp_data;

            grdExpense.FieldLayouts[0].Fields["Description"].Width = new Infragistics.Windows.DataPresenter.FieldLength(200);
            grdExpense.FieldLayouts[0].Fields["Quantity"].Width = new Infragistics.Windows.DataPresenter.FieldLength(100);

            grdExpense.FieldLayouts[0].Fields["Months"].Visibility = System.Windows.Visibility.Collapsed;
            grdExpense.FieldLayouts[0].Fields["Expense_Type"].Visibility = System.Windows.Visibility.Collapsed;
            grdExpense.FieldLayouts[0].Fields["NoDays"].Visibility = System.Windows.Visibility.Collapsed;

            grdExpLock.IsEnabled = true;

        }

        private List<ActualOBR> _ActualOBR = new List<ActualOBR>();
        private List<QuarterOBR> QuarterOBR = new List<QuarterOBR>();

        private void FetchActualMonths() 
        {
          
        
            cmbMonths.Items.Clear();
           
            cmbMonths.Items.Add("1st Qtr. [Jan-Mar]");
            cmbMonths.Items.Add("2nd Qtr. [Apr-Jun]");
            cmbMonths.Items.Add("3rd Qtr. [Jul-Sep]");
            cmbMonths.Items.Add("4th Qtr. [Oct-Dec]");
        
        }
        private void FetchActualObligation() 
        {
            RISFundLibrary _selected = (RISFundLibrary)grdPPMP.ActiveDataItem;
            txtTotalAllocation.Text = c_ris.FetchAllocation(this.DivId, _selected.MOOE_ID, FundYear,FundCode,this.FundType).ToString("#,##0.00"); 

            txtBalance.Text = "0.00";
            QuarterOBR.Clear();
            string MOOE = "";
            string _extype = "";
            switch (_selected.ExpenseItem)
            {
                case "Other Supplies and Materials Expenses":
                    _extype = "EXP-OTS";
                    MOOE = "5020399000";
                    break;
                case "ICT Office Supplies":
                    _extype = "EXP-ICT";
                    MOOE = "50203011002";
                    break;
                case "Office Supplies Expenses":
                    _extype = "EXP-OS";
                    MOOE = "5020301000";
                    break;
                case "Local Travel":
                    _extype = "Local Travel";
                    MOOE = "5020101000";
                    break;
            }
            _ActualOBR = c_ris.FetchOBRData(FundYear, this.FundSource, "", MOOE);

            double _TotalPlanned =0.00;

            switch (_selected.ExpenseItem)
            {
                case "Other Supplies and Materials Expenses":
                    List<ActualOBR> _FirstOS = _ActualOBR.Where(x => x._Month >= 1 && x._Month <= 3).ToList();

                    List<ActualOBR> _SecondOS = _ActualOBR.Where(x => x._Month >= 4 && x._Month <= 6).ToList();
                    List<ActualOBR> _ThirdOS = _ActualOBR.Where(x => x._Month >= 7 && x._Month <= 9).ToList();
                    List<ActualOBR> _FourthOS = _ActualOBR.Where(x => x._Month >= 10 && x._Month <= 12).ToList();

                    QuarterOBR _dataOS = new QuarterOBR();
                    double _amountOS = 0.00;
                    double _totalOS = 0.00;

                        _dataOS.Quarter = "1st Qtr. [Jan-Mar]";
                        foreach (var item in _FirstOS)
                        {
                            _amountOS += Convert.ToDouble(item.Total);
                        }
                        _totalOS += _amountOS;
                        _dataOS.Amount = _amountOS.ToString("#,##0.00");
                        _amountOS = 0;
                        QuarterOBR.Add(_dataOS);

                        _dataOS = new QuarterOBR();

                        _dataOS.Quarter = "2nd Qtr. [Apr-Jun]";
                        foreach (var item in _SecondOS)
                        {
                            _amountOS += Convert.ToDouble(item.Total);
                        }
                        _totalOS += _amountOS;
                        _dataOS.Amount = _amountOS.ToString("#,##0.00");
                        _amountOS = 0;
                        QuarterOBR.Add(_dataOS);

                        _dataOS = new QuarterOBR();

                        _dataOS.Quarter = "3rd Qtr. [Jul-Sep]";
                        foreach (var item in _ThirdOS)
                        {
                            _amountOS += Convert.ToDouble(item.Total);
                        }
                        _totalOS += _amountOS;
                        _dataOS.Amount = _amountOS.ToString("#,##0.00");
                        _amountOS = 0;
                        QuarterOBR.Add(_dataOS);

                        _dataOS = new QuarterOBR();

                        _dataOS.Quarter = "4th Qtr. [Oct-Dec]";
                        foreach (var item in _FourthOS)
                        {
                            _amountOS += Convert.ToDouble(item.Total);
                        }
                        _totalOS += _amountOS;
                        _dataOS.Amount = _amountOS.ToString("#,##0.00");
                        _amountOS = 0;
                        QuarterOBR.Add(_dataOS);

                        grdActual.DataSource = null;
                        grdActual.DataSource = QuarterOBR;

                        _TotalPlanned = c_ris.FetchBudgetedAmount(this.FundCode, MOOE, this.FundYear);
                        double _contigencyOS = Convert.ToDouble(txtTotalAllocation.Text) - _TotalPlanned;

                        double _balanceOS = ((Convert.ToDouble(txtTotalAllocation.Text) - _contigencyOS) - _totalOS);

                        txtBalance.Text = _balanceOS.ToString("#,##0.00");
                        txtContingency.Text = _contigencyOS.ToString("#,##0.00");

                        if (_balanceOS <= 0)
                        {
                            MessageBox.Show("Cannot generate RIS due to low in budget amount");
                            //btnSubmit.IsEnabled = false;
                            //btnUp.IsEnabled = false;
                        }
                    break;
                case "ICT Office Supplies":
                  List<ActualOBR> _FirstIT = _ActualOBR.Where(x => x._Month >= 1 && x._Month <= 3).ToList();

                    List<ActualOBR> _SecondIT = _ActualOBR.Where(x => x._Month >= 4 && x._Month <= 6).ToList();
                    List<ActualOBR> _ThirdIT = _ActualOBR.Where(x => x._Month >= 7 && x._Month <= 9).ToList();
                    List<ActualOBR> _FourthIT = _ActualOBR.Where(x => x._Month >= 10 && x._Month <= 12).ToList();

                    QuarterOBR _dataIT = new QuarterOBR();
                    double _amountIT = 0.00;
                    double _totalIT = 0.00;

                        _dataIT.Quarter = "1st Qtr. [Jan-Mar]";
                        foreach (var item in _FirstIT)
                        {
                            _amountIT += Convert.ToDouble(item.Total);
                        }
                        _totalIT += _amountIT;
                        _dataIT.Amount = _amountIT.ToString("#,##0.00");
                        _amountIT = 0;
                        QuarterOBR.Add(_dataIT);

                        _dataIT = new QuarterOBR();

                        _dataIT.Quarter = "2nd Qtr. [Apr-Jun]";
                        foreach (var item in _SecondIT)
                        {
                            _amountIT += Convert.ToDouble(item.Total);
                        }
                        _totalIT += _amountIT;
                        _dataIT.Amount = _amountIT.ToString("#,##0.00");
                        _amountIT = 0;
                        QuarterOBR.Add(_dataIT);

                        _dataIT = new QuarterOBR();

                        _dataIT.Quarter = "3rd Qtr. [Jul-Sep]";
                        foreach (var item in _ThirdIT)
                        {
                            _amountIT += Convert.ToDouble(item.Total);
                        }
                        _totalIT += _amountIT;
                        _dataIT.Amount = _amountIT.ToString("#,##0.00");
                        _amountIT = 0;
                        QuarterOBR.Add(_dataIT);

                        _dataIT = new QuarterOBR();

                        _dataIT.Quarter = "4th Qtr. [Oct-Dec]";
                        foreach (var item in _FourthIT)
                        {
                            _amountIT += Convert.ToDouble(item.Total);
                        }
                        _totalIT += _amountIT;
                        _dataIT.Amount = _amountIT.ToString("#,##0.00");
                        _amountIT = 0;
                        QuarterOBR.Add(_dataIT);

                        grdActual.DataSource = null;
                        grdActual.DataSource = QuarterOBR;


                        _TotalPlanned = c_ris.FetchBudgetedAmount(this.FundCode, MOOE, this.FundYear);
                        double _contigencyIT = Convert.ToDouble(txtTotalAllocation.Text) - _TotalPlanned;

                        double _balanceIT = ((Convert.ToDouble(txtTotalAllocation.Text) - _contigencyIT) - _totalIT);

                        txtContingency.Text = _contigencyIT.ToString("#,##0.00");
                        txtBalance.Text = _balanceIT.ToString("#,##0.00");

                        if (_balanceIT <= 0)
                        {
                            MessageBox.Show("Cannot generate RIS due to low in budget amount");
                            //btnSubmit.IsEnabled = false;
                            //btnUp.IsEnabled = false;
                        }
                    break;
                case "Office Supplies Expenses":
                     List<ActualOBR> _FirstOFS = _ActualOBR.Where(x => x._Month >= 1 && x._Month <= 3).ToList();

                    List<ActualOBR> _SecondOFS = _ActualOBR.Where(x => x._Month >= 4 && x._Month <= 6).ToList();
                    List<ActualOBR> _ThirdOFS = _ActualOBR.Where(x => x._Month >= 7 && x._Month <= 9).ToList();
                    List<ActualOBR> _FourthOFS = _ActualOBR.Where(x => x._Month >= 10 && x._Month <= 12).ToList();

                    QuarterOBR _dataOFS = new QuarterOBR();
                    double _amountOFS = 0.00;
                    double _totalOFS = 0.00;

                        _dataOFS.Quarter = "1st Qtr. [Jan-Mar]";
                        foreach (var OFSem in _FirstOFS)
                        {
                            _amountOFS += Convert.ToDouble(OFSem.Total);
                        }
                        _totalOFS += _amountOFS;
                        _dataOFS.Amount = _amountOFS.ToString("#,##0.00");
                        _amountOFS = 0;
                        QuarterOBR.Add(_dataOFS);

                        _dataOFS = new QuarterOBR();

                        _dataOFS.Quarter = "2nd Qtr. [Apr-Jun]";
                        foreach (var OFSem in _SecondOFS)
                        {
                            _amountOFS += Convert.ToDouble(OFSem.Total);
                        }
                        _totalOFS += _amountOFS;
                        _dataOFS.Amount = _amountOFS.ToString("#,##0.00");
                        _amountOFS = 0;
                        QuarterOBR.Add(_dataOFS);

                        _dataOFS = new QuarterOBR();

                        _dataOFS.Quarter = "3rd Qtr. [Jul-Sep]";
                        foreach (var OFSem in _ThirdOFS)
                        {
                            _amountOFS += Convert.ToDouble(OFSem.Total);
                        }
                        _totalOFS += _amountOFS;
                        _dataOFS.Amount = _amountOFS.ToString("#,##0.00");
                        _amountOFS = 0;
                        QuarterOBR.Add(_dataOFS);

                        _dataOFS = new QuarterOBR();

                        _dataOFS.Quarter = "4th Qtr. [Oct-Dec]";
                        foreach (var OFSem in _FourthOFS)
                        {
                            _amountOFS += Convert.ToDouble(OFSem.Total);
                        }
                        _totalOFS += _amountOFS;
                        _dataOFS.Amount = _amountOFS.ToString("#,##0.00");
                        _amountOFS = 0;
                        QuarterOBR.Add(_dataOFS);

                        grdActual.DataSource = null;
                        grdActual.DataSource = QuarterOBR;

                        _TotalPlanned = c_ris.FetchBudgetedAmount(this.FundCode, MOOE, this.FundYear);
                        double _contigencyOFS= Convert.ToDouble(txtTotalAllocation.Text) - _TotalPlanned;
                        double _balanceOFS = ((Convert.ToDouble(txtTotalAllocation.Text) - _contigencyOFS) - _totalOFS);

                        txtContingency.Text = _contigencyOFS.ToString("#,##0.00");
                        txtBalance.Text = _balanceOFS.ToString("#,##0.00");

                        if (_balanceOFS <= 0)
                        {
                            MessageBox.Show("Cannot generate RIS due to low in budget amount");
                            //btnSubmOFS.IsEnabled = false;
                            //btnUp.IsEnabled = false;
                        }
                    break;
                case "Local Travel":
                      List<ActualOBR> _First = _ActualOBR.Where(x => x._Month >= 1 && x._Month <= 3).ToList();

                        List<ActualOBR> _Second = _ActualOBR.Where(x => x._Month >= 4 && x._Month <= 6).ToList();
                        List<ActualOBR> _Third = _ActualOBR.Where(x => x._Month >= 7 && x._Month <= 9).ToList();
                        List<ActualOBR> _Fourth = _ActualOBR.Where(x => x._Month >= 10 && x._Month <= 12).ToList();

                        QuarterOBR _data = new QuarterOBR();
                        double _amount = 0.00;
                        double _total = 0.00;

                        _data.Quarter = "1st Qtr. [Jan-Mar]";
                        foreach (var item in _First)
                        {
                            _amount += Convert.ToDouble(item.Total);
                        }
                        _total += _amount;
                        _data.Amount = _amount.ToString("#,##0.00");
                        _amount = 0;
                        QuarterOBR.Add(_data);

                        _data = new QuarterOBR();

                        _data.Quarter = "2nd Qtr. [Apr-Jun]";
                        foreach (var item in _Second)
                        {
                            _amount += Convert.ToDouble(item.Total);
                        }
                        _total += _amount;
                        _data.Amount = _amount.ToString("#,##0.00");
                        _amount = 0;
                        QuarterOBR.Add(_data);

                        _data = new QuarterOBR();

                        _data.Quarter = "3rd Qtr. [Jul-Sep]";
                        foreach (var item in _Third)
                        {
                            _amount += Convert.ToDouble(item.Total);
                        }
                        _total += _amount;
                        _data.Amount = _amount.ToString("#,##0.00");
                        _amount= 0;
                        QuarterOBR.Add(_data);

                        _data = new QuarterOBR();

                        _data.Quarter = "4th Qtr. [Oct-Dec]";
                        foreach (var item in _Fourth)
                        {
                            _amount += Convert.ToDouble(item.Total);
                        }
                        _total += _amount;
                        _data.Amount = _amount.ToString("#,##0.00");
                        _amount= 0;
                        QuarterOBR.Add(_data);

                        grdActual.DataSource = null;
                        grdActual.DataSource = QuarterOBR;

                        double _balance = (Convert.ToDouble(txtTotalAllocation.Text) - _total);

                        txtBalance.Text = _balance.ToString("#,##0.00");
                        if (_balance <=0)
                        {
                            MessageBox.Show("Cannot generate RIS due to low in budget amount");
                            //btnSubmit.IsEnabled = false;
                            //btnUp.IsEnabled = false;
                        }
                    break;
            }
            FetchActualMonths();
        }

        private void cmbType_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                grdActual.DataSource = null;
                _exp_data = new List<ExpenseItems>();
                cmbMonths.SelectedIndex = -1;

                grdExpense.DataSource = null;
                grdExpense.DataSource = _exp_data;

                grdExpense.FieldLayouts[0].Fields["Description"].Width = new Infragistics.Windows.DataPresenter.FieldLength(200);
                grdExpense.FieldLayouts[0].Fields["Quantity"].Width = new Infragistics.Windows.DataPresenter.FieldLength(100);

                grdExpense.FieldLayouts[0].Fields["Months"].Visibility = System.Windows.Visibility.Collapsed;
                grdExpense.FieldLayouts[0].Fields["Expense_Type"].Visibility = System.Windows.Visibility.Collapsed;
                grdExpense.FieldLayouts[0].Fields["NoDays"].Visibility = System.Windows.Visibility.Collapsed;
                FetchExpenseItems("");
                  FetchActualObligation();
            }
            catch (Exception)
            {
        
            }
           
            
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtSearch.Text=="")
            {
                grdExpense.DefaultFieldLayout.RecordFilters.Clear();
            }
            else
            {
                RecordFilter filter = new RecordFilter();
                filter.FieldName = "Description";
                filter.Conditions.Add(new ComparisonCondition(ComparisonOperator.StartsWith, txtSearch.Text));
                grdExpense.DefaultFieldLayout.RecordFilters.Add(filter);
            }
         
        }

        private void AddData()
        {
            SelectedRecordCollection src2 = grdExpense.SelectedItems.Records;

            RISData _xdata = new RISData();

            foreach (DataRecord record in src2)
            {

                ExpenseItems obj = record.DataItem as ExpenseItems;

                _xdata = new RISData();

                _xdata.mad_id = obj.Id;
                _xdata.stock_no = "";
                _xdata.unit = "pax";
                _xdata.description = obj.Description;
                _xdata.quantity = "1";

                _RISData.Add(_xdata);
                _ExpenseItems.Remove(obj);
            }

           

            RISFundLibrary _selected = (RISFundLibrary)grdPPMP.ActiveDataItem;

             string _extype = "";
             switch (_selected.ExpenseItem)
             {
                 case "Other Supplies":
                     _extype = "EXP-OTS";
                     break;
                 case "ICT Office Supplies":
                     _extype = "EXP-ICT";
                     break;
                 case "Office Supplies Expenses":
                     _extype = "EXP-OS";
                     break;
                 case "Local Travel":
                     _extype = "Local Travel";
                     break;
             }

             _exp_data = new List<ExpenseItems>();
             _exp_data = _ExpenseItems;
             grdExpense.DataSource = null;
             grdExpense.DataSource = _exp_data;

             grdExpense.FieldLayouts[0].Fields["Description"].Width = new Infragistics.Windows.DataPresenter.FieldLength(200);
             grdExpense.FieldLayouts[0].Fields["Quantity"].Width = new Infragistics.Windows.DataPresenter.FieldLength(100);

             grdExpense.FieldLayouts[0].Fields["Months"].Visibility = System.Windows.Visibility.Collapsed;
             grdExpense.FieldLayouts[0].Fields["Expense_Type"].Visibility = System.Windows.Visibility.Collapsed;
             grdExpense.FieldLayouts[0].Fields["NoDays"].Visibility = System.Windows.Visibility.Collapsed;

             grdData.DataSource = null;
             grdData.DataSource = _RISData; 

             grdData.FieldLayouts[0].Fields["stock_no"].Width = new Infragistics.Windows.DataPresenter.FieldLength(50);
             grdData.FieldLayouts[0].Fields["unit"].Width = new Infragistics.Windows.DataPresenter.FieldLength(50);
             grdData.FieldLayouts[0].Fields["description"].Width = new Infragistics.Windows.DataPresenter.FieldLength(250);
             grdData.FieldLayouts[0].Fields["quantity"].Width = new Infragistics.Windows.DataPresenter.FieldLength(40);

             grdData.FieldLayouts[0].Fields["stock_no"].Label = "STOCK NO";
             grdData.FieldLayouts[0].Fields["unit"].Label = "UNIT";
             grdData.FieldLayouts[0].Fields["description"].Label = "DESCRIPTION";
             grdData.FieldLayouts[0].Fields["quantity"].Label = "QTY";
             grdData.FieldLayouts[0].Fields["mad_id"].Visibility = System.Windows.Visibility.Collapsed;

        }

        private void btnUp_Click(object sender, RoutedEventArgs e)
        {
            AddData();
            if (grdExpense.Records.Count<=0)
            {
                btnUp.IsEnabled = false;
            }
        }
        private void SaveData() 
        {
            foreach (var item in grdData.DataSource)
            {
                RISData _xdata = (RISData)item;
                c_ris.SaveRISData(_xdata.mad_id, "", _xdata.unit, _xdata.description, _xdata.quantity, "Submitted", cmbMonths.SelectedItem.ToString(), txtPurpose.Text,this.DivId,this.User);
            }
            MessageBox.Show("RIS has been submitted for review/approval");
            FetchSubmittedRIS();

            _RISData = new List<RISData>();
            _ExpenseItems = new List<ExpenseItems>();

            grdData.DataSource = _RISData;
            grdExpense.DataSource = _ExpenseItems;


            grdData.FieldLayouts[0].Fields["stock_no"].Width = new Infragistics.Windows.DataPresenter.FieldLength(50);
            grdData.FieldLayouts[0].Fields["unit"].Width = new Infragistics.Windows.DataPresenter.FieldLength(50);
            grdData.FieldLayouts[0].Fields["description"].Width = new Infragistics.Windows.DataPresenter.FieldLength(250);
            grdData.FieldLayouts[0].Fields["quantity"].Width = new Infragistics.Windows.DataPresenter.FieldLength(40);

            grdData.FieldLayouts[0].Fields["stock_no"].Label = "STOCK NO";
            grdData.FieldLayouts[0].Fields["unit"].Label = "UNIT";
            grdData.FieldLayouts[0].Fields["description"].Label = "DESCRIPTION";
            grdData.FieldLayouts[0].Fields["quantity"].Label = "QTY";
            grdData.FieldLayouts[0].Fields["mad_id"].Visibility = System.Windows.Visibility.Collapsed;

            try
            {
                grdExpense.FieldLayouts[0].Fields["Description"].Width = new Infragistics.Windows.DataPresenter.FieldLength(200);
                grdExpense.FieldLayouts[0].Fields["Quantity"].Width = new Infragistics.Windows.DataPresenter.FieldLength(100);
                grdExpense.FieldLayouts[0].Fields["Months"].Visibility = System.Windows.Visibility.Collapsed;
                grdExpense.FieldLayouts[0].Fields["Expense_Type"].Visibility = System.Windows.Visibility.Collapsed;
                grdExpense.FieldLayouts[0].Fields["Id"].Visibility = System.Windows.Visibility.Collapsed;
            }
            catch (Exception)
            {

            }
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            if (txtPurpose.Text == "")
            {
                MessageBox.Show("Purpose filled must have data", "Cannot Proceed", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                SaveData();
            }
          
        }

        private void grdData_SelectedItemsChanged(object sender, Infragistics.Windows.DataPresenter.Events.SelectedItemsChangedEventArgs e)
        {
           
        }

        private void grdPPMP_SelectedItemsChanged(object sender, Infragistics.Windows.DataPresenter.Events.SelectedItemsChangedEventArgs e)
        {
            _RISData = new List<RISData>();
            _ExpenseItems = new List<ExpenseItems>();

            grdData.DataSource = _RISData;
            grdExpense.DataSource = _ExpenseItems;


            grdData.FieldLayouts[0].Fields["stock_no"].Width = new Infragistics.Windows.DataPresenter.FieldLength(50);
            grdData.FieldLayouts[0].Fields["unit"].Width = new Infragistics.Windows.DataPresenter.FieldLength(50);
            grdData.FieldLayouts[0].Fields["description"].Width = new Infragistics.Windows.DataPresenter.FieldLength(250);
            grdData.FieldLayouts[0].Fields["quantity"].Width = new Infragistics.Windows.DataPresenter.FieldLength(40);

            grdData.FieldLayouts[0].Fields["stock_no"].Label = "STOCK NO";
            grdData.FieldLayouts[0].Fields["unit"].Label = "UNIT";
            grdData.FieldLayouts[0].Fields["description"].Label = "DESCRIPTION";
            grdData.FieldLayouts[0].Fields["quantity"].Label = "QTY";
            grdData.FieldLayouts[0].Fields["mad_id"].Visibility = System.Windows.Visibility.Collapsed;

            try
            {
                grdExpense.FieldLayouts[0].Fields["Description"].Width = new Infragistics.Windows.DataPresenter.FieldLength(200);
                grdExpense.FieldLayouts[0].Fields["Quantity"].Width = new Infragistics.Windows.DataPresenter.FieldLength(100);
                grdExpense.FieldLayouts[0].Fields["Months"].Visibility = System.Windows.Visibility.Collapsed;
                grdExpense.FieldLayouts[0].Fields["Expense_Type"].Visibility = System.Windows.Visibility.Collapsed;
                grdExpense.FieldLayouts[0].Fields["Id"].Visibility = System.Windows.Visibility.Collapsed;
            }
            catch (Exception)
            {

            }
            FetchActualObligation();
        }

        

       
    }
}
