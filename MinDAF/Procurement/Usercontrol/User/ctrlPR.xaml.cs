﻿using Procurement.Class;
using Procurement.Forms;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Procurement.Usercontrol.User
{
    /// <summary>
    /// Interaction logic for ctrlPR.xaml
    /// </summary>
    public partial class ctrlPR : UserControl
    {
        private List<ProgramList> _Programs = new List<ProgramList>();
        private List<ProjectList> _Projects = new List<ProjectList>();
        private List<OutputList> _Output = new List<OutputList>();
        private List<ActivityList> _Activity = new List<ActivityList>();
        private List<ExpenseItems> _ExpenseItems = new List<ExpenseItems>();
        private List<ExpenseTypes> _ExpenseTypes = new List<ExpenseTypes>();
        private List<ExpenseMonths> _ExpenseMonths = new List<ExpenseMonths>();

        private clsPR c_pr = new clsPR();

        public String DivId { get; set; }
        public String Division { get; set; }
        public String Office { get; set; }
        public String PapCode { get; set; }

        private List<PRItems> pData;

        public ctrlPR()
        {
            InitializeComponent();
            this.Loaded += ctrlPR_Loaded;
        }

        void ctrlPR_Loaded(object sender, RoutedEventArgs e)
        {
            Initialize();
        }

        public void Initialize() 
        {
            pData = new List<PRItems>();

            grdData.DataSource = pData;
            //txtDivision.Text = this.Division;
            txtOffice.Text = this.Office;
            txtPapCode.Text = this.PapCode;
            GenerateYear();

        }
        private void GenerateYear()
        {
            Int32 _start_year = 2016;
            cmbYear.Items.Clear();
            cmbType.Items.Clear();

            for (int i = 0; i < 2050; i++)
            {
                if (_start_year > 2050)
                {
                    break;
                }
                else
                {
                    cmbYear.Items.Add(_start_year);
                    _start_year += 1;
                }
              
            }


            cmbType.Items.Add("ICT Office Supplies");
            cmbType.Items.Add("Office Supplies");
            cmbType.Items.Add("Other Supplies");
        }

        private void FetchPrograms()
        {
            cmbProgram.Items.Clear();
            cmbProject.Items.Clear();
            cmbOutput.Items.Clear();
            cmbActivity.Items.Clear();


            _Programs.Clear();
            _Projects.Clear();
            _Output.Clear();
            _Activity.Clear();
            _ExpenseItems.Clear();
           // grdData.DataSource = pData;

            _Programs = c_pr.FetchPrograms(DivId, cmbYear.SelectedItem.ToString());


            foreach (ProgramList item in _Programs)
            {
                cmbProgram.Items.Add(item.Name);
            }
            //cmbProgram.ItemsSource = cmbProgram.DataContext;
            //   cmbProgram.SelectedIndex = -1;
        }
        private void FetchProjects()
        {
            string _Id = "";
            List<ProgramList> _xdata = _Programs.Where(x => x.Name == cmbProgram.SelectedItem).ToList();
            if (_xdata.Count != 0)
            {
                _Id = _xdata[0].Id.ToString();
            }
            cmbProject.Items.Clear();
            cmbOutput.Items.Clear();
            _Projects.Clear();

            _Projects = c_pr.FetchProjects(DivId, cmbYear.SelectedItem.ToString(), _Id);


            foreach (ProjectList item in _Projects)
            {
                cmbProject.Items.Add(item.Name);
            }

            // cmbProgram.SelectedIndex = -1;
            //cmbProgram.ItemsSource = cmbProgram.DataContext;
        }

        private void FetchOutput()
        {
            string _Id = "";
            List<ProjectList> _xdata = _Projects.Where(y => y.Name == cmbProject.SelectedItem).ToList();
            if (_xdata.Count != 0)
            {
                _Id = _xdata[0].Id.ToString();
            }
            cmbOutput.Items.Clear();
            cmbActivity.Items.Clear();
            _Output.Clear();
            _Output = c_pr.FetchOutput(DivId, cmbYear.SelectedItem.ToString(), _Id);


            foreach (OutputList item in _Output)
            {
                cmbOutput.Items.Add(item.Name);
            }

            // cmbProgram.SelectedIndex = -1;
            //cmbProgram.ItemsSource = cmbProgram.DataContext;
        }
        private void FetchActivity()
        {
            string _Id = "";
            List<OutputList> _xdata = _Output.Where(y => y.Name == cmbOutput.SelectedItem).ToList();
            if (_xdata.Count != 0)
            {
                _Id = _xdata[0].Id.ToString();
            }
            cmbActivity.Items.Clear();
            _Activity.Clear();
            _Activity = c_pr.FetchActivity(DivId, cmbYear.SelectedItem.ToString(), _Id);


            foreach (ActivityList item in _Activity)
            {
                cmbActivity.Items.Add(item.Name);
            }

            // cmbProgram.SelectedIndex = -1;
            //cmbProgram.ItemsSource = cmbProgram.DataContext;
        }
        private void FetchExpenseItems()
        {
            FetchExpenseType();

            string _Id = "";
            List<ActivityList> _xdata = _Activity.Where(y => y.Name == cmbActivity.SelectedItem).ToList();
            if (_xdata.Count != 0)
            {
                _Id = _xdata[0].Id.ToString();
            }

            _ExpenseItems.Clear();
            _ExpenseItems = c_pr.FetchExpenseItems(DivId, cmbYear.SelectedItem.ToString(), _Id);

            
        }

        private void FetchExpenseType() 
        {
            string _Id = "";
            List<ActivityList> _xdata = _Activity.Where(y => y.Name == cmbActivity.SelectedItem).ToList();
            if (_xdata.Count != 0)
            {
                _Id = _xdata[0].Id.ToString();
            }

            _ExpenseTypes.Clear();
            _ExpenseTypes = c_pr.FetchExpenseTypes(DivId, cmbYear.SelectedItem.ToString(), _Id);

            cmbType.Items.Clear();

            foreach (var item in _ExpenseTypes)
            {
                cmbType.Items.Add(item.Name);
            }

          
           

        }
         private void FetchExpenseMonths() 
                {
                    string _Id = "";
                    List<ActivityList> _xdata = _Activity.Where(y => y.Name == cmbActivity.SelectedItem).ToList();
                    if (_xdata.Count != 0)
                    {
                        _Id = _xdata[0].Id.ToString();
                    }

                    _ExpenseMonths.Clear();
                    _ExpenseMonths = c_pr.FetchExpenseMonths(DivId, cmbYear.SelectedItem.ToString(), _Id);

                    cmbMonths.Items.Clear();
                    List<String> monthNames = DateTimeFormatInfo.CurrentInfo.MonthNames.ToList();
          
                    foreach (var itemm in monthNames)
                    {
                        if (itemm!="")
                        {
                            string str = itemm.Substring(0, 3);
                            List<ExpenseMonths> _month = _ExpenseMonths.Where(x => x.Name == str).ToList();
                            if (_month.Count != 0)
                            {
                                cmbMonths.Items.Add(str);
                            } 
                        }
                       
                    }
                   
           

                }
        private void cmbYear_DropDownClosed(object sender, EventArgs e)
        {
            FetchPrograms();
        }

        private void cmbProgram_DropDownClosed(object sender, EventArgs e)
        {
            FetchProjects();
        }

        private void cmbProject_DropDownClosed(object sender, EventArgs e)
        {
            FetchOutput();
        }

        private void cmbOutput_DropDownClosed(object sender, EventArgs e)
        {
            FetchActivity();
        }

        private void cmbActivity_DropDownClosed(object sender, EventArgs e)
        {
            txtPurpose.Text = cmbActivity.SelectedItem.ToString();
            FetchExpenseItems();

         //   GenerateMonths();
        }

        private void AddToGrid(String _unit,String _description,String _quantity,String _unit_cost,String _total) 
        {
            PRItems _data = new PRItems();
           
            _data.Unit = _unit;
            _data.Description = _description;
            _data.Quantity = _quantity;
            _data.Unit_Cost = _unit_cost;
            _data.Total_Cost = _total;

            pData.Add(_data);
        }

        private void cmbType_DropDownClosed(object sender, EventArgs e)
        {
            if ( cmbType.SelectedItem!=null)
            {
                FetchExpenseMonths();
                txtExpenseItem.Text = cmbType.SelectedItem.ToString();
            }
            
        }

        private frmTravelSchedule frm_sched = new frmTravelSchedule();

        private void cmbMonths_DropDownClosed(object sender, EventArgs e)
        {
            if (cmbType.SelectedItem==null)
            {
                return;
            }
            switch (cmbType.SelectedItem.ToString())
            {
                case "Local Travel":
                    List<ExpenseItems> _exp_data = _ExpenseItems.Where(x => x.Months == cmbMonths.SelectedItem.ToString() && x.Expense_Type == cmbType.SelectedItem.ToString()).ToList();
                    if (_exp_data.Count != 0)
                    {
                        frm_sched = new frmTravelSchedule();
                        frm_sched._DayLimit = Convert.ToInt32(_exp_data[0].NoDays);
                        frm_sched._Months = cmbMonths.SelectedItem.ToString();
                        frm_sched.ShowDialog();

                        if (frm_sched.DialogResult == true)
                        {


                            AddToGrid("", "Plane Ticket for " + frm_sched.TravelInfo.Personnel, "", "", "");
                            AddToGrid("pax", frm_sched.TravelInfo.TravelDate, _exp_data[0].Quantity, _exp_data[0].UnitCost, (Convert.ToDouble(_exp_data[0].UnitCost) * Convert.ToDouble(_exp_data[0].UnitCost)).ToString());

                            grdData.DataSource = null;
                            grdData.DataSource = pData;

                        }
                    }
                   
                    break;
                default:
                      //pData = new List<PRItems>();
                      // List<ExpenseItems> _exp_data = _ExpenseItems.Where(x => x.Months == cmbMonths.SelectedItem.ToString() && x.Expense_Type == cmbType.SelectedItem.ToString()).ToList();
                      // foreach (var item in _exp_data)
                      // {
                      //     PRItems _items = new PRItems();
                      //     _items.Description = item.Description;
                      //     _items.Quantity = item.Quantity;
                      //     _items.Unit ="pax";
                      //     _items.SP_NO = "";
                      //     _items.Unit_Cost = item.UnitCost;
                      //     _items.Total_Cost =(Convert.ToDouble(item.Quantity) * Convert.ToDouble(item.UnitCost)).ToString();

                      //     pData.Add(_items);
               
                      // }
                      //  grdData.DataSource = pData;
                    break;
            }
            FixColumn();
            
          
        }

        private void FixColumn() 
        {
            grdData.FieldLayouts[0].Fields["SP_NO"].Width = new Infragistics.Windows.DataPresenter.FieldLength(50);
            grdData.FieldLayouts[0].Fields["Unit"].Width = new Infragistics.Windows.DataPresenter.FieldLength(50);
            grdData.FieldLayouts[0].Fields["Description"].Width = new Infragistics.Windows.DataPresenter.FieldLength(150);
            grdData.FieldLayouts[0].Fields["Quantity"].Width = new Infragistics.Windows.DataPresenter.FieldLength(75);
            grdData.FieldLayouts[0].Fields["Unit_Cost"].Width = new Infragistics.Windows.DataPresenter.FieldLength(75);
            grdData.FieldLayouts[0].Fields["Total_Cost"].Width = new Infragistics.Windows.DataPresenter.FieldLength(75);
        }
       

    }

    public class PRItems 
    {
        public String SP_NO { get; set; }
        public String Unit { get; set; }
        public String Description { get; set; }
        public String Quantity { get; set; }
        public String Unit_Cost { get; set; }
        public String Total_Cost { get; set; }
    }
}
