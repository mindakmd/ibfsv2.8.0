﻿using Procurement.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Procurement.Usercontrol.User
{
    /// <summary>
    /// Interaction logic for ctrlDashboard.xaml
    /// </summary>
    public partial class ctrlDashboard : UserControl
    {
        public String DivId { get; set; }
        public String FundCode { get; set; }
        public String FundType { get; set; }

        private List<DashBoard> c_data = new List<DashBoard>();
        private List<FundSourceData> c_fundsource = new List<FundSourceData>();
        private List<ExpenseDetails> c_expense_detail = new List<ExpenseDetails>();

        private List<MonthList> c_months = new List<MonthList>();


        private clsDashboard c_dash = new clsDashboard();

        public ctrlDashboard()
        {
            InitializeComponent();
           
            this.Loaded += ctrlDashboard_Loaded;
            this.cmbFundsource.SelectionChanged += cmbFundsource_SelectionChanged;
            this.grdData.SelectedItemsChanged += grdData_SelectedItemsChanged;
        }

       
        private void Instantiate() 
        {
            c_data = new List<DashBoard>();
            c_expense_detail = new List<ExpenseDetails>();

            grdData.DataSource = null;
            grdData.DataSource = c_data;


            grdExpenseItems.DataSource = null;
            grdExpenseItems.DataSource = c_expense_detail;

            grdData.FieldLayouts[0].Fields["FundSourceID"].Visibility = System.Windows.Visibility.Collapsed;
           

            cmbFundsource.SelectedIndex = 0;
        }
        void grdData_SelectedItemsChanged(object sender, Infragistics.Windows.DataPresenter.Events.SelectedItemsChangedEventArgs e)
        {
            LoadSelectedUACS();

          
        }

      
        void cmbFundsource_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LoadDashboard();
          
        }

        void ctrlDashboard_Loaded(object sender, RoutedEventArgs e)
        {
            GenerateYear();          
            LoadFundsources();
            Instantiate();
        }

        private void LoadFundsources() 
        {
            c_fundsource = c_dash.FetchFundSources(this.DivId);
            cmbFundsource.Items.Clear();

            foreach (FundSourceData item in c_fundsource)
            {
                cmbFundsource.Items.Add(item.FundName);
            }
        }

        private void LoadSelectedUACS() 
        {
            DashBoard _selected = (DashBoard)grdData.ActiveDataItem;
        

            c_expense_detail = c_dash.FetchExpenseDetail(_selected.FundSourceID, cmbYear.SelectedItem.ToString(), _selected.UACS);
            grdExpenseItems.DataSource = null;
            grdExpenseItems.DataSource = c_expense_detail;

            grdExpenseItems.FieldLayouts[0].Fields["ItemDetail"].Visibility = System.Windows.Visibility.Collapsed;

            grdExpenseItems.FieldLayouts[0].Fields["Information"].Width = new Infragistics.Windows.DataPresenter.FieldLength(300);
            grdExpenseItems.FieldLayouts[0].Fields["Months"].Width = new Infragistics.Windows.DataPresenter.FieldLength(70);
            grdExpenseItems.FieldLayouts[0].Fields["RIS_Refference"].Label = "RIS Refference";
            grdExpenseItems.FieldLayouts[0].Fields["PR_Refference"].Label = "PR Refference";
        }
        private void LoadDashboard() 
        {
            List<FundSourceData> _selected = c_fundsource.Where(x => x.FundName == cmbFundsource.SelectedItem.ToString()).ToList();
            if (_selected.Count!=0)
            {
                this.FundCode = _selected[0].FundCode;
                this.FundType = _selected[0].FundType;

                c_data = c_dash.FetchDashboard(this.DivId, this.FundCode, cmbYear.SelectedItem.ToString(), this.FundType);
                grdData.DataSource = null;
                grdData.DataSource = c_data;

      

                grdData.FieldLayouts[0].Fields["FundSourceID"].Visibility = System.Windows.Visibility.Collapsed;
                grdData.FieldLayouts[0].Fields["UACS"].Visibility = System.Windows.Visibility.Collapsed;
            }
           
        }
        private void GenerateYear() 
        {
            int _Year = DateTime.Now.Year;
            cmbYear.Items.Clear();

            for (int i = 0; i < 20; i++)
            {
                cmbYear.Items.Add(_Year.ToString());
                _Year += 1;
            }

            cmbYear.SelectedIndex = 0;
        }

        private void grdMonths_SelectedItemsChanged(object sender, Infragistics.Windows.DataPresenter.Events.SelectedItemsChangedEventArgs e)
        {
            LoadSelectedUACS();
        }
    }
}
