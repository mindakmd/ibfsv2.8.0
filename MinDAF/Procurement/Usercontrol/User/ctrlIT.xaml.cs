﻿using Procurement.Class;
using Procurement.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Procurement.Usercontrol.User
{
    /// <summary>
    /// Interaction logic for ctrlIT.xaml
    /// </summary>
    public partial class ctrlIT : UserControl
    {
        public String DivId { get; set; }
        public String User { get; set; }
        public String TOCode { get; set; }
        public String TODescription { get; set; }
        public String TOPersonnel { get; set; }

        private frmITDetails f_itDetails = new frmITDetails();
        private clsIT c_it = new clsIT();
        private List<ITMain> c_main = new List<ITMain>();
        private List<ITDetails> c_details = new List<ITDetails>();
        public ctrlIT()
        {
            InitializeComponent();
            this.Loaded += ctrlIT_Loaded;
        }

        void ctrlIT_Loaded(object sender, RoutedEventArgs e)
        {
            LoadITData();
        }

        private void InitColumns() 
        {
            grdData.FieldLayouts[0].Fields["Code"].Visibility = System.Windows.Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["Dates"].Label = "Date";
            grdData.FieldLayouts[0].Fields["Dates"].Width = new Infragistics.Windows.DataPresenter.FieldLength(50);
            grdData.FieldLayouts[0].Fields["Details"].Label = "Purpose of Travel";
            grdData.FieldLayouts[0].Fields["Details"].Width = new Infragistics.Windows.DataPresenter.FieldLength(150);
            grdData.FieldLayouts[0].Fields["UserCreated"].Label = "Create By";
            grdData.FieldLayouts[0].Fields["UserCreated"].Width = new Infragistics.Windows.DataPresenter.FieldLength(50);

        }

        private void LoadITData() 
        {
            c_main = c_it.FetchItinerary(this.DivId);
            grdData.DataSource = null;
            grdData.DataSource = c_main;
            grdITDetails.DataSource = null;
            InitColumns();
         
        }
        private void DeleteData()
        {

            if (grdData.ActiveDataItem != null)
            {
                ITMain _code = (ITMain)grdData.ActiveDataItem;

                if (c_it.DeleteITData(_code.Code))
                {
                    MessageBox.Show("Entry has been deleted successfully!!", "Itinerary Deleted", MessageBoxButton.OK);
                    LoadITData();
                };

            }
            
        }

        private frmTOList f_TO = new frmTOList();
        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            f_TO = new frmTOList();

            f_TO.DivisionId = this.DivId;
            f_TO.ShowDialog();
            if (f_TO.DialogResult== true)
            {
                f_itDetails = new frmITDetails();
                f_itDetails.DivId = DivId;
                f_itDetails.UserId = User;
                f_itDetails.TOCode = f_TO.SelectedCode;
                f_itDetails.TODescription = f_TO.SelectedDescription;
                f_itDetails.TOPersonnel = f_TO.SelectedPersonnel;
                f_itDetails.TOPurpose = f_TO.SelectedPurpose;
                f_itDetails.RefreshData += f_itDetails_RefreshData;
                f_itDetails.ShowDialog();
            }
           
        }

        void f_itDetails_RefreshData(object sender, EventArgs e)
        {
            LoadITData();
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult _res = MessageBox.Show("Confirm Delete Entry?", "Save Data", MessageBoxButton.YesNo);
            if (_res.ToString() == "Yes")
            {
                DeleteData();
            }
        }

        private void btnPrintPreview_Click(object sender, RoutedEventArgs e)
        {

        }

        private void FetchITDetails() 
        {
            if (grdData.ActiveDataItem!=null)
            {
                ITMain _code = (ITMain)grdData.ActiveDataItem;

                c_details = c_it.FetchItineraryDetails(_code.Code);

                double _totalTranspo = 0.00;
                double _totalHotel = 0.00;
                double _totalMeals = 0.00;
                double _totalIncidental = 0.00;

                foreach (var item in c_details)
                {
                    _totalTranspo += Convert.ToDouble(item.Transpo_Expenses);
                    _totalHotel += Convert.ToDouble(item.Hotel_Allowance);
                    _totalMeals += Convert.ToDouble(item.Meal_Allowance);
                    _totalIncidental += Convert.ToDouble(item.Incidental);
                }

                txtTotalTranspo.Text = _totalTranspo.ToString("#,##0.00");
                txtTotalHotel.Text = _totalHotel.ToString("#,##0.00");
                txtTotalMeal.Text = _totalMeals.ToString("#,##0.00");
                txtTotalIncidental.Text = _totalIncidental.ToString("#,##0.00");

                txtTotalAmount.Text = (_totalTranspo +_totalMeals+_totalIncidental+_totalHotel).ToString("#,##0.00");


                grdITDetails.DataSource = null;
                grdITDetails.DataSource = c_details;
                InitColumnDetails();

            }
         
        }
        private void InitColumnDetails()
        {
            grdITDetails.FieldLayouts[0].Fields[0].Label = "Date";
            grdITDetails.FieldLayouts[0].Fields[0].Width = new Infragistics.Windows.DataPresenter.FieldLength(30);

            grdITDetails.FieldLayouts[0].Fields[1].Label = "Places to be visited";
            grdITDetails.FieldLayouts[0].Fields[1].Width = new Infragistics.Windows.DataPresenter.FieldLength(80);

            grdITDetails.FieldLayouts[0].Fields[2].Label = "Departure";
            grdITDetails.FieldLayouts[0].Fields[2].Width = new Infragistics.Windows.DataPresenter.FieldLength(35);

            grdITDetails.FieldLayouts[0].Fields[3].Label = "Arrival";
            grdITDetails.FieldLayouts[0].Fields[3].Width = new Infragistics.Windows.DataPresenter.FieldLength(35);

            grdITDetails.FieldLayouts[0].Fields[4].Label = "Means of Transportation";
            grdITDetails.FieldLayouts[0].Fields[4].Width = new Infragistics.Windows.DataPresenter.FieldLength(45);

            grdITDetails.FieldLayouts[0].Fields[5].Label = "Transportation Expense";
            grdITDetails.FieldLayouts[0].Fields[5].Width = new Infragistics.Windows.DataPresenter.FieldLength(45);

            grdITDetails.FieldLayouts[0].Fields[6].Label = "Hotel";
            grdITDetails.FieldLayouts[0].Fields[6].Width = new Infragistics.Windows.DataPresenter.FieldLength(25);

            grdITDetails.FieldLayouts[0].Fields[7].Label = "Meals";
            grdITDetails.FieldLayouts[0].Fields[7].Width = new Infragistics.Windows.DataPresenter.FieldLength(25);

            grdITDetails.FieldLayouts[0].Fields[8].Label = "Incidental";
            grdITDetails.FieldLayouts[0].Fields[8].Width = new Infragistics.Windows.DataPresenter.FieldLength(25);

            grdITDetails.FieldLayouts[0].Fields[9].Label = "Total Amount";
            grdITDetails.FieldLayouts[0].Fields[9].Width = new Infragistics.Windows.DataPresenter.FieldLength(30);
            grdITDetails.FieldLayouts[0].Fields[9].Visibility = System.Windows.Visibility.Collapsed;

        }
        private void grdData_SelectedItemsChanged(object sender, Infragistics.Windows.DataPresenter.Events.SelectedItemsChangedEventArgs e)
        {
            try
            {
                FetchITDetails();
            }
            catch (Exception)
            {

                grdITDetails.DataSource = null;
            }
        }
    }
}
