﻿using Procurement.Usercontrol.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Procurement.Usercontrol
{
    /// <summary>
    /// Interaction logic for ctrlAdmin.xaml
    /// </summary>
    public partial class ctrlAdmin : UserControl
    {
        private ctrlIMS c_ims = new ctrlIMS();
        private ctrlPPMPConsolidated c_ppmp = new ctrlPPMPConsolidated();
        public ctrlAdmin()
        {
            InitializeComponent();
        }

        private void mnuInven_Click(object sender, EventArgs e)
        {
            grdStack.Children.Clear();
            c_ims.Height = grdStack.Height;
            c_ims.Width = grdStack.Width;
            grdStack.Children.Add(c_ims);
        }

        private void mnuPPMPConsolidated_Click(object sender, RoutedEventArgs e)
        {
            grdStack.Children.Clear();
            c_ppmp.Height = grdStack.Height;
            c_ppmp.Width = grdStack.Width;
            grdStack.Children.Add(c_ppmp);
        }

        private void mnuPPMPConsolidated_SubmenuClosed(object sender, RoutedEventArgs e)
        {

        }
    }
}
