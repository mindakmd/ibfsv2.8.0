﻿using Procurement.Class;
using Procurement.Forms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Procurement.Usercontrol.Admin
{
    /// <summary>
    /// Interaction logic for ctrlPPMPConsolidated.xaml
    /// </summary>
    public partial class ctrlPPMPConsolidated : UserControl
    {

        private List<ppmpCONS_Division> _division = new List<ppmpCONS_Division>();
        private List<ppmpCONS_ExpenditureItems> _expenseitem = new List<ppmpCONS_ExpenditureItems>();

        private List<ppmpCONS_DivisionExpense> _divexpenseitem = new List<ppmpCONS_DivisionExpense>();
        private List<ppmpCONS_DivisionExpense> _divexpenseALLOWANCE = new List<ppmpCONS_DivisionExpense>();

        private List<ppmpCONS_PPMPConsolidated> _ppmpdata = new List<ppmpCONS_PPMPConsolidated>();

        private clsPPMPCons c_ppmp = new clsPPMPCons();


        public ctrlPPMPConsolidated()
        {
            InitializeComponent();
            this.Loaded += ctrlPPMPConsolidated_Loaded;
        }

        private void Initialize() 
        {


            grdData.DataSource = null;
            grdData.DataSource = _ppmpdata;
            //grdData.FieldLayouts[0].Fields["TypeExpense"].Visibility = System.Windows.Visibility.Collapsed;
            //grdData.FieldLayouts[0].Fields["Description"].Width = new Infragistics.Windows.DataPresenter.FieldLength(200);

        }
        void ctrlPPMPConsolidated_Loaded(object sender, RoutedEventArgs e)
        {
            Initialize();
        }

        private void LoadExpenseItems() 
        {
            List<String> _InOrder = new List<string>();

            if (chkCSE.IsChecked== true)
            {
                _InOrder.Add("4");
             
                _InOrder.Add("82");
                _InOrder.Add("81");
                _InOrder.Add("15");

            }
            else
            {
                _InOrder.Add("1");
                _InOrder.Add("2");
                _InOrder.Add("105");
                _InOrder.Add("6");
                _InOrder.Add("52");
                _InOrder.Add("46");
                _InOrder.Add("37");
                _InOrder.Add("103");
                _InOrder.Add("11");
                _InOrder.Add("85");
                _InOrder.Add("36");
                _InOrder.Add("8");
                _InOrder.Add("9");
                _InOrder.Add("13");
                _InOrder.Add("19");
                _InOrder.Add("20");
                _InOrder.Add("25");
                _InOrder.Add("80");
                _InOrder.Add("83");
                _InOrder.Add("92");
                _InOrder.Add("96");
                _InOrder.Add("97");
                _InOrder.Add("98");
                _InOrder.Add("99");
                _InOrder.Add("101");
                _InOrder.Add("102");

                _InOrder.Add("17");
                _InOrder.Add("51");
                _InOrder.Add("18");
                _InOrder.Add("3");
                _InOrder.Add("38");
                _InOrder.Add("");

            }

          

            _ppmpdata.Clear();
            _expenseitem = c_ppmp.FetchItemExpenditures();
            _division = c_ppmp.FetchDivisions();
            Int32 _count = 0;
            prgMain.Maximum = _expenseitem.Count;
            List<ppmpCONS_ExpenditureItems> _itemFixed = new List<ppmpCONS_ExpenditureItems>();
            foreach (String item in _InOrder)
            {
                 List<ppmpCONS_ExpenditureItems> _search = _expenseitem.Where(x => x.Id == item).ToList();
                 if (_search.Count != 0)
                {
                    ppmpCONS_ExpenditureItems _add = _search[0];
                    _itemFixed.Add(_add);
                }
            }
            _expenseitem.Clear();
            _expenseitem = _itemFixed;
           
            foreach (ppmpCONS_ExpenditureItems item in _expenseitem)
            {
                bool _noItems = false;
                ppmpCONS_PPMPConsolidated _data = new ppmpCONS_PPMPConsolidated();
               
             //  _data.Description ="---------------";
                if (item.Id == "4" || item.Id == "82" || item.Id == "81" || item.Id == "15")
                {
                    _data.TypeExpense = "CSE ITEMS";
                }
                else
                {
                    switch (item.Id)
                    {

                        case "105":
                        case "2":
                        case "6":
                        case "1":
                        case "52":
                        case "46":
                        case "37":
                        case "103":
                        case "44":
                        case "11":
                        case "85":
                        case "91":
                        case "8":
                        case "9":
                        case "13":
                        case "19":
                        case "20":
                        case "25":
                        case "80":
                        case "83":
                        case "92":
                        case "96":
                        case "97":
                        case "98":
                        case "99":
                        case "101":
                        case "102":
                        case "36": _data.TypeExpense = "GOODS AND SERVICES"; break;
                        case "17": _data.TypeExpense = "CONSULTANCY"; break;
                        case "51": _data.TypeExpense = "LABOR AND WAGES"; break;
                        case "18": _data.TypeExpense = "OTHER PROFESSIONAL SERVICES"; break;
                        case "3": _data.TypeExpense = "TRAINING EXPENSES"; break;
                        case "38": _data.TypeExpense = "Membership Dues & Contns to Organizations".ToUpper(); break;
                        case "26":
                        case "27":
                        case "28":
                        case "104":
                        case "15":
                            _data.TypeExpense = "NON PROCUREMENT ITEMS"; break;
                    }
                }
                

    
                if (item.Id=="85")
                {
                    
                }
                switch (item.Id)
	            {
                    case "1":
                        
                        ppmpCONS_PPMPConsolidated _totalVertical = new ppmpCONS_PPMPConsolidated();
                            double _FTotal = 0.00;
                            double _FtotJan = 0.00;
                            double _FtotFeb = 0.00;
                            double _FtotMar = 0.00;
                            double _FtotApr = 0.00;
                            double _FtotMay = 0.00;
                            double _FtotJun = 0.00;
                            double _FtotJul = 0.00;
                            double _FtotAug = 0.00;
                            double _FtotSep = 0.00;
                            double _FtotOct = 0.00;
                            double _FtotNov = 0.00;
                            double _FtotDec = 0.00;
                            double _F1QTR = 0.00;
                            double _F2QTR = 0.00;
                            double _F3QTR = 0.00;
                            double _F4QTR = 0.00;
                        
                        foreach (ppmpCONS_Division item_d in _division)
                        {
                            

                            _totalVertical.Description = "Total :";
                            _totalVertical.Jan = "0.00";
                            if (item_d.DivName == "ISSP")
                            {

                            }
                            _divexpenseitem = c_ppmp.FetchExpenseItems(item_d.Id, item.Id);


                            int _stat = 0;
                            prdStat.Maximum = _divexpenseitem.Count;

                            if (_divexpenseitem.Count != 0)
                            {

                                double _Total = 0.00;
                                double _totJan = 0.00;
                                double _totFeb = 0.00;
                                double _totMar = 0.00;
                                double _totApr = 0.00;
                                double _totMay = 0.00;
                                double _totJun = 0.00;
                                double _totJul = 0.00;
                                double _totAug = 0.00;
                                double _totSep = 0.00;
                                double _totOct = 0.00;
                                double _totNov = 0.00;
                                double _totDec = 0.00;
                                double _1QTR = 0.00;
                                double _2QTR = 0.00;
                                double _3QTR = 0.00;
                                double _4QTR = 0.00;

                                int _Quantity = 0;
                       


                                _data = new ppmpCONS_PPMPConsolidated();

                                _data.MOOE = item.ExpenseItem.ToUpper() + "(PLANE TICKET)";
                                _data.TypeExpense = "GOODS AND SERVICES";
                                _data.Description = "PLANE TICKET";

                                foreach (var itemdei in _divexpenseitem)
                                {
                                    if (Convert.ToDouble(itemdei.rate) != 0)
                                    {
                                        double _rate = (Convert.ToDouble(itemdei.rate) * Convert.ToDouble(itemdei.no_staff));
                                        switch (itemdei.month)
                                        {
                                            case "Jan":
                                                _totJan += _rate;
                                                _Quantity += 1 * Convert.ToInt32(itemdei.no_staff);
                                                break;
                                            case "Feb":
                                                _totFeb += _rate;
                                                _Quantity += 1* Convert.ToInt32(itemdei.no_staff);;
                                                break;
                                            case "Mar":
                                                _totMar += _rate;
                                                _Quantity += 1* Convert.ToInt32(itemdei.no_staff);;
                                                break;
                                            case "Apr":
                                                _totApr += _rate;
                                                _Quantity += 1* Convert.ToInt32(itemdei.no_staff);;
                                                break;
                                            case "May":
                                                _totMay += _rate;
                                                _Quantity += 1;
                                                break;
                                            case "Jun":
                                                _totJun += _rate;
                                                _Quantity += 1;
                                                break;
                                            case "Jul":
                                                _totJul += _rate;
                                                _Quantity += 1;
                                                break;
                                            case "Aug":
                                                _totAug += _rate;
                                                _Quantity += 1;
                                                break;
                                            case "Sep":
                                                _totSep += _rate;
                                                _Quantity += 1;
                                                break;
                                            case "Oct":
                                                _totOct += _rate;
                                                _Quantity += 1;
                                                break;
                                            case "Nov":
                                                _totNov += _rate;
                                                _Quantity += 1;
                                                break;
                                            case "Dec":
                                                _totDec += _rate;
                                                _Quantity += 1;
                                                break;
                                        }
                                    }


                                  

                                }
                                _1QTR = _totJan + _totFeb + _totMar;
                                _2QTR = _totApr + _totMay + _totJun;
                                _3QTR = _totJul + _totAug + _totSep;
                                _4QTR = _totOct + _totNov + _totDec;

                                _Total = _1QTR + _2QTR + _3QTR + _4QTR;

                                _data.QTY = _Quantity.ToString();
                                _data.EstimatedBudget = (_Total / _Quantity).ToString("#,##0.00");

                                _data.Jan = _totJan.ToString("#,##0.00");
                                _data.Feb = _totFeb.ToString("#,##0.00");
                                _data.Mar = _totMar.ToString("#,##0.00");
                                _data.Q1 = _1QTR.ToString("#,##0.00");

                                _data.Apr = _totApr.ToString("#,##0.00");
                                _data.May = _totMay.ToString("#,##0.00");
                                _data.Jun = _totJun.ToString("#,##0.00");
                                _data.Q2 = _2QTR.ToString("#,##0.00");

                                _data.Jul = _totJul.ToString("#,##0.00");
                                _data.Aug = _totAug.ToString("#,##0.00");
                                _data.Sep = _totSep.ToString("#,##0.00");
                                _data.Q3 = _3QTR.ToString("#,##0.00");


                                _data.Oct = _totOct.ToString("#,##0.00");
                                _data.Nov = _totNov.ToString("#,##0.00");
                                _data.Dec = _totDec.ToString("#,##0.00");
                                _data.Q4 = _4QTR.ToString("#,##0.00");

                                _data.Total = _Total.ToString("#,##0.00");


                                _data.UACS = item_d.DivName;
                                if (_Total != 0)
                                {
                                    _ppmpdata.Add(_data);

                                    _FTotal += _Total;
                                    _FtotJan +=_totJan;
                                    _FtotFeb +=_totFeb;
                                    _FtotMar +=_totMar;
                                    _FtotApr += _totApr;
                                    _FtotMay += _totMay;
                                    _FtotJun += _totJun;
                                    _FtotJul += _totJul;
                                    _FtotAug +=_totAug;
                                    _FtotSep += _totSep;
                                    _FtotOct += _totOct;
                                    _FtotNov += _totNov;
                                    _FtotDec += _totDec;
                                    _F1QTR += _1QTR;
                                    _F2QTR += _2QTR;
                                    _F3QTR += _3QTR;
                                    _F4QTR += _4QTR;
                                    
                                }


                                _stat += 1;
                                prdStat.Value = _stat;
                                DoEvents();
                            }
                           
                            
                            DoEvents();



                        }
                

                        //'---------------------- DSA TRAVEL'
                        foreach (ppmpCONS_Division item_d in _division)
                        {

                            if (item_d.DivName == "ISSP")
                            {

                            }
                            _divexpenseitem = c_ppmp.FetchExpenseItems(item_d.Id, item.Id);


                            int _stat = 0;
                            prdStat.Maximum = _divexpenseitem.Count;

                            if (_divexpenseitem.Count != 0)
                            {

                                double _Total = 0.00;
                                double _totJan = 0.00;
                                double _totFeb = 0.00;
                                double _totMar = 0.00;
                                double _totApr = 0.00;
                                double _totMay = 0.00;
                                double _totJun = 0.00;
                                double _totJul = 0.00;
                                double _totAug = 0.00;
                                double _totSep = 0.00;
                                double _totOct = 0.00;
                                double _totNov = 0.00;
                                double _totDec = 0.00;
                                double _1QTR = 0.00;
                                double _2QTR = 0.00;
                                double _3QTR = 0.00;
                                double _4QTR = 0.00;

                                int _Quantity = 0;

                                _data = new ppmpCONS_PPMPConsolidated();
                                _data.MOOE = item.ExpenseItem.ToUpper() + " DSA (ALLOWANCE/HOTEL)"; ;
                                _data.TypeExpense = "GOODS AND SERVICES";
                                _data.Description = "DSA (ALLOWANCE/HOTEL)";
                                foreach (var itemdei in _divexpenseitem)
                                {
                                    if (Convert.ToDouble(itemdei.rate) != 0)
                                    {
                                      
                                        if (itemdei.no_days=="")
                                        {
                                            itemdei.no_days = "0";
                                        }
                                        if (itemdei.no_staff == "")
                                        {
                                            itemdei.no_staff = "0";
                                        }
                                        if (itemdei.allowance == "")
                                        {
                                            itemdei.allowance = "0";
                                        }
                                        double _allowance = (Convert.ToDouble(itemdei.allowance) * Convert.ToDouble(itemdei.no_staff)) * Convert.ToDouble(itemdei.no_days);
                                        switch (itemdei.month)
                                        {
                                            case "Jan":
                                                _totJan += _allowance;
                                                _Quantity += 1;
                                                break;
                                            case "Feb":
                                                _totFeb += _allowance;
                                                _Quantity += 1;
                                                break;
                                            case "Mar":
                                                _totMar += _allowance;
                                                _Quantity += 1;
                                                break;
                                            case "Apr":
                                                _totApr += _allowance;
                                                _Quantity += 1;
                                                break;
                                            case "May":
                                                _totMay += _allowance;
                                                _Quantity += 1;
                                                break;
                                            case "Jun":
                                                _totJun += _allowance;
                                                _Quantity += 1;
                                                break;
                                            case "Jul":
                                                _totJul += _allowance;
                                                _Quantity += 1;
                                                break;
                                            case "Aug":
                                                _totAug += _allowance;
                                                _Quantity += 1;
                                                break;
                                            case "Sep":
                                                _totSep += _allowance;
                                                _Quantity += 1;
                                                break;
                                            case "Oct":
                                                _totOct += _allowance;
                                                _Quantity += 1;
                                                break;
                                            case "Nov":
                                                _totNov += _allowance;
                                                _Quantity += 1;
                                                break;
                                            case "Dec":
                                                _totDec += _allowance;
                                                _Quantity += 1;
                                                break;
                                        }
                                    }




                                }
                                _1QTR = _totJan + _totFeb + _totMar;
                                _2QTR = _totApr + _totMay + _totJun;
                                _3QTR = _totJul + _totAug + _totSep;
                                _4QTR = _totOct + _totNov + _totDec;

                                _Total = _1QTR + _2QTR + _3QTR + _4QTR;

                                _data.QTY = _Quantity.ToString();
                                _data.EstimatedBudget = (_Total / _Quantity).ToString("#,##0.00");

                                _data.Jan = _totJan.ToString("#,##0.00");
                                _data.Feb = _totFeb.ToString("#,##0.00");
                                _data.Mar = _totMar.ToString("#,##0.00");
                                _data.Q1 = _1QTR.ToString("#,##0.00");

                                _data.Apr = _totApr.ToString("#,##0.00");
                                _data.May = _totMay.ToString("#,##0.00");
                                _data.Jun = _totJun.ToString("#,##0.00");
                                _data.Q2 = _2QTR.ToString("#,##0.00");

                                _data.Jul = _totJul.ToString("#,##0.00");
                                _data.Aug = _totAug.ToString("#,##0.00");
                                _data.Sep = _totAug.ToString("#,##0.00");
                                _data.Q3 = _3QTR.ToString("#,##0.00");


                                _data.Oct = _totOct.ToString("#,##0.00");
                                _data.Nov = _totNov.ToString("#,##0.00");
                                _data.Dec = _totDec.ToString("#,##0.00");
                                _data.Q4 = _4QTR.ToString("#,##0.00");

                                _data.Total = _Total.ToString("#,##0.00");


                                _data.UACS = item_d.DivName;
                                if (_Total != 0)
                                {
                                    _ppmpdata.Add(_data);

                                    _FTotal += _Total;
                                    _FtotJan += _totJan;
                                    _FtotFeb += _totFeb;
                                    _FtotMar += _totMar;
                                    _FtotApr += _totApr;
                                    _FtotMay += _totMay;
                                    _FtotJun += _totJun;
                                    _FtotJul += _totJul;
                                    _FtotAug += _totAug;
                                    _FtotSep += _totSep;
                                    _FtotOct += _totOct;
                                    _FtotNov += _totNov;
                                    _FtotDec += _totDec;
                                    _F1QTR += _1QTR;
                                    _F2QTR += _2QTR;
                                    _F3QTR += _3QTR;
                                    _F4QTR += _4QTR;
                                }


                                _stat += 1;
                                prdStat.Value = _stat;
                                DoEvents();
                            }
                        
                        }
             
                            DoEvents();

                        //'---------------------- ALLOWANCE TRAVEL'
                        foreach (ppmpCONS_Division item_d in _division)
                        {

                            if (item_d.DivName == "ISSP")
                            {

                            }
                            _divexpenseitem = c_ppmp.FetchExpenseItems(item_d.Id, item.Id);


                            int _stat = 0;
                            prdStat.Maximum = _divexpenseitem.Count;

                            if (_divexpenseitem.Count != 0)
                            {

                                double _Total = 0.00;
                                double _totJan = 0.00;
                                double _totFeb = 0.00;
                                double _totMar = 0.00;
                                double _totApr = 0.00;
                                double _totMay = 0.00;
                                double _totJun = 0.00;
                                double _totJul = 0.00;
                                double _totAug = 0.00;
                                double _totSep = 0.00;
                                double _totOct = 0.00;
                                double _totNov = 0.00;
                                double _totDec = 0.00;
                                double _1QTR = 0.00;
                                double _2QTR = 0.00;
                                double _3QTR = 0.00;
                                double _4QTR = 0.00;

                                int _Quantity = 0;

                                _data = new ppmpCONS_PPMPConsolidated();

                                _data.MOOE = item.ExpenseItem.ToUpper() + " LAND TRAVEL";
                                _data.Description = "LAND TRAVEL (ALLOWANCE)";
                                _data.TypeExpense = "GOODS AND SERVICES";
                                foreach (var itemdei in _divexpenseitem)
                                {
                                    if (Convert.ToDouble(itemdei.rate) == 0)
                                    {
                                        double _allowance = (Convert.ToDouble(itemdei.allowance) * Convert.ToDouble(itemdei.no_staff)) * Convert.ToDouble(itemdei.quantity);
                                        switch (itemdei.month)
                                        {
                                            case "Jan":
                                                _totJan += _allowance;
                                                _Quantity += 1;
                                                break;
                                            case "Feb":
                                                _totFeb += _allowance;
                                                _Quantity += 1;
                                                break;
                                            case "Mar":
                                                _totMar += _allowance;
                                                _Quantity += 1;
                                                break;
                                            case "Apr":
                                                _totApr += _allowance;
                                                _Quantity += 1;
                                                break;
                                            case "May":
                                                _totMay += _allowance;
                                                _Quantity += 1;
                                                break;
                                            case "Jun":
                                                _totJun += _allowance;
                                                _Quantity += 1;
                                                break;
                                            case "Jul":
                                                _totJul += _allowance;
                                                _Quantity += 1;
                                                break;
                                            case "Aug":
                                                _totAug += _allowance;
                                                _Quantity += 1;
                                                break;
                                            case "Sep":
                                                _totSep += _allowance;
                                                _Quantity += 1;
                                                break;
                                            case "Oct":
                                                _totOct += _allowance;
                                                _Quantity += 1;
                                                break;
                                            case "Nov":
                                                _totNov += _allowance;
                                                _Quantity += 1;
                                                break;
                                            case "Dec":
                                                _totDec += _allowance;
                                                _Quantity += 1;
                                                break;
                                        }
                                    }




                                }
                                _1QTR = _totJan + _totFeb + _totMar;
                                _2QTR = _totApr + _totMay + _totJun;
                                _3QTR = _totJul + _totAug + _totSep;
                                _4QTR = _totOct + _totNov + _totDec;

                                _Total = _1QTR + _2QTR + _3QTR + _4QTR;

                                _data.QTY = _Quantity.ToString();
                                _data.EstimatedBudget = (_Total / _Quantity).ToString("#,##0.00");

                                _data.Jan = _totJan.ToString("#,##0.00");
                                _data.Feb = _totFeb.ToString("#,##0.00");
                                _data.Mar = _totMar.ToString("#,##0.00");
                                _data.Q1 = _1QTR.ToString("#,##0.00");

                                _data.Apr = _totApr.ToString("#,##0.00");
                                _data.May = _totMay.ToString("#,##0.00");
                                _data.Jun = _totJun.ToString("#,##0.00");
                                _data.Q2 = _2QTR.ToString("#,##0.00");

                                _data.Jul = _totJul.ToString("#,##0.00");
                                _data.Aug = _totAug.ToString("#,##0.00");
                                _data.Sep = _totAug.ToString("#,##0.00");
                                _data.Q3 = _3QTR.ToString("#,##0.00");


                                _data.Oct = _totOct.ToString("#,##0.00");
                                _data.Nov = _totNov.ToString("#,##0.00");
                                _data.Dec = _totDec.ToString("#,##0.00");
                                _data.Q4 = _4QTR.ToString("#,##0.00");

                                _data.Total = _Total.ToString("#,##0.00");


                                _data.UACS = item_d.DivName;
                                if (_Total != 0)
                                {
                                    _ppmpdata.Add(_data);
                                    _FTotal += _Total;
                                    _FtotJan += _totJan;
                                    _FtotFeb += _totFeb;
                                    _FtotMar += _totMar;
                                    _FtotApr += _totApr;
                                    _FtotMay += _totMay;
                                    _FtotJun += _totJun;
                                    _FtotJul += _totJul;
                                    _FtotAug += _totAug;
                                    _FtotSep += _totSep;
                                    _FtotOct += _totOct;
                                    _FtotNov += _totNov;
                                    _FtotDec += _totDec;
                                    _F1QTR += _1QTR;
                                    _F2QTR += _2QTR;
                                    _F3QTR += _3QTR;
                                    _F4QTR += _4QTR;
                                }


                                _stat += 1;
                                prdStat.Value = _stat;
                                DoEvents();
                            }
                           
                            DoEvents();



                        }
              
                        break;

                    case  "2":
          
                        foreach (ppmpCONS_Division item_d in _division)
                        {

                            if (item_d.DivName == "ISSP")
                            {

                            }
                            _divexpenseitem = c_ppmp.FetchExpenseItems(item_d.Id, item.Id);


                            int _stat = 0;
                            prdStat.Maximum = _divexpenseitem.Count;

                            if (_divexpenseitem.Count != 0)
                            {

                                double _Total = 0.00;
                                double _totJan = 0.00;
                                double _totFeb = 0.00;
                                double _totMar = 0.00;
                                double _totApr = 0.00;
                                double _totMay = 0.00;
                                double _totJun = 0.00;
                                double _totJul = 0.00;
                                double _totAug = 0.00;
                                double _totSep = 0.00;
                                double _totOct = 0.00;
                                double _totNov = 0.00;
                                double _totDec = 0.00;
                                double _1QTR = 0.00;
                                double _2QTR = 0.00;
                                double _3QTR = 0.00;
                                double _4QTR = 0.00;

                                int _Quantity = 0;



                                _data = new ppmpCONS_PPMPConsolidated();

                                String _destination = "";
                                foreach (var itemdei in _divexpenseitem)
                                {

                                    _destination = itemdei.destination;
                                    if (Convert.ToDouble(itemdei.rate) != 0)
                                    {
                                        double _rate = (Convert.ToDouble(itemdei.rate) * Convert.ToDouble(itemdei.no_staff)) * Convert.ToDouble(itemdei.quantity);
                                        switch (itemdei.month)
                                        {
                                            case "Jan":
                                                _totJan += _rate;
                                                _Quantity += 1;
                                                break;
                                            case "Feb":
                                                _totFeb += _rate;
                                                _Quantity += 1;
                                                break;
                                            case "Mar":
                                                _totMar += _rate;
                                                _Quantity += 1;
                                                break;
                                            case "Apr":
                                                _totApr += _rate;
                                                _Quantity += 1;
                                                break;
                                            case "May":
                                                _totMay += _rate;
                                                _Quantity += 1;
                                                break;
                                            case "Jun":
                                                _totJun += _rate;
                                                _Quantity += 1;
                                                break;
                                            case "Jul":
                                                _totJul += _rate;
                                                _Quantity += 1;
                                                break;
                                            case "Aug":
                                                _totAug += _rate;
                                                _Quantity += 1;
                                                break;
                                            case "Sep":
                                                _totSep += _rate;
                                                _Quantity += 1;
                                                break;
                                            case "Oct":
                                                _totOct += _rate;
                                                _Quantity += 1;
                                                break;
                                            case "Nov":
                                                _totNov += _rate;
                                                _Quantity += 1;
                                                break;
                                            case "Dec":
                                                _totDec += _rate;
                                                _Quantity += 1;
                                                break;
                                        }
                                    }




                                }
                                _1QTR = _totJan + _totFeb + _totMar;
                                _2QTR = _totApr + _totMay + _totJun;
                                _3QTR = _totJul + _totAug + _totSep;
                                _4QTR = _totOct + _totNov + _totDec;

                                _Total = _1QTR + _2QTR + _3QTR + _4QTR;

                                _data.QTY = _Quantity.ToString();
                                _data.EstimatedBudget = (_Total / _Quantity).ToString("#,##0.00");
                                _data.Description = _destination;
                                _data.TypeExpense = "GOODS AND SERVICES";
                                _data.MOOE = item.ExpenseItem.ToUpper();

                                _data.Jan = _totJan.ToString("#,##0.00");
                                _data.Feb = _totFeb.ToString("#,##0.00");
                                _data.Mar = _totMar.ToString("#,##0.00");
                                _data.Q1 = _1QTR.ToString("#,##0.00");

                                _data.Apr = _totApr.ToString("#,##0.00");
                                _data.May = _totMay.ToString("#,##0.00");
                                _data.Jun = _totJun.ToString("#,##0.00");
                                _data.Q2 = _2QTR.ToString("#,##0.00");

                                _data.Jul = _totJul.ToString("#,##0.00");
                                _data.Aug = _totAug.ToString("#,##0.00");
                                _data.Sep = _totAug.ToString("#,##0.00");
                                _data.Q3 = _3QTR.ToString("#,##0.00");


                                _data.Oct = _totOct.ToString("#,##0.00");
                                _data.Nov = _totNov.ToString("#,##0.00");
                                _data.Dec = _totDec.ToString("#,##0.00");
                                _data.Q4 = _4QTR.ToString("#,##0.00");

                                _data.Total = _Total.ToString("#,##0.00");


                                _data.UACS = item_d.DivName;
                                if (_Total != 0)
                                {
                                    _ppmpdata.Add(_data);

                                }



                                _stat += 1;
                                prdStat.Value = _stat;
                                DoEvents();
                            }
                           
                            DoEvents();



                        }


                        break;
                   default:
                       

                        if (item.Id =="4" || item.Id  == "82" || item.Id =="81" || item.Id =="15")
                        {
                         //   _ppmpdata.Clear();
                            List<ppmpCONS_InventoryItems> Inventory = c_ppmp.FetchItemInventory();
                            prgMain.Maximum = Inventory.Count;
                            prgMain.Value = 0;
                                _count =0;
                            _divexpenseitem = c_ppmp.FetchExpenseItemsCSE(item.Id);
                            foreach (ppmpCONS_InventoryItems itemInven in Inventory)
                            {
                                if (itemInven.item_specifications.Contains("Plot"))
                                {

                                }
                                double _Total = 0.00;
                                double _totJan = 0.00;
                                double _totFeb = 0.00;
                                double _totMar = 0.00;
                                double _totApr = 0.00;
                                double _totMay = 0.00;
                                double _totJun = 0.00;
                                double _totJul = 0.00;
                                double _totAug = 0.00;
                                double _totSep = 0.00;
                                double _totOct = 0.00;
                                double _totNov = 0.00;
                                double _totDec = 0.00;
                                double _1QTR = 0.00;
                                double _2QTR = 0.00;
                                double _3QTR = 0.00;
                                double _4QTR = 0.00;
                                int _Quantity = 0;
                                int _stat = 0;
                                String DivisionName = "";

                                String _destination = "";
                                _data = new ppmpCONS_PPMPConsolidated();
                                _data.TypeExpense = "CSE ITEMS";
                                _destination = itemInven.item_specifications;
                                prdStat.Maximum = _division.Count;
                                foreach (ppmpCONS_Division item_d in _division)
                                {


                                    List<ppmpCONS_DivisionExpense> _divdata = _divexpenseitem.Where(x => x.DivId == item_d.Id && x.destination == _destination).ToList();


                                    if (_divdata.Count != 0)
                                    {
                                        DivisionName += item_d.DivName + ", ";
                                    }

                                    foreach (var itemdei in _divdata)
                                    {

                                       
                                        if (Convert.ToDouble(itemdei.total) != 0)
                                        {
                                            double _rate = Convert.ToDouble(itemdei.total);// (Convert.ToDouble(itemdei.rate) * Convert.ToDouble(itemdei.no_staff)) * Convert.ToDouble(itemdei.quantity);
                                            switch (itemdei.month)
                                            {
                                                case "Jan":
                                                    _totJan += _rate;
                                                    _Quantity += 1;
                                                    break;
                                                case "Feb":
                                                    _totFeb += _rate;
                                                    _Quantity += 1;
                                                    break;
                                                case "Mar":
                                                    _totMar += _rate;
                                                    _Quantity += 1;
                                                    break;
                                                case "Apr":
                                                    _totApr += _rate;
                                                    _Quantity += 1;
                                                    break;
                                                case "May":
                                                    _totMay += _rate;
                                                    _Quantity += 1;
                                                    break;
                                                case "Jun":
                                                    _totJun += _rate;
                                                    _Quantity += 1;
                                                    break;
                                                case "Jul":
                                                    _totJul += _rate;
                                                    _Quantity += 1;
                                                    break;
                                                case "Aug":
                                                    _totAug += _rate;
                                                    _Quantity += 1;
                                                    break;
                                                case "Sep":
                                                    _totSep += _rate;
                                                    _Quantity += 1;
                                                    break;
                                                case "Oct":
                                                    _totOct += _rate;
                                                    _Quantity += 1;
                                                    break;
                                                case "Nov":
                                                    _totNov += _rate;
                                                    _Quantity += 1;
                                                    break;
                                                case "Dec":
                                                    _totDec += _rate;
                                                    _Quantity += 1;
                                                    break;
                                            }
                                        }

                                     
                                    }
                                    _stat += 1;
                                    prdStat.Value = _stat;
                                    DoEvents();
                                }

                                _1QTR = _totJan + _totFeb + _totMar;
                                _2QTR = _totApr + _totMay + _totJun;
                                _3QTR = _totJul + _totAug + _totSep;
                                _4QTR = _totOct + _totNov + _totDec;

                                _Total = _1QTR + _2QTR + _3QTR + _4QTR;

                                _data.QTY = _Quantity.ToString();
                                _data.EstimatedBudget = (_Total / _Quantity).ToString("#,##0.00");
                                _data.Description = _destination;
                                _data.MOOE = item.ExpenseItem.ToUpper();

                                _data.Jan = _totJan.ToString("#,##0.00");
                                _data.Feb = _totFeb.ToString("#,##0.00");
                                _data.Mar = _totMar.ToString("#,##0.00");
                                _data.Q1 = _1QTR.ToString("#,##0.00");

                                _data.Apr = _totApr.ToString("#,##0.00");
                                _data.May = _totMay.ToString("#,##0.00");
                                _data.Jun = _totJun.ToString("#,##0.00");
                                _data.Q2 = _2QTR.ToString("#,##0.00");

                                _data.Jul = _totJul.ToString("#,##0.00");
                                _data.Aug = _totAug.ToString("#,##0.00");
                                _data.Sep = _totAug.ToString("#,##0.00");
                                _data.Q3 = _3QTR.ToString("#,##0.00");


                                _data.Oct = _totOct.ToString("#,##0.00");
                                _data.Nov = _totNov.ToString("#,##0.00");
                                _data.Dec = _totDec.ToString("#,##0.00");
                                _data.Q4 = _4QTR.ToString("#,##0.00");

                                _data.Total = _Total.ToString("#,##0.00");


                                _data.UACS = DivisionName;

                                DivisionName = "";

                                if (_Total != 0)
                                {
                                    _ppmpdata.Add(_data);

                                }
                                _count += 1;
                                prgMain.Value = _count;
                           
                                DoEvents();
                            }

                          
                        }
                        else
                        {
                            if (chkCSE.IsChecked== true)
                            {
                                continue;
                            }
                            foreach (ppmpCONS_Division item_d in _division)
                            {

                                if (item_d.DivName == "ISSP")
                                {

                                }
                                _divexpenseitem = c_ppmp.FetchExpenseItems(item_d.Id, item.Id);


                                int _stat = 0;
                                prdStat.Maximum = _divexpenseitem.Count;

                                if (_divexpenseitem.Count != 0)
                                {

                                    double _Total = 0.00;
                                    double _totJan = 0.00;
                                    double _totFeb = 0.00;
                                    double _totMar = 0.00;
                                    double _totApr = 0.00;
                                    double _totMay = 0.00;
                                    double _totJun = 0.00;
                                    double _totJul = 0.00;
                                    double _totAug = 0.00;
                                    double _totSep = 0.00;
                                    double _totOct = 0.00;
                                    double _totNov = 0.00;
                                    double _totDec = 0.00;
                                    double _1QTR = 0.00;
                                    double _2QTR = 0.00;
                                    double _3QTR = 0.00;
                                    double _4QTR = 0.00;

                                    int _Quantity = 0;



                                    _data = new ppmpCONS_PPMPConsolidated();

                                    switch (item.Id)
                                    {
                                        case "105":
                                        case "2":
                                        case "6":
                                        case "1":
                                        case "52":
                                        case "46":
                                        case "37":
                                        case "103":
                                        case "44":
                                        case "11":
                                        case "85":
                                        case "91":
                                        case "8":
                                        case "9":
                                        case "13":
                                        case "19":
                                        case "20":
                                        case "25":
                                        case "80":
                                        case "83":
                                        case "92":
                                        case "96":
                                        case "97":
                                        case "98":
                                        case "99":
                                        case "101":
                                        case "102":
                                        case "36": _data.TypeExpense = "GOODS AND SERVICES"; break;

                                        case "17": _data.TypeExpense = "CONSULTANCY"; break;
                                        case "51": _data.TypeExpense = "LABOR AND WAGES"; break;
                                        case "18": _data.TypeExpense = "OTHER PROFESSIONAL SERVICES"; break;
                                        case "3": _data.TypeExpense = "TRAINING EXPENSES"; break;
                                        case "38": _data.TypeExpense = "Membership Dues & Contns to Organizations".ToUpper(); break;
                                        case "26":
                                        case "27":
                                        case "28":
                                        case "104":
                                        case "15":
                                            _data.TypeExpense = "NON PROCUREMENT ITEMS"; break;
                                    }


                                    String _destination = _divexpenseitem[0].destination;
                                    foreach (var itemdei in _divexpenseitem)
                                    {
                                        _destination = itemdei.destination;
                                        if (Convert.ToDouble(itemdei.total) != 0)
                                        {
                                            double _rate = Convert.ToDouble(itemdei.total);// (Convert.ToDouble(itemdei.rate) * Convert.ToDouble(itemdei.no_staff)) * Convert.ToDouble(itemdei.quantity);
                                            switch (itemdei.month)
                                            {
                                                case "Jan":
                                                    _totJan += _rate;
                                                    _Quantity += 1;
                                                    break;
                                                case "Feb":
                                                    _totFeb += _rate;
                                                    _Quantity += 1;
                                                    break;
                                                case "Mar":
                                                    _totMar += _rate;
                                                    _Quantity += 1;
                                                    break;
                                                case "Apr":
                                                    _totApr += _rate;
                                                    _Quantity += 1;
                                                    break;
                                                case "May":
                                                    _totMay += _rate;
                                                    _Quantity += 1;
                                                    break;
                                                case "Jun":
                                                    _totJun += _rate;
                                                    _Quantity += 1;
                                                    break;
                                                case "Jul":
                                                    _totJul += _rate;
                                                    _Quantity += 1;
                                                    break;
                                                case "Aug":
                                                    _totAug += _rate;
                                                    _Quantity += 1;
                                                    break;
                                                case "Sep":
                                                    _totSep += _rate;
                                                    _Quantity += 1;
                                                    break;
                                                case "Oct":
                                                    _totOct += _rate;
                                                    _Quantity += 1;
                                                    break;
                                                case "Nov":
                                                    _totNov += _rate;
                                                    _Quantity += 1;
                                                    break;
                                                case "Dec":
                                                    _totDec += _rate;
                                                    _Quantity += 1;
                                                    break;
                                            }
                                        }




                                    }
                                    _1QTR = _totJan + _totFeb + _totMar;
                                    _2QTR = _totApr + _totMay + _totJun;
                                    _3QTR = _totJul + _totAug + _totSep;
                                    _4QTR = _totOct + _totNov + _totDec;

                                    _Total = _1QTR + _2QTR + _3QTR + _4QTR;

                                    _data.QTY = _Quantity.ToString();
                                    _data.EstimatedBudget = (_Total / _Quantity).ToString("#,##0.00");
                                    _data.Description = _destination;
                                    _data.MOOE = item.ExpenseItem.ToUpper();

                                    _data.Jan = _totJan.ToString("#,##0.00");
                                    _data.Feb = _totFeb.ToString("#,##0.00");
                                    _data.Mar = _totMar.ToString("#,##0.00");
                                    _data.Q1 = _1QTR.ToString("#,##0.00");

                                    _data.Apr = _totApr.ToString("#,##0.00");
                                    _data.May = _totMay.ToString("#,##0.00");
                                    _data.Jun = _totJun.ToString("#,##0.00");
                                    _data.Q2 = _2QTR.ToString("#,##0.00");

                                    _data.Jul = _totJul.ToString("#,##0.00");
                                    _data.Aug = _totAug.ToString("#,##0.00");
                                    _data.Sep = _totAug.ToString("#,##0.00");
                                    _data.Q3 = _3QTR.ToString("#,##0.00");


                                    _data.Oct = _totOct.ToString("#,##0.00");
                                    _data.Nov = _totNov.ToString("#,##0.00");
                                    _data.Dec = _totDec.ToString("#,##0.00");
                                    _data.Q4 = _4QTR.ToString("#,##0.00");

                                    _data.Total = _Total.ToString("#,##0.00");


                                    _data.UACS = item_d.DivName;



                                    if (_Total != 0)
                                    {
                                        _ppmpdata.Add(_data);

                                    }

                                    _stat += 1;
                                    prdStat.Value = _stat;
                                    DoEvents();
                                }

                                DoEvents();



                            }
                        }
                  
                        break;
	            }

              
                _count += 1;
                prgMain.Value = _count;
            }
            //List<ppmpCONS_PPMPConsolidated> _tempFilter =  _ppmpdata.Where(x=>x.UACS!=null).ToList();

            //_ppmpdata.Clear();
            //_ppmpdata = _tempFilter;

            prdStat.Value = 0;
            prgMain.Value = 0;
            grdData.DataSource = null;
            grdData.DataSource = _ppmpdata;

            //grdData.FieldLayouts[0].Fields["TypeExpense"].Visibility = System.Windows.Visibility.Collapsed;
            //grdData.FieldLayouts[0].Fields["Description"].Width = new Infragistics.Windows.DataPresenter.FieldLength(200);
            //grdData.FieldLayouts[0].Fields["UACS"].FixedLocation = Infragistics.Windows.DataPresenter.FixedFieldLocation.FixedToNearEdge;

        }
        public void DoEvents()
        {
            DispatcherFrame frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
                new DispatcherOperationCallback(ExitFrame), frame);
            Dispatcher.PushFrame(frame);
        }
        public object ExitFrame(object f)
        {
            ((DispatcherFrame)f).Continue = false;

            return null;
        }

        private void btnRevConsPPMP_Click(object sender, RoutedEventArgs e)
        {
            LoadExpenseItems();
        }

        private void grdData_FieldPositionChanged(object sender, Infragistics.Windows.DataPresenter.Events.FieldPositionChangedEventArgs e)
        {
            
        }

        private void grdData_FieldPositionChanging(object sender, Infragistics.Windows.DataPresenter.Events.FieldPositionChangingEventArgs e)
        {
           
        }
        private void PrintData() 
        {
           

            DataSet _dsRefrain = c_ppmp.ConvertReport(_ppmpdata);
            frmReportPreview _freport = new frmReportPreview("PPMP_CONSO",_dsRefrain);

            _freport.ShowDialog();
        }
        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PrintData();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            
        }
    }
}
