﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Procurement.Usercontrol.Admin
{
    /// <summary>
    /// Interaction logic for ctrlIMS.xaml
    /// </summary>
    public partial class ctrlIMS : UserControl
    {
        private ctrlItems c_items = new ctrlItems();

        public ctrlIMS()
        {
            InitializeComponent();
            
        }

        private void LoadItemsUI() 
        {
            grdStack.Children.Clear();
            c_items.Height = grdStack.ActualHeight;
            c_items.Width = grdStack.ActualWidth;
            grdStack.Children.Add(c_items);
        }

        private void btnManageItems_Click(object sender, RoutedEventArgs e)
        {
            LoadItemsUI();
        }
    }
}
