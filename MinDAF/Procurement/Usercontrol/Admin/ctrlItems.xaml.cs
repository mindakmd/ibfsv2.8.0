﻿using Infragistics.Windows.DataPresenter;
using Procurement.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Procurement.Usercontrol.Admin
{
    /// <summary>
    /// Interaction logic for ctrlItems.xaml
    /// </summary>
    public partial class ctrlItems : UserControl
    {
        private clsItems c_items = new clsItems();
        private DataTable dtItems;
        public ctrlItems()
        {
            InitializeComponent();
            this.Loaded += ctrlItems_Loaded;
        }

        void ctrlItems_Loaded(object sender, RoutedEventArgs e)
        {
            LoadItemTypes();
            LoadItems();
            AddComboDatas();
        }

        private void LoadItems() 
        {
            dtItems = c_items.LoadItems().DefaultView.ToTable();

            grdData.DataSource = dtItems.DefaultView;
            grdData.FieldLayouts[0].Fields["id"].Visibility = System.Windows.Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["code"].Visibility = System.Windows.Visibility.Collapsed;
            grdData.FieldLayouts[0].Fields["expenditure"].Visibility = System.Windows.Visibility.Collapsed;

            grdData.FieldLayouts[0].Fields["item_specifications"].Width = new Infragistics.Windows.DataPresenter.FieldLength(500);
            
        }
        private void LoadSelected()
        {

            DataRowView dt = (grdData.SelectedItems.Records[0] as DataRecord).DataItem as DataRowView;
            txtItemSpec.Text = dt.Row.ItemArray[3].ToString();
            cmbGenCat.Text = dt.Row.ItemArray[4].ToString();
            cmbSubCat.Text = dt.Row.ItemArray[5].ToString();
            cmbMinInven.Text = dt.Row.ItemArray[6].ToString();
            cmbUnitMeas.Text = dt.Row.ItemArray[7].ToString().ToUpper();

            txtPrice.Text = Convert.ToDouble(dt.Row.ItemArray[8].ToString()).ToString("#,##0.00"); 
        }

        private void AddComboDatas() 
        {


            cmbGenCat.Items.Clear();
            cmbItemType.Items.Clear();
            cmbMinInven.Items.Clear();
            cmbSubCat.Items.Clear();
            cmbUnitMeas.Items.Clear();

            cmbGenCat.Items.Add("AVAILABLE AT PROCUREMENT SERVICE STORES");
            cmbGenCat.Items.Add("NOT AVAILABLE AT PROCUREMENT SERVICE STORE");

            cmbSubCat.Items.Add("COMMON CLEANING SUPPLIES");
            cmbSubCat.Items.Add("COMMON COMPUTER SUPPLIES/CONSUMABLES");
            cmbSubCat.Items.Add("COMMON ELECTRICAL SUPPLIES");
            cmbSubCat.Items.Add("COMMON JANITORIAL SUPPLIES");
            cmbSubCat.Items.Add("COMMON OFFICE DEVICES");
            cmbSubCat.Items.Add("COMMON OFFICE SUPPLIES");
            cmbSubCat.Items.Add("COMMON OFFICE SUPPLIES 1");
            cmbSubCat.Items.Add("COMMON OFFICE SUPPLIES 2");
            cmbSubCat.Items.Add("COMMON OFFICE SUPPLIES 3");
            cmbSubCat.Items.Add("HANDBOOKS ON PROCUREMENT");
            cmbSubCat.Items.Add("LEGAL SIZE PAPER");
            cmbSubCat.Items.Add("OTHER SUPPLIES");

            cmbMinInven.Items.Add("YES");
            cmbMinInven.Items.Add("NO");

            cmbUnitMeas.Items.Add("UNIT");
            cmbUnitMeas.Items.Add("PIECE");
            cmbUnitMeas.Items.Add("ROLL");
            cmbUnitMeas.Items.Add("CART");
            cmbUnitMeas.Items.Add("ROLL");
            cmbUnitMeas.Items.Add("PACK");
            cmbUnitMeas.Items.Add("TUBE");
            cmbUnitMeas.Items.Add("BOX");
            cmbUnitMeas.Items.Add("CARD");
            cmbUnitMeas.Items.Add("PAD");
            cmbUnitMeas.Items.Add("REAM");
            cmbUnitMeas.Items.Add("ROLL");
            cmbUnitMeas.Items.Add("BAR");
            cmbUnitMeas.Items.Add("BOOK");
            cmbUnitMeas.Items.Add("BOTTLE");
            cmbUnitMeas.Items.Add("BUNDLE");
            cmbUnitMeas.Items.Add("CAN");
            cmbUnitMeas.Items.Add("CANISTER");
            cmbUnitMeas.Items.Add("CONT");
            cmbUnitMeas.Items.Add("CONTAINER");
            cmbUnitMeas.Items.Add("JAR");
            cmbUnitMeas.Items.Add("KILO");
            cmbUnitMeas.Items.Add("PACKET");
            cmbUnitMeas.Items.Add("PAIR");
            cmbUnitMeas.Items.Add("ROLL");
            cmbUnitMeas.Items.Add("POUCH");
            cmbUnitMeas.Items.Add("QUIRE");
            cmbUnitMeas.Items.Add("SET");
            cmbUnitMeas.Items.Add("SLEAVE");
            cmbUnitMeas.Items.Add("SPOOL");


            cmbItemType.Items.Add("Other Supplies");
            cmbItemType.Items.Add("ICT Office Supplies");
            cmbItemType.Items.Add("Office Supplies");
          
        }
        private void LockControls(Boolean _status) 
        {

        }

        private void LoadItemTypes() 
        {
            DataTable dtData = c_items.LoadItemTypes().Copy();
            cmbFilter.Items.Clear();

            cmbFilter.Items.Add("All");

            foreach (DataRow item in dtData.Rows)
            {
                cmbFilter.Items.Add(item[0].ToString());

            }
            cmbFilter.SelectedIndex = 0;
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (txtSearch.Text =="")
            {
                switch (cmbFilter.SelectedItem.ToString())
                {
                    case "All":
                        dtItems.DefaultView.RowFilter = "";
                        break;
                    case "Other Supplies":
                        dtItems.DefaultView.RowFilter = "expenditure ='" + cmbFilter.SelectedItem.ToString() + "'";
                        break;
                    case "ICT Office Supplies":
                        dtItems.DefaultView.RowFilter = "expenditure ='" + cmbFilter.SelectedItem.ToString() + "'";
                        break;
                    case "Office Supplies":
                        dtItems.DefaultView.RowFilter = "expenditure ='" + cmbFilter.SelectedItem.ToString() + "'";
                        break;
                    default:
                        break;
                }
            }
            else
            {
                switch (cmbFilter.SelectedItem.ToString())
                {
                    case "All":
                        dtItems.DefaultView.RowFilter = "item_specifications LIKE '" + txtSearch.Text + "%'";
                        break;
                    case "Other Supplies":
                        dtItems.DefaultView.RowFilter = "expenditure ='" + cmbFilter.SelectedItem.ToString() + "' AND item_specifications LIKE '" + txtSearch.Text + "%'";
                        break;
                    case "ICT Office Supplies":
                        dtItems.DefaultView.RowFilter = "expenditure ='" + cmbFilter.SelectedItem.ToString() + "' AND item_specifications LIKE '" + txtSearch.Text + "%'";
                        break;
                    case "Office Supplies":
                        dtItems.DefaultView.RowFilter = "expenditure ='" + cmbFilter.SelectedItem.ToString() + "' AND item_specifications LIKE '" + txtSearch.Text + "%'";
                        break;
                    default:
                        break;
                }
             
                  
               
            }
        }

        private void cmbFilter_DropDownClosed(object sender, EventArgs e)
        {
            switch (cmbFilter.SelectedItem.ToString())
            {
                case "All":
                    dtItems.DefaultView.RowFilter = "";
                    break;
                case "Other Supplies":
                    dtItems.DefaultView.RowFilter = "expenditure ='"+ cmbFilter.SelectedItem.ToString() +"'";
                    break;
                case "ICT Office Supplies":
                    dtItems.DefaultView.RowFilter = "expenditure ='" + cmbFilter.SelectedItem.ToString() + "'";
                    break;
                case "Office Supplies":
                    dtItems.DefaultView.RowFilter = "expenditure ='" + cmbFilter.SelectedItem.ToString() + "'";
                    break;
                default:
                    break;
            }
        }

        private void grdData_SelectedItemsChanged(object sender, Infragistics.Windows.DataPresenter.Events.SelectedItemsChangedEventArgs e)
        {
            LoadSelected();
        }
    }
}
