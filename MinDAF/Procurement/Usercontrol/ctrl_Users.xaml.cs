﻿using Procurement.Class;
using Procurement.Usercontrol.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Procurement.Usercontrol
{
    /// <summary>
    /// Interaction logic for ctrl_Users.xaml
    /// </summary>
    public partial class ctrl_Users : UserControl
    {
        public String _DivId { get; set; }
        public String _Division { get; set; }
        public String _Office { get; set; }
        public String _PapCode { get; set; }

        public String _User { get; set; }
        public Boolean  IsChief { get; set; }

        private ctrlUserForms c_forms = new ctrlUserForms();
        private ctrlRIS c_ris = new ctrlRIS();
        private ctrlRISLIstxaml c_ris_list = new ctrlRISLIstxaml();
        private ctrlDashboard c_dashboard = new ctrlDashboard();


        clsRIS _cls_ris = new clsRIS();


        public ctrl_Users()
        {
            InitializeComponent();
          
            this.Loaded += ctrl_Users_Loaded;
        }

        void ctrl_Users_Loaded(object sender, RoutedEventArgs e)
        {
            c_forms._DivId = this._DivId;
            c_forms._Division = this._Division;
            c_forms._Office = this._Office;
            c_forms._PapCode = this._PapCode;
            c_forms._User = this._User;
            if (IsChief)
            {
                LoadDashboard();
            }
        }

        private void LoadDashboard() 
        {
            c_dashboard = new ctrlDashboard();
            grdStack.Children.Clear();

            c_dashboard.DivId = this._DivId;
            c_dashboard.Height = grdStack.Height;
            c_dashboard.Width = grdStack.Width;
            grdStack.Children.Add(c_dashboard);
        }

        private void LoadRISList() 
        {
            c_ris_list = new ctrlRISLIstxaml();
            grdStack.Children.Clear();

            c_ris_list.DivId = this._DivId;
            c_ris_list.Height = grdStack.Height;
            c_ris_list.Width = grdStack.Width;
            grdStack.Children.Add(c_ris_list);
        }

        private void LoadRIS()
        {
            c_ris = new ctrlRIS();
            grdStack.Children.Clear();
            c_ris.DivId = this._DivId;
            c_ris.User = this._User;
            c_ris.Office = this._Office;
            c_ris.txtRespoCenter.Text = this._PapCode;
            c_ris.Division = this._Division;

            c_ris.Height = grdStack.Height;
            c_ris.Width = grdStack.Width;
            grdStack.Children.Add(c_ris);
        }
        private void mnuFile_Click(object sender, EventArgs e)
        {
            grdStack.Children.Clear();
            c_forms.Height = grdStack.Height;
            c_forms.Width = grdStack.Width;
            grdStack.Children.Add(c_forms);
        }

        private void mnuRIS_PR_Click(object sender, EventArgs e)
        {
            
        }

        private void mnuRISEntry_Click(object sender, RoutedEventArgs e)
        {
            LoadRIS();
        }

        private void mnuRISView_Click(object sender, RoutedEventArgs e)
        {
            LoadRISList();
          
        }

        private void mnuRISView_SubmenuClosed(object sender, RoutedEventArgs e)
        {
           
        }

        private void mnuRISView_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {
            
        }
    }
}
